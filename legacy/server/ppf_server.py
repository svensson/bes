#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "21/06/2019"

import os
import sys
import json
import time
import flask
import flask_cors
import pprint
import logging
import pathlib
import pymongo
import subprocess
from dateutil.parser import parse

from pymongo import MongoClient
from bson.objectid import ObjectId
from bson import json_util

import socket
from bes.workflow_lib import mxnice

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("workflow_server")

logger.debug(socket.gethostname())

app = flask.Flask(__name__)
flask_cors.CORS(app, support_credentials=True)

os.environ["EDNA_SITE"] = "ESRF_ID30A2"


dictRequest = {}
currentRequestId = 0

BES_HOME = pathlib.Path(__file__).parents[1] / "src"
BES_BIN = pathlib.Path(__file__).parents[1] / "bin"

EWOKS_PPF_HOME = pathlib.Path(__file__).parents[2] / "ewoksppf" / "src"
EWOKS_CORE_HOME = pathlib.Path(__file__).parents[2] / "ewokscore" / "src"
EWOKS_UTILS_HOME = pathlib.Path(__file__).parents[2] / "ewoksutils" / "src"

PYPUSHFLOW_HOST = socket.gethostname()
PYPUSHFLOW_PORT = os.environ.get("PYPUSHFLOW_PORT", "38280")
PYPUSHFLOW_MONGOURL = "mongodb://bes:bes@besdb1.esrf.fr:27017/bes"

PYPUSHFLOW_HOME = pathlib.Path(__file__).parents[2] / "pypushflow"
EDNA_HOME = "/opt/pxsoft/EDNA/vMX/edna"
EDNA2_HOME = pathlib.Path(__file__).parents[2] / "edna2" / "src"

sys.path.insert(0, str(BES_HOME))
print(sys.path)

if "PYPUSHFLOW_INITIATOR" not in os.environ:
    os.environ["PYPUSHFLOW_INITIATOR"] = "id30a2"

# Check that EDNA_SITE and ISPyB passwords are set:
if "EDNA_SITE" not in os.environ:
    logger.debug('WARNING: EDNA_SITE is not set, setting it to "ESRF".')
    os.environ["EDNA_SITE"] = "ESRF"
if os.environ["EDNA_SITE"] != "ESRF":
    if "ISPyB_user" not in os.environ:
        raise RuntimeError(
            'ISPyB_user is not set for EDNA_SITE="{}"!'.format(os.environ["EDNA_SITE"])
        )
    if "ISPyB_pass" not in os.environ:
        raise RuntimeError(
            'ISPyB_pass is not set for EDNA_SITE="{}"!'.format(os.environ["EDNA_SITE"])
        )


def search_bes(
    listName=[],
    listExcludeName=[],
    id=None,
    initiator=None,
    startDateTime=None,
    stopDateTime=None,
    status=None,
):
    client = MongoClient("besdb1.esrf.fr", 27017)
    db = client.bes
    collection = db.bes
    listWorkflows = []
    listQuery = []
    # Name
    for name in listName:
        if name != "":
            nameQuery = {"name": {"$regex": name, "$options": "i"}}
            listQuery.append(nameQuery)
    # Excluded name
    for excludeName in listExcludeName:
        if excludeName != "":
            nameQuery = {
                "name": {"$regex": "^((?!{0}).)*$".format(excludeName), "$options": "i"}
            }
            listQuery.append(nameQuery)
    # Id
    if id is not None:
        idQuery = {"_id": ObjectId(id)}
        listQuery.append(idQuery)
    # Initiator
    if initiator is not None:
        initiatorQuery = {"initiator": {"$regex": initiator, "$options": "i"}}
        listQuery.append(initiatorQuery)
    # Start date/time
    if startDateTime is not None:
        d1 = parse(startDateTime)
        startDateQuery = {"startTime": {"$gt": d1}}
        listQuery.append(startDateQuery)
    # Stop date/time
    if stopDateTime is not None and stopDateTime != "":
        d2 = parse(stopDateTime)
        stopDateQuery = {"stopTime": {"$lt": d2}}
        listQuery.append(stopDateQuery)
    # Status
    if status is not None:
        statusQuery = {"status": {"$regex": status, "$options": "i"}}
        listQuery.append(statusQuery)
    if len(listQuery) == 0:
        # Search for everything
        # Projection - remove actors
        for workflow in collection.find({}, {"actors": 0}):
            listWorkflows.append(workflow)
    else:
        if len(listQuery) == 1:
            query = listQuery[0]
        else:
            query = {"$and": listQuery}
        if id is None:
            for workflow in collection.find(query, {"actors": 0}):
                listWorkflows.append(workflow)
        else:
            for workflow in collection.find(query):
                listWorkflows.append(workflow)

    return listWorkflows


@app.route("/bes/requests", methods=["POST"])
@flask_cors.cross_origin(supports_credentials=True)
def requests_query():
    if not flask.request.json:
        flask.abort(400)
    dictRequest = dict(flask.request.json)
    logger.debug(dictRequest)
    dictResults = search_bes(**dictRequest)
    logger.debug(len(dictResults))
    return str(json_util.dumps(dictResults))


@app.route("/bes/readfile", methods=["POST"])
def readfile():
    fileContent = None
    dictRequest = dict(flask.request.json)
    if "filePath" in dictRequest:
        filePath = dictRequest["filePath"]
        if os.path.exists(filePath):
            with open(filePath) as f:
                fileContent = f.read()
        # Limit the number of lines to 10000
        if len(fileContent.split("\n")) > 10000:
            fileContent = "\n".join(fileContent.split("\n")[0:10000])
            fileContent += "...\n"
            fileContent += "Remainder of file truncated\n"
            fileContent += "See following file for full log:\n"
            fileContent += filePath + "\n"
    return fileContent


@app.route("/RUN/<string:workflowName>", methods=["POST"])
def do_run(workflowName, requestDict=None):
    global dictRequest
    logger.debug("*" * 80)
    logger.debug(workflowName)
    if requestDict is not None:
        logger.debug(requestDict)
    elif flask.request.json:
        requestDict = flask.request.json
        logger.debug(flask.request.json)
    else:
        flask.abort(400)
    objectId = str(ObjectId())
    requestDict["Request ID"] = objectId
    os.environ["PYPUSHFLOW_MONGOURL"] = PYPUSHFLOW_MONGOURL
    os.environ["PYPUSHFLOW_HOST"] = PYPUSHFLOW_HOST
    os.environ["PYPUSHFLOW_PORT"] = str(PYPUSHFLOW_PORT)
    os.environ["PYPUSHFLOW_OBJECTID"] = objectId
    os.environ["PYTHONPATH"] = (
        str(EWOKS_PPF_HOME)
        + ":"
        + str(EWOKS_CORE_HOME)
        + ":"
        + str(EWOKS_UTILS_HOME)
        + ":"
        + str(PYPUSHFLOW_HOME)
        + ":"
        + str(EDNA_HOME)
        + ":"
        + str(EDNA2_HOME)
        + ":"
        + str(BES_HOME)
    )
    logger.debug(os.environ["PYTHONPATH"])
    cmd = "/opt/pxsoft/bes/vgit/linux-x86_64/miniconda3/bin/python3 {0}/run_ppf_json.py".format(
        BES_BIN
    ) + " --inData '{0}' {1} &".format(
        json.dumps(requestDict), workflowName
    )
    logger.debug(cmd)
    # os.system(cmd)
    _ = subprocess.run(
        cmd,
        # stdout=subprocess.DEVNULL,
        # stderr=subprocess.DEVNULL,
        close_fds=True,
        env=os.environ,
        shell=True,
        preexec_fn=os.setsid,
    )
    dictRequest[objectId] = {
        "status": "RUNNING",
        "startTime": time.time(),
        "workflowName": workflowName,
    }
    logger.debug("Workflow {0} started, requestId = {1}".format(workflowName, objectId))
    return objectId


# /BES/bridge/rest/processes/TroubleShooting/RUN?sessionId=70195
@app.route("/BES/bridge/rest/processes/<string:workflowName>/RUN", methods=["POST"])
def do_legacy_run(workflowName):
    global dictRequest
    requestDict = dict(flask.request.args)
    objectId = do_run(workflowName, requestDict=requestDict)
    return objectId


@app.route("/ABORT/<string:requestId>")
def do_abort(requestId):
    global dictRequest
    logger.debug("Aborting request id {0}".format(requestId))
    # collection = pymongo.MongoClient(PYPUSHFLOW_MONGOURL).bes.bes
    # objectId = ObjectId(requestId)
    # dictWorkflow = collection.find_one({'_id': objectId})
    if requestId in dictRequest:
        pgrepCommand = "pgrep -f {0}".format(requestId)
        pgrepProcess = subprocess.run(pgrepCommand, shell=True, stdout=subprocess.PIPE)
        listPid = pgrepProcess.stdout.splitlines()
        if len(listPid) > 1:
            for pidBin in listPid:
                pid = int(pidBin.decode("utf-8"))
                logger.info("Killing pid {0}".format(pid))
                try:
                    os.kill(pid, 9)
                except ProcessLookupError:
                    pass
        # time.sleep(1)
        # pgrepProcess = subprocess.run(pgrepCommand, shell=True, stdout=subprocess.PIPE)
        # listPid = pgrepProcess.stdout.splitlines()
        # for pidBin in listPid:
        #     pid = int(pidBin.decode('utf-8'))
        #     logger.info("Remaining pid {0}".format(pid))
        returnValue = "Aborted {0}".format(requestId)
        dictRequest[requestId]["status"] = "ABORTED"
    else:
        returnValue = "No such process"
    logger.debug("Return value: " + returnValue)
    # if dictWorkflow is not None and dictWorkflow != {}:
    #     if "status" in dictWorkflow and dictWorkflow["status"] == "aborted":
    #         logger.debug("database entry status already set to aborted")
    #     else:
    #         dictWorkflow["status"] = "aborted"
    #         dictWorkflow["stopTime"] = datetime.datetime.now()
    #         collection.update_one({'_id': objectId}, {"$set": dictWorkflow}, upsert=False)
    #         logger.debug("Database entry set to aborted")
    return returnValue


@app.route("/BES/bridge/rest/processes/<string:requestId>/STOP", methods=["POST"])
def do_legacy_abort(requestId):
    return do_abort(requestId)


@app.route("/STATUS/<string:requestId>")
def get_status(requestId):
    global dictRequest
    logger.debug(pprint.pformat(dictRequest))
    collection = pymongo.MongoClient(PYPUSHFLOW_MONGOURL).bes.bes
    objectId = ObjectId(requestId)
    dictWorkflow = collection.find_one({"_id": objectId})
    if dictWorkflow is not None:
        returnValue = dictWorkflow["status"].upper()
    else:
        returnValue = "NOT EXISTS"
    return returnValue


@app.route("/BES/bridge/rest/processes/<string:requestId>/STATUS")
def get_legacy_status(requestId):
    return get_status(requestId)


@app.route("/SLURM/jobs/pending")
@app.route("/SLURM/jobs/pending/<string:partition>")
def are_jobs_pending(partition=None):
    jobsPending = mxnice.areJobsPending(partitionName=partition)
    return {"jobsPending": jobsPending}


@app.route("/")
def index():
    page = "<!DOCTYPE html>\n"
    page += "<html>\n"
    page += "<head></head>\n"
    page += "<body>\n"
    page += "<h1>BES server running on " + socket.gethostname() + "</h1>\n"
    page += "<br>\n"
    if len(dictRequest) == 0:
        page += "No running tasks<br>\n"
    else:
        page += "Running tasks:<br>\n"
        page += '<table border="1" cellpadding="5" cellspacing="0">\n'
        page += "<tr>\n"
        page += "<th>Request Id</th>\n"
        page += "<th>Start Time</th>\n"
        page += "<th>Status</th>\n"
        page += "</tr>\n"
        for requestId in dictRequest:
            status = get_status(requestId)
            _ = dictRequest[requestId]["process"]
            startTime = dictRequest[requestId]["startTime"]
            strDateTime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(startTime))
            # page += str(requestId) + "<br>\n"
            # page += str(status) + "<br>\n"
            # page += str(strDateTime) + "<br>\n"
            page += "<tr>\n"
            page += "<th>{0}</th>\n".format(requestId)
            page += "<th>{0}</th>\n".format(strDateTime)
            page += "<th>{0}</th>\n".format(status)
            page += "</tr>\n"
        page += "</table>\n"
    page += "</body>\n"
    return page


# <!DOCTYPE html>
# <html>
#    <head>
#    </head>
#
#    <body>
#       <h1>Programming Languages</h1>
# <table border="1" cellpadding="5" cellspacing="0" width="200px">
#          <tr>
#             <th>Language</th>
#             <th>Release Year</th>
#          </tr>
#          <tr>
#             <td>Java</td>
#             <td>1995</td>
#          </tr>
#          <tr>
#             <td>Pascal</td>
#             <td>1970</td>
#          </tr>
#       </table>
#    </body>

if __name__ == "__main__":
    # app.run(host='0.0.0.0', port=8092, debug=True)
    logger.info("BES_HOME: {0}".format(BES_HOME))
    app.run(host=PYPUSHFLOW_HOST, port=PYPUSHFLOW_PORT)
