import time
import requests
import unittest

from bson import json_util


class Test(unittest.TestCase):

    def tes_post(self):
        dictParameters = {
            "directory": "/data/id23eh2/inhouse/opid232/20200713/RAW_DATA/t2",
            "beamline": "id23eh2",
            "prefix": "t1",
            "run_number": 1,
            "sample_lims_id": 1,
        }
        r = requests.post(
            "http://mxbes2-1707.esrf.fr:31280/RUN/TroubleShooting", json=dictParameters
        )
        print(r)
        print(r.text)
        print(r.status_code)
        processId = r.text
        for i in range(10):
            r = requests.get(
                "http://mxbes2-1707.esrf.fr:31280/STATUS/{0}".format(processId)
            )
            print(r.text)
            time.sleep(1)

    #        print("Abort!")
    #        r = requests.post('http://mxhpc2-1705.esrf.fr:8093/ABORT/{0}'.format(processId))

    def tes_post2(self):
        dictParameters = {
            "directory": "/data/id30a2/inhouse/opid30a2/20190321/RAW_DATA/t1",
            "beamline": "id30a2",
            "prefix": "t1",
            "run_number": 1,
        }
        r = requests.post(
            "http://mxhpc2-1704.esrf.fr:8092/RUN/HelloWorldTask", json=dictParameters
        )
        print(r)
        print(r.text)
        print(r.status_code)

    def test_getStatus(self):
        # requestId = "5f4f7628cf28300eee2fd80a"
        requestId = "5f4f79922f589b6e5f3e8bc6"
        r = requests.get(
            "http://mxbes2-1707.esrf.fr:31280/STATUS/{0}".format(requestId)
        )
        print(r)
        print(r.text)
        print(r.status_code)

    def tes_abort(self):
        r = requests.get("http://mxhpc2-1705.esrf.fr:8092/ABORT/1")
        print(r)
        print(r.text)
        print(r.status_code)

    def tes_query(self):
        dictRequest = {"name": "trouble", "listStartTime": [2020, 1, 1]}
        r = requests.post(
            "http://mxhpc2-1705.esrf.fr:8094/bes/requests", json=dictRequest
        )
        print(r)
        print(r.text)
        print(json_util.loads(r.text))
        print(r.status_code)

    def test_slurm_jobs_pending(self):
        r = requests.get("http://mxbes2-1707.esrf.fr:38280/SLURM/jobs/pending")
        print(r)
        print(r.text)
        print(json_util.loads(r.text))
        print(r.status_code)
