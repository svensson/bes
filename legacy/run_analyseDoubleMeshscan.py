#!/opt/pxsoft/mxworkflows/id30a2/miniconda3/envs/id30a2/bin/python3.9

import os
import pprint
import sys

from bes.workflow_lib import doublemesh_lib
from bes.workflow_lib import workflow_logging

logger = workflow_logging.getLogger()

phiz = 0.0

if len(sys.argv) == 1:
    print(f"Usage: {os.path.basename(sys.argv[0])} dozorm2_working_directory")
    sys.exit(1)
dozorm2_working_directory = sys.argv[1]

print(f"Analysing {dozorm2_working_directory}")

collect_positions, potentialMatches = doublemesh_lib.analyseDoubleMeshscan(
    dozorm2_working_directory
)
all_positions = []
pprint.pprint(collect_positions)
if len(collect_positions) > 0:
    # logger.info("")
    # logger.info("Positions found by analyseDoubleMeshscan:")
    # logger.info("")
    # logger.info(collect_positions)
    for collect_position in collect_positions:
        resolution = collect_position[0]
        beam_size = collect_position[1]
        sampx = collect_position[2]
        sampy = collect_position[3]
        phiy = collect_position[4]
        # logger.info(f"Resolution: {resolution}, beam size: {beam_size}, sampx: {sampx}, sampy: {sampy}, phiy: {phiy}")
        position = {
            "sampx": sampx,
            "sampy": sampy,
            "phiy": phiy,
            "phiz": phiz,
            "beam_size": beam_size,
            "dozor2m_resolution": resolution,
        }
        all_positions.append(position)
    # logger.info("")
else:
    logger.warning("")
    logger.warning("No positions found by analyseDoubleMeshscan!")
    logger.warning("")

pprint.pprint(all_positions)
