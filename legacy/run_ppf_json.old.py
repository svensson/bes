#!/usr/bin/env /opt/pxsoft/bes/vgit/linux-x86_64/miniconda3/bin/python3
import os
import json
import sys
import logging
import pathlib

import pypushflow
from ewokscore import load_graph
from ewoksppf import execute_graph

BES_HOME = pathlib.Path(__file__).parents[2]
flowsDir = os.path.join(BES_HOME, "bes", "src", "bes", "flows")

inData = json.loads(sys.argv[2])
taskName = sys.argv[3]

user = os.environ.get("USER", "unknown")
initiator = os.environ.get("PYPUSHFLOW_INITIATOR", "pypushflow")
log_file_dir = pathlib.Path(f"/var/log/bes/{initiator}")
if not log_file_dir.exists():
    log_file_dir.mkdir(mode=0o755, parents=True)
log_file_path = log_file_dir / "{0}.log".format(taskName)

maxBytes = 1e7
backupCount = 10
fileHandler = logging.handlers.RotatingFileHandler(
    log_file_path, maxBytes=maxBytes, backupCount=backupCount
)
logFileFormat = "%(asctime)s %(levelname)-8s %(message)s"
formatter = logging.Formatter(logFileFormat)
fileHandler.setFormatter(formatter)
fileHandler.setLevel(logging.DEBUG)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("pypushflow")
logger.addHandler(fileHandler)

logger.info("*" * 80)
logger.info("*" * 80)
logger.info("*" * 80)
logger.info(sys.argv)

logger.debug("pypushflow path %s", pypushflow.__file__)

logger.debug("Before load_graph")

taskGraph = os.path.join(flowsDir, "{0}.json".format(taskName))
graph = load_graph(taskGraph, root_dir=flowsDir)

execute_graph(
    graph, load_options={"root_dir": flowsDir}, startargs=inData, pre_import=False
)
