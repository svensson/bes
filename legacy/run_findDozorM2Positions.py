#!/opt/pxsoft/mxworkflows/id30a2/miniconda3/envs/id30a2/bin/python3.9

import os
import json
import pprint
import pathlib
import tempfile
import time

from bes.workflow_lib import grid

dir1 = "/data/id23eh1/inhouse/opid231/20240130/RAW_DATA/Sample-8:2:08/run_01_MXPressF/run_01_04_datacollection"
dir2 = "/data/id23eh1/inhouse/opid231/20240130/RAW_DATA/Sample-8:2:07/run_01_MXPressF/run_01_04_datacollection"
dir3 = "/data/id23eh1/inhouse/opid231/20240130/RAW_DATA/Sample-8:2:06/run_01_MXPressF/run_01_04_datacollection"
dir4 = "/data/id23eh1/inhouse/opid231/20240130/RAW_DATA/Sample-8:2:05/run_01_MXPressF/run_01_04_datacollection"

dir5 = "/data/id30b/inhouse/opid30b/20240205/RAW_DATA/LYS/LYS-lyz3/run_01_MXPressE/run_01_05_datacollection"
dir6 = "/data/visitor/mx2482/id30b/20240201/RAW_DATA/LeuZip2/LeuZip2-addB6/run_01_MXPressE/run_01_05_datacollection"
dir7 = "/data/visitor/mx2482/id30b/20240201/RAW_DATA/LeuZip2/LeuZip2-AmPO06/run_01_MXPressE/run_01_08_datacollection"

dir8 = "/data/id23eh2/inhouse/opid232/20240208/RAW_DATA/Sample-8-1-08/run_01_MXPressF/run_01_02_mesh"
dir9 = "/data/visitor/mx2538/id23eh2/20240208/RAW_DATA/NCS1C/NCS1C-NCS1-CB1-7X8/run_01_XrayCentering/run_01_02_line"
raw_data_dir = pathlib.Path(dir9)

numberOfPositions = 100
workflow_working_dir = pathlib.Path(
    tempfile.mkdtemp(prefix="dozorm2_tests_", dir="/tmp_14_days/svensson")
)
dir_parts = list(raw_data_dir.parts)
index_raw_data = dir_parts.index("RAW_DATA")
dir_parts[index_raw_data] = "PROCESSED_DATA"
processed_data_dir = pathlib.Path(*dir_parts).parent
mesh_results_dir = list(processed_data_dir.glob("MeshResults"))[0]
print(mesh_results_dir)
for mesh_result_file in mesh_results_dir.glob("*.json"):
    print(mesh_result_file)
    mesh_result = json.loads(open(mesh_result_file).read())
    grid_info = mesh_result["grid_info"]
    if grid_info["steps_x"] != 1 and grid_info["steps_y"] != 1:
        raw_directory = mesh_result["raw_directory"]
        print(raw_directory)
        if raw_directory == str(raw_data_dir.parent):
            break

pprint.pprint(grid_info)
meshPositions = mesh_result["meshPositions"]

# Find DozorM2 results
workflow_dir = list(processed_data_dir.glob("Workflow*"))[0]
list_dozorm2_dir = list(workflow_dir.glob("DozorM2*"))
if len(list_dozorm2_dir) > 1:
    raise RuntimeError("More than one Dozorm2 directory!")
if len(list_dozorm2_dir) == 0:
    # if True:
    # No dozorm2! Try dozor...
    all_positions = grid.find_all_positions(
        grid_info,
        meshPositions,
        maxNoPositions=int(1),
        data_threshold=None,
        upper_threshold=0.90,
        radius=2,
        useDozor=True,
        correct_dozorm2=False,
    )

else:
    dozorm2_dir = pathlib.Path(list_dozorm2_dir[0])
    print(dozorm2_dir)
    out_data_dozorm2_path = dozorm2_dir / "outDataDozorM2.json"
    dozorm2_results = json.loads(open(out_data_dozorm2_path).read())
    dozormListPositions = dozorm2_results["dictCoord"]["scan1"]
    dozor_log_path = dozorm2_results["logPath"]
    dozor_log = open(dozor_log_path).read()

    # dozormListPositions =
    isHorizontalRotationAxis = False

    all_positions = grid.find_all_position_dozorm(
        listPositions=dozormListPositions,
        grid_info=grid_info,
        meshPositions=meshPositions,
        maxNoPositions=numberOfPositions,
        isHorizontalRotationAxis=isHorizontalRotationAxis,
        workflowWorkingDir=workflow_working_dir,
        correct_dozorm2=True,
    )
    print(dozor_log)

time.sleep(1)
pprint.pprint(all_positions)

plot_file = grid.create_plot_file(
    grid_info,
    meshPositions,
    all_positions,
    workflow_working_dir,
    data_threshold=None,
    useDozor=True,
)
print(plot_file)
os.system(f"display {workflow_working_dir / plot_file}")
