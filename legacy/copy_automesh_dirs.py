import time
import shutil
import pathlib

raw_data_dir = pathlib.Path("/data/visitor/mx2524/id30a3/20240220/RAW_DATA")
beamline = list(raw_data_dir.parts)[4]
scisoft_dir = pathlib.Path("/data/scisoft/pxsoft/data/automesh") / beamline
if not scisoft_dir.exists():
    scisoft_dir.mkdir(mode=0o755)

if beamline == "id30a3":
    processed_data_dir = pathlib.Path(
        str(raw_data_dir).replace("RAW_DATA", "PROCESSED_DATA")
    )
    list_dir = list(processed_data_dir.glob("*/*/*/snapshots_*"))
else:
    list_dir = list(raw_data_dir.glob("**/run_*_automesh"))

print(len(list_dir))

for raw_automesh_dir in list_dir:
    timestamp = raw_automesh_dir.stat().st_mtime
    new_automesh_dir_name = time.strftime(
        "automesh_%Y%m%d_%H%M%S", time.localtime(timestamp)
    )
    new_automesh_dir = scisoft_dir / new_automesh_dir_name
    print(new_automesh_dir)
    if new_automesh_dir.exists():
        print("New automesh dir already exists - skipped")
    else:
        shutil.copytree(raw_automesh_dir, new_automesh_dir)
