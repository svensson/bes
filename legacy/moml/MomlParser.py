#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "28/05/2019"

import os
import pprint
import logging
import pathlib

logger = logging.getLogger()


def createModelDict(jsonMoml):
    modelDict = {}
    #    pprint.pprint(jsonMoml)
    # Workflow or submodel?
    if "class" in jsonMoml:
        dictClass = jsonMoml["class"]
        modelDict["type"] = "submodel"
        modelDict["name"] = dictClass["@name"]
        modelDict["actor"] = getListActor(dictClass["entity"])
        modelDict["port"] = getListPort(dictClass["port"])
        modelDict["relation"] = getListRelation(
            dictClass["link"], modelDict["actor"], modelDict["port"]
        )
    else:
        requestStatusActor = None
        dictEntity = jsonMoml["entity"]
        modelDict["type"] = "workflow"
        modelDict["name"] = dictEntity["@name"]
        modelDict["actor"] = getListActor(dictEntity["entity"])
        # Check that we have a stop actor
        hasStopActor = False
        for actor in modelDict["actor"]:
            if actor["type"] == "StopActor":
                hasStopActor = True
            elif actor["type"] == "RequestStatus":
                requestStatusActor = actor
        if not hasStopActor:
            modelDict["actor"].append(
                {"name": "Stop actor", "type": "StopActor", "instance": "stopActor"}
            )
        modelDict["port"] = []
        modelDict["relation"] = getListRelation(
            dictEntity["link"], modelDict["actor"], []
        )
        if not hasStopActor and requestStatusActor is not None:
            modelDict["relation"].append(
                [[requestStatusActor["name"], "ouput"], ["Stop actor", "input"]]
            )
    return modelDict


def getListPort(listPort):
    if isinstance(listPort, dict):
        listPort = [listPort]
    listPortOut = []
    for dictPort in listPort:
        name = dictPort["@name"]
        for dictProperty in dictPort["property"]:
            if dictProperty["@name"] == "input":
                portType = "output"
            elif dictProperty["@name"] == "output":
                portType = "input"
        portDict = {"name": name, "type": portType}
        listPortOut.append(portDict)
    return listPortOut


def getUniqueInstanceName(name, listInstance):
    instance = name[0].lower() + name[1:]
    if instance in listInstance:
        index = 1
        while "{0}_{1}".format(instance, index) in listInstance:
            index += 1
        instance = "{0}_{1}".format(instance, index)
    listInstance.append(instance)
    return instance


def getListActor(listEntity):
    listActor = []
    listInstance = []
    for dictEntity in listEntity:
        name = dictEntity["@name"]
        actorDict = {}
        actorType = dictEntity["@class"]
        if actorType.endswith("PythonActor"):
            actorType = "PythonActor"
            listProperty = dictEntity["property"]
            for actorProperty in listProperty:
                if actorProperty["@name"] == "script_path":
                    scriptPath = pathlib.Path(actorProperty["@value"]).name
                    scriptName = os.path.splitext(scriptPath)[0]
                    actorDict["scriptName"] = scriptName
                    actorDict["instance"] = getUniqueInstanceName(
                        scriptName, listInstance
                    )
        elif actorType.endswith("ResultItemSplitter") or actorType.endswith(
            "ExpertRouter"
        ):
            actorType = "RouterActor"
            className = name.replace(" ", "_")
            actorDict["class"] = className
            listPort = dictEntity["port"]
            if isinstance(listPort, dict):
                listPort = [listPort]
            actorDict["listPort"] = ["other"]
            for port in listPort:
                actorDict["listPort"].append(port["@name"])
            listProperty = dictEntity["property"]
            for property in listProperty:
                if property["@name"] == "item name":
                    actorDict["itemName"] = property["@value"]
            #            pprint.pprint(dictEntity)
            actorDict["instance"] = getUniqueInstanceName(className, listInstance)
        elif actorType.endswith("StartActor"):
            actorType = "StartActor"
            actorDict["instance"] = getUniqueInstanceName("startActor", listInstance)
        elif actorType.endswith("Stop"):
            actorType = "StopActor"
            actorDict["instance"] = getUniqueInstanceName("stopActor", listInstance)
        elif actorType.endswith("RequestStatus"):
            actorType = "RequestStatus"
            listProperty = dictEntity["property"]
            for property in listProperty:
                if property["@name"] == "status":
                    if property["@value"] == "":
                        actorDict["status"] = "FINISHED"
                    else:
                        actorDict["status"] = property["@value"]
            actorDict["instance"] = getUniqueInstanceName("requestStatus", listInstance)
        elif actorType.endswith("ErrorHandlerBySeverity"):
            actorType = "ErrorHandler"
            actorDict["instance"] = getUniqueInstanceName("errorHandler", listInstance)
        elif actorType.endswith("Fork"):
            actorType = "ForkActor"
            actorDict["instance"] = getUniqueInstanceName("forkActor", listInstance)
        elif actorType.endswith("Join"):
            actorType = "JoinActor"
            actorDict["instance"] = getUniqueInstanceName("joinActor", listInstance)
        else:
            actorType = "Submodel"
            className = dictEntity["@class"]
            actorDict["class"] = className
            actorDict["instance"] = getUniqueInstanceName(className, listInstance)
        actorDict["name"] = name
        actorDict["type"] = actorType
        listActor.append(actorDict)
    return listActor


def getEntityData(port, listActor, listPort):
    entityName = None
    entityType = None
    portType = None
    if "." in port:
        entityName, portType = port.split(".")
    else:
        entityName = port
    for actor in listActor:
        if actor["name"] == entityName:
            entityType = actor["type"]
    if portType is None:
        for port in listPort:
            if port["name"] == entityName:
                entityType = "Port"
                portType = port["type"]
    return entityName, entityType, portType


def getListRelation(listLink, listActor, listPort):
    listRelation = []
    # pprint.pprint(listLink)
    for link in listLink:
        # pprint.pprint(link)
        startPort = link["@port"]
        startEntityName, startEntityType, startPortType = getEntityData(
            startPort, listActor, listPort
        )
        if (
            (startEntityType == "RouterActor" and startPortType != "input")
            or (startEntityType == "Submodel" and startPortType != "In")
            or (
                startEntityType not in ["RouterActor", "Submodel"]
                and startPortType == "output"
            )
            or (startEntityType == "ForkActor")
            or (startEntityType == "ErrorHandler")
        ):
            startRelation = link["@relation"]
            for link2 in listLink:
                relationName = link2["@relation"]
                if relationName == startRelation:
                    endPort = link2["@port"]
                    if startPort != endPort:
                        endEntityName, endEntityType, endPortType = getEntityData(
                            endPort, listActor, listPort
                        )
                        if (
                            (endEntityType == "RouterActor" and endPortType == "input")
                            or (endEntityType == "Submodel" and endPortType == "In")
                            or (
                                endEntityType not in ["RouterActor", "Submodel"]
                                and endPortType == "input"
                            )
                            or (startEntityType == "JoinActor")
                        ):
                            # print('*'*80)
                            # print([startEntityName, startEntityType, startPortType])
                            # print([endEntityName, endEntityType, endPortType])
                            listRelation.append(
                                [
                                    [startEntityName, startPortType],
                                    [endEntityName, endPortType],
                                ]
                            )
    return listRelation


def createImports(dictModel):
    listImport = []
    importUtils = ""
    importSubmodule = ""
    if dictModel["type"] == "submodel":
        importUtils += "from pypushflow.Submodel import Submodel\n"
    else:
        importUtils += "from pypushflow.Workflow import Workflow\n"
    for actor in dictModel["actor"]:
        actorType = actor["type"]
        if actorType == "Submodel":
            if importSubmodule == "":
                importSubmodule = "\n"
            if actor["class"] == dictModel["name"]:
                importSubmodule += (
                    "from submodels.{0} import {0} as {0}Submodel\n".format(
                        actor["class"]
                    )
                )
            else:
                importSubmodule += "from submodels.{0} import {0}\n".format(
                    actor["class"]
                )
        elif actorType not in listImport:
            importUtils += "from pypushflow.{0} import {0}\n".format(actor["type"])
            listImport.append(actor["type"])
    return importUtils + importSubmodule


def createInitActor(dictModel):
    # Create BES workflow python file
    initActor = ""
    for actor in dictModel["actor"]:
        if actor["type"] == "PythonActor":
            initActor += "        self.{0} = PythonActor(\n".format(actor["instance"])
            initActor += "            parent=self,\n"
            initActor += "            errorHandler=self,\n"
            initActor += '            script="bes.actors.{0}.py",\n'.format(
                actor["scriptName"]
            )
            initActor += '            name="{0}"\n'.format(actor["name"])
            initActor += "        )\n"
        elif actor["type"] == "Submodel":
            if actor["class"] == dictModel["name"]:
                initActor += "        self.{0} = {1}(\n".format(
                    actor["instance"], actor["class"] + "Submodel"
                )
            else:
                initActor += "        self.{0} = {1}(\n".format(
                    actor["instance"], actor["class"]
                )
            initActor += "            parent=self,\n"
            initActor += "            errorHandler=self,\n"
            initActor += '            name="{0}"\n'.format(actor["name"])
            initActor += "        )\n"
        elif actor["type"] == "RouterActor":
            initActor += "        self.{0} = RouterActor(\n".format(actor["instance"])
            initActor += "            parent=self,\n"
            initActor += '            name="{0}",\n'.format(actor["name"])
            initActor += '            itemName="{0}",\n'.format(actor["itemName"])
            initActor += "            listPort={0}\n".format(actor["listPort"])
            initActor += "        )\n"
        elif actor["type"] in [
            "StartActor",
            "RequestStatus",
            "ErrorHandler",
            "ForkActor",
            "JoinActor",
        ]:
            initActor += "        self.{0} = {1}(\n".format(
                actor["instance"], actor["type"]
            )
            initActor += "            parent=self,\n"
            if actor["type"] == "RequestStatus":
                initActor += '            name="{0}",\n'.format(actor["name"])
                initActor += '            status="{0}")\n'.format(
                    actor["status"].lower()
                )
            else:
                initActor += '            name="{0}")\n'.format(actor["name"])
        elif actor["type"] in ["StopActor"]:
            initActor += "        self.{0} = {1}(\n".format(
                actor["instance"], actor["type"]
            )
            initActor += "            parent=self,\n"
            initActor += "            errorHandler=self,\n"
            initActor += '            name="{0}")\n'.format(actor["name"])

    return initActor


def createInitRelations(dictModel):
    initRelations = ""
    listActor = dictModel["actor"]
    if "port" in dictModel:
        listPort = dictModel["port"]
    else:
        listPort = []
    for relation in dictModel["relation"]:
        pprint.pprint(relation)
        startPortName = relation[0][0]
        startPortType = relation[0][1]
        dictStartActor = None
        for dictActor in listActor:
            if dictActor["name"] == startPortName:
                dictStartActor = dictActor
                break
        # if dictStartActor is not None:
        #     print(dictStartActor['type'])
        #     if 'fork' in dictStartActor['type'].lower():
        #         print(1/0)
        dictStartPort = None
        if dictStartActor is None:
            for dictPort in listPort:
                if dictPort["name"] == startPortName:
                    dictStartPort = dictPort
            if dictStartPort is None:
                raise RuntimeError("Both dictStartActor and dictStartPort are None!")
        else:
            startInstance = dictStartActor["instance"]
        #
        endPortName = relation[1][0]
        endPortType = relation[1][1]
        dictEndActor = None
        for dictActor in listActor:
            if dictActor["name"] == endPortName:
                dictEndActor = dictActor
                break
        dictEndPort = None
        if dictEndActor is None:
            for dictPort in listPort:
                if dictPort["name"] == endPortName:
                    dictEndPort = dictPort
            if dictEndPort is None:
                raise RuntimeError("Both dictEndActor and dictEndPort are None!")
        else:
            endInstance = dictEndActor["instance"]
        if dictStartActor is None:
            startInstance = "getPort('{0}')".format(dictStartPort["name"])
        elif dictStartActor["type"] in ["Submodel"]:
            startInstance += ".getPort('{0}')".format(startPortType)

        if dictEndActor is None:
            endInstance = "getPort('{0}')".format(dictEndPort["name"])
        elif dictEndActor["type"] == "Submodel":
            endInstance += ".getPort('{0}')".format(endPortType)
        if dictStartActor is not None and dictStartActor["type"] == "RouterActor":
            endInstance += ", '{0}'".format(startPortType)
        initRelations += "        self.{0}.connect(self.{1})\n".format(
            startInstance, endInstance
        )
        if dictEndActor is not None and dictEndActor["type"] == "JoinActor":
            initRelations += "        self.{0}.increaseNumberOfThreads()\n".format(
                dictEndActor["instance"]
            )
        if dictStartActor is not None and dictStartActor["type"] == "ErrorHandler":
            initRelations += "        self.connectOnError(self.{0})\n".format(
                startInstance
            )
    return initRelations


def createClassAndInitMethod(dictModel):
    initMethod = ""
    initMethod += "\n"
    initMethod += "\n"
    if dictModel["type"] == "workflow":
        initMethod += "class {0}(Workflow):\n".format(dictModel["name"])
    else:
        initMethod += "class {0}(Submodel):\n".format(dictModel["name"])
    initMethod += "\n"
    if dictModel["type"] == "submodel":
        listPortNames = []
        for port in dictModel["port"]:
            listPortNames.append(port["name"])
        initMethod += "    def __init__(self, parent, errorHandler=None, name='{1}', portNames={0}):\n".format(
            listPortNames, dictModel["name"]
        )
        initMethod += "        Submodel.__init__(self, parent=parent, errorHandler=errorHandler, name=name, portNames=portNames)\n"
    else:
        initMethod += "    def __init__(self, name=None):\n"
        initMethod += "        Workflow.__init__(self, name='{0}')\n".format(
            dictModel["name"]
        )
    return initMethod


def createRunOrTriggerMethod(dictModel):
    runMethod = "\n"
    if dictModel["type"] == "workflow":
        runMethod += "    def run(self, inData, timeout=None):\n"
        runMethod += "        self.startActor.trigger(inData)\n"
        runMethod += "        self.stopActor.join(timeout=timeout)\n"
        runMethod += "        return self.stopActor.outData\n"
    # else:
    #     runMethod += '    def trigger(self, inData):\n'
    #     runMethod += '        self.getPort(\'In\').trigger(inData)\n'

    return runMethod


def createPythonScript(dictModel):
    script = ""
    script += createImports(dictModel)
    script += createClassAndInitMethod(dictModel)
    script += createInitActor(dictModel)
    script += createInitRelations(dictModel)
    script += createRunOrTriggerMethod(dictModel)
    return script


#    for actor in actorList:
#        name = actor['name']
#        pprint.pprint(actor)
#        if actor['type'] == 'PythonActor':
#            for relation in actor['output']:
#                for outputActor in actorList(self):
#                    if outputActor['name'] == relation:
