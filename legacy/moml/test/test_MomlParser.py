#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "28/05/2019"

import json
import pprint
import logging
import pathlib
import unittest
import xmltodict

from moml import MomlParser

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()


class TestMomlParser(unittest.TestCase):

    def setUp(self):
        self.flowsMomlDir = pathlib.Path(__file__).parents[2] / "moml" / "flows"
        self.submodelsMomlDir = pathlib.Path(__file__).parents[2] / "moml" / "submodels"
        self.flows = pathlib.Path(__file__).parents[2] / "bes" / "flows"
        self.submodels = pathlib.Path(__file__).parents[2] / "bes" / "submodels"

    def getFlowJsonMoml(self, momlFileName):
        momlPath = self.flowsMomlDir / momlFileName
        with open(str(momlPath)) as f:
            moml = f.read()
        jsonMoml = xmltodict.parse(moml)
        return json.loads(json.dumps(jsonMoml))

    def getSubmodelJsonMoml(self, momlFileName):
        momlPath = self.submodelsMomlDir / momlFileName
        with open(str(momlPath)) as f:
            moml = f.read()
        jsonMoml = xmltodict.parse(moml)
        return json.loads(json.dumps(jsonMoml))

    def tes_createActorList_Dehydration_SubModel(self):
        jsonMoml = self.getJsonMoml("Dehydration.moml")
        actorList = MomlParser.createModelDict(jsonMoml)
        pprint.pprint(actorList)

    def tes_createActorList_MXPRessA_Workflow(self):
        jsonMoml = self.getJsonMoml("MXPressA.moml")
        actorList = MomlParser.createModelDict(jsonMoml)
        pprint.pprint(actorList)

    def tes_getLinks_workflow(self):
        jsonMoml = self.getJsonMoml("EnhancedCharacterisation.moml")
        listRelation = MomlParser.getListRelation(jsonMoml["entity"]["link"])
        pprint.pprint(listRelation)

    def tes_getLinks_submodule(self):
        jsonMoml = self.getJsonMoml("Dehydration.moml")
        listRelation = MomlParser.getListRelation(jsonMoml["class"]["link"])
        pprint.pprint(listRelation)

    def tes_createImports(self):
        jsonMoml = self.getJsonMoml("MXPressA.moml")
        # jsonMoml = self.getJsonMoml('Dehydration.moml')
        dictModel = MomlParser.createModelDict(jsonMoml)
        importString = MomlParser.createImports(dictModel["actor"])
        print(importString)

    def tes_createInit(self):
        jsonMoml = self.getJsonMoml("TroubleShooting.moml")
        # jsonMoml = self.getJsonMoml('Dehydration.moml')
        dictModel = MomlParser.createModelDict(jsonMoml)
        initString = MomlParser.createInitActor(dictModel)
        print(initString)

    def tes_createRelations(self):
        jsonMoml = self.getSubmodelJsonMoml("MXPressDataCollection.moml")
        # pprint.pprint(jsonMoml)
        dictModel = MomlParser.createModelDict(jsonMoml)
        # pprint.pprint(dictModel)
        initString = MomlParser.createInitRelations(dictModel)
        print(initString)

    def test_getUniqueInstanceName(self):
        listInstance = []
        self.assertEqual(
            MomlParser.getUniqueInstanceName("Hello", listInstance), "hello"
        )
        self.assertEqual(
            MomlParser.getUniqueInstanceName("Hello", listInstance), "hello_1"
        )

    def test_createFlow(self):
        blackList = []
        whiteList = []
        print(self.flowsMomlDir)
        for momlFileName in self.flowsMomlDir.glob("*.moml"):
            jsonMoml = self.getFlowJsonMoml(momlFileName)
            dictModel = MomlParser.createModelDict(jsonMoml)
            name = dictModel["name"]
            print(name)
            if name in blackList:
                print("{} is blacklisted!".format(name))
            elif len(whiteList) == 0 or (len(whiteList) > 0 and name in whiteList):
                print("*" * 80)
                print(momlFileName)
                print(name)
                # pprint.pprint(dictModel)
                script = MomlParser.createPythonScript(dictModel)
                print(script)
                with open(str(self.flows / (name + ".py")), "w") as f:
                    f.write(script)

    def test_createSubmodels(self):
        #        name = 'CommonErrorReporter'
        blackList = ["ExecuteMXPressOrig"]
        whiteList = []
        # print(self.submodelsMomlDir)
        for momlFileName in self.submodelsMomlDir.glob("*.moml"):
            jsonMoml = self.getSubmodelJsonMoml(momlFileName)
            dictModel = MomlParser.createModelDict(jsonMoml)
            name = dictModel["name"]
            # print(name)
            if name in blackList:
                print("{} is blacklisted!".format(name))
            elif len(whiteList) == 0 or (len(whiteList) > 0 and name in whiteList):
                # print('*'*80)
                # print(name)
                # pprint.pprint(jsonMoml)
                pprint.pprint(dictModel)
                script = MomlParser.createPythonScript(dictModel)
                print(script)
                with open(str(self.submodels / (name + ".py")), "w") as f:
                    f.write(script)
