import os
import json
import shutil
import pprint
import unittest
import tempfile

from bes.workflow_lib import edna_kernel, edna_mxv1, grid
from XSDataCommon import XSDataString


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # Test reference image collection
        f = open(os.path.join(strTestDataDirectory, "reference_strategy.xml"), "r")
        self.referenceStrategy = f.read()
        f.close()
        # Test 'normal' data collection
        f = open(os.path.join(strTestDataDirectory, "burn_strategy.xml"), "r")
        self.mxv1StrategyResult = f.read()
        f.close()
        # Burn processing
        # f = open(
        #     os.path.join(strTestDataDirectory,
        #     "ControlCharacterisationv1_4_dataOutput.xml"),
        #     "r"
        # )
        # self.mxv1ResultCharacterisation = f.read()
        # f.close()
        # mxv2ResultCharacterisation
        f = open(
            os.path.join(strTestDataDirectory, "mxv2ResultCharacterisation.xml"), "r"
        )
        self.mxv2ResultCharacterisation = f.read()
        f.close()

    def tes_executeEdnaPlugin(self):
        strTmpDirectory = tempfile.mkdtemp()
        strDirectory = os.path.join(strTmpDirectory, "RAW_DATA")
        xsDataString = XSDataString("test")
        xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
            "EDPluginTestPluginFactory", xsDataString, strDirectory
        )
        self.assertEqual("test", xsDataResult.value, "Not correct result")
        shutil.rmtree(strTmpDirectory)

    def tes_storeImageQualityIndicators(self):
        beamline = "simulator"
        mxv1StrategyResult = self.mxv1StrategyResult
        run_number = 6
        expTypePrefix = "ref-"
        prefix = "ref-p1burnw1"
        directory = "/data/id14eh4/inhouse/opid144/20130220/RAW_DATA/WF/burn"
        suffix = "img"
        workflow_working_dir = None
        edna_mxv1.storeImageQualityIndicators(
            beamline,
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            workflow_working_dir,
        )

    def te_processBurnStrategy(self):
        beamline = "simulator"
        mxv1ResultCharacterisation = self.mxv1ResultCharacterisation
        run_number = 2
        expTypePrefix = "burn-"
        prefix = "test2"
        directory = "/data/id23eh1/inhouse/opid231/20130417/RAW_DATA/kappa/test2"
        suffix = "cbf"
        beamline = "simulator"
        workflow_working_dir = None
        edna_mxv1.processBurnStrategy(
            beamline,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            mxv1ResultCharacterisation,
            workflow_working_dir,
        )

    def test_mxv1ControlInterface(self):
        mxv1StrategyResult = edna_mxv1.create_ref_data_collect(
            exposureTime=0.01,
            transmission=100,
            osc_range=0.1,
            no_reference_images=1,
            angle_between_reference_images=90,
            phi=0.0,
        )
        run_number = "1"
        expTypePrefix = ""
        prefix = "ab_test-eiger"
        directory = "/opt/pxsoft/bes/vgit/linux-x86_64/id30b/edna2/testdata/images"
        suffix = "h5"
        beamline = "id30b"
        flux = 1e12
        beamSizeX = 0.1
        beamSizeY = 0.1
        strategyOption = None
        minExposureTime = None
        workflow_working_dir = None
        crystalSizeX = 0.5
        crystalSizeY = 0.3
        crystalSizeZ = 0.15
        anomalousData = True
        listImages = edna_mxv1.getListImagePath(
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            bFirstImageOnly=False,
        )
        edna_mxv1.h5ToCbf(beamline, listImages, workflow_working_dir)
        suffix = "cbf"
        newListImages = []
        # Change suffix to cbf...
        for imagePath in listImages:
            newListImages.append(imagePath.replace(".h5", ".cbf"))
        listImages = newListImages

        xs_data_result_interface, edna_log_path = edna_mxv1.mxv1ControlInterface(
            beamline,
            listImages,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            flux,
            beamSizeX,
            beamSizeY,
            strategyOption,
            minExposureTime,
            workflow_working_dir,
            crystalSizeX=crystalSizeX,
            crystalSizeY=crystalSizeY,
            crystalSizeZ=crystalSizeZ,
            anomalousData=anomalousData,
            bLogExecutiveSummary=True,
        )
        if xs_data_result_interface is not None:
            pprint.pprint(xs_data_result_interface.marshal())
        print(edna_log_path)
        if os.path.exists(edna_log_path):
            with open(edna_log_path) as fd:
                print(fd.read())

    def te_mxv2ControlInterface(self):
        beamline = "simulator"
        mxv1StrategyResult = self.referenceStrategy
        run_number = 2
        expTypePrefix = "ref-"
        prefix = "TestX1w1"
        directory = "/data/id14eh4/inhouse/opid144/20130403/RAW_DATA/Andrew/test"
        suffix = "img"
        flux = 1e12
        beamSizeX = 0.1
        beamSizeY = 0.1
        kappaStrategyOption = "CELL"
        phi = 0.0
        omega = 0.0
        kappa = 0.0
        workflow_working_dir = None
        edna_mxv1.mxv2ControlInterface(
            beamline,
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            omega,
            kappa,
            phi,
            flux,
            beamSizeX,
            beamSizeY,
            kappaStrategyOption,
            workflow_working_dir,
        )

    def tes_createSimpleHtmlPage(self):
        os.environ["EDNA_SITE"] = "ESRF_ID30A2"
        beamline = "simulator"
        workflow_working_dir = None
        pyarch_html_dir = None
        mxv1ResultCharacterisation = self.mxv1ResultCharacterisation
        strPath, strJsonPath = edna_mxv1.createSimpleHtmlPage(
            beamline, mxv1ResultCharacterisation, workflow_working_dir, pyarch_html_dir
        )
        self.assertTrue(os.path.exists(strPath))

    def tes_createSimpleHtmlPagev2_0(self):
        beamline = "simulator"
        mxv2ResultCharacterisation = self.mxv2ResultCharacterisation
        workflow_working_dir = None
        pyarch_html_dir = None
        strPath, strJsonPath = edna_mxv1.createSimpleHtmlPagev2_0(
            beamline, mxv2ResultCharacterisation, workflow_working_dir, pyarch_html_dir
        )
        self.assertTrue(os.path.exists(strPath))

    def tes_storeImageQualityIndicatorsFromMeshPositions_2D(self):
        beamline = "simulator"
        directory = "/data/scisoft/pxsoft/data/FastMesh1D/RAW_DATA"
        workflow_working_dir = None
        run_number = 2
        expTypePrefix = "4dscan-"
        prefix = "testw1"
        suffix = "cbf"
        phi = 0.0
        kappa = 0.0
        kappa_phi = 0.0
        sampx = 0.0
        sampy = 0.0
        phix = 0.0
        phiy = 0.0
        phiz = 0.0
        grid_info = {
            "x1": 0.0,
            "y1": 0.0,
            "dx_mm": 1.0,
            "dy_mm": 1.0,
            "steps_x": 100,
            "steps_y": 1,
            "angle": 0.0,
        }
        meshPositions = grid.determineMeshPositions(
            beamline,
            phi,
            kappa,
            kappa_phi,
            sampx,
            sampy,
            phix,
            phiy,
            phiz,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        _ = edna_mxv1.storeImageQualityIndicatorsFromMeshPositions(
            beamline, directory, meshPositions, workflow_working_dir
        )

    def test_storeImageQualityIndicatorsFromMeshPositions_1D(self):
        workflow_working_dir = None
        beamline = "simulator"
        directory = "/data/scisoft/pxsoft/data/Mesh/opid231/20130507/RAW_DATA/mesh003"
        expTypePrefix = "mesh-"
        prefix = "test"
        suffix = "cbf"
        run_number = 2
        motorPositions = {
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "sampx": 0.0,
            "sampy": 0.0,
            "phix": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "focus": 0.0,
            "chi": 0.0,
        }
        osc_range = 0.1
        grid_info = json.loads(
            """{
                "angle": 0.0,
                "dx_mm": 0.100,
                "dy_mm": 0.110,
                "steps_x": 11,
                "steps_y": 1,
                "x1": -0.1036,
                "y1": -0.0621}
            """
        )
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositions,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )

        edPlugin = edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsStart(
            beamline, directory, meshPositions, workflow_working_dir, grid_info
        )
        xsDataResult = (
            edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsSynchronize(
                edPlugin, meshPositions
            )[0]
        )
        print(xsDataResult)

    def tes_storeImageQualityIndicatorsFromMeshPositions(self):
        workflow_working_dir = None
        beamline = "simulator"
        directory = "/data/scisoft/pxsoft/data/Mesh/opid231/20130507/RAW_DATA/mesh003"
        expTypePrefix = "mesh-"
        run_number = 1
        prefix = "test"
        suffix = "cbf"
        sampx = 0.0
        sampy = 0.0
        phix = 0.0
        phiy = 0.0
        phiz = 0.0
        phi = 0.0
        kappa = 0.0
        kappa_phi = 0.0
        grid_info = """{
            "angle": 0.0,
            "dx_mm": 0.100,
            "dy_mm": 0.110,
            "steps_x": 4,
            "steps_y": 4,
            "x1": -0.1036,
            "y1": -0.0621}
        """
        meshPositions = grid.determineMeshPositions(
            beamline,
            phi,
            kappa,
            kappa_phi,
            sampx,
            sampy,
            phix,
            phiy,
            phiz,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        edPlugin = edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsStart(
            beamline, directory, meshPositions, workflow_working_dir
        )
        _ = edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsSynchronize(
            edPlugin, meshPositions
        )

    def test_create_thumbnails_for_pyarch(self):
        list_image = [
            "/data/visitor/mx2503/id23eh1/20230412/RAW_DATA/Sample-2-2-02/ref-test_LC_1_0001.h5"
        ]
        workflow_working_dir = None
        edna_mxv1.create_thumbnails_for_pyarch(list_image, workflow_working_dir)

    def tes_create_thumbnails_for_pyarch_asynchronous(self):
        beamline = "simulator"
        list_image_creation = [
            {
                "isCreated": True,
                "fileLocation": "/data/id23eh2/inhouse/opid232/20130516/RAW_DATA/delete_me",
                "imageId": 43645776,
                "fileName": "delete_me_1_0010.mccd",
            }
        ]
        edna_mxv1.create_thumbnails_for_pyarch_asynchronous(
            beamline, list_image_creation
        )

    def tes_runDozorForHalfDoseTime(self):
        beamline = "simulator"
        dataDirectory = "/data/scisoft/pxsoft/data/DOZOR/id23eh1/20160229"
        prefix = "x"
        runNumber = 1
        wedgeNumber = 10
        noCycles = 10
        listImages = []
        workflow_working_dir = None
        for index in range(wedgeNumber * noCycles):
            imagePath = os.path.join(
                dataDirectory,
                "{0}_{1}_{2:04d}.cbf".format(prefix, runNumber, index + 1),
            )
            listImages.append(imagePath)
        halfDoseTime = edna_mxv1.runDozorForHalfDoseTime(
            beamline, listImages, wedgeNumber, workflow_working_dir
        )
        print(halfDoseTime)

    def tes_h5ToCbf(self):
        beamline = "simulator"
        dir = "/data/id30a3/inhouse/opid30a3/20160301/RAW_DATA"
        dir = os.path.join(dir, "test1/EnhancedCharacterisation_03")
        imagePath1 = os.path.join(dir, "ref-x_1_0001.h5")
        imagePath2 = os.path.join(dir, "ref-x_1_0002.h5")
        tmpDirectory = tempfile.mkdtemp(prefix="test_h5toCbf_")
        print(tmpDirectory)
        workflow_working_dir = tmpDirectory
        forcedOutputDirectory = tmpDirectory
        listImages = [imagePath1, imagePath2]
        edna_mxv1.h5ToCbf(
            beamline, listImages, workflow_working_dir, forcedOutputDirectory
        )

    def tes_waitForFile(self):
        beamline = "simulator"
        tmpDirectory = tempfile.mkdtemp(prefix="test_waitForFile_")
        print(tmpDirectory)
        dir = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA"
        dir = os.path.join(dir, "id30a1/20140901/RAW_DATA")
        imagePath1 = os.path.join(dir, "line-phiz_centred_6_0001.cbf")
        imagePath2 = "/path/that/does/not/exist"
        self.assertFalse(edna_mxv1.waitForFile(beamline, imagePath1, tmpDirectory))
        self.assertTrue(
            edna_mxv1.waitForFile(beamline, imagePath2, tmpDirectory, timeOut=5)
        )

    def tes_runFbest(self):
        tmpDirectory = tempfile.mkdtemp(prefix="test_runFbest_")
        flux = 1e12
        resolution = 2.0
        dictResult = edna_mxv1.runFbest(
            flux=flux,
            resolution=resolution,
            # rotationRange=0.0,
            workflow_working_dir=tmpDirectory,
        )
        pprint.pprint(dictResult)

    def test_getListFineSlicedReferenceImagePath(self):
        beamline = "id30a1"
        strategy_result = edna_mxv1.create_n_images_data_collect(
            exposureTime=1.0,
            transmission=100,
            number_of_images=1,
            osc_range=0.1,
            phi=123.0,
        )
        directory = (
            "/data/id30a1/inhouse/opid30a1/20220408/RAW_DATA/Sample-8-1-01/MXPressA_01"
        )
        run_number = 1
        exp_type_prefix = "ref-fineslice-"
        prefix = "opid30a1"
        suffix = "cbf"
        tmpdir = tempfile.mkdtemp()
        edna_mxv1.getListFineSlicedReferenceImagePath(
            beamline=beamline,
            mxv1StrategyResult=strategy_result,
            directory=directory,
            run_number=run_number,
            expTypePrefix=exp_type_prefix,
            prefix=prefix,
            suffix=suffix,
            dest_dir=tmpdir,
        )
