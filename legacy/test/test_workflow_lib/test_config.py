#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

import pprint
import unittest

from bes import config


class TestConfig(unittest.TestCase):
    def test_getListOfValues(self):
        beamline = "id30a2"
        section_name = "Test"
        list_value = config.get_value(beamline, section_name, "error")
        pprint.pprint(list_value)
        list_value = config.get_value(beamline, section_name, "success")
        pprint.pprint(list_value)
        list_value = config.get_value(beamline, section_name, "empty")
        pprint.pprint(list_value)
        list_value = config.get_value(
            beamline, section_name, "absent", default_value=[]
        )
        pprint.pprint(list_value)
        value = config.get_value(beamline, section_name, "float")
        pprint.pprint(value)
        value = config.get_value(beamline, section_name, "int")
        pprint.pprint(value)
        list_value = config.get_value(
            beamline, section_name, "floatlist", default_value=[]
        )
        pprint.pprint(list_value)
        list_value = config.get_value(
            beamline, section_name, "intlist", default_value=[]
        )
        pprint.pprint(list_value)
        list_value = config.get_value(
            beamline, section_name, "intlist2", default_value=[]
        )
        pprint.pprint(list_value)

    def test_default(self):
        value = config.get_value(
            "id30a2", "NoSection", "noKey", default_value="testValue"
        )
        self.assertEqual(value, "testValue")

    def test_default_2(self):
        value = config.get_value(
            "id30a2",
            "Mesh",
            "noKey",
            default_value="DozorM2 (macro-molecules crystal detection)",
        )
        self.assertEqual(value, "DozorM2 (macro-molecules crystal detection)")

    def test_boolean(self):
        allowZeroDegreeMeshScan = (
            config.get_value(
                "id30a2", "Goniometer", "allowZeroDegreeMeshScan", default_value=None
            )
            == "true"
        )
        print(allowZeroDegreeMeshScan)

    def test_id30b(self):
        list_value = config.get_value("id30b", "Kappa", "PhiRot")
        pprint.pprint(list_value)
        list_value = config.get_value("id30b", "Kappa", "KappaRot")
        pprint.pprint(list_value)
        list_value = config.get_value("id30b", "Kappa", "OmegaRot")
        pprint.pprint(list_value)

    def test_id23eh2(self):
        maxDelta = config.get_value("id23eh2", "Goniometer", "kappa_phi_max_delta")
        print(maxDelta)
        maxDelta = config.get_value(
            "id23eh2", "Goniometer", "phi_max_delta", default_value=None
        )
        print(maxDelta)
        aperture_sizes = config.get_value("id23eh2", "Beam", "apertureSizes")
        print(aperture_sizes)

    def test_id30b_bis(self):
        fixedTargetApertureSize = config.get_value(
            "id30b", "AutoMesh", "fixedTargetApertureSize"
        )
        print(fixedTargetApertureSize)
