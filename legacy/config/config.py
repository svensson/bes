#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

import json
import pathlib
import configparser
from typing import Optional, Union, List

DEFAULT_VALUE_NOT_DEFINED = "default_value_not_defined"


_ValueType = Union[str, int, float, None]


def getValue(
    beamline, section_name, key_name, default_value=DEFAULT_VALUE_NOT_DEFINED
) -> Optional[_ValueType]:
    return_value = None
    config = configparser.ConfigParser()
    config_file = beamline + ".ini"
    path_file = pathlib.Path(__file__)
    config_dir = path_file.parent
    config_path = config_dir / config_file.lower()
    if not config_path.exists():
        raise RuntimeError("Config doesn't exist for beamline {0}!".format(beamline))
    config.read(config_path.as_posix())
    sections = config.sections()
    if section_name not in sections:
        if default_value == DEFAULT_VALUE_NOT_DEFINED:
            raise RuntimeError(
                "Config section {0} doesn't exist for beamline {1}!".format(
                    section_name, beamline
                )
            )
        else:
            return_value = default_value
    else:
        if key_name not in config[section_name]:
            if default_value == DEFAULT_VALUE_NOT_DEFINED:
                raise RuntimeError(
                    "Config key {0} for section {1} doesn't exist for beamline {2}!".format(
                        key_name, section_name, beamline
                    )
                )
            else:
                return_value = default_value

        else:
            value = config[section_name][key_name]
            return_value = _deserialize_config_value(value)
    return return_value


def getListOfValues(
    beamline,
    section_name,
    key_name,
    default_value=DEFAULT_VALUE_NOT_DEFINED,
) -> List[_ValueType]:
    config_value = getValue(
        beamline, section_name, key_name, default_value=default_value
    )
    if config_value is None:
        return []
    list_of_values = []
    config_value = str(config_value)
    if config_value.startswith("["):
        list_of_values = json.loads(config_value)
    else:
        for value in config_value.split(","):
            if value.strip() != "":
                list_of_values.append(_deserialize_config_value(value.strip()))
    return list_of_values


def _deserialize_config_value(
    string_value: Optional[str],
) -> Optional[Union[str, int, float]]:
    if string_value is None:
        return string_value
    if string_value.isdigit():  # Not true for negative integers like "-1"
        return int(string_value)
    try:
        return float(string_value)
    except ValueError:
        return string_value


if __name__ == "__main__":
    from bes import config as yaml_config

    beamlines = [
        "bm07",
        "id23eh1",
        "id23eh2",
        "id30a1",
        "id30a2",
        "id30a3",
        "id30b",
        "simulator",
    ]
    for beamline in beamlines:
        config_path = pathlib.Path(__file__).parent / f"{beamline}.ini".lower()

        config = configparser.ConfigParser()
        config.optionxform = str
        config.read(config_path.as_posix())

        for section in config.sections():
            csection = config[section]
            for key in csection:
                cvalue = csection[key]
                if isinstance(cvalue, list) or "," in cvalue or "[" in cvalue:
                    ini_value = getListOfValues(beamline, section, key)
                else:
                    ini_value = getValue(beamline, section, key)
                if ini_value == "true":
                    ini_value = True
                elif ini_value == "false":
                    ini_value = False
                elif isinstance(ini_value, float):
                    if ini_value < 0 and ini_value == int(ini_value):
                        # bug in _deserialize_config_value
                        ini_value = int(ini_value)

                yaml_value = yaml_config.get_value(beamline, section, key)

                err_msg = f"Mismatch in {beamline}, {section}, {key}: INI Value = {ini_value}({type(ini_value)}), YAML Value = {yaml_value}({type(yaml_value)})"
                assert ini_value == yaml_value, err_msg
                assert type(ini_value) is type(yaml_value), err_msg
