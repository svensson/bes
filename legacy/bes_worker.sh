#!/bin/bash
#
# Start a Celery worker pool (processes by default) that serves the ewoks application.
# Not all configurations can be provided through the CLI (e.g. `result_serializer`)
#

# celery -A ewoksjob.apps.ewoks worker --broker redis://localhost:10003/3 --result-backend redis://localhost:10003/4

BES_BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BES_DIR=$(dirname $BES_BIN_DIR)
BEAMLINE_DIR=$(dirname $BES_DIR)
BEAMLINE=$(basename $BEAMLINE_DIR)
CONDA_SH=$BEAMLINE_DIR/miniconda3/etc/profile.d/conda.sh

# Activate conda
. $CONDA_SH
conda activate $BEAMLINE

# Start worker
celery --config bes.celeryconfig -A ewoksjob.apps.ewoks worker -E --pool=process