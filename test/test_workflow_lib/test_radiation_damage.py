#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "20/04/2022"

import pprint
import pathlib
import tempfile

from bes.workflow_lib import radiation_damage


def test_run_flux2dose():
    flux = 1e12
    wavelength = 1
    aperture = 0.030
    dose_rate = radiation_damage.run_flux2dose(flux, wavelength, aperture)
    print(dose_rate)
    assert dose_rate == 0.5566


def test_run_rdfit():
    dose_rate = 0.0556 * 1e6
    visible_resolution = 2.0
    sensitivity = 1
    rd_plan = radiation_damage.run_rdfit_plan(
        dose_rate, visible_resolution, sensitivity
    )
    pprint.pprint(rd_plan)
    assert rd_plan["images"] == 10
    assert rd_plan["wedges"] == 10
    assert rd_plan["resolution"] == 2.0
    assert rd_plan["exposure"] == 1.611
    assert rd_plan["dose_per_wedge"] == 895735.0


def test_run_rdfit2():
    working_directory = tempfile.mkdtemp()
    test_data_dir = pathlib.Path(__file__).parent / "data"
    list_xds_ascii_hkl = []
    for index in range(1, 6):
        data_path = test_data_dir / "XDS_ASCII_{0}.HKL".format(index)
        list_xds_ascii_hkl.append(data_path)
    dose_per_wedge = 895735.0
    results_rd = radiation_damage.run_rdfit(
        dose_per_wedge, list_xds_ascii_hkl, working_directory
    )
    print(results_rd)
