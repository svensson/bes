#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "22/10/2020"

import pprint
import pathlib
import unittest


from src.bes.workflow_lib import edna_mxv1


class TestEdnaMXv1(unittest.TestCase):
    def test_createEdna2CharacterisationInput(self):
        inputFile = pathlib.Path(__file__).parent / "data" / "EDNAInput_2507151.xml"
        characterisationInput = edna_mxv1.createEdna2CharacterisationInput(inputFile)
        print()
        pprint.pprint(characterisationInput)

    def test_createEdna2CharacterisationInput2(self):
        inputFile = pathlib.Path(__file__).parent / "data" / "EDNAInput_2507549.xml"
        characterisationInput = edna_mxv1.createEdna2CharacterisationInput(inputFile)
        print()
        pprint.pprint(characterisationInput)

    def test_createEdna2CharacterisationInput3(self):
        inputFile = pathlib.Path(__file__).parent / "data" / "EDNAInput_2518306.xml"
        characterisationInput = edna_mxv1.createEdna2CharacterisationInput(inputFile)
        print()
        pprint.pprint(characterisationInput)

    def test_createEdna2CharacterisationOutput(self):
        outputFile = pathlib.Path(__file__).parent / "data" / "EDNAOutput_2507151.xml"
        characterisationOutput = edna_mxv1.createEdna2CharacterisationOutput(outputFile)
        print()
        pprint.pprint(characterisationOutput)
