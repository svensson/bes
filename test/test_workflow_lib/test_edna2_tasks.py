#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "22/10/2020"

import pprint
import unittest


from edna2.tasks.Characterisation import Characterisation


class TestEdna2Tasks(unittest.TestCase):
    def test_characterisation(self):
        indexingInput = {
            "dataCollectionId": 2518306,
            "diffractionPlan": {
                "aimedCompleteness": 0.99,
                "aimedIOverSigmaAtHighestResolution": 1.0,
                "aimedResolution": 0.0,
                "anomalousData": "false",
                "complexity": "none",
                "detectorDistanceMax": 930.0,
                "detectorDistanceMin": 100.0,
                "forcedSpaceGroup": "",
                "maxExposureTimePerDataCollection": 30.0,
                "strategyOption": "-low never",
            },
            "experimentalCondition": {
                "beam": {
                    "flux": 419241200000.0,
                    "minExposureTimePerImage": 0.02,
                    "size": {"x": 0.05, "y": 0.05},
                    "transmission": 2.506716,
                    "wavelength": 0.9676967,
                },
                "goniostat": {
                    "maxOscillationSpeed": 360.0,
                    "minOscillationWidth": 0.05,
                },
            },
            "imagePath": [
                "/data/visitor/mx2273/id30a3/20201110/RAW_DATA/aHB/aHB-TLIG_Morph_D1/ref-aHB-TLIG_Morph_D1_1_0001.h5",
                "/data/visitor/mx2273/id30a3/20201110/RAW_DATA/aHB/aHB-TLIG_Morph_D1/ref-aHB-TLIG_Morph_D1_1_0002.h5",
            ],
            "sample": {
                "radiationDamageModelBeta": 1e-06,
                "radiationDamageModelGamma": 6e-08,
                "shape": 1.0,
                "size": {"x": 0.05, "y": 0.05, "z": 0.05},
                "susceptibility": 1.0,
            },
            "token": "abbbb58952",
        }
        characterisation = Characterisation(inData=indexingInput)
        characterisation.execute()
        pprint.pprint(characterisation.outData)


#     def test_controlIndexing(self):
#         indexingInput = {
#   "dataCollectionId": 2507313,
#   "diffractionPlan": {
#     "aimedCompleteness": 0.99,
#     "aimedIOverSigmaAtHighestResolution": 3,
#     "aimedMultiplicity": 4,
#     "aimedResolution": 3,
#     "anomalousData": "false",
#     "complexity": "none",
#     "forcedSpaceGroup": None,
#     "maxExposureTimePerDataCollection": 30,
#     "strategyOption": "-low never"
#   },
#   "experimentalCondition": {
#     "beam": {
#       "flux": 3000000000000,
#       "minExposureTimePerImage": 0.037,
#       "size": {
#         "x": 0.05,
#         "y": 0.05
#       }
#     },
#     "goniostat": {
#       "maxOscillationSpeed": 360,
#       "minOscillationWidth": 0.05
#     }
#   },
#   "sample": {
#     "radiationDamageModelBeta": 0.000001,
#     "radiationDamageModelGamma": 6e-8,
#     "shape": 1,
#     "size": {
#       "x": 0.1,
#       "y": 0.1,
#       "z": 0.1
#     },
#     "susceptibility": 1
#   },
#   "token": None,
#   "imagePath": [
#     "/data/id30a2/inhouse/opid30a2/20201022/RAW_DATA/t1/ref-t1_4_0001.cbf",
#     "/data/id30a2/inhouse/opid30a2/20201022/RAW_DATA/t1/ref-t1_4_0002.cbf"
#   ]
# }
#         controlIndexing = ControlIndexing(inData=indexingInput)
#         controlIndexing.execute()
#         pprint.pprint(controlIndexing.outData)


#    def test_controlIndexing2(self):
#        indexingInput = {'dataCollectionId': 2518306,
# 'diffractionPlan': {'aimedCompleteness': 0.99,
#                     'aimedIOverSigmaAtHighestResolution': 1.0,
#                     'aimedResolution': 0.0,
#                     'anomalousData': 'false',
#                     'complexity': 'none',
#                     'detectorDistanceMax': 930.0,
#                     'detectorDistanceMin': 100.0,
#                     'forcedSpaceGroup': None,
#                     'maxExposureTimePerDataCollection': 30.0,
#                     'strategyOption': '-low never'},
# 'experimentalCondition': {'beam': {'flux': 419241200000.0,
#                                    'minExposureTimePerImage': 0.02,
#                                    'size': {'x': 0.05, 'y': 0.05},
#                                    'transmission': 2.506716,
#                                    'wavelength': 0.9676967},
#                           'goniostat': {'maxOscillationSpeed': 360.0,
#                                         'minOscillationWidth': 0.05}},
# 'imagePath': ['/data/visitor/mx2273/id30a3/20201110/RAW_DATA/aHB/aHB-TLIG_Morph_D1/ref-aHB-TLIG_Morph_D1_1_0001.h5',
#               '/data/visitor/mx2273/id30a3/20201110/RAW_DATA/aHB/aHB-TLIG_Morph_D1/ref-aHB-TLIG_Morph_D1_1_0002.h5'],
# 'sample': {'radiationDamageModelBeta': 1e-06,
#            'radiationDamageModelGamma': 6e-08,
#            'shape': 1.0,
#            'size': {'x': 0.05, 'y': 0.05, 'z': 0.05},
#            'susceptibility': 1.0},
# 'token': 'abbbb58952'}
#        controlIndexing = ControlIndexing(inData=indexingInput)
#        controlIndexing.execute()
#        pprint.pprint(controlIndexing.outData)
#
