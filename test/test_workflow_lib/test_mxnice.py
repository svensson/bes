#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "11/10/2019"

import os
import shutil
import pprint
import pathlib
import tempfile
import unittest


from src.bes.workflow_lib import mxnice


class TestMxnice(unittest.TestCase):
    def test_parseSLURMStat(self):
        testData = (
            pathlib.Path(__file__).parent / "data" / "slurm_scontrol_show_job.txt"
        )
        with open(str(testData)) as f:
            slurmStat = f.read()
        dictSLURMStat = mxnice.parseSLURMStat(slurmStat)
        self.assertTrue(dictSLURMStat["JobState"] == "COMPLETED")

    def test_getSLURMStat(self):
        loadDict = mxnice.getSLURMStat(10)
        pprint.pprint(loadDict)

    def test_submitJobToSLURM(self):
        commandLine = str(pathlib.Path(__file__).parent / "data" / "hostname.sh")
        if not os.path.exists("/tmp_14_days/slurm"):
            os.makedirs("/tmp_14_days/slurm")
        workingDirectory = tempfile.mkdtemp(dir="/tmp_14_days/slurm")
        print(workingDirectory)
        slurmScriptPath, slurmJobId, stdout, stderr = mxnice.submitJobToSLURM(
            commandLine,
            workingDirectory,
            nodes=1,
            core=4,
            time="2:00:00",
            host=None,
            queue=None,
            name="test_submitJobToSLURM",
        )
        print(slurmJobId)
        print(stdout)
        print(stderr)
        shutil.rmtree(workingDirectory)

    def test_areSlurmJobsPending(self):
        arePendingJobs = mxnice.areJobsPending("nice")
        print(arePendingJobs)
