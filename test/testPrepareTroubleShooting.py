import tempfile

import logging

logger = logging.getLogger()


def run(directory, beamline=None, **kwargs):
    logger.info("In run")
    return_dict = {}

    if directory is None:
        beamline = "simulator"
        return_dict = {
            "directory": tempfile.mkdtemp(prefix="WorkflowTroubleShooting_"),
            "beamline": beamline,
            "prefix": "troubleShooting",
            "run_number": 1,
        }
    elif beamline is None:
        beamline = "unknown"

    return_dict["workflow_title"] = "Trouble shooting on {0}".format(beamline)
    return_dict["workflow_type"] = "TroubleShooting"
    return_dict["beamline"] = beamline.lower()

    return return_dict
