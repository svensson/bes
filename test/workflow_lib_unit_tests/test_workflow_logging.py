import os
import shutil
import unittest
import tempfile

from bes.workflow_lib import workflow_logging


class TestMethods(unittest.TestCase):
    def test_getLogger(self):
        beamline = "id30a2"
        strLogfile = tempfile.mktemp(".txt", "workflow_log_")
        strDebugLogfile = tempfile.mktemp(".txt", "workflow_debug_log_")
        strPyarchDir = tempfile.mkdtemp(prefix="pyarchDirTest_")
        strPyarchLogFile = os.path.join(strPyarchDir, os.path.basename(strLogfile))
        workflowParameters = {
            "workingDir": None,
            "logFile": strLogfile,
            "debugLogFile": strDebugLogfile,
            "index": None,
            "pyarchLogFile": strPyarchLogFile,
            "type": None,
        }
        logger = workflow_logging.getLogger(beamline, workflowParameters)
        self.assertTrue(logger is not None, "Logger is None")
        logger.debug("Unit test logger.debug: Debugging...")
        logger.info("Unit test logger.info: While this is just chatty")
        logger.warning("Unit test logger.warn: Warning!")
        logger.error("Unit test logger.error: We have a problem")
        self.assertTrue(
            os.path.exists(strLogfile), "Log file %s does not exist!" % strLogfile
        )
        self.assertTrue(
            os.path.exists(strDebugLogfile),
            "Debug log file %s does not exist!" % strDebugLogfile,
        )
        self.assertTrue(
            os.path.exists(strPyarchLogFile),
            "Pyarch log file %s does not exist!" % strPyarchLogFile,
        )
        #        os.system("cat %s" % strLogfile)
        #        os.system("cat %s" % strDebugLogfile)
        os.remove(strLogfile)
        os.remove(strDebugLogfile)
        shutil.rmtree(strPyarchDir)

    def test_mxCuBELogger(self):
        logger = workflow_logging.getLogger(beamline="simulator")
        logger.debug("Unit test logger.debug: Debugging...")
        logger.info("Unit test logger.info: While this is just chatty")
        logger.warning("Unit test logger.warn: Warning!")
        logger.error("Unit test logger.error: We have a problem")
