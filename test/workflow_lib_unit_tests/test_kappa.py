import unittest
from bes.workflow_lib import kappa_reorientation


class TestMethods(unittest.TestCase):
    def test_isIncompatibleKappaStrategy(self):
        self.assertTrue(
            kappa_reorientation.isIncompatibleKappaStrategy("id30b", "Anomalous", "P3")
        )
        self.assertFalse(
            kappa_reorientation.isIncompatibleKappaStrategy("id30b", "Anomalous", "P1")
        )
        self.assertFalse(
            kappa_reorientation.isIncompatibleKappaStrategy("id30b", "Cell", "P3")
        )

    def test_alignVectorWithOmegaAxis_1(self):
        dictKappa = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [0.241, -0.112, 0.414]
        )
        self.assertAlmostEqual(88.1, dictKappa["kappa"], 1)
        self.assertAlmostEqual(209.668, dictKappa["phi"], 1)
        self.assertEqual(dictKappa["status"], "ok")

    #
    #     def test_alignVectorWithOmegaAxis_2(self):
    #         # /data/id14eh4/inhouse/opid144/20130904/PROCESSED_DATA/KappaWF/Trypsin/Kappa_02/Workflow_20130904-185256
    #         dictKappa = kappa.alignVectorWithOmegaAxis("simulator_id14eh4", [0.2934246, 0.4687303 , 0.8331828 ])
    #         # /data/id14eh4/inhouse/opid144/20130606/PROCESSED_DATA/FAE/WorkFlow/Kappa_02/Workflow_20130606-162529
    #         self.assertAlmostEqual(93.666, dictKappa["kappa"], 1)
    #         self.assertAlmostEqual(212.025, dictKappa["phi"], 1)
    #         self.assertEqual(dictKappa["status"], "ok")

    def test_alignVectorWithOmegaAxis_3(self):
        # /data/id23eh1/inhouse/opid231/20130617/PROCESSED_DATA/Kappa_03/Workflow_20130617-121213
        dictKappa = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [0.6711779, -0.0057669, 0.7412740]
        )
        # /data/id14eh4/inhouse/opid144/20130606/PROCESSED_DATA/FAE/WorkFlow/Kappa_02/Workflow_20130606-162529
        self.assertAlmostEqual(125.0, dictKappa["kappa"], 1)
        self.assertAlmostEqual(166.4, dictKappa["phi"], 1)
        self.assertEqual(dictKappa["status"], "ok")

    def test_alignVectorWithOmegaAxis_4(self):
        dictKappa = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [0.211, 0.108, -1.09]
        )
        self.assertAlmostEqual(30.3, dictKappa["kappa"], 1)
        self.assertAlmostEqual(5.3, dictKappa["phi"], 1)
        self.assertEqual(dictKappa["status"], "ok")

    def test_alignVectorWithOmegaAxis_tooLargeAngle(self):
        dictKappa = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [1.241, -1.112, 0.414]
        )
        self.assertEqual(dictKappa["status"], "tooLargeAngle")

    def test_alignVectorWithOmegaAxis_tooShortVector(self):
        dictKappa = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [0.001, 0.001, 0.020]
        )
        self.assertEqual(dictKappa["status"], "tooShortVector")

    def test_getNewSamplePosition(self):
        #         print kappa_reorientation.getNewSamplePosition("id23eh1", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        #         print kappa_reorientation.getNewSamplePosition("id23eh1", 0.0, 0.0, 0.0, 0.0, 0.0, 90.0, 0.0)
        #         print kappa_reorientation.getNewSamplePosition("id23eh1", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 180.0)
        #         print kappa_reorientation.getNewSamplePosition("id23eh1", 90.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        #         print kappa_reorientation.getNewSamplePosition("id23eh1", 0.0, 180.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        #         print kappa_reorientation.getNewSamplePosition("id23eh1", 11.0, 220.0, 1.0, 2.0, 3.0, 33.0, 44.0)
        #         print kappa_reorientation.getNewSamplePosition("id29", 0.0, 0.0, 0.238, 0.1240, -0.4510, 90, 107.36)
        print(
            kappa_reorientation.getNewSamplePosition(
                "id30b", 0.0, 0.0, 0.348, -0.055, 22.575, 62.71, 122.8
            )
        )
