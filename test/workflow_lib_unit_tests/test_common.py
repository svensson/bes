import unittest
from bes.workflow_lib import common


class TestMethods(unittest.TestCase):
    def test_getDefunctPythonIds(self):
        list_ps_aux = []
        list_ps_aux.append(
            "opid23   16168     1  0 10:01 ?        00:00:01 /usr/bin/python2.6 -u -mscisoftp"
        )
        list_ps_aux.append(
            "opid23   20482     1  0 11:06 ?        00:00:01 /usr/bin/python2.6 -u -mscisoftp"
        )
        list_ps_aux.append(
            "opid23   20484  4345  0 11:06 ?        00:00:01 /usr/bin/python2.6 -u -mscisoftp"
        )
        list_ps_aux.append(
            "opid29   20485     1  0 11:06 ?        00:00:01 /usr/bin/python2.6 -u -mscisoftp"
        )
        list_ids = common.getDefunctPythonIds(list_ps_aux, "opid23")
        self.assertEqual([16168, 20482], list_ids)

    def test_interpretStacktrace(self):
        print("-" * 80)
        # /data/visitor/mx1745/id23eh1/20151013/RAW_DATA/Lancaster/Sabine/CB467Ap1/HelicalCharacterisation_01
        stack_trace_text = """<class 'socket.error'> [Errno 111] Connection refused
  File "/data/id23eh1/inhouse/opid231/workflow/mx_submodels/src/collect_centerSamplePos2.py", line 19, in run"""
        interpreted_text = common.interpretStacktrace(stack_trace_text)
        print(interpreted_text)
        print("-" * 80)
        # /data/visitor/mx1734/id30a1/20151012/RAW_DATA/UspA/UspA-ins_091/MXPressA_01
        stack_trace_text = """<class 'xmlrpclib.Fault'> <Fault 1: "<class 'core.MotorNotReadyException'>:motor z state is not ready">
  File "/data/id30a1/inhouse/opid30a1/workflow/mx_submodels/src/centerRotationAxis_moveToNewAxisPosition.py", line 30, in run"""
        interpreted_text = common.interpretStacktrace(stack_trace_text)
        print(interpreted_text)
        print("-" * 80)
        # /data/id30b/inhouse/opid30b/20150924/RAW_DATA/deleteme/Advanced/MeshScan_01
        stack_trace_text = """<type 'exceptions.SyntaxError'> invalid syntax (<string>, line 1052)
  File "/data/id30b/inhouse/opid30b/workflow/mx_submodels/src/execute_mesh_startDataCollection.py", line 61, in run"""
        interpreted_text = common.interpretStacktrace(stack_trace_text)
        print(interpreted_text)
        print("-" * 80)

    def test_getBESWorkflowStatus(self):
        bes_parameters = {
            "host": "localhost",
            "port": 5000,
            "request_id": "62cc342348cc1920ad8e9fb3",
        }
        status = common.getBESWorkflowStatus(bes_parameters)
        self.assertEqual(status, "FINISHED")
