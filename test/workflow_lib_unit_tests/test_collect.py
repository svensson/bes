import os
import json
import shutil
import pprint
import tempfile
import unittest

from bes.workflow_lib import grid
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        self.testDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # Test 'normal' data collection
        f = open(os.path.join(self.testDataDirectory, "burn_strategy.xml"), "r")
        self.mxv1StrategyResult = f.read()
        f.close()
        # 4dscan queue list
        f = open(os.path.join(self.testDataDirectory, "mesh4dQueueList.txt"), "r")
        strData = f.read()
        self.meshQueueList = json.loads(strData)
        f.close()

    def test_createMeshQueueList(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        process_directory = "/tmp/PROCESSED_DATA"
        exposureTime = 1.0
        transmission = 100.0
        osc_range = 1.0
        motorPositons = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        run_number = 1
        expTypePrefix = "mesh-"
        prefix = "test"
        suffix = "cbf"
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 3, "steps_y": 3, "angle": 0.0}"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        listQueue = collect.createMeshQueueList(
            beamline,
            directory,
            process_directory,
            run_number,
            expTypePrefix,
            prefix,
            meshPositions,
            motorPositons,
            exposureTime,
            osc_range,
            transmission,
        )
        self.assertEqual(9, len(listQueue))

    #        pprint.pprint(listQueue)

    def test_createHelicalQueueList(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        process_directory = "/tmp/PROCESSED_DATA"
        exposureTime = 1.0
        transmission = 100.0
        run_number = 6
        osc_range = 0.1
        motorPositions = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        run_number = 1
        expTypePrefix = "mesh-"
        prefix = "test"
        suffix = "cbf"
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 3, "steps_y": 3, "angle": 0.0}"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositions,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        listQueue = collect.createHelicalQueueList(
            beamline,
            directory,
            process_directory,
            run_number,
            expTypePrefix,
            prefix,
            meshPositions,
            motorPositions,
            exposureTime,
            osc_range,
            transmission,
            grid_info,
        )
        print(json.dumps(listQueue, indent=4))
        self.assertEqual(3, len(listQueue))

    def test_getDataCollectionInfo(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        transmission = 100
        run_number = 1
        dictReponse = collect.getDataCollectionInfo(
            beamline, directory, transmission, run_number, self.mxv1StrategyResult
        )
        self.assertEqual(7, dictReponse["totalNoImages"])
        self.assertEqual(2, dictReponse["noSubWedges"])
        self.assertEqual(1, dictReponse["noCollectionPlans"])
        self.assertEqual(1, dictReponse["noBurnSubWedges"])
        self.assertAlmostEqual(4.376, dictReponse["totalExposureTime"])

    def test_createQueueModelDataCollections(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        process_directory = "/tmp/PROCESSED_DATA"
        prefix = "test"
        expTypePrefix = "ref-"
        run_number = 6
        motorPositions = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        resolution = 2.4
        mxv1StrategyResult = self.mxv1StrategyResult
        mxv1ResultCharacterisation = None
        import queue_model_objects_v1 as QUEUE_MODEL_OBJECTS

        collect.QUEUE_MODEL_OBJECTS = QUEUE_MODEL_OBJECTS
        listQueueDataCollections = collect._createQueueModelDataCollections(
            beamline,
            directory,
            process_directory,
            prefix,
            expTypePrefix,
            run_number,
            resolution,
            mxv1ResultCharacterisation,
            mxv1StrategyResult,
            motorPositions,
        )
        # TODO: Better test of collect._createQueueModelDataCollections!
        self.assertEqual(3, len(listQueueDataCollections))

    def test_checkMotorSpeed(self):
        beamline = "id30a2"
        gridExposureTime = 1.0
        transmission = 100.0
        meshQueueList = self.meshQueueList
        dictResult = collect.checkMotorSpeed(
            beamline, meshQueueList, gridExposureTime, transmission
        )
        self.assertAlmostEqual(66.667, dictResult["gridExposureTime"], 1)
        self.assertAlmostEqual(1.5, dictResult["transmission"], 1)

    def test_checkBeamlineDirectory(self):
        collect.checkBeamlineDirectory("simulator", "/tmp")
        #
        b_is_exception = True
        try:
            collect.checkBeamlineDirectory("id29", "/tmp")
            b_is_exception = False
        except RuntimeError:
            pass
        self.assertTrue(b_is_exception)
        #
        b_is_exception = True
        try:
            collect.checkBeamlineDirectory("id29", "/data/id29/")
            b_is_exception = False
        except RuntimeError:
            pass
        self.assertTrue(b_is_exception)
        #
        b_is_exception = True
        try:
            collect.checkBeamlineDirectory(
                "id29", "/data/id23eh1/inhouse/opid231/20140220/RAW_DATA"
            )
            b_is_exception = False
        except RuntimeError:
            pass
        self.assertTrue(b_is_exception)
        #
        b_is_exception = True
        try:
            collect.checkBeamlineDirectory(
                "id29", "/data/id29/inhouse/opid231/20140220"
            )
            b_is_exception = False
        except RuntimeError:
            pass
        self.assertTrue(b_is_exception)
        #
        b_is_exception = True
        try:
            collect.checkBeamlineDirectory(
                "id29", "/data/id29/inhouse/opid291/20140220/RAW_DATA"
            )
            b_is_exception = False
        except RuntimeError:
            pass
        self.assertFalse(b_is_exception)

    def test_id30a1PhiyCorrection(self):
        self.assertAlmostEqual(0.0, collect.id30a1PhiyCorrection(0.0))
        self.assertAlmostEqual(-0.0252, collect.id30a1PhiyCorrection(-0.5))
        self.assertAlmostEqual(-0.0182, collect.id30a1PhiyCorrection(0.2))
        self.assertAlmostEqual(0.00155, collect.id30a1PhiyCorrection(0.025))
        self.assertAlmostEqual(-0.00485, collect.id30a1PhiyCorrection(-0.025))

    def test_detectorDistanceToResolution(self):
        beamline = "simulator"
        wavelength = 0.976
        detectorDistance = 601.45
        self.assertAlmostEqual(
            2.896,
            collect.detectorDistanceToResolution(
                beamline, detectorDistance, wavelength
            ),
            3,
        )
        beamline = "id23eh1"
        wavelength = 1.0
        detectorDistance = 101.0
        self.assertAlmostEqual(
            0.937,
            collect.detectorDistanceToResolution(
                beamline, detectorDistance, wavelength
            ),
            3,
        )
        beamline = "id23eh2"
        wavelength = 0.873
        detectorDistance = 150.0
        self.assertAlmostEqual(
            1.270,
            collect.detectorDistanceToResolution(
                beamline, detectorDistance, wavelength
            ),
            3,
        )
        beamline = "id29"
        wavelength = 1.0
        detectorDistance = 155
        self.assertAlmostEqual(
            1.105,
            collect.detectorDistanceToResolution(
                beamline, detectorDistance, wavelength
            ),
            3,
        )
        beamline = "id30a1"
        wavelength = 0.965
        detectorDistance = 101
        self.assertAlmostEqual(
            1.111,
            collect.detectorDistanceToResolution(
                beamline, detectorDistance, wavelength
            ),
            3,
        )
        beamline = "id30a3"
        wavelength = 0.967
        detectorDistance = 101
        self.assertAlmostEqual(
            1.503,
            collect.detectorDistanceToResolution(
                beamline, detectorDistance, wavelength
            ),
            3,
        )
        beamline = "id30b"
        wavelength = 1.0
        detectorDistance = 157.0
        self.assertAlmostEqual(
            1.112,
            collect.detectorDistanceToResolution(
                beamline, detectorDistance, wavelength
            ),
            3,
        )
        beamline = "id30b"
        wavelength = 0.976
        detectorDistance = 820.0
        self.assertAlmostEqual(
            3.872,
            collect.detectorDistanceToResolution(
                beamline, detectorDistance, wavelength
            ),
            3,
        )

    def test_resolutionToDetectorDistance(self):
        beamline = "simulator"
        wavelength = 0.976
        resolution = 2.951
        self.assertAlmostEqual(
            601.45,
            collect.resolutionToDetectorDistance(beamline, resolution, wavelength),
            1,
        )

    def test_readDiagFile(self):
        diagFilePath = "/tmp_14_days/nurizzo/Diagnostiques/LastDiagnostics.dat"
        arrayValues = collect.readDiagFile(diagFilePath)
        pprint.pprint(arrayValues)
        # self.assertTrue(arrayValues.shape == (2, 6005, 7))

    def test_readDiagFileNew(self):
        diagFilePath = os.path.join(self.testDataDirectory, "id30a1_20160323.dat")
        arrayValues = collect.readDiagFile(diagFilePath)
        self.assertTrue(arrayValues.shape == (1, 13406, 7))

    def test_readDiagFileIOError(self):
        diagFilePath = "/file/that/does/not/exist.dat"
        try:
            _ = collect.readDiagFile(diagFilePath, timeOut=1)
        except OSError:
            print("OSError exception caught.")

    def test_mxsup3096(self):
        with open(
            os.path.join(self.testDataDirectory, "meshPositionsBefore_76ZDRh.json")
        ) as f:
            meshPositions = json.loads(f.read())
        xsDataResult = edna_mxv1.parseXSDataResultControlImageQualityIndicators(
            os.path.join(
                self.testDataDirectory,
                "ControlImageQualityIndicatorsv1_4_dataOutput.xml",
            )
        )
        (
            resultMeshPositions,
            inputDozor,
        ) = edna_mxv1.convertXSDataImageQualityIndicatorsToDict(
            meshPositions, xsDataResult
        )
        pprint.pprint(resultMeshPositions)
        useFastMesh = False
        grid_info = {
            "beam_height": 0.014409,
            "x1": -0.25217399999999995,
            "angle": 0,
            "dy_mm": 0.043227,
            "y1": 0.008005,
            "dx_mm": 0.256932,
            "steps_y": 4,
            "steps_x": 19,
            "id": "Grid - 9",
            "beam_width": 0.014273999999999998,
        }

        if len(meshPositions) > 0:
            if useFastMesh or grid_info["steps_x"] == 1 or grid_info["steps_y"] == 1:
                firstImage = meshPositions[0]["imageName"]
            else:
                lastRow = [
                    item
                    for item in meshPositions
                    if item["indexZ"] == (grid_info["steps_y"] - 1)
                ]
                firstImage = lastRow[0]["imageName"]
            print("firstImage: {0}".format(firstImage))

    def test_createSnapShotDir(self):
        beamline = "simulator"
        directory = tempfile.mkdtemp(prefix="createSnapShotDir_")
        snapShotDir = collect.createSnapShotDir(beamline, directory)
        self.assertTrue(os.path.exists(snapShotDir))
        shutil.rmtree(snapShotDir)

    def test_getAperture(self):
        beamline = "simulator"
        print(collect.getApertureSize(beamline))

    def test_setAperture(self):
        beamline = "simulator"
        aperture_size = 7
        print(collect.setApertureSize(beamline, aperture_size))
