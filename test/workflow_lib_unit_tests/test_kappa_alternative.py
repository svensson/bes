import unittest
from bes.workflow_lib import kappa_reorientation


class TestMethods(unittest.TestCase):
    def test_isIncompatibleKappaStrategy(self):
        self.assertTrue(
            kappa_reorientation.isIncompatibleKappaStrategy("id30b", "Anomalous", "P3")
        )
        self.assertFalse(
            kappa_reorientation.isIncompatibleKappaStrategy("id30b", "Anomalous", "P1")
        )
        self.assertFalse(
            kappa_reorientation.isIncompatibleKappaStrategy("id30b", "Cell", "P3")
        )

    def test_alignVectorWithOmegaAxis_1(self):
        dictKappa1 = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [0.241, -0.112, 0.414]
        )
        dictKappa2 = kappa_reorientation.alternativeAlignVectorWithOmegaAxis(
            "id30b", [0.241, -0.112, 0.414]
        )
        print(dictKappa1, dictKappa2)

    #        self.assertEqual(dictKappa["status"], "ok")

    #     def test_alignVectorWithOmegaAxis_2(self):
    #         # /data/id14eh4/inhouse/opid144/20130904/PROCESSED_DATA/KappaWF/Trypsin/Kappa_02/Workflow_20130904-185256
    #         dictKappa1 = kappa_reorientation.alignVectorWithOmegaAxis("simulator_id14eh4", [0.2934246, 0.4687303 , 0.8331828 ])
    #         dictKappa2 = kappa_reorientation.alternativeAlignVectorWithOmegaAxis("simulator_id14eh4",  [0.2934246, 0.4687303 , 0.8331828 ])
    #         print dictKappa1, dictKappa2
    # /data/id14eh4/inhouse/opid144/20130606/PROCESSED_DATA/FAE/WorkFlow/Kappa_02/Workflow_20130606-162529
    #        self.assertAlmostEqual(93.666, dictKappa["kappa"], 1)
    #        self.assertAlmostEqual(212.025, dictKappa["phi"], 1)
    #        self.assertEqual(dictKappa["status"], "ok")

    def test_alignVectorWithOmegaAxis_3(self):
        # /data/id23eh1/inhouse/opid231/20130617/PROCESSED_DATA/Kappa_03/Workflow_20130617-121213
        dictKappa1 = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [0.6711779, -0.0057669, 0.7412740]
        )
        dictKappa2 = kappa_reorientation.alternativeAlignVectorWithOmegaAxis(
            "id30b", [0.6711779, -0.0057669, 0.7412740]
        )
        print(dictKappa1, dictKappa2)
        # /data/id14eh4/inhouse/opid144/20130606/PROCESSED_DATA/FAE/WorkFlow/Kappa_02/Workflow_20130606-162529

    #        self.assertAlmostEqual(123.974, dictKappa["kappa"], 1)
    #        self.assertAlmostEqual(255.359, dictKappa["phi"], 1)
    #        self.assertEqual(dictKappa["status"], "ok")

    #     def test_alignVectorWithOmegaAxis_4(self):
    #         dictKappa = kappa.alignVectorWithOmegaAxis("simulator_id29", [0.211, 0.108 , -1.09])
    #         dictKappa2 = kappa.alternativeAlignVectorWithOmegaAxis("simulator",   [0.211, 0.108 , -1.09])
    #         print dictKappa, dictKappa2
    #         self.assertAlmostEqual(26.05, dictKappa["kappa"], 1)
    #         self.assertAlmostEqual(190.35, dictKappa["phi"], 1)
    #         self.assertEqual(dictKappa["status"], "ok")

    def test_alignVectorWithOmegaAxis_tooLargeAngle(self):
        dictKappa = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [1.241, -1.112, 0.414]
        )
        dictKappa2 = kappa_reorientation.alternativeAlignVectorWithOmegaAxis(
            "id30b", [1.241, -1.112, 0.414]
        )
        print(dictKappa, dictKappa2)
        self.assertEqual(dictKappa["status"], "tooLargeAngle")

    def test_alignVectorWithOmegaAxis_tooShortVector(self):
        dictKappa = kappa_reorientation.alignVectorWithOmegaAxis(
            "id30b", [0.001, 0.001, 0.020]
        )
        self.assertEqual(dictKappa["status"], "tooShortVector")
