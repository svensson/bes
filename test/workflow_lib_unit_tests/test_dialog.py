import unittest
import pprint

from bes.workflow_lib import dialog


class TestMethods(unittest.TestCase):
    def test_float_1(self):
        listDialog = [
            {
                "variableName": "targetRH",
                "label": "Relative Humidity Start Level",
                "type": "float",
                "defaultValue": 99.0,
                "unit": "%",
                "upperBound": 100.0,
            }
        ]
        referenceXML = """<?xml version="1.0" encoding="UTF-8"?>
<java version="1.7.0_40" class="java.beans.XMLDecoder">
 <object class="org.dawb.passerelle.actors.ui.config.FieldContainer">
  <void property="customLabel">
   <string></string>
  </void>
  <void property="fields">
   <void method="add">
    <object class="org.dawb.passerelle.actors.ui.config.FieldBean">
     <void property="defaultValue">
      <string>99.0</string>
     </void>
     <void property="uiClass">
      <string>org.dawnsci.common.richbeans.components.scalebox.StandardBox</string>
     </void>
     <void property="uiLabel">
      <string>Relative Humidity Start Level</string>
     </void>
     <void property="unit">
      <string>%</string>
     </void>
     <void property="upperBound">
      <double>100.0</double>
     </void>
     <void property="variableName">
      <string>targetRH</string>
     </void>
    </object>
   </void>
 </object>
</java>"""
        obtainedXML = dialog.createDawnBeanDecoderXML(listDialog)
        print([referenceXML])
        print([obtainedXML])
        self.assertEqual(referenceXML, obtainedXML)

    def test_float_2(self):
        listDialog = [
            {
                "variableName": "targetRH",
                "label": "Relative Humidity Start Level",
                "type": "float",
                "defaultValue": 99.0,
                "unit": "%",
                "upperBound": 100.0,
            },
            {
                "variableName": "endRH",
                "label": "Relative Humidity End Level",
                "type": "float",
                "defaultValue": 50,
                "unit": "%",
                "lowerBound": 50.0,
                "upperBound": 100.0,
            },
        ]
        referenceXML = """<?xml version="1.0" encoding="UTF-8"?>
<java version="1.7.0_40" class="java.beans.XMLDecoder">
 <object class="org.dawb.passerelle.actors.ui.config.FieldContainer">
  <void property="customLabel">
   <string></string>
  </void>
  <void property="fields">
   <void method="add">
    <object class="org.dawb.passerelle.actors.ui.config.FieldBean">
     <void property="defaultValue">
      <string>99.0</string>
     </void>
     <void property="uiClass">
      <string>org.dawnsci.common.richbeans.components.scalebox.StandardBox</string>
     </void>
     <void property="uiLabel">
      <string>Relative Humidity Start Level</string>
     </void>
     <void property="unit">
      <string>%</string>
     </void>
     <void property="upperBound">
      <double>100.0</double>
     </void>
     <void property="variableName">
      <string>targetRH</string>
     </void>
    </object>
   </void>
   <void method="add">
    <object class="org.dawb.passerelle.actors.ui.config.FieldBean">
     <void property="defaultValue">
      <string>50</string>
     </void>
     <void property="lowerBound">
      <double>50.0</double>
     </void>
     <void property="uiClass">
      <string>org.dawnsci.common.richbeans.components.scalebox.StandardBox</string>
     </void>
     <void property="uiLabel">
      <string>Relative Humidity End Level</string>
     </void>
     <void property="unit">
      <string>%</string>
     </void>
     <void property="upperBound">
      <double>100.0</double>
     </void>
     <void property="variableName">
      <string>endRH</string>
     </void>
    </object>
   </void>
 </object>
</java>"""
        obtainedXML = dialog.createDawnBeanDecoderXML(listDialog)
        print([referenceXML])
        print([obtainedXML])
        self.assertEqual(referenceXML, obtainedXML)

    def test_int(self):
        listDialog = [
            {
                "defaultValue": 1,
                "variableName": "no_reference_images",
                "label": "No reference images",
                "type": "int",
            }
        ]
        referenceXML = """<?xml version="1.0" encoding="UTF-8"?>
<java version="1.7.0_40" class="java.beans.XMLDecoder">
 <object class="org.dawb.passerelle.actors.ui.config.FieldContainer">
  <void property="customLabel">
   <string></string>
  </void>
  <void property="fields">
   <void method="add">
    <object class="org.dawb.passerelle.actors.ui.config.FieldBean">
     <void property="defaultValue">
      <string>1</string>
     </void>
     <void property="uiClass">
      <string>org.dawnsci.common.richbeans.components.wrappers.SpinnerWrapper</string>
     </void>
     <void property="uiLabel">
      <string>No reference images</string>
     </void>
     <void property="variableName">
      <string>no_reference_images</string>
     </void>
    </object>
   </void>
 </object>
</java>"""
        obtainedXML = dialog.createDawnBeanDecoderXML(listDialog)
        print([referenceXML])
        print([obtainedXML])
        self.assertEqual(referenceXML, obtainedXML)

    def test_string(self):
        listDialog = [
            {
                "defaultValue": "test",
                "variableName": "text_input",
                "label": "Text input",
                "type": "text",
            }
        ]
        referenceXML = """<?xml version="1.0" encoding="UTF-8"?>
<java version="1.7.0_40" class="java.beans.XMLDecoder">
 <object class="org.dawb.passerelle.actors.ui.config.FieldContainer">
  <void property="customLabel">
   <string></string>
  </void>
  <void property="fields">
   <void method="add">
    <object class="org.dawb.passerelle.actors.ui.config.FieldBean">
     <void property="defaultValue">
      <string>test</string>
     </void>
     <void property="uiClass">
      <string>org.dawnsci.common.richbeans.components.wrappers.StandardBox</string>
     </void>
     <void property="uiLabel">
      <string>Text input</string>
     </void>
     <void property="variableName">
      <string>text_input</string>
     </void>
    </object>
   </void>
 </object>
</java>"""
        obtainedXML = dialog.createDawnBeanDecoderXML(listDialog)
        print([referenceXML])
        print([obtainedXML])
        self.assertEqual(referenceXML, obtainedXML)

    def test_combo(self):
        listDialog = [
            {
                "variableName": "automatic_mode",
                "label": "Automatic mode",
                "type": "combo",
                "textChoices": ["true", "false"],
            }
        ]
        referenceXML = """<?xml version="1.0" encoding="UTF-8"?>
<java version="1.7.0_40" class="java.beans.XMLDecoder">
 <object class="org.dawb.passerelle.actors.ui.config.FieldContainer">
  <void property="customLabel">
   <string></string>
  </void>
  <void property="fields">
   <void method="add">
    <object class="org.dawb.passerelle.actors.ui.config.FieldBean">
     <void property="textChoices">
      <void method="add">
       <object class="org.dawb.passerelle.actors.ui.config.StringValueBean">
        <void property="textValue">
         <string>true</string>
        </void>
       </object>
      </void>
      <void method="add">
       <object class="org.dawb.passerelle.actors.ui.config.StringValueBean">
        <void property="textValue">
         <string>false</string>
        </void>
       </object>
      </void>
     </void>
     <void property="uiClass">
      <string>org.dawnsci.common.richbeans.components.wrappers.ComboWrapper</string>
     </void>
     <void property="uiLabel">
      <string>Automatic mode</string>
     </void>
     <void property="variableName">
      <string>automatic_mode</string>
     </void>
    </object>
   </void>
 </object>
</java>"""
        obtainedXML = dialog.createDawnBeanDecoderXML(listDialog)
        print([referenceXML])
        print([obtainedXML])
        self.assertEqual(referenceXML, obtainedXML)

    def test_automaticMode(self):
        characterisationExposureTime = 1.0
        osc_range = 2.0
        transmission = 29.6
        resolution = 2.1
        listDialog = [
            {
                "variableName": "no_reference_images",
                "label": "Number of reference images",
                "type": "int",
                "defaultValue": 2,
                "unit": "",
                "lowerBound": 1,
                "upperBound": 4,
            },
            {
                "variableName": "angle_between_reference_images",
                "label": "Angle between reference images",
                "type": "combo",
                "defaultValue": "90",
                "textChoices": ["30", "45", "60", "90"],
            },
            {
                "variableName": "characterisationExposureTime",
                "label": "Characterisation exposure time",
                "type": "float",
                "value": characterisationExposureTime,
                "unit": "%",
                "lowerBound": 0.0,
                "upperBound": 100.0,
            },
            {
                "variableName": "osc_range",
                "label": "Total oscillation range",
                "type": "float",
                "value": osc_range,
                "unit": "%",
                "lowerBound": 0.1,
                "upperBound": 10.0,
            },
            {
                "variableName": "transmission",
                "label": "Transmission",
                "type": "float",
                "value": transmission,
                "unit": "%",
                "lowerBound": 0,
                "upperBound": 100.0,
            },
            {
                "variableName": "resolution",
                "label": "Resolution",
                "type": "float",
                "defaultValue": resolution,
                "unit": "A",
                "lowerBound": 0.5,
                "upperBound": 7.0,
            },
            {
                "variableName": "do_data_collect",
                "label": "Do data collection?",
                "type": "combo",
                "textChoices": ["true", "false"],
                "value": "false",
            },
            {
                "variableName": "useFastMesh",
                "label": "Use fast mesh",
                "type": "boolean",
                "value": True,
                "defaultValue": True,
            },
        ]
        dictValues = dialog.openDialog(
            beamline="id30a2",
            listDialog=listDialog,
            collection_software="MXCuBE - 3.0",
            automaticMode=True,
        )
        dictReference = {
            "characterisationExposureTime": 1.0,
            "transmission": 29.600000000000001,
            "angle_between_reference_images": "90",
            "do_data_collect": "false",
            "osc_range": 2.0,
            "no_reference_images": 2,
            "resolution": 2.1000000000000001,
            "useFastMesh": True,
        }
        for key in dictReference:
            if isinstance(dictReference[key], float):
                self.assertAlmostEqual(dictReference[key], dictValues[key], 3)
            else:
                self.assertEqual(dictReference[key], dictValues[key])

        print(dictValues)

    def test_automaticMode_outOfBound1(self):
        listDialog = [
            {
                "variableName": "no_reference_images",
                "label": "Number of reference images",
                "type": "int",
                "defaultValue": 2,
                "value": 0,
                "unit": "",
                "lowerBound": 1,
                "upperBound": 4,
            }
        ]
        try:
            _ = dialog.openDialog("simulator", listDialog, automaticMode=True)
        except Exception:
            pass
        else:
            self.assertTrue(False, "No exception caught!")

    def test_automaticMode_outOfBound2(self):
        listDialog = [
            {
                "variableName": "no_reference_images",
                "label": "Number of reference images",
                "type": "int",
                "defaultValue": 2,
                "value": 5,
                "unit": "",
                "lowerBound": 1,
                "upperBound": 4,
            }
        ]
        try:
            _ = dialog.openDialog("simulator", listDialog, automaticMode=True)
        except Exception:
            pass
        else:
            self.assertTrue(False, "No exception caught!")

    def test_floatAsInt(self):
        listDialog = [
            {
                "variableName": "no_reference_images",
                "label": "Number of reference images",
                "type": "int",
                "value": 3.2,
                "unit": "",
                "lowerBound": 1,
                "upperBound": 4,
            }
        ]
        dictValues = dialog.openDialog("simulator", listDialog, automaticMode=True)
        self.assertTrue(isinstance(dictValues["no_reference_images"], int))

    def test_stringFloatAsInt(self):
        listDialog = [
            {
                "variableName": "no_reference_images",
                "label": "Number of reference images",
                "type": "int",
                "value": "3.2",
                "unit": "",
                "lowerBound": 1,
                "upperBound": 4,
            }
        ]
        dictValues = dialog.openDialog("simulator", listDialog, automaticMode=True)
        self.assertTrue(isinstance(dictValues["no_reference_images"], int))

    def test_liform1(self):
        flux = float(9.55e11)
        listDialog = [
            {
                "variableName": "no_reference_images",
                "label": "Number of reference images",
                "type": "int",
                "value": "3",
                "unit": "",
                "lowerBound": 1,
                "upperBound": 4,
            },
            {
                "variableName": "characterisationExposureTime",
                "label": "Characterisation exposure time",
                "type": "float",
                "value": 0.1,
                "unit": "s",
                "lowerBound": 0.0,
                "upperBound": 100.0,
            },
            {
                "variableName": "angle_between_reference_images",
                "label": "Angle between reference images",
                "type": "combo",
                "defaultValue": "90",
                "textChoices": ["30", "45", "60", "90"],
            },
            {
                "variableName": "flux",
                "label": "Flux",
                "type": "float",
                "value": flux,
                "unit": "photos/s",
            },
        ]
        dictLiform = dialog.createLiformDict(listDialog, dialogName="Test dialog")
        dictDialog = {}
        dictDialog["reviewData"] = dictLiform
        dictDialog["inputMap"] = listDialog
        pprint.pprint(dictDialog)
        reviewData = dialog.createDawnBeanDecoderXML(listDialog)
        dictDialog = {}
        dictDialog["reviewData"] = reviewData
        dictDialog["inputMap"] = listDialog
        pprint.pprint(dictDialog)
