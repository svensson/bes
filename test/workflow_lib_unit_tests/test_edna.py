import os
import shutil
import unittest
import tempfile

from bes.workflow_lib import edna_kernel
from bes.workflow_lib import edna_mxv1


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # Test two sweep data collection
        f = open(
            os.path.join(strTestDataDirectory, "mxv1ResultStrategy_twoSweep.xml"), "r"
        )
        self.mxv1TwoSweepStrategyResult = f.read()
        f.close()
        # Test 'normal' data collection
        f = open(os.path.join(strTestDataDirectory, "burn_strategy.xml"), "r")
        self.mxv1BurnStrategyResult = f.read()
        f.close()
        # mxv1ResultCharacterisation
        f = open(
            os.path.join(strTestDataDirectory, "burn_mxv1ResultCharacterisation.xml"),
            "r",
        )
        self.burn_mxv1ResultCharacterisation = f.read()
        f.close()
        # mxv1ResultCharacterisation_eEDNA
        f = open(
            os.path.join(strTestDataDirectory, "mxv1ResultCharacterisation_eEDNA.xml"),
            "r",
        )
        self.mxv1ResultCharacterisation_eEDNA = f.read()
        f.close()
        # mxv2ResultCharacterisation
        f = open(
            os.path.join(strTestDataDirectory, "mxv2ResultCharacterisation.xml"), "r"
        )
        self.mxv2ResultCharacterisation = f.read()
        f.close()
        # controlDozorv1
        with open(
            os.path.join(strTestDataDirectory, "ControlDozorv1_0_dataOutput.xml")
        ) as f:
            self.mxv1ResultControlDozor = f.read()

    def test_getEdnaBaseDirectory(self):
        # Test with directory not writeable
        strBaseDirectoryPath = edna_kernel.getEdnaBaseDirectory(
            "/path_that_cannot_be_created"
        )
        self.assertTrue(
            os.path.exists(strBaseDirectoryPath), "Cannot create temp base directory"
        )
        shutil.rmtree(strBaseDirectoryPath)
        # Test with directory containing RAW_DATA
        strTmpDirectory = tempfile.mkdtemp()
        strDirectory = os.path.join(strTmpDirectory, "RAW_DATA")
        strBaseDirectoryPath = edna_kernel.getEdnaBaseDirectory(strDirectory)
        self.assertTrue(
            os.path.exists(strBaseDirectoryPath), "Cannot create temp base directory"
        )
        shutil.rmtree(strTmpDirectory)

    def test_create_ref_data_collect(self):
        characterisationExposureTime = 0.1
        transmission = 100
        osc_range = 0.1
        no_reference_images = 2
        angle_between_reference_images = 90
        phi = 100
        mxv1StrategyResult = edna_mxv1.create_ref_data_collect(
            characterisationExposureTime,
            transmission,
            osc_range,
            no_reference_images,
            angle_between_reference_images,
            phi,
        )
        self.assertTrue(mxv1StrategyResult is not None)

    def test_create_n_images_data_collect(self):
        collectExposureTime = 0.1
        transmission = 100
        osc_range = 180.0 / 900
        number_of_images = 900
        phi = 100
        mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
            collectExposureTime, transmission, number_of_images, osc_range, phi
        )
        print(mxv1StrategyResult)
        self.assertTrue(mxv1StrategyResult is not None)

    def test_parseMxv1StrategyResult(self):
        self.assertNotEqual(
            None, edna_mxv1.parseMxv1StrategyResult(self.mxv1BurnStrategyResult)
        )

    def test_createEdnaImagePath(self):
        beamline = "simulator"
        directory = "/data/id29/inhouse/id291/20130417/RAW_DATA/fastMesh1D"
        prefix = "test"
        expTypePrefix = "ref-"
        suffix = "img"
        no_reference_images = 2
        run_number = 2
        mxv1StrategyResult = edna_mxv1.createEdnaImagePath(
            beamline,
            directory,
            prefix,
            expTypePrefix,
            suffix,
            no_reference_images,
            run_number,
        )
        self.assertTrue(mxv1StrategyResult is not None)

    def test_getListImagePath(self):
        run_number = 6
        expTypePrefix = "ref-"
        prefix = "p1burn"
        directory = "/tmp"
        suffix = "img"
        listImagePath = edna_mxv1.getListImagePath(
            self.mxv1BurnStrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            bIgnoreBurnImages=True,
            bFirstImageOnly=False,
        )
        listImagePathReference = [
            "/tmp/ref-p1burnw1_6_0001.img",
            "/tmp/ref-p1burnw1_6_0002.img",
            "/tmp/ref-p1burnw1_6_0003.img",
            "/tmp/ref-p1burnw3_6_0001.img",
            "/tmp/ref-p1burnw3_6_0002.img",
            "/tmp/ref-p1burnw3_6_0003.img",
        ]
        self.assertEqual(
            listImagePathReference, listImagePath, "Generate list of images failed"
        )

    def test_extractKappaAnglesFromComment(self):
        beamline = "simulator"
        mxv2ResultCharacterisation = self.mxv2ResultCharacterisation
        dictKappaAngles = edna_mxv1.extractKappaAnglesFromComment(
            beamline, mxv2ResultCharacterisation
        )
        self.assertTrue(
            "omega" in dictKappaAngles
            and "kappa" in dictKappaAngles
            and "phi" in dictKappaAngles
            and "strategy" in dictKappaAngles
        )

    def test_extractSpaceGroupFromMXv2Results(self):
        strSpaceGroupName = edna_mxv1.extractSpaceGroupFromMXv2Results(
            self.mxv2ResultCharacterisation
        )
        self.assertEqual("P3", strSpaceGroupName)

    def test_checkRankingResolution(self):
        beamline = "simulator"
        mxv1ResultCharacterisation = self.burn_mxv1ResultCharacterisation
        tupleNewResolution = edna_mxv1.checkRankingResolution(
            beamline, mxv1ResultCharacterisation
        )
        print(tupleNewResolution)
        self.assertFalse(tupleNewResolution[0])
        mxv1ResultCharacterisation = self.mxv1ResultCharacterisation_eEDNA
        tupleNewResolution = edna_mxv1.checkRankingResolution(
            beamline, mxv1ResultCharacterisation
        )
        print(tupleNewResolution)
        self.assertTrue(tupleNewResolution[0])

    def test_getResolutionAndPhistart(self):
        beamline = "simulator"
        mxv1ResultCharacterisation = self.burn_mxv1ResultCharacterisation
        tupleResolutionPhiStart = edna_mxv1.getResolutionAndPhistart(
            beamline, mxv1ResultCharacterisation
        )
        self.assertAlmostEqual(tupleResolutionPhiStart[0], 2.13, 2)
        self.assertAlmostEqual(tupleResolutionPhiStart[1], 12.34, 2)
        mxv1ResultCharacterisation = self.mxv1ResultCharacterisation_eEDNA
        tupleResolutionPhiStart = edna_mxv1.getResolutionAndPhistart(
            beamline, mxv1ResultCharacterisation
        )
        self.assertAlmostEqual(tupleResolutionPhiStart[0], 2.22, 2)
        self.assertAlmostEqual(tupleResolutionPhiStart[1], 90.0, 2)

    def test_getCellSpaceGroupFromCharacterisationResult(self):
        beamline = "simulator"
        mxv1ResultCharacterisation = self.mxv1ResultCharacterisation_eEDNA
        dictCellSpaceGroup = edna_mxv1.getCellSpaceGroupFromCharacterisationResult(
            beamline, mxv1ResultCharacterisation
        )
        self.assertTrue("spaceGroup" in dictCellSpaceGroup)
        self.assertTrue("cell_a" in dictCellSpaceGroup)
        self.assertTrue("cell_b" in dictCellSpaceGroup)
        self.assertTrue("cell_c" in dictCellSpaceGroup)
        self.assertTrue("cell_alpha" in dictCellSpaceGroup)
        self.assertTrue("cell_beta" in dictCellSpaceGroup)
        self.assertTrue("cell_gamma" in dictCellSpaceGroup)

    def test_isMutipleSweepDataCollection(self):
        self.assertTrue(
            edna_mxv1.isMutipleSweepDataCollection(self.mxv1TwoSweepStrategyResult)
        )
        self.assertTrue(
            not edna_mxv1.isMutipleSweepDataCollection(self.mxv1BurnStrategyResult)
        )

    def tes_createRdExpressWebpage(self):
        halfDoseTime = 47.102
        tmpDir = tempfile.mkdtemp(
            prefix="test_createRdExpressWebpage_",
            dir="/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/tmp",
        )
        snapShotPath, htmlFilePath, jsonFilePath = edna_mxv1.createRdExpressWebpage(
            self.mxv1ResultControlDozor, halfDoseTime, tmpDir
        )
        self.assertTrue(os.path.exists(snapShotPath))
        self.assertTrue(os.path.exists(htmlFilePath))
        self.assertTrue(os.path.exists(jsonFilePath))
        shutil.rmtree(tmpDir)
