import os
import json
import time
import shutil
import unittest
import tempfile

from bes.workflow_lib import dehydration, edna_mxv1


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # mxv1ResultCharacterisation_eEDNA
        f = open(
            os.path.join(strTestDataDirectory, "mxv1ResultCharacterisation_eEDNA.xml"),
            "r",
        )
        self.mxv1ResultCharacterisation_eEDNA = f.read()
        f.close()
        # dehydrationResults
        f = open(os.path.join(strTestDataDirectory, "dehydrationResults.txt"), "r")
        self.listDehydrationResults = json.loads(f.read())
        f.close()

    def test_extract_results(self):
        beamline = "simulator"
        startTime = time.time()
        currentRH = 98.0
        xsDataResultCharacterisation = (
            edna_mxv1.XSDataResultCharacterisation.parseString(
                self.mxv1ResultCharacterisation_eEDNA
            )
        )
        dictResults = dehydration.extract_results(
            beamline, startTime, currentRH, xsDataResultCharacterisation
        )
        self.assertTrue("RH" in dictResults)
        self.assertTrue("elapsedTime" in dictResults)
        self.assertTrue("imageName" in dictResults)
        self.assertTrue("TIS" in dictResults)
        self.assertTrue("spots" in dictResults)
        self.assertTrue("goodBraggCandidates" in dictResults)
        self.assertTrue("length_a" in dictResults)
        self.assertTrue("length_b" in dictResults)
        self.assertTrue("length_c" in dictResults)
        self.assertTrue("r1" in dictResults)
        self.assertTrue("r2" in dictResults)
        self.assertTrue("rankingResolution" in dictResults)
        self.assertTrue("spaceGroup" in dictResults)
        self.assertTrue("mosaicity" in dictResults)

    def test_makePlots(self):
        beamline = "simulator"
        targetRH = 98.0
        currentRH = 98.0
        directory = "/tmp"
        workflow_working_dir = tempfile.mkdtemp(prefix="dehydrationPlotTest_")
        dehydration.makePlotsAndWebPage(
            beamline,
            directory,
            workflow_working_dir,
            targetRH,
            currentRH,
            self.listDehydrationResults,
        )
        shutil.rmtree(workflow_working_dir)
