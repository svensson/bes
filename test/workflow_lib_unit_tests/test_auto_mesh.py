"""
Created on Oct 29, 2014

@author: opid30
"""

import os
import unittest
import tempfile
import json
import shutil

from bes.workflow_lib import grid


class Test(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        self.test_data_directory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        self.working_dir = tempfile.mkdtemp(prefix="autoMesh_", dir="/tmp")
        os.chmod(self.working_dir, 0o755)

    def tearDown(self) -> None:
        shutil.rmtree(self.working_dir)

    def test_checkForCorrelatedImages(self):
        testDataPath1 = os.path.join(self.test_data_directory, "dictLoop_1.json")
        f = open(testDataPath1)
        dictLoop = json.loads(f.read())
        f.close()
        self.assertTrue(grid.checkForCorrelatedImages(dictLoop))
        testDataPath2 = os.path.join(self.test_data_directory, "dictLoop_2.json")
        f = open(testDataPath2)
        dictLoop = json.loads(f.read())
        f.close()
        self.assertFalse(grid.checkForCorrelatedImages(dictLoop))

    def test_autoMesh_identicalImages_1(self):
        snapshotDir1 = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_sameimage_1"
        (
            angleMinThickness,
            x1Pixels,
            y1Pixels,
            dxPixels,
            dyPixels,
            deltaPhiz,
            stdPhiz,
            imagePath,
            areTheSameImage,
        ) = grid.autoMesh(
            snapshotDir=snapshotDir1,
            workflowWorkingDir=self.working_dir,
            autoMeshWorkingDir=self.working_dir,
            loopMaxWidth=330,
            loopMinWidth=250,
            prefix="snapshot",
            debug=False,
            findLargestMesh=False,
        )
        self.assertTrue(areTheSameImage)

    def test_autoMesh_identicalImages_2(self):
        snapshotDir2 = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-152805_jhp9cH"
        (
            angleMinThickness,
            x1Pixels,
            y1Pixels,
            dxPixels,
            dyPixels,
            deltaPhiz,
            stdPhiz,
            imagePath,
            areTheSameImage,
        ) = grid.autoMesh(
            snapshotDir=snapshotDir2,
            workflowWorkingDir=self.working_dir,
            autoMeshWorkingDir=self.working_dir,
            loopMaxWidth=330,
            loopMinWidth=250,
            prefix="snapshot",
            debug=False,
            findLargestMesh=False,
        )
        self.assertFalse(areTheSameImage)

    def test_autoMesh_identicalImages_3(self):
        snapshotDir3 = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151211-133113_unt8EX"
        (
            angleMinThickness,
            x1Pixels,
            y1Pixels,
            dxPixels,
            dyPixels,
            deltaPhiz,
            stdPhiz,
            imagePath,
            areTheSameImage,
        ) = grid.autoMesh(
            snapshotDir=snapshotDir3,
            workflowWorkingDir=self.working_dir,
            autoMeshWorkingDir=self.working_dir,
            loopMaxWidth=330,
            loopMinWidth=250,
            prefix="snapshot",
            debug=False,
            findLargestMesh=False,
        )
        self.assertFalse(areTheSameImage)

    def test_autoMesh_identicalImages_4(self):
        snapshotDir4 = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots//snapshots_20160203-092805_rJIRxO"
        (
            angleMinThickness,
            x1Pixels,
            y1Pixels,
            dxPixels,
            dyPixels,
            deltaPhiz,
            stdPhiz,
            imagePath,
            areTheSameImage,
        ) = grid.autoMesh(
            snapshotDir=snapshotDir4,
            workflowWorkingDir=self.working_dir,
            autoMeshWorkingDir=self.working_dir,
            loopMaxWidth=330,
            loopMinWidth=250,
            prefix="snapshot",
            debug=False,
            findLargestMesh=False,
        )
        self.assertTrue(areTheSameImage)

    def test_autoMesh_identicalImages_5(self):
        snapshotDir5 = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30b/snapshots/snapshots_20180503-104037_pMpOuP"
        (
            angleMinThickness,
            x1Pixels,
            y1Pixels,
            dxPixels,
            dyPixels,
            deltaPhiz,
            stdPhiz,
            imagePath,
            areTheSameImage,
        ) = grid.autoMesh(
            snapshotDir=snapshotDir5,
            workflowWorkingDir=self.working_dir,
            autoMeshWorkingDir=self.working_dir,
            loopMaxWidth=1000,
            loopMinWidth=350,
            prefix="snapshot",
            debug=False,
            findLargestMesh=False,
        )
        self.assertTrue(areTheSameImage)
