"""
Created on Feb 9, 2015

@author: svensson
"""

import os
import pprint
import unittest
from bes.workflow_lib import crims


class Test(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        self.testDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )

    def test_xml2dict(self):
        xml = "<erik><a x='1'>v</a><a y='2'>w</a></erik>"
        dictXml = crims.xml2dict(xml)
        reference = {
            "erik": {"a": [{"@x": "1", "#text": "v"}, {"#text": "w", "@y": "2"}]}
        }
        self.assertEqual(reference, dictXml)

    def test_xml2dict_ProcessingPlan(self):
        xmlPath = os.path.join(self.testDataDirectory, "ProcessingPlan_CD006710.xml")
        xml = open(xmlPath, "r").read()
        dictXml = crims.xml2dict(xml)
        pprint.pprint(dictXml)
        self.assertNotEqual(None, dictXml)

    def test_getBarcodeFromPath(self):
        directory = "/data/id30b/inhouse/opid30b/20160108/RAW_DATA/CD006710/Test"
        barcode = crims.getBarcodeFromPath(directory)
        self.assertEqual("CD006710", barcode)
