#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "09/05/2016"

import os
import shutil
import tempfile
import unittest

from bes.workflow_lib.report import WorkflowStepReport


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        self.test_data_directory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )

    def test_Info(self):
        wf_report = WorkflowStepReport("Test", "Test title")
        wf_report.addInfo("Some text")
        test_dir = tempfile.mkdtemp(prefix="WorkflowStepReport_")
        html_file = wf_report.renderHtml(test_dir)
        json_file = wf_report.renderJson(test_dir)
        self.assertTrue(os.path.exists(html_file))
        self.assertTrue(os.path.exists(json_file))
        shutil.rmtree(test_dir)

    def test_Image(self):
        wf_report = WorkflowStepReport("Test", "Test Image")
        pathToImage = os.path.join(
            self.test_data_directory, "snapshots", "snapshot_000.png"
        )
        wf_report.addImage(pathToImage, "Snapshot phi = 0 degrees")
        html_dir = tempfile.mkdtemp(prefix="WorkflowStepReport_")
        html_file = wf_report.renderHtml(html_dir)
        self.assertTrue(os.path.exists(html_file))
        shutil.rmtree(html_dir)

    def test_ImageList(self):
        wf_report = WorkflowStepReport("Test", "Test Image List")
        path_to_image = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20141128-080912_2OjRxn/snapshot_000.png"
        wf_report.startImageList()
        wf_report.addImage(path_to_image, "Snapshot phi = 0 degrees")
        wf_report.addImage(path_to_image, "Snapshot phi = 90 degrees")
        wf_report.endImageList()
        html_dir = tempfile.mkdtemp(prefix="WorkflowStepReport_")
        html_file = wf_report.renderHtml(html_dir)
        json_file = wf_report.renderJson(html_dir)
        self.assertTrue(os.path.exists(html_file))
        self.assertTrue(os.path.exists(json_file))
        shutil.rmtree(html_dir)

    def test_ImageWithThumbnail(self):
        wf_report = WorkflowStepReport("Test", "Test Image With Thumbnail")
        path_to_image = os.path.join(
            self.test_data_directory, "thumbnail", "t1_1_0001.jpeg"
        )
        path_to_thumbnail_image = os.path.join(
            self.test_data_directory, "thumbnail", "t1_1_0001.thumb.jpeg"
        )
        wf_report.addImage(
            path_to_image, "Best location", pathToThumbnailImage=path_to_thumbnail_image
        )
        html_dir = tempfile.mkdtemp(prefix="WorkflowStepReport_")
        html_file = wf_report.renderHtml(html_dir)
        self.assertTrue(os.path.exists(html_file))
        shutil.rmtree(html_dir)

    def test_Table(self):
        wf_report = WorkflowStepReport("Test", "Test Table")
        table_columns = ["Column 1", "Column 2", "Column 3"]
        table_data = [[1, 2, 3], ["hi", "ho", "ha"], [4, 5, 6]]
        wf_report.addTable("Table 1", table_columns, table_data, orientation="vertical")
        html_dir = tempfile.mkdtemp(prefix="WorkflowStepReport_")
        html_file = wf_report.renderHtml(html_dir)
        self.assertTrue(os.path.exists(html_file))
        shutil.rmtree(html_dir)
