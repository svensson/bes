import os
import json
import shutil
import unittest

from bes.workflow_lib import grid, path


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        self.strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # Test mesh data
        f = open(os.path.join(self.strTestDataDirectory, "meshResults.json"), "r")
        strData = f.read()
        self.meshPositions = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(os.path.join(self.strTestDataDirectory, "meshResults2D.json"), "r")
        strData = f.read()
        self.meshPositions2D = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(os.path.join(self.strTestDataDirectory, "meshResults1D.json"), "r")
        strData = f.read()
        self.meshPositions1D = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(
            os.path.join(self.strTestDataDirectory, "meshResults2DnoSignal.json"), "r"
        )
        strData = f.read()
        self.meshPositions2DnoSignal = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(
            os.path.join(self.strTestDataDirectory, "meshResults1DnoSignal.json"), "r"
        )
        strData = f.read()
        self.meshPositions1DnoSignal = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(
            os.path.join(
                self.strTestDataDirectory, "meshResults1DzeroDozorSignal.json"
            ),
            "r",
        )
        strData = f.read()
        self.meshResults1DzeroDozorSignal = json.loads(strData)
        f.close()

    def test_createImageResults2D(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 20,
            "steps_y": 3,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        bestPosition = grid.find_best_position(grid_info, self.meshPositions2D)
        plotFile = grid.create_image(
            grid_info, self.meshPositions2D, [bestPosition], workflow_working_dir
        )
        os.system("display {0}".format(plotFile))
        self.assertTrue(os.path.exists(os.path.join(workflow_working_dir, plotFile)))
        shutil.rmtree(workflow_working_dir)

    def test_createImageResults1D(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 11,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        bestPosition = grid.find_best_position(grid_info, self.meshPositions1D)
        plotFile = grid.create_image(
            grid_info, self.meshPositions2D, [bestPosition], workflow_working_dir
        )
        os.system("display {0}".format(plotFile))
        self.assertTrue(os.path.exists(os.path.join(workflow_working_dir, plotFile)))
        shutil.rmtree(workflow_working_dir)
