import os
import json
import pprint
import shutil
import unittest
import tempfile

from bes.workflow_lib import grid, path


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        self.strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # Test mesh data
        f = open(os.path.join(self.strTestDataDirectory, "meshResults.json"), "r")
        strData = f.read()
        self.meshPositions = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(os.path.join(self.strTestDataDirectory, "meshResults2D.json"), "r")
        strData = f.read()
        self.meshPositions2D = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(os.path.join(self.strTestDataDirectory, "meshResults1D.json"), "r")
        strData = f.read()
        self.meshPositions1D = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(
            os.path.join(self.strTestDataDirectory, "meshResults2DnoSignal.json"), "r"
        )
        strData = f.read()
        self.meshPositions2DnoSignal = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(
            os.path.join(self.strTestDataDirectory, "meshResults1DnoSignal.json"), "r"
        )
        strData = f.read()
        self.meshPositions1DnoSignal = json.loads(strData)
        f.close()
        # Test mesh data
        f = open(
            os.path.join(
                self.strTestDataDirectory, "meshResults1DzeroDozorSignal.json"
            ),
            "r",
        )
        strData = f.read()
        self.meshResults1DzeroDozorSignal = json.loads(strData)
        f.close()

    def test_determineMeshPositions(self):
        beamline = "simulator"
        motorPositons = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        run_number = 1
        expTypePrefix = "mesh-"
        prefix = "test"
        suffix = "cbf"
        osc_range = 1.0
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 1, "steps_y": 10, "angle": 0.0}"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        self.assertEqual(10, len(meshPositions))
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 10, "steps_y": 1, "angle": 0.0}"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        self.assertEqual(10, len(meshPositions))
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 10, "steps_y": 10, "angle": 0.0}"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        self.assertEqual(100, len(meshPositions))

    def test_determineMeshPositions2(self):
        beamline = "simulator"
        motorPositons = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        run_number = 1
        expTypePrefix = "mesh-"
        prefix = "test"
        suffix = "cbf"
        osc_range = 1.0
        grid_info = """{"x1": 0.0, "y1": -0.3, "dx_mm": 0.6, "dy_mm": 0.6, "steps_x": 1, "steps_y": 40, "angle": 0.0}"""
        # 022-07-20 15:30:39,416 DEBUG Grid x1=0.000
        # 2022-07-20 15:30:39,419 DEBUG Grid dx=0.600
        # 2022-07-20 15:30:39,421 DEBUG Grid nx=1
        # 2022-07-20 15:30:39,613 DEBUG Grid y1=-0.300
        # 2022-07-20 15:30:39,616 DEBUG Grid dy=0.600
        # 2022-07-20 15:30:39,815 DEBUG Grid ny=40
        # 2022-07-20 15:30:39,816 DEBUG Grid angle=0.0
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        pprint.pprint(meshPositions)

    def test_determineMeshPositions3(self):
        beamline = "simulator"
        motorPositons = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        prefix = "test"
        suffix = "img"
        run_number = 1
        expTypePrefix = "ref-"
        osc_range = 1.0
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 1, "steps_y": 10, "angle": 0.0}"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        pprint.pprint(meshPositions)
        self.assertEqual(10, len(meshPositions))
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 10, "steps_y": 1, "angle": 0.0}"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        self.assertEqual(10, len(meshPositions))
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 10, "steps_y": 10, "angle": 0.0}"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )
        self.assertEqual(100, len(meshPositions))

    def test_determineMeshPositions_multi_wedge(self):
        beamline = "simulator"
        motorPositons = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        prefix = "test"
        suffix = "img"
        run_number = 1
        expTypePrefix = "ref-"
        grid_info = """{"x1": 0.0, "y1": 0.0, "dx_mm": 1.0, "dy_mm": 1.0, "steps_x": 1, "steps_y": 10, "angle": 0.0}"""
        osc_range = 1.0
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
            multi_wedge=True,
        )
        pprint.pprint(meshPositions)
        self.assertEqual(10, len(meshPositions))

    def test_determineMeshPositions_id30a3(self):
        beamline = "id30a3"
        motorPositons = {
            "phi": 269.9992,
            "focus": -0.01,
            "phiy": -1.2134,
            "phiz": 0.622,
            "sampx": 0.0216,
            "sampy": 0.5155,
            "zoom": 1,
            "kappa": 0.007,
            "kappa_phi": 0.0022,
        }
        run_number = 0
        expTypePrefix = "mesh-"
        prefix = "local-user"
        suffix = "h5"
        osc_range = 1
        grid_info = """{
              "t": "G",
              "id": "G1",
              "name": "Grid-1",
              "state": "SAVED",
              "label": "Grid",
              "screen_coord": [
                278.3070539419087,
                427.0207468879668
              ],
              "selected": true,
              "refs": [],
              "width": 216.50776328536094,
              "height": 185.57808281602365,
              "cell_count_fun": "zig-zag",
              "cell_h_space": 0,
              "cell_height": 30,
              "cell_v_space": 0,
              "cell_width": 30,
              "num_cols": 7,
              "num_rows": 6,
              "result": null,
              "pixels_per_mm": [
                516.1956381468576,
                516.1956381468576
              ],
              "beam_pos": [
                640,
                512
              ],
              "beam_width": 0.03,
              "beam_height": 0.03,
              "hide_threshold": 5,
              "motor_positions": {
                "phi": 269.99834863281194,
                "focus": null,
                "phiz": 0.6219693306587839,
                "phiy": -0.5127260444297095,
                "zoom": null,
                "sampx": -0.1430251808299267,
                "sampy": 0.5154926834149213,
                "kappa": null,
                "kappa_phi": null
              },
              "x1": -0.6856896597510375,
              "y1": -0.14962605809128626,
              "steps_x": 7,
              "steps_y": 6,
              "dx_mm": 0.3894296644245655,
              "dy_mm": 0.32951114093534184,
              "angle": 0
            }"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
            meshZigZag=True,
        )
        pprint.pprint(meshPositions)

    def test_determineMeshPositions_id30b(self):
        beamline = "id30b"
        motorPositons = {
            "phi": 40.00058100000024,
            "focus": None,
            "phiy": -0.3849,
            "phiz": 0.1,
            "sampx": -0.0079,
            "sampy": -0.03,
            "zoom": None,
            "kappa": 0.001,
            "kappa_phi": 0,
        }
        run_number = 0
        expTypePrefix = "mesh-"
        prefix = "local-user"
        suffix = "h5"
        osc_range = 1
        grid_info = """{
  "t": "G",
  "id": "G1",
  "name": "Grid-1",
  "state": "SAVED",
  "label": "Grid",
  "screen_coord": [
    202.0495867768595,
    327.93388429752065
  ],
  "selected": true,
  "refs": [],
  "width": 791.4391633313248,
  "height": 474.8634979987949,
  "cell_count_fun": "zig-zag",
  "cell_h_space": 0,
  "cell_height": 20,
  "cell_v_space": 0,
  "cell_width": 20,
  "num_cols": 10,
  "num_rows": 6,
  "result": [],
  "pixels_per_mm": [
    3957.195816656624,
    3957.195816656624
  ],
  "beam_pos": [
    640,
    512
  ],
  "beam_width": 0.02,
  "beam_height": 0.02,
  "hide_threshold": 5,
  "motor_positions": {
    "phi": 39.9998643984145,
    "focus": null,
    "phiz": 0.10000795760267334,
    "phiy": -0.27425312149942893,
    "zoom": null,
    "sampx": 0.02203895999447037,
    "sampy": -0.06565126536228626,
    "kappa": null,
    "kappa_phi": null
  },
  "x1": -0.10067190847107442,
  "y1": -0.03651428037190085,
  "steps_x": 10,
  "steps_y": 6,
  "dx_mm": 0.18000000000000002,
  "dy_mm": 0.1,
  "angle": 0
}"""
        meshPositions1 = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
            meshZigZag=True,
            phiyOffset=0.0,
            phiyOffsetDirection=1,
        )
        meshPositions2 = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
            meshZigZag=True,
        )
        for index, position in enumerate(meshPositions1):
            phiy1 = meshPositions1[index]["phiy"]
            phiy2 = meshPositions2[index]["phiy"]
            print(f"{index+1:5d}, {phiy1:10.3f}, {phiy2:10.3f}")
        # pprint.pprint(meshPositions)

    def test_determineMeshPositions_id23eh2(self):
        beamline = "id23eh2"
        motorPositons = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        run_number = 1
        expTypePrefix = "mesh-"
        prefix = "test"
        suffix = "cbf"
        osc_range = 1
        grid_info = """{
    "beam_height": 0.009614,
    "x1": -0.01653,
    "angle": 0,
    "dy_mm": 0.17305199999999998,
    "y1": -0.057247,
    "dx_mm": 0.01914,
    "steps_y": 19,
    "steps_x": 3,
    "id": "Grid - 1",
    "beam_width": 0.00957
}
"""
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
            meshZigZag=True,
        )
        pprint.pprint(meshPositions)

    def test_find_best_position2D(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 20,
            "steps_y": 3,
            "x1": -0.104,
            "y1": -0.062,
        }
        bestPosition = grid.find_best_position(grid_info, self.meshPositions2D)
        self.assertNotEqual(None, bestPosition)

    def test_find_best_position1DnoSignal(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 4,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        bestPosition = grid.find_best_position(grid_info, self.meshPositions1DnoSignal)
        self.assertEqual(None, bestPosition)

    def test_find_best_position2DnoSignal(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 4,
            "steps_y": 4,
            "x1": -0.104,
            "y1": -0.062,
        }
        bestPosition = grid.find_best_position(grid_info, self.meshPositions2DnoSignal)
        self.assertEqual(None, bestPosition)

    def test_find_best_position1D(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 11,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        bestPosition = grid.find_best_position(grid_info, self.meshPositions1D)
        self.assertNotEqual(None, bestPosition)

    def test_grid_create_working_directory(self):
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        grid_working_directory = grid.grid_create_working_directory(
            workflow_working_dir
        )
        self.assertTrue(os.path.exists(grid_working_directory))
        shutil.rmtree(workflow_working_dir)

    def test_plotResults1D(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 11,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        bestPosition = grid.find_best_position(grid_info, self.meshPositions1D)
        plotFile = grid.create_plot_file(
            grid_info, self.meshPositions1D, [bestPosition], workflow_working_dir
        )
        self.assertTrue(os.path.exists(os.path.join(workflow_working_dir, plotFile)))
        shutil.rmtree(workflow_working_dir)

    def test_plotResults1DzeroDozorSignal(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 1,
            "steps_y": 40,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        bestPosition = grid.find_best_position(
            grid_info, self.meshResults1DzeroDozorSignal
        )
        plotFile = grid.create_plot_file(
            grid_info,
            self.meshResults1DzeroDozorSignal,
            [bestPosition],
            workflow_working_dir,
        )
        self.assertTrue(os.path.exists(os.path.join(workflow_working_dir, plotFile)))
        shutil.rmtree(workflow_working_dir)

    def test_plotResults2D(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 20,
            "steps_y": 3,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        bestPosition = grid.find_best_position(grid_info, self.meshPositions2D)
        plotFile = grid.create_plot_file(
            grid_info, self.meshPositions2D, [bestPosition], workflow_working_dir
        )
        self.assertTrue(os.path.exists(os.path.join(workflow_working_dir, plotFile)))
        shutil.rmtree(workflow_working_dir)

    def test_grid_create_result_html_page1D(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 11,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory(directory)
        bestPosition = grid.find_best_position(grid_info, self.meshPositions1D)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            self.meshPositions1D,
            [bestPosition],
            directory,
            workflow_working_dir,
        )
        self.assertTrue(
            os.path.exists(
                os.path.join(
                    dict_html["html_directory_path"], dict_html["index_file_name"]
                )
            )
        )
        shutil.rmtree(workflow_working_dir)

    def test_grid_create_result_html_page2D(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 20,
            "steps_y": 3,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory(directory)
        bestPosition = grid.find_best_position(grid_info, self.meshPositions2D)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            self.meshPositions2D,
            [bestPosition],
            directory,
            workflow_working_dir,
        )
        self.assertTrue(
            os.path.exists(
                os.path.join(
                    dict_html["html_directory_path"], dict_html["index_file_name"]
                )
            )
        )
        shutil.rmtree(workflow_working_dir)

    def test_getMeshListImagePath(self):
        directory = "/tmp/RAW_DATA"
        meshPositions = self.meshPositions2D
        listImage = grid.getMeshListImagePath(directory, meshPositions)
        self.assertEqual(60, len(listImage))

    def test_mesh_scan_statistics(self):
        meshScanDir = tempfile.mkdtemp(prefix="meshScanStatistics")
        dict_statistics = grid.mesh_scan_statistics(
            self.meshPositions2D, workingDir=meshScanDir
        )
        strMessage = dict_statistics["message"]
        print(strMessage)
        self.assertNotEqual(None, strMessage)
        shutil.rmtree(meshScanDir)

    def test_writeTISResultFile(self):
        workflow_working_dir = path.createWorkflowWorkingDirectory()
        grid.writeTISResultFile(self.meshPositions2D, workflow_working_dir, "TIS.txt")
        self.assertTrue(os.path.exists(os.path.join(workflow_working_dir, "TIS.txt")))
        shutil.rmtree(workflow_working_dir)

    def test_get_best_image_id(self):
        mesh_positions = [
            {
                "goodBraggCandidates": 57,
                "iceRings": 4,
                "imageName": "delete_me_1_0010.mccd",
                "inResTotal": 68,
                "inResolutionOvrlSpots": 1,
                "index": 10,
                "indexY": 2,
                "indexZ": 4,
                "maxUnitCell": 288.19999999999999,
                "method1Res": 1.8899999999999999,
                "method2Res": 3.7400000000000002,
                "mosaicity": 0.20000000000000001,
                "pctSaturationTop50Peaks": 6.5999999999999996,
                "phi": 1.0,
                "kappa": 2.0,
                "omega": 3.0,
                "focus": 4.0,
                "phiy": -0.10361557403028013,
                "phiz": 0.0,
                "sampx": 0.0,
                "sampy": -0.062145366117265601,
                "spaceGroup": "F222",
                "spotTotal": 266,
                "totalIntegratedSignal": 3920172.0,
            }
        ]
        list_image_creation = [
            {
                "isCreated": True,
                "fileLocation": "/data/id23eh2/inhouse/opid232/20130516/RAW_DATA/delete_me",
                "imageId": 43645776,
                "fileName": "delete_me_1_0010.mccd",
            }
        ]
        best_position = {
            "goodBraggCandidates": 57,
            "iceRings": 4,
            "imageName": "delete_me_1_0010.mccd",
            "inResTotal": 68,
            "inResolutionOvrlSpots": 1,
            "index": 10,
            "indexY": 2,
            "indexZ": 4,
            "maxUnitCell": 288.19999999999999,
            "method1Res": 1.8899999999999999,
            "method2Res": 3.7400000000000002,
            "mosaicity": 0.20000000000000001,
            "pctSaturationTop50Peaks": 6.5999999999999996,
            "phi": 1.0,
            "kappa": 2.0,
            "omega": 3.0,
            "focus": 4.0,
            "phiy": -0.10361557403028013,
            "phiz": 0.0,
            "sampx": 0.0,
            "sampy": -0.062145366117265601,
            "spaceGroup": "F222",
            "spotTotal": 266,
            "totalIntegratedSignal": 3920172.0,
        }
        best_image_id = grid.find_best_image_id(
            mesh_positions, list_image_creation, best_position
        )
        self.assertEqual(best_image_id, 43645776)

    def test_get_best_image_id_2(self):
        list_image_creation = [
            {
                "isCreated": True,
                "fileLocation": "/data/id23eh2/inhouse/opid232/20130516/RAW_DATA/delete_me",
                "imageId": 43645776,
                "fileName": "4dscan-testw1_1_0073.cbf",
            }
        ]
        best_position = {
            "phiy": -0.72727272727272729,
            "sampx": 0.0,
            "sampy": 0.0,
        }
        best_image_id = grid.find_best_image_id(
            self.meshPositions, list_image_creation, best_position
        )
        self.assertEqual(best_image_id, 43645776)

    def test_find_ispyb_meshPositions2D(self):
        ispyb_positions = grid.find_ispyb_positions(self.meshPositions2D, max_no=5)
        #         pprint.pprint(ispyb_positions)
        self.assertEqual(5, len(ispyb_positions))

    def test_determineMeshGaps(self):
        grid_info = {
            "beam_height": 0.029721696838692243,
            "x1": 0.0,
            "angle": 0,
            "dy_mm": 0.07970818697649283,
            "y1": 0.0013509862199405564,
            "dx_mm": 0.09386352000390082,
            "steps_y": 2,
            "steps_x": 2,
            "id": "Grid - 2",
            "beam_width": 0.043884243118706874,
        }
        meshGaps = grid.determineMeshGaps(grid_info)
        self.assertAlmostEqual(0.050, meshGaps[0], 3)
        self.assertAlmostEqual(0.050, meshGaps[1], 3)
        grid_info = {
            "angle": 0,
            "beam_height": 0.029721696838692243,
            "beam_width": 0.043884243118706874,
            "dx_mm": 0.043884243118706874,
            "dy_mm": 0.029721696838692243,
            "id": "Grid - 3",
            "steps_x": 2,
            "steps_y": 2,
            "x1": -0.028037155325840505,
            "y1": -0.024317751958930017,
        }
        self.assertEqual((0.0, 0.0), grid.determineMeshGaps(grid_info))
        grid_info = {
            "angle": 0,
            "beam_height": 0.029721696838692243,
            "beam_width": 0.043884243118706874,
            "dx_mm": 0.043884243118706874,
            "dy_mm": 0.029721696838692243,
            "id": "Grid - 3",
            "steps_x": 1,
            "steps_y": 2,
            "x1": -0.028037155325840505,
            "y1": -0.024317751958930017,
        }
        self.assertEqual((None, 0.0), grid.determineMeshGaps(grid_info))
        grid_info = {
            "angle": 0,
            "beam_height": 0.029721696838692243,
            "beam_width": 0.043884243118706874,
            "dx_mm": 0.043884243118706874,
            "dy_mm": 0.029721696838692243,
            "id": "Grid - 3",
            "steps_x": 2,
            "steps_y": 1,
            "x1": -0.028037155325840505,
            "y1": -0.024317751958930017,
        }
        self.assertEqual((0.0, None), grid.determineMeshGaps(grid_info))
        grid_info = {}
        self.assertEqual((None, None), grid.determineMeshGaps(grid_info))

    def test_saveMeshResults(self):
        # First save some results...
        testDir = os.path.join(tempfile.mkdtemp(prefix="test_saveMeshresults_"))
        raw_directory = os.path.join(testDir, "RAW_DATA", "test")
        pyarch_html_dir = raw_directory
        directory = os.path.join(raw_directory, "MeshScan_01")
        os.makedirs(raw_directory, 0o755)
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 20,
            "steps_y": 3,
            "x1": -0.104,
            "y1": -0.062,
        }
        meshPhiPosition = 50.0
        diffractionSignalDetection = "Dozor (macro-molecules)"
        grid.saveMeshResults(
            directory=directory,
            raw_directory=raw_directory,
            grid_info=grid_info,
            meshPositions=self.meshPositions2D,
            meshPhiPosition=meshPhiPosition,
            pyarch_html_dir=pyarch_html_dir,
            diffractionSignalDetection=diffractionSignalDetection,
        )
        # Then we try to find and read them
        listMeshResults = grid.findMeshResults(raw_directory)
        self.assertTrue(len(listMeshResults) > 0)
        meshResults = grid.readMeshResults(raw_directory, listMeshResults[0])
        self.assertTrue("raw_directory" in meshResults)
        self.assertTrue("grid_info" in meshResults)
        self.assertTrue("meshPositions" in meshResults)
        self.assertTrue("pyarch_html_dir" in meshResults)
        self.assertTrue("meshPhiPosition" in meshResults)
        self.assertTrue("diffractionSignalDetection" in meshResults)
        shutil.rmtree(testDir)

    def test_findMeshResults(self):
        raw_directory = (
            "/data/visitor/mx2532/id23eh2/20240125/RAW_DATA/A9_03/run_02_MeshAndCollect"
        )
        listMeshResults = grid.findMeshResults(raw_directory)
        print(listMeshResults)

    def test___commandsForMovingMotors(self):
        best_position = {
            "goodBraggCandidates": 57,
            "iceRings": 4,
            "imageName": "delete_me_1_0010.mccd",
            "inResTotal": 68,
            "inResolutionOvrlSpots": 1,
            "index": 10,
            "indexY": 2,
            "indexZ": 4,
            "maxUnitCell": 288.19999999999999,
            "method1Res": 1.8899999999999999,
            "method2Res": 3.7400000000000002,
            "mosaicity": 0.20000000000000001,
            "pctSaturationTop50Peaks": 6.5999999999999996,
            "phi": 1.0,
            "kappa": 2.0,
            "omega": 3.0,
            "focus": 4.0,
            "phiy": -0.10361557403028013,
            "phiz": 0.0,
            "sampx": 0.0,
            "sampy": -0.062145366117265601,
            "spaceGroup": "F222",
            "spotTotal": 266,
            "totalIntegratedSignal": 3920172.0,
        }
        beamline = "bm14"
        pprint.pprint(grid._commandsForMovingMotors(beamline, best_position))

    def test_deletePositions(self):
        dozorScoreThreshold = 10.0
        remainingPositionStr = """[
    {
        "sampx": 0.15912806539509536,
        "focus": 0.0,
        "omega": 149.883,
        "phiz": 0.21145,
        "phiy": -0.4138,
        "no_pixels_cross_x": 1,
        "indexY": 11.0,
        "max_peak": 106.894,
        "crystal_size_y": 0.07582417582417583,
        "indexZ": 6.595171545973876,
        "peak_size_x": 0.0351395730706,
        "sampy": -0.0776566757493188,
        "crystal_size_x": 0.0702791461412151,
        "no_pixels_cross_y": 2,
        "peak_size_y": 0.07582417582417583,
        "nearest_image": {
            "sampx": 0.15912806539509536,
            "goodBraggCandidates": 30,
            "dozor_score": 106.894,
            "focus": 0.0,
            "indexZ": 7,
            "dozorSpotsIntAver": 309.0,
            "inResolutionOvrlSpots": 0,
            "index": 221,
            "method1Res": 2.88,
            "imageName": "mesh-PGM-x07_1_0222.cbf",
            "phiz": 0.24935,
            "indexY": 11,
            "dozorSpotListShape": [
                122,
                5
            ],
            "peak_size_x": 0.0351395730706,
            "sampy": -0.0776566757493188,
            "phiy": -0.4134,
            "peak_size_y": 0.07582417582417583,
            "method2Res": 3.26,
            "spotTotal": 47,
            "dozorVisibleResolution": 2.26,
            "maxUnitCell": 59.5,
            "crystal_size_y": 0.07582417582417583,
            "omega": 149.883,
            "iceRings": 0,
            "crystal_size_x": 0.0702791461412151,
            "inResTotal": 34,
            "totalIntegratedSignal": 32218.0
        }
    },
    {
        "sampx": 0.15912806539509536,
        "goodBraggCandidates": 19,
        "dozor_score": 19.271,
        "focus": 0.0,
        "dozorSpotsIntAver": 203.0,
        "inResolutionOvrlSpots": 0,
        "phiy": -0.24365,
        "index": 136,
        "method1Res": 4.13,
        "imageName": "mesh-PGM-x07_1_0137.cbf",
        "no_pixels_cross_x": 1,
        "indexY": 16,
        "max_peak": 19.271,
        "dozorSpotListShape": [
            27,
            5
        ],
        "indexZ": 4,
        "peak_size_x": 0.0351395730706,
        "sampy": -0.0776566757493188,
        "nearest_image": {
            "sampx": 0.15912806539509536,
            "goodBraggCandidates": 19,
            "dozor_score": 19.271,
            "focus": 0.0,
            "indexZ": 4,
            "dozorSpotsIntAver": 203.0,
            "inResolutionOvrlSpots": 0,
            "index": 136,
            "method1Res": 4.13,
            "imageName": "mesh-PGM-x07_1_0137.cbf",
            "phiz": 0.13935,
            "indexY": 16,
            "dozorSpotListShape": [
                27,
                5
            ],
            "peak_size_x": 0.0351395730706,
            "sampy": -0.0776566757493188,
            "phiy": -0.24365,
            "peak_size_y": 0.037912087912087916,
            "spotTotal": 23,
            "dozorVisibleResolution": 3.43,
            "maxUnitCell": 208.6,
            "crystal_size_y": 0.037912087912087916,
            "omega": 150.05,
            "iceRings": 1,
            "crystal_size_x": 0.0351395730706,
            "inResTotal": 23,
            "totalIntegratedSignal": 12868.0
        },
        "peak_size_y": 0.037912087912087916,
        "spotTotal": 23,
        "phiz": 0.13935,
        "dozorVisibleResolution": 3.43,
        "no_pixels_cross_y": 1,
        "maxUnitCell": 208.6,
        "crystal_size_y": 0.037912087912087916,
        "omega": 150.05,
        "iceRings": 1,
        "crystal_size_x": 0.0351395730706,
        "inResTotal": 23,
        "totalIntegratedSignal": 12868.0
    },
    {
        "sampx": 0.15912806539509536,
        "goodBraggCandidates": 15,
        "dozor_score": 15.757,
        "focus": 0.0,
        "dozorSpotsIntAver": 220.0,
        "inResolutionOvrlSpots": 0,
        "phiy": -0.17555,
        "index": 108,
        "method1Res": 6.15,
        "imageName": "mesh-PGM-x07_1_0109.cbf",
        "no_pixels_cross_x": 1,
        "indexY": 18,
        "max_peak": 15.757,
        "dozorSpotListShape": [
            17,
            5
        ],
        "indexZ": 3,
        "peak_size_x": 0.0351395730706,
        "sampy": -0.0776566757493188,
        "nearest_image": {
            "sampx": 0.15912806539509536,
            "goodBraggCandidates": 15,
            "dozor_score": 15.757,
            "focus": 0.0,
            "indexZ": 3,
            "dozorSpotsIntAver": 220.0,
            "inResolutionOvrlSpots": 0,
            "index": 108,
            "method1Res": 6.15,
            "imageName": "mesh-PGM-x07_1_0109.cbf",
            "phiz": 0.10235,
            "indexY": 18,
            "dozorSpotListShape": [
                17,
                5
            ],
            "peak_size_x": 0.0351395730706,
            "sampy": -0.0776566757493188,
            "phiy": -0.17555,
            "peak_size_y": 0.037912087912087916,
            "spotTotal": 16,
            "dozorVisibleResolution": 3.77,
            "maxUnitCell": 55.3,
            "crystal_size_y": 0.037912087912087916,
            "omega": 150.117,
            "iceRings": 2,
            "crystal_size_x": 0.0351395730706,
            "inResTotal": 16,
            "totalIntegratedSignal": 16382.0
        },
        "peak_size_y": 0.037912087912087916,
        "spotTotal": 16,
        "phiz": 0.10235,
        "dozorVisibleResolution": 3.77,
        "no_pixels_cross_y": 1,
        "maxUnitCell": 55.3,
        "crystal_size_y": 0.037912087912087916,
        "omega": 150.117,
        "iceRings": 2,
        "crystal_size_x": 0.0351395730706,
        "inResTotal": 16,
        "totalIntegratedSignal": 16382.0
    },
    {
        "sampx": 0.15912806539509536,
        "goodBraggCandidates": 9,
        "dozor_score": 7.167,
        "focus": 0.0,
        "dozorSpotsIntAver": 242.0,
        "inResolutionOvrlSpots": 0,
        "phiy": -0.0057,
        "index": 53,
        "method1Res": 6.12,
        "imageName": "mesh-PGM-x07_1_0054.cbf",
        "no_pixels_cross_x": 1,
        "indexY": 23,
        "max_peak": 7.167,
        "dozorSpotListShape": [
            13,
            5
        ],
        "indexZ": 1,
        "peak_size_x": 0.0351395730706,
        "sampy": -0.0776566757493188,
        "nearest_image": {
            "sampx": 0.15912806539509536,
            "goodBraggCandidates": 9,
            "dozor_score": 7.167,
            "focus": 0.0,
            "indexZ": 1,
            "dozorSpotsIntAver": 242.0,
            "inResolutionOvrlSpots": 0,
            "index": 53,
            "method1Res": 6.12,
            "imageName": "mesh-PGM-x07_1_0054.cbf",
            "phiz": 0.0267,
            "indexY": 23,
            "dozorSpotListShape": [
                13,
                5
            ],
            "peak_size_x": 0.0351395730706,
            "sampy": -0.0776566757493188,
            "phiy": -0.0057,
            "peak_size_y": 0.037912087912087916,
            "spotTotal": 11,
            "dozorVisibleResolution": 4.97,
            "maxUnitCell": 52.7,
            "crystal_size_y": 0.037912087912087916,
            "omega": 150.283,
            "iceRings": 0,
            "crystal_size_x": 0.0702791461412151,
            "inResTotal": 11,
            "totalIntegratedSignal": 26053.0
        },
        "peak_size_y": 0.037912087912087916,
        "spotTotal": 11,
        "phiz": 0.0267,
        "dozorVisibleResolution": 4.97,
        "no_pixels_cross_y": 1,
        "maxUnitCell": 52.7,
        "crystal_size_y": 0.037912087912087916,
        "omega": 150.283,
        "iceRings": 0,
        "crystal_size_x": 0.0702791461412151,
        "inResTotal": 11,
        "totalIntegratedSignal": 26053.0
    }
]"""
        listPositions = json.loads(remainingPositionStr)
        listPositions = grid.deletePositions(listPositions, dozorScoreThreshold)
        self.assertEqual(len(listPositions), 3)

    def test_extendGridToMultipleOf100(self):
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.1,
            "dy_mm": 0.1,
            "steps_x": 5,
            "steps_y": 5,
            "x1": -0.104,
            "y1": -0.062,
        }
        newGridInfo = grid.extendGridToMultipleOf100(grid_info)
        self.assertEqual(
            {
                "steps_x": 5,
                "dx_mm": 0.1,
                "y1": -0.062,
                "angle": 0.0,
                "steps_y": 5,
                "x1": -0.104,
                "dy_mm": 0.1,
            },
            newGridInfo,
        )
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.1,
            "dy_mm": 0.1,
            "steps_x": 10,
            "steps_y": 10,
            "x1": -0.104,
            "y1": -0.062,
        }
        newGridInfo = grid.extendGridToMultipleOf100(grid_info)
        self.assertEqual(
            {
                "steps_x": 10,
                "dx_mm": 0.1,
                "y1": -0.062,
                "angle": 0.0,
                "steps_y": 10,
                "x1": -0.104,
                "dy_mm": 0.1,
            },
            newGridInfo,
        )
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.1,
            "dy_mm": 0.1,
            "steps_x": 11,
            "steps_y": 10,
            "x1": -0.104,
            "y1": -0.062,
        }
        newGridInfo = grid.extendGridToMultipleOf100(grid_info)
        self.assertEqual(
            {
                "steps_x": 20,
                "dx_mm": 0.18181818181818182,
                "y1": -0.062,
                "angle": 0.0,
                "steps_y": 10,
                "x1": -0.104,
                "dy_mm": 0.1,
            },
            newGridInfo,
        )
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.1,
            "dy_mm": 0.1,
            "steps_x": 17,
            "steps_y": 16,
            "x1": -0.104,
            "y1": -0.062,
        }
        newGridInfo = grid.extendGridToMultipleOf100(grid_info)
        self.assertEqual(
            {
                "steps_x": 25,
                "dx_mm": 0.14705882352941177,
                "y1": -0.062,
                "angle": 0.0,
                "steps_y": 16,
                "x1": -0.104,
                "dy_mm": 0.1,
            },
            newGridInfo,
        )
        grid_info = {
            "x1": 0,
            "y1": -0.25,
            "dx_mm": 0,
            "dy_mm": 0.5,
            "steps_x": 1,
            "steps_y": 429,
        }
        newGridInfo = grid.extendGridToMultipleOf100(grid_info)
        self.assertEqual(
            {
                "steps_x": 1,
                "dx_mm": 0.0,
                "y1": -0.25,
                "steps_y": 500,
                "x1": 0,
                "dy_mm": 0.5827505827505828,
            },
            newGridInfo,
        )

    def test_getSmallXrayCentringGrid(self):
        pprint.pprint(grid.getSmallXrayCentringGrid(0.05, 0.05))
        pprint.pprint(grid.getSmallXrayCentringGrid(0.03, 0.03))
        pprint.pprint(grid.getSmallXrayCentringGrid(0.02, 0.02))
        pprint.pprint(grid.getSmallXrayCentringGrid(0.015, 0.015))
        pprint.pprint(grid.getSmallXrayCentringGrid(0.01, 0.01))

    def test_newSizeStepsWithGaps(self):
        grid_size = 0.050
        no_steps = 5
        gap_size = 0
        new_grid_size, new_no_steps = grid.newSizeStepsWithGaps(
            grid_size, gap_size, no_steps
        )
        # print(new_grid_size, new_no_steps)
        self.assertEqual((new_grid_size, new_no_steps), (0.05, 5))
        grid_size = 0.050
        no_steps = 5
        gap_size = 0.001
        new_grid_size, new_no_steps = grid.newSizeStepsWithGaps(
            grid_size, gap_size, no_steps
        )
        # print(new_grid_size, new_no_steps)
        self.assertEqual((new_grid_size, new_no_steps), (0.054, 5))
        grid_size = 0.050
        no_steps = 5
        gap_size = 0.005
        new_grid_size, new_no_steps = grid.newSizeStepsWithGaps(
            grid_size, gap_size, no_steps
        )
        # print(new_grid_size, new_no_steps)
        self.assertEqual((new_grid_size, new_no_steps), (0.055, 4))
        grid_size = 0.050
        no_steps = 5
        gap_size = 0.009
        new_grid_size, new_no_steps = grid.newSizeStepsWithGaps(
            grid_size, gap_size, no_steps
        )
        # print(new_grid_size, new_no_steps)
        self.assertEqual((new_grid_size, new_no_steps), (0.048, 3))

    def test_newGridInfoWithGaps(self):
        grid_info = {"dx_mm": 0.050, "dy_mm": 0.030, "steps_x": 5, "steps_y": 3}
        new_grid_info = grid.newGridInfoWithGaps(grid_info, 0, 0)
        pprint.pprint(new_grid_info)
        new_grid_info = grid.newGridInfoWithGaps(grid_info, 0.001, 0.001)
        pprint.pprint(new_grid_info)
        new_grid_info = grid.newGridInfoWithGaps(grid_info, 0.005, 0.005)
        pprint.pprint(new_grid_info)
        new_grid_info = grid.newGridInfoWithGaps(grid_info, 0.009, 0.009)
        pprint.pprint(new_grid_info)
