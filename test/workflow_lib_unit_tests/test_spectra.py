import os
import shutil
import tempfile
import unittest
import numpy as np
from bes.workflow_lib import spectra


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        self.testDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )

    def test_plotSpectra(self):
        plotFile = tempfile.mktemp(suffix=".png", prefix="spectraPlot_")
        allSpectra = np.load(os.path.join(self.testDataDirectory, "spectra.npy"))
        spectra.plotSpectra(allSpectra, 3, plotFile)
        self.assertTrue(os.path.exists(plotFile))
        os.remove(plotFile)

    def test_createHtmlPage(self):
        htmlDir = tempfile.mkdtemp(prefix="SpectraHtmlDr_")
        prefix = "spectratest5"
        directory = "/data/id30a3/inhouse/opid30a3/20151202/RAW_DATA/spectra/xtal1"
        plotFile = os.path.join(htmlDir, "plot.png")
        allSpectra = np.load(os.path.join(self.testDataDirectory, "spectra.npy"))
        spectra.plotSpectra(allSpectra, 3, plotFile)
        indexPath = spectra.createHtmlPage(plotFile, htmlDir, prefix, directory)
        self.assertTrue(os.path.exists(indexPath))
        shutil.rmtree(htmlDir)
