#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "01/03/2021"

import unittest

from bes import config


class TestMethods(unittest.TestCase):
    def test_DICT_PARAMETER(self):

        self.assertEqual("id231control2", config.get_value("id23eh1", "MXCuBE", "host"))
        self.assertEqual("id232control", config.get_value("id23eh2", "MXCuBE", "host"))
        self.assertEqual("id30a1control", config.get_value("id30a1", "MXCuBE", "host"))
        self.assertEqual("id30a3control", config.get_value("id30a3", "MXCuBE", "host"))
        self.assertEqual("id30bcontrol2", config.get_value("id30b", "MXCuBE", "host"))

        self.assertEqual(
            "horizontal", config.get_value("id23eh1", "Goniometer", "rotationAxis")
        )
        self.assertEqual(
            "vertical", config.get_value("id23eh2", "Goniometer", "rotationAxis")
        )
        self.assertEqual(
            "horizontal", config.get_value("id30a1", "Goniometer", "rotationAxis")
        )
        self.assertEqual(
            "horizontal", config.get_value("id30a3", "Goniometer", "rotationAxis")
        )
        self.assertEqual(
            "horizontal", config.get_value("id30b", "Goniometer", "rotationAxis")
        )

        self.assertAlmostEqual(
            0.01, config.get_value("id23eh1", "Beam", "minCharacterisationExposureTime")
        )
        self.assertAlmostEqual(
            0.0043,
            config.get_value("id23eh2", "Beam", "minCharacterisationExposureTime"),
        )
        self.assertAlmostEqual(
            0.02, config.get_value("id30a1", "Beam", "minCharacterisationExposureTime")
        )
        self.assertAlmostEqual(
            0.002, config.get_value("id30a3", "Beam", "minCharacterisationExposureTime")
        )
        self.assertAlmostEqual(
            0.01, config.get_value("id30b", "Beam", "minCharacterisationExposureTime")
        )

        self.assertAlmostEqual(
            0.01, config.get_value("id23eh1", "Beam", "minGridExposureTime")
        )
        self.assertAlmostEqual(
            0.0043, config.get_value("id23eh2", "Beam", "minGridExposureTime")
        )
        self.assertAlmostEqual(
            0.02, config.get_value("id30a1", "Beam", "minGridExposureTime")
        )
        self.assertAlmostEqual(
            0.0014, config.get_value("id30a3", "Beam", "minGridExposureTime")
        )
        self.assertAlmostEqual(
            0.02, config.get_value("id30b", "Beam", "minGridExposureTime")
        )

        self.assertAlmostEqual(
            0.01, config.get_value("id23eh1", "Beam", "minTransmission")
        )
        self.assertAlmostEqual(
            0.01, config.get_value("id23eh2", "Beam", "minTransmission")
        )
        self.assertAlmostEqual(5, config.get_value("id30a1", "Beam", "minTransmission"))
        self.assertAlmostEqual(
            0.1, config.get_value("id30a3", "Beam", "minTransmission")
        )
        self.assertAlmostEqual(1, config.get_value("id30b", "Beam", "minTransmission"))

        self.assertAlmostEqual(
            360.0, config.get_value("id23eh1", "Goniometer", "maxOscillationSpeed")
        )
        self.assertAlmostEqual(
            720.0, config.get_value("id23eh2", "Goniometer", "maxOscillationSpeed")
        )
        self.assertAlmostEqual(
            360.0, config.get_value("id30a1", "Goniometer", "maxOscillationSpeed")
        )
        self.assertAlmostEqual(
            180.0, config.get_value("id30a3", "Goniometer", "maxOscillationSpeed")
        )
        self.assertAlmostEqual(
            360.0, config.get_value("id30b", "Goniometer", "maxOscillationSpeed")
        )

        self.assertAlmostEqual(
            0.01, config.get_value("id23eh1", "Goniometer", "minOscillationWidth")
        )
        self.assertAlmostEqual(
            0.05, config.get_value("id23eh2", "Goniometer", "minOscillationWidth")
        )
        self.assertAlmostEqual(
            0.01, config.get_value("id30a1", "Goniometer", "minOscillationWidth")
        )
        self.assertAlmostEqual(
            0.05, config.get_value("id30a3", "Goniometer", "minOscillationWidth")
        )
        self.assertAlmostEqual(
            0.02, config.get_value("id30b", "Goniometer", "minOscillationWidth")
        )

        self.assertAlmostEqual(
            0.01,
            config.get_value("id23eh1", "Beam", "defaultCharacterisationExposureTime"),
        )
        self.assertAlmostEqual(
            0.02,
            config.get_value("id23eh2", "Beam", "defaultCharacterisationExposureTime"),
        )
        self.assertAlmostEqual(
            0.1,
            config.get_value("id30a1", "Beam", "defaultCharacterisationExposureTime"),
        )
        self.assertAlmostEqual(
            0.075,
            config.get_value("id30a3", "Beam", "defaultCharacterisationExposureTime"),
        )
        self.assertAlmostEqual(
            0.02,
            config.get_value("id30b", "Beam", "defaultCharacterisationExposureTime"),
        )

        self.assertAlmostEqual(
            0.0,
            config.get_value("id23eh1", "Goniometer", "defaultGridOscillationRange"),
        )
        self.assertAlmostEqual(
            0.1,
            config.get_value("id23eh2", "Goniometer", "defaultGridOscillationRange"),
        )
        self.assertAlmostEqual(
            0.0, config.get_value("id30a1", "Goniometer", "defaultGridOscillationRange")
        )
        self.assertAlmostEqual(
            0.0, config.get_value("id30a3", "Goniometer", "defaultGridOscillationRange")
        )
        self.assertAlmostEqual(
            0.0, config.get_value("id30b", "Goniometer", "defaultGridOscillationRange")
        )

        self.assertAlmostEqual(
            0.5, config.get_value("id23eh1", "Beam", "defaultDehydrationTransmission")
        )
        self.assertAlmostEqual(
            1.0, config.get_value("id23eh2", "Beam", "defaultDehydrationTransmission")
        )
        self.assertAlmostEqual(
            0.34, config.get_value("id30a1", "Beam", "defaultDehydrationTransmission")
        )
        self.assertAlmostEqual(
            0.34, config.get_value("id30a3", "Beam", "defaultDehydrationTransmission")
        )
        self.assertAlmostEqual(
            0.34, config.get_value("id30b", "Beam", "defaultDehydrationTransmission")
        )
