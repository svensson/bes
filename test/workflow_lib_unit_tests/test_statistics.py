import os
import unittest
import pprint
import datetime

from bes.workflow_lib import statistics


class TestMethods(unittest.TestCase):
    def test_getNumberOfPDBDepositions(self):
        beamline = "id30a1"
        ndep = statistics.getNumberOfPDBDepositions(beamline)
        print("Number of depositions: {0}".format(ndep))
        self.assertTrue(ndep > 1)

    def test_getListOfCaseID(self):
        externalRef = "fx57"
        listOfCaseID = statistics.getListOfCaseID(externalRef)
        print(listOfCaseID)
        self.assertTrue(len(listOfCaseID) > 1)

    # Not working any longer with besdb4
    #    def test_getTotalNumberOfMXPressPerCaseID(self):
    #        caseID = 7403  # fx57
    #        listDict = statistics.getTotalNumberOfMXPressPerCaseID(caseID)
    #        pprint.pprint(listDict)
    #        self.assertGreater(len(listDict), 1)

    def test_getExecutionTime(self):
        bes_request_id = 5978253
        cursor = statistics.getCursor()
        executionTime = statistics.getExecutionTime(cursor, bes_request_id)
        self.assertEqual(executionTime, 57.273)

    def test_getListOfAllActors(self):
        mainRequestId = 5978241
        listActor = statistics.getListOfAllActors(mainRequestId)
        self.assertTrue(len(listActor) > 0)

    #        pprint.pprint(listActor)

    def test_getDictOfAllActors(self):
        mainRequestId = 5978241
        listDictActor = statistics.getDictOfAllActors(mainRequestId)
        self.assertTrue(len(listDictActor) > 0)
        # pprint.pprint(listDictActor)

    def test_getMXPressExecutionStatisticsForExcel_headings(self):
        mainRequestId = 5978241
        headings = statistics.getMXPressExecutionStatisticsForExcel(
            mainRequestId, getHeadings=True
        )
        print(headings)

    def test_getMXPressExecutionStatisticsForExcel(self):
        mainRequestId = 5978241
        headings = statistics.getMXPressExecutionStatisticsForExcel(
            mainRequestId, getHeadings=False
        )
        print(headings)

    def test_getDictRequestAttribute(self):
        mainRequestId = 5978241
        dictAttribute = statistics.getDictRequestAttribute(mainRequestId)
        pprint.pprint(dictAttribute)
        bes_request_id = 5978253
        dictAttribute = statistics.getDictRequestAttribute(bes_request_id)
        pprint.pprint(dictAttribute)

    def tes_createMXPressCSVFile(self):
        proposal = "mx415"
        startDate = datetime.datetime(2017, 3, 10)
        endDate = datetime.datetime(2017, 3, 15)
        fileName = "/tmp_14_days/svensson/mx415.csv"
        statistics.createMXPressCSVFile(proposal, startDate, endDate, fileName)
        self.assertTrue(os.path.exists(fileName))

    def tes_createMXPressCSVFile2(self):
        proposal = "mx1819"
        startDate = datetime.datetime(2017, 2, 10)
        endDate = datetime.datetime(2017, 3, 15)
        fileName = "/tmp_14_days/svensson/mx1819_20170216.csv"
        statistics.createMXPressCSVFile(proposal, startDate, endDate, fileName)
