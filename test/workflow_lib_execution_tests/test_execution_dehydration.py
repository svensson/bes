import os
import json
import time
import shutil
import unittest
import tempfile

from bes.workflow_lib import dehydration


class TestMethods(unittest.TestCase):

    def setUp(self):
        path = os.path.abspath(__file__)
        strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # mxv1ResultCharacterisation_eEDNA
        f = open(
            os.path.join(strTestDataDirectory, "mxv1ResultCharacterisation_eEDNA.xml"),
            "r",
        )
        self.mxv1ResultCharacterisation_eEDNA = f.read()
        f.close()
        # dehydrationResults
        f = open(os.path.join(strTestDataDirectory, "dehydrationResults.txt"), "r")
        self.listDehydrationResults = json.loads(f.read())
        f.close()

    def tes_read_hydration_level(self):
        beamline = "simulator"
        targetRH = 90.0
        currentRH = dehydration.read_hydration_level(beamline, targetRH)
        print(currentRH)

    def tes_set_hydration_level(self):
        beamline = "simulator"
        targetRH = 97.0
        delay = 1
        dehydration.set_hydration_level(beamline, targetRH, delay)
        currentRH = dehydration.read_hydration_level(beamline, targetRH)
        print(currentRH)

    def tes_makePlots(self):
        beamline = "simulator"
        targetRH = 98.0
        currentRH = 98.0
        directory = "/tmp_14_days/svensson"
        workflow_working_dir = tempfile.mkdtemp(
            prefix="dehydrationPlotTest_", dir=directory
        )
        pagePath = dehydration.makePlotsAndWebPage(
            beamline,
            directory,
            workflow_working_dir,
            targetRH,
            currentRH,
            self.listDehydrationResults,
        )
        os.system("ssh -X svensson@rnice8 firefox %s" % pagePath)
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)
