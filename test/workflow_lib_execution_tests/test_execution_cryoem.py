"""
Created on Feb 9, 2015

@author: svensson
"""

import os
import time
import pprint
import unittest
from bes.workflow_lib import cryoem


class Test(unittest.TestCase):

    def tes_processMovie(self):
        beamline = "cm01"
        directory = "/data/visitor/mx415/cm01/20171123/RAW_DATA/testsecretin-grid1/Images-Disc1/GridSquare_23722826/Data"
        template = "FoilHole_23724105_Data_23724797_23724798_20171123_1448-3385.mrc"
        protein = "Protein6"
        sample = "Sample6"
        cwd = os.getcwd()
        doseInitial = 60
        dosePerFrame = 1
        stdout, stderr = cryoem.processMovie(
            beamline,
            directory,
            template,
            protein,
            sample,
            doseInitial,
            dosePerFrame,
            cwd,
            timeout_sec=600,
        )
        print(stdout)
        print(stderr)

    def tes_startCryoEMProcessMovieWF(self):
        beamline = "cm01"
        directory = "/data/visitor/mx415/cm01/20171123/RAW_DATA/testsecretin-grid1/Images-Disc1/GridSquare_23722826/Data"
        template = "FoilHole_23724105_Data_23724797_23724798_20171123_1448-3385.mrc"
        protein = "Protein6"
        sample = "Sample6"
        cwd = os.getcwd()
        doseInitial = 60
        dosePerFrame = 1
        bes_request_id = cryoem.startCryoEMProcessWF(
            beamline,
            directory,
            template,
            protein,
            sample,
            doseInitial,
            dosePerFrame,
            cwd,
            timeout_sec=600,
        )
        print(bes_request_id)
        cryoem.waitTillWFEnd(bes_request_id)

    def test_startCryoEMDataArchive(self):
        beamline = "cm01"
        directory = "/data/visitor/mx2029/cm01/20180216/RAW_DATA/SPEFL_box1_grid1new"
        bes_request_id = cryoem.startCryoEMDataArchive(
            beamline, directory, workflow_name="CryoEMDataArchive"
        )
        print(bes_request_id)
        cryoem.waitTillWFEnd(bes_request_id)

    def tes_findFilesToArchive(self):
        moviePath = "/data/visitor/mx2005/cm01/20171207/RAW_DATA/baseplate-epu-grid2/Images-Disc1/GridSquare_7259758/Data/FoilHole_7268103_Data_7264706_7264707_20171207_1730-10932.mrc"
        listFilesToArchive = cryoem.findFilesToArchive(moviePath)
        pprint.pprint(listFilesToArchive)

    def tes_archiveDataFiles(self):
        #        listFiles = [
        #    "/data/visitor/mx2005/cm01/20171209/RAW_DATA/baseplate-epu-grid2/Images-Disc1/GridSquare_7259648/Data/FoilHole_7265309_Data_7264706_7264707_20171207_1704-10925.mrc",
        #    "/data/visitor/mx2005/cm01/20171209/RAW_DATA/baseplate-epu-grid2/Images-Disc1/GridSquare_7259648/Data/process/FoilHole_7265309_Data_7264706_7264707_20171207_1704-10925/FoilHole_7265309_Data_7264706_7264707_20171207_1704-10925_aligned_mic_DW.mrc",
        #    "/data/visitor/mx2005/cm01/20171209/RAW_DATA/baseplate-epu-grid2/Images-Disc1/GridSquare_7259648/Data/process/FoilHole_7265309_Data_7264706_7264707_20171207_1704-10925/FoilHole_7265309_Data_7264706_7264707_20171207_1704-10925_aligned_mic.mrc",
        #    "/data/visitor/mx2005/cm01/20171209/RAW_DATA/baseplate-epu-grid2/Images-Disc1/GridSquare_7259648/Data/process/FoilHole_7265309_Data_7264706_7264707_20171207_1704-10925/run.log",
        #    "/data/visitor/mx2005/cm01/20171209/RAW_DATA/baseplate-epu-grid2/Images-Disc1/GridSquare_7259648/Data/process/FoilHole_7265309_Data_7264706_7264707_20171207_1704-10925/ctfEstimation.mrc",
        #    ]
        listFiles = [
            "/data/visitor/mx2014/cm01/20171215/RAW_DATA/EPU-gridC/Images-Disc1/GridSquare_15530963/Data/FoilHole_15541090_Data_15537409_15537410_20171215_2220-15472.mrc",
            "/data/visitor/mx2014/cm01/20171215/RAW_DATA/EPU-gridC/Images-Disc1/GridSquare_15530963/Data/process/FoilHole_15541090_Data_15537409_15537410_20171215_2220-15472/FoilHole_15541090_Data_15537409_15537410_20171215_2220-15472_aligned_mic_DW.mrc",
            "/data/visitor/mx2014/cm01/20171215/RAW_DATA/EPU-gridC/Images-Disc1/GridSquare_15530963/Data/process/FoilHole_15541090_Data_15537409_15537410_20171215_2220-15472/FoilHole_15541090_Data_15537409_15537410_20171215_2220-15472_aligned_mic.mrc",
            "/data/visitor/mx2014/cm01/20171215/RAW_DATA/EPU-gridC/Images-Disc1/GridSquare_15530963/Data/process/FoilHole_15541090_Data_15537409_15537410_20171215_2220-15472/run.log",
            "/data/visitor/mx2014/cm01/20171215/RAW_DATA/EPU-gridC/Images-Disc1/GridSquare_15530963/Data/process/FoilHole_15541090_Data_15537409_15537410_20171215_2220-15472/ctfEstimation.mrc",
        ]
        directory = "/data/visitor/mx2014/cm01/20171215/RAW_DATA/EPU-gridC"
        proposal = "id310004"
        sample = "sample1"
        dataSetName = "GridSquare_15530963_{0}".format(round(time.time()))
        cryoem.archiveDataFiles(listFiles, directory, proposal, sample, dataSetName)

    def tes_findAllGridSquareDirectories(self):
        directory = "/data/visitor/mx415/cm01/20171123/RAW_DATA"
        listGridSquare = cryoem.findAllGridSquareDirectories(directory)
        print(listGridSquare)
        print("*" * 80)
        directory = "/data/visitor/mx2023/cm01/20171130/RAW_DATA"
        listGridSquare = cryoem.findAllGridSquareDirectories(directory)
        pprint.pprint(listGridSquare)

    def tes_findAllMovies(self):
        gridSquareDirectory = "/data/visitor/mx2023/cm01/20171130/RAW_DATA/session_mx2023_3/Images-Disc1/GridSquare_1018938"
        listMovies = cryoem.findAllMovies(gridSquareDirectory)
        pprint.pprint(listMovies)

    def tes_updateDictGridSquareForNewMovies(self):
        gridSquareDirectory = "/data/visitor/mx2023/cm01/20171130/RAW_DATA/session_mx2023_3/Images-Disc1/GridSquare_1018938"
        dictGridSquare = {"path": gridSquareDirectory}
        listMovieCreatedSorted, currentMovie = cryoem.updateDictGridSquareForNewMovies(
            dictGridSquare
        )
        pprint.pprint(dictGridSquare)

    def tes_updateDictForNewGridSquares(self):
        directory = "/data/visitor/mx2023/cm01/20171130/RAW_DATA"
        dictGridSquares = {}
        listGridSquareCreatedSorted, currentGridSquare = (
            cryoem.updateDictForNewGridSquares(directory, dictGridSquares)
        )
        pprint.pprint(dictGridSquares)
