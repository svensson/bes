import pprint
import unittest
from bes.workflow_lib import collect
from bes.workflow_lib import kappa_reorientation


class TestMethods(unittest.TestCase):

    def tes_moveKappa(self):
        beamline = "id30a2"
        directory = "/data/id30a2/inhouse/opid30a2/20210517/RAW_DATA"
        motorPositons = collect.readMotorPositions(beamline, token=None)
        pprint.pprint(motorPositons)
        newKappa = 40
        sampx = motorPositons["sampx"]
        sampy = motorPositons["sampy"]
        phiy = motorPositons["phiy"]
        kappa = motorPositons["kappa"]
        kappaPhi = motorPositons["kappa_phi"]
        newMotorPositions = kappa_reorientation.getNewSamplePosition(
            beamline, kappa, kappaPhi, sampx, sampy, phiy, newKappa, kappaPhi
        )
        newMotorPositions["kappa"] = newKappa
        pprint.pprint(newMotorPositions)
        collect.moveMotors(
            beamline, directory, dictPosition=newMotorPositions, token=None
        )
        motorPositons = collect.readMotorPositions(beamline, token=None)
        pprint.pprint(motorPositons)
