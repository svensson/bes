"""
Created on Apr 10, 2014

@author: svensson
"""

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib
import mimetypes

if __name__ == "__main__":

    msg = MIMEMultipart("alternative")
    msg["Subject"] = "Test of sending an image"
    msg["From"] = "workflow@esrf.fr"
    msg["To"] = "svensson@esrf.fr"
    msg.preamble = "Testing to send an image"

    text = MIMEText("test")
    msg.attach(text)  # msg.attach(MIMEText(file("text.txt").read()))
    #     msg.attach(MIMEImage(file("/data/scisoft/dawn/mx/workflow/mx_submodels/tests/data/grid.png").read()))
    image_path = "/data/scisoft/dawn/mx/workflow/mx_submodels/tests/data/grid.png"
    ctype, encoding = mimetypes.guess_type(image_path)
    print(ctype, encoding)
    maintype, subtype = ctype.split("/", 1)
    print(maintype, subtype)
    fp = open("/data/scisoft/dawn/mx/workflow/mx_submodels/tests/data/grid.png", "rb")
    img = MIMEImage(fp.read(), _subtype=subtype)
    fp.close()
    msg.attach(img)  # to send
    #     print msg
    mailer = smtplib.SMTP()
    mailer.connect()
    mailer.sendmail("workflow@esrf.fr", "svensson@esrf.fr", msg.as_string())
    mailer.close()
