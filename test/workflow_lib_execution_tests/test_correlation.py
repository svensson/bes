import numpy
import matplotlib.pyplot as plt


def gaussian(x, mu, sig):
    return numpy.exp(-numpy.power(x - mu, 2.0) / (2 * numpy.power(sig, 2.0)))


shift = 0.5
size = 100
xmin = -20.0
xmax = 20.0
x = numpy.linspace(xmin, xmax, size)
print(x)
arr1 = gaussian(x, -shift / 2.0, 3)
print(arr1)
plt.plot(x, arr1, "o")
arr2 = gaussian(x, shift / 2.0, 3)
plt.plot(x, arr2, "o")
print(arr2)
corr = numpy.correlate(arr1, arr2, mode="full")
shift = -(len(corr) / 2 - numpy.argmax(corr)) * (xmin - xmax) / size
print(shift)
plt.show()
