"""
Created on Oct 29, 2014

@author: opid30
"""

import unittest
import subprocess
import logging

logging.basicConfig(level=logging.DEBUG)


class Test(unittest.TestCase):

    def test_runLauncher(self):
        args = "autoPROCLauncher -workingDir /tmp_14_days/is4a/tmpcxltl9fp -mode after -datacollectionID 2345392 -reprocess True -do_anom false".split(
            " "
        )
        logging.info("Start!")
        process = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        listLines = process.stdout.decode("utf-8").split("\n")
        for line in listLines:
            logging.info(line)
            if "Script dir:" in line:
                _ = line.split("Script dir:")[1].strip()
            if "Job id:" in line:
                _ = int(line.split("Job id:")[1].strip())
