"""
Created on Feb 9, 2015

@author: svensson
"""

import pprint
import unittest
from bes.workflow_lib import dialog


class Test(unittest.TestCase):

    def test_dialog(self):
        characterisationExposureTime = 1.0
        osc_range = 2.0
        transmission = 29.6
        resolution = 2.1
        listDialog = [
            {
                "variableName": "no_reference_images",
                "label": "Number of reference images",
                "type": "int",
                "defaultValue": 2,
                "unit": "",
                "lowerBound": 1,
                "upperBound": 4,
            },
            {
                "variableName": "angle_between_reference_images",
                "label": "Angle between reference images",
                "type": "combo",
                "defaultValue": "90",
                "textChoices": ["30", "45", "60", "90"],
            },
            {
                "variableName": "characterisationExposureTime",
                "label": "Characterisation exposure time",
                "type": "float",
                "value": characterisationExposureTime,
                "unit": "%",
                "lowerBound": 0.0,
                "upperBound": 100.0,
            },
            {
                "variableName": "osc_range",
                "label": "Total oscillation range",
                "type": "float",
                "value": osc_range,
                "unit": "%",
                "lowerBound": 0.1,
                "upperBound": 10.0,
            },
            {
                "variableName": "transmission",
                "label": "Transmission",
                "type": "float",
                "value": transmission,
                "unit": "%",
                "lowerBound": 0,
                "upperBound": 100.0,
            },
            {
                "variableName": "resolution",
                "label": "Resolution",
                "type": "float",
                "defaultValue": resolution,
                "unit": "A",
                "lowerBound": 0.5,
                "upperBound": 7.0,
            },
            {
                "variableName": "do_data_collect",
                "label": "Do data collection?",
                "type": "combo",
                "textChoices": ["true", "false"],
                "value": "false",
            },
        ]
        dictValues = dialog.openDialog(
            "simulator_mxcube", listDialog, automaticMode=False
        )
        pprint.pprint(dictValues)
