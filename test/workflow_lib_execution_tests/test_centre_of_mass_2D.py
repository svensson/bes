"""
Created on Mar 31, 2014

@author: svensson
"""

import os
import json

# import matplotlib.pyplot as pyplot
from bes.workflow_lib import grid

if __name__ == "__main__":
    path = os.path.abspath(__file__)
    strTestDataDirectory = os.path.join(os.path.dirname(os.path.dirname(path)), "data")
    f = open(os.path.join(strTestDataDirectory, "meshResults2D_10x10_test.json"), "r")
    strData = f.read()
    meshPositions = json.loads(strData)
    f.close()
    grid_info = {
        "x1": 0.091331999999999997,
        "angle": 0.0,
        "dy_mm": 0.113987997174263,
        "y1": -0.062726999999999991,
        "dx_mm": 0.17770799994468689,
        "steps_y": 10,
        "steps_x": 10,
    }
    grid.find_best_position(grid_info, meshPositions)
