import os
import json
import pprint
import unittest
import tempfile
import requests

from bes.workflow_lib import meshbest


class TestMethods(unittest.TestCase):

    def test_meshbest(self):
        jsonFile = "/data/visitor/mx1977/id30a3/20180826/PROCESSED_DATA/Alzari/OdhA/OdhA-12749_E12_3_oil/MeshResults/MeshResults_20180826-160918.json"
        plotDir = tempfile.mkdtemp(prefix="MeshBest_")
        print(plotDir)
        os.chdir(plotDir)
        jsondata = meshbest.algorithms.xraycentering(jsonFile, plotDir)
        pprint.pprint(jsondata)

    def getToken(self, userName, password):
        token = None
        ispybWebServiceURL1 = os.path.join(
            "http://ispyb:8080/ispyb", "ispyb-ws", "rest", "authenticate?site='ESRF'"
        )
        dictUser = {"login": userName, "password": password}
        print(dictUser)
        r = requests.post(ispybWebServiceURL1, dictUser)
        if r.status_code == 200:
            dictResponse = json.loads(r.text)
            token = dictResponse["token"]
        return token


if __name__ == "__main__":
    unittest.main()
