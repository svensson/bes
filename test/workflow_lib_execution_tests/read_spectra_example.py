import time
import numpy as np
import matplotlib.pyplot as plt
import json
import xmlrpclib


def plot_spectra(allspectra, figfilename):
    n = len(allspectra)

    for i in range(n):
        npspectra = np.asarray(allspectra[i])
        plot_xdata = npspectra[:, 0]
        plot_ydata = npspectra[:, 1]
        plt.plot(
            plot_xdata[5:],
            plot_ydata[5:],
            linewidth=1,
            color=rainbowcolors[ncycles - i],
            label="spectrum #%d" % i,
        )

    plt.xlim([min(plot_xdata[5:]), max(plot_xdata[5:])])
    plt.legend(loc="upper right", shadow=True, prop={"size": 10})
    plt.savefig(figfilename, dpi=80)
    plt.close()


rpc_host = "massif3backup"
rpc_port = 47351
ncycles = 2
rainbowcolors = plt.cm.hsv(np.linspace(0.0, 0.6, ncycles + 1))
serverProxy = xmlrpclib.ServerProxy("http://%s:%d" % (rpc_host, int(rpc_port)))
figfilename = "/tmp/testspc_all.png"
specfilename = "/tmp/testspc_%03d.txt"
allSpectra = None

for i in range(ncycles + 1):
    if i > 0:
        # collect X-ray dataset #i:
        time.sleep(2)

    spectrumString = serverProxy.rpcGetSpectrum()
    spectrum = json.loads(spectrumString)
    npspectra = np.asarray(spectrum)
    npspectra = np.transpose(npspectra)
    if allSpectra is None:
        allSpectra = np.array(npspectra, ndmin=3)
    else:
        allSpectra = np.vstack((allSpectra, np.array(npspectra, ndmin=3)))

    #    np.savetxt(specfilename % i, npspectra, fmt='%.2f', delimiter='\t')
    plot_spectra(allSpectra, figfilename)

np.save(
    "/usr/local/BES/simulator_mxcube/workflow/mx_submodels/tests/data/spectra.npy",
    allSpectra,
)
