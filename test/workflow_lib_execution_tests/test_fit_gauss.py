"""
Created on Feb 19, 2014

@author: svensson
"""

import numpy as np
import scipy.optimize as opt

from pylab import plot
from pylab import axvspan
from pylab import show


def gauss(x, p):  # p[0]==mean, p[1]==stdev, p[2]==amplitude
    return (
        p[2]
        / (p[1] * np.sqrt(2 * np.pi))
        * np.exp(-((x - p[0]) ** 2) / (2 * p[1] ** 2))
    )


# Create some sample data
known_param = np.array([7.0, 1.0, 10.0])
xmin, xmax = 0, 14.0
N = 15
X = np.linspace(xmin, xmax, N)
Y = gauss(X, known_param)

# Add some noise
Y += 2 * np.random.random(N)

# Renormalize to a proper PDF
# Y /= ((xmax-xmin)/N)*Y.sum()

# Fit a guassian
p0 = [0, 1, np.max(Y)]  # Inital guess is a normal distribution


def errfunc(p, x, y):
    return gauss(x, p) - y  # Distance to the target function


p1, success = opt.leastsq(errfunc, p0[:], args=(X, Y))

fit_mu, fit_stdev, fit_ampl = p1

FWHM = 2 * np.sqrt(2 * np.log(2)) * fit_stdev
print("FWHM", FWHM)


plot(X, Y)
plot(X, gauss(X, p1), lw=3, alpha=0.5, color="r")
axvspan(fit_mu - FWHM / 2, fit_mu + FWHM / 2, facecolor="g", alpha=0.5)
show()
