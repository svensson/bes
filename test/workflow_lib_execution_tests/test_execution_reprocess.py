import os
import tempfile
import unittest

from bes.workflow_lib import reprocess


class TestMethods(unittest.TestCase):

    def test_reprocess(self):
        dataCollectionId = 1
        start = 2
        end = 3
        cutoff = 4
        workingDir = None
        logFilePath = tempfile.mktemp(prefix="Reprocess_", suffix=".log")
        os.environ["ISPyB_user"] = "user"
        os.environ["ISPyB_pass"] = "pass"
        reprocessScriptPath = reprocess.prepareReprocessScript(
            autoProcProgram="EDNA_proc",
            dataCollectionId=dataCollectionId,
            startImage=start,
            endImage=end,
            resolutionCutoff=cutoff,
            workingDir=workingDir,
            logFilePath=logFilePath,
        )
        print(reprocessScriptPath)
