import os
import json
import pprint
import unittest

from bes.workflow_lib import grid
from bes.workflow_lib import edna2_tasks


class TestMethods(unittest.TestCase):

    def setUp(self):
        path = os.path.abspath(__file__)
        strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # Test reference image collection
        f = open(os.path.join(strTestDataDirectory, "reference_strategy.xml"), "r")
        self.referenceStrategy = f.read()
        f.close()
        # Test 'normal' data collection
        f = open(os.path.join(strTestDataDirectory, "burn_strategy.xml"), "r")
        self.mxv1StrategyResult = f.read()
        f.close()
        # Burn processing
        # f = open(os.path.join(strTestDataDirectory, "ControlCharacterisationv1_4_dataOutput.xml"), "r")
        # self.mxv1ResultCharacterisation = f.read()
        # f.close()
        # mxv2ResultCharacterisation
        f = open(
            os.path.join(strTestDataDirectory, "mxv2ResultCharacterisation.xml"), "r"
        )
        self.mxv2ResultCharacterisation = f.read()
        f.close()

    def tes_storeImageQualityIndicatorsFromMeshPositions_1D(self):
        workflow_working_dir = None
        beamline = "simulator"
        directory = "/data/scisoft/pxsoft/data/Mesh/opid231/20130507/RAW_DATA/mesh003"
        expTypePrefix = "mesh-"
        prefix = "test"
        suffix = "cbf"
        run_number = 2
        motorPositions = {
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "sampx": 0.0,
            "sampy": 0.0,
            "phix": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "focus": 0.0,
            "chi": 0.0,
        }
        osc_range = 0.1
        grid_info = json.loads(
            """{"angle": 0.0,"dx_mm": 0.10000000149011612,"dy_mm": 0.10999999940395355,"steps_x": 11,"steps_y": 1,"x1": -0.10361557403028013,"y1": -0.062145366117265601}"""
        )
        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositions,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )

        edPlugin = edna2_tasks.storeImageQualityIndicatorsFromMeshPositionsStart(
            beamline, directory, meshPositions, workflow_working_dir, grid_info
        )
        xsDataResult = (
            edna2_tasks.storeImageQualityIndicatorsFromMeshPositionsSynchronize(
                edPlugin, meshPositions
            )[0]
        )
        pprint.pprint(xsDataResult)

    def tes_ice_rings(self):
        workflow_working_dir = "/tmp_14_days/svensson"
        meshPositions = json.loads(
            open(
                "/data/id30b/inhouse/opid30b/20231211/PROCESSED_DATA/Sample-1-2-01/MeshScan_01/Workflow_20231211-154003/meshPositionsBefore_6va2kaks.json"
            ).read()
        )
        outData = json.loads(
            open(
                "/data/id30b/inhouse/opid30b/20231211/PROCESSED_DATA/Sample-1-2-01/MeshScan_01/Workflow_20231211-154003/ImageQualityIndicators_mr3li1fz/outDataImageQualityIndicators.json"
            ).read()
        )
        resultMeshPositions, inputDozor = (
            edna2_tasks.convertXSDataImageQualityIndicatorsToDict(
                meshPositions, outData
            )
        )
        grid_info = {
            "t": "G",
            "id": "G1",
            "name": "Grid-1",
            "state": "SAVED",
            "label": "Grid",
            "screen_coord": [309.90397805212615, 154.51303155006858],
            "selected": True,
            "refs": [],
            "width": 644.7504479140006,
            "height": 723.3785513181468,
            "cell_count_fun": "zig-zag",
            "cell_h_space": 0,
            "cell_height": 30,
            "cell_v_space": 0,
            "cell_width": 30,
            "num_cols": 41,
            "num_rows": 46,
            "result": [],
            "pixels_per_mm": [524.1873560276426, 524.1873560276426],
            "beam_pos": [640, 512],
            "beam_width": 0.03,
            "beam_height": 0.03,
            "hide_threshold": 5,
            "motor_positions": {
                "phi": 319.99960248438583,
                "focus": None,
                "phiz": 0.10202273807010087,
                "phiy": -2.3596244082986346,
                "zoom": None,
                "sampx": -0.5650008370958821,
                "sampy": 0.5891048536886773,
                "kappa": None,
                "kappa_phi": None,
            },
            "x1": -0.6147290809327848,
            "y1": -0.6669831961591222,
            "steps_x": 41,
            "steps_y": 46,
            "dx_mm": 1.2000000000000002,
            "dy_mm": 1.3499999999999999,
            "angle": 0,
        }
        from bes.workflow_lib import grid

        plot_ice_rings_file = grid.create_ice_rings_plot_file(
            grid_info, resultMeshPositions, workflow_working_dir
        )
        plot_ice_rings_file_image = grid.create_ice_rings_image(
            grid_info,
            resultMeshPositions,
            [],
            workflow_working_dir,
            100000.0,
            useDozor=True,
        )
        plot_file = grid.create_plot_file(
            grid_info,
            resultMeshPositions,
            [],
            workflow_working_dir,
            100000.0,
            useDozor=True,
        )

        with open(os.path.join(workflow_working_dir, "meshPositions.json"), "w") as f:
            f.write(json.dumps(resultMeshPositions, indent=4))
        print(plot_ice_rings_file)
        print(plot_ice_rings_file_image)
        print(plot_file)

    def tes_ice_rings_2(self):
        workflow_working_dir = "/tmp_14_days/svensson/plot2"
        os.mkdir(workflow_working_dir)
        meshPositions = json.loads(
            open(
                "/data/id30b/inhouse/opid30b/20231211/PROCESSED_DATA/Sample-1-2-01/MeshScan_02/Workflow_20231211-171421/meshPositionsBefore_qjx2njtr.json"
            ).read()
        )
        outData = json.loads(
            open(
                "/data/id30b/inhouse/opid30b/20231211/PROCESSED_DATA/Sample-1-2-01/MeshScan_02/Workflow_20231211-171421/outDataImageQualityIndicators.json"
            ).read()
        )
        resultMeshPositions, inputDozor = (
            edna2_tasks.convertXSDataImageQualityIndicatorsToDict(
                meshPositions, outData
            )
        )
        grid_info = {
            "t": "G",
            "id": "G1",
            "name": "Grid-1",
            "state": "SAVED",
            "label": "Grid",
            "screen_coord": [302.00274348422494, 197.53086419753086],
            "selected": True,
            "refs": [],
            "width": 691.9273099564883,
            "height": 707.6529306373176,
            "cell_count_fun": "zig-zag",
            "cell_h_space": 0,
            "cell_height": 30,
            "cell_v_space": 0,
            "cell_width": 30,
            "num_cols": 44,
            "num_rows": 45,
            "result": [],
            "pixels_per_mm": [524.1873560276426, 524.1873560276426],
            "beam_pos": [640, 512],
            "beam_width": 0.03,
            "beam_height": 0.03,
            "hide_threshold": 5,
            "motor_positions": {
                "phi": 339.99988100000337,
                "focus": None,
                "phiz": 0.10202273807010087,
                "phiy": -2.3647244862011876,
                "zoom": None,
                "sampx": -0.30510618135244916,
                "sampy": 0.2577224144016794,
                "kappa": None,
                "kappa_phi": None,
            },
            "x1": -0.6298023834019205,
            "y1": -0.584917438271605,
            "steps_x": 44,
            "steps_y": 45,
            "dx_mm": 1.29,
            "dy_mm": 1.32,
            "angle": 0,
        }
        from bes.workflow_lib import grid

        plot_ice_rings_file = grid.create_ice_rings_plot_file(
            grid_info, resultMeshPositions, workflow_working_dir
        )
        plot_ice_rings_file_image = grid.create_ice_rings_image(
            grid_info,
            resultMeshPositions,
            [],
            workflow_working_dir,
            100000.0,
            useDozor=True,
        )
        plot_file = grid.create_plot_file(
            grid_info,
            resultMeshPositions,
            [],
            workflow_working_dir,
            100000.0,
            useDozor=True,
        )

        with open(os.path.join(workflow_working_dir, "meshPositions.json"), "w") as f:
            f.write(json.dumps(resultMeshPositions, indent=4))
        print(plot_ice_rings_file)
        print(plot_ice_rings_file_image)
        print(plot_file)
