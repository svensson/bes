import unittest

from bes.workflow_lib import autoprocessing


class TestMethods(unittest.TestCase):

    def test_startAutoProcessing(self):
        beamline = "id30a2"
        firstImagePath = "/data/visitor/mx415/id30a2/20170124/RAW_DATA/t6/t6_1_0001.cbf"
        autoProcessingChoice = "Grenades_fastproc+EDNA_proc"
        autoprocessing.startAutoProcessing(
            beamline, firstImagePath, autoProcessingChoice
        )
