"""
Created on Feb 9, 2015

@author: svensson
"""

import pprint
import unittest
from bes.workflow_lib import ispyb


class Test(unittest.TestCase):

    def tes_getTransport(self):
        transport = ispyb.getTransport()
        self.assertTrue(transport is not None)

    def tes_getWdslRoot(self):
        beamline = "simulator"
        wdslRoot = ispyb.getWdslRoot(beamline)
        self.assertTrue("ispyb-valid" in wdslRoot)

    def test_findPersonByProposal(self):
        beamline = "id30a1"
        proposalCode = "mx"
        proposalNumber = 2501
        person = ispyb.findPersonByProposal(beamline, proposalCode, proposalNumber)
        pprint.pprint(person)
        self.assertTrue(person is not None)

    def tes_findDataCollectionFromDataCollectionId(self):
        beamline = "id30a3"
        dataCollectionId = "2568050"
        dataCollectionWS3VO = ispyb.findDataCollection(beamline, dataCollectionId)
        print(dataCollectionWS3VO)
        self.assertTrue(dataCollectionWS3VO is not None)

    def tes_findDataCollectionFromFileLocationAndFileName(self):
        beamline = "id30a3"
        firstImagePath = "/data/visitor/mx2271/id30a3/20210217/RAW_DATA/fge-wt/h2-85/jk037_1/ref-jk037_1_1_0004.h5"
        # firstImagePath = "/data/id30a3/inhouse/opid30a3/20200727/RAW_DATA/Sample-6-1-06/MeshScan_01/mesh-local-user_0_0001.h5"
        dataCollectionWS3VO = ispyb.findDataCollectionFromFileLocationAndFileName(
            beamline, firstImagePath
        )
        dataCollectionId = dataCollectionWS3VO.dataCollectionId
        dataCollectionGroupId = dataCollectionWS3VO.dataCollectionGroupId
        print(dataCollectionWS3VO)
        print(dataCollectionId)
        print(dataCollectionGroupId)
        self.assertTrue(dataCollectionWS3VO is not None)

    def tes_findDataCollectionGroup(self):
        beamline = "simulator"
        dataCollectionGroupId = 1335127
        dataCollectionGroupWS3VO = ispyb.findDataCollectionGroup(
            beamline, dataCollectionGroupId
        )
        self.assertTrue(dataCollectionGroupWS3VO is not None)

    def tes_storeOrUpdateDataCollectionGroup(self):
        beamline = "simulator"
        dataCollectionId = 1335127
        dataCollectionGroupWS3VO = ispyb.findDataCollectionGroup(
            beamline, dataCollectionId
        )
        dataCollectionGroupWS3VO.comments = "Test comment."
        print(dataCollectionGroupWS3VO)
        dataCollectionGroupId = ispyb.storeOrUpdateDataCollectionGroup(
            beamline, dataCollectionGroupWS3VO
        )
        print(dataCollectionGroupId)

    def tes_updateDataCollectionComment(self):
        beamline = "simulator_mxcube"
        filePath = "/data/visitor/mx415/id30a2/20170505/RAW_DATA/mx415_1_0001.cbf"
        # newComment = "New comment 2."
        newComment = str(range(2))
        print(len(newComment))
        dataCollectionId = ispyb.updateDataCollectionComment(
            beamline, filePath, newComment
        )
        print(dataCollectionId)

    def tes_updateDataCollectionGroupComment(self):
        beamline = "id30a2"
        filePath = "/data/id30a2/inhouse/opid30a2/20210531/RAW_DATA/EnhancedCharacterisation_01/ref-opid30a2_1_0001.cbf"
        # filePath = "/data/visitor/mx2269/id30a1/20210602/RAW_DATA/LrpB/LrpB-SrA/MXPressA_01/mesh-LrpB-SrA_0_0001.cbf"
        newComment = "New comment."
        print(len(newComment))
        dataCollectionGroupId = ispyb.updateDataCollectionGroupComment(
            beamline, filePath, newComment
        )
        print(dataCollectionGroupId)

    def tes_storeOrUpdateWorkflow(self):
        beamline = "simulator"
        workflowStepID = ispyb.storeOrUpdateWorkflow(
            beamline,
            workflowLogFile="/tmp/pyarchHtmlDir",
            workflowPyarchLogFile="/tmp/pyarchLogFile",
            workflowId=None,
            workflowTitle="Workflow title",
            workflowType="TroubleShooting",
            comments="Bla bla",
        )
        print(workflowStepID)

    def tes_storeWorkflowStep(self):
        beamline = "simulator"
        workflowId = 1
        workflowStepType = "Test step"
        #        folderPath = ""
        #        imageResultFilePath = ""
        #        htmlResultFilePath = ""
        #        resultFilePath = ""
        #        comments = ""
        workflowStepID = ispyb.storeWorkflowStep(beamline, workflowId, workflowStepType)
        print(workflowStepID)

    def tes_findDataCollection(self):
        beamline = "id30a2"
        dataCollectionId = 1775330
        dataCollection = ispyb.findDataCollection(beamline, dataCollectionId)
        pprint.pprint(dataCollection)

    def tes_findProposalByCodeAndNumber(self):
        beamline = "simulator"
        code = "mx"
        number = 1743
        proposal = ispyb.findProposalByCodeAndNumber(beamline, code, number)
        pprint.pprint(proposal)

    def test_findSessionsByProposalAndBeamLine(self):
        beamline = "ID30A-3"
        code = "mx"
        number = "2603"
        proposal = ispyb.findSessionsByProposalAndBeamLine(code, number, beamline)
        pprint.pprint(proposal)

    def test_findPersonByProteinAcronym(self):
        beamline = "id30a1"
        #        beamline = "id30a1"
        code = "mx"
        number = "2501"
        acronym = "FabB"
        person = ispyb.findPersonByProteinAcronym(beamline, code, number, acronym)
        pprint.pprint(person)

    def tes_findDetector(self):
        beamline = "id23eh2"
        manufacturer = "Dectris"
        model = "Pilatus3_2M"
        mode = "Unbinned"
        detectorId = ispyb.findDetector(beamline, type, manufacturer, model, mode)
        print(detectorId)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_getTransport']
    unittest.main()
