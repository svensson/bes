import unittest
from bes.workflow_lib import machineInfo


class TestMethods(unittest.TestCase):

    def test_current(self):
        current = machineInfo.readMachineCurrent("id30b")
        print(current)

    def test_feStatus(self):
        feState = machineInfo.readFeState("id30b")
        print(feState)

    def test_readAutoModeTime(self):
        autoModeTime = machineInfo.readAutoModeTime("id30b")
        print(autoModeTime / 3600.0)

    def test_readFillingMode(self):
        fillingMode = machineInfo.readFillingMode("id30b")
        print(fillingMode)

    def test_checkFeBeamStatus(self):
        feBeamStatus = machineInfo.checkFeBeamStatus("id30b")
        print(feBeamStatus)

    def test__checkFeBeamStatus(self):
        machineCurrent = 180.0
        feState = "FE open"
        fillingMode = "24*8+1bunch"
        self.assertTrue(
            machineInfo._checkFeBeamStatus(machineCurrent, feState, fillingMode)
        )
        machineCurrent = 180.0
        feState = "FE closed"
        fillingMode = "24*8+1bunch"
        self.assertFalse(
            machineInfo._checkFeBeamStatus(machineCurrent, feState, fillingMode)
        )
        machineCurrent = 100.0
        feState = "FE open"
        fillingMode = "24*8+1bunch"
        self.assertFalse(
            machineInfo._checkFeBeamStatus(machineCurrent, feState, fillingMode)
        )

    def test_correctExposureTimeForFillingMode(self):
        exposureTime = 100.0
        newExposureTime = machineInfo.correctExposureTimeForFillingMode(
            "id30b", exposureTime
        )
        print(newExposureTime)
        newExposureTime = machineInfo.correctExposureTimeForFillingMode(
            "id30a1", exposureTime
        )
        print(newExposureTime)
