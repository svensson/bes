import os
import json
import pprint
import unittest

from bes.workflow_lib import collect


class TestMethods(unittest.TestCase):
    def setUp(self):
        path = os.path.abspath(__file__)
        strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # Test reference image collection
        f = open(os.path.join(strTestDataDirectory, "burn_strategy.xml"), "r")
        self.mxv1StrategyResult = f.read()
        f.close()
        f = open(os.path.join(strTestDataDirectory, "EDNAOutput_1718380.xml"), "r")
        self.mxv1StrategyResult_1718380 = f.read()
        f.close()

    def tes_get_beamsize(self):
        beamline = "simulator"
        print(collect.getBeamSize(beamline))

    def tes_displayNewHtmlPage(self):
        beamline = "simulator"
        collect.displayNewHtmlPage(beamline, "/tmp/dummy/path.hmtl", "DummyName", "99")
        # Test with "real" web page
        strHtmlPath = "/data/id14eh1/inhouse/opid141/20130424/PROCESSED_DATA/1-1/characterisation-1/EDApplication_20130424-100154/ControlInterfaceToMXCuBEv1_3/SimpleHTML/index.html"
        collect.displayNewHtmlPage(beamline, strHtmlPath, "DummyName", "99")

    def tes_getGridInfo(self):
        beamline = "simulator"
        shape = "G2"
        grid_info = collect.getGridInfo(beamline, shape=shape)
        pprint.pprint(grid_info)

    def tes_setGridData(self):
        beamline = "simulator"
        grid_info = collect.getGridInfo(beamline)
        # Test mesh data
        path = os.path.abspath(__file__)
        strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        f = open(os.path.join(strTestDataDirectory, "meshResults2D.json"), "r")
        strData = f.read()
        meshPositions2D = json.loads(strData)
        f.close()
        collect.setGridData(beamline, grid_info, meshPositions2D)

    def test_loadQueue(self):
        beamline = "id30a2"
        mxv1StrategyResult = self.mxv1StrategyResult_1718380
        run_number = 1
        expTypePrefix = "ref-"
        prefix = "test_queue"
        directory = "/data/id30a2/inhouse/opid30a2/20220322/RAW_DATA"
        process_directory = "/data/id30a2/inhouse/opid30a2/20220322/PROCESSED_DATA"
        workflow_type = "Kappa"
        resolution = 2.0
        motorPositions = {
            "sampx": 0.1,
            "sampy": 0.2,
            "phiy": 0.3,
            "phiz": 0.4,
        }
        workflow_index = 1
        sample_node_id = 1
        enabled = False
        group_node_id = collect.createNewDataCollectionGroup(
            beamline, workflow_type, workflow_index, enabled, sample_node_id
        )
        print(group_node_id)
        collect.loadQueue(
            beamline=beamline,
            directory=directory,
            process_directory=process_directory,
            prefix=prefix,
            expTypePrefix=expTypePrefix,
            run_number=run_number,
            resolution=resolution,
            mxv1StrategyResult=mxv1StrategyResult,
            motorPositions=motorPositions,
            workflow_type=workflow_type,
            workflow_index=workflow_index,
            group_node_id=group_node_id,
            sample_node_id=sample_node_id,
            enabled=enabled,
        )

    def test_executeQueueForMeshScan(self):
        beamline = "id30b"
        sample_name = "t1"
        raw_data_directory = "/data/id30b/inhouse/opid30b/20220711/RAW_DATA"
        prefix = "{0}-{0}".format(sample_name)
        directory = "{0}/{1}/{2}/Chipx_test_01".format(
            raw_data_directory, sample_name, prefix
        )
        process_directory = raw_data_directory.replace("RAW_DATA", "PROCESSED_DATA")
        queueEntry = {
            "fileData": {
                "directory": directory,
                "expTypePrefix": "mesh-",
                "firstImage": 1,
                "prefix": prefix,
                "process_directory": process_directory,
                "runNumber": 1,
            },
            "motorPositions4dscan": {
                "phiy_s": -80.958,
                "phiz_s": 0.076,
                "sampx_s": 2.064,
                "sampy_s": -2.061,
                "phiy_e": -41.270,
                "phiz_e": 0.076,
                "sampx_e": 2.066,
                "sampy_e": -2.035,
            },
            "subWedgeData": {
                "doProcessing": False,
                "exposureTime": 0.1,
                "noImages": 210,
                "oscillationWidth": 0.1,
                "rotationAxisStart": 296,
                "transmission": 11.5,
            },
        }

        workflowParameters = {"index": 1, "type": "ChipxTest"}
        groupNodeId = None
        sampleNodeId = 1
        resolution = 3.359
        motorPositions = collect.readMotorPositions(beamline)
        force_not_shutterless = False
        _ = collect.executeQueueForMeshScan(
            beamline,
            workflowParameters["index"],
            workflowParameters["type"],
            [queueEntry],
            groupNodeId,
            sampleNodeId,
            resolution,
            motorPositions,
            force_not_shutterless=force_not_shutterless,
        )

    def tes_executeQueueForFastMeshScan(self):
        beamline = "id30a2"
        sampleName = "t6"
        directory = "/data/id30a2/inhouse/opid30a2/20190307/RAW_DATA/{0}/{0}-{0}/MeshScan_01".format(
            sampleName
        )
        dict_positions = collect.readMotorPositions(beamline, token=None)
        sampleName = "t6"
        queueEntry = {
            "fileData": {
                "directory": directory,
                "expTypePrefix": "mesh-",
                "firstImage": 1,
                "prefix": "{0}-{0}".format(sampleName),
                "process_directory": directory.replace("RAW_DATA", "PROCESSED_DATA"),
                "runNumber": 1,
            },
            "subWedgeData": {
                "exposureTime": 0.1,
                "transmission": 2.5,
                "rotationAxisStart": dict_positions["phi"],
                "noImages": 10,
                "doProcessing": False,
                "oscillationWidth": 0.1,
            },
            "newMotorPositions": dict_positions,
        }
        noLines = 8
        meshRange = {"horizontal_range": 0.5, "vertical_range": 0.5}
        workflowParameters = {"index": 1, "type": "XrayCentering"}
        groupNodeId = None
        sampleNodeId = 1
        resolution = 3.0
        _ = collect.executeQueueForFastMeshScan(
            beamline,
            workflowParameters["index"],
            workflowParameters["type"],
            [queueEntry],
            groupNodeId,
            sampleNodeId,
            resolution,
            dict_positions,
            noLines,
            meshRange,
        )

    def tes_addCentringToQueue(self):
        beamline = "simulator_mxcube"
        workflow_type = "Kappa"
        workflow_index = 17
        group_node_id = None
        sample_node_id = 1
        collect.addCentringToQueue(
            beamline,
            workflow_type,
            workflow_index,
            group_node_id,
            sample_node_id,
            enabled=False,
            message="Center sample",
        )

    def test_readMotorPositions(self):
        beamline = "id30a2"
        dict_positions = collect.readMotorPositions(beamline)
        print(dict_positions)

    def test_moveMotors(self):
        beamline = "id30a2"
        directory = "/data/id30a2/inhouse/opid30a2/20200817/RAW_DATA/t1"
        collect.moveMotors(beamline, directory, {"phi": 0.0})
        # collect.moveMotors(beamline, directory, {"sampx": "0.2"})

    def tes_getEnergy(self):
        beamline = "id30a2"
        print(collect.getEnergy(beamline))
        print(12.398 / collect.getEnergy(beamline))

    def test_getResolution(self):
        beamline = "id30a1"
        print(collect.getResolution(beamline))

    def test_getTransmission(self):
        beamline = "id30a1"
        print(collect.getTransmission(beamline))

    def tes_moveSampleOrthogonallyToRotationAxis(self):
        beamline = "simulator"
        directory = "/data/visitor/mx415/id30a1/20140918/RAW_DATA"
        distance = 0.05
        collect.moveSampleOrthogonallyToRotationAxis(beamline, directory, distance)

    def tes_movePhiAndSample(self):
        beamline = "simulator"
        directory = "/data/visitor/mx415/id30a1/20140918/RAW_DATA"
        distance = 0.05
        motorPositions = collect.readMotorPositions(beamline, token=None)
        newMotorPositions = {}
        newMotorPositions["phiz"] = motorPositions["phiz"] + distance
        collect.moveMotors(beamline, directory, newMotorPositions)
        collect.moveSampleOrthogonallyToRotationAxis(beamline, directory, -distance)

    def tes_saveSnapshot(self):
        beamline = "id30a2"
        # snapShotFilePath = "/data/id30a1/inhouse/opid30a1/snapshots/background/snapshot_background_20141112.png"
        snapShotFilePath = "/tmp/test1.png"
        collect.saveSnapshot(beamline, snapShotFilePath)
        # im = imageio.v2.imread(snapShotFilePath, as_gray=True)
        # numpy.save("/data/id30a1/inhouse/opid30a1/snapshots/background/snapshot_background_20141112.npy", im)

    def tes_takeSnapshots(self):
        beamline = "simulator"
        directory = "/data/id30a1/inhouse/opid30a1/20141104/RAW_DATA"
        background_image = "/data/id30a1/inhouse/opid30a1/snapshots/background/snapshot_background_20141104_light0_10.png"
        collect.takeSnapshots(beamline, directory, background_image)

    def tes_totalMesh(self):
        beamline = "simulator"
        directory = "/data/id30a1/inhouse/opid30a1/20141103/RAW_DATA"
        processed_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")
        if not os.path.exists(processed_directory):
            os.makedirs(processed_directory, 0o755)
        collect.moveMotors(beamline, directory, {"zoom": 2, "focus": -1.3})
        background_image = "/data/id30a1/inhouse/opid30a1/snapshots/background/snapshot_background_20141104_light1_00.png"
        #         if not os.path.exists(background_image):
        #             motorPositions = collect.readMotorPositions(beamline, token=token)
        #             # Move phiy relative 1.5 mm for background snapshot
        #             phiyOrig = motorPositions["phiy"]
        #             phiyNew = phiyOrig + 1.5
        #             collect.moveMotors(beamline, directory, {"phiy": phiyNew})
        #             collect.saveSnapshot(beamline, background_image)
        #             collect.moveMotors(beamline, directory, {"phiy": phiyOrig})
        snapshotDir = collect.takeSnapshots(beamline, directory, background_image)
        from bes.workflow_lib import grid

        (
            angleMinThickness,
            x1Pixels,
            y1Pixels,
            dxPixels,
            dyPixels,
            deltaPhiz,
            stdPhiz,
            autoMeshWorkingDir,
        ) = grid.autoMesh(snapshotDir, processed_directory, debug=True)
        print("deltaPhiz: %.4f, stdPhiz: %.4f" % (deltaPhiz / 420.0, stdPhiz / 420.0))

    def tes_phizCalib(self):
        from bes.workflow_lib import grid

        beamline = "simulator"
        directory = "/data/id30a1/inhouse/opid30a1/20141103/RAW_DATA"
        processed_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")
        resultFileName = "Loop1c.txt"
        resultFile = open(os.path.join(processed_directory, resultFileName), "w")
        if not os.path.exists(processed_directory):
            os.makedirs(processed_directory, 0o755)
        zoomLevel = 2
        collect.moveMotors(beamline, directory, {"zoom": zoomLevel, "focus": -1.3})
        # Take background image
        backgroundDir = processed_directory
        background_image = os.path.join(
            backgroundDir, "snapshot_background_zoom_%d.png" % zoomLevel
        )
        if not os.path.exists(backgroundDir):
            os.makedirs(backgroundDir, 0o755)
        motorPositions = collect.readMotorPositions(beamline, token=None)
        # Move phiy relative 1.5 mm for background snapshot
        phiyOrig = motorPositions["phiy"]
        phiyNew = phiyOrig + 1.0
        collect.moveMotors(beamline, directory, {"phiy": phiyNew})
        collect.saveSnapshot(beamline, background_image)
        collect.moveMotors(beamline, directory, {"phiy": phiyOrig})
        print(background_image)
        for phiy in [-0.50, -0.45, -0.40, -0.35, -0.30, -0.25, -0.20, -0.15, -0.10]:
            #         for phiy in [-0.05, 0.00, 0.05, 0.10, 0.15, 0.20]:
            collect.moveMotors(beamline, directory, {"phiy": phiy})
            snapshotDir = collect.takeSnapshots(beamline, directory, background_image)
            (
                angleMinThickness,
                x1Pixels,
                y1Pixels,
                dxPixels,
                dyPixels,
                deltaPhiz,
                stdPhiz,
                autoMeshWorkingDir,
            ) = grid.autoMesh(snapshotDir, processed_directory, debug=False)
            print(
                "deltaPhiz: %.4f, stdPhiz: %.4f" % (deltaPhiz / 420.0, stdPhiz / 420.0)
            )
            resultFile.write(
                "%.4f %.4f %.4f\n" % (phiy, deltaPhiz / 420.0, stdPhiz / 420.0)
            )
            resultFile.flush()
        resultFile.close()

    def tes_get_cp(self):
        beamline = "simulator"
        print(collect.get_cp(beamline))

    def tes_saveCurrentPos(self):
        beamline = "simulator"
        collect.saveCurrentPos(beamline)

    def tes_readDiagFile(self):
        diagFilePath = "/tmp_14_days/nurizzo/Diagnostiques/LastDiagnostics.dat"
        _ = collect.readDiagFile(diagFilePath)

    def tes_correctUsingEncoderPositions1D(self):
        directory = (
            "/data/id30a1/inhouse/opid30a1/20141022/RAW_DATA/opid30a1/6-1-9/MXPressE_03"
        )
        path = os.path.abspath(__file__)
        testDataDirectory = os.path.join(os.path.dirname(os.path.dirname(path)), "data")
        f = open(
            os.path.join(testDataDirectory, "id30a1_20141022_verticalScan.json"), "r"
        )
        strData = f.read()
        f.close()
        meshPositions = json.loads(strData)
        _ = collect.correctUsingEncoderPositions(directory, meshPositions)

    def tes_correctUsingEncoderPositions2D(self):
        directory = "/data/id30a1/inhouse/opid30a1/20150119/RAW_DATA/MeshScan_02"
        path = os.path.abspath(__file__)
        testDataDirectory = os.path.join(os.path.dirname(os.path.dirname(path)), "data")
        f = open(
            os.path.join(testDataDirectory, "meshPositionsAxisCentring_zV3kyO.json"),
            "r",
        )
        strData = f.read()
        f.close()
        meshPositions = json.loads(strData)
        newMeshPositions = collect.correctUsingEncoderPositions(
            directory, meshPositions
        )
        pprint.pprint(newMeshPositions)

    def tes_correctUsingEncoderPositions2D_new(self):
        diagFile = "/data/id30a1/inhouse/opid30a1/20211004/RAW_DATA/CD032398_B10-3_5/MXPressR_180_01/LastDiagnostics.dat"
        f = open(
            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20211004/PROCESSED_DATA/CD032398_B10-3_5/MXPressR_180_01/Workflow_20211004-142600/meshPositions2D_moel2r0u.json",
            "r",
        )
        strData = f.read()
        f.close()
        meshPositions = json.loads(strData)
        newMeshPositions = collect.correctUsingEncoderPositions(diagFile, meshPositions)
        pprint.pprint(newMeshPositions)

    def tes_correctUsingEncoderPositions2D_new2(self):
        directory = (
            "/data/id30a1/inhouse/opid30a1/20160323/RAW_DATA/MXPressA_32/MXPressA_01"
        )
        path = os.path.abspath(__file__)
        testDataDirectory = os.path.join(os.path.dirname(os.path.dirname(path)), "data")
        f = open(
            os.path.join(
                testDataDirectory,
                "/data/id30a1/inhouse/opid30a1/20160323/PROCESSED_DATA/MXPressA_32/MXPressA_01/Workflow_20160324-081955/meshPositions2D_a8qDuQ.json",
            ),
            "r",
        )
        strData = f.read()
        f.close()
        meshPositions = json.loads(strData)
        newMeshPositions = collect.correctUsingEncoderPositionsNew(
            directory, meshPositions
        )
        pprint.pprint(newMeshPositions)

    def tes_energyScan(self):
        beamline = "simulator"
        elementSymbol = "Se"
        edgeEnergy = "K"
        workflow_type = None
        sample_node_id = 1
        directory = "/data/visitor/mx415/id23eh1/20141110/RAW_DATA"
        process_directory = "/data/visitor/mx415/id23eh1/20141110/PROCESSED_DATA"
        expTypePrefix = ""
        prefix = "opid23"
        run_number = 1
        workflow_index = 1
        print(
            collect.energyScan(
                beamline,
                directory,
                process_directory,
                expTypePrefix,
                prefix,
                run_number,
                elementSymbol,
                edgeEnergy,
                workflow_type,
                workflow_index,
                sample_node_id,
                enabled=False,
                group_node_id=None,
            )
        )

    def test_getApertureNames(self):
        beamline = "id30b"
        print(collect.getApertureNames(beamline))

    def test_getAperture(self):
        beamline = "id30b"
        print(collect.getApertureSize(beamline))

    def test_setAperture(self):
        # id23eh1 ["10 um", "20 um", "30 um", "50 um", "Outbeam"]
        beamline = "id30b"
        aperture_size = 30
        print(collect.setApertureSize(beamline, aperture_size))

    def tes_rpcGetSpectrum(self):
        host = "massif3backup.esrf.fr"
        port = 47351
        spectrum = collect.rpcGetSpectrum(host, port)
        print(spectrum)

    def tes_getZoomLevel(self):
        beamline = "id30a2"
        zoomLevel = collect.getZoomLevel(beamline)
        print(zoomLevel)

    def tes_setZoomLevel(self):
        beamline = "id23eh2"
        collect.setZoomLevel(beamline, 2)

    def tes_getAvailableZoomLevels(self):
        beamline = "simulator"
        print(collect.getAvailableZoomLevels(beamline))

    def tes_getBackLightLevel(self):
        beamline = "id23eh2"
        print(collect.getBackLightLevel(beamline))
        pass

    def tes_setBackLightLevel(self):
        beamline = "id23eh2"
        collect.setBackLightLevel(beamline, 5)

    def tes_getFrontLightLevel(self):
        beamline = "simulator"
        print(collect.getFrontLightLevel(beamline))

    def tes_setFrontLightLevel(self):
        beamline = "id23eh2"
        collect.setFrontLightLevel(beamline, 0.0)

    def tes_getCurrentCdCrystalId(self):
        beamline = "simulator"
        crystalId = collect.getCurrentCdCrystalId(beamline)
        print(crystalId)

    def tes_centreBeam(self):
        beamline = "simulator"
        collect.centreBeam(beamline)

    def tes_importQueueModelObjects(self):
        beamline = "id30a2"
        token = None
        serverProxy = collect.getServerProxy(beamline, token)
        collect.importQueueModelObjects(beamline, serverProxy)

    def test_getResolutionLimits(self):
        beamline = "id30b"
        lowResolution, highResolution = collect.getResolutionLimits(beamline)
        print(lowResolution, highResolution)

    def tes_take_twelve_snapshots(self):
        beamline = "id30a1"
        snapshots_directory = "/tmp"
        collect.take_twelve_snapshots(beamline, snapshots_directory)

    def tes_getBeamsize(self):
        beamline = "id30b"
        beam_width, beam_height, beam_label = collect.getBeamsize(beamline=beamline)
        print(beam_width, beam_height, beam_label)

    def tes_setBeamsize(self):
        beamline = "id30b"
        beam_label = "A10"
        collect.setBeamsize(beamline=beamline, beam_label=beam_label)

    def tes_getAvailableBeamsize(self):
        beamline = "id30b"
        available_beamsizes = collect.getAvailableBeamsizes(beamline=beamline)
        pprint.pprint(available_beamsizes)
