import unittest
from bes.workflow_lib import stac


class TestMethods(unittest.TestCase):

    def tes_getNewSamplePosition(self):
        beamline = "id23eh1"
        print(stac.getNewSamplePosition(beamline, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0))
        print(stac.getNewSamplePosition(beamline, 0.0, 0.0, 0.0, 0.0, 0.0, 90.0, 0.0))
        print(stac.getNewSamplePosition(beamline, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 180.0))
        print(stac.getNewSamplePosition(beamline, 90.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0))
        print(stac.getNewSamplePosition(beamline, 0.0, 180.0, 0.0, 0.0, 0.0, 0.0, 0.0))
        print(
            stac.getNewSamplePosition(beamline, 11.0, 220.0, 1.0, 2.0, 3.0, 33.0, 44.0)
        )
