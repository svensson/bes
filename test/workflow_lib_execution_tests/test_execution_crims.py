"""
Created on Feb 9, 2015

@author: svensson
"""

import pprint
import unittest
from bes.workflow_lib import crims


class Test(unittest.TestCase):

    def tes_getBarcodeXtalInfos(self):
        url = "https://embl.fr/htxlab/index.php"
        barcode = "CD006710"
        xml = crims.getBarcodeXtalInfos(url, barcode)
        dictXtalInfos = crims.xml2dict(xml)
        pprint.pprint(dictXtalInfos)

    def tes_getImage(self):
        url = "https://embl.fr/htxlab/index.php"
        barcode = "CD006710"
        row = "A"
        column = "03"
        shelf = "2"
        image = crims.getImage(url, barcode, row, column, shelf)
        f = open("/tmp/image.jpg", "w")
        f.write(image)
        f.close()

    def test_sendCrystalDataCollectionIds(self):
        url = "http://htx1.embl.fr:8026/api/v1/ispyb_datacollections"
        crystalUuid = "479b51a0-9d40-45a3-9336-60d4d6ba67df"
        dataCollectionId = "2205402"
        status = crims.sendCrystalDataCollectionIds(url, crystalUuid, dataCollectionId)
        print(status)
