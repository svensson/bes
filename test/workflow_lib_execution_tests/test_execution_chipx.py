import json
import math
import sys
import os.path
import pprint

from bes.workflow_lib import collect

beamline = "id30b"
sample_name = "test6"
runNumber = 1
exposureTime = 0.2  # s
oscillationWidth = 0.1  # degree
transmission = 100.0  # %
resolution = 1.8  # A

raw_data_directory = "/data/id30b/inhouse/opid30b/20220712/RAW_DATA"
raw_process_directory = raw_data_directory.replace("RAW_DATA", "PROCESSED_DATA")
prefix = "{0}-{0}".format(sample_name)
directory = "{0}/{1}/{2}".format(raw_data_directory, sample_name, prefix)
process_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")


def test_save_left_position():
    motorPositions = collect.readMotorPositions(beamline)
    pprint.pprint(motorPositions)
    if not os.path.exists(process_directory):
        os.makedirs(process_directory, exist_ok=True, mode=0o755)
    file_path = os.path.join(process_directory, "left_pos.json")
    print(file_path)
    if os.path.exists(file_path):
        yesno = input("File exists! Do you want to overwrite (yes/no) ?")
        if yesno.lower() != "yes":
            sys.exit(0)
    with open(file_path, "w") as fd:
        fd.write(json.dumps(motorPositions, indent=4))
    print("File {0} written.".format(file_path))


def test_save_right_position():
    motorPositions = collect.readMotorPositions(beamline)
    pprint.pprint(motorPositions)
    if not os.path.exists(process_directory):
        os.makedirs(process_directory, exist_ok=True, mode=0o755)
    file_path = os.path.join(process_directory, "right_pos.json")
    print(file_path)
    if os.path.exists(file_path):
        yesno = input("File exists! Do you want to overwrite (yes/no) ?")
        if yesno.lower() != "yes":
            sys.exit(0)
    with open(file_path, "w") as fd:
        fd.write(json.dumps(motorPositions, indent=4))
    print("File {0} written.".format(file_path))


def test_move_to_left_position():
    left_file_path = os.path.join(process_directory, "left_pos.json")
    with open(left_file_path) as fd:
        left_pos = json.loads(fd.read())
    del left_pos["plate_translation"]
    del left_pos["zoom"]
    del left_pos["focus"]
    pprint.pprint(left_pos)
    collect.moveMotors(beamline, directory, left_pos)


def test_move_to_right_position():
    right_file_path = os.path.join(process_directory, "right_pos.json")
    with open(right_file_path) as fd:
        right_pos = json.loads(fd.read())
    del right_pos["plate_translation"]
    del right_pos["zoom"]
    del right_pos["focus"]
    pprint.pprint(right_pos)
    collect.moveMotors(beamline, directory, right_pos)


def test_collect_data():
    left_file_path = os.path.join(process_directory, "left_pos.json")
    with open(left_file_path) as fd:
        left_pos = json.loads(fd.read())
    right_file_path = os.path.join(process_directory, "right_pos.json")
    with open(right_file_path) as fd:
        right_pos = json.loads(fd.read())
    pprint.pprint(left_pos)
    pprint.pprint(right_pos)
    no_lines = 7
    list_queue_entries = []
    direction = 1
    first_image = 1
    for index_line in range(no_lines):
        delta_h = (index_line - int(no_lines / 2)) * 0.05
        if direction > 0:
            phiy_s = left_pos["phiy"]
            phiz_s = left_pos["phiz"]
            phiy_e = right_pos["phiy"]
            phiz_e = right_pos["phiz"]
            delta_sampx_s = -delta_h * math.sin(math.radians(left_pos["phi"]))
            delta_sampy_s = delta_h * math.cos(math.radians(left_pos["phi"]))
            delta_sampx_e = -delta_h * math.sin(math.radians(right_pos["phi"]))
            delta_sampy_e = delta_h * math.cos(math.radians(right_pos["phi"]))
            sampx_s = left_pos["sampx"] + delta_sampx_s
            sampy_s = left_pos["sampy"] + delta_sampy_s
            sampx_e = right_pos["sampx"] + delta_sampx_e
            sampy_e = right_pos["sampy"] + delta_sampy_e
            rotation_axis_start = round(left_pos["phi"], 2)
        else:
            phiy_s = right_pos["phiy"]
            phiz_s = right_pos["phiz"]
            phiy_e = left_pos["phiy"]
            phiz_e = left_pos["phiz"]
            delta_sampx_s = -delta_h * math.sin(math.radians(right_pos["phi"]))
            delta_sampy_s = delta_h * math.cos(math.radians(right_pos["phi"]))
            delta_sampx_e = -delta_h * math.sin(math.radians(left_pos["phi"]))
            delta_sampy_e = delta_h * math.cos(math.radians(left_pos["phi"]))
            sampx_s = right_pos["sampx"] + delta_sampx_s
            sampy_s = right_pos["sampy"] + delta_sampy_s
            sampx_e = left_pos["sampx"] + delta_sampx_e
            sampy_e = left_pos["sampy"] + delta_sampy_e
            rotation_axis_start = round(right_pos["phi"], 2)
        motorPositions4dscan = {
            "phiy_s": phiy_s,
            "phiz_s": phiz_s,
            "sampx_s": sampx_s,
            "sampy_s": sampy_s,
            "phiy_e": phiy_e,
            "phiz_e": phiz_e,
            "sampx_e": sampx_e,
            "sampy_e": sampy_e,
        }
        # pprint.pprint(motorPositions4dscan)
        fileData = {
            "directory": directory,
            "expTypePrefix": "",
            "firstImage": first_image,
            "prefix": prefix,
            "process_directory": process_directory,
            "runNumber": runNumber,
        }
        # pprint.pprint(fileData)
        noImages = int((right_pos["phi"] - left_pos["phi"]) / oscillationWidth + 0.5)
        subWedgeData = {
            "doProcessing": True,
            "exposureTime": exposureTime,
            "noImages": noImages,
            "oscillationWidth": oscillationWidth * direction,
            "rotationAxisStart": rotation_axis_start,
            "transmission": transmission,
        }
        # pprint.pprint(subWedgeData)
        queue_entry = {
            "fileData": fileData,
            "motorPositions4dscan": motorPositions4dscan,
            "subWedgeData": subWedgeData,
        }
        list_queue_entries.append(queue_entry)
        first_image += noImages
        direction = direction * -1
    pprint.pprint(list_queue_entries)
    # for queue_entry in list_queue_entries:
    #     motor_pos_start = {
    #         "phi": queue_entry["subWedgeData"]["rotationAxisStart"],
    #         "phiy": queue_entry["motorPositions4dscan"]["phiy_s"],
    #         "sampx": queue_entry["motorPositions4dscan"]["sampx_s"],
    #         "sampy": queue_entry["motorPositions4dscan"]["sampy_s"]
    #     }
    #     collect.moveMotors(beamline, directory, motor_pos_start)
    #     yesno = input("Continue (yes/no)? ")
    #     motor_pos_end = {
    #         "phi": queue_entry["subWedgeData"]["rotationAxisStart"] + queue_entry["subWedgeData"]["noImages"] * queue_entry["subWedgeData"]["oscillationWidth"],
    #         "phiy": queue_entry["motorPositions4dscan"]["phiy_e"],
    #         "sampx": queue_entry["motorPositions4dscan"]["sampx_e"],
    #         "sampy": queue_entry["motorPositions4dscan"]["sampy_e"]
    #     }
    #     collect.moveMotors(beamline, directory, motor_pos_end)
    #     yesno = input("Continue (yes/no)? ")

    yesno = input("Continue (yes/no)? ")
    if yesno.lower() == "yes":
        workflowParameters = {"index": 1, "type": "ChipxTest"}
        groupNodeId = None
        sampleNodeId = 1
        motorPositions = collect.readMotorPositions(beamline)
        force_not_shutterless = False
        _ = collect.executeQueueForMeshScan(
            beamline,
            workflowParameters["index"],
            workflowParameters["type"],
            list_queue_entries,
            groupNodeId,
            sampleNodeId,
            resolution,
            motorPositions,
            force_not_shutterless=force_not_shutterless,
            process_data=True,
        )

        # noLines = 5
        # meshRange = {
        #     "horizontal_range": math.fabs(right_pos["phiy"] - left_pos["phiy"]),
        #     "vertical_range": 0.1}
        # workflowParameters = {"index": 1, "type": "XrayCentering"}
        # groupNodeId = None
        # sampleNodeId = 1
        # motorPositions = collect.readMotorPositions(beamline)
        # dictId = collect.executeQueueForFastMeshScan(
        #     beamline,
        #     workflowParameters["index"],
        #     workflowParameters["type"],
        #     [queueEntry],
        #     groupNodeId,
        #     sampleNodeId,
        #     resolution,
        #     motorPositions,
        #     noLines,
        #     meshRange,
        # )


#
#
# queueEntry = {
#         "fileData": {
#             "directory": directory,
#             "expTypePrefix": "mesh-",
#             "firstImage": 1,
#             "prefix": prefix,
#             "process_directory": process_directory,
#             "runNumber": 1,
#         },
#         "motorPositions4dscan": {
#             "phiy_s": -80.958,
#             "phiz_s": 0.076,
#             "sampx_s": 2.064,
#             "sampy_s": -2.061,
#             "phiy_e": -41.270,
#             "phiz_e": 0.076,
#             "sampx_e": 2.066,
#             "sampy_e": -2.035,
#         },
#         "subWedgeData": {
#             "doProcessing": False,
#             "exposureTime": 0.1,
#             "noImages": 210,
#             "oscillationWidth": 0.1,
#             "rotationAxisStart": 296,
#             "transmission": 11.5,
#         },
#     }
#
#     workflowParameters = {"index": 1, "type": "ChipxTest"}
#     groupNodeId = None
#     sampleNodeId = 1
#     resolution = 3.359
#     motorPositions = collect.readMotorPositions(beamline)
#     force_not_shutterless = False
#     dictId = collect.executeQueueForMeshScan(
#         beamline,
#         workflowParameters["index"],
#         workflowParameters["type"],
#         [queueEntry],
#         groupNodeId,
#         sampleNodeId,
#         resolution,
#         motorPositions,
#         force_not_shutterless=force_not_shutterless,
#     )
