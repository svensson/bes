import os
import unittest

from bes.workflow_lib import edna_ispyb, edna_mxv1


class TestMethods(unittest.TestCase):

    def setUp(self):
        path = os.path.abspath(__file__)
        strTestDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )
        # Test 'normal' data collection
        f = open(os.path.join(strTestDataDirectory, "burn_strategy.xml"), "r")
        self.mxv1StrategyResult = f.read()
        f.close()

    def tes_storeOrUpdateWorkflow(self):
        beamline = "id30a2"
        workflowWorkingDir = None
        workflowTitle = "Test of edna_ispyb.storeOrUpdateWorkflow"
        workflowType = "MXPressA"
        workflowPyarchLogFile = "/tmp/workflowPyarchLogFile.log"
        workflowLogFile = "/tmp/workflowLogFile.log"
        pyarchHtmlDir = "/tmp/pyarch_html_dir"
        workflowId = edna_ispyb.storeOrUpdateWorkflow(
            beamline,
            workflowTitle=workflowTitle,
            workflowType=workflowType,
            workflowPyarchLogFile=workflowPyarchLogFile,
            workflowLogFile=workflowLogFile,
            pyarchHtmlDir=pyarchHtmlDir,
            workflowWorkingDir=workflowWorkingDir,
        )
        self.assertTrue(workflowId > 0, "Invalid workflow id")

    def tes_storeOrUpdateWorkflow_update(self):
        beamline = "id30a2"
        workflowId = 33629
        workflowWorkingDir = None
        workflowTitle = "Test of edna_ispyb.storeOrUpdateWorkflow"
        workflowType = "MXPressO"
        workflowPyarchLogFile = "/tmp/workflowPyarchLogFile.log"
        workflowLogFile = "/tmp/workflowLogFile.log"
        pyarchHtmlDir = "/tmp/pyarch_html_dir"
        workflowId = edna_ispyb.storeOrUpdateWorkflow(
            beamline,
            workflowId=workflowId,
            workflowTitle=workflowTitle,
            workflowType=workflowType,
            workflowPyarchLogFile=workflowPyarchLogFile,
            workflowLogFile=workflowLogFile,
            pyarchHtmlDir=pyarchHtmlDir,
            workflowWorkingDir=workflowWorkingDir,
        )
        self.assertTrue(workflowId > 0, "Invalid workflow id")

    def tes_getFluxBeamsize(self):
        beamline = "simulator"
        #        run_number = 6
        #        expTypePrefix = "ref-"
        #        prefix = "p1burn"
        #        directory = "/data/id14eh4/inhouse/opid144/20130220/RAW_DATA/WF/burn"
        #        suffix = "img"
        # /data/id29/inhouse/opid291/20160408/RAW_DATA/FAE/FAE-sampl1/ref-FAE-sampl1_2_0001.cbf
        run_number = 2
        expTypePrefix = "ref-"
        prefix = "FAE-sampl1"
        directory = "/data/id14eh4/inhouse/opid144/20130220/RAW_DATA/WF/burn"
        suffix = "cbf"
        from EDUtilsPath import EDUtilsPath

        EDUtilsPath.setEdnaSite("ESRF_ISPyBTest")
        workflow_working_dir = None
        dictFluxBeamsize = edna_ispyb.getFluxBeamsize(
            beamline,
            self.mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            workflow_working_dir,
        )
        # print dictFluxBeamsize
        self.assertAlmostEqual(
            0.05, float(dictFluxBeamsize["beamSizeAtSampleX"]), 4, "Wrong beamsize x"
        )
        self.assertAlmostEqual(
            0.05, float(dictFluxBeamsize["beamSizeAtSampleY"]), 4, "Wrong beamsize y"
        )
        self.assertAlmostEqual(
            976000000000.0, float(dictFluxBeamsize["flux"]), 4, "Wrong flux"
        )

    def tes_setWorkflowStatus(self):
        beamline = "simulator"
        workflow_id = "1"
        workflow_working_dir = None
        edna_ispyb.setWorkflowStatus(
            beamline, workflow_working_dir, workflow_id, "Success"
        )

    def tes_updateDataCollectionGroupworkflow_id(self):
        workflow_id = "1"
        mxv1StrategyResult = self.mxv1StrategyResult
        run_number = "2"
        expTypePrefix = "ref-"
        prefix = "opid141"
        directory = "/data/id14eh1/inhouse/opid141/20130403/RAW_DATA"
        suffix = "img"
        beamline = "simulator"
        workflow_working_dir = None
        firstImagePath = edna_mxv1.getListImagePath(
            mxv1StrategyResult, directory, run_number, expTypePrefix, prefix, suffix
        )[0]
        iDataCollectionGroupId = edna_ispyb.updateDataCollectionGroupWorkflowId(
            beamline,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            firstImagePath,
            workflow_id,
            workflow_working_dir,
        )
        # print iDataCollectionGroupId
        self.assertEqual(1096720, iDataCollectionGroupId, "Wrong dataCollectionId")

    def tes_groupDataCollections(self):
        iDataCollectionGroupId = 1081903
        mxv1StrategyResult = self.mxv1StrategyResult
        run_number = "6"
        expTypePrefix = "ref-"
        prefix = "ref-p1burnw1"
        directory = "/data/id14eh4/inhouse/opid144/20130220/RAW_DATA/WF/burn"
        suffix = "img"
        beamline = "simulator"
        workflow_working_dir = None
        listDataCollectionId = edna_ispyb.groupDataCollections(
            beamline,
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            workflow_working_dir,
            iDataCollectionGroupId,
        )
        self.assertEqual(
            3, len(listDataCollectionId), "Wrong number of data collection ids"
        )

    def tes_storeDataCollectionPosition(self):
        beamline = "simulator"
        directory = "/data/id23eh2/inhouse/opid232/20130516/RAW_DATA/delete_me"
        meshPositions = [
            {
                "goodBraggCandidates": 57,
                "iceRings": 4,
                "imageName": "delete_me_1_0010.mccd",
                "inResTotal": 68,
                "inResolutionOvrlSpots": 1,
                "index": 10,
                "indexY": 2,
                "indexZ": 4,
                "maxUnitCell": 288.19999999999999,
                "method1Res": 1.8899999999999999,
                "method2Res": 3.7400000000000002,
                "mosaicity": 0.20000000000000001,
                "pctSaturationTop50Peaks": 6.5999999999999996,
                "phi": 1.0,
                "kappa": 2.0,
                "omega": 3.0,
                "focus": 4.0,
                "phiy": -0.10361557403028013,
                "phiz": 0.0,
                "sampx": 0.0,
                "sampy": -0.062145366117265601,
                "spaceGroup": "F222",
                "spotTotal": 266,
                "totalIntegratedSignal": 3920172.0,
            }
        ]
        workflow_working_dir = None
        listImageCreation = edna_ispyb.store_positions(
            beamline, directory, meshPositions, workflow_working_dir
        )
        self.assertNotEqual([], listImageCreation)

    def tes_get_sample_information(self):
        dict_sample_info = edna_ispyb.get_sample_information("id30a1", 526477, None)
        print(dict_sample_info)
