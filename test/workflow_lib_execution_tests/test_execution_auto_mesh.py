"""
Created on Oct 29, 2014

@author: opid30
"""

import os
import pathlib
import pprint
import unittest
import tempfile
import shutil

from bes.workflow_lib import grid
from bes import config
from bes.workflow_lib import auto_mesh


class Test(unittest.TestCase):

    def setUp(self):
        path = os.path.abspath(__file__)
        self.testDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )

    def tes_plotMesh(self):
        beamline = "simulator"
        imagePath = os.path.join(
            self.testDataDirectory, "snapshots", "snapshot_000.png"
        )
        imagePath = "/data/visitor/mx1644/id30a1/20150610/PROCESSED_DATA/MXPressE_02/snapshots_20150610-181042_zmn71G/snapshot_000.png"
        #        grid_info = {"x1": -0.40, "y1": -0.10, "dx_mm": 0.80, "dy_mm": 0.20, "steps_x": 30, "steps_y": 8, "angle": 0.0}
        grid_info = {
            "x1": -0.4642857142857143,
            "dx_mm": 0.60714285714285721,
            "dy_mm": 0.12380952380952381,
            "y1": -0.0023809523809523812,
            "steps_y": 3,
            "steps_x": 18,
        }
        PIXELS_PER_MM = 420.0
        destinationDir = tempfile.mkdtemp(prefix="tes_plotMesh_")
        auto_mesh.plotMesh(
            beamline, imagePath, grid_info, PIXELS_PER_MM, destinationDir
        )
        print(destinationDir)

    def tes_createSnapshotHTMLPage(self):
        snapshotDir = os.path.join(self.testDataDirectory, "snapshots")
        autoMeshWorkingDir = tempfile.mkdtemp(prefix="tes_createSnapshotHTMLPage_")
        print(autoMeshWorkingDir)
        snapShotPath, htmlResultFilePath, dictWorkflowStep = (
            auto_mesh.createSnapshotHTMLPage(snapshotDir, autoMeshWorkingDir)
        )
        pprint.pprint(dictWorkflowStep)
        os.system("firefox {0}".format(htmlResultFilePath))

    def tes_createAutomeshHTMLPage(self):
        beamline = "simulator"
        snapshotDir = os.path.join(self.testDataDirectory, "snapshots")
        autoMeshWorkingDir = tempfile.mkdtemp(prefix="tes_autoMeshWorkingDir_")
        newPhi = 90
        grid_info = {
            "x1": -0.40,
            "y1": -0.10,
            "dx_mm": 0.80,
            "dy_mm": 0.20,
            "steps_x": 30,
            "steps_y": 8,
            "angle": 0.0,
        }
        PIXELS_PER_MM = 420.0
        print(autoMeshWorkingDir)
        meshSnapShotPath, htmlResultFilePath, dictWorkflowStep = (
            auto_mesh.createAutomeshHTMLPage(
                beamline,
                newPhi,
                grid_info,
                PIXELS_PER_MM,
                snapshotDir,
                autoMeshWorkingDir,
                pyarchHtmlDir=autoMeshWorkingDir,
            )
        )
        pprint.pprint(htmlResultFilePath)
        os.system("firefox {0}".format(htmlResultFilePath))

    def tes_autoMesh(self):
        beamline = "simulator"
        listSnapshotDirs = [
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20141128-080912_2OjRxn",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20141128-082628_ts2BbA",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20141128-084026_ZJhTTm",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20141128-090655_HkAeYr",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20141128-091906_9a21G1",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20141128-115621_bJWx8r",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20150909-162502_IgFLIh",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20150910-160027_ixjYuu",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20150910-161141_Uw9DL_",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-100732_YtlQg0",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-095438_mOWCEL",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-094218_Tz0zXg",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-093117_ID_786",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-091844_At26Fs",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-090753_LrAcfV",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-085551_YkiACP",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-084450_bnKVDI",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-200409_8enI_6",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-150456_p7WJe4",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-152805_jhp9cH",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-154808_HCBGNs",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-163817_OwzXKK",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-165709_RWOXa2",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-172300_SKn99Y",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-175435_Q0g3lr",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-182232_hjvEfx",
            #                            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20151013-184258_GFq48d",
            # "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30b/snapshots/snapshots_20160503-164412_Vx9Nz3",
            # "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20160727-165510_CDQupt",
            # "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/snapshots/snapshots_20170201-082728_Lt3Etn",
            # "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30b/snapshots/snapshots_20170503-120344_m3P4oP",
            # "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30b/snapshots/snapshots_20170503-122550_yssPEA",
        ]
        # listSnapshotDirs = glob.glob("/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210516/snapshots/*")
        # listSnapshotDirs = ["/data/id30a2/inhouse/opid30a2/20230328/PROCESSED_DATA/w/w-w/MXPressM_05/snapshots_20230328-123335_ydvw6ny0"]
        # listSnapshotDirs = ["/data/id30a2/inhouse/opid30a2/20230328/PROCESSED_DATA/e/e-e/MXPressM_01/snapshots_20230328-125547_8gbyv3wn"]
        # listSnapshotDirs = ["/data/visitor/mx2503/id30b/20230623/PROCESSED_DATA/SsTyrM/SsTyrM-TyrH12/MXPressE_01/snapshots_20230623-133304_ng3_6dub"]
        # listSnapshotDirs = ["/data/visitor/mx2503/id30b/20230623/PROCESSED_DATA/SsTyrM/SsTyrM-TyrH13/MXPressE_01/snapshots_20230623-133914_nl26yag8"]
        listSnapshotDirs = [
            "/data/visitor/mx2532/id30a1/20230922/RAW_DATA/PYL9/PYL9-CD038646_B01-3/run_01_MXPressA/run_01_01_automesh"
        ]
        pprint.pprint(listSnapshotDirs)
        workingDir = tempfile.mkdtemp(prefix="autoMesh_", dir="/tmp_14_days/svensson")
        print(workingDir)
        os.chmod(workingDir, 0o755)
        for snapshotDir in listSnapshotDirs:
            identifier = snapshotDir[-22:-7]
            (
                newPhi,
                x1Pixels,
                y1Pixels,
                dxPixels,
                dyPixels,
                deltaPhizPixels,
                stdPhizPixels,
                imagePath,
                areTheSameImage,
            ) = grid.autoMesh(
                snapshotDir,
                workingDir,
                workingDir,
                debug=False,
                loopMaxWidth=0.75 * 608,
                loopMinWidth=0.25 * 608,
                snapShotAngles=[0, 30, 60, 90, 120, 150],
                findLargestMesh=False,
            )
            #            (angleMinThickness, x1Pixels, y1Pixels, dxPixels, dyPixels, deltaPhiz, stdPhiz, imagePath) = \
            #                (angleMinThickness, x1Pixels, y1Pixels, dxPixels, dyPixels, deltaPhiz, stdPhiz, imagePath, areTheSameImage) = grid.autoMesh(snapshotDir, workingDir, workingDir, \
            #                              loopMaxWidth=330, loopMinWidth=250, prefix="snapshot", debug=False, findLargestMesh=False)
            print(
                "Grid coordinates in pixels: x1={0:.1f} y1={1:.1f} dx={2:.1f} dy={3:.1f}".format(
                    x1Pixels, y1Pixels, dxPixels, dyPixels
                )
            )
            PIXELS_PER_MM = 608.0
            deltaPhizPixels = 0.0
            xOffsetLeft = 0.05
            xOffsetRight = 0.05
            beamSize = 0.1
            overSamplingX = 1.5
            overSamplingY = 1.5
            x1 = x1Pixels / PIXELS_PER_MM - xOffsetLeft
            y1 = -(y1Pixels + dyPixels + deltaPhizPixels) / PIXELS_PER_MM
            dx_mm = dxPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight
            dy_mm = dyPixels / PIXELS_PER_MM
            steps_x = int(
                (dxPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight)
                / beamSize
                * overSamplingX
            )
            steps_y = int(dyPixels / PIXELS_PER_MM / beamSize * overSamplingY)
            # Check that we have at least three lines:
            if steps_y == 1 or steps_y == 2:
                steps_y = 3
                y1 -= dyPixels / PIXELS_PER_MM / 4.0
                dy_mm *= 1.5
            grid_info = {
                "x1": x1,
                "y1": y1,
                "dx_mm": dx_mm,
                "dy_mm": dy_mm,
                "steps_x": steps_x,
                "steps_y": steps_y,
            }
            print("Auto grid_info: %r" % grid_info)
            print(workingDir)
            resultImagePath = os.path.join(workingDir, "snapshot_automesh.png")
            auto_mesh.plotMesh(
                beamline, imagePath, grid_info, PIXELS_PER_MM, workingDir
            )
            os.system("display %s" % resultImagePath)
            shutil.copyfile(
                os.path.join(workingDir, "snapshot_automesh.png"),
                os.path.join(
                    workingDir, "snapshot_automesh_{0}.png".format(identifier)
                ),
            )

    #             print snapshotDir.split("/")[8], "%.4f" % (deltaPhiz / 420.0), "phiz std: %.4f" % (stdPhiz / 420.0)
    #
    #             print workingDir

    def tes_autoMesh_verticalRotationAxis(self):
        beamline = "simulator"
        listSnapshotDirs = [
            # "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/snapshots/snapshots_20180601-102320_38_8i5",
            #                             "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/snapshots/snapshots_20180601-103406_w41XMb",
            #                             "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/snapshots/snapshots_20180601-104438_bx7AM6",
            #                             "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/snapshots/snapshots_20180601-104948_gmJr0V",
            #                             "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/snapshots/snapshots_20180601-105718_Hap3zL",
            #                              "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/snapshots/snapshots_20180608-103725_G2SwMM",
            # #                             "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/snapshots/snapshots_20180608-105630_b0jGKB",
            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/20210517/snapshots/snapshots_20210517-162956_7tm8y_24",
            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/20210517/snapshots/snapshots_20210517-163245_pzl4od0j",
        ]
        workingDir = tempfile.mkdtemp(prefix="autoMesh_", dir="/tmp")
        print(workingDir)
        os.chmod(workingDir, 0o755)
        for snapshotDir in listSnapshotDirs:
            identifier = snapshotDir[-22:-7]
            (
                newPhi,
                x1Pixels,
                y1Pixels,
                dxPixels,
                dyPixels,
                deltaPhizPixels,
                stdPhizPixels,
                imagePath,
                areTheSameImage,
            ) = grid.autoMesh(
                snapshotDir,
                workingDir,
                workingDir,
                debug=False,
                loopMaxWidth=0.5 * 560,
                loopMinWidth=0.35 * 560,
                snapShotAngles=[0, 30, 60, 90],
                findLargestMesh=True,
                isVerticalAxis=True,
            )
            #            (angleMinThickness, x1Pixels, y1Pixels, dxPixels, dyPixels, deltaPhiz, stdPhiz, imagePath) = \
            #                (angleMinThickness, x1Pixels, y1Pixels, dxPixels, dyPixels, deltaPhiz, stdPhiz, imagePath, areTheSameImage) = grid.autoMesh(snapshotDir, workingDir, workingDir, \
            #                              loopMaxWidth=330, loopMinWidth=250, prefix="snapshot", debug=False, findLargestMesh=False)
            print(
                "Grid coordinates in pixels: x1=%.1f y1=%.1f dx=%.1f dy=%.1f"
                % (x1Pixels, y1Pixels, dxPixels, dyPixels)
            )
            PIXELS_PER_MM = 1235.0
            deltaPixels = dyPixels - dxPixels
            dyPixels = dxPixels
            xOffsetLeft = -dxPixels * 0.1
            xOffsetRight = -dxPixels * 0.1
            yOffsetTop = -dyPixels * 0.1 + deltaPixels
            yOffsetBottom = -dyPixels * 0.1 - deltaPixels
            beamSize = 0.005
            overSamplingX = 1.0
            overSamplingY = 1.0
            x1 = (x1Pixels - xOffsetLeft) / PIXELS_PER_MM
            y1 = -(y1Pixels + dyPixels + yOffsetTop) / PIXELS_PER_MM
            dx_mm = (dxPixels + xOffsetLeft + xOffsetRight) / PIXELS_PER_MM
            dy_mm = (dyPixels + yOffsetTop + yOffsetBottom) / PIXELS_PER_MM
            steps_x = int(dx_mm / beamSize * overSamplingX)
            steps_y = int(dy_mm / beamSize * overSamplingY)
            # Check that we have at least three lines:
            if steps_y == 1 or steps_y == 2:
                steps_y = 3
                y1 -= dyPixels / PIXELS_PER_MM / 4.0
                dy_mm *= 1.5
            grid_info = {
                "x1": x1,
                "y1": y1,
                "dx_mm": dx_mm,
                "dy_mm": dy_mm,
                "steps_x": steps_x,
                "steps_y": steps_y,
            }
            print("Auto grid_info: %r" % grid_info)
            print(workingDir)
            resultImagePath = os.path.join(workingDir, "snapshot_automesh.png")
            auto_mesh.plotMesh(
                beamline, imagePath, grid_info, PIXELS_PER_MM, workingDir
            )
            os.system("display %s" % resultImagePath)
            shutil.copyfile(
                os.path.join(workingDir, "snapshot_automesh.png"),
                os.path.join(
                    workingDir, "snapshot_automesh_{0}.png".format(identifier)
                ),
            )

    #             print snapshotDir.split("/")[8], "%.4f" % (deltaPhiz / 420.0), "phiz std: %.4f" % (stdPhiz / 420.0)
    #
    #             print workingDir

    # def tes_one_image(self):
    # imagePath = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id23eh2/20201013/snapshots/ADIPOR-CD031189-H11-2_0_snapshot_before_mesh.png"
    # (listIndex, listUpper, listLower) = grid.loopExam(
    #    imagePath,
    #    background_image,
    #    omega,
    #    autoMeshWorkingDir=autoMeshWorkingDir,
    #    isVerticalAxis=isVerticalAxis,
    #    debug=debug,
    # )

    def test_all_beamlines(self):
        test_auto_mesh_dir = pathlib.Path("/data/scisoft/pxsoft/data/automesh")
        working_dir = tempfile.mkdtemp(
            prefix="tmp_autoMesh_", dir="/tmp_14_days/svensson"
        )
        # for beamline in ["id23eh1", "id23eh2", "id30a1", "id30a3", "id30b"]:
        for beamline in ["id30a1"]:
            result_dir = pathlib.Path(
                tempfile.mkdtemp(
                    prefix=f"automesh_result_{beamline}_", dir="/tmp_14_days/svensson"
                )
            )
            loopMinWidth = float(config.get_value(beamline, "AutoMesh", "loopMinWidth"))
            loopMaxWidth = float(config.get_value(beamline, "AutoMesh", "loopMaxWidth"))
            findLargestMesh = (
                config.get_value(beamline, "AutoMesh", "findLargestMesh").lower()
                == "true"
            )
            PIXELS_PER_MM = config.get_value(beamline, "SnapShotZoomMmPerPixel", "2")
            rotationAxis = config.get_value(beamline, "Goniometer", "rotationAxis")
            xOffsetLeft = config.get_value(beamline, "AutoMesh", "xOffsetLeft")
            xOffsetRight = config.get_value(beamline, "AutoMesh", "xOffsetRight")
            isHorizontalRotationAxis = rotationAxis == "horizontal"
            isVerticalAxis = not isHorizontalRotationAxis
            if isHorizontalRotationAxis:
                _ = config.get_value(beamline, "AutoMesh", "deltaPhiy")
                signPhiy = config.get_value(beamline, "AutoMesh", "signPhiy")
            else:
                _ = config.get_value(beamline, "AutoMesh", "deltaPhiz")
                signPhiz = config.get_value(beamline, "AutoMesh", "signPhiz")
            loopMinWidthPixels = loopMinWidth * PIXELS_PER_MM
            loopMaxWidthPixels = loopMaxWidth * PIXELS_PER_MM
            overSamplingX = config.get_value(beamline, "AutoMesh", "overSamplingX")
            overSamplingY = config.get_value(beamline, "AutoMesh", "overSamplingY")
            snapShotAngles = config.get_value(beamline, "AutoMesh", "snapShotAngles")
            fixedTargetApertureSize = config.get_value(
                beamline, "AutoMesh", "fixedTargetApertureSize"
            )
            beamSize = fixedTargetApertureSize / 1000.0
            beamline_dir = test_auto_mesh_dir / beamline
            list_automesh_dir = list(beamline_dir.glob("automesh_*"))
            filtered_list_automesh_dir = [
                dir for dir in list_automesh_dir if "20240327" in str(dir)
            ]
            for automesh_dir in filtered_list_automesh_dir:
                # automesh_dir = "/data/scisoft/pxsoft/data/automesh/id23eh1/automesh_20240218_145711"
                # automesh_dir = pathlib.Path("/data/scisoft/pxsoft/data/automesh/id30a3/automesh_20240220_190858")
                print(automesh_dir)
                x1Pixels = y1Pixels = dxPixels = dyPixels = None
                try:
                    (
                        newPhi,
                        x1Pixels,
                        y1Pixels,
                        dxPixels,
                        dyPixels,
                        deltaPhizPixels,
                        stdPhizPixels,
                        imagePath,
                        areTheSameImage,
                    ) = grid.autoMesh(
                        snapshotDir=str(automesh_dir),
                        workflowWorkingDir=working_dir,
                        autoMeshWorkingDir=working_dir,
                        debug=False,
                        loopMaxWidth=loopMaxWidthPixels,
                        loopMinWidth=loopMinWidthPixels,
                        snapShotAngles=snapShotAngles,
                        findLargestMesh=findLargestMesh,
                        isVerticalAxis=isVerticalAxis,
                    )
                except Exception:
                    pass
                if None not in [x1Pixels, y1Pixels, dxPixels, dyPixels]:
                    print(
                        "Grid coordinates in pixels: x1=%.1f y1=%.1f dx=%.1f dy=%.1f"
                        % (x1Pixels, y1Pixels, dxPixels, dyPixels)
                    )
                    if deltaPhizPixels is None:
                        deltaPhizPixels = 0.0
                    if isVerticalAxis:
                        x1 = x1Pixels / PIXELS_PER_MM
                        y1 = (
                            -(y1Pixels + dyPixels + deltaPhizPixels) / PIXELS_PER_MM
                            - xOffsetLeft
                        )
                        dx_mm = dxPixels / PIXELS_PER_MM
                        dy_mm = dyPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight
                        steps_x = int(
                            dxPixels / PIXELS_PER_MM / beamSize * overSamplingX
                        )
                        steps_y = int(
                            (dyPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight)
                            / beamSize  # noqa W503
                            * overSamplingY  # noqa W503
                        )
                    else:
                        x1 = x1Pixels / PIXELS_PER_MM - xOffsetLeft
                        y1 = -(y1Pixels + dyPixels + deltaPhizPixels) / PIXELS_PER_MM
                        dx_mm = dxPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight
                        dy_mm = dyPixels / PIXELS_PER_MM
                        steps_x = int(
                            (dxPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight)
                            / beamSize  # noqa W503
                            * overSamplingX  # noqa W503
                        )
                        steps_y = int(
                            dyPixels / PIXELS_PER_MM / beamSize * overSamplingY
                        )
                    # Check that we have at least three lines:
                    if isVerticalAxis and steps_x < 3:
                        steps_x = 3
                        x1 -= dxPixels / PIXELS_PER_MM / 4.0
                        dx_mm *= 1.5
                    elif steps_y < 3:
                        steps_y = 3
                        y1 -= dyPixels / PIXELS_PER_MM / 4.0
                        dy_mm *= 1.5
                    grid_info = {
                        "x1": round(float(x1), 4),
                        "y1": round(float(y1), 4),
                        "dx_mm": round(float(dx_mm), 4),
                        "dy_mm": round(float(dy_mm), 4),
                        "steps_x": int(steps_x),
                        "steps_y": int(steps_y),
                    }
                    # logger.info("Auto grid_info: %r" % grid_info)
                    prefix = "snapshot"
                    suffix = "png"
                    imagePath = os.path.join(
                        automesh_dir, "{0}_{1:03d}.{2}".format(prefix, newPhi, suffix)
                    )
                    if isHorizontalRotationAxis:
                        meshSnapShotPath = auto_mesh.plotMesh(
                            beamline,
                            imagePath,
                            grid_info,
                            PIXELS_PER_MM,
                            working_dir,
                            signPhiy=-signPhiy,
                        )
                    else:
                        meshSnapShotPath = auto_mesh.plotMesh(
                            beamline,
                            imagePath,
                            grid_info,
                            PIXELS_PER_MM,
                            working_dir,
                            signPhiy=-signPhiz,
                        )
                    new_mesh_file_name = f"{beamline}_{automesh_dir.name}.png"
                    new_mesh_path = result_dir / new_mesh_file_name
                    print(new_mesh_path)
                    shutil.copyfile(meshSnapShotPath, new_mesh_path)
                    # os.system(f"display {meshSnapShotPath}")
                    # break

                # sys.exit(0)
