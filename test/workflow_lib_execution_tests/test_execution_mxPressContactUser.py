"""
Created on Feb 9, 2015

@author: svensson
"""

import unittest
import mxPressContactUser


class Test(unittest.TestCase):

    def test_mxPressContactUser(self):
        directory = (
            "/data/visitor/mx1581/id30a1/20150209/RAW_DATA/pol3/pol3-S04/MXPressE_01"
        )
        beamline = "simulator"
        workflowParameters = {}
        raw_directory = directory
        mxPressContactUser.run(beamline, workflowParameters, directory, raw_directory)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_mxPressContactUser']
    unittest.main()
