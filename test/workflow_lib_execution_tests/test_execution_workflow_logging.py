import unittest

from bes.workflow_lib import workflow_logging


class TestMethods(unittest.TestCase):

    def test_mxCuBELogger(self):
        logger = workflow_logging.getLogger(beamline="simulator")
        logger.debug("Debugging...")
        logger.info("While this is just chatty")
        logger.warning("Warning!")
        logger.error("We have a problem")
