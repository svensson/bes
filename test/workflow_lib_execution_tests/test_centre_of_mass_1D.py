"""
Created on Mar 31, 2014

@author: svensson
"""

import os
import json

# import matplotlib.pyplot as pyplot
from bes.workflow_lib import grid

if __name__ == "__main__":
    path = os.path.abspath(__file__)
    strTestDataDirectory = os.path.join(os.path.dirname(os.path.dirname(path)), "data")
    f = open(os.path.join(strTestDataDirectory, "meshResults1D.json"), "r")
    strData = f.read()
    meshPositions = json.loads(strData)
    f.close()
    grid_info = {
        "angle": 0.0,
        "dx_mm": 0.10,
        "dy_mm": 0.11,
        "steps_x": 11,
        "steps_y": 1,
        "x1": -0.104,
        "y1": -0.062,
    }
    grid.find_best_position(grid_info, meshPositions)
