import unittest
import pprint
from bes.workflow_lib import statistics


class TestMethods(unittest.TestCase):

    def test_getTotalNumberOfMXPress(self):
        beamline = "id30a1"
        year = 2017
        nmxpress = statistics.getTotalNumberOfMXPress(beamline, year)
        print("Number of MXPress for year {0}: {1}".format(year, nmxpress))
        self.assertTrue(nmxpress > 1)

    def test_getAverageCrystalVolumeLastWeek(self):
        beamline = "id30a1"
        dictResult = statistics.getCrystalVolumeStatisticsForLastWeek(beamline)
        pprint.pprint(dictResult)
        print("Average volume: {0}".format(dictResult["averageVolume"]))
        print("Mode volume: {0}".format(dictResult["modeVolume"]))
        print("Min crystal length: {0}".format(dictResult["minCrysatlLength"]))
        print("Max crystal length: {0}".format(dictResult["maxCrysatlLength"]))
        self.assertTrue("averageVolume" in dictResult)
