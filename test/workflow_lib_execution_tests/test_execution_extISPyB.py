import os
import unittest
import pprint
import tempfile
import urllib
from bes.workflow_lib import extISPyB


class TestMethods(unittest.TestCase):

    def tes_getListDataCollection(self):
        urlExtISPyB = "http://linsvensson:8080/ispyb/ispyb-ws/rest"
        proposal = "MX1743"
        token = "259524ab171cb5ee0525bd176626af2649ed08c4"
        workflowId = 23793
        listDataCollection = extISPyB.getListDataCollection(
            urlExtISPyB, token, proposal, workflowId
        )
        pprint.pprint(listDataCollection)

    def test_getProcessResults(self):
        urlExtISPyB = "http://linsvensson:8080/ispyb/ispyb-ws/rest"
        proposal = "MX1743"
        token = "259524ab171cb5ee0525bd176626af2649ed08c4"
        workflowId = 23793
        listDataCollection = extISPyB.getProcessResults(
            urlExtISPyB, token, proposal, workflowId
        )
        pprint.pprint(listDataCollection)

    def tes_getListAutoprocIntegration(self):
        urlExtISPyB = "http://linsvensson:8080/ispyb/ispyb-ws/rest"
        proposal = "MX1743"
        #        proposal = "MX415"
        token = "259524ab171cb5ee0525bd176626af2649ed08c4"
        dataCollectionId = 1551567
        #        dataCollectionId = 1770850
        listAutoprocIntegration = extISPyB.getListAutoprocIntegration(
            urlExtISPyB, token, proposal, dataCollectionId
        )
        #        pprint.pprint(listAutoprocIntegration)
        bestAutoprocIntegration = extISPyB.selectBestAutoprocIntegration(
            listAutoprocIntegration
        )
        pprint.pprint(bestAutoprocIntegration)

    def tes_findHighestSymmetrySpaceGroup(self):
        listSpaceGroups = [
            "P 21 21 21",
            "P 21 21 21",
            " P 2 2 2 ",
            " P 2 2 2 ",
            " P 2 2 2 ",
            " P 2 2 2 ",
            " P 1 2 1 ",
            " P 1 ",
            " P 1 2 1 ",
            " P 1 ",
            " P 1 2 1 ",
            " P 1 2 1 ",
            " P 21 21 21 ",
            " P 21 21 21 ",
        ]
        highestSymmetry = extISPyB.findHighestSymmetrySpaceGroup(listSpaceGroups)
        print(highestSymmetry)

    def tes_getFromURL(self):
        url1 = "https://ispyb-valid.esrf.fr/ispyb/ispyb-ws/rest/1144dacb5ac634b0bfb8fb29219f46e5d1c29e0f/proposal/MX415/mx/autoprocintegration/datacollection/1774492/view"
        listAutoProcIntegration = extISPyB.getFromURL(url1)
        url2 = "https://ispyb-valid.esrf.fr/ispyb/ispyb-ws/rest/1144dacb5ac634b0bfb8fb29219f46e5d1c29e0f/proposal/MX415/mx/autoprocintegration/attachment/autoprocprogramid/%autoprocprogramIds%/list"

        for autoProcIntegration in listAutoProcIntegration:
            if (
                autoProcIntegration["v_datacollection_processingPrograms"]
                == "EDNA_proc"
                and not autoProcIntegration[
                    "v_datacollection_summary_phasing_anomalous"
                ]
            ):
                #                pprint.pprint(autoProcIntegration)
                autoProcProgramId = autoProcIntegration[
                    "v_datacollection_summary_phasing_autoProcProgramId"
                ]
        #                print(autoProcProgramId)

        url2 = url2.replace("%autoprocprogramIds%", str(autoProcProgramId))
        listAttachment = extISPyB.getFromURL(url2)
        #        pprint.pprint(listAttachment)
        for attachment in listAttachment:
            if attachment["fileName"].endswith("noanom_aimless.mtz"):
                #                pprint.pprint(attachment)
                autoProcProgramAttachmentId = attachment["autoProcProgramAttachmentId"]

        url3 = "https://ispyb-valid.esrf.fr/ispyb/ispyb-ws/rest/1144dacb5ac634b0bfb8fb29219f46e5d1c29e0f/proposal/MX415/mx/autoprocintegration/autoprocattachmentid/%autoProcAttachmentId%/download"
        url3 = url3.replace("%autoProcAttachmentId%", str(autoProcProgramAttachmentId))

        print(url3)

        fd, tmpFile = tempfile.mkstemp(suffix=".mtz")
        os.close(fd)
        print(tmpFile)
        urllib.urlretrieve(url3, tmpFile)
