import os
import glob
import json
import pprint
import datetime
import dateutil.parser
import requests

startDate = datetime.datetime(2016, 9, 13, 0, 0)
endDate = datetime.datetime(2016, 9, 13, 0, 0)
token = None
proposal = "opid231"
password = "???????"
ispybWebServiceURL1 = os.path.join(
    "http://ispyb:8080/ispyb", "ispyb-ws", "rest", "authenticate?site='ESRF'"
)
dictUser = {"login": proposal, "password": password}
r = requests.post(ispybWebServiceURL1, dictUser)
if r.status_code == 200:
    dictResponse = json.loads(r.text)
    token = dictResponse["token"]
if token is not None:
    ispybWebServiceURL2 = os.path.join(
        "http://ispyb:8080/ispyb",
        "ispyb-ws",
        "rest",
        token,
        "proposal",
        proposal,
        "session",
        "list",
    )
    r = requests.get(ispybWebServiceURL2)
    listSession = []
    listMeshResultJsonPath = []
    if r.status_code == 200:
        dictResponse = json.loads(r.text)
        for session in dictResponse:
            # Filter dates
            strDate = session["BLSession_startDate"]
            sessionDate = dateutil.parser.parse(strDate)
            if (
                sessionDate.date() >= startDate.date()
                and sessionDate.date() <= endDate.date()
            ):
                #                    print(startDate)
                #                    pprint.pprint(session)
                listSession.append(session)
    for session in listSession:
        proposalCode = session["Proposal_proposalCode"]
        proposalNumber = session["Proposal_ProposalNumber"]
        sessionId = session["sessionId"]
        ispybWebServiceURL3 = os.path.join(
            "http://ispyb:8080/ispyb",
            "ispyb-ws",
            "rest",
            token,
            "proposal",
            str(proposalCode + proposalNumber),
            "mx",
            "datacollection",
            "session",
            str(sessionId),
            "list",
        )
        r = requests.get(ispybWebServiceURL3)
        listSession = []
        if r.status_code == 200:
            listDataCollection = json.loads(r.text)
            for dataCollection in listDataCollection:
                if "Workflow_workflowType" in dataCollection:
                    if "Mesh" in dataCollection["Workflow_workflowType"]:
                        imageDirectory = dataCollection["DataCollection_imageDirectory"]
                        processedDataDirectory = imageDirectory.replace(
                            "RAW_DATA", "PROCESSED_DATA"
                        )
                        # Find the workflow directory
                        listDir = glob.glob(os.path.join(processedDataDirectory, "*"))
                        for directory in listDir:
                            if os.path.basename(directory).startswith("Workflow"):
                                listWorkflowDir = glob.glob(
                                    os.path.join(directory, "*")
                                )
                                for meshDir in listWorkflowDir:
                                    if os.path.basename(meshDir).startswith(
                                        "MeshClustering"
                                    ):
                                        meshResultJsonPath = os.path.join(
                                            meshDir, "MeshResults.json"
                                        )
                                        if os.path.exists(meshResultJsonPath):
                                            listMeshResultJsonPath.append(
                                                meshResultJsonPath
                                            )
                                            break
    pprint.pprint(listMeshResultJsonPath)
