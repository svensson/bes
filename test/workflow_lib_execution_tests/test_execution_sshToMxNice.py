"""
Created on Feb 9, 2015

@author: svensson
"""

import unittest
import StringIO
import logging
import sshToMxNice


class Test(unittest.TestCase):

    def test_execution_sshToMxNice(self):
        ednaDpLaunchPath = "/data/visitor/mx415/id30a2/20150310/PROCESSED_DATA/xds_mx415_run1_1/edna-autoproc-launcher-6NUIFA.sh"
        result = sshToMxNice.run(ednaDpLaunchPath)
        print(result["log"])

    def tes_stringLogger(self):
        stringBuffer = StringIO.StringIO()
        logger = logging.getLogger()
        logHandler = logging.StreamHandler(stringBuffer)
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )
        logHandler.setFormatter(formatter)
        #        formatter = logging.Formatter('%(levelname)s %(message)s')
        #        logHandler.setFormatter(formatter)
        logHandler.setLevel(logging.DEBUG)
        logger.addHandler(logHandler)
        logger.setLevel(logging.DEBUG)

        #        stream_hdlr = logging.StreamHandler()
        #        stream_formatter = logging.Formatter('%(levelname)s %(message)s')
        #        stream_hdlr.setFormatter(stream_formatter)
        #        stream_hdlr.setLevel(logging.DEBUG)
        #        logger.addHandler(stream_hdlr)

        logger.debug("Debug...")
        logger.info("Info...")
        logger.warning("Warning...")
        logger.error("Error!")

        logHandler.flush()
        stringBuffer.flush()
        print([stringBuffer.getvalue()])


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_mxPressContactUser']
    unittest.main()
