import os
import json
import time
import pprint
import shutil
import unittest
import tempfile

from bes.workflow_lib import path
from bes.workflow_lib import grid
from bes import config


class TestMethods(unittest.TestCase):

    def setUp(self):
        path = os.path.abspath(__file__)
        self.testDataDirectory = os.path.join(
            os.path.dirname(os.path.dirname(path)), "data"
        )

    def tes_determineMeshPositions(self):
        beamline = "id30a1"
        motorPositons = {
            "sampx": 0.0,
            "sampy": 0.0,
            "focus": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "phi": 0.0,
            "kappa": 0.0,
            "kappa_phi": 0.0,
            "chi": 0.0,
        }
        run_number = 1
        expTypePrefix = "mesh-"
        prefix = "test"
        suffix = "cbf"
        osc_range = 1
        grid_info = """{"beam_height": 0.048148148148148148, "x1": -0.14814814814814814, "angle": 0, "dy_mm": 0.048148148148148148, "y1": -0.23703703703703705, "dx_mm": 0.90000000000000002, "steps_y": 2, "steps_x": 10, "id": "Grid - 4", "beam_width": 0.10000000000000001}"""
        _ = grid.determineMeshPositions(
            beamline,
            motorPositons,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )

    def tes_grid_create_result_html_page1D(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 11,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions1D = self.load_meshResults1D()
        bestPosition = grid.find_best_position(grid_info, meshPositions1D)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions1D,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        pprint.pprint(dict_html)
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page1D_id29_20140218_1_5(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 15,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions1D_id29_20140218_1_5 = self.load_meshResults1D_id29_20140218_1_5()
        bestPosition = grid.find_best_position(
            grid_info, meshPositions1D_id29_20140218_1_5
        )
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions1D_id29_20140218_1_5,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page1D_id29_20140218_1_7(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 15,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions1D_id29_20140218_1_7 = self.load_meshResults1D_id29_20140218_1_7()
        bestPosition = grid.find_best_position(
            grid_info, meshPositions1D_id29_20140218_1_7
        )
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions1D_id29_20140218_1_7,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page1D_vertical(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "x1": -0.0052380952380952379,
            "angle": 0,
            "dy_mm": 0.028846153846153848,
            "y1": -0.028846153846153848,
            "dx_mm": 0.0,
            "steps_y": 4,
            "steps_x": 1,
            "id": "Grid - 5",
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshResults1D_vertical = self.load_meshResults1D_vertical()
        bestPosition = grid.find_best_position(grid_info, meshResults1D_vertical)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshResults1D_vertical,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D(self):
        beamline = "simulator"
        directory = "/tmp_14_days/svensson/RAW_DATA"
        grid_info = {
            "steps_x": 20,
            "angle": 6.2824920881613364,
            "dy_mm": 0.090000003576278687,
            "y1": -0.10179200000000001,
            "dx_mm": 0.68999999761581421,
            "steps_y": 3,
            "x1": 0.55247500000000005,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory(
            "/tmp_14_days/svensson/RAW_DATA"
        )
        meshPositions2D = self.load_meshResults2D()
        bestPosition = grid.find_best_position(grid_info, meshPositions2D)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "ssh -X svensson@rnice8 firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_square(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "x1": 0.1357142857142857,
            "angle": 0.8550973962666929,
            "dy_mm": 0.2393060028553009,
            "y1": -0.025000000000000001,
            "dx_mm": 0.2393060028553009,
            "steps_y": 3,
            "steps_x": 3,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions2D_square = self.load_meshResults2D_square()
        bestPosition = grid.find_best_position(grid_info, meshPositions2D_square)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D_square,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_rectangle(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "x1": -0.36071428571428571,
            "angle": -5.1520817022062397,
            "dy_mm": 0.085980996489524841,
            "y1": -0.18437500000000001,
            "dx_mm": 0.43706798553466797,
            "steps_y": 2,
            "steps_x": 6,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions2D_rectangle = self.load_meshResults2D_rectangle()
        bestPosition = grid.find_best_position(grid_info, meshPositions2D_rectangle)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D_rectangle,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def test_grid_create_result_html_page2D_10x10(self):
        beamline = "simulator"
        directory = "/data/scisoft/pxsoft/data/WORKFLOW_test_DATA/id30a1/20141003/RAW_DATA/MXPressE_01"
        grid_info = {
            "x1": 0.091331999999999997,
            "angle": 0.0,
            "dy_mm": 0.113987997174263,
            "y1": -0.062726999999999991,
            "dx_mm": 0.17770799994468689,
            "steps_y": 10,
            "steps_x": 10,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions2D_10x10 = self.load_meshResults2D_10x10()
        bestPosition = grid.find_best_position(grid_info, meshPositions2D_10x10)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D_10x10,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_54x48(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "x1": 0.091331999999999997,
            "angle": 0.0,
            "dy_mm": 0.113987997174263,
            "y1": -0.062726999999999991,
            "dx_mm": 0.17770799994468689,
            "steps_y": 48,
            "steps_x": 54,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions2D_54x48 = self.load_meshResults2D_54x48()
        all_positions = grid.find_all_positions(grid_info, meshPositions2D_54x48)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D_54x48,
            all_positions,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_50x47(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "x1": 0.091331999999999997,
            "angle": 0.0,
            "dy_mm": 0.113987997174263,
            "y1": -0.062726999999999991,
            "dx_mm": 0.17770799994468689,
            "steps_y": 47,
            "steps_x": 50,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions2D_50x47 = self.load_meshResults2D_50x47()
        all_positions = grid.find_all_positions(grid_info, meshPositions2D_50x47)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D_50x47,
            all_positions,
            directory,
            workflow_working_dir,
            data_threshold=3000,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_40x15_cogfail(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "x1": 0.091331999999999997,
            "angle": 0.0,
            "dy_mm": 0.42,
            "y1": -0.062726999999999991,
            "dx_mm": 0.9,
            "steps_y": 15,
            "steps_x": 40,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions = self.load_meshResults2D_40x15_cogfail()
        best_position = grid.find_best_position(grid_info, meshPositions)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            best_position,
            directory,
            workflow_working_dir,
        )
        pprint.pprint(dict_html)
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)

    #        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_20140917(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "beam_height": 0.009777560498655585,
            "x1": -0.070887313615252995,
            "angle": 0,
            "dy_mm": 0.11733072598386701,
            "y1": -0.047665607430945979,
            "dx_mm": 0.14666340747983378,
            "steps_y": 13,
            "steps_x": 16,
            "id": "Grid - 2",
            "beam_width": 0.009777560498655585,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions = self.load_meshResults2D_20140917()
        bestPosition = grid.find_best_position(grid_info, meshPositions)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        #         all_positions = grid.find_all_positions(grid_info, meshPositions, upper_threshold=0.5, lower_threshold=0.1)
        #         dict_html = grid.grid_create_result_html_page(beamline, grid_info, meshPositions, all_positions, directory, workflow_working_dir)
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_tls_3(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        grid_info = {
            "beam_height": 0.0074180000000000001,
            "x1": -0.36531000000000002,
            "angle": 0,
            "dy_mm": 0.415408,
            "y1": -0.33751900000000001,
            "dx_mm": 0.81918000000000002,
            "steps_y": 57,
            "steps_x": 112,
            "id": "Grid - 2",
            "beam_width": 0.0073800000000000003,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions = self.load_meshResults2D_tls_3()
        listBestPositions = grid.find_all_positions(
            grid_info, meshPositions, radius=3.0
        )
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            listBestPositions,
            directory,
            workflow_working_dir,
        )
        #         all_positions = grid.find_all_positions(grid_info, meshPositions, upper_threshold=0.5, lower_threshold=0.1)
        #         dict_html = grid.grid_create_result_html_page(beamline, grid_info, meshPositions, all_positions, directory, workflow_working_dir)
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_remove_position_from_array(self):
        grid_info = {
            "beam_height": 0.0074180000000000001,
            "x1": -0.36531000000000002,
            "angle": 0,
            "dy_mm": 0.415408,
            "y1": -0.33751900000000001,
            "dx_mm": 0.81918000000000002,
            "steps_y": 57,
            "steps_x": 112,
            "id": "Grid - 2",
            "beam_width": 0.0073800000000000003,
        }
        _ = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions = self.load_meshResults2D_tls_3()
        best_position = grid.find_best_position(
            grid_info, meshPositions, upper_threshold=0.90
        )
        dataArray = grid._createDataArray(grid_info, meshPositions)
        grid.remove_position_from_array(dataArray, best_position)

    def tes_mesh_scan_statistics(self):
        dict_statictics = grid.mesh_scan_statistics(self.load_meshResults2D())
        strStatistics = dict_statictics["message"]
        print(strStatistics)
        self.assertNotEqual(None, strStatistics)

    def tes_find_best_position2D_54x48(self):
        grid_info = {
            "x1": 0.091331999999999997,
            "angle": 0.0,
            "dy_mm": 0.113987997174263,
            "y1": -0.062726999999999991,
            "dx_mm": 0.17770799994468689,
            "steps_y": 48,
            "steps_x": 54,
        }
        bestPosition = grid.find_all_positions(
            grid_info, self.load_meshResults2D_54x48()
        )
        self.assertNotEqual(None, bestPosition)

    def tes_find_best_position2D_50x47(self):
        grid_info = {
            "x1": 0.091331999999999997,
            "angle": 0.0,
            "dy_mm": 0.113987997174263,
            "y1": -0.062726999999999991,
            "dx_mm": 0.17770799994468689,
            "steps_y": 47,
            "steps_x": 50,
        }
        bestPosition = grid.find_best_position(
            grid_info, self.load_meshResults2D_50x47()
        )
        self.assertNotEqual(None, bestPosition)

    def tes_plotResults2D_54x48(self):
        beamline = "simulator"
        grid_info = {
            "x1": 0.091331999999999997,
            "angle": 0.0,
            "dy_mm": 0.113987997174263,
            "y1": -0.062726999999999991,
            "dx_mm": 0.17770799994468689,
            "steps_y": 48,
            "steps_x": 54,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        meshPositions2D_54x48 = self.load_meshResults2D_54x48()
        all_positions = grid.find_all_positions(grid_info, meshPositions2D_54x48)
        plotFile = grid.create_plot_file(
            beamline,
            grid_info,
            meshPositions2D_54x48,
            all_positions,
            workflow_working_dir,
        )
        self.assertTrue(os.path.exists(os.path.join(workflow_working_dir, plotFile)))

    #         shutil.rmtree(workflow_working_dir)

    def tes_plotResults_MeshResults_20150702_150011(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        f = open(
            os.path.join(self.testDataDirectory, "MeshResults_20150702-150011.json"),
            "r",
        )
        strData = f.read()
        meshResults = json.loads(strData)
        f.close()
        grid_info = meshResults["grid_info"]
        meshPositions = meshResults["meshPositions"]
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        bestPosition = grid.find_best_position(grid_info, meshPositions)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)

    #        shutil.rmtree(workflow_working_dir)

    def tes_plotResults_MeshResults_20150705_162241(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        f = open(
            os.path.join(self.testDataDirectory, "MeshResults_20150705-162241.json"),
            "r",
        )
        strData = f.read()
        meshResults = json.loads(strData)
        f.close()
        grid_info = meshResults["grid_info"]
        meshPositions = meshResults["meshPositions"]
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        bestPosition = grid.find_best_position(grid_info, meshPositions)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)

    #        shutil.rmtree(workflow_working_dir)

    def tes_findBestPosition_20150720_114555(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        f = open(
            os.path.join(self.testDataDirectory, "MeshResults_20150720-114555.json"),
            "r",
        )
        strData = f.read()
        meshResults = json.loads(strData)
        f.close()
        grid_info = meshResults["grid_info"]
        meshPositions = meshResults["meshPositions"]
        bestPosition = grid.find_best_position(grid_info, meshPositions, useDozor=False)
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            bestPosition,
            directory,
            workflow_working_dir,
            useDozor=False,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_findBestPosition_20160704(self):
        with open(
            "/data/visitor/mx415/id30a2/20160704/PROCESSED_DATA/t1/MeshScan_01/Workflow_20160704-134750/MeshClustering_v4/MeshResults.json"
        ) as f:
            strData = f.read()
        meshResults = json.loads(strData)
        grid_info = meshResults["grid_info"]
        meshPositions = meshResults["meshPositions"]
        bestPosition = grid.find_best_position(grid_info, meshPositions, useDozor=True)
        #        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        #        dict_html = grid.grid_create_result_html_page(beamline, grid_info, meshPositions, bestPosition, directory, workflow_working_dir, useDozor=True)
        #        os.system("firefox %s" % os.path.join(dict_html["html_directory_path"], dict_html["index_file_name"]))
        #        time.sleep(1)
        #        shutil.rmtree(workflow_working_dir)
        pprint.pprint(bestPosition)

    def tes_findBestPosition_20160704_MeshClustering_v4(self):
        # resultMeshPath = "/data/visitor/mx1791/id23eh1/20161214/PROCESSED_DATA/AS074/AS074-AS8c1_4a/MeshResults/MeshResults_20161214-161400.json"
        #        resultMeshPath = "/data/visitor/mx415/id30a2/20170302/PROCESSED_DATA/t1/MeshResults/MeshResults_20170302-094110.json"
        # resultMeshPath = "/data/visitor/mx415/id30a2/20170309/PROCESSED_DATA/t1/MeshResults/MeshResults_20170309-095505.json"
        resultMeshPath = "/data/visitor/mx1819/id30a1/20180628/PROCESSED_DATA/BRU/BRU-CD023347_C02-3/MeshResults/MeshResults_20180628-165705.json"
        with open(resultMeshPath) as f:
            strData = f.read()
        meshResults = json.loads(strData)
        grid_info = meshResults["grid_info"]
        bestPosition, plotPath, meshBestResults = grid.find_best_position_mcv4(
            grid_info, resultMeshPath
        )
        #        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        #        dict_html = grid.grid_create_result_html_page(beamline, grid_info, meshPositions, bestPosition, directory, workflow_working_dir, useDozor=True)
        #        os.system("firefox %s" % os.path.join(dict_html["html_directory_path"], dict_html["index_file_name"]))
        #        time.sleep(1)
        #        shutil.rmtree(workflow_working_dir)
        pprint.pprint(bestPosition)
        pprint.pprint(meshBestResults)
        print(plotPath)

    def tes_findBestPosition_linescan(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        resultMeshPath = "/data/id30a2/inhouse/opid30a2/20180629/PROCESSED_DATA/t2/MeshResults/MeshResults_20180629-162007.json"
        with open(resultMeshPath) as f:
            strData = f.read()
        meshResults = json.loads(strData)
        grid_info = meshResults["grid_info"]
        meshPositions = meshResults["meshPositions"]
        bestPosition, plotPath, meshBestResults = grid.find_best_position_mcv4(
            grid_info, resultMeshPath, meshBestType="linescan"
        )
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            bestPosition,
            directory,
            workflow_working_dir,
            useDozor=True,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        #        shutil.rmtree(workflow_working_dir)
        pprint.pprint(bestPosition)
        pprint.pprint(meshBestResults)
        print(plotPath)

    def tes_findBestPosition_linescan_with_2_peaks(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        resultMeshPath = "/data/visitor/mx1942/id30a1/20180709/PROCESSED_DATA/Lyso/Lyso-CD022381_C09-1/MeshResults/MeshResults_20180709-150320.json"
        resultMeshPath = "/data/visitor/mx1942/id30a1/20180709/PROCESSED_DATA/Lyso/Lyso-CD022381_C09-2/MeshResults/MeshResults_20180709-153351.json"
        with open(resultMeshPath) as f:
            strData = f.read()
        meshResults = json.loads(strData)
        grid_info = meshResults["grid_info"]
        meshPositions = meshResults["meshPositions"]
        bestPosition, meshBestPlotPath, meshBestPositions = (
            grid.find_best_position_mcv4(
                grid_info, resultMeshPath, meshBestType="linescan"
            )
        )
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            bestPosition,
            directory,
            workflow_working_dir,
            useDozor=True,
            meshBestPositions=meshBestPositions,
            meshBestPlotPath=meshBestPlotPath,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        #        shutil.rmtree(workflow_working_dir)
        pprint.pprint(bestPosition)
        print(meshBestPlotPath)

    def tes_findAllPositions_mesh_with_three_crystals(self):
        beamline = "simulator"
        directory = "/tmp/RAW_DATA"
        resultMeshPath = "/data/visitor/mx1942/id30a1/20180709/PROCESSED_DATA/Lyso/Lyso-CD022381_C09-2/MeshResults/MeshResults_20180709-161411.json"
        resultMeshPath = "/data/visitor/mx1977/id30a3/20180826/PROCESSED_DATA/Alzari/OdhA/OdhA-12749_E12_3_oil/MeshResults/MeshResults_20180826-160918.json"
        with open(resultMeshPath) as f:
            strData = f.read()
        meshResults = json.loads(strData)
        grid_info = meshResults["grid_info"]
        meshPositions = meshResults["meshPositions"]
        bestPositions, plotPath, meshBestResults = grid.find_all_positions_mcv4(
            grid_info, resultMeshPath, meshBestType="xraycentering"
        )
        workflow_working_dir = path.createWorkflowWorkingDirectory("/tmp/RAW_DATA")
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            bestPositions,
            directory,
            workflow_working_dir,
            useDozor=True,
        )
        os.system(
            "firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        #        shutil.rmtree(workflow_working_dir)
        pprint.pprint(bestPositions)
        print(plotPath)

    def tes_findRotationAxisOffset(self):
        mesh1D = self.load_meshResults1D()
        grid_info = {
            "angle": 0.0,
            "dx_mm": 0.10,
            "dy_mm": 0.11,
            "steps_x": 11,
            "steps_y": 1,
            "x1": -0.104,
            "y1": -0.062,
        }
        offset = grid.findRotationAxisOffset(grid_info, mesh1D, mesh1D)
        print("offset: %f" % offset)

    def tes_findRotationAxisOffset_vertical(self):
        mesh1D = self.load_meshResults1D_vertical()
        grid_info = {
            "x1": -0.0052380952380952379,
            "angle": 0,
            "dy_mm": 0.028846153846153848,
            "y1": -0.028846153846153848,
            "dx_mm": 0.0,
            "steps_y": 4,
            "steps_x": 1,
            "id": "Grid - 5",
        }
        offset = grid.findRotationAxisOffset(grid_info, mesh1D, mesh1D)
        print("offset: %f" % offset)

    def tes_loopExam(self):
        snapshotDir = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_02/Workflow_20210527-111745"
        prefix = "snapshot"
        background_image = os.path.join(snapshotDir, "%s_background.png" % prefix)
        omega = 0
        autoMeshWorkingDir = tempfile.mkdtemp(prefix="autoMesh_")
        os.chmod(autoMeshWorkingDir, 0o755)
        image = os.path.join(snapshotDir, "%s_%03d.png" % (prefix, omega))
        (listIndex, listUpper, listLower) = grid.loopExam(
            image, background_image, 90, autoMeshWorkingDir=autoMeshWorkingDir
        )
        shutil.rmtree(autoMeshWorkingDir)

    def tes_autoMesh(self):
        # snapshotDir = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_02/Workflow_20210527-111745"
        snapshotDir = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_23/Workflow_20210527-114808"
        workflowWorkingDir = tempfile.mkdtemp(
            prefix="autoMesh_", dir="/tmp_14_days/svensson"
        )
        print(workflowWorkingDir)
        os.chmod(workflowWorkingDir, 0o755)
        (
            angleMinThickness,
            x1Pixels,
            y1Pixels,
            dxPixels,
            dyPixels,
            deltaPhiz,
            stdPhiz,
            imagePath,
            areTheSameImag,
        ) = grid.autoMesh(
            snapshotDir, workflowWorkingDir, workflowWorkingDir, debug=True
        )
        print(angleMinThickness, x1Pixels, y1Pixels, dxPixels, dyPixels, deltaPhiz)

    #        shutil.rmtree(autoMeshWorkingDir)

    def tes_centrePin(self):
        # snapshotDir = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_02/Workflow_20210527-111745"
        snapshotDir = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_23/Workflow_20210527-114808"
        listSnapshotDir = [
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_02/Workflow_20210527-111745",
                1,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_03/Workflow_20210527-111928",
                1,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_04/Workflow_20210527-112227",
                5,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_05/Workflow_20210527-112349",
                5,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_06/Workflow_20210527-112527",
                7,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_07/Workflow_20210527-112725",
                7,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_08/Workflow_20210527-112853",
                1,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_09/Workflow_20210527-113016",
                1,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_10/Workflow_20210527-113123",
                5,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_11/Workflow_20210527-113226",
                7,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_12/Workflow_20210527-113403",
                7,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_13/Workflow_20210527-113532",
                1,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_14/Workflow_20210527-113634",
                5,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_15/Workflow_20210527-113803",
                5,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_16/Workflow_20210527-113907",
                7,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_17/Workflow_20210527-114032",
                7,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_18/Workflow_20210527-114158",
                1,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_19/Workflow_20210527-114332",
                1,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_20/Workflow_20210527-114452",
                1,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_21/Workflow_20210527-114550",
                5,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_22/Workflow_20210527-114648",
                7,
            ],
            [
                "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210527/snapshots/CentrePin_23/Workflow_20210527-114808",
                7,
            ],
        ]
        listSnapshotDir = [
            [
                "/data/id30a1/inhouse/opid30a1/20210601/PROCESSED_DATA/test/test-test/CentrePin_01/Workflow_20210601-163139",
                4,
            ]
        ]
        for index, (snapshotDir, zoom) in enumerate(listSnapshotDir[0:]):
            print(snapshotDir)
            print("Zoom level: {0}".format(zoom))
            pixelPerMm = config.get_value("id30a1", "SnapShotZoomMmPerPixel", str(zoom))
            print("PixelPerMm: {0}".format(pixelPerMm))
            tungstenPixels = 0.020 * pixelPerMm / 2
            print("Tungsten pixels: {0}".format(tungstenPixels))
            workflowWorkingDir = tempfile.mkdtemp(
                prefix="findCentre_{0}_".format(index), dir="/tmp_14_days/svensson"
            )
            print(workflowWorkingDir)
            os.chmod(workflowWorkingDir, 0o755)
            deltaX, deltaY, deltaZ = grid.centrePin(
                snapshotDir, workflowWorkingDir, loopWidth=tungstenPixels, debug=True
            )
            print(deltaX, deltaY, deltaZ)
            print(deltaX / pixelPerMm, deltaY / pixelPerMm)
            with open(os.path.join(snapshotDir, "workflow_log.txt")) as fd:
                listLines = fd.readlines()
            for line in listLines:
                if "sampx:" in line and "sampy" not in line:
                    sampx = float(line.split("sampx:")[1])
                elif "sampy:" in line and "sampx" not in line:
                    sampy = float(line.split("sampy:")[1])
                elif "phiy:" in line and "sampx" not in line:
                    phiy = float(line.split("phiy:")[1])
            print("Old sampx: {0}, sampy: {1}, phiy: {2}".format(sampx, sampy, phiy))
            sampx = sampx + deltaX / pixelPerMm
            sampy = sampy + deltaY / pixelPerMm
            phiy = phiy + deltaZ / pixelPerMm
            print("New sampx: {0}, sampy: {1}, phiy: {2}".format(sampx, sampy, phiy))

            _ = input("Continue")

    #        shutil.rmtree(autoMeshWorkingDir)

    def tes_centrePin_2(self):
        snapshotParentDir = (
            "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20210614"
        )
        # Find all zoom level 7
        import glob

        listDir = glob.glob(os.path.join(snapshotParentDir, "kappa_60_*_zoom_7_*"))
        pprint.pprint(listDir)
        for index, snapshotDir in enumerate(listDir):
            print(snapshotDir)
            zoom = int(snapshotDir.split("zoom")[1].split("_")[1])
            print("Zoom level: {0}".format(zoom))
            pixelPerMm = config.get_value("id30a1", "SnapShotZoomMmPerPixel", str(zoom))
            print("PixelPerMm: {0}".format(pixelPerMm))
            tungstenPixels = 0.020 * pixelPerMm
            print("Tungsten pixels: {0}".format(tungstenPixels))
            workflowWorkingDir = tempfile.mkdtemp(
                prefix="findCentre_{0}_".format(index), dir="/tmp_14_days/svensson"
            )
            print(workflowWorkingDir)
            os.chmod(workflowWorkingDir, 0o755)
            if zoom >= 5:
                doCircleFit = True
            else:
                doCircleFit = False
            deltaX, deltaY, deltaZ = grid.centrePin(
                snapshotDir,
                workflowWorkingDir,
                loopWidth=tungstenPixels,
                doCircleFit=doCircleFit,
                debug=True,
            )
            print(deltaX, deltaY, deltaZ)
            print(deltaX / pixelPerMm, deltaY / pixelPerMm)

            _ = input("Continue")

    def load_meshResults(self):
        # Test mesh data
        f = open(os.path.join(self.testDataDirectory, "meshResults.json"), "r")
        strData = f.read()
        meshPositions = json.loads(strData)
        f.close()
        return meshPositions

    def load_meshResults2D(self):
        # Test mesh data
        f = open(os.path.join(self.testDataDirectory, "meshResults2D.json"), "r")
        strData = f.read()
        meshPositions2D = json.loads(strData)
        f.close()
        return meshPositions2D

    def load_meshResults2D_square(self):
        f = open(os.path.join(self.testDataDirectory, "meshResults2D_square.json"), "r")
        strData = f.read()
        meshPositions2D_square = json.loads(strData)
        f.close()
        return meshPositions2D_square

    def load_meshResults2D_rectangle(self):
        f = open(
            os.path.join(self.testDataDirectory, "meshResults2D_rectangle.json"), "r"
        )
        strData = f.read()
        meshPositions2D_rectangle = json.loads(strData)
        f.close()
        return meshPositions2D_rectangle

    def load_meshResults2D_10x10(self):
        f = open(os.path.join(self.testDataDirectory, "meshResults2D_10x10.json"), "r")
        strData = f.read()
        meshPositions2D_10x10 = json.loads(strData)
        f.close()
        return meshPositions2D_10x10

    def load_meshResults1D(self):
        f = open(os.path.join(self.testDataDirectory, "meshResults1D.json"), "r")
        strData = f.read()
        meshPositions1D = json.loads(strData)
        f.close()
        return meshPositions1D

    def load_meshResults1D_id29_20140218_1_5(self):
        f = open(
            os.path.join(
                self.testDataDirectory, "meshPositions1D_id29_20140218_1_5.json"
            ),
            "r",
        )
        strData = f.read()
        meshPositions1D_id29_20140218_1_5 = json.loads(strData)
        f.close()
        return meshPositions1D_id29_20140218_1_5

    def load_meshResults1D_id29_20140218_1_7(self):
        f = open(
            os.path.join(
                self.testDataDirectory, "meshPositions1D_id29_20140218_1-7.json"
            ),
            "r",
        )
        strData = f.read()
        meshPositions1D_id29_20140218_1_7 = json.loads(strData)
        f.close()
        return meshPositions1D_id29_20140218_1_7

    def load_meshResults1D_vertical(self):
        f = open(
            os.path.join(self.testDataDirectory, "meshResults1D_vertical.json"), "r"
        )
        strData = f.read()
        meshResults1D_vertical = json.loads(strData)
        f.close()
        return meshResults1D_vertical

    def load_meshResults2D_54x48(self):
        f = open(os.path.join(self.testDataDirectory, "meshResults2D_54x48.json"), "r")
        strData = f.read()
        meshPositions2D_54x48 = json.loads(strData)
        f.close()
        return meshPositions2D_54x48

    def load_meshResults2D_50x47(self):
        f = open(os.path.join(self.testDataDirectory, "meshResults2D_50x47.json"), "r")
        strData = f.read()
        meshPositions2D_50x47 = json.loads(strData)
        f.close()
        return meshPositions2D_50x47

    def load_meshResults2D_40x15_cogfail(self):
        f = open(
            os.path.join(self.testDataDirectory, "meshResults2D_40x15_cogfail.json"),
            "r",
        )
        strData = f.read()
        meshPositions = json.loads(strData)
        f.close()
        return meshPositions

    def load_meshResults2D_tls_3(self):
        f = open(os.path.join(self.testDataDirectory, "meshResults2D_tls_3.json"), "r")
        strData = f.read()
        meshPositions = json.loads(strData)
        f.close()
        return meshPositions

    def load_meshResults2D_20140917(self):
        f = open(
            os.path.join(self.testDataDirectory, "meshResults2D_20140917.json"), "r"
        )
        strData = f.read()
        meshPositions = json.loads(strData)
        f.close()
        return meshPositions

    def tes_grid_create_result_html_page2D_id30a2(self):
        beamline = "id30a2"
        directory = "/data/id30a2/inhouse/opid30a2/20210416/RAW_DATA/t1/MeshScan_02"
        grid_info = {
            "x1": -0.429,
            "dx_mm": 0.6,
            "dy_mm": 0.145,
            "y1": -0.112,
            "steps_y": 4,
            "steps_x": 18,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory(
            "/tmp_14_days/svensson/RAW_DATA"
        )
        meshPositions2D = json.loads(
            open(
                "/data/id30a2/inhouse/opid30a2/20210416/PROCESSED_DATA/t1/MeshScan_02/Workflow_20210416-105031/meshPositions2D_tydl0uph.json"
            ).read()
        )
        bestPosition = grid.find_best_position(grid_info, meshPositions2D)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "ssh -X svensson@rnice8 firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_id30a3(self):
        beamline = "id30a2"
        directory = "/data/id30a3/inhouse/opid30a3/20210416/RAW_DATA/TRYP/TRYP-trypsin3/XrayCentering_01"
        grid_info = {
            "x1": -0.338,
            "dx_mm": 0.585,
            "dy_mm": 0.435,
            "y1": -0.117,
            "steps_y": 30,
            "steps_x": 40,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory(
            "/tmp_14_days/svensson/RAW_DATA"
        )
        meshPositions2D = json.loads(
            open(
                "/data/id30a3/inhouse/opid30a3/20210416/PROCESSED_DATA/TRYP/TRYP-trypsin3/XrayCentering_01/Workflow_20210416-091301/meshPositions2D_825xw41f.json"
            ).read()
        )
        bestPosition = grid.find_best_position(grid_info, meshPositions2D)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "ssh -X svensson@rnice8 firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def tes_grid_create_result_html_page2D_id30a3_2(self):
        beamline = "id30a3"
        directory = "/data/visitor/mx2292/id30a3/20210420/RAW_DATA/CNB/RBD-Nb-x8/XrayCentering_01"
        grid_info = {
            "x1": -0.338,
            "dx_mm": 0.585,
            "dy_mm": 0.435,
            "y1": -0.117,
            "steps_y": 9,
            "steps_x": 11,
        }
        workflow_working_dir = path.createWorkflowWorkingDirectory(
            "/tmp_14_days/svensson/RAW_DATA"
        )
        meshPositions2D = json.loads(
            open(
                "/data/visitor/mx2292/id30a3/20210420/PROCESSED_DATA/CNB/RBD-Nb-x8/XrayCentering_01/Workflow_20210420-110101/meshPositions2D_xi69rt63.json"
            ).read()
        )
        # meshPositions2D = json.loads(open("/data/visitor/mx2292/id30a3/20210420/PROCESSED_DATA/CNB/RBD-Nb-x8/XrayCentering_01/Workflow_20210420-110101/meshPositionsDozorM_mesh-x8m_0_kknxvdou.json").read())
        bestPosition = grid.find_best_position(grid_info, meshPositions2D)
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions2D,
            bestPosition,
            directory,
            workflow_working_dir,
        )
        os.system(
            "ssh -X svensson@rnice8 firefox %s"
            % os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
        )
        time.sleep(1)
        shutil.rmtree(workflow_working_dir)

    def test_getDirection(self):
        meshPositions2D_10x10 = self.load_meshResults2D_10x10()
        # pprint.pprint(meshPositions2D_10x10)
        grid_info = {
            "x1": 0.091331999999999997,
            "angle": 0.0,
            "dy_mm": 0.113987997174263,
            "y1": -0.062726999999999991,
            "dx_mm": 0.17770799994468689,
            "steps_y": 10,
            "steps_x": 10,
        }
        indexY = 2
        indexZ = 2
        dir_x, dir_y = grid.getDirection(
            meshPositions2D_10x10, grid_info, indexY, indexZ
        )
        print(dir_x, dir_y)

    def test_find_all_position_dozorm(self):
        grid_info = {
            "x1": -0.4794,
            "y1": -0.1898,
            "dx_mm": 0.5833,
            "dy_mm": 0.4306,
            "steps_x": 19,
            "steps_y": 14,
            "cell_height": 30,
            "cell_width": 30,
        }
        outDataDozorM2 = json.loads(
            open(
                "/data/visitor/oa14/id23eh1/20240507/PROCESSED_DATA/XDLD9/XDLD9-apo-01/run_02_MXPressF/Workflow_20240507-142145/DozorM2_mesh-XDLD9-apo-01_2_3/outDataDozorM2.json"
            ).read()
        )
        meshPositions = path.parseJsonFile(
            "/data/visitor/oa14/id23eh1/20240507/PROCESSED_DATA/XDLD9/XDLD9-apo-01/run_02_MXPressF/Workflow_20240507-142145/meshPositionsDozorM_mesh-XDLD9-apo-01_2_3_1y64q_ud.json"
        )
        dozormListPositions = outDataDozorM2["dictCoord"]["scan1"]
        numberOfPositions = 2
        isHorizontalRotationAxis = True
        workflow_working_dir = None
        correct_dozorm2 = False
        all_positions = grid.find_all_position_dozorm(
            listPositions=dozormListPositions,
            grid_info=grid_info,
            meshPositions=meshPositions,
            maxNoPositions=numberOfPositions,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
            workflowWorkingDir=workflow_working_dir,
            correct_dozorm2=correct_dozorm2,
        )
        pprint.pprint(all_positions)
