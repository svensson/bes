"""
Created on Feb 9, 2015

@author: svensson
"""

import os
import unittest
import tempfile
import ednaCharacterisation


class Test(unittest.TestCase):

    def test_execution_ednaCharacterisation(self):
        testDir = tempfile.mkdtemp(prefix="test_ednaCharacterisation_")
        inputFile = "/data/id29/inhouse/opid291/20150302/PROCESSED_DATA/opid291/manually-mounted/EDNAInput_1393638.xml"
        outputFile = os.path.join(testDir, "EDNAOutput_1393638.xml")
        pluginName = "EDPluginControlInterfaceToMXCuBEv1_3"
        ednaCharacterisation.run(
            inputFile=inputFile,
            outputFile=outputFile,
            workingDirectory=testDir,
            pluginName=pluginName,
        )


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_mxPressContactUser']
    unittest.main()
