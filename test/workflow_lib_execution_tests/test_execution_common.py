import unittest
from bes.workflow_lib import common


class TestMethods(unittest.TestCase):

    def tes_sendEmail(self):
        beamline = "simulator2"
        subject = "Test subject"
        message = "Test message"
        replyTo = "svensson@esrf.fr"
        listBCC = ["s.olof.svensson@gmail.com"]
        listTo = ["svensson@esrf.fr"]
        common.sendEmail(
            beamline, listTo, subject, message, replyTo=replyTo, listBCC=listBCC
        )

    def tes_send_email(self):
        directory = "/tmp"
        beamline = "simulator2"
        workflow_title = "Test send email"
        workflow_type = "Test email unit test"
        workflow_working_dir = "/tmp"
        common.send_email(
            beamline,
            ["svensson@esrf.fr", "s.olof.svensson@gmail.com"],
            "test_send_email",
            directory,
            workflow_title,
            workflow_type,
            workflow_working_dir,
            str_message="Test message",
            str_me="workflow@esrf.fr",
        )

    def tes_getListPsAux(self):
        list_ps_aux = common.getListPsAux()
        print(list_ps_aux)
        list_ids = common.getDefunctPythonIds(list_ps_aux, "opid23")
        print(list_ids)

    def tes_getBESWorkflowStatus(self):
        bes_parameters = {"host": "localhost", "port": 5000, "request_id": 2955656}
        status = common.getBESWorkflowStatus(bes_parameters)
        print(status)
