import csv
import pathlib
import unittest
import pprint
import datetime
from bes.workflow_lib import statistics


class TestMethods(unittest.TestCase):

    def tes_createMXPressCSVFile(self):
        proposal = "all"
        startYear = 2018
        startMonth = 1
        startDay = 1
        endYear = 2018
        endMonth = 4
        endDay = 26
        beamline = "id30b"
        maxResults = None
        startDate = datetime.datetime(startYear, startMonth, startDay, 0, 0)
        endDate = datetime.datetime(endYear, endMonth, endDay, 23, 59)
        fileName = "/tmp_14_days/svensson/{beamline}_{startYear}{startMonth:02d}{startDay:02d}_{endYear}{endMonth:02d}{endDay:02d}_besdb4.csv".format(
            beamline=beamline,
            startYear=startYear,
            startMonth=startMonth,
            startDay=startDay,
            endYear=endYear,
            endMonth=endMonth,
            endDay=endDay,
        )
        print(fileName)
        statistics.createMXPressCSVFile(
            proposal,
            startDate,
            endDate,
            fileName=fileName,
            beamline=beamline,
            maxResults=maxResults,
        )

    def tes_createExperimentTypeSelectedCsvFile(self):
        proposal = "all"
        startYear = 2017
        startMonth = 3
        startDay = 21
        endYear = 2017
        endMonth = 3
        endDay = 21
        beamline = "id30a1"
        startDate = datetime.datetime(startYear, startMonth, startDay, 0, 0)
        endDate = datetime.datetime(endYear, endMonth, endDay, 23, 59)
        fileName = "/tmp_14_days/svensson/{beamline}_{startYear}{startMonth:02d}{startDay:02d}_{endYear}{endMonth:02d}{endDay:02d}.csv".format(
            beamline=beamline,
            startYear=startYear,
            startMonth=startMonth,
            startDay=startDay,
            endYear=endYear,
            endMonth=endMonth,
            endDay=endDay,
        )
        print(fileName)
        statistics.createExperimentTypeSelectedCsvFile(
            proposal, startDate, endDate, fileName=fileName, beamline=beamline
        )

    def tes_getAverageCrystalVolumeLastWeek(self):
        beamline = "id30a1"
        startYear = 2017
        startMonth = 1
        startDay = 1
        endYear = 2018
        endMonth = 12
        endDay = 18
        startDate = datetime.datetime(startYear, startMonth, startDay, 0, 0)
        endDate = datetime.datetime(endYear, endMonth, endDay, 23, 59)
        dictResult = statistics.getCrystalVolumeStatisticsForLastWeek(
            beamline, startDate=startDate, endDate=endDate
        )
        pprint.pprint(dictResult)
        print("Average volume: {0}".format(dictResult["averageVolume"]))
        print("Mode volume: {0}".format(dictResult["modeVolume"]))
        print("Min crystal length: {0}".format(dictResult["minCrysatlLength"]))
        print("Max crystal length: {0}".format(dictResult["maxCrysatlLength"]))
        self.assertTrue("averageVolume" in dictResult)

    def test_getTotalNumberOfMXPress(self):
        beamline = "id30a2"
        year = 2022
        noMXPress = statistics.getTotalNumberOfMXPress(beamline, year)
        print(noMXPress)

    def test_getNumberOfMXPress(self):
        workflow_type = "MXPressA"
        beamlines = ["id23eh1"]
        for beamline in beamlines:
            no_workflows = statistics.getNumberOfMXPress(
                beamline, 2023, 11, 29, 13, 0, 2023, 11, 30, 2, 0, type=workflow_type
            )
            print(f"Beamline: {beamline}, no {workflow_type}: {no_workflows}")

    def test_getTotalNumberOfMXPress2(self):
        workflow_type = "MXPress"
        beamlines = ["id30a1", "id30a3", "id30b", "id23eh1", "id23eh2"]
        year = 2024
        for beamline in beamlines:
            no_workflows = statistics.getTotalNumberOfMXPress(
                beamline, year, type=workflow_type
            )
            print(f"Beamline: {beamline}, no {workflow_type}: {no_workflows}")

    def test_getRunningWorkflows(self):
        statistics.getRunningWorkflows()

    def test_getSlurmStartDelay(self):
        date1 = "2024-01-01"
        date2 = "2024-02-01"
        name = "autoPROC_SLURM"
        # pprint.pprint(listSlurmStartDelay)
        list_delay = []
        no_excluded = 0
        csv_path = pathlib.Path(f"/tmp_14_days/svensson/{name}_{date1}_{date2}.csv")
        if csv_path.exists():
            with open(csv_path) as f:
                csv_reader = csv.reader(f)
                for row in csv_reader:
                    delay = float(row[5])
                    if delay < 100:
                        list_delay.append(delay)
                    else:
                        no_excluded += 1
        else:
            listSlurmStartDelay = statistics.getSlurmStartDelay(date1, date2, name=name)
            with open(f"/tmp_14_days/svensson/{name}_{date1}_{date2}.csv", "w") as fd:
                for entry in listSlurmStartDelay:
                    # print(len(entry))
                    if len(entry) == 8:
                        # print([entry])
                        dateTimeString = entry[0].split(".")[0]
                        listTimes = list(map(str, entry[1:]))
                        csvString = dateTimeString + "," + ",".join(listTimes)
                        fd.write(csvString + "\n")
                        delay = float(listTimes[4])
                        if delay < 10:
                            list_delay.append(delay)
        # pprint.pprint(list_delay)
        print(f"Total number: {len(list_delay)}")
        print(f"No excleded: {no_excluded}")
        import matplotlib.pyplot as pyplot

        num_bins = 100
        fig, ax = pyplot.subplots()
        # pyplot.xscale('log')
        pyplot.yscale("log")
        n, bins, patches = ax.hist(list_delay, num_bins, density=False)
        pyplot.show()

    def tes_getTotalNumberOfMXPressOrig(self):
        beamline = "id30a3"
        year = 2018
        noMXPress = statistics.getTotalNumberOfMXPressOrig(beamline, year)
        print(noMXPress)

    def tes_checkCharacterisation(self):
        date1 = "2020-11-20"
        date2 = "2020-11-23"
        listResults = statistics.checkCharacterisation(date1, date2)
        pprint.pprint(listResults)
