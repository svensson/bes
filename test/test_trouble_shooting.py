#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "28/05/2019"

import os
import logging
import pathlib

from ewoksppf import execute_graph
from ewokscore import load_graph

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("bes")


def test_TroubleShooting():
    bes_home = pathlib.Path(__file__).parents[2]
    flows_dir = os.path.join(bes_home, "bes", "src", "bes", "flows")
    os.environ["EDNA_SITE"] = "ESRF"
    inData = {
        "prefix": "x1",
        "run_number": 1,
        "sample_lims_id": 0,
        "beamline": "simulator",
        "directory": "/data/id30a2/inhouse/opid30a2/20190528/RAW_DATA",
    }
    taskGraph = os.path.join(flows_dir, "{0}.json".format("TroubleShooting"))
    graph = load_graph(taskGraph, root_dir=flows_dir)

    execute_graph(
        graph, load_options={"root_dir": flows_dir}, startargs=inData, pre_import=False
    )
