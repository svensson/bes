# BES - The Beamline Expert System

The BES project can be installed like any python package.

## Start BES

Start the BES Flask server for testing (werkzeug WSGI)

```bash
flask --app bes.bin.ppf_server run [--host 127.0.0.1] [--port 5000] [--debug]
```

Start the BES Flask server in production (gunicorn WSGI)

```bash
gunicorn bes.bin.ppf_server:app [--host 127.0.0.1] [--port 8000] [--log-level DEBUG]
```

These environment variables are used by BES

* `BESDB_HOST`: Mongo database host where workflow events are stored
* `BESDB_PORT`: Mongo database port where workflow events are stored

These variables are also used by the workflows, in addition to the following list

* `EDNA_SITE`: Facility + Beamline name for the EDNA workflow system
* `EDNA2_SITE`: Facility + Beamline name for the EDNA2 workflow system
* `PYPUSHFLOW_INITIATOR`: Beamline name for the pypushflow workflow engine

## BES Mongo database

All BES workflows triggered through the BES server need a BES Mongo database.

Install and start a Mongo database

```bash
conda install -c conda-forge mongodb
mkdir mongodb
mongod --dbpath $(pwd)/mongodb [--bind_ip 127.0.0.1] [--port 27017]
```

Install the MongoDb shell

```bash
mkdir mongosh
wget -qO- "https://github.com/mongodb-js/mongosh/releases/download/v2.2.11/mongosh-2.2.11-linux-x64.tgz" | tar xvz -C ./mongosh --strip-components=1
alias mongo=$(pwd)/mongosh/bin/mongosh
```

Create the `bes` user

```bash
mongo --authenticationDatabase admin --eval 'db = db.getSiblingDB("bes"); db.createUser({user: "bes", pwd: "bes", roles: [{role: "readWrite", db: "bes"}]})' [--host 127.0.0.1] [--port 27017]
```

## Run a Workflow

The test workflow is called `BesTestExecution` and needs an XML-RPC server

```bash
python -m bes.tests.utils.xmlrpc_server [--host 127.0.0.1] [--port 8080] [--log-level DEBUG]
```

Run the `BesTestExecution` workflow without the BES server

```bash
JSON_DATA="{ \"callBackHost\": \"127.0.0.1\", \"callBackPort\": \"8080\", \"besParameters\": {\"request_id\": \"$(uuidgen)\"} }"

bes-run-workflow BesTestExecution --inData="${JSON_DATA}" [--log-level DEBUG] [--ppf-log-level DEBUG] [--ewoks-log-level DEBUG]
```

Run a workflow with the BES server (`besParameters` is provided by the server)

```bash
JSON_DATA="{ \"callBackHost\": \"127.0.0.1\", \"callBackPort\": \"8080\", \"sleep\": 1 }"

requestid=$(curl -s -X POST "http://127.0.0.1:5000/RUN/BesTestExecution" -H "Content-Type: application/json" -d "$JSON_DATA")
```

Get the workflow status

```bash
curl -s -X GET "http://127.0.0.1:5000/STATUS/$requestid"
```

## Contribute

- Issue Tracker: https://gitlab.esrf.fr/svensson/bes/issues
- Source Code: https://gitlab.esrf.fr/svensson/bes

## Support

If you are having issues, please let us know via the issue tracker.

## License

The source code of *bes* is licensed under the MIT license.
See the `LICENSE <https://gitlab.esrf.fr/svensson/bes/blob/master/LICENSE>`_
and `copyright <https://gitlab.esrf.fr/svensson/bes/blob/master/copyright>`_
files for details.
