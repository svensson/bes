# Contributing guide

Development requirements are listed in `setup.cfg` and can be installed with

```bash
pip install -e .[dev]
```

## Edit workflows

```bash
./edit_bes_workflows.sh
```

## Naming

Follow the [PEP8](https://peps.python.org/pep-0008) style guide except for workflow
task parameter names which are `mixedCase` (`CamelCase` starting with a lowercase character).

## Linting

The configuration for [black](https://black.readthedocs.io/en/stable/) and [flake8](https://flake8.pycqa.org/en/latest/index.html) is defined in `setup.cfg`. To run them on the project

```bash
black .
flake8
```

Comment lines with `# noqa: E123` to ignore certain linting errors.

## Testing

Tests make use of [pytest](https://docs.pytest.org/en/stable/index.html) and can be run as follows

```bash
pytest .
```

Testing an installed project is done like this

```bash
pytest --pyargs <project_name>
```