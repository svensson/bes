# CHANGELOG.md

## Unreleased

## 1.1.0

New features:

- Add `ewoks` time profiling.

Changes:

- Replace INI configuration files with YAML files.
- Make clear distinction between aperture size (integer) and name (string).
- Add `snapshotDelay` to configuration.
- Enable `pypushflow` pre-import to improve speed.
- Add low-resolution diffraction image to gallery.
- Replace environment variables `PYPUSHFLOW_MONGOURL`, `PYPUSHFLOW_HOST`
  `PYPUSHFLOW_PORT` and `PYPUSHFLOW_OBJECTID` with explicit parameters
  provided by `ewoksppf` 0.4.0.

Bug fixes:

- Fix summing of CBF files.
- Fix `fixedTargetApertureSize` for ID30A3.
- Fix bug that pervented stopping workflows from `besgui`.
