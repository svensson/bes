import re
from pprint import pprint
from datetime import datetime

import requests
from pymongo import MongoClient


def find_workflows(query: dict) -> list[dict]:
    with MongoClient("mongodb://bes:bes@besdb1.esrf.fr:27017/bes") as client:
        db = client["bes"]
        collection = db["bes"]
        return list(collection.find(query))


def relaunch_workflow(workflow: dict, dryrun=True):
    request_id = workflow["Request ID"]
    name = workflow["name"]
    host = workflow["host"]
    beamline = workflow["initiator"]
    parameters = workflow["actors"][0]["inData"]
    parameters.pop("besParameters", None)  # added by BES

    if name == "CreateThumbnails":
        fix_pyarch_directory(parameters)

    if dryrun:
        print()
        print(f"{name=}, {beamline=}")
        pprint(parameters)
    else:
        print(
            f"\nRelaunch http://besgui.esrf.fr/actorPage/{request_id}/mxbes3-2204:38280"
        )
        trigger_workflow(name, parameters, beamline, host=host)


def fix_pyarch_directory(parameters):
    expected_year = extract_year_from_image_path(parameters["image_path"])

    for key in ("jpeg_path", "jpeg_thumbnail_path"):
        actual_year = extract_year_from_archive_path(parameters[key])
        if expected_year != actual_year:
            parameters[key] = parameters[key].replace(
                f"/{actual_year}/", f"/{expected_year}/"
            )


def extract_year_from_image_path(path):
    match = re.search(r"/(\d{4})\d{4}/", path)
    if match:
        return int(match.group(1))


def extract_year_from_archive_path(path):
    match = re.search(r"/(\d{4})/", path)
    if match:
        return int(match.group(1))


def trigger_workflow(name, parameters, beamline, host="scisoft10"):
    beamline_to_ports = {
        "bm07": 15180,
        "id23eh1": 31180,
        "id23eh2": 31280,
        "id30a1": 38180,
        "id30a2": 38280,
        "id30a3": 38380,
        "id30b": 38480,
    }
    port = beamline_to_ports[beamline]

    url = f"http://{host}:{port}/RUN/{name}"
    response = requests.post(url, json=parameters)
    response.raise_for_status()

    request_id = response.text

    print(
        f"Started {name} on http://{host}:{port}: http://besgui.esrf.fr/actorPage/{request_id}/mxbes3-2204:38280"
    )


def trigger_manually(host="scisoft10"):
    name = "Characterisation"
    beamline = "id30a2"
    parameters = {
        "beamline": "id30a2",
        "ednaStartScript": "/data/visitor/mx2112/id30a2/20241219/PROCESSED_DATA/WDN/WDN-2024dec19-05//characterisation_3/edna_start_20241219-172916.sh",
        "externalRef": "mx2112",
        "initiator": "id30a2",
        "inputFile": "/data/visitor/mx2112/id30a2/20241219/PROCESSED_DATA/WDN/WDN-2024dec19-05//characterisation_3/EDNAInput_3395276.xml",
        "logFile": "/data/visitor/mx2112/id30a2/20241219/PROCESSED_DATA/WDN/WDN-2024dec19-05//characterisation_3/EDNA_3395276.log",
        "outputFile": "/data/visitor/mx2112/id30a2/20241219/PROCESSED_DATA/WDN/WDN-2024dec19-05//characterisation_3/EDNAOutput_3395276.xml",
        "pluginName": "EDPluginControlInterfaceToMXCuBEv1_4",
        "proposal": "mx2112",
        "reuseCase": "true",
        "workingDirectory": "/data/visitor/mx2112/id30a2/20241219/PROCESSED_DATA/WDN/WDN-2024dec19-05//characterisation_3",
    }
    trigger_workflow(name, parameters, beamline, host=host)


if __name__ == "__main__":
    runtype = "batch"

    if runtype == "manual":
        trigger_manually()
    elif runtype == "batch":
        query = {
            "status": "error",
            # "host": "mxbes3-2304",  # these are all the non-data-collection workflows
            "initiator": "id30a1",
            "name": "CreateThumbnails",
            "startTime": {
                "$gt": datetime(2025, 1, 22, 15, 0, 0),
                "$lt": datetime(2025, 1, 22, 16, 0, 0),
            },
        }
        workflows = find_workflows(query)
        for workflow in workflows:
            relaunch_workflow(workflow, dryrun=True)
    elif runtype == "single":
        query = {"Request ID": "676449dca4e6ae60ef7d86af"}
        workflow = find_workflows(query)[0]
        relaunch_workflow(workflow, dryrun=True)
    else:
        print("unknown runtype")
