"""Script for re-processing MX data collections"""

import os
import json
import sys
import pathlib

_PIPELINE_DIR_TO_LAUNCHER = {
    "dozor": "/cvmfs/sb.esrf.fr/bin/dozorLauncher",
    "EDNA_proc": "/cvmfs/sb.esrf.fr/bin/EDNA_procLauncher",
    "autoPROC": "/cvmfs/sb.esrf.fr/bin/autoPROCLauncher",
    "grenades_fastproc": "/cvmfs/sb.esrf.fr/bin/GrenadesFastProcLauncher",
    "grenades_parallelproc": "/cvmfs/sb.esrf.fr/bin/GrenadesParallelProcLauncher",
    "XIA2_DIALS": "/cvmfs/sb.esrf.fr/bin/XIA2_DIALS_Launcher",
}


def check_autoprocessing(metadata_path):
    with open(metadata_path) as f:
        metadata = json.loads(f.read())

    parent_path = metadata_path.parent
    if str(parent_path).endswith("datacollection"):
        list_pipeline_dirs = [
            "dozor",
            "EDNA_proc",
            "grenades_fastproc",
            "grenades_parallelproc",
            "XIA2_DIALS",
        ]
    else:
        list_pipeline_dirs = [
            "dozor",
        ]

    processed_data_path = pathlib.Path(
        str(parent_path).replace("RAW_DATA", "PROCESSED_DATA")
    )
    if not processed_data_path.exists():
        raise RuntimeError(
            f"fatal error - PROCESSED_DATA directory does not exist: {processed_data_path}"
        )

    autoprocessing_path = processed_data_path / "autoprocessing"
    if not autoprocessing_path.exists():
        print(f"WARNING! autoprocessing path does not exist: {autoprocessing_path}")
        print("Creating the directory")
        autoprocessing_path.mkdir(mode=0o755)
    print(autoprocessing_path)

    # Check pipeline directories
    for pipeline_dir in list_pipeline_dirs:
        print()
        print(pipeline_dir)

        pipeline_path = autoprocessing_path / pipeline_dir
        if not pipeline_path.exists():
            continue

        nobackup_path = pipeline_path / "nobackup"
        finished_path = nobackup_path / "FINISHED"
        if finished_path.exists():
            print(f"{pipeline_dir} finished")
            continue

        started_path = nobackup_path / "STARTED"
        if started_path.exists():
            print(f"{pipeline_dir} started")
            continue

        print(f"{pipeline_dir} not started nor finished")

        data_collection_id = metadata["MX_dataCollectionId"]
        launcher_prog = _PIPELINE_DIR_TO_LAUNCHER[pipeline_dir]
        command_line = launcher_prog
        command_line += f" -path {autoprocessing_path}"
        command_line += f" -datacollectionID {data_collection_id}"
        command_line += " -mode after"
        print(command_line)

        yes_no = input(f"Should {pipeline_dir} be started? (yes/no) : ")
        if yes_no == "yes":
            os.system(command_line)


if __name__ == "__main__":
    # Get top level data collection dir from command line
    if len(sys.argv) == 1:
        print("Usage: reproces <to level data collection dir>")
        sys.exit(1)
    top_dir = pathlib.Path(sys.argv[1])

    # Find all metadata.json files in top_dir
    list_metadata_path = list(top_dir.rglob("**/metadata.json"))
    print(f"Total number of data collections: {len(list_metadata_path)}")

    # Go through all data collections, distinguish between
    # datacollection, characterisation, etc.
    for metadata_path in list_metadata_path:
        check_autoprocessing(metadata_path)
