import argparse
import json
import logging
import os
import re
import requests
import sys
from collections import defaultdict
from typing import List, Optional, Tuple
from glob import glob


logging.basicConfig(
    format="%(levelname)s:%(message)s",
    level=logging.INFO,
)
logger = logging.getLogger("RELAUNCH BES WORKFLOWS")

_BEAMLINE_NAME_TO_DIR = {
    "id30a-1": "id30a1",
    "id30a-3": "id30a3",
    "id23-1": "id23eh1",
    "id23-2": "id23eh2",
}

_BEAMLINE_DIR_TO_NAME = {v: k for k, v in _BEAMLINE_NAME_TO_DIR.items()}

MX_METADATA_FILENAME = "metadata.json"

_BEAMLINE_TO_HOSTS = {
    "bm07": "bm07server2",
    "id23eh1": "id231control2",
    "id23eh2": "id232control",
    "id30a1": "lid30a1control2",
    "id30a2": "scisoft10",
    "id30a3": "lid30a3control",
    "id30b": "id30bcontrol2",
}

_BEAMLINE_TO_PORTS = {
    "bm07": 15180,
    "id23eh1": 31180,
    "id23eh2": 31280,
    "id30a1": 38180,
    "id30a2": 38280,
    "id30a3": 38380,
    "id30b": 38480,
}


def split(path: str) -> str:
    while path and not os.path.basename(path):
        path = os.path.dirname(path)
    return path.split(os.sep)


def markdir(path: str) -> str:
    if path and os.path.basename(path):
        path = os.path.join(path, "")
    return path


def basename(path: str) -> str:
    while path and not os.path.basename(path):
        path = os.path.dirname(path)
    return os.path.basename(path)


def get_raw_data_dir(session_dir: str) -> str:
    """Get the raw data directory from proposal, beamline and session name.
    This is the directory when Bliss saves the raw data.
    """
    return markdir(os.path.join(session_dir, "RAW_DATA"))


def parse_session_dir(
    session_dir: str,
) -> Tuple[Optional[str], Optional[str], Optional[str]]:
    """Get proposal, beamline and session name from the session directory."""
    proposal, beamline_dir, session = split(session_dir)[-3:]
    if not session.isdigit():
        return None, None, None
    beamline = _BEAMLINE_DIR_TO_NAME.get(beamline_dir, beamline_dir)
    return proposal, beamline, session


def get_dataset_filters(raw_root_dir: str) -> List[str]:
    """Get the dataset directory search filters from the raw data directory."""
    filters = []
    for root, dirs, files in os.walk(raw_root_dir):
        if MX_METADATA_FILENAME in files:
            filters.append(markdir(root))
    return filters


def get_session_dir(
    proposal: str,
    beamline: str,
    session: str,
    root_dir: Optional[str] = None,
) -> str:
    """Get the session directory from the proposal, beamlines and session name."""
    if root_dir is None:
        root_dir = os.path.join(os.sep, "data", "visitor")
    session_dir = markdir(os.path.join(root_dir, proposal, beamline, session))
    return session_dir


def _get_value_from_file(json_file: str, key: str) -> Optional[str]:
    if not os.path.exists(json_file):
        logger.error(f"File not found in the path: {json_file}")
        return
    try:
        with open(json_file, "r") as file:
            metadata = json.load(file)
            scan_type = metadata.get(key)
            return scan_type
    except json.JSONDecodeError as e:
        raise ValueError(f"Failed to parse JSON in {json_file}: {e}")


def get_dataset_type(dataset_filter: str) -> Optional[str]:
    metadata_file = os.path.join(dataset_filter, MX_METADATA_FILENAME)
    return _get_value_from_file(metadata_file, "scanType")


def get_number_images(dataset_filter: str) -> Optional[int]:
    metadata_file = os.path.join(dataset_filter, MX_METADATA_FILENAME)
    number_images = _get_value_from_file(metadata_file, "MX_numberOfImages")
    if number_images is None:
        return None
    try:
        return int(number_images)
    except ValueError:
        logger.error(
            f"Cannot convert '{number_images}' to a number. dataset_filter={dataset_filter}"
        )
        return None


def get_prefix(dataset_filter: str) -> Optional[str]:
    metadata_file = os.path.join(dataset_filter, MX_METADATA_FILENAME)
    template = _get_value_from_file(metadata_file, "MX_template")
    match = re.match(r"^(.*)_%04d\.\w+$", template)
    if match:
        return match.group(1)


def get_data_collection_id(processed_dataset: str) -> Optional[str]:
    dozor_path = os.path.join(processed_dataset, "autoprocessing", "dozor", "nobackup")
    input_file = os.path.join(dozor_path, "dozor_input.json")
    return _get_value_from_file(input_file, "dataCollectionId")


def get_processed_dataset(dataset_filter: str) -> str:
    return dataset_filter.replace("RAW_DATA", "PROCESSED_DATA")


def get_corresponding_pyarch_path(dataset_filter: str) -> str:
    parts = dataset_filter.strip("/").split("/")
    year = parts[4][:4]
    proposal, beamline = parts[2], parts[3]
    pyarch_path = os.path.join(
        "/", "data", "pyarch", year, beamline, proposal, *parts[4:]
    )
    return pyarch_path


def get_common_workflow_parameters(proposal: str, beamline_dir: str) -> dict:
    parameters = {}
    parameters["externalRef"] = proposal
    parameters["initiator"] = beamline_dir
    parameters["reuseCase"] = "true"
    parameters["doIcatUpload"] = True
    parameters["doIspybUpload"] = True

    parameters.pop("besParameters", None)  # added by BES
    return parameters


def check_dozor(processed_dataset: str) -> bool:
    dozor_path = os.path.join(processed_dataset, "autoprocessing", "dozor", "gallery")
    if not os.path.exists(dozor_path):
        return False
    png_exists = any(fname.endswith(".png") for fname in os.listdir(dozor_path))
    csv_exists = any(fname.endswith(".csv") for fname in os.listdir(dozor_path))
    return png_exists and csv_exists


def generate_dozor(
    dataset_filter: str, processed_dataset: str, proposal: str, beamline: str
) -> dict:
    logger.debug(f"Generate dozor workflow for dataset={dataset_filter}")
    workflow = {}
    workflow["name"] = "dozor_SLURM"
    beamline_dir = beamline
    if beamline in _BEAMLINE_NAME_TO_DIR:
        beamline_dir = _BEAMLINE_NAME_TO_DIR[beamline]
    workflow["initiator"] = beamline_dir
    workflow["parameters"] = get_dozor_parameters(
        dataset_filter, processed_dataset, proposal, beamline_dir
    )
    return workflow


def get_dozor_parameters(
    dataset_filter: str, processed_dataset: str, proposal: str, beamline_dir: str
) -> dict:
    parameters = get_common_workflow_parameters(proposal, beamline_dir)
    parameters["beamline"] = beamline_dir
    parameters["core"] = 20
    parameters["dataCollectionId"] = get_data_collection_id(processed_dataset)
    parameters["doWaitForImages"] = "true"
    parameters["host"] = _BEAMLINE_TO_HOSTS[beamline_dir]
    parameters["launchPath"] = os.path.join(
        processed_dataset, "autoprocessing", "dozor", "nobackup", "dozor_launcher.sh"
    )
    parameters["mem"] = 8000
    parameters["mode"] = "before"
    parameters["nodes"] = 1
    parameters["numberOfImages"] = get_number_images(dataset_filter)
    parameters["prefix"] = get_prefix(dataset_filter)
    parameters["procType"] = "dozor"
    parameters["proposal"] = proposal
    parameters["queue"] = "mx"
    parameters["time"] = "2:00:00"
    return parameters


def extract_suffix(base_name: str) -> str:
    match = re.search(r"(\d+)_(\d+)$", base_name)
    if not match:
        logger.error(f"Unable to extract the suffix from base_name={base_name}")
        return ""
    first_number = match.group(1)
    second_number = int(match.group(2))
    second_number_padded = f"{second_number:04d}"
    return f"{first_number}_{second_number_padded}"


def check_diffraction_thumbnail(processed_dataset: str, base_name: str) -> bool:
    diffraction_thumbnail_suffix = extract_suffix(base_name)
    diffraction_thumbnail_path = os.path.join(
        os.path.dirname(os.path.normpath(processed_dataset)),
        f"diffraction_thumbnail_{diffraction_thumbnail_suffix}",
    )
    if not os.path.exists(diffraction_thumbnail_path):
        return False
    gallery_path = os.path.join(diffraction_thumbnail_path, "gallery")
    if not os.path.exists(gallery_path):
        return False
    jpeg_exists = any(fname.endswith(".jpeg") for fname in os.listdir(gallery_path))
    return jpeg_exists


def generate_create_thumbnails(
    dataset_filter: str, base_name: str, proposal: str, beamline: str
) -> dict:
    logger.debug(f"Generate createThumbnails workflow for dataset={dataset_filter}")
    workflow = {}
    workflow["name"] = "CreateThumbnails"
    beamline_dir = beamline
    if beamline in _BEAMLINE_NAME_TO_DIR:
        beamline_dir = _BEAMLINE_NAME_TO_DIR[beamline]
    workflow["initiator"] = beamline_dir
    workflow["parameters"] = get_create_thumbnails_parameters(
        dataset_filter, base_name, proposal, beamline_dir
    )
    return workflow


def get_master_file_prefixes(directory_path):
    master_files = glob(os.path.join(directory_path, "*_master.h5"))
    prefixes = [os.path.basename(f).rsplit("_master.h5", 1)[0] for f in master_files]
    return prefixes


def get_create_thumbnails_parameters(
    dataset_filter: str, base_name: str, proposal: str, beamline_dir: str
) -> dict:
    directory_path = os.path.dirname(dataset_filter)
    match = re.search(r"(\d+)_(\d+)$", base_name)
    if not match:
        logger.error(f"Unable to update the base_name file={base_name}")
    first_number = match.group(1)
    second_number = int(match.group(2))
    second_number_padded = f"{second_number:04d}"
    modified_base_name = re.sub(
        r"(\d+)_(\d+)$", f"{first_number}_{second_number_padded}", base_name
    )
    image_filenemane = f"{modified_base_name}.h5"
    jpeg_name = f"{modified_base_name}.jpeg"
    thumb_name = f"{modified_base_name}.thumb.jpeg"
    pyarch_path = get_corresponding_pyarch_path(directory_path)
    parameters = get_common_workflow_parameters(proposal, beamline_dir)
    parameters["image_path"] = os.path.join(directory_path, image_filenemane)
    parameters["jpeg_path"] = os.path.join(pyarch_path, jpeg_name)
    parameters["jpeg_thumbnail_path"] = os.path.join(pyarch_path, thumb_name)
    return parameters


def generate_workflows(
    dataset_filters: List[str], proposal: str, beamline: str
) -> dict[str, List[dict]]:
    workflows_by_name = defaultdict(list)
    for dataset_filter in dataset_filters:
        logger.debug(f"dataset {dataset_filter}")
        dataset_type = get_dataset_type(dataset_filter)
        processed_dataset = get_processed_dataset(dataset_filter)
        if dataset_type is None:
            logger.warning(f"Unrecognized type for dataset={dataset_filter}")
        logger.debug(f"dataset is {dataset_type}")
        if (
            dataset_type == "datacollection"
            or dataset_type == "characterisation"
            or dataset_type == "helical"
        ):
            has_dozor = check_dozor(processed_dataset)
            if not has_dozor:
                workflow = generate_dozor(
                    dataset_filter, processed_dataset, proposal, beamline
                )
                workflows_by_name["dozor"].append(workflow)
            prefixes = get_master_file_prefixes(dataset_filter)
            for prefix in prefixes:
                has_diffraction_thumbnail = check_diffraction_thumbnail(
                    processed_dataset, prefix
                )
                if not has_diffraction_thumbnail:
                    workflow = generate_create_thumbnails(
                        dataset_filter, prefix, proposal, beamline
                    )
                    workflows_by_name["CreateThumbnails"].append(workflow)

    return workflows_by_name


def trigger_workflow(workflow: dict):
    port = _BEAMLINE_TO_PORTS[workflow["initiator"]]
    name = workflow["name"]
    parameters = workflow["parameters"]

    # scisoft10: host for backup BES servers
    url = f"http://scisoft10:{port}/RUN/{name}"
    response = requests.post(url, json=parameters)
    response.raise_for_status()

    request_id = response.text

    logger.info(
        f"Started {name} on http://scisoft10:{port}: http://besgui.esrf.fr/actorPage/{request_id}/mxbes3-2204:38280"
    )


def _ask_execute_confirmation(action: str) -> bool:
    result = input(f"{action}? (y/[n])")
    return result.lower() in ("y", "yes")


def _is_mx_beamline(beamline_dir: str) -> bool:
    return _BEAMLINE_NAME_TO_DIR.get(beamline_dir, beamline_dir) in _BEAMLINE_TO_HOSTS


def relaunch_bes_workflows(
    beamline: Optional[str] = None,
    proposal: Optional[str] = None,
    session: Optional[str] = None,
    root_dir: Optional[str] = None,
    relaunch: Optional[bool] = False,
) -> None:
    logger.info("Browse sessions")
    if beamline is None:
        beamline = "*"
    if proposal is None:
        proposal = "*"
    if session is None:
        session = "*"
    session_filter = get_session_dir(proposal, beamline, session, root_dir=root_dir)
    session_dirs = glob(markdir(session_filter))
    logger.info("Parse %d session directories ...", len(session_dirs))
    for session_dir in session_dirs:
        try:
            proposal_from_dir, beamline_from_dir, session_from_dir = parse_session_dir(
                session_dir
            )
            if not proposal_from_dir:
                continue
            if not _is_mx_beamline(beamline_from_dir):
                continue
            session = basename(session_dir)
            raw_root_dir = get_raw_data_dir(session_dir)
            logger.debug(f"Retrieve datasets for raw_root_dir={raw_root_dir}")
            dataset_filters = get_dataset_filters(raw_root_dir)
            workflows_by_name = generate_workflows(
                dataset_filters, proposal_from_dir, beamline_from_dir
            )
            for workflow_name, workflows in workflows_by_name.items():
                logger.info(
                    f"Workflow '{workflow_name}' has {len(workflows)} workflows to be relaunched."
                )
                if relaunch:
                    confirmation = _ask_execute_confirmation(
                        f"Relaunch {workflow_name}?"
                    )
                    if confirmation:
                        for workflow in workflows:
                            trigger_workflow(workflow)
        except Exception as e:
            raise RuntimeError(
                f"Failed to relaunch bes workflows for beamline={beamline_from_dir}, proposal={proposal_from_dir}, session={session_from_dir} "
            ) from e


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(
        description="Relaunch BES workflows if missing data on disk"
    )
    parser.add_argument(
        "--beamline",
        type=str.lower,
        required=False,
        help="Beamline name, BES conventions (e.g. id23eh1 or id30a1)",
    )
    parser.add_argument(
        "--proposal",
        type=str.lower,
        required=False,
        help="Proposal name (e.g. mx415)",
    )
    parser.add_argument(
        "--session", required=False, help="Session name (e.g. 20241119)"
    )
    parser.add_argument(
        "--relaunch",
        action="store_true",
        required=False,
        help="Relaunch the workflows when needed (prompts for confirmation)",
    )
    args = parser.parse_args(argv[1:])
    logger.info("Start relaunch bes workflows from disk")
    relaunch_bes_workflows(
        beamline=args.beamline,
        proposal=args.proposal,
        session=args.session,
        relaunch=args.relaunch,
    )


if __name__ == "__main__":
    main()
