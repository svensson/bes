import time
import os
from contextlib import contextmanager


@contextmanager
def timeit(name, description):
    annotated_print(name, description)
    start_time = time.time()
    try:
        yield
    finally:
        end_time = time.time()
        annotated_print(name, f"took {end_time - start_time:.4f} seconds")


def annotated_print(*args):
    message = f"\n[PID {os.getpid()}]"
    extra = " ".join(args)
    if extra:
        message = f"{message} {extra}"
    print(message)


def run_bes_server(njobs):
    import concurrent.futures

    with concurrent.futures.ThreadPoolExecutor() as executor:
        for _ in range(njobs):
            print("\n" * 5)
            future = executor.submit(run_bes_job)
            try:
                future.result()
            except Exception:
                annotated_print("BES server stopped")
                break


def run_bes_job():
    with timeit("BES job", "launches a BES job using subprocess.Popen"):
        import sys
        import subprocess

        process = subprocess.Popen(
            [sys.executable] + sys.argv + ["--job"], stdout=None, stderr=None
        )
        stdout, stderr = process.communicate()
        if process.returncode:
            raise RuntimeError("BES job failed")


def run_ewoks_workflow():
    with timeit("EWOKS workflow", "runs EWOKS tasks using ewoksppf"):
        import shutil
        from ewoksppf import execute_graph
        from bes.workflow_lib import path

        root_dir = "/tmp/bes"
        shutil.rmtree(root_dir, ignore_errors=True)

        dataset = f'{time.strftime("%Y%m%d", time.localtime(time.time()))}_1_1'
        raw_directory = f"{root_dir}/RAW_DATA/PROT/PROT-{dataset}"

        raw_directory, _ = path.createUniqueICATDirectory(
            raw_directory=raw_directory,
            run_number=1,
            expTypePrefix="MXPressF",
        )

        workflow_working_dir = path.createWorkflowWorkingDirectory(raw_directory)

        nodes = [
            {
                "id": "node1",
                "task_type": "ppfmethod",
                "task_identifier": "__main__.ewoks_task_echo",
            },
            {
                "id": "node2",
                "task_type": "ppfmethod",
                "task_identifier": "__main__.ewoks_task_dozorm2",
            },
            {
                "id": "node3",
                "task_type": "ppfmethod",
                "task_identifier": "__main__.ewoks_task_dozorm2",
            },
            {
                "id": "node4",
                "task_type": "ppfmethod",
                "task_identifier": "__main__.ewoks_task_dozorm2",
            },
            {
                "id": "node5",
                "task_type": "ppfmethod",
                "task_identifier": "__main__.ewoks_task_dozorm2",
            },
            {
                "id": "node6",
                "task_type": "ppfmethod",
                "task_identifier": "__main__.ewoks_task_dozorm2",
            },
        ]
        links = []
        graph = {"graph": {"id": "MXPressF"}, "nodes": nodes, "links": links}
        startargs = {"workflow_working_dir": workflow_working_dir}
        inputs = list()
        _ = execute_graph(
            graph,
            startargs=startargs,
            inputs=inputs,
            raise_on_error=True,
            pool_type="thread",
        )


def ewoks_task_echo(workflow_working_dir=None, **_):

    with timeit("EWOKS task 'Echo'", "launches an EDNA2 task using billiard.Process"):
        from edna2.tasks.AbstractTask import AbstractTask

        class Echo(AbstractTask):

            def run(self, inData):
                commandLine = 'echo "[PID $$] Hello from Edna2 Test Task!"'
                logPath = self.getWorkingDirectory() / "test.log"
                self.runCommandLine(commandLine, log_path=logPath)
                outData = dict()
                outData["logPath"] = str(logPath)
                outData["workingDirectory"] = str(self.getWorkingDirectory())
                return outData

        inData = {"workingDirectory": workflow_working_dir}

        return _execute_edna2_task("Echo", Echo, inData=inData)


def ewoks_task_dozorm2(workflow_working_dir=None, **_):

    with timeit(
        "EWOKS task 'DozorM2'", "launches an EDNA2 task using billiard.Process"
    ):

        from edna2.tasks.DozorM2 import DozorM2

        # /data/visitor/ix61/id30b/20241113/PROCESSED_DATA/ODY/ODY-20241101_4_4/run_01_MXPressF/Workflow_20241113-105937/DozorM2_two_meshes/inDataDozorM2.json
        inData = {
            "detectorType": "eiger9m",
            "beamline": "id30b",
            "detector_distance": 245.95,
            "wavelength": 0.873128,
            "orgx": 1563.72,
            "orgy": 1702.6666666666667,
            "number_row": 26,
            "number_images": 338,
            "isZigZag": True,
            "step_h": 14,
            "step_v": 13,
            "beam_shape": "G",
            "beam_h": 20,
            "beam_v": 30,
            "number_apertures": 3,
            "aperture_size": "10 20 30",
            "reject_level": 40,
            "list_dozor_all": [
                "/data/visitor/ix61/id30b/20241113/PROCESSED_DATA/ODY/ODY-20241101_4_4/run_01_MXPressF/Workflow_20241113-105937/ImageQualityIndicators_mbrw96qm/dozor_all",
                "/data/visitor/ix61/id30b/20241113/PROCESSED_DATA/ODY/ODY-20241101_4_4/run_01_MXPressF/Workflow_20241113-105937/ImageQualityIndicators_nlaon1du/dozor_all",
            ],
            "phi_values": [89.99998099999993, 179.99998099999993],
            "number_scans": 2,
            "first_scan_number": 1,
            "workingDirectory": workflow_working_dir,
            "isHorizontalScan": True,
            "grid_x0": 5.6,
            "grid_y0": 13.776923076923078,
            "loop_thickness": 500,
            "sampx": 0.1324,
            "sampy": 0.6285,
            "phiy": -2.4071,
            "phiz": 0.101,
        }

        return _execute_edna2_task("DozorM2", DozorM2, inData=inData)


def _execute_edna2_task(name, task_class, **kwargs):
    dozorM2 = task_class(**kwargs)
    dozorM2.execute()
    if not dozorM2.isSuccess():
        raise RuntimeError(f"{name} failed")
    return dozorM2.outData


def main():
    import argparse

    parser = argparse.ArgumentParser(description="BES Server Simulation")
    parser.add_argument(
        "--job",
        action="store_true",
        help="BES job instead of the main BES server",
    )
    parser.add_argument(
        "--njobs",
        default=1,
        type=int,
        help="Number of BES jobs",
    )
    args = parser.parse_args()

    if args.job:
        run_ewoks_workflow()
    else:
        run_bes_server(args.njobs)


if __name__ == "__main__":
    main()
