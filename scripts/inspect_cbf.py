import os
from glob import glob
import fabio
from bes.workflow_lib import mini_cbf_utils


if __name__ == "__main__":
    characterisation = (
        "/data/id30a1/inhouse/opid30a1/20220408/RAW_DATA/Sample-8-1-01/MXPressA_01"
    )

    characterisation = "/data/visitor/fx87/id30a1/20241113/RAW_DATA/DGK/DGK-IM96_A3Opti_4/run_01_MXPressA/run_01_04_characterisation"

    print(characterisation)
    files = glob(f"{characterisation}/ref-*.cbf")
    for filename in sorted(files):
        with fabio.open(filename) as fh:
            mini_cbf_header = fh.header["_array_data.header_contents"]
            header = mini_cbf_utils.deserialize_mini_cbf_header(
                mini_cbf_header, as_dict=True
            )
            print(os.path.basename(filename))
            for key in ["Start_angle", "Angle_increment"]:
                value, unit = header[key]
                print(f" {key} = {value} {unit}")
