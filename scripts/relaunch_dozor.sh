#!/bin/bash

# Navigate to the base directory
base_dir="/data/visitor/blc15922/id30a1/20250121/PROCESSED_DATA"

# Find directories without a FINISHED file and containing slurm.sh
find "$base_dir" -type d -path "*/autoprocessing/dozor/nobackup" | while read -r dir; do
    if [[ ! -f "$dir/FINISHED" && -f "$dir/slurm.sh" ]]; then
        echo "Submitting job in $dir"
        sbatch "$dir/slurm.sh"
    fi
done
