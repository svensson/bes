import sys
import logging
from bes.app import create_app

app = create_app()


def main_cli(argv=None) -> int:
    if argv is None:
        argv = sys.argv
    try:
        # Not flask and not gunicorn so we are in charge
        # Do what werkzeug would do with `flask ... --debug`
        app.logger.setLevel(logging.DEBUG)
        app.run()
    except Exception as e:
        app.logger.error(e, exc_info=True)
        return 1
    return 0


if __name__ == "__main__":
    sys.exit(main_cli())
