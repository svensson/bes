import os
import sys
import json
import pickle
import logging
import pathlib
import datetime
import argparse
from typing import Optional
from logging import StreamHandler
from logging.handlers import RotatingFileHandler

from ewokscore import load_graph
from ewoksppf import execute_graph
from bson.objectid import ObjectId

from bes import flows


def _load_workflow(
    graph_path: pathlib.Path, cache_directory: Optional[pathlib.Path] = None
) -> None:
    logging.debug("Path graph file: %s", graph_path)

    if cache_directory is None:
        return load_graph(str(graph_path), root_dir=graph_path.parent)

    # Find the latest workflow file based on modification time
    latest_file = max(graph_path.parent.glob("*.json"), key=lambda p: p.stat().st_mtime)
    time_stamp_latest_json_file = latest_file.stat().st_mtime
    dt = datetime.datetime.fromtimestamp(time_stamp_latest_json_file)
    logging.debug(f"Time last modified json file: {dt:%Y-%m-%d %H:%M:%S}")

    # Find the the cached version of the requested workflow
    if not cache_directory.exists():
        cache_directory.mkdir(mode=0o755, parents=True)
    cached_graph_path = cache_directory / f"{graph_path.stem}.pickle"
    if cached_graph_path.exists():
        time_stamp_cached_graph = cached_graph_path.stat().st_mtime
        read_from_cache = time_stamp_cached_graph > time_stamp_latest_json_file
        dt = datetime.datetime.fromtimestamp(time_stamp_cached_graph)
        logging.debug(f"Time last modified cached file: {dt:%Y-%m-%d %H:%M:%S}")
    else:
        read_from_cache = False
        logging.debug("Workflow was never cached")

    # Load the workflow from cache when it is newer than any workflow file
    if read_from_cache:
        logging.debug(f"Reading graph from cache: {cached_graph_path}")
        with open(str(cached_graph_path), "rb") as f:
            return pickle.load(f)

    # Load the workflow from source and cache it
    graph = load_graph(str(graph_path), root_dir=graph_path.parent)

    if cache_directory:
        logging.debug(f"Writing graph to cache: {cached_graph_path}")
        with open(str(cached_graph_path), "wb") as f:
            pickle.dump(graph, f)

    return graph


def _setup_logging(
    job_id: str,
    log_name: str,
    log_directory: Optional[pathlib.Path] = None,
    log_level: Optional[int] = None,
    ppf_log_level: Optional[int] = None,
    ewoks_log_level: Optional[int] = None,
    log_stdout: bool = True,
) -> None:
    handlers = []
    log_file_format = f"%(asctime)s %(levelname)-5s [{job_id}] %(message)s"

    if log_stdout:
        # Logs are visible on the standard output
        handlers.append(StreamHandler(sys.stdout))

    if log_directory:
        # Logs are stored in a file
        if not log_directory.exists():
            log_directory.mkdir(mode=0o755, parents=True)
        log_file_path = log_directory / f"{log_name}.log"

        # Rollover size check happens when a message is emitted to the handler.
        # 7 beamlines with 10 workflows and 10MB logs results in 700MB.
        # With a backup count of 10 we will have 7GB of logs.
        file_handler = RotatingFileHandler(
            log_file_path, maxBytes=10 << 20, backupCount=10
        )

        formatter = logging.Formatter(log_file_format)
        file_handler.setFormatter(formatter)

        handlers.append(file_handler)

    logging.basicConfig(level=log_level, format=log_file_format, handlers=handlers)

    if ppf_log_level is not None:
        logging.getLogger("pypushflow").setLevel(ppf_log_level)

    if ewoks_log_level is not None:
        logging.getLogger("ewokscore.events").setLevel(ewoks_log_level)

    logging.getLogger("matplotlib").setLevel(logging.ERROR)


def _prevent_rotation():
    """Prevent all `RotatingFileHandler` instances from rotating."""
    for handler in logging.getLogger().handlers:
        if isinstance(handler, RotatingFileHandler):
            handler.maxBytes = 0


os.register_at_fork(after_in_child=_prevent_rotation)


def _parse_input_parameters(inData: Optional[dict]) -> dict:
    if inData is None:
        inData = dict()
    elif not isinstance(inData, dict):
        raise ValueError(f"{inData=} must be a dictionary")

    bes_parameters = inData.setdefault("besParameters", dict())
    if not bes_parameters.get("request_id"):
        bes_parameters["request_id"] = str(ObjectId())

    return inData


def _parse_database_parameters(db_options: Optional[dict]) -> dict:
    if db_options is None:
        db_options = dict()
    elif not isinstance(db_options, dict):
        raise ValueError(f"{db_options=} must be a dictionary")

    return db_options


def execute_workflow(
    workflow_name: str,
    inData: dict,
    db_options: dict,
    cache_directory: Optional[pathlib.Path] = None,
    log_directory: Optional[pathlib.Path] = None,
    log_level: Optional[int] = None,
    ppf_log_level: Optional[int] = None,
    ewoks_log_level: Optional[int] = None,
    profile_directory: Optional[str] = None,
) -> dict:
    job_id = inData["besParameters"]["request_id"]

    if workflow_name.endswith(".json"):
        graph_path = pathlib.Path(workflow_name)
        flows_dir = graph_path.parent
        workflow_name = graph_path.stem
    else:
        flows_dir = pathlib.Path(flows.__file__).parent
        graph_path = flows_dir / f"{workflow_name}.json"

    _setup_logging(
        job_id,
        workflow_name,
        log_directory=log_directory,
        log_level=log_level,
        ppf_log_level=ppf_log_level,
        ewoks_log_level=ewoks_log_level,
    )

    if not graph_path.exists():
        raise RuntimeError(f"Workflow {graph_path} does not exist")

    logging.info("Command: %s", sys.argv)
    logging.debug("*" * 80)
    graph = _load_workflow(graph_path, cache_directory=cache_directory)
    logging.debug("*" * 80)

    # pool_type="thread": faster but hangs on matplotlib and XML-RPC?
    # pool_type="billiard": causes the currect process to never exit.
    return execute_graph(
        graph,
        load_options={"root_dir": str(flows_dir)},
        startargs=inData,
        execinfo={"job_id": job_id},
        max_worker=None,
        scaling_workers=True,
        pool_type="process",
        task_options={"profile_directory": profile_directory},
        pre_import=True,
        db_options=db_options,
    )


def main_cli(argv=None) -> int:
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(
        "run_ppf_json",
        description="Launch Ewoks workflow with the ppf engine",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "task_name",
        type=str,
        help="Ewoks workflow identifier",
    )
    parser.add_argument(
        "--inData",
        type=json.loads,
        help="Workflow input parameters",
    )
    parser.add_argument(
        "--db-options",
        type=json.loads,
        default="{}",
        help="Pypushflow database parameters",
    )
    parser.add_argument(
        "--log-level",
        default="WARNING",
        type=str.upper,
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Set the logging level",
    )
    parser.add_argument(
        "--ppf-log-level",
        default="WARNING",
        type=str.upper,
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Set the logging level",
    )
    parser.add_argument(
        "--ewoks-log-level",
        default="INFO",
        type=str.upper,
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Set the logging level",
    )
    parser.add_argument(
        "--time-profile",
        action="store_true",
        help="Enable time profiling",
    )
    parser.add_argument(
        "--data-directory",
        default=None,
        type=str,
        help="Directory for logs, cache and time profiling",
    )

    args = parser.parse_args(args=argv[1:])

    try:
        inData = _parse_input_parameters(args.inData)
        db_options = _parse_database_parameters(args.db_options)

        if args.data_directory:
            data_directory = pathlib.Path(args.data_directory)
        else:
            data_directory = None

        log_level = getattr(logging, args.log_level.upper())
        ppf_log_level = getattr(logging, args.ppf_log_level.upper())
        ewoks_log_level = getattr(logging, args.ewoks_log_level.upper())

        if args.time_profile:
            if data_directory is None:
                raise ValueError("Missing --data-directory argument")
            profile_directory = data_directory
        else:
            profile_directory = None

        execute_workflow(
            args.task_name,
            inData,
            db_options,
            cache_directory=data_directory,
            log_directory=data_directory,
            log_level=log_level,
            ppf_log_level=ppf_log_level,
            ewoks_log_level=ewoks_log_level,
            profile_directory=profile_directory,
        )
    except Exception as e:
        logging.error(e, exc_info=True)
        return 1
    return 0


if __name__ == "__main__":
    sys.exit(main_cli())
