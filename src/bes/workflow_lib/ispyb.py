"""
Workflow library module for connecting to ISPyB
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013"
__updated__ = "2016-02-17"

import os
import sys
import json
import time
import pprint
import logging.config
import traceback

from edna2.utils import UtilsImage

from bes.workflow_lib import workflow_logging

# sys.path.insert(0, "/opt/pxsoft/EDNA/vMX/edna/libraries/suds-0.4")

from suds.client import Client
from suds.transport.https import HttpAuthenticated
from suds.sudsobject import asdict

logging.getLogger("suds").setLevel(logging.WARNING)


def getTransport():
    transport = None
    logger = workflow_logging.getLogger()
    if "ISPyB_user" not in os.environ:
        logger.error("No ISPyB user name defined as environment variable!")
    elif "ISPyB_pass" not in os.environ:
        logger.error("No ISPyB password defined as environment variable!")
    else:
        ispybUserName = os.environ["ISPyB_user"]
        ispybPassword = os.environ["ISPyB_pass"]
        transport = HttpAuthenticated(username=ispybUserName, password=ispybPassword)
    return transport


def getWdslRoot(beamline):
    # if beamline in ["id30a2"] or beamline.startswith("simulator"):
    if beamline in [] or beamline.startswith("simulator"):
        wdslRoot = "https://ispyb-valid.esrf.fr/ispyb/ispyb-ws/ispybWS"
    else:
        wdslRoot = "https://ispyb.esrf.fr/ispyb/ispyb-ws/ispybWS"
    return wdslRoot


def getToolsForShippingWebServiceWdsl(beamline):
    return os.path.join(getWdslRoot(beamline), "ToolsForShippingWebService?wsdl")


def getToolsForCollectionWebService(beamline):
    return os.path.join(getWdslRoot(beamline), "ToolsForCollectionWebService?wsdl")


def getToolsForShippingWebService(beamline):
    return os.path.join(getWdslRoot(beamline), "ToolsForShippingWebService?wsdl")


def getShippingWebServiceClient(beamline):
    logger = workflow_logging.getLogger()
    shippingWdsl = getToolsForShippingWebServiceWdsl(beamline)
    transport = getTransport()
    if transport is None:
        logger.error(
            "No transport defined, ISPyB web service client cannot be instantiated."
        )
        shippingWSClient = None
    else:
        shippingWSClient = Client(shippingWdsl, transport=transport, cache=None)
    return shippingWSClient


def getCollectionWebService(beamline):
    logger = workflow_logging.getLogger()
    collectionWdsl = getToolsForCollectionWebService(beamline)
    transport = getTransport()
    if transport is None:
        logger.error(
            "No transport defined, ISPyB web service client cannot be instantiated."
        )
        collectionWSClient = None
    else:
        collectionWSClient = Client(collectionWdsl, transport=transport, cache=None)
    return collectionWSClient


def getShippingWebService(beamline):
    logger = workflow_logging.getLogger()
    shippingWdsl = getToolsForShippingWebService(beamline)
    transport = getTransport()
    if transport is None:
        logger.error(
            "No transport defined, ISPyB web service client cannot be instantiated."
        )
        collectionWSClient = None
    else:
        collectionWSClient = Client(shippingWdsl, transport=transport, cache=None)
    return collectionWSClient


def findPersonByProposal(beamline, proposalCode, proposalNumber):
    person = None
    logger = workflow_logging.getLogger()
    if proposalCode.startswith("opid"):
        if beamline == "id30a2":
            # Test account - return a fake email address
            person = {"emailAddress": "svensson@esrf.fr"}
    else:
        client = getShippingWebServiceClient(beamline)
        if client is None:
            logger.error(
                "No web service client available, cannot contact findPersonByProposal web service."
            )
        elif proposalCode is None:
            logger.error(
                "No proposal code given, cannot contact findPersonByProposal web service."
            )
        elif proposalNumber is None:
            logger.error(
                "No proposal number given, cannot contact findPersonByProposal web service."
            )
        else:
            person = asdict(
                client.service.findPersonByProposal(proposalCode, proposalNumber)
            )
    return person


def findDataCollection(beamline, dataCollectionId, client=None):
    dataCollectionWS3VO = None
    noTrials = 5
    logger = workflow_logging.getLogger()
    if client is None:
        client = getCollectionWebService(beamline)
    if client is None:
        logger.error(
            "No web service client available, cannot contact findDataCollection web service."
        )
    elif dataCollectionId is None:
        logger.error(
            "No dataCollectionId given, cannot contact findDataCollection web service."
        )
    else:
        while noTrials > 0:
            noTrials -= 1
            try:
                dataCollectionWS3VO = client.service.findDataCollection(
                    dataCollectionId
                )
                if dataCollectionWS3VO is not None:
                    noTrials = 0
                else:
                    logger.error(
                        "Cannot find data collection for dataCollectionId = {0}, {1} trials left".format(
                            dataCollectionId, noTrials
                        )
                    )
                    time.sleep(1)
            except Exception as e:
                logger.error(
                    "ISPyB error for findDataCollection: {0}, {1} trials left".format(
                        e, noTrials
                    )
                )
                time.sleep(1)
    if dataCollectionWS3VO is None:
        logger.error(
            "No data collections found for dataCollectionId {0}".format(
                dataCollectionId
            )
        )
    return dataCollectionWS3VO


def findDataCollectionFromFileLocationAndFileName(beamline, imagePath, client=None):
    dataCollectionWS3VO = None
    noTrials = 10
    fileLocation = os.path.dirname(imagePath)
    fileName = os.path.basename(imagePath)
    if fileName.endswith(".h5"):
        prefix = UtilsImage.getPrefix(fileName)
        imageNumber = UtilsImage.getImageNumber(fileName)
        fileName = "{0}_{1:04d}.h5".format(prefix, imageNumber)
    logger = workflow_logging.getLogger()
    if client is None:
        client = getCollectionWebService(beamline)
    if client is None:
        logger.error(
            "No web service client available, cannot contact findDataCollectionFromFileLocationAndFileName web service."
        )
    elif fileLocation is None:
        logger.error(
            "No fileLocation given, cannot contact findDataCollectionFromFileLocationAndFileName web service."
        )
    elif fileName is None:
        logger.error(
            "No fileName given, cannot contact findDataCollectionFromFileLocationAndFileName web service."
        )
    else:
        while noTrials > 0:
            noTrials -= 1
            try:
                dataCollectionWS3VO = (
                    client.service.findDataCollectionFromFileLocationAndFileName(
                        fileLocation, fileName
                    )
                )
                if dataCollectionWS3VO is not None:
                    noTrials = 0
                else:
                    logger.warning(
                        "Cannot find data collection in ISPyB for {0}, {1} trials left".format(
                            os.path.join(fileLocation, fileName), noTrials
                        )
                    )
                    time.sleep(1)
            except Exception as e:
                logger.warning(
                    "ISPyB error for findDataCollectionFromFileLocationAndFileName: {0}, {1} trials left".format(
                        e, noTrials
                    )
                )
                time.sleep(1)
    if dataCollectionWS3VO is None:
        logger.error("No data collections found for path {0}".format(imagePath))
    return dataCollectionWS3VO


def storeOrUpdateDataCollection(beamline, dataCollectionWS3VO, client=None):
    dataCollectionId = None
    logger = workflow_logging.getLogger()
    if client is None:
        client = getCollectionWebService(beamline)
    if client is None:
        logger.error(
            "No web service client available, cannot contact storeOrUpdateDataCollection web service."
        )
    elif dataCollectionWS3VO is None:
        logger.error(
            "No dataCollection given, cannot contact storeOrUpdateDataCollection web service."
        )
    else:
        dataCollectionId = client.service.storeOrUpdateDataCollection(
            dataCollectionWS3VO
        )
    return dataCollectionId


def findDataCollectionGroup(beamline, dataCollectionId, client=None):
    dataCollectionGroupWS3VO = None
    logger = workflow_logging.getLogger()
    if client is None:
        client = getCollectionWebService(beamline)
    if client is None:
        logger.error(
            "No web service client available, cannot contact findDataCollectionGroup web service."
        )
    elif dataCollectionId is None:
        logger.error(
            "No dataCollectionId given, cannot contact findDataCollectionGroup web service."
        )
    else:
        dataCollectionGroupWS3VO = client.service.findDataCollectionGroup(
            dataCollectionId
        )
    return dataCollectionGroupWS3VO


def storeOrUpdateDataCollectionGroup(beamline, dataCollectionGroupWS3VO, client=None):
    logger = workflow_logging.getLogger()
    if client is None:
        client = getCollectionWebService(beamline)
    if client is None:
        logger.error(
            "No web service client available, cannot contact storeOrUpdateDataCollectionGroup web service."
        )
    elif dataCollectionGroupWS3VO is None:
        logger.error(
            "No dataCollectionGroup given, cannot contact storeOrUpdateDataCollectionGroup web service."
        )
    else:
        dataCollectionGrouoId = client.service.storeOrUpdateDataCollectionGroup(
            dataCollectionGroupWS3VO
        )
    return dataCollectionGrouoId


def updateDataCollectionComment(beamline, filePath, newComment):
    dataCollectionId = None
    logger = workflow_logging.getLogger()
    try:
        client = getCollectionWebService(beamline)
        if client is None:
            logger.error(
                "No web service client available, cannot contact storeOrUpdateDataCollection web service."
            )
        dataCollectionWS3VO = findDataCollectionFromFileLocationAndFileName(
            beamline, filePath, client=client
        )
        if (dataCollectionWS3VO is not None) and (newComment is not None):
            if hasattr(dataCollectionWS3VO, "comments") and (
                dataCollectionWS3VO.comments is not None
            ):
                newComments = dataCollectionWS3VO.comments + " " + newComment
                if len(newComments) < 1024:
                    dataCollectionWS3VO.comments = newComments
                else:
                    logger.warning(
                        "New ISPyB comment too long! '{0}'".format(newComments)
                    )
            elif len(newComment) < 1024:
                dataCollectionWS3VO.comments = newComment
            else:
                logger.warning("New ISPyB comment too long! '{0}'".format(newComment))
            dataCollectionId = storeOrUpdateDataCollection(
                beamline, dataCollectionWS3VO, client=client
            )
            logger.info("ISPyB data collection comment: {0}".format(newComment))
        else:
            dataCollectionId = dataCollectionWS3VO.dataCollectionId
    except Exception:
        logger.error(
            "Error when trying to upload comment '{0}' to ISPyB data collection group.".format(
                newComment
            )
        )
        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        listTrace = traceback.extract_tb(exc_traceback)
        logger.debug(exc_type)
        logger.debug(exc_value)
        logger.debug("Traceback (most recent call last): %s" % os.linesep)
        for listLine in listTrace:
            errorLine = '  File "%s", line %d, in %s%s' % (
                listLine[0],
                listLine[1],
                listLine[2],
                os.linesep,
            )
            logger.debug(errorLine)
    return dataCollectionId


def updateDataCollectionGroupComment(beamline, filePath, newComment):
    dataCollectionGroupId = None
    logger = workflow_logging.getLogger()
    try:
        client = getCollectionWebService(beamline)
        if client is None:
            logger.error(
                "No web service client available, cannot contact storeOrUpdateDataCollectionGroup web service."
            )
        dataCollectionWS3VO = findDataCollectionFromFileLocationAndFileName(
            beamline, filePath, client=client
        )
        if (dataCollectionWS3VO is not None) and (newComment is not None):
            dataCollectionGroupId = dataCollectionWS3VO.dataCollectionGroupId
            dataCollectionGroupWS3VO = findDataCollectionGroup(
                beamline, dataCollectionGroupId, client=client
            )
            if dataCollectionGroupWS3VO is not None:
                dataCollectionGroupBefore = asdict(dataCollectionGroupWS3VO)
                beforeComment = (
                    "DataCollectionGroup before changing comment: {0}".format(
                        pprint.pformat(dataCollectionGroupBefore)
                    )
                )
                logger.debug(beforeComment)
                if (dataCollectionGroupWS3VO.comments is not None) and len(
                    dataCollectionGroupWS3VO.comments + " " + newComment
                ) >= 1024:
                    logger.warning(
                        "ISPyB comment make data collection group comment too long! '{0}'".format(
                            dataCollectionGroupWS3VO.comments + " " + newComment
                        )
                    )
                elif len(newComment) >= 1024:
                    logger.warning(
                        "ISPyB collection group comment too long! '{0}'".format(
                            newComment
                        )
                    )
                else:
                    dataCollectionGroupWS3VO.comments = newComment
                    dataCollectionGroupId = storeOrUpdateDataCollectionGroup(
                        beamline, dataCollectionGroupWS3VO, client=client
                    )
                    logger.info(
                        "ISPyB data collection group comment: {0}".format(newComment)
                    )
        elif dataCollectionWS3VO is not None:
            dataCollectionGroupId = dataCollectionWS3VO.dataCollectionGroupId
    except Exception:
        logger.error(
            "Error when trying to upload comment '{0}' to ISPyB data collection group.".format(
                newComment
            )
        )
        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        listTrace = traceback.extract_tb(exc_traceback)
        logger.debug(exc_type)
        logger.debug(exc_value)
        logger.debug("Traceback (most recent call last): %s" % os.linesep)
        for listLine in listTrace:
            errorLine = '  File "%s", line %d, in %s%s' % (
                listLine[0],
                listLine[1],
                listLine[2],
                os.linesep,
            )
            logger.debug(errorLine)
    return dataCollectionGroupId


def storeOrUpdateWorkflow(
    beamline,
    workflowTitle="",
    workflowType="Undefined",
    workflowPyarchLogFile=None,
    workflowLogFile=None,
    pyarchHtmlDir="",
    workflowWorkingDir="",
    workflowId=None,
    comments=None,
):
    newWorkflowId = None
    if workflowType in [
        "Undefined",
        "BioSAXS Post Processing",
        "EnhancedCharacterisation",
        "LineScan",
        "MeshScan",
        "Dehydration",
        "KappaReorientation",
        "BurnStrategy",
        "XrayCentering",
        "DiffractionTomography",
        "TroubleShooting",
        "VisualReorientation",
        "HelicalCharacterisation",
        "GroupedProcessing",
        "MXPressE",
        "MXPressO",
        "MXPressL",
        "MXScore",
        "MXPressI",
        "MXPressM",
        "MXPressA",
        "CollectAndSpectra",
        "LowDoseDC",
        "MXPressH",
        "MXPressP",
        "MXPressP_SAD",
        "MXPressR",
        "MXPressR_180",
        "MXPressR_dehydration",
        "MXPressF",
        "MXScore",
        "MeshAndCollect",
        "MeshAndCollectFromFile",
    ]:
        ispyb_workflow_type = workflowType
    else:
        ispyb_workflow_type = "Undefined"
    logger = workflow_logging.getLogger()
    client = getCollectionWebService(beamline)
    if workflowPyarchLogFile is not None:
        workflowLogFile = workflowPyarchLogFile
    elif workflowLogFile is not None:
        workflowLogFile = workflowLogFile
    if client is None:
        logger.error(
            "No web service client available, cannot contact storeOrUpdateDataCollectionGroup web service."
        )
    else:
        workflow3VO = client.factory.create("workflow3VO")
        workflow3VO.comments = comments
        workflow3VO.logFilePath = workflowLogFile
        workflow3VO.resultFilePath = pyarchHtmlDir
        workflow3VO.status = "Started"
        workflow3VO.workflowId = workflowId
        workflow3VO.workflowTitle = workflowTitle
        workflow3VO.workflowType = ispyb_workflow_type
        newWorkflowId = client.service.storeOrUpdateWorkflow(workflow3VO)
    return newWorkflowId


def storeWorkflowStep(
    beamline,
    workflowId,
    workflowStepType,
    status="Success",
    folderPath=None,
    imageResultFilePath=None,
    htmlResultFilePath=None,
    resultFilePath=None,
    comments=None,
):
    workflowStepID = None
    logger = workflow_logging.getLogger()
    client = getCollectionWebService(beamline)
    if client is None:
        logger.error(
            "No web service client available, cannot contact storeOrUpdateDataCollectionGroup web service."
        )
    else:
        try:
            workflowStep = json.dumps(
                {
                    "workflowId": workflowId,
                    "workflowStepType": workflowStepType,
                    "status": status,
                    "folderPath": folderPath,
                    "imageResultFilePath": imageResultFilePath,
                    "htmlResultFilePath": htmlResultFilePath,
                    "resultFilePath": resultFilePath,
                    "comments": comments,
                }
            )
            workflowStepID = client.service.storeWorkflowStep(workflowStep)
        except Exception as e:
            logger.error(
                "Error when trying to upload workflowStep to ISPyB: {0}".format(e)
            )
    return workflowStepID


def findProposal(beamline, code, number):
    client = getShippingWebService(beamline)
    proposal = client.service.findProposalByCodeAndNumber(code, number)
    return proposal


def findProposalByCodeAndNumber(beamline, code, number):
    client = getShippingWebService(beamline)
    proposal = client.service.findProposalByCodeAndNumber(code, number)
    return proposal


def findSessionsByProposalAndBeamLine(code, number, beamline):
    client = getCollectionWebService(beamline)
    proposal = client.service.findSessionsByProposalAndBeamLine(code, number, beamline)
    return proposal


def findPersonByProteinAcronym(beamline, code, number, acronym):
    logger = workflow_logging.getLogger()
    person = None
    if code.startswith("opid"):
        if beamline == "id30a2":
            # Test account - return a fake email address
            person = {"emailAddress": "svensson@esrf.fr"}
    else:
        proposal = findProposal(beamline, code, number)
        if proposal is not None:
            personWS3VO = None
            for _ in range(5):
                client = getShippingWebService(beamline)
                try:
                    personWS3VO = client.service.findPersonByProteinAcronym(
                        proposal.proposalId, acronym
                    )
                    break
                except Exception as e:
                    logger.warning("Cannot contact ISPyB web service")
                    logger.warning("Error message: %s", e)
                    logger.warning("Trying again...")
                    time.sleep(1)
            if personWS3VO is None:
                logger.warning(
                    "Cannot contact ISPyB web service 'findPersonByProteinAcronym'"
                )
            else:
                person = asdict(personWS3VO)
    return person


def updateDataCollectionSnapShots(beamline, dataCollectionId, listSnapShotPath):
    logger = workflow_logging.getLogger()
    client = getCollectionWebService(beamline)
    if client is None:
        logger.error(
            "No web service client available, cannot contact storeOrUpdateDataCollection web service."
        )
    dataCollectionWS3VO = findDataCollection(beamline, dataCollectionId, client=client)
    if dataCollectionWS3VO is not None:
        index = 1
        for snapShotPath in listSnapShotPath:
            setattr(
                dataCollectionWS3VO,
                "xtalSnapshotFullPath{0}".format(index),
                snapShotPath,
            )
            index += 1
            if index > 4:
                break
        dataCollectionId = storeOrUpdateDataCollection(
            beamline, dataCollectionWS3VO, client=client
        )
    return dataCollectionId


def setImageQualityIndicatorsPlot(beamline, dataCollectionId, plotFile, csvFile):
    logger = workflow_logging.getLogger()
    client = getCollectionWebService(beamline)
    if client is None:
        logger.error(
            "No web service client available, cannot contact setImageQualityIndicatorsPlot web service."
        )
    returnDataCollectionId = client.service.setImageQualityIndicatorsPlot(
        dataCollectionId, plotFile, csvFile
    )
    return returnDataCollectionId


def findDetector(beamline, type, manufacturer, model, mode):
    client = getCollectionWebService(beamline)
    res = client.service.findDetectorByParam("", manufacturer, model, mode)
    return res
