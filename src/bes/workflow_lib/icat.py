#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

"""
Workflow library module for connecting to ICAT
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.fr"
__copyright__ = "ESRF, 2023"
__updated__ = "2023-09-13"

import json
import os.path
import shutil
import pathlib

from edna2.utils import UtilsPath
from pyicat_plus.client.main import IcatClient

from bes import config
from bes.workflow_lib import workflow_logging


def get_sample_name(directory):
    path_directory = pathlib.Path(str(directory))
    metadata_path = path_directory / "metadata.json"
    sample_name = None
    if metadata_path.exists():
        with open(metadata_path) as f:
            metadata = json.loads(f.read())
        if "Sample_name" in metadata:
            sample_name = metadata["Sample_name"]
    if sample_name is None:
        dir1 = path_directory.parent
        if dir1.name.startswith("run"):
            dir2 = dir1.parent
            if dir2.name.startswith("run"):
                sample_name = dir2.parent.name
            else:
                sample_name = dir2.name
        else:
            sample_name = dir1.name
    return sample_name


def storeWorkflowStep(
    beamline,
    proposal,
    directory,
    workflowStepType,
    sample_name,
    workflow_name,
    workflow_type,
    bes_request_id,
    snap_shot_path,
    json_path,
    icat_sub_dir=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
):
    logger = workflow_logging.getLogger(beamline)
    if directory.endswith("/"):
        directory = directory[:-1]
    if sample_name is None:
        sample_name = get_sample_name(directory)
    # Create PROCESSED_DATA directory
    processed_dir = pathlib.Path(directory.replace("RAW_DATA", "PROCESSED_DATA"))
    if icat_sub_dir is not None:
        processed_dir = processed_dir / icat_sub_dir
    processed_dir.mkdir(mode=0o755, parents=True, exist_ok=False)
    # Copy image and json to gallery directory
    gallery_dir = processed_dir / "gallery"
    gallery_dir.mkdir(mode=0o755, exist_ok=False)
    gallery_image_path = gallery_dir / (
        workflowStepType + os.path.splitext(snap_shot_path)[1]
    )
    gallery_json_path = gallery_dir / (workflowStepType + "-report.json")
    shutil.copy(snap_shot_path, gallery_image_path)
    shutil.copy(json_path, gallery_json_path)
    # ICAT parameters
    icat_beamline = UtilsPath.getIcatBeamline(beamline)
    metadata_urls = config.get_value(
        beamline, "ICAT", "metadata_urls", default_value=None
    )
    client = IcatClient(metadata_urls=metadata_urls)
    metadata = {
        "scanType": workflowStepType,
        "Sample_name": sample_name,
        "Workflow_name": workflow_name,
        "Workflow_type": workflow_type,
        "Workflow_id": bes_request_id,
        "MX_position_id": position_id,
        "MX_characterisation_id": characterisation_id,
        "MX_kappa_settings_id": kappa_settings_id,
    }
    logger.debug(f"metadata_urls {metadata_urls}")
    logger.debug(f"beamline {icat_beamline}")
    logger.debug(f"proposal {proposal}")
    logger.debug(f"dataset {workflowStepType}")
    logger.debug(f"path {processed_dir}")
    logger.debug(f"metadata {metadata}")
    logger.debug(f"raw {directory}")
    client.store_processed_data(
        beamline=icat_beamline,
        proposal=proposal,
        dataset=workflowStepType,
        path=str(processed_dir),
        metadata=metadata,
        raw=[str(directory)],
    )
