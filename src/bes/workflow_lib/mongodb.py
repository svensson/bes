#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "21/02/2022"

import os
import pymongo


def _getMongoUrl():
    BESDB_HOST = os.environ.get("BESDB_HOST", "127.0.0.1")
    BESDB_PORT = os.environ.get("BESDB_PORT", 27017)
    mongoUrl = f"mongodb://bes:bes@{BESDB_HOST}:{BESDB_PORT}/bes"
    return mongoUrl


def getBesCollection():
    return pymongo.MongoClient(_getMongoUrl()).bes.bes


def setMongoStatus(bes_request_id, status):
    setMongoAttribute(bes_request_id=bes_request_id, attribute="status", value=status)


def setMongoAttribute(bes_request_id, attribute, value):
    collection = getBesCollection()
    dictWorkflow = collection.find_one({"Request ID": bes_request_id})
    dictWorkflow[attribute] = value
    collection.update_one(
        {"Request ID": bes_request_id}, {"$set": dictWorkflow}, upsert=False
    )
