"""
Workflow library module for CryoEM
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2017"
__updated__ = "2017-12-05"

import os
import glob
import time
import shlex
import urllib
import pprint
import httplib
import threading
import subprocess

from bes.workflow_lib import workflow_logging
from bes.workflow_lib import MetadataManagerClient


def processMovie(
    beamline,
    directory,
    movieFileName,
    proteinAcronym,
    sampleAcronym,
    doseInitial,
    dosePerFrame,
    cwd,
    timeout_sec=600,
):

    logger = workflow_logging.getLogger()

    commandLine = "/opt/pxsoft/bin/scipion"
    commandLine += " python"
    commandLine += " /mntdirect/_sware/exp/pxsoft/scipion/v1.1_ESRF/debian90-x86_64/scipion/scripts/esrf_launch_workflow.py"
    commandLine += " --directory {0}".format(directory)
    commandLine += " --template {0}".format(movieFileName)
    commandLine += " --protein {0}".format(proteinAcronym)
    commandLine += " --sample {0}".format(sampleAcronym)
    commandLine += " --doseInitial {0}".format(doseInitial)
    commandLine += " --dosePerFrame {0}".format(dosePerFrame)

    logger.debug("Movie processing: args = {0}".format(commandLine))
    argList = shlex.split(commandLine)
    # Submit processing
    proc = subprocess.Popen(
        argList, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd
    )

    def kill_proc(p):
        return p.kill()

    timer = threading.Timer(timeout_sec, kill_proc, [proc])
    try:
        timer.start()
        stdout, stderr = proc.communicate()
    finally:
        timer.cancel()

    return stdout, stderr


def startCryoEMProcessWF(
    beamline,
    directory,
    movieFileName,
    proteinAcronym,
    sampleAcronym,
    doseInitial,
    dosePerFrame,
    cwd,
    timeout_sec=600,
    workflow_name="CryoEMProcessMovie",
):

    bes_request_id = None
    logger = workflow_logging.getLogger()
    bes_host = "cmproc1"
    bes_port = 8090

    try:
        conn = httplib.HTTPConnection(bes_host, bes_port)
        dictSubmit = {
            "beamline": beamline,
            "directory": directory,
            "movieFileName": movieFileName,
            "proteinAcronym": proteinAcronym,
            "sampleAcronym": sampleAcronym,
            "doseInitial": doseInitial,
            "dosePerFrame": dosePerFrame,
            "cwd": cwd,
            "timeout_sec": timeout_sec,
        }
        params = urllib.urlencode(dictSubmit)
        conn.request(
            "POST",
            "/BES/bridge/rest/processes/{0}/RUN?{1}".format(workflow_name, params),
            headers={"Accept": "text/plain"},
        )
        response = conn.getresponse()
        if response.status != 200:
            logger.error("RUN response status = {0}".format(response.status))
        else:
            logger.info(
                "Job successfully submitted on {host}:{port}".format(
                    host=bes_host, port=bes_port
                )
            )
            bes_request_id = response.read()
            logger.info("Request id: %r" % bes_request_id)
    except Exception:
        logger.error(
            "Cannot connect to BES server on {host}:{port}!".format(
                host=bes_host, port=bes_port
            )
        )

    return bes_request_id


def startCryoEMDataArchive(beamline, directory, workflow_name="CryoEMDataArchive"):

    bes_request_id = None
    logger = workflow_logging.getLogger()
    bes_host = "cmproc1"
    bes_port = 8090

    try:
        conn = httplib.HTTPConnection(bes_host, bes_port)
        dictSubmit = {
            "beamline": beamline,
            "directory": directory,
        }
        params = urllib.urlencode(dictSubmit)
        conn.request(
            "POST",
            "/BES/bridge/rest/processes/{0}/RUN?{1}".format(workflow_name, params),
            headers={"Accept": "text/plain"},
        )
        response = conn.getresponse()
        if response.status != 200:
            logger.error("RUN response status = {0}".format(response.status))
        else:
            logger.info(
                "Job successfully submitted on {host}:{port}".format(
                    host=bes_host, port=bes_port
                )
            )
            bes_request_id = response.read()
            logger.info("Request id: %r" % bes_request_id)
    except Exception:
        logger.error(
            "Cannot connect to BES server on {host}:{port}!".format(
                host=bes_host, port=bes_port
            )
        )

    return bes_request_id


def findAllGridSquareDirectories(topLevelDirectory):
    listGridSquare = []
    MAX_DEPTH = 3
    for root, dirs, files in os.walk(topLevelDirectory, topdown=True):
        for dir in dirs:
            if os.path.basename(root) == "Images-Disc1" and dir.startswith(
                "GridSquare"
            ):
                # reldir = root.replace(topLevelDirectory + "/", "")
                listGridSquare.append(os.path.join(root, dir))
                # listGridSquare.append(dir)
        if root.count(os.sep) - topLevelDirectory.count(os.sep) == MAX_DEPTH - 1:
            del dirs[:]
    return listGridSquare


def updateDictGridSquareForNewMovies(dictGridSquare):
    logger = workflow_logging.getLogger()
    gridSquarePath = dictGridSquare["path"]
    logger.info("Grid square path: {0}".format(gridSquarePath))
    logger.debug(
        "updateDictGridSquareForNewMovies dictGridSquare {0} before".format(
            pprint.pformat(dictGridSquare)
        )
    )

    if "movies" in dictGridSquare:
        dictMovies = dictGridSquare["movies"]
    else:
        # First iteration
        dictMovies = {}
        dictGridSquare["movies"] = dictMovies

    listAllMovies = findAllMovies(gridSquarePath)

    noMoviesCreated = 0
    listMovieCreated = []
    for moviePath in listAllMovies:
        movieFileName = os.path.basename(moviePath)
        movieName = os.path.splitext(movieFileName)[0]
        if movieName not in dictMovies:
            ctimeCreated = os.path.getctime(moviePath)
            dictMovies[movieName] = {}
            dictMovies[movieName]["status"] = "created"
            dictMovies[movieName]["ctimeCreated"] = ctimeCreated
            dictMovies[movieName]["timeDateCreated"] = (time.ctime(ctimeCreated),)
            dictMovies[movieName]["path"] = moviePath
            logger.info("Creating movie: {0}".format(movieName))
            noMoviesCreated += 1
            listMovieCreated.append([dictMovies[movieName]["ctimeCreated"], movieName])
        elif dictMovies[movieName]["status"] == "created":
            listMovieCreated.append([dictMovies[movieName]["ctimeCreated"], movieName])

    # Sort listMovieCreated
    listMovieCreatedSorted = sorted(listMovieCreated, key=lambda x: x[0])
    # Remove ctimeCreated
    listMovieCreatedSorted = [x[1] for x in listMovieCreatedSorted]
    logger.debug(
        "Sorted listMovieCreated {0}".format(pprint.pformat(listMovieCreatedSorted))
    )
    logger.debug(
        "updateDictGridSquareForNewMovies dictGridSquare {0} after".format(
            pprint.pformat(dictGridSquare)
        )
    )

    return listMovieCreatedSorted


def updateDictForNewGridSquares(directory, dictGridSquares):
    logger = workflow_logging.getLogger()
    logger.debug(
        "updateDictForNewGridSquares no dictGridSquares {0}".format(
            len(dictGridSquares)
        )
    )
    listAllGridSquares = findAllGridSquareDirectories(directory)
    noGridSquaresCreated = 0
    listGridSquareCreated = []
    for gridSquarePath in listAllGridSquares:
        gridSquareName = os.path.basename(gridSquarePath)
        if gridSquareName not in dictGridSquares:
            ctimeCreated = os.path.getctime(gridSquarePath)
            dictGridSquares[gridSquareName] = {
                "status": "created",
                "ctimeCreated": ctimeCreated,
                "timeDateCreated": time.ctime(ctimeCreated),
                "path": gridSquarePath,
            }
            noGridSquaresCreated += 1

    # Get a sorted list of gridSquareName vs timeCreated
    listGridSquareCreated = []
    for gridSquareName in dictGridSquares:
        if dictGridSquares[gridSquareName]["status"] == "created":
            listGridSquareCreated.append(
                [dictGridSquares[gridSquareName]["ctimeCreated"], gridSquareName]
            )

    # Sort listGridSquareCreated
    listGridSquareCreatedSorted = sorted(listGridSquareCreated, key=lambda x: x[0])
    # logger.debug("Sorted listGridSquareCreatedSorted {0}".format(pprint.pformat(listGridSquareCreatedSorted)))
    # logger.debug("updateDictForNewGridSquares dictGridSquares after {0}".format(pprint.pformat(dictGridSquares)))

    return listGridSquareCreatedSorted


def findAllMovies(gridSquareDirectory):
    listMovies = glob.glob(
        os.path.join(gridSquareDirectory, "Data", "FoilHole_*-*.mrc")
    )
    return listMovies


def waitTillWFEnd(bes_request_id):
    logger = workflow_logging.getLogger()
    bes_host = "cmproc1"
    bes_port = 8090

    bContinue = True
    logger.debug("Waiting for request ID = {0}".format(bes_request_id))
    while bContinue:
        time.sleep(5)
        statusWorkflowURL = os.path.join(
            "/BES", "bridge", "rest", "processes", bes_request_id, "STATUS"
        )
        conn = httplib.HTTPConnection(bes_host, bes_port)
        conn.request("GET", statusWorkflowURL)
        response = conn.getresponse()
        if response.status == 200:
            workflowStatus = response.read()
            logger.debug("workflowStatus: {0}".format(workflowStatus))
            if workflowStatus != "STARTED":
                bContinue = False
        else:
            logger.error("ERROR! STATUS response status = {0}".format(response.status))
            bContinue = False


def findFilesToArchive(moviePath):
    listFiles = []
    movieDir = os.path.dirname(moviePath)
    processDir = os.path.join(movieDir, "process")
    if os.path.exists(processDir):
        movieName = os.path.splitext(os.path.basename(moviePath))[0]
        movieProcessDir = os.path.join(processDir, movieName)
        if os.path.exists(movieProcessDir):
            listFiles = glob.glob(os.path.join(movieProcessDir, "*"))
    return listFiles


def archiveDataFiles(listFiles, directory, proposal, sample, dataSetName):
    logger = workflow_logging.getLogger()
    metadataManagerName = "cm01/metadata/ingest"
    metaExperimentName = "cm01/metadata/experiment"
    client = MetadataManagerClient.MetadataManagerClient(
        metadataManagerName, metaExperimentName
    )
    client.start(directory, proposal, sample, dataSetName)
    for filePath in listFiles:
        archivePath = filePath.replace(directory + "/", "")
        logger.debug("Archiving file: {0}".format(archivePath))
        client.appendFile(archivePath)
    dictMetadata = {"definition": "EM"}
    for attributeName, value in dictMetadata.items():
        logger.debug(
            "Setting metadata client attribute '{0}' to '{1}'".format(
                attributeName, value
            )
        )
        setattr(client.metadataManager, attributeName, str(value))
    logger.info(client.getStatus())
    client.end()
