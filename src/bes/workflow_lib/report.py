#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "09/05/2016"

import os
import html
import PIL.Image
import json
import time
import base64
import tempfile

from bes.workflow_lib.edna_kernel import markupv1_10

# Report version
REPORT_VERSION = 1.0


class WorkflowStepReport(object):
    def __init__(self, stepType, stepTitle=""):
        self.image_list = None
        self.dict_report = {}
        self.dict_report["version"] = REPORT_VERSION
        self.dict_report["type"] = stepType
        self.dict_report["title"] = stepTitle
        self.dict_report["items"] = []

    def setTitle(self, stepTitle):
        self.dict_report["title"] = stepTitle

    def addInfo(self, infoText):
        self.dict_report["items"].append({"type": "info", "value": infoText})

    def addWarning(self, infoText):
        self.dict_report["items"].append({"type": "warning", "value": infoText})

    def startImageList(self):
        self.image_list = {}
        self.image_list["type"] = "images"
        self.image_list["items"] = []

    def endImageList(self):
        self.dict_report["items"].append(self.image_list)
        self.image_list = None

    def __createImageItem(
        self,
        pathToImage,
        imageTitle,
        pathToThumbnailImage=None,
        thumbnailHeight=None,
        thumbnailWidth=None,
    ):
        item = {}
        item["type"] = "image"
        item["suffix"] = pathToImage.split(".")[-1]
        item["title"] = imageTitle
        im = PIL.Image.open(pathToImage)
        item["xsize"] = im.size[0]
        item["ysize"] = im.size[1]
        item["value"] = base64.b64encode(open(pathToImage, "rb").read()).decode("utf-8")
        if pathToThumbnailImage is None:
            if thumbnailHeight is not None and thumbnailWidth is not None:
                item["thumbnailSuffix"] = pathToImage.split(".")[-1]
                item["thumbnailXsize"] = thumbnailHeight
                item["thumbnailYsize"] = thumbnailWidth
                item["thumbnailValue"] = base64.b64encode(open(pathToImage).read())
        else:
            item["thumbnailSuffix"] = pathToThumbnailImage.split(".")[-1]
            thumbnailIm = PIL.Image.open(pathToThumbnailImage)
            item["thumbnailXsize"] = thumbnailIm.size[0]
            item["thumbnailYsize"] = thumbnailIm.size[1]
            item["thumbnailValue"] = base64.b64encode(
                open(pathToThumbnailImage, "rb").read()
            ).decode("utf-8")
        return item

    def addImage(
        self,
        pathToImage,
        imageTitle="",
        pathToThumbnailImage=None,
        thumbnailHeight=None,
        thumbnailWidth=None,
    ):
        if self.image_list is None:
            self.dict_report["items"].append(
                self.__createImageItem(
                    pathToImage,
                    imageTitle,
                    pathToThumbnailImage,
                    thumbnailHeight,
                    thumbnailWidth,
                )
            )
        else:
            self.image_list["items"].append(
                self.__createImageItem(
                    pathToImage,
                    imageTitle,
                    pathToThumbnailImage,
                    thumbnailHeight,
                    thumbnailWidth,
                )
            )

    def addTable(self, title, columns, data, orientation="horizontal"):
        item = {}
        item["type"] = "table"
        item["title"] = title
        item["columns"] = columns
        item["data"] = data
        item["orientation"] = orientation
        self.dict_report["items"].append(item)

    def addLogFile(self, title, linkText, pathToLogFile):
        item = {}
        item["type"] = "logFile"
        item["title"] = title
        item["linkText"] = linkText
        item["logText"] = str(open(pathToLogFile).read(), errors="ignore")
        self.dict_report["items"].append(item)

    def getDictReport(self):
        return self.dict_report

    def renderJson(self, pathToJsonDir):
        pathToJsonFile = os.path.join(pathToJsonDir, "report.json")
        open(pathToJsonFile, "w").write(json.dumps(self.dict_report, indent=4))
        return pathToJsonFile

    def escapeCharacters(self, strValue):
        strValue = html.escape(strValue)
        strValue = strValue.replace(str("Å"), "&Aring;")
        strValue = strValue.replace(str("°"), "&deg;")
        strValue = strValue.replace("\n", "<br>")
        return strValue

    def renderHtml(self, pathToHtmlDir, nameOfIndexFile="index.html"):
        page = markupv1_10.page(mode="loose_html")
        page.init(
            title=self.dict_report["title"], footer="Generated on %s" % time.asctime()
        )
        page.div(align_="LEFT")
        page.h1()
        page.strong(self.dict_report["title"])
        page.h1.close()
        page.div.close()
        for item in self.dict_report["items"]:
            if "value" in item:
                itemValue = item["value"]
            if "title" in item:
                itemTitle = self.escapeCharacters(item["title"])
            if item["type"] == "info":
                page.p(itemValue)
            if item["type"] == "warning":
                page.font(_color="red", size="+1")
                page.p()
                page.strong(itemValue)
                page.p.close()
                page.font.close()
            elif item["type"] == "image":
                self.__renderImage(page, item, pathToHtmlDir)
                page.br()
                page.p(itemTitle)
            elif item["type"] == "images":
                page.table()
                page.tr(align_="CENTER")
                for item in item["items"]:
                    itemTitle = self.escapeCharacters(item["title"])
                    page.td()
                    page.table()
                    page.tr()
                    page.td()
                    self.__renderImage(page, item, pathToHtmlDir)
                    page.td.close()
                    page.tr.close()
                    page.tr(align_="CENTER")
                    page.td(itemTitle)
                    page.tr.close()
                    page.table.close()
                    page.td.close()
                page.tr.close()
                page.table.close()
                page.br()
            elif item["type"] == "table":
                titleBgColour = "#99c2ff"
                columnTitleBgColour = "#ffffff"
                dataBgColour = "#e6f0ff"
                page.table(
                    border_="1",
                    cellpadding_="2",
                    width="100%",
                    style_="border: 1px solid black; border-collapse: collapse; margin: 0px; font-size: 12px",
                )
                page.tr(align_="LEFT")
                # page.strong(itemTitle)
                page.th(
                    itemTitle,
                    bgcolor_=titleBgColour,
                    align_="LEFT",
                    style_="border: 1px solid black; border-collapse: collapse; padding: 5px;",
                    colspan_=str(len(item["columns"])),
                )
                page.tr.close()
                if "orientation" in item and item["orientation"] == "vertical":
                    for index1 in range(len(item["columns"])):
                        itemColumn = self.escapeCharacters(item["columns"][index1])
                        page.tr(align_="LEFT")
                        page.th(
                            itemColumn,
                            bgcolor_=columnTitleBgColour,
                            align_="LEFT",
                            style_="border: 1px solid black; border-collapse: collapse; padding: 5px;",
                        )
                        for index2 in range(len(item["data"])):
                            itemData = self.escapeCharacters(
                                str(item["data"][index2][index1])
                            )
                            page.th(
                                itemData, bgcolor_=dataBgColour, style_="padding: 5px;"
                            )
                        page.tr.close()
                else:
                    page.tr(
                        align_="LEFT",
                        bgcolor_=columnTitleBgColour,
                        style_="border: 1px solid black; border-collapse: collapse; padding: 5px;",
                    )
                    for column in item["columns"]:
                        itemColumn = self.escapeCharacters(column)
                        page.th(
                            itemColumn,
                            style_="border: 1px solid black; border-collapse: collapse; padding: 5px;",
                        )
                    page.tr.close()
                    for listRow in item["data"]:
                        page.tr(
                            align_="LEFT",
                            bgcolor_=dataBgColour,
                            style_="border-collapse: collapse; padding: 5px;",
                        )
                        for cell in listRow:
                            itemCell = self.escapeCharacters(str(cell))
                            page.th(
                                itemCell,
                                style_="border: 1px solid black; border-collapse: collapse; padding: 5px;",
                            )
                        page.tr.close()
                page.table.close()
                page.br()
            elif item["type"] == "logFile":
                pathToLogHtml = os.path.join(pathToHtmlDir, itemTitle + ".html")
                if os.path.exists(pathToLogHtml):
                    fd, pathToLogHtml = tempfile.mkstemp(
                        suffix=".html",
                        prefix=itemTitle.replace(" ", "_") + "_",
                        dir=pathToHtmlDir,
                    )
                    os.close(fd)
                pageLogHtml = markupv1_10.page()
                pageLogHtml.h1(itemTitle)
                pageLogHtml.pre(page.escape(item["logText"]))
                open(pathToLogHtml, "w").write(str(pageLogHtml))
                os.chmod(pathToLogHtml, 0o644)
                page.p()
                page.a(item["linkText"], href_=os.path.basename(pathToLogHtml))
                page.p.close()
        html = str(page)
        pagePath = os.path.join(pathToHtmlDir, nameOfIndexFile)
        filePage = open(pagePath, "w")
        filePage.write(html)
        filePage.close()
        return pagePath

    def __renderImage(self, page, item, pathToHtmlDir):
        imageName = item["title"].replace(" ", "_")
        pathToImage = os.path.join(
            pathToHtmlDir, "{0}.{1}".format(imageName, item["suffix"])
        )
        if os.path.exists(pathToImage):
            fd, pathToImage = tempfile.mkstemp(
                suffix="." + item["suffix"], prefix=imageName + "_", dir=pathToHtmlDir
            )
            os.close(fd)
        open(pathToImage, "wb").write(base64.b64decode(item["value"]))
        os.chmod(pathToImage, 0o644)
        if "thumbnailValue" in item:
            thumbnailImageName = imageName + "_thumbnail"
            pathToThumbnailImage = os.path.join(
                pathToHtmlDir, "{0}.{1}".format(thumbnailImageName, item["suffix"])
            )
            if os.path.exists(pathToThumbnailImage):
                fd, pathToThumbnailImage = tempfile.mkstemp(
                    suffix="." + item["suffix"],
                    prefix=thumbnailImageName + "_",
                    dir=pathToHtmlDir,
                )
                os.close(fd)
            open(pathToThumbnailImage, "wb").write(
                base64.b64decode(item["thumbnailValue"])
            )
            os.chmod(pathToThumbnailImage, 0o644)
            pageReferenceImage = markupv1_10.page(mode="loose_html")
            pageReferenceImage.init(
                title=imageName, footer="Generated on %s" % time.asctime()
            )
            pageReferenceImage.h1(imageName)
            pageReferenceImage.br()
            pageReferenceImage.img(
                src=os.path.basename(pathToImage),
                title=imageName,
                width=item["xsize"],
                height=item["ysize"],
            )
            pageReferenceImage.br()
            pathPageReferenceImage = os.path.join(
                pathToHtmlDir, "{0}.html".format(imageName)
            )
            filePage = open(pathPageReferenceImage, "w")
            filePage.write(str(pageReferenceImage))
            filePage.close()
            page.a(href=os.path.basename(pathPageReferenceImage))
            page.img(
                src=os.path.basename(pathToThumbnailImage),
                title=imageName,
                width=item["thumbnailXsize"],
                height=item["thumbnailYsize"],
            )
            page.a.close()
        else:
            page.img(
                src=os.path.basename(pathToImage),
                title=item["title"],
                width=item["xsize"],
                height=item["ysize"],
            )
