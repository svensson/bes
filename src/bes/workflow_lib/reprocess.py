"""
Workflow library module for connecting to extISPyB
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2016"
__updated__ = "2016-07-25"

import os
import string
import tempfile

from bes.workflow_lib import edna_mxv1

# from edna_mxv1 import XSDataEDNAprocInput

scriptTemplate = """#!/usr/bin/env python

import os
import sys
import time
import socket
import traceback

sys.path.insert(0, "/opt/pxsoft/EDNA/vMX_TEST/edna/kernel/src")

from EDVerbose import EDVerbose
from EDFactoryPluginStatic import EDFactoryPluginStatic

dataCollectionId = $dataCollectionId
inputFile = "$inputFile"
pluginBaseDir = "$pluginBaseDir"

# Remove start and end file if existing
startFile = os.path.join(pluginBaseDir, "STARTED")
finishFile = os.path.join(pluginBaseDir, "FINISHED")
if os.path.exists(startFile):
    os.remove(startFile)
if os.path.exists(finishFile):
    os.remove(finishFile)

pluginName = "$pluginName"
os.environ["EDNA_SITE"] = "$EDNA_SITE"
os.environ["ISPyB_user"]="$ispybUserName"
os.environ["ISPyB_pass"]="$ispybPassword"

EDVerbose.screen("Executing EDNA plugin %s" % pluginName)
EDVerbose.screen("EDNA_SITE %s" % os.environ["EDNA_SITE"])

hostname = socket.gethostname()
dateString  = time.strftime("%Y%m%d", time.localtime(time.time()))
timeString = time.strftime("%H%M%S", time.localtime(time.time()))

baseName = "{0}_EDNA_proc".format(timeString)
baseDir = os.path.join(pluginBaseDir, baseName)
if not os.path.exists(baseDir):
    try:
        os.makedirs(baseDir, 0o755)
    except Exception:
        pass

EDVerbose.screen("EDNA plugin working directory: %s" % baseDir)

if os.path.exists("$logFilePath"):
    logFilePath = os.path.join(pluginBaseDir, "EDNA_proc_{0}-{1}.log".format(dateString, timeString))
else:
    logFilePath = "$logFilePath"

EDVerbose.setLogFileName(logFilePath)
EDVerbose.setVerboseOn()

edPlugin = EDFactoryPluginStatic.loadPlugin(pluginName)
edPlugin.setDataInput(open(inputFile).read())
edPlugin.setBaseDirectory(pluginBaseDir)
edPlugin.setBaseName(baseName)

EDVerbose.screen("Start of execution of EDNA plugin %s" % pluginName)
os.chdir(baseDir)
open(startFile, "w").write("Started")
edPlugin.executeSynchronous()
open(finishFile, "w").write("Finished")

"""


def prepareReprocessScript(
    autoProcProgram="EDNA_proc",
    dataCollectionId=None,
    startImage=None,
    endImage=None,
    resolutionCutoff=None,
    workingDir=None,
    logFilePath=None,
):

    reprocessScriptPath = None
    if workingDir is None:
        workingDir = tempfile.mkdtemp(prefix="Reprocess_")

    if autoProcProgram == "EDNA_proc":
        outputFile = os.path.join(workingDir, "EDNA_ispyb.xml")
        xsDataEDNAprocInput = edna_mxv1.createEDNAprocInput(
            dataCollectionId=dataCollectionId,
            startImage=startImage,
            endImage=endImage,
            resolutionCutoff=resolutionCutoff,
            outputFile=outputFile,
        )
        inputFileName = "EDNA_proc_input.xml"
        inputFilePath = os.path.join(workingDir, inputFileName)
        open(inputFilePath, "w").write(xsDataEDNAprocInput.marshal())

    template = string.Template(scriptTemplate)
    script = template.substitute(
        pluginName="EDPluginControlEDNAprocv1_0",
        pluginBaseDir=workingDir,
        dataCollectionId=dataCollectionId,
        inputFile=inputFilePath,
        ispybUserName=os.environ["ISPyB_user"],
        ispybPassword=os.environ["ISPyB_pass"],
        EDNA_SITE="ESRF_ISPyBTest",
        logFilePath=logFilePath,
    )
    reprocessScriptFileName = "launcher.sh"
    reprocessScriptPath = os.path.join(workingDir, reprocessScriptFileName)
    open(reprocessScriptPath, "w").write(script)
    os.chmod(reprocessScriptPath, 0o755)

    return reprocessScriptPath
