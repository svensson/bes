"""
Created on Jun 5, 2015

@author: svensson
"""

import os
import json

import numpy
import datetime
import dateutil
import requests
import scipy.stats
import dateutil.parser
import MySQLdb

from .mongodb import getBesCollection
from . import aperture_utils

database = "besdb5"


def getCursor(cursor=None):
    if cursor is None:
        # Fix for converting timestamps for MariaDB
        # http://stackoverflow.com/questions/15420470/mysqldb-converts-timestamp-data-to-none
        orig_conv = MySQLdb.converters.conversions
        conv_iter = iter(orig_conv)
        convert = dict(
            zip(
                conv_iter,
                [
                    str,
                ]
                * len(orig_conv.keys()),
            )
        )
        con = MySQLdb.connect(
            database, "passerelle", "passerelle", "passerelle", conv=convert
        )
        cursor = con.cursor()
    return cursor


def getDictRequestAttribute(bes_request_id, cursor=None):
    dictAttribute = {}
    cursor = getCursor(cursor)
    cursor.execute(
        "SELECT * FROM pas_requestattribute where DTYPE = 'RequestAttributeImpl' and REQUEST_ID = {0};".format(
            bes_request_id
        )
    )
    listAttribute = cursor.fetchall()
    for attributeTuple in listAttribute:
        dictAttribute[attributeTuple[4]] = attributeTuple[5]
    return dictAttribute


def getDictResultAttribute(bes_request_id, cursor=None):
    dictAttribute = {}
    cursor = getCursor(cursor)
    cursor.execute(
        "SELECT * FROM pas_resultblock where TASK_ID = '{0}';".format(bes_request_id)
    )
    listResultBlock = cursor.fetchall()
    for resultBlock in listResultBlock:
        blockID = resultBlock[0]
        cursor.execute(
            "SELECT * FROM pas_resultitem where RESULTBLOCK_ID = '{0}';".format(blockID)
        )
        listResultItem = cursor.fetchall()
        for resultItem in listResultItem:
            dictAttribute[resultItem[4]] = resultItem[5]
    return dictAttribute


def getNumberOfPDBDepositions(beamline):
    if "http_proxy" not in os.environ:
        os.environ["http_proxy"] = "http://proxy.esrf.fr:3128"
    if "https_proxy" not in os.environ:
        os.environ["https_proxy"] = "https://proxy.esrf.fr:3128"
    # ## PDB functions ##
    url = "https://www.ebi.ac.uk/pdbe/search/pdb/select"
    if beamline == "id30a1":
        pdbBeamline = "MASSIF-1"
    elif beamline == "id30a3":
        pdbBeamline = "MASSIF-3"
    #    elif beamline == "id30b":
    #        pdbBeamline = "MASSIF-B"
    elif beamline == "id23eh1":
        pdbBeamline = "id23-1"
    elif beamline == "id23eh2":
        pdbBeamline = "id23-2"
    else:
        pdbBeamline = beamline
    query = {"q": "synchrotron_beamline:{0}".format(pdbBeamline), "wt": "json"}
    r = requests.post(url, data=query)
    numResult = None
    if r.status_code == 200:
        result = json.loads(r.text)
        numResult = result["response"]["numFound"]
    return numResult


def getNumberOfMXPress(
    beamline, y1, mo1, d1, h1, mi1, y2, mo2, d2, h2, mi2, type="MXPressA"
):
    collection = getBesCollection()
    # nameQuery = {"name": {"$regex": "MXPress", "$options": "i"}}
    nameQuery = {"name": type}
    # initiatorQuery = {"initiator": {"$regex": beamline, "$options": "i"}}
    initiatorQuery = {"initiator": beamline}
    d1 = datetime.datetime(y1, mo1, d1, h1, mi1)
    d2 = datetime.datetime(y2, mo2, d2, h2, mi2)
    startDateQuery = {"startTime": {"$gt": d1}}
    stopDateQuery = {"startTime": {"$lt": d2}}
    query = {"$and": [initiatorQuery, nameQuery, startDateQuery, stopDateQuery]}
    no_documents = collection.count_documents(query)
    return no_documents


def getRunningWorkflows():
    collection = getBesCollection()
    # stateQuery = { "status":  { "$in": [ "started", "error" ]} }
    stateQuery = {"status": "started"}
    d1 = datetime.datetime(2023, 11, 29, 20, 0)
    d2 = datetime.datetime(2023, 11, 30, 13, 0)
    startDateQuery = {"startTime": {"$gt": d1}}
    stopDateQuery = {"startTime": {"$lt": d2}}
    query = {"$and": [stateQuery, startDateQuery, stopDateQuery]}
    with open("/tmp/tt2.txt", "w") as f:
        for document in collection.find(query):
            list_actors = document["actors"]
            for actor in list_actors:
                if "outData" in actor:
                    if "slurmJobId" in actor["outData"]:
                        slurm_id = actor["outData"]["slurmJobId"]
                        f.write(f"{slurm_id}\n")
    #     # list_document.append(document)
    #     # pprint.pprint(document)
    #     break
    # print(len(list_document))


def getTotalNumberOfMXPress(beamline, year, type="MXPressA"):
    collection = getBesCollection()
    nameQuery = {"name": {"$regex": type, "$options": "i"}}
    # nameQuery = {"name": type}
    # initiatorQuery = {"initiator": {"$regex": beamline, "$options": "i"}}
    initiatorQuery = {"initiator": beamline}
    d1 = datetime.datetime(year, 1, 1, 1, 1)
    d2 = datetime.datetime(year + 1, 1, 1, 1, 1)
    startDateQuery = {"startTime": {"$gt": d1}}
    stopDateQuery = {"startTime": {"$lt": d2}}
    query = {"$and": [initiatorQuery, nameQuery, startDateQuery, stopDateQuery]}
    no_documents = collection.count_documents(query)
    return no_documents
    # with open("/tmp_14_days/svensson/MXPressR.csv", "w") as f:
    #     for workflow in collection.find(query, {"actors": 0}):
    #         if "logFile" in workflow:
    #             start_time = workflow["startTime"].strftime("%Y-%m-%d %H:%M")
    #             # print(start_time)
    #             raw_data = pathlib.Path(workflow["logFile"].replace("PROCESSED_DATA", "RAW_DATA")).parents[1]
    #             # print(raw_data)
    #             # pprint.pprint(workflow)
    #             f.write(f"{start_time},{workflow['status']},{workflow['name']},{raw_data}\n")
    #             listWorkflows.append(workflow)
    # return len(listWorkflows)


def checkCharacterisation(date1, date2):
    collection = getBesCollection()
    nameQuery = {"name": {"$regex": "Characterisation"}}
    listResults = []
    for beamline in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
        initiatorQuery = {"initiator": {"$regex": beamline}}
        d1 = dateutil.parser.parse(date1 + " 00:00:00.0000")
        d2 = dateutil.parser.parse(date2 + " 23:59:59.9999")
        startDateQuery = {"startTime": {"$gt": d1}}
        stopDateQuery = {"stopTime": {"$lt": d2}}
        query = {"$and": [nameQuery, initiatorQuery, startDateQuery, stopDateQuery]}
        noTotal = 0
        noEqual = 0
        noNotEqual = 0
        noTotalEdna = 0
        noOnlyEdna = 0
        noOnlyEdnaP1 = 0
        noOnlyEdna2 = 0
        listInputFileWithOnlyEdna = []
        for workflow in collection.find(query):
            for actor in workflow["actors"]:
                if "Compare characterisations" in actor["name"]:
                    ednaSpaceGroupNumber = None
                    edna2SpaceGroupNumber = None
                    # edna2CounterSpaceGroup = None
                    outData = actor.get("outData", None)
                    statusMessage = None
                    if outData is not None:
                        if "edna2IndexingOutput" in outData:
                            if (
                                "resultIndexing" in outData["edna2IndexingOutput"]
                                and "spaceGroupNumber"
                                in outData["edna2IndexingOutput"]["resultIndexing"]
                            ):
                                edna2SpaceGroupNumber = outData["edna2IndexingOutput"][
                                    "resultIndexing"
                                ]["spaceGroupNumber"]
                                # edna2CounterSpaceGroup = outData["edna2IndexingOutput"]["resultIndexing"]["counterSpaceGroup"]
                        if "ednaIndexingOutput" in outData:
                            if "selectedSolution" in outData["ednaIndexingOutput"]:
                                ednaSpaceGroupNumber = outData["ednaIndexingOutput"][
                                    "selectedSolution"
                                ]["crystal"]["spaceGroup"]["ITNumber"]
                            statusMessage = outData["ednaIndexingOutput"].get(
                                "statusMessage", None
                            )
                    noTotal += 1
                    if (
                        statusMessage is not None
                        and "Strategy calculation successful." in statusMessage
                    ):
                        if ednaSpaceGroupNumber is not None:
                            noTotalEdna += 1
                            if edna2SpaceGroupNumber is None:
                                noOnlyEdna += 1
                                listInputFileWithOnlyEdna.append(
                                    workflow["actors"][0]["inData"]["inputFile"]
                                )
                                if ednaSpaceGroupNumber == 1:
                                    noOnlyEdnaP1 += 1
                            elif ednaSpaceGroupNumber == edna2SpaceGroupNumber:
                                noEqual += 1
                            else:
                                noNotEqual += 1
                        elif edna2SpaceGroupNumber is not None:
                            noOnlyEdna2 += 1
        listResults.append(
            {
                beamline: {
                    "listInputFileWithOnlyEdna": listInputFileWithOnlyEdna,
                    "noTotal": noTotal,
                    "noTotalEdna": noTotalEdna,
                    "noEqual": noEqual,
                    "noNotEqual": noNotEqual,
                    "noOnlyEdna": noOnlyEdna,
                    "noOnlyEdnaP1": noOnlyEdnaP1,
                    "noOnlyEdna2": noOnlyEdna2,
                }
            }
        )

    return listResults


def getSlurmStartDelay(date1, date2, name=None):
    collection = getBesCollection()
    listSlurmStartDelay = []
    if name is None:
        nameQuery = {"name": {"$regex": "SLURM", "$options": "i"}}
    else:
        nameQuery = {"name": name}
    d1 = dateutil.parser.parse(date1 + " 00:00:00.0000")
    d2 = dateutil.parser.parse(date2 + " 23:59:59.9999")
    startDateQuery = {"startTime": {"$gt": d1}}
    stopDateQuery = {"stopTime": {"$lt": d2}}
    query = {"$and": [nameQuery, startDateQuery, stopDateQuery]}
    for workflow in collection.find(query):
        startTime = workflow["startTime"]
        listWorkflow = [
            str(startTime),
            workflow["name"],
            workflow["actors"][0]["inData"]["numberOfImages"],
        ]
        for actor in workflow["actors"]:
            if "stopTime" in actor:
                stopTime = actor["stopTime"]
                delay = stopTime - startTime
                delayInSeconds = delay.total_seconds()
                listWorkflow.append(delayInSeconds)
                startTime = stopTime
        listSlurmStartDelay.append(listWorkflow)
    return listSlurmStartDelay


def getTotalNumberOfMXPressOrig(beamline, year):
    cursor = getCursor()
    cursor.execute(
        "SELECT * FROM pas_request where DTYPE = 'MAINREQUEST' and TYPE like '%MXPress%' and CREATION_TS > '{0}-01-01' and CREATION_TS < '{1}-01-01';".format(
            year, year + 1
        )
    )
    listMXPress = cursor.fetchall()
    listMXPress2 = []
    for mxPress in listMXPress:
        requestIDMXPress = mxPress[0]
        # Check that it was executed on ID30a1
        cursor.execute(
            "SELECT * FROM pas_requestattribute where DTYPE = 'RequestAttributeImpl' and REQUEST_ID = {0};".format(
                requestIDMXPress
            )
        )
        listAttribute = cursor.fetchall()
        for attributeTuple in listAttribute:
            if attributeTuple[4] == "directory":
                directory = attributeTuple[5]
                if beamline in directory:
                    listMXPress2.append(mxPress)
    # Add 635 which is the number of mxpress before purging the database...
    # And add 164 for April 16th-18th when mxhpc2-1704 was replacing mxhpc2-1705...
    return len(listMXPress2)


def get2DSampleSize(cursor, actorName, parentContextId):
    crystalSizeX = None
    crystalSizeY = None
    cursor.execute(
        "SELECT * FROM pas_request where INITIATOR like '%{0}%' AND PARENT_CONTEXT_ID = {1};".format(
            actorName, parentContextId
        )
    )
    listActor = cursor.fetchall()
    for actor in listActor:
        taskID = actor[0]
        cursor.execute(
            "SELECT * FROM pas_resultblock where TASK_ID = '{0}';".format(taskID)
        )
        listResultBlock = cursor.fetchall()
        for resultBlock in listResultBlock:
            blockID = resultBlock[0]
            cursor.execute(
                "SELECT * FROM pas_resultitem where RESULTBLOCK_ID = '{0}';".format(
                    blockID
                )
            )
            listResultItem = cursor.fetchall()
            for resultItem in listResultItem:
                if "crystalSizeX" in resultItem:
                    crystalSizeX = resultItem[5]
                elif "crystalSizeY" in resultItem:
                    crystalSizeY = resultItem[5]

    return crystalSizeX, crystalSizeY


def get1DSampleSize(cursor, actorName, parentContextId):
    crystalSizeZ = None
    cursor.execute(
        "SELECT * FROM pas_request where INITIATOR like '%{0}%' AND PARENT_CONTEXT_ID = {1};".format(
            actorName, parentContextId
        )
    )
    listActor = cursor.fetchall()
    for actor in listActor:
        taskID = actor[0]
        cursor.execute(
            "SELECT * FROM pas_resultblock where TASK_ID = '{0}';".format(taskID)
        )
        listResultBlock = cursor.fetchall()
        for resultBlock in listResultBlock:
            blockID = resultBlock[0]
            cursor.execute(
                "SELECT * FROM pas_resultitem where RESULTBLOCK_ID = '{0}';".format(
                    blockID
                )
            )
            listResultItem = cursor.fetchall()
            for resultItem in listResultItem:
                if "crystalSizeZ" in resultItem:
                    crystalSizeZ = resultItem[5]

    return crystalSizeZ


def getExecutionTime(cursor, bes_request_id):
    executionTime = None
    contextId = getContextId(cursor, bes_request_id)
    cursor.execute("SELECT * FROM pas_event where CONTEXT_ID = {0};".format(contextId))
    listEvent = cursor.fetchall()
    started = None
    finished = None
    for event in listEvent:
        if "CREATED" in event:
            started = dateutil.parser.parse(event[5])
        elif "FINISHED" in event:
            finished = dateutil.parser.parse(event[5])
    if started is not None and finished is not None:
        deltaTime = finished - started
        executionTime = deltaTime.seconds + deltaTime.microseconds / 1.0e6
    return executionTime


def getListActorExecutionTime(cursor, actorName, parentContextId):
    listTime = []
    cursor.execute(
        "SELECT * FROM pas_request where INITIATOR like '%{0}%' AND PARENT_CONTEXT_ID = {1};".format(
            actorName, parentContextId
        )
    )
    listActor = cursor.fetchall()
    for actor in listActor:
        requestID = actor[0]
        executionTime = getExecutionTime(cursor, requestID)
        if executionTime is not None:
            listTime.append(executionTime)
    return listTime


def getContextId(cursor, requestIDMXPressE):
    cursor.execute(
        "SELECT * FROM pas_context where REQUEST_ID = '{0}';".format(requestIDMXPressE)
    )
    context = cursor.fetchall()[0]
    return context[0]


def getCrystalVolumeStatisticsForLastWeek(beamline, startDate=None, endDate=None):
    listCrystalSizeX = []
    listCrystalSizeY = []
    listCrystalSizeZ = []
    listVolume = []
    listMXPress2 = []
    # Find the last MPressE record
    cursor = getCursor()
    cursor.execute(
        "SELECT * FROM pas_request where DTYPE = 'MAINREQUEST' and INITIATOR = '{beamline}' and TYPE like '%MXPress%' ORDER BY ID DESC LIMIT 1;".format(
            beamline=beamline
        )
    )
    lastMXPressE = cursor.fetchall()[0]
    if endDate is None:
        datetime2 = dateutil.parser.parse(lastMXPressE[10])
    else:
        datetime2 = endDate
    oneWeek = datetime.timedelta(weeks=1)
    if startDate is None:
        datetime1 = datetime2 - oneWeek
    else:
        datetime1 = startDate
    # Check if last MXPress was executed more than one week ago
    dateTimeNow = datetime.datetime.now()
    dateTimeOneWeekAgo = dateTimeNow - oneWeek
    if dateTimeOneWeekAgo > datetime2:
        olderThanOneWeek = True
    else:
        olderThanOneWeek = False
    #    datetime1 = datetime.datetime(2016, 9, 27, 10, 40, 35)
    #    datetime2 = datetime.datetime(2016, 10, 13, 16, 47, 6, 943000)
    #    d1 = datetime.date(2015, 1, 1)
    #    d2 = datetime.date(2015, 12, 31)
    #    t1 = datetime.time(00, 00)
    #    t2 = datetime.time(23, 59)
    #    datetime1 = datetime.datetime.combine(d1, t1)
    #    datetime2 = datetime.datetime.combine(d2, t2)
    cursor.execute(
        "SELECT * FROM pas_request where DTYPE = 'MAINREQUEST' and TYPE like '%MXPress%' and CREATION_TS > '{year1}-{month1}-{day1}T{hour1:02d}:{minute1:02d}:00.000' and CREATION_TS < '{year2}-{month2}-{day2}T{hour2:02d}:{minute2:02d}:00.000';".format(
            year1=datetime1.year,
            month1=datetime1.month,
            day1=datetime1.day,
            hour1=datetime1.hour,
            minute1=datetime1.minute,
            year2=datetime2.year,
            month2=datetime2.month,
            day2=datetime2.day,
            hour2=datetime2.hour,
            minute2=datetime2.minute,
        )
    )
    listMXPress = cursor.fetchall()
    #    print(len(listMXPress))

    for mxPress in listMXPress:
        requestIDMXPress = mxPress[0]
        # Check that it was executed on ID30a1
        cursor.execute(
            "SELECT * FROM pas_requestattribute where DTYPE = 'RequestAttributeImpl' and REQUEST_ID = {0};".format(
                requestIDMXPress
            )
        )
        listAttribute = cursor.fetchall()
        for attributeTuple in listAttribute:
            if attributeTuple[4] == "directory":
                directory = attributeTuple[5]
                if beamline in directory:
                    listMXPress2.append(mxPress)

    for mxPressE in listMXPress2:
        requestIDMXPressE = mxPressE[0]
        # requestType = mxPressE[4]
        parentContextId = getContextId(cursor, requestIDMXPressE)
        # Attempt to use only data collections from successful ENDA characterisations
        #        actorNameCollect2 = "MXPressDataCollection.Only"
        #        listOnly = getListActorExecutionTime(cursor, actorNameCollect2, parentContextId)
        #        if len(listOnly) > 0:
        actorNameMesh2DBestPosition = "ExecuteMesh2D.Find"
        crystalSizeX, crystalSizeY = get2DSampleSize(
            cursor, actorNameMesh2DBestPosition, parentContextId
        )
        if (
            crystalSizeX is not None
            and crystalSizeY is not None
            and crystalSizeX != "null"
            and crystalSizeY != "null"
        ):
            listCrystalSizeX.append(float(crystalSizeX))
            listCrystalSizeY.append(float(crystalSizeY))
            actorNameMesh1DBestPosition = "ExecuteVerticalLineScan.ExecuteMesh1D.Find"
            crystalSizeZ = get1DSampleSize(
                cursor, actorNameMesh1DBestPosition, parentContextId
            )
            if crystalSizeZ is not None:
                listCrystalSizeZ.append(float(crystalSizeZ))
                volume = float(crystalSizeX) * float(crystalSizeY) * float(crystalSizeZ)
                listVolume.append(volume)
    dictResult = {}
    dictResult["startDateTime"] = datetime1
    dictResult["endDateTime"] = datetime2
    dictResult["noMXPress"] = len(listMXPress2)
    dictResult["noVolumes"] = len(listVolume)
    dictResult["maxCrystalSizeX"] = (
        numpy.amax(listCrystalSizeX) if len(listCrystalSizeX) > 0 else None
    )
    dictResult["minCrystalSizeX"] = (
        numpy.amin(listCrystalSizeX) if len(listCrystalSizeX) > 0 else None
    )
    dictResult["averageCrystalSizeX"] = (
        numpy.average(listCrystalSizeX) if len(listCrystalSizeX) > 0 else None
    )
    dictResult["modeCrystalSizeX"] = (
        scipy.stats.mode(listCrystalSizeX)[0][0] if len(listCrystalSizeX) > 0 else None
    )
    dictResult["maxCrystalSizeY"] = (
        numpy.amax(listCrystalSizeY) if len(listCrystalSizeY) > 0 else None
    )
    dictResult["minCrystalSizeY"] = (
        numpy.amin(listCrystalSizeY) if len(listCrystalSizeY) > 0 else None
    )
    dictResult["averageCrystalSizeY"] = (
        numpy.average(listCrystalSizeY) if len(listCrystalSizeY) > 0 else None
    )
    dictResult["modeCrystalSizeY"] = (
        scipy.stats.mode(listCrystalSizeY)[0][0] if len(listCrystalSizeY) > 0 else None
    )
    dictResult["maxCrystalSizeZ"] = (
        numpy.amax(listCrystalSizeZ) if len(listCrystalSizeZ) > 0 else None
    )
    dictResult["minCrystalSizeZ"] = (
        numpy.amin(listCrystalSizeZ) if len(listCrystalSizeZ) > 0 else None
    )
    dictResult["averageCrystalSizeZ"] = (
        numpy.average(listCrystalSizeZ) if len(listCrystalSizeZ) > 0 else None
    )
    dictResult["modeCrystalSizeZ"] = (
        scipy.stats.mode(listCrystalSizeZ)[0][0] if len(listCrystalSizeZ) > 0 else None
    )
    dictResult["maxVolume"] = numpy.amax(listVolume) if len(listVolume) > 0 else None
    dictResult["minVolume"] = numpy.amin(listVolume) if len(listVolume) > 0 else None
    dictResult["modeVolume"] = (
        scipy.stats.mode(listVolume)[0][0] if len(listVolume) > 0 else None
    )
    dictResult["averageVolume"] = (
        numpy.average(listVolume) if len(listVolume) > 0 else None
    )
    # For the moment not using the z dimension for min crystal length
    #    dictResult["minCrysatlLength"] = numpy.amin([dictResult["minCrystalSizeX"], dictResult["minCrystalSizeY"], dictResult["minCrystalSizeZ"]])
    dictResult["minCrysatlLength"] = numpy.amin(
        [dictResult["minCrystalSizeX"], dictResult["minCrystalSizeY"]]
    )
    dictResult["maxCrysatlLength"] = numpy.amax(
        [
            dictResult["maxCrystalSizeX"],
            dictResult["maxCrystalSizeY"],
            dictResult["maxCrystalSizeZ"],
        ]
    )
    dictResult["olderThanOneWeek"] = olderThanOneWeek
    return dictResult


def getListOfCaseID(externalRef):
    cursor = getCursor()
    cursor.execute(
        "SELECT * FROM pas_case where EXTERNAL_REF = '{0}';".format(externalRef)
    )
    listSelect = cursor.fetchall()
    listCaseID = [select[0] for select in listSelect]
    return listCaseID


def getTotalNumberOfMXPressPerCaseID(caseID):
    cursor = getCursor()
    cursor.execute(
        "SELECT * FROM pas_request where DTYPE = 'MAINREQUEST' and TYPE like '%MXPress%' and CASE_ID = '{0}';".format(
            caseID
        )
    )
    listAllMXPress = cursor.fetchall()
    listDict = []
    # Return only finished workflows
    for mxPress in listAllMXPress:
        bes_request_id = mxPress[0]
        parentContextId = getContextId(cursor, bes_request_id)
        executionTime = getExecutionTime(cursor, bes_request_id)
        if executionTime is not None:
            dictMXPress = {}
            dictMXPress["parentContextId"] = parentContextId
            dictMXPress["mainRequest"] = mxPress
            dictMXPress["executionTime"] = executionTime
            # Number of crystals
            actorNameIntepretation = "MXPressMultiCrystal.Interpretation"
            cursor.execute(
                "SELECT * FROM pas_request where INITIATOR like '%{0}%' AND PARENT_CONTEXT_ID = {1};".format(
                    actorNameIntepretation, parentContextId
                )
            )
            listActor = cursor.fetchall()
            for actor in listActor:
                taskID = actor[0]
                cursor.execute(
                    "SELECT * FROM pas_resultblock where TASK_ID = '{0}';".format(
                        taskID
                    )
                )
                listResultBlock = cursor.fetchall()
                for resultBlock in listResultBlock:
                    blockID = resultBlock[0]
                    cursor.execute(
                        "SELECT * FROM pas_resultitem where RESULTBLOCK_ID = '{0}';".format(
                            blockID
                        )
                    )
                    listResultItem = cursor.fetchall()
                    for resultItem in listResultItem:
                        if "numberOfPositions" in resultItem:
                            dictMXPress["numberOfPositions"] = resultItem[5]
            # Grid info
            actorNameAutoMesh = "PrepareAutoMesh.Auto"
            cursor.execute(
                "SELECT * FROM pas_request where INITIATOR like '%{0}%' AND PARENT_CONTEXT_ID = {1};".format(
                    actorNameAutoMesh, parentContextId
                )
            )
            listActor = cursor.fetchall()
            for actor in listActor:
                taskID = actor[0]
                cursor.execute(
                    "SELECT * FROM pas_resultblock where TASK_ID = '{0}';".format(
                        taskID
                    )
                )
                listResultBlock = cursor.fetchall()
                for resultBlock in listResultBlock:
                    blockID = resultBlock[0]
                    cursor.execute(
                        "SELECT * FROM pas_resultitem where RESULTBLOCK_ID = '{0}';".format(
                            blockID
                        )
                    )
                    listResultItem = cursor.fetchall()
                    for resultItem in listResultItem:
                        if "grid_info" in resultItem:
                            dictMXPress["grid_info"] = resultItem[5]
            # Mesh2D execution time
            actorNameMesh2D = "ExecuteMesh2D.Collect"
            cursor.execute(
                "SELECT * FROM pas_request where INITIATOR like '%{0}%' AND PARENT_CONTEXT_ID = {1};".format(
                    actorNameMesh2D, parentContextId
                )
            )
            listActor = cursor.fetchall()
            mesh2Dactor = listActor[0]
            requestID = mesh2Dactor[0]
            executionTime = getExecutionTime(cursor, requestID)
            if executionTime is not None:
                dictMXPress["mesh2DExecutionTime"] = executionTime
            # Crystal size
            actorNameMesh2DBestPosition = "ExecuteMesh2D.Find"
            crystalSizeX, crystalSizeY = get2DSampleSize(
                cursor, actorNameMesh2DBestPosition, parentContextId
            )
            if (
                crystalSizeX is not None
                and crystalSizeY is not None
                and crystalSizeX != "null"
                and crystalSizeY != "null"
            ):
                actorNameMesh1DBestPosition = (
                    "ExecuteVerticalLineScan.ExecuteMesh1D.Find"
                )
                crystalSizeZ = get1DSampleSize(
                    cursor, actorNameMesh1DBestPosition, parentContextId
                )
                if crystalSizeZ is not None:
                    volume = (
                        float(crystalSizeX) * float(crystalSizeY) * float(crystalSizeZ)
                    )
                    dictMXPress["crystalSizeX"] = crystalSizeX
                    dictMXPress["crystalSizeY"] = crystalSizeY
                    dictMXPress["crystalSizeZ"] = crystalSizeZ
                    dictMXPress["volume"] = volume
            # Number of collects
            actorNameCollectLoad = "MXPressDataCollection.Load"
            cursor.execute(
                "SELECT * FROM pas_request where INITIATOR like '%{0}%' AND PARENT_CONTEXT_ID = {1};".format(
                    actorNameCollectLoad, parentContextId
                )
            )
            listActor = cursor.fetchall()
            dictMXPress["numberOfCollects"] = len(listActor)
            listDict.append(dictMXPress)
    return listDict


def getListOfAllActors(mainRequestId, cursor=None):
    cursor = getCursor(cursor)
    parentContextId = getContextId(cursor, mainRequestId)
    cursor.execute(
        "SELECT * FROM pas_request where PARENT_CONTEXT_ID = '{0}';".format(
            parentContextId
        )
    )
    listActor = cursor.fetchall()
    return listActor


def getDictOfAllActors(mainRequestId, cursor=None):
    cursor = getCursor(cursor)
    listActor = getListOfAllActors(mainRequestId, cursor)
    listDictActor = []
    for actor in listActor:
        dictActor = {}
        dictActor["id"] = actor[0]
        dictActor["fullName"] = actor[6].replace("%20", " ")
        dictActor["name"] = actor[6].split(".")[-1].replace("%20", " ")
        dictActor["executionTime"] = getExecutionTime(cursor, actor[0])
        dictActor["startTime"] = dateutil.parser.parse(actor[10])
        listDictActor.append(dictActor)
    return listDictActor


def findActors(listDictActor, actorName, allowEmpty=False):
    listActors = []
    for actor in listDictActor:
        if actor["name"] == actorName or actorName in actor["fullName"]:
            listActors.append(actor)
    if len(listActors) == 0 and not allowEmpty:
        raise BaseException("Cannot find actor '{0}'!".format(actorName))
    return listActors


def findActor(listDictActor, actorName, allowEmpty=False):
    listActors = findActors(listDictActor, actorName, allowEmpty=allowEmpty)
    if len(listActors) > 1:
        raise BaseException("More than one actor '{0}'!".format(actorName))
    elif len(listActors) == 0:
        return None
    else:
        return listActors[0]


def getResultValue(listDictActor, actorName, key, cursor=None, allowEmpty=False):
    value = None
    cursor = getCursor(cursor)
    actor = findActor(listDictActor, actorName, allowEmpty=allowEmpty)
    if actor is not None:
        dictAttribute = getDictResultAttribute(actor["id"], cursor)
        if key in dictAttribute:
            value = dictAttribute[key]
    return value


def getMXPressExecutionStatisticsForExcel(
    mainRequestId, getHeadings=False, cursor=None
):
    staticHeadings = [
        "Id",
        "Beamline",
        "Proposal",
        "Prefix",
        "Status",
        "Experiment kind",
        "Target aperture [um]",
        "No positions required",
        "WF start (date)",
        "WF start (time)",
        "WF start (seconds)",
        "WF duration [s]",
        "Auto mesh [s]",
        "Mesh duration [s]",
        "Grid no x positions",
        "Grid no y positions",
        "Grid size x [mm]",
        "Grid size y[mm]",
        "Aperture selected [um]",
    ]
    iterationHeadings = [
        "Vertical scans [s]",
        "Reference image data collect [s]",
        "Characterisation [s]",
        "Short Xray Centring",
        "Data collection [s]",
    ]
    maxIterations = 10
    totalHeadings = staticHeadings
    for index in range(maxIterations):
        for heading in iterationHeadings:
            totalHeadings.append("Sample position {0}: {1}".format(index + 1, heading))
    if getHeadings:
        return totalHeadings
    else:
        listActions = []
        cursor = getCursor(cursor)
        listDictActor = getDictOfAllActors(mainRequestId, cursor)
        dictRequestAttribute = getDictRequestAttribute(mainRequestId, cursor)
        cursor.execute(
            "SELECT * FROM pas_context where REQUEST_ID = '{0}';".format(mainRequestId)
        )
        listContext = cursor.fetchall()[0]
        startTime = dateutil.parser.parse(listContext[3])
        status = listContext[5]
        for heading in staticHeadings:
            if heading == "Id":
                listActions.append(mainRequestId)
            elif heading == "Proposal":
                listDirectory = dictRequestAttribute["directory"].split("/")
                #                print(listDirectory)
                if listDirectory[1] == "data" and listDirectory[2] == "visitor":
                    proposal = listDirectory[3]
                elif listDirectory[1] == "data" and listDirectory[3] == "inhouse":
                    proposal = listDirectory[4]
                else:
                    proposal = None
                listActions.append(proposal)
            elif heading == "Beamline":
                beamline = getResultValue(
                    listDictActor,
                    "Beamline setup",
                    "beamline",
                    cursor=cursor,
                    allowEmpty=True,
                )
                listActions.append(beamline)
            elif heading == "Prefix":
                listActions.append(dictRequestAttribute["prefix"])
            elif heading == "Status":
                listActions.append(status)
            elif heading == "Experiment kind":
                strSampleInfo = getResultValue(
                    listDictActor,
                    "Init workflow",
                    "sampleInfo",
                    cursor=cursor,
                    allowEmpty=True,
                )
                try:
                    sampleInfo = json.loads(strSampleInfo)
                    if "experimentKind" in sampleInfo:
                        experimentKind = sampleInfo["experimentKind"]
                    else:
                        experimentKind = ""
                except Exception:
                    experimentKind = ""
                listActions.append(experimentKind)
            elif heading == "Target aperture [um]":
                targetApertureName = getResultValue(
                    listDictActor,
                    "Interpretation of diffraction plan",
                    "targetApertureName",
                    cursor=cursor,
                    allowEmpty=True,
                )
                if targetApertureName is None:
                    targetApertureSize = ""
                else:
                    targetApertureSize = str(
                        aperture_utils.aperture_name_to_size(
                            beamline, targetApertureName
                        )
                    )
                listActions.append(targetApertureSize)
            elif heading == "No positions required":
                noPositions = getResultValue(
                    listDictActor,
                    "Interpretation of diffraction plan",
                    "numberOfPositions",
                    cursor,
                    allowEmpty=True,
                )
                listActions.append(noPositions)
            elif heading == "Auto mesh [s]":
                actorAutoMesh = findActor(listDictActor, "Auto Mesh", allowEmpty=True)
                if actorAutoMesh is None:
                    listActions.append("NA")
                else:
                    bes_request_id = actorAutoMesh["id"]
                    executionTime = getExecutionTime(cursor, bes_request_id)
                    if executionTime is None:
                        listActions.append("NA")
                    else:
                        listActions.append(int(executionTime + 0.5))
            elif heading == "WF start (date)":
                listActions.append(str(startTime.date()))
            elif heading == "WF start (time)":
                listActions.append(startTime.strftime("%H:%M:%S"))
            elif heading == "WF start (seconds)":
                hour = startTime.time().hour
                minute = startTime.time().minute
                second = startTime.time().second
                wfStartInSeconds = second + 60 * (minute + 60 * hour)
                listActions.append(wfStartInSeconds)
            if status == "FINISHED":
                endTime = dateutil.parser.parse(listContext[4])
                gridInfo = json.loads(
                    getResultValue(listDictActor, "Auto Mesh", "grid_info", cursor)
                )
                if heading == "WF duration [s]":
                    deltaTime = endTime - startTime
                    listActions.append(deltaTime.seconds)
                elif heading == "Mesh duration [s]":
                    actor2DMesh = findActor(
                        listDictActor, "ExecuteMesh2D.Collect and process data"
                    )
                    bes_request_id = actor2DMesh["id"]
                    executionTime = getExecutionTime(cursor, bes_request_id)
                    listActions.append(int(executionTime + 0.5))
                elif heading == "Grid no x positions":
                    listActions.append(gridInfo["steps_x"])
                elif heading == "Grid no y positions":
                    listActions.append(gridInfo["steps_y"])
                elif heading == "Grid size x [mm]":
                    listActions.append(round(gridInfo["dx_mm"], 3))
                elif heading == "Grid size y[mm]":
                    listActions.append(round(gridInfo["dy_mm"], 3))
                elif heading == "Aperture selected [um]":
                    try:
                        apertureSelected = getResultValue(
                            listDictActor,
                            "Select aperture",
                            "targetApertureName",
                            cursor,
                            allowEmpty=True,
                        )
                    except Exception:
                        apertureSelected = None
                    else:
                        apertureSelected = str(
                            aperture_utils.aperture_name_to_size(
                                beamline, apertureSelected
                            )
                        )
                    listActions.append(apertureSelected)
        # Second part - iteration over data collections
        if status == "FINISHED":
            # Create list of sub workflows
            listSubWorkflow = []
            inSubSample = False
            for actor in listDictActor:
                #                print(actor["fullName"])
                if not inSubSample and actor["name"] == "Check if 2D mesh only":
                    subWorkflow = []
                    inSubSample = True
                elif inSubSample:
                    subWorkflow.append(actor)
                    if actor["name"] == "Check if more positions to collect":
                        listSubWorkflow.append(subWorkflow)
                        subWorkflow = []
            for index in range(len(listSubWorkflow)):
                subWorkflow = listSubWorkflow[index]
                for heading in iterationHeadings:
                    if "Vertical scans [s]" in heading:
                        try:
                            startActor = findActor(
                                subWorkflow, "Check if center rotation axis"
                            )
                            endActor = findActor(
                                subWorkflow,
                                "ExecuteVerticalLineScan.Move to new sample position",
                                allowEmpty=True,
                            )
                            if endActor is not None:
                                deltaTime = (
                                    endActor["startTime"] - startActor["startTime"]
                                )
                                listActions.append(deltaTime.seconds)
                            else:
                                listActions.append("NA")
                        except BaseException:
                            listActions.append("NA")
                    elif "Reference image data collect [s]" in heading:
                        totalTime = None
                        listActors = findActors(
                            subWorkflow, "Start collect using queue", allowEmpty=True
                        )
                        for actor in listActors:
                            if totalTime is None:
                                totalTime = 0.0
                            totalTime += actor["executionTime"]
                        if totalTime is None:
                            listActions.append("NA")
                        else:
                            listActions.append(int(totalTime + 0.5))
                    elif "Characterisation [s]" in heading:
                        totalTime = None
                        listActors = findActors(
                            subWorkflow, "EDNA MXv1 characterisation", allowEmpty=True
                        )
                        for actor in listActors:
                            if totalTime is None:
                                totalTime = 0.0
                            totalTime += actor["executionTime"]
                        if totalTime is None:
                            listActions.append("NA")
                        else:
                            listActions.append(int(totalTime + 0.5))
                    elif "Short Xray Centring" in heading:
                        listActors = findActors(
                            subWorkflow, "ShortXrayCentringVertical", allowEmpty=True
                        )
                        if len(listActors) == 0:
                            value = "False"
                        else:
                            value = "True"
                        listActions.append(value)
                    elif "Data collection [s]" in heading:
                        dataCollectActor = findActor(
                            subWorkflow, "Load mxCuBE queue", allowEmpty=True
                        )
                        if dataCollectActor is None:
                            listActions.append("NA")
                        else:
                            listActions.append(
                                int(dataCollectActor["executionTime"] + 0.5)
                            )
        #                        print("Data collection: {0}".format(dataCollectActor["executionTime"]))
        return listActions


def createMXPressCSVFile(
    proposal, startDate, endDate, fileName=None, beamline="all", maxResults=None
):
    cursor = getCursor()
    listCsv = []
    listCsv.append(getMXPressExecutionStatisticsForExcel(1, getHeadings=True))
    if proposal != "all":
        cursor.execute(
            "SELECT * FROM pas_case where EXTERNAL_REF = '{0}';".format(proposal)
        )
        listSelect = cursor.fetchall()
        listCaseID = [select[0] for select in listSelect]
        print(listCaseID)
        for caseId in listCaseID:
            sql = "SELECT * FROM pas_request where DTYPE = 'MAINREQUEST' and TYPE like '%MXPress%' and CASE_ID = '{caseId}';".format(
                caseId=caseId
            )
            cursor.execute(sql)
            listMXPress = cursor.fetchall()
            if maxResults is None:
                maxResults = len(listMXPress)
            for mxPress in listMXPress[0 : maxResults + 1]:
                startTime = dateutil.parser.parse(mxPress[10])
                if startTime > startDate and startTime < endDate:
                    listCsv.append(
                        getMXPressExecutionStatisticsForExcel(mxPress[0], cursor=cursor)
                    )
    else:
        # Take all proposals!
        sql = "SELECT * FROM pas_request where DTYPE = 'MAINREQUEST' and TYPE like '%MXPress%' and CREATION_TS > '{year1}-{month1}-{day1}T{hour1:02d}:{minute1:02d}:00.000' and CREATION_TS < '{year2}-{month2}-{day2}T{hour2:02d}:{minute2:02d}:00.000';".format(
            year1=startDate.year,
            month1=startDate.month,
            day1=startDate.day,
            hour1=startDate.hour,
            minute1=startDate.minute,
            year2=endDate.year,
            month2=endDate.month,
            day2=endDate.day,
            hour2=endDate.hour,
            minute2=endDate.minute,
        )
        cursor.execute(sql)
        listMXPress = cursor.fetchall()
        if maxResults is None:
            maxResults = len(listMXPress)
        for mxPress in listMXPress[0 : maxResults + 1]:
            if beamline == "all" or beamline == mxPress[6]:
                lineCsv = getMXPressExecutionStatisticsForExcel(
                    mxPress[0], cursor=cursor
                )
                print(lineCsv)
                listCsv.append(lineCsv)

    if fileName is not None:
        with open(fileName, "w") as f:
            for lineCsv in listCsv:
                for value in lineCsv[0:-1]:
                    f.write("{0}, ".format(value))
                f.write("{0}\n".format(lineCsv[-1]))


def getExperimentTypeSelected(mainRequestId, cursor):
    listDictActor = getDictOfAllActors(mainRequestId, cursor)
    print(listDictActor)
    sampleInfo = getResultValue(listDictActor, "Init workflow", "sampleInfo", cursor)
    print(sampleInfo)


def createExperimentTypeSelectedCsvFile(
    proposal, startDate, endDate, fileName=None, beamline="all"
):
    cursor = getCursor()
    if proposal != "all":
        cursor.execute(
            "SELECT * FROM pas_case where EXTERNAL_REF = '{0}';".format(proposal)
        )
        listSelect = cursor.fetchall()
        listCaseID = [select[0] for select in listSelect]
        print(listCaseID)
        for caseId in listCaseID:
            sql = "SELECT * FROM pas_request where DTYPE = 'MAINREQUEST' and TYPE like '%MXPress%' and CASE_ID = '{caseId}';".format(
                caseId=caseId
            )
            cursor.execute(sql)
            listMXPress = cursor.fetchall()
            for mxPress in listMXPress:
                startTime = dateutil.parser.parse(mxPress[10])
                if startTime > startDate and startTime < endDate:
                    getExperimentTypeSelected(mxPress[0], cursor)
    else:
        # Take all proposals!
        sql = "SELECT * FROM pas_request where DTYPE = 'MAINREQUEST' and TYPE like '%MXPress%' and CREATION_TS > '{year1}-{month1}-{day1}T{hour1:02d}:{minute1:02d}:00.000' and CREATION_TS < '{year2}-{month2}-{day2}T{hour2:02d}:{minute2:02d}:00.000';".format(
            year1=startDate.year,
            month1=startDate.month,
            day1=startDate.day,
            hour1=startDate.hour,
            minute1=startDate.minute,
            year2=endDate.year,
            month2=endDate.month,
            day2=endDate.day,
            hour2=endDate.hour,
            minute2=endDate.minute,
        )
        cursor.execute(sql)
        listMXPress = cursor.fetchall()
        for mxPress in listMXPress:
            if beamline == "all" or beamline == mxPress[6]:
                getExperimentTypeSelected(mxPress[0], cursor)
