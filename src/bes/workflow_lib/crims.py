"""
Workflow library module for connecting to ISPyB
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013"
__updated__ = "2016-01-11"

import os
import urllib

try:
    from urllib2 import Request
    from urllib2 import urlopen
    from urllib import urlencode
except Exception:
    from urllib.request import Request
    from urllib.request import urlopen
    from urllib.parse import urlencode

import xml.etree.ElementTree

from collections import defaultdict

from bes.workflow_lib import workflow_logging


# From http://stackoverflow.com/questions/2148119/how-to-convert-an-xml-string-to-a-dictionary-in-python
def etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(etree_to_dict, children):
            for k, v in dc.items():
                dd[k].append(v)
        d = {t.tag: {k: v[0] if len(v) == 1 else v for k, v in dd.items()}}
    if t.attrib:
        d[t.tag].update(("@" + k, v) for k, v in t.attrib.items())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
                d[t.tag]["#text"] = text
        else:
            d[t.tag] = text
    return d


def xml2dict(xmlString):
    root = xml.etree.ElementTree.XML(xmlString)
    return etree_to_dict(root)


def getPostData(url, params):
    request = Request(url + "?" + urllib.urlencode(params))
    f = urlopen(request)
    data = f.read()
    f.close()
    return data


def postData(url, params):
    logger = workflow_logging.getLogger()
    logger.debug("CRIMS postData url: {0}".format(url))
    logger.debug("CRIMS postData params: {0}".format(params))
    data = urlencode(params)
    req = Request(url=url, data=data)
    content = urlopen(req).read()
    return content


def getBarcodeXtalInfos(url, barcode):
    params = {
        "option": "com_crimswebservices",
        "format": "raw",
        "task": "getBarcodeXtalInfos",
        "barcode": barcode,
        "action": "insitu",
    }
    xmlString = getPostData(url, params)
    xtalInfoDict = xml2dict(xmlString)
    return xtalInfoDict


def getImage(url, barcode, row, column, shelf):
    params = {
        "option": "com_crimswebservices",
        "format": "raw",
        "task": "getImage",
        "barcode": barcode,
        "inspection": "6",
        "row": row,
        "column": column,
        "shelf": shelf,
    }
    data = getPostData(url, params)
    return data


def getBarcodeFromPath(directory):
    listDirectory = directory.split(os.sep)
    # Check that we have a valid ESRF MX directory path
    if listDirectory[1] != "data" or listDirectory[6] != "RAW_DATA":
        raise BaseException("Wrong directory structure: {0}".format(listDirectory))
    barcode = listDirectory[7]
    return barcode


def sendCrystalDataCollectionIds(url, crystalUuid, dataCollectionId):
    params = {"crystal_uuid": crystalUuid, "datacollection_id": dataCollectionId}
    status = postData(url, params)
    return status
