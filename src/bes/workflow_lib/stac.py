"""
Workflow library module for connecting to the STAC server
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013"
__updated__ = "2013-11-17"

import PyTango

from bes import config
from bes.workflow_lib import workflow_logging


def getNewSamplePosition(
    beamline, kappaAngle1, phiAngle1, X, Y, Z, kappaAngle2, phiAngle2
):
    logger = workflow_logging.getLogger()
    logger.debug("In workflow_lib.stac.moveToSamplePosition")
    TCproxy = None
    logger.debug("beamline = %s" % beamline)
    strStacDevice = config.get_value(beamline, "Goniometer", "stacDevice")
    if strStacDevice is not None:
        logger.debug("STAC device: %s" % strStacDevice)
        TCproxy = PyTango.DeviceProxy(strStacDevice)
    # From minikappa.mac:
    # inverse to get normalised location vector
    # and then convert to motor poistions at zero datum
    # get the translation for the given datum
    motor_pos_string = "Kappa %f Phi %f X %f Y %f Z %f Kappa2 %f Phi2 %f" % (
        kappaAngle1,
        phiAngle1,
        X,
        Y,
        Z,
        kappaAngle2,
        phiAngle2,
    )
    logger.debug("Current positions: %s" % motor_pos_string)
    new_motor_pos = {}
    new_motor_pos["sampx"] = X
    new_motor_pos["sampy"] = Y
    new_motor_pos["phiy"] = Z
    if TCproxy is None:
        logger.debug("No stac device configured.")
    else:
        try:
            correctionResult = TCproxy.TranslationCorrectionString(motor_pos_string)
            logger.debug("Actual datum translation: %s", correctionResult)
            # Create motor pos dictionary
            listCorrectionResult = correctionResult.split(" ")
            #        new_motor_pos["kappa"] = kappa
            #        new_motor_pos["kappa_phi"] = kappa_phi
            new_motor_pos["sampx"] = -round(float(listCorrectionResult[0]), 3)
            new_motor_pos["sampy"] = round(float(listCorrectionResult[1]), 3)
            new_motor_pos["phiy"] = round(float(listCorrectionResult[2]), 3)
            logger.debug("New motor positions: %r" % new_motor_pos)
        except Exception as e:
            logger.error("Error when trying to calculate new sample position: %r" % e)
    return new_motor_pos


def getSamplePositionAtZeroDatum(beamline, X, Y, Z, kappa, kappa_phi):
    logger = workflow_logging.getLogger()
    logger.debug("In workflow_lib.stac.getSamplePositionAtZeroDatum")
    TCproxy = None
    logger.debug("beamline = %s" % beamline)
    strStacDevice = config.get_value(beamline, "Goniometer", "stacDevice")
    if strStacDevice is not None:
        logger.debug("STAC device: %s" % strStacDevice)
        TCproxy = PyTango.DeviceProxy(strStacDevice)
    sx = -X
    sy = Y
    sz = Z
    # get centered orientation
    sk = kappa
    sp = kappa_phi
    # get the translation for the given datum
    motor_pos_string = "Kappa %f Phi %f X %f Y %f Z %f Kappa2 %f Phi2 %f" % (
        sk,
        sp,
        sx,
        sy,
        sz,
        0,
        0,
    )
    logger.debug("Current positions: %s" % motor_pos_string)
    new_motor_pos = {}
    new_motor_pos["sampx"] = sx
    new_motor_pos["sampy"] = sy
    new_motor_pos["phiy"] = sz
    if TCproxy is None:
        logger.debug("No stac device configured.")
    else:
        try:
            correctionResult = TCproxy.TranslationCorrectionString(motor_pos_string)
            logger.debug("Actual datum translation: %s", correctionResult)
            # Create motor pos dictionary
            listCorrectionResult = correctionResult.split(" ")
            #        new_motor_pos["kappa"] = kappa
            #        new_motor_pos["kappa_phi"] = kappa_phi
            new_motor_pos["sampx"] = -round(float(listCorrectionResult[0]), 3)
            new_motor_pos["sampy"] = round(float(listCorrectionResult[1]), 3)
            new_motor_pos["phiy"] = round(float(listCorrectionResult[2]), 3)
            logger.debug("New motor positions: %r" % new_motor_pos)
        except Exception as e:
            logger.error("Error when trying to calculate new sample position: %r" % e)
    return new_motor_pos
