"""
Workflow library module for kappa re-orientation
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013"
__updated__ = "2013-11-17"

import numpy as np

from bes import config
from bes.workflow_lib import workflow_logging


def isIncompatibleKappaStrategy(beamline, kappaStrategyOption, spaceGroupName):
    logger = workflow_logging.getLogger()
    bValue = False
    if kappaStrategyOption.lower() == "anomalous":
        if spaceGroupName == "P3":
            logger.warning(
                "The kappa strategy option anomalous is incompatible with the space group %s!"
                % spaceGroupName
            )
            bValue = True
    return bValue


def rotation_invariant(v):
    return np.outer(v, v)


def skew_symmetric(v):
    l, m, n = v
    return np.array([[0, -n, m], [n, 0, -l], [-m, l, 0]])


def inverse_skew_symmetric(v):
    l, m, n = v
    return np.array([[0, n, -m], [-n, 0, l], [m, -l, 0]])


def rotation_symmetric(v):
    return np.identity(3) - np.outer(v, v)


def rotation_matrix(axis, theta):
    return (
        rotation_invariant(axis)
        + skew_symmetric(axis) * np.sin(theta)
        + rotation_symmetric(axis) * np.cos(theta)
    )


def alignVectorWithOmegaAxis(beamline, target_vector):
    dictResult = {}
    logger = workflow_logging.getLogger()

    phiRot = np.array(config.get_value(beamline, "Kappa", "PhiRot"))
    kappaRot = np.array(config.get_value(beamline, "Kappa", "KappaRot"))
    omegaRot = np.array(config.get_value(beamline, "Kappa", "OmegaRot"))
    # Target rotation map
    e = np.array(target_vector)
    norm = np.sqrt(np.dot(e, e))
    # Check length of e:
    if norm < 0.05:
        logger.debug("Norm too short: %f" % norm)
        dictResult = {
            "kappa": 0.0,
            "phi": 0.0,
            "status": "tooShortVector",
            "message_text": "Too short vector! Please choose two centred positions further apart.",
        }
    else:
        e = e / norm
        if e[2] < 0:
            e = -e
        e_theta = np.arccos(e[2])
        e_theta_degrees = e_theta * 180.0 / np.pi
        logger.debug("Angle of target vector: %.1f degrees" % e_theta_degrees)
        # Check if angle > 2 times kappa angle!
        # TODO: replace hardcoded kappa angle
        if e_theta_degrees > 48.0:
            logger.debug("Angle too large")
            dictResult = {
                "kappa": 0.0,
                "phi": 0.0,
                "status": "tooLargeAngle",
                "message_text": "Angle of target vector too large: %.1f degrees, max angle allowed is 48 degrees with respect to rotation axis"
                % e_theta_degrees,
            }
        else:
            e_rot_vector = np.array([e[1], -e[0], 0])
            e_rot_vector = e_rot_vector / np.sqrt(np.dot(e_rot_vector, e_rot_vector))
            e_rotation_matrix = rotation_matrix(e_rot_vector, e_theta)
            logger.debug(
                "Result of rotating target vector: %r" % np.dot(e_rotation_matrix, e)
            )
            # Calculate kappa angle
            sin_coeff = np.dot(omegaRot, np.dot(skew_symmetric(kappaRot), phiRot))
            cos_coeff = np.dot(omegaRot, np.dot(rotation_symmetric(kappaRot), phiRot))
            norm = np.sqrt(sin_coeff**2 + cos_coeff**2)
            zero_angle = np.arctan(sin_coeff / cos_coeff)
            arccos = np.arccos(
                (
                    np.dot(omegaRot, np.dot(e_rotation_matrix, phiRot))
                    - np.dot(omegaRot, np.dot(rotation_invariant(kappaRot), phiRot))
                )
                / norm
            )
            kappaAngle = zero_angle + arccos
            if kappaAngle < 0:
                kappaAngle = zero_angle - arccos
            logger.info(
                "Kappa angle calculated to be %.1f degrees"
                % (kappaAngle * 180.0 / np.pi)
            )
            # Omega angle
            kappaRotMat = rotation_matrix(kappaRot, kappaAngle)
            kappaRotMatBack = rotation_matrix(kappaRot, -kappaAngle)
            t1 = np.dot(e_rotation_matrix, phiRot)
            t2 = np.dot(inverse_skew_symmetric(omegaRot), t1)
            t3 = np.dot(rotation_symmetric(omegaRot), t1)
            t4 = np.dot(kappaRotMatBack, t2)
            t5 = np.dot(kappaRotMatBack, t3)
            t6 = np.dot(phiRot, t4)
            t7 = np.dot(phiRot, t5)
            omegaAngle = np.arctan2(t6, t7)
            logger.debug(
                "Omega angle calculated to be %.1f degrees" % (omegaAngle * 180 / np.pi)
            )
            # Phi angle
            # omegaRotMat = rotation_matrix(omegaRot, omegaAngle)
            omegaRotMatBack = rotation_matrix(omegaRot, -omegaAngle)
            n1 = np.dot(omegaRotMatBack, e_rotation_matrix)
            phiRotMat = np.dot(kappaRotMatBack, n1)
            n2 = np.array(skew_symmetric(phiRot) * phiRotMat).sum()
            n3 = np.array(rotation_symmetric(phiRot) * phiRotMat).sum()
            phiAngle = np.arctan2(n2, n3)
            if phiAngle < 0:
                phiAngle += 2 * np.pi
            logger.info(
                "Phi angle calculated to be %.1f degrees" % (phiAngle * 180 / np.pi)
            )
            phiRotMat = rotation_matrix(phiRot, phiAngle)
            logger.debug(
                "Result of rotating target vector with kappa and phi: %r"
                % np.dot(kappaRotMat, np.dot(phiRotMat, e))
            )
            dictResult = {
                "kappa": float(kappaAngle * 180 / np.pi),
                "phi": float(phiAngle * 180 / np.pi),
                "status": "ok",
                "message_text": "",
            }
    return dictResult


def alternativeAlignVectorWithOmegaAxis(beamline, target_vector):
    dictResult = {}
    logger = workflow_logging.getLogger()

    phiRot = np.array(config.get_value(beamline, "Kappa", "PhiRot"))
    kappaRot = np.array(config.get_value(beamline, "Kappa", "KappaRot"))
    omegaRot = np.array(config.get_value(beamline, "Kappa", "OmegaRot"))
    # Target rotation map
    e = np.array(target_vector)
    norm = np.sqrt(np.dot(e, e))
    # Check length of e:
    if norm < 0.05:
        logger.error("Too short vector!")
        dictResult = {"kappa": 0.0, "phi": 0.0, "status": "tooShortVector"}
    else:
        e = e / norm
        if e[2] < 0:
            e = -e
        e_theta = np.arccos(e[2])
        e_theta_degrees = e_theta * 180.0 / np.pi
        logger.debug("Angle of target vector: %.1f degrees" % e_theta_degrees)
        # Check if angle > 2 times kappa angle!
        # TODO: replace hardcoded kappa angle
        if e_theta_degrees > 48.0:
            logger.error(
                "Angle of target vector too large: %.1f degrees, max angle is 48 degrees with respect to rotation axis"
                % e_theta_degrees
            )
            dictResult = {"kappa": 0.0, "phi": 0.0, "status": "tooLargeAngle"}
        else:
            e_rot_vector = np.array([e[1], -e[0], 0])
            e_rot_vector = e_rot_vector / np.sqrt(np.dot(e_rot_vector, e_rot_vector))
            e_rotation_matrix = rotation_matrix(e_rot_vector, e_theta)
            logger.debug(
                "Result of rotating target vector: %r" % np.dot(e_rotation_matrix, e)
            )
            # Check with known angles:
            #            kappaAngle = 87.29 * np.pi / 180.0
            #            kappaRotMat = rotation_matrix(kappaRot, kappaAngle)
            #            phiAngle = 298.62 *  np.pi / 180.0
            #            phiRotMat = rotation_matrix(phiRot, phiAngle)
            #            print e
            #            print "kappa-phi-e", np.dot(kappaRotMat, np.dot(phiRotMat, e))
            left_term = np.dot(kappaRot, omegaRot)
            sin_coeff = np.dot(kappaRot, np.dot(skew_symmetric(phiRot), e))
            cos_coeff = np.dot(kappaRot, np.dot(rotation_symmetric(phiRot), e))
            first_term = np.dot(kappaRot, np.dot(rotation_invariant(phiRot), e))
            #            re = first_term + sin_coeff * np.sin(phiAngle) + cos_coeff * np.cos(phiAngle)
            #            print "Right expanded:", re
            # Calculate phi angle
            norm = np.sqrt(sin_coeff**2 + cos_coeff**2)
            zero_angle = np.arctan2(sin_coeff, cos_coeff)
            arccos = np.arccos((left_term - first_term) / norm)
            phiAngle1 = zero_angle + arccos
            if phiAngle1 < 0:
                phiAngle1 += np.pi * 2
            phiAngle2 = zero_angle - arccos
            if phiAngle2 < 0:
                phiAngle2 += np.pi * 2
            logger.debug(
                "Phi angles: %r" % (np.array([phiAngle1, phiAngle2]) * 180.0 / np.pi)
            )
            # Kappa angle
            for phiAngle in [phiAngle1, phiAngle2]:
                phiRotMat = rotation_matrix(phiRot, phiAngle)
                phiRotE = np.dot(phiRotMat, e)
                #            kappaAngle = 87.29 * np.pi / 180.0
                #            kappaRotMat = rotation_matrix(kappaRot, kappaAngle)
                #            print np.dot(kappaRotMat, phiRotE)
                #            print np.dot(e, omegaRot)
                #            print np.dot(e, np.dot(kappaRotMat, phiRotE))
                left_term = np.dot(e, omegaRot)
                sin_coeff = np.dot(e, np.dot(skew_symmetric(kappaRot), phiRotE))
                cos_coeff = np.dot(e, np.dot(rotation_symmetric(kappaRot), phiRotE))
                first_term = np.dot(e, np.dot(rotation_invariant(kappaRot), phiRotE))
                #            print "first_term", first_term
                norm = np.sqrt(sin_coeff**2 + cos_coeff**2)
                #            print "norm", norm
                zero_angle = np.arctan2(sin_coeff, cos_coeff)
                #            print(left_term - first_term)/norm
                arccos = np.arccos((left_term - first_term) / norm)
                kappaAngle1 = zero_angle + arccos
                if kappaAngle1 < 0:
                    kappaAngle1 += np.pi * 2
                kappaAngle2 = zero_angle - arccos
                if kappaAngle2 < 0:
                    kappaAngle2 += np.pi * 2
                logger.debug(
                    "kappa angles: %r"
                    % (np.array([kappaAngle1, kappaAngle2]) * 180.0 / np.pi)
                )
                # Test kappa angles
                kappaRotMat1 = rotation_matrix(kappaRot, kappaAngle1)
                dist1 = np.dot(kappaRotMat1, phiRotE)
                kappaRotMat2 = rotation_matrix(kappaRot, kappaAngle2)
                dist2 = np.dot(kappaRotMat2, phiRotE)
                if dist1[2] > 0.999:
                    kappaAngle = kappaAngle1
                elif dist2[2] > 0.999:
                    kappaAngle = kappaAngle2
                else:
                    raise Exception("Wrong phi and kappa angles!!!")
                if kappaAngle < np.pi:
                    dictResult = {
                        "kappa": float(kappaAngle * 180 / np.pi),
                        "phi": float(phiAngle * 180 / np.pi),
                        "status": "ok",
                    }
                    print(dictResult)
    return dictResult


def getNewSamplePosition(
    beamline, kappaAngle1, phiAngle1, sampx, sampy, phiy, kappaAngle2, phiAngle2
):
    """
    This method calculates the translation correction for inversed kappa goniostats.
    For more info see Acta Cryst.(2011). A67, 219-228, Sandor Brockhauser et al., formula (3).
    See also MXSUP-1823.
    """
    logger = workflow_logging.getLogger()
    logger.debug("In workflow_lib.kappa_reorientation.getNewSamplePosition")
    logger.debug(
        "Input arguments: Kappa %.2f Phi %.2f sampx %.3f sampy %.3f phiy %.3f Kappa2 %.2f Phi2 %.2f"
        % (kappaAngle1, phiAngle1, sampx, sampy, phiy, kappaAngle2, phiAngle2)
    )
    kappaRot = np.array(config.get_value(beamline, "Kappa", "KappaTransD"))
    phiRot = np.array(config.get_value(beamline, "Kappa", "PhiTransD"))
    t_kappa_zero = np.array(config.get_value(beamline, "Kappa", "KappaTrans"))
    t_phi_zero = np.array(config.get_value(beamline, "Kappa", "PhiTrans"))
    if beamline in ["id23eh1", "id30a1", "id30a3", "id30b"]:
        t_start = np.array([-sampx, -sampy, -phiy])
    else:
        t_start = np.array([sampx, sampy, -phiy])
    kappaRotMat1 = rotation_matrix(kappaRot, -kappaAngle1 * np.pi / 180.0)
    kappaRotMat2 = rotation_matrix(kappaRot, kappaAngle2 * np.pi / 180.0)
    phiRotMat = rotation_matrix(phiRot, (phiAngle2 - phiAngle1) * np.pi / 180.0)
    t_step1 = t_kappa_zero - t_start
    t_step2 = t_kappa_zero - np.dot(kappaRotMat1, t_step1)
    t_step3 = t_phi_zero - t_step2
    t_step4 = t_phi_zero - np.dot(phiRotMat, t_step3)
    t_step5 = t_kappa_zero - t_step4
    t_end = t_kappa_zero - np.dot(kappaRotMat2, t_step5)
    new_motor_pos = {}
    new_motor_pos["sampx"] = float(-t_end[0])
    new_motor_pos["sampy"] = float(-t_end[1])
    new_motor_pos["phiy"] = float(-t_end[2])
    logger.debug("New motor positions: %r" % new_motor_pos)
    return new_motor_pos
