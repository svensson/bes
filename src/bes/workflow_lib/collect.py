#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "15/02/2021"

"""
Workflow library module for connecting to beamline control
"""

import time

import pprint
import os
import json
import importlib
import jsonpickle
import getpass
import sys
import matplotlib
import math
import tempfile
import shutil
import numpy
import copy
import subprocess
from typing import Optional, List

from xmlrpc.client import Transport
from xmlrpc.client import ServerProxy

from bes import config
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging

# Global variable for queue model objects
QUEUE_MODEL_OBJECTS = None


class SimulatorServerProxy:

    def __init__(self):
        self.resolution = 2.0
        self.positions = {
            "phi": 0.0,
            "phiy": 0.0,
            "phiz": 0.0,
            "sampx": 0.0,
            "sampy": 0.0,
        }
        self.backlight = 1.0
        self.apertures = [
            "15um, A15 um",
            "10um, A10 um",
            "7um, A7 um",
            "15um, A30 um",
            "15um, A50 um",
        ]
        self.aperture = self.apertures[0]

    def get_diffractometer_positions(self):
        return self.positions

    def move_diffractometer(self, motor_positions):
        for name, value in motor_positions.items():
            self.positions[name] = value

    def beamline_setup_read(self, name):
        if name == "/beamline/resolution":
            return self.resolution

    def setBacklightLevel(self, value):
        self.backlight = value

    def getBackLightLevel(self):
        return self.backlight

    def get_aperture(self):
        return self.aperture

    def set_aperture(self, aperture):
        if aperture in self.apertures:
            self.aperture = aperture

    def get_aperture_list(self):
        return self.apertures


simulated_server_proxy = SimulatorServerProxy()


class TokenTransport(Transport):
    def __init__(self, token, use_datetime=0):
        Transport.__init__(self, use_datetime=use_datetime)
        self.token = token

    def send_content(self, connection, request_body):
        connection.putheader("Content-Type", "text/xml")
        connection.putheader("Content-Length", str(len(request_body)))
        connection.putheader("Token", self.token)
        connection.endheaders()
        if request_body:
            connection.send(request_body)


def createMeshQueueList(
    beamline,
    directory,
    process_directory,
    run_number,
    expTypePrefix,
    prefix,
    meshPositions,
    motorPositions,
    exposureTime,
    osc_range,
    transmission,
):
    """
    This method creates a list of data collections, one data collection per mesh/grid position
    """
    logger = workflow_logging.getLogger()
    # Loop over all the mesh positions
    allQueueEntries = []
    if isinstance(meshPositions, str):
        meshPositions = json.loads(meshPositions)
    for position in meshPositions:
        sampx = position["sampx"]
        sampy = position["sampy"]
        phiy = position["phiy"]
        phiz = position["phiz"]
        logger.debug("Sample Position: [%f ; %f ; %f ; %f]", sampx, sampy, phiy, phiz)
        newMotorPositions = {"sampx": sampx, "sampy": sampy, "phiy": phiy, "phiz": phiz}
        fileData = {}
        fileData["directory"] = directory
        fileData["process_directory"] = process_directory
        fileData["expTypePrefix"] = expTypePrefix
        fileData["prefix"] = prefix
        fileData["firstImage"] = position["index"] + 1
        fileData["runNumber"] = run_number
        subWedgeData = {}
        subWedgeData["exposureTime"] = exposureTime
        subWedgeData["noImages"] = 1
        subWedgeData["oscillationWidth"] = osc_range
        subWedgeData["runNumber"] = run_number
        subWedgeData["doProcessing"] = False
        subWedgeData["rotationAxisStart"] = motorPositions["phi"]
        subWedgeData["transmission"] = transmission
        queueEntry = {
            "fileData": fileData,
            "subWedgeData": subWedgeData,
            "newMotorPositions": newMotorPositions,
        }
        allQueueEntries.append(queueEntry)
    return allQueueEntries


def createHelicalQueueList(
    beamline,
    directory,
    process_directory,
    run_number,
    expTypePrefix,
    prefix,
    meshPositions,
    motorPositions,
    exposureTime,
    osc_range,
    transmission,
    grid_info,
    meshZigZag=False,
):
    """
    This method creates a list of helical data collections, one helical data collection per mesh/grid row
    """
    logger = workflow_logging.getLogger()
    logger.info("Preparing helical data collection")
    # Loop over all the mesh positions
    allQueueEntries = []
    if isinstance(meshPositions, str):
        meshPositions = json.loads(meshPositions)
    logger.info("Number of positions/images: %d", len(meshPositions))
    if isinstance(grid_info, str):
        grid_info = json.loads(grid_info)
    # Find star and end positions for each row:
    gridNx = grid_info["steps_x"]
    gridNy = grid_info["steps_y"]
    phi = motorPositions["phi"]
    if beamline in ["id23eh2"]:
        isHorizontalRotationAxis = False
    else:
        isHorizontalRotationAxis = True
    if isHorizontalRotationAxis and gridNx == 1:
        logger.info("Vertical 1D scan")
        motorPositions4dscan = {}
        for position in meshPositions:
            if position["indexZ"] == 0:
                _setStartPosition(position, motorPositions4dscan)
                firstImage = position["index"] + 1
            elif position["indexZ"] == gridNy - 1:
                _setEndPosition(position, motorPositions4dscan)
            if "sampx_s" in motorPositions4dscan and "sampx_e" in motorPositions4dscan:
                queueEntry = _createQueueEntry(
                    directory,
                    process_directory,
                    expTypePrefix,
                    prefix,
                    firstImage,
                    run_number,
                    exposureTime,
                    phi,
                    osc_range,
                    gridNy,
                    transmission,
                )
                queueEntry["motorPositions4dscan"] = motorPositions4dscan
                allQueueEntries.append(queueEntry)
    elif (not isHorizontalRotationAxis) and gridNy == 1:
        # Horizontal 1D scan
        motorPositions4dscan = {}
        for position in meshPositions:
            if position["indexY"] == 0:
                _setStartPosition(position, motorPositions4dscan)
                firstImage = position["index"] + 1
            elif position["indexY"] == gridNx - 1:
                _setEndPosition(position, motorPositions4dscan)
            if "sampx_s" in motorPositions4dscan and "sampx_e" in motorPositions4dscan:
                queueEntry = _createQueueEntry(
                    directory,
                    process_directory,
                    expTypePrefix,
                    prefix,
                    firstImage,
                    run_number,
                    exposureTime,
                    phi,
                    osc_range,
                    gridNx,
                    transmission,
                )
                queueEntry["motorPositions4dscan"] = motorPositions4dscan
                allQueueEntries.append(queueEntry)
    else:
        scan_direction_positive = True
        if isHorizontalRotationAxis:
            for indexZ in range(gridNy):
                motorPositions4dscan = {}
                for position in meshPositions:
                    if indexZ == position["indexZ"]:
                        if (scan_direction_positive and (position["indexY"] == 0)) or (
                            not scan_direction_positive
                            and (position["indexY"] == gridNx - 1)
                        ):
                            _setStartPosition(position, motorPositions4dscan)
                            firstImage = position["index"] + 1
                        elif (
                            scan_direction_positive
                            and (position["indexY"] == gridNx - 1)
                        ) or (
                            not scan_direction_positive and (position["indexY"] == 0)
                        ):
                            _setEndPosition(position, motorPositions4dscan)
                        if (
                            "sampx_s" in motorPositions4dscan
                            and "sampx_e" in motorPositions4dscan
                        ):
                            queueEntry = _createQueueEntry(
                                directory,
                                process_directory,
                                expTypePrefix,
                                prefix,
                                firstImage,
                                run_number,
                                exposureTime,
                                phi,
                                osc_range,
                                gridNx,
                                transmission,
                            )
                            queueEntry["motorPositions4dscan"] = motorPositions4dscan
                            allQueueEntries.append(queueEntry)
                if meshZigZag:
                    # Change direction, zig-zag
                    scan_direction_positive = not scan_direction_positive
        else:
            for indexY in range(gridNx):
                motorPositions4dscan = {}
                for position in meshPositions:
                    if indexY == position["indexY"]:
                        if (scan_direction_positive and (position["indexZ"] == 0)) or (
                            not scan_direction_positive
                            and (position["indexZ"] == gridNy - 1)
                        ):
                            _setStartPosition(position, motorPositions4dscan)
                            firstImage = position["index"] + 1
                        elif (
                            scan_direction_positive
                            and (position["indexZ"] == gridNy - 1)
                        ) or (
                            not scan_direction_positive and (position["indexZ"] == 0)
                        ):
                            _setEndPosition(position, motorPositions4dscan)
                        if (
                            "sampx_s" in motorPositions4dscan
                            and "sampx_e" in motorPositions4dscan
                        ):
                            queueEntry = _createQueueEntry(
                                directory,
                                process_directory,
                                expTypePrefix,
                                prefix,
                                firstImage,
                                run_number,
                                exposureTime,
                                phi,
                                osc_range,
                                gridNy,
                                transmission,
                            )
                            queueEntry["motorPositions4dscan"] = motorPositions4dscan
                            allQueueEntries.append(queueEntry)
                if meshZigZag:
                    # Change direction, zig-zag
                    scan_direction_positive = not scan_direction_positive
    logger.info("Number of queue entries: %d", len(allQueueEntries))
    logger.debug(pprint.pformat(allQueueEntries))
    return allQueueEntries


def _setStartPosition(position, motorPositions4dscan):
    motorPositions4dscan["sampx_s"] = position["sampx_mxcube"]
    motorPositions4dscan["sampy_s"] = position["sampy_mxcube"]
    motorPositions4dscan["phiy_s"] = position["phiy_mxcube"]
    motorPositions4dscan["phiz_s"] = position["phiz_mxcube"]


def _setEndPosition(position, motorPositions4dscan):
    motorPositions4dscan["sampx_e"] = position["sampx_mxcube"]
    motorPositions4dscan["sampy_e"] = position["sampy_mxcube"]
    motorPositions4dscan["phiy_e"] = position["phiy_mxcube"]
    motorPositions4dscan["phiz_e"] = position["phiz_mxcube"]


def _createQueueEntry(
    directory,
    process_directory,
    expTypePrefix,
    prefix,
    firstImage,
    run_number,
    exposureTime,
    phi,
    osc_range,
    gridN,
    transmission,
):
    fileData = {}
    fileData["directory"] = directory
    fileData["process_directory"] = process_directory
    fileData["expTypePrefix"] = expTypePrefix
    fileData["prefix"] = prefix
    fileData["firstImage"] = firstImage
    fileData["runNumber"] = run_number
    subWedgeData = {}
    subWedgeData["exposureTime"] = exposureTime
    subWedgeData["noImages"] = gridN
    subWedgeData["oscillationWidth"] = osc_range / float(gridN)
    subWedgeData["doProcessing"] = False
    subWedgeData["rotationAxisStart"] = phi - osc_range / 2.0
    subWedgeData["transmission"] = transmission
    queueEntry = {
        "fileData": fileData,
        "subWedgeData": subWedgeData,
    }
    return queueEntry


def createSerialDataCollectionQueueList(
    beamline,
    directory,
    process_directory,
    run_number,
    expTypePrefix,
    prefix,
    meshPositions,
    motorPositions,
    exposureTime,
    osc_range,
    transmission,
    grid_info,
    meshZigZag=False,
):
    """
    This method creates a lst of helical data collections, one helical data collection per mesh/grid row
    """
    logger = workflow_logging.getLogger()
    logger.info("Preparing multi-wedge data collection")
    # Loop over all the mesh positions
    allQueueEntries = []
    if isinstance(meshPositions, str):
        meshPositions = json.loads(meshPositions)
    logger.info("Number of positions/images: %d", len(meshPositions))
    if isinstance(grid_info, str):
        grid_info = json.loads(grid_info)
    # Find star and end positions for each row:
    gridNx = grid_info["steps_x"]
    gridNy = grid_info["steps_y"]
    if gridNx == 1:
        # Vertical scan!
        motorPositions4dscan = {}
        for position in meshPositions:
            if position["indexZ"] == 0:
                motorPositions4dscan["sampx_s"] = position["sampx_mxcube"]
                motorPositions4dscan["sampy_s"] = position["sampy_mxcube"]
                motorPositions4dscan["phiy_s"] = position["phiy_mxcube"]
                motorPositions4dscan["phiz_s"] = position["phiz_mxcube"]
                firstImage = position["index"] + 1
            elif position["indexZ"] == gridNy - 1:
                motorPositions4dscan["sampx_e"] = position["sampx_mxcube"]
                motorPositions4dscan["sampy_e"] = position["sampy_mxcube"]
                motorPositions4dscan["phiy_e"] = position["phiy_mxcube"]
                motorPositions4dscan["phiz_e"] = position["phiz_mxcube"]
            if "sampx_s" in motorPositions4dscan and "sampx_e" in motorPositions4dscan:
                fileData = {}
                fileData["directory"] = directory
                fileData["process_directory"] = process_directory
                fileData["expTypePrefix"] = expTypePrefix
                fileData["prefix"] = prefix
                fileData["firstImage"] = firstImage
                fileData["runNumber"] = run_number
                subWedgeData = {}
                subWedgeData["exposureTime"] = exposureTime
                subWedgeData["noImages"] = gridNy
                subWedgeData["oscillationWidth"] = osc_range / float(gridNy)
                subWedgeData["doProcessing"] = False
                subWedgeData["rotationAxisStart"] = (
                    motorPositions["phi"] - osc_range / 2.0
                )
                subWedgeData["transmission"] = transmission
                # Create a small shift if phiy_s and phiy_e are too close
                # in order to avoid identical position error
                # if beamline in ["id23eh1", "id29", "id30a3"]:
                #     logger.debug("Applying small shift to phiy start and end")
                #     if beamline in ["id23eh1", "id29"] and gridNy > 40:
                #         deltaPhiy = 0.01
                #     else:
                #         deltaPhiy = 0.005
                #     if abs(motorPositions4dscan["phiy_s"] - motorPositions4dscan["phiy_e"]) < 2 * deltaPhiy:
                #         motorPositions4dscan["phiy_s"] -= deltaPhiy
                #         motorPositions4dscan["phiy_e"] += deltaPhiy
                queueEntry = {
                    "fileData": fileData,
                    "subWedgeData": subWedgeData,
                    "motorPositions4dscan": motorPositions4dscan,
                }
                allQueueEntries.append(queueEntry)
    else:
        scan_direction_positive = True
        for indexZ in range(gridNy):
            motorPositions4dscan = {}
            for position in meshPositions:
                if indexZ == position["indexZ"]:
                    if (scan_direction_positive and (position["indexY"] == 0)) or (
                        not scan_direction_positive
                        and (position["indexY"] == gridNx - 1)
                    ):
                        motorPositions4dscan["sampx_s"] = position["sampx_mxcube"]
                        motorPositions4dscan["sampy_s"] = position["sampy_mxcube"]
                        motorPositions4dscan["phiy_s"] = position["phiy_mxcube"]
                        motorPositions4dscan["phiz_s"] = position["phiz_mxcube"]
                        firstImage = position["index"] + 1
                    elif (
                        scan_direction_positive and (position["indexY"] == gridNx - 1)
                    ) or (not scan_direction_positive and (position["indexY"] == 0)):
                        motorPositions4dscan["sampx_e"] = position["sampx_mxcube"]
                        motorPositions4dscan["sampy_e"] = position["sampy_mxcube"]
                        motorPositions4dscan["phiy_e"] = position["phiy_mxcube"]
                        motorPositions4dscan["phiz_e"] = position["phiz_mxcube"]
                    if (
                        "sampx_s" in motorPositions4dscan
                        and "sampx_e" in motorPositions4dscan
                    ):
                        fileData = {}
                        fileData["directory"] = directory
                        fileData["process_directory"] = process_directory
                        fileData["expTypePrefix"] = expTypePrefix
                        fileData["prefix"] = f"{prefix}_w{indexZ + 1:03d}"
                        fileData["firstImage"] = 1
                        fileData["runNumber"] = run_number
                        subWedgeData = {}
                        subWedgeData["exposureTime"] = exposureTime
                        subWedgeData["noImages"] = gridNx
                        subWedgeData["oscillationWidth"] = osc_range / float(gridNx)
                        subWedgeData["doProcessing"] = False
                        subWedgeData["rotationAxisStart"] = (
                            motorPositions["phi"] - osc_range / 2.0
                        )
                        subWedgeData["transmission"] = transmission
                        # Create a small shift if phiy_s and phiy_e are too close
                        # in order to avoid identical position error
                        # if beamline == "id23eh1":
                        #     if abs(motorPositions4dscan["phiy_s"] - motorPositions4dscan["phiy_e"]) < 0.02:
                        #         motorPositions4dscan["phiy_s"] -= 0.01
                        #         motorPositions4dscan["phiy_e"] += 0.01
                        # elif beamline in ["id29", "id30a3"]:
                        #     if abs(motorPositions4dscan["phiy_s"] - motorPositions4dscan["phiy_e"]) < 0.01:
                        #         motorPositions4dscan["phiy_s"] -= 0.005
                        #         motorPositions4dscan["phiy_e"] += 0.005
                        queueEntry = {
                            "fileData": fileData,
                            "subWedgeData": subWedgeData,
                            "motorPositions4dscan": motorPositions4dscan,
                        }
                        allQueueEntries.append(queueEntry)
            if meshZigZag:
                # Change direction, zig-zag
                scan_direction_positive = not scan_direction_positive
    logger.info("Number of queue entries: %d", len(allQueueEntries))
    logger.debug(pprint.pformat(allQueueEntries))
    return allQueueEntries


def getServerProxy(beamline, token=None):
    """
    This method returns a ServerProxy connected to mxCuBE
    """
    if beamline == "simulator":
        return simulated_server_proxy

    mxcube_host = config.get_value(beamline, "MXCuBE", "host", default_value=None)
    if mxcube_host is None:
        raise ValueError(
            "'host' must be provided in the [MXCuBE] section of the BES configuration"
        )
    mxcube_port = config.get_value(beamline, "MXCuBE", "port", default_value=7171)
    mxcube_uri = f"http://{mxcube_host}.esrf.fr:{mxcube_port}"

    logger = workflow_logging.getLogger()
    logger.debug("MXcube URI = '%s'", mxcube_uri)

    if token is None:
        return ServerProxy(mxcube_uri)
    return ServerProxy(mxcube_uri, transport=TokenTransport(token))


def _createQueueModelDataCollections(
    beamline,
    directory,
    process_directory,
    prefix,
    expTypePrefix,
    run_number,
    resolution,
    mxv1StrategyResult,
    motorPositions,
    dictCellSpaceGroup=None,
    energy=None,
    snapShots=False,
    firstImageNo=1,
    inverseBeam=False,
    doProcess=True,
    highResolution=None,
    lowResolution=None,
    fineSlicedReferenceImages=False,
    token=None,
    workflow_name=None,
    workflow_type=None,
    workflow_id=None,
    bes_request_id=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    group_by=None,
    sample_name=None,
):
    """
    This method returns list of mxCuBE data collections
    """
    logger = workflow_logging.getLogger()
    if dictCellSpaceGroup is None:
        dictCellSpaceGroup = {}
    xsDataResultStrategy = edna_mxv1.parseMxv1StrategyResult(mxv1StrategyResult)
    listQueueModelDataCollections = []
    if xsDataResultStrategy is not None:
        serverProxy = getServerProxy(beamline, token)
        if resolution is None:
            if serverProxy is not None:
                resolution = serverProxy.beamline_setup_read("/beamline/resolution")
            else:
                resolution = 0.0

        if serverProxy is not None and not beamline == "simulator":
            if energy is None:
                energy = serverProxy.beamline_setup_read("/beamline/energy")
            transmission = serverProxy.beamline_setup_read("/beamline/transmission")
        else:
            energy = 0.0
            transmission = 0.0
        # Create data collections
        maxDCct = 0
        # We loop over all the collection plans
        for xsDataCollectionPlan in xsDataResultStrategy.getCollectionPlan():
            xsDataCollectionStrategy = xsDataCollectionPlan.getCollectionStrategy()
            runNumberDataCollection = (
                run_number + xsDataCollectionPlan.collectionPlanNumber.value - 1
            )

            # Try to get the resolution
            if xsDataCollectionPlan.strategySummary is not None:
                xsDataStrategySummary = xsDataCollectionPlan.strategySummary
                if xsDataStrategySummary.resolution is not None:
                    resolution = xsDataStrategySummary.resolution.value
                    resolution = limitResolutiom(
                        resolution, lowResolution, highResolution
                    )
            # Then we loop over all the sub wedges
            iWedgeID = 0
            for xsDataSubWedge in xsDataCollectionStrategy.subWedge:
                iWedgeID += 1
                if len(xsDataCollectionStrategy.subWedge) > 1:
                    wedgeID = f"w{iWedgeID}"
                else:
                    wedgeID = ""
                maxDCct += 1
                logger.info("Wedge id: %s", wedgeID)
                xsDataExperimentalCondition = xsDataSubWedge.experimentalCondition
                exposureTime = xsDataExperimentalCondition.beam.exposureTime.value
                if xsDataExperimentalCondition.beam.transmission is None:
                    pass
                else:
                    transmission = xsDataExperimentalCondition.beam.transmission.value
                oscillationWidth = (
                    xsDataExperimentalCondition.goniostat.oscillationWidth.value
                )
                rotationAxisStart = (
                    xsDataExperimentalCondition.goniostat.rotationAxisStart.value
                )
                rotationAxisEnd = (
                    xsDataExperimentalCondition.goniostat.rotationAxisEnd.value
                )
                if xsDataExperimentalCondition.goniostat.overlap is None:
                    overlap = 0.0
                else:
                    overlap = xsDataExperimentalCondition.goniostat.overlap.value

                if oscillationWidth < 0.001:
                    noImages = int(rotationAxisEnd)
                else:
                    noImages = max(
                        1,
                        int(
                            (rotationAxisEnd - rotationAxisStart) / oscillationWidth
                            + 0.5
                        ),
                    )

                if beamline == "id30a2" and noImages > 100:
                    # Limit the number of images for simulation...
                    noImages = 100
                elif beamline in ["id30a3"] and noImages > 100 and noImages % 100:
                    # Make sure the number of images is a multiple of 100
                    newNoImages = int(noImages / 100 + 1) * 100
                    logger.warning(
                        "Number of images increased to {0} for {1} (was {2})".format(
                            newNoImages, beamline, noImages
                        )
                    )
                    noImages = newNoImages

                if xsDataSubWedge.getAction() is None:
                    dcAction = "default"
                else:
                    dcAction = xsDataSubWedge.getAction().getValue()
                processingParameters = QUEUE_MODEL_OBJECTS.ProcessingParameters()
                if dcAction == "burn":
                    processingParameters.process_data = False
                else:
                    if not doProcess:
                        processingParameters.process_data = False
                    else:
                        processingParameters.process_data = True
                        # if dictCellSpaceGroup == {}:
                        #    dictCellSpaceGroup = \
                        #        edna_mxv1.getCellSpaceGroupFromCharacterisationResult(beamline, mxv1ResultCharacterisation)
                        if "spaceGroup" in dictCellSpaceGroup:
                            logger.debug(
                                "Autoproc forced space group: {spaceGroup}".format(
                                    **dictCellSpaceGroup
                                )
                            )
                            processingParameters.space_group = dictCellSpaceGroup[
                                "spaceGroup"
                            ]
                            if "cell_a" in dictCellSpaceGroup:
                                logger.debug(
                                    "Autoproc forced cell: (a={cell_a:.1f}, b={cell_b:.1f}, c={cell_c:.1f}, alpha={cell_alpha:.1f}, beta={cell_beta:.1f}, gamma={cell_gamma:.1f})".format(
                                        **dictCellSpaceGroup
                                    )
                                )
                                processingParameters.cell_a = dictCellSpaceGroup[
                                    "cell_a"
                                ]
                                processingParameters.cell_b = dictCellSpaceGroup[
                                    "cell_b"
                                ]
                                processingParameters.cell_c = dictCellSpaceGroup[
                                    "cell_c"
                                ]
                                processingParameters.cell_alpha = dictCellSpaceGroup[
                                    "cell_alpha"
                                ]
                                processingParameters.cell_beta = dictCellSpaceGroup[
                                    "cell_beta"
                                ]
                                processingParameters.cell_gamma = dictCellSpaceGroup[
                                    "cell_gamma"
                                ]
                        else:
                            logger.debug(
                                "Not forcing space group or cell, dictCellSpaceGroup: %s",
                                pprint.pformat(dictCellSpaceGroup),
                            )

                samplePosition = xsDataExperimentalCondition.goniostat.samplePosition
                motorPositionCopy = dict(motorPositions)
                if "kappa" not in motorPositionCopy:
                    motorPositionCopy["kappa"] = 0.0
                    motorPositionCopy["kappa_phi"] = 0.0
                    logger.debug(motorPositionCopy)
                firstCentredPosition = QUEUE_MODEL_OBJECTS.CentredPosition(
                    motorPositionCopy
                )
                firstCentredPosition.phi = rotationAxisStart
                firstCentredPosition.phiy = motorPositions["phiy"]
                firstCentredPosition.phiz = motorPositions["phiz"]
                firstCentredPosition.beam_x = 0.0
                firstCentredPosition.beam_y = 0.0
                secondCentredPosition = None
                if samplePosition != []:
                    if beamline in ["id23eh2"]:
                        logger.debug(
                            "Sample position from incoming strategy: sampx=%.3f sampy=%.3f phiz=%.3f",
                            samplePosition[0].v1,
                            samplePosition[0].v2,
                            samplePosition[0].v3,
                        )
                    else:
                        logger.debug(
                            "Sample position from incoming strategy: sampx=%.3f sampy=%.3f phiy=%.3f",
                            samplePosition[0].v1,
                            samplePosition[0].v2,
                            samplePosition[0].v3,
                        )
                    firstCentredPosition.sampx = samplePosition[0].v1
                    firstCentredPosition.sampy = samplePosition[0].v2
                    if beamline in ["id23eh2"]:
                        firstCentredPosition.phiz = samplePosition[0].v3
                    else:
                        firstCentredPosition.phiy = samplePosition[0].v3
                    if len(samplePosition) == 2:
                        # Helical data collection
                        if beamline in ["id23eh2"]:
                            logger.debug(
                                "Second sample position from incoming strategy: sampx=%.3f sampy=%.3f phiz=%.3f",
                                samplePosition[1].v1,
                                samplePosition[1].v2,
                                samplePosition[1].v3,
                            )
                        else:
                            logger.debug(
                                "Second sample position from incoming strategy: sampx=%.3f sampy=%.3f phiy=%.3f",
                                samplePosition[1].v1,
                                samplePosition[1].v2,
                                samplePosition[1].v3,
                            )
                        secondCentredPosition = QUEUE_MODEL_OBJECTS.CentredPosition(
                            motorPositionCopy
                        )
                        secondCentredPosition.sampx = samplePosition[1].v1
                        secondCentredPosition.sampy = samplePosition[1].v2
                        if beamline in ["id23eh2"]:
                            secondCentredPosition.phiz = samplePosition[1].v3
                        else:
                            secondCentredPosition.phiy = samplePosition[1].v3
                        secondCentredPosition.phi = rotationAxisStart
                else:
                    sampx = motorPositions["sampx"]
                    sampy = motorPositions["sampy"]
                    phiy = motorPositions["phiy"]
                    phiz = motorPositions["phiz"]
                    logger.debug(
                        "Using current sample position: sampx=%.3f sampy=%.3f phiy=%.3f",
                        sampx,
                        sampy,
                        phiy,
                    )
                    firstCentredPosition.sampx = sampx
                    firstCentredPosition.sampy = sampy
                    if beamline in ["id23eh2"]:
                        firstCentredPosition.phiz = phiz
                    else:
                        firstCentredPosition.phiy = phiy

                #        wavelength = xsDataExperimentalCondition.getBeam().getWavelength().getValue()

                dataCollection = QUEUE_MODEL_OBJECTS.DataCollection()
                dataCollection.processing_parameters = processingParameters
                if not beamline == "simulator":
                    # Default parameters
                    default_collection_parameters = jsonpickle.decode(
                        serverProxy.beamline_setup_read(
                            "/beamline/default-acquisition-parameters/"
                        )
                    )
                    logger.debug(
                        "default_collection_parameters: %s",
                        pprint.pformat(default_collection_parameters),
                    )
                    dataCollection.acquisitions[0].acquisition_parameters = (
                        default_collection_parameters
                    )
                    default_path_template = jsonpickle.decode(
                        serverProxy.beamline_setup_read(
                            "/beamline/default-path-template/"
                        )
                    )
                    logger.debug(
                        "default_path_template: %s",
                        pprint.pformat(default_path_template),
                    )
                    dataCollection.acquisitions[0].path_template = default_path_template
                else:
                    dataCollection.acquisitions[0].acquisition_parameters = (
                        QUEUE_MODEL_OBJECTS.AcquisitionParameters()
                    )
                dataCollection.acquisitions[
                    0
                ].acquisition_parameters.centred_position = firstCentredPosition
                if fineSlicedReferenceImages and beamline in ["id30a1"]:
                    dataCollection.experiment_type = 6
                    dataCollection.acquisitions[
                        0
                    ].acquisition_parameters.shutterless = True
                elif secondCentredPosition is not None:
                    dataCollection.experiment_type = 5
                    dataCollection.acquisitions.append(
                        QUEUE_MODEL_OBJECTS.Acquisition()
                    )
                    dataCollection.acquisitions[
                        1
                    ].acquisition_parameters.centred_position = secondCentredPosition
                    dataCollection.acquisitions[
                        0
                    ].acquisition_parameters.shutterless = True
                elif abs(overlap) > 0.1:
                    dataCollection.experiment_type = 7
                    dataCollection.acquisitions[
                        0
                    ].acquisition_parameters.shutterless = False
                else:
                    dataCollection.experiment_type = 7
                    dataCollection.acquisitions[
                        0
                    ].acquisition_parameters.shutterless = True
                dataCollection.acquisitions[0].acquisition_parameters.exp_time = (
                    exposureTime
                )
                dataCollection.acquisitions[0].acquisition_parameters.first_image = (
                    firstImageNo
                )
                dataCollection.acquisitions[0].path_template.start_num = firstImageNo
                dataCollection.acquisitions[0].acquisition_parameters.num_images = (
                    noImages
                )
                dataCollection.acquisitions[0].path_template.num_files = firstImageNo
                dataCollection.acquisitions[0].acquisition_parameters.osc_start = (
                    rotationAxisStart
                )
                dataCollection.acquisitions[0].acquisition_parameters.osc_range = (
                    oscillationWidth
                )
                dataCollection.acquisitions[0].acquisition_parameters.overlap = overlap
                dataCollection.acquisitions[0].acquisition_parameters.transmission = (
                    transmission
                )
                dataCollection.acquisitions[0].acquisition_parameters.take_snapshots = (
                    snapShots
                )
                logger.debug("snapShots: %r", snapShots)
                if inverseBeam:
                    dataCollection.acquisitions[
                        0
                    ].acquisition_parameters.inverse_beam = True
                logger.debug("transmission: %r", transmission)
                dataCollection.acquisitions[0].acquisition_parameters.resolution = (
                    resolution
                )
                logger.debug("resolution: %r", resolution)
                dataCollection.acquisitions[0].acquisition_parameters.energy = energy
                logger.debug("energy: %r", energy)

                logger.debug("directory: %r", directory)
                dataCollection.acquisitions[0].path_template.directory = directory
                logger.debug("process_directory: %r", process_directory)
                dataCollection.acquisitions[0].path_template.process_directory = (
                    process_directory
                )
                logger.debug(
                    "expTypePrefix+prefix+wedgeId: %s%s%s",
                    expTypePrefix,
                    prefix,
                    wedgeID,
                )
                dataCollection.acquisitions[0].path_template.base_prefix = (
                    f"{expTypePrefix}{prefix}{wedgeID}"
                )
                logger.debug(
                    "name: %r",
                    dataCollection.acquisitions[0].path_template.get_prefix(),
                )
                dataCollection.set_name(
                    dataCollection.acquisitions[0].path_template.get_prefix()
                )
                dataCollection.set_number(runNumberDataCollection)
                logger.debug("noImages: %r", noImages)
                dataCollection.acquisitions[0].path_template.num_files = noImages
                logger.debug("runNumberDataCollection: %r", runNumberDataCollection)
                dataCollection.acquisitions[0].path_template.run_number = (
                    runNumberDataCollection
                )
                dataCollection.workflow_parameters = {
                    "workflow_name": workflow_name,
                    "workflow_type": workflow_type,
                    "workflow_uid": bes_request_id,
                    "workflow_characterisation_id": characterisation_id,
                    "workflow_kappa_settings_id": kappa_settings_id,
                    "workflow_position_id": position_id,
                    "workflow_group_by": group_by,
                    "sample_name": sample_name,
                }
                dataCollection.workflow_name = workflow_name
                dataCollection.workflow_type = workflow_type
                dataCollection.workflow_uid = bes_request_id
                dataCollection.workflow_position_id = position_id
                dataCollection.workflow_characterisation_id = characterisation_id
                dataCollection.workflow_kappa_settings_id = kappa_settings_id
                dataCollection.workflow_group_by = group_by

                listQueueModelDataCollections.append(dataCollection)
    return listQueueModelDataCollections


def executeQueueForMeshScan(
    beamline,
    workflow_index,
    workflow_type,
    meshQueueList,
    group_node_id,
    sample_node_id,
    resolution,
    motorPositions,
    force_not_shutterless=False,
    noLines=1,
    meshRange=None,
    token=None,
    gridId=None,
    highResolution=None,
    lowResolution=None,
    process_data=False,
    workflow_name=None,
    workflow_id=None,
    bes_request_id=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    group_by=None,
    sample_name=None,
):
    """
    This method loads and executes the mxCuBE queue for mesh scans
    """
    global QUEUE_MODEL_OBJECTS
    listNodeId = []
    logger = workflow_logging.getLogger()
    if isinstance(meshQueueList, str):
        meshQueueList = json.loads(meshQueueList)
    if meshQueueList is not None:
        serverProxy = getServerProxy(beamline, token)
        if beamline == "simulator":
            logger.info("Local queue_model_objects_v1 loaded")
            import queue_model_objects_v1 as QUEUE_MODEL_OBJECTS
        else:
            serverProxy = getServerProxy(beamline, token)
            importQueueModelObjects(beamline, serverProxy)
            if group_node_id is None:
                # Create new data collection group
                group_node_id = createNewDataCollectionGroup(
                    beamline,
                    workflow_type,
                    workflow_index,
                    True,
                    sample_node_id,
                    token=token,
                )
            else:
                logger.debug("Incoming group node id: %r", group_node_id)
            logger.debug("Sample node id: %r", sample_node_id)
        for queueEntry in meshQueueList:
            fileData = queueEntry["fileData"]
            subWedgeData = queueEntry["subWedgeData"]
            # Default parameters
            if beamline == "simulator":
                default_collection_parameters = (
                    QUEUE_MODEL_OBJECTS.AcquisitionParameters()
                )
                default_path_template = QUEUE_MODEL_OBJECTS.PathTemplate()
            else:
                default_collection_parameters = jsonpickle.decode(
                    serverProxy.beamline_setup_read(
                        "/beamline/default-acquisition-parameters/"
                    )
                )
                logger.debug(
                    "default_collection_parameters: %s",
                    pprint.pformat(default_collection_parameters),
                )
                default_path_template = jsonpickle.decode(
                    serverProxy.beamline_setup_read("/beamline/default-path-template/")
                )
                logger.debug(
                    "default_path_template: %s", pprint.pformat(default_path_template)
                )
            centredPosition = None
            startCentredPosition = None
            endCentredPosition = None
            motorPositionCopy = dict(motorPositions)
            if "kappa" not in motorPositionCopy:
                motorPositionCopy["kappa"] = 0.0
                motorPositionCopy["kappa_phi"] = 0.0
                logger.debug(motorPositionCopy)
            if "newMotorPositions" in queueEntry:
                newMotorPositions = queueEntry["newMotorPositions"]
                centredPosition = QUEUE_MODEL_OBJECTS.CentredPosition(motorPositionCopy)
                logger.debug("phi: %r", subWedgeData["rotationAxisStart"])
                centredPosition.phi = subWedgeData["rotationAxisStart"]
                logger.debug("sampx: %r", newMotorPositions["sampx"])
                centredPosition.sampx = newMotorPositions["sampx"]
                logger.debug("sampy: %r", newMotorPositions["sampy"])
                centredPosition.sampy = newMotorPositions["sampy"]
                logger.debug("phiy: %r", newMotorPositions["phiy"])
                centredPosition.phiy = newMotorPositions["phiy"]
                logger.debug("phiz: %r", newMotorPositions["phiz"])
                centredPosition.phiz = newMotorPositions["phiz"]
            elif "motorPositions4dscan" in queueEntry:
                motorPositions4dscan = queueEntry["motorPositions4dscan"]
                startCentredPosition = QUEUE_MODEL_OBJECTS.CentredPosition(
                    motorPositionCopy
                )
                endCentredPosition = QUEUE_MODEL_OBJECTS.CentredPosition(
                    motorPositionCopy
                )
                logger.debug("phi: %r", subWedgeData["rotationAxisStart"])
                startCentredPosition.phi = subWedgeData["rotationAxisStart"]
                endCentredPosition.phi = subWedgeData["rotationAxisStart"]
                logger.debug("sampx start: %r", motorPositions4dscan["sampx_s"])
                logger.debug("sampx end:   %r", motorPositions4dscan["sampx_e"])
                startCentredPosition.sampx = motorPositions4dscan["sampx_s"]
                endCentredPosition.sampx = motorPositions4dscan["sampx_e"]
                logger.debug("sampy start: %r", motorPositions4dscan["sampy_s"])
                logger.debug("sampy end:   %r", motorPositions4dscan["sampy_e"])
                startCentredPosition.sampy = motorPositions4dscan["sampy_s"]
                endCentredPosition.sampy = motorPositions4dscan["sampy_e"]
                logger.debug("phiy start: %r", motorPositions4dscan["phiy_s"])
                logger.debug("phiy end:   %r", motorPositions4dscan["phiy_e"])
                startCentredPosition.phiy = motorPositions4dscan["phiy_s"]
                endCentredPosition.phiy = motorPositions4dscan["phiy_e"]
                logger.debug("phiz start: %r", motorPositions4dscan["phiz_s"])
                logger.debug("phiz end:   %r", motorPositions4dscan["phiz_e"])
                startCentredPosition.phiz = motorPositions4dscan["phiz_s"]
                endCentredPosition.phiz = motorPositions4dscan["phiz_e"]
                # Robodiff on ID30a1
                if "y" in motorPositions:
                    startCentredPosition.y = None
                    endCentredPosition.y = None
                if "z" in motorPositions:
                    startCentredPosition.z = None
                    endCentredPosition.z = None
            processingParameters = QUEUE_MODEL_OBJECTS.ProcessingParameters()
            processingParameters.process_data = process_data

            dataCollection = QUEUE_MODEL_OBJECTS.DataCollection()
            if gridId is not None:
                dataCollection.shape = gridId
                logger.debug("shape / gridId: %r", gridId)
            if subWedgeData["noImages"] > 1:
                dataCollection.experiment_type = 5
            else:
                dataCollection.experiment_type = 7
            dataCollection.processing_parameters = processingParameters
            dataCollection.acquisitions[0].acquisition_parameters = (
                default_collection_parameters
            )
            if centredPosition is not None:
                dataCollection.acquisitions[
                    0
                ].acquisition_parameters.centred_position = centredPosition
                dataCollection.acquisitions[0].acquisition_parameters.shutterless = (
                    False
                )
            else:
                dataCollection.acquisitions[
                    0
                ].acquisition_parameters.centred_position = startCentredPosition
                dataCollection.acquisitions.append(QUEUE_MODEL_OBJECTS.Acquisition())
                dataCollection.acquisitions[
                    1
                ].acquisition_parameters.centred_position = endCentredPosition
                if force_not_shutterless:
                    dataCollection.acquisitions[
                        0
                    ].acquisition_parameters.shutterless = False
                else:
                    dataCollection.acquisitions[
                        0
                    ].acquisition_parameters.shutterless = True
            dataCollection.acquisitions[0].acquisition_parameters.exp_time = (
                subWedgeData["exposureTime"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.num_images = (
                subWedgeData["noImages"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.osc_start = (
                subWedgeData["rotationAxisStart"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.osc_range = (
                subWedgeData["oscillationWidth"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.first_image = (
                fileData["firstImage"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.overlap = 0.0
            dataCollection.acquisitions[0].acquisition_parameters.take_snapshots = False
            logger.debug("transmission: %r", subWedgeData["transmission"])
            dataCollection.acquisitions[0].acquisition_parameters.transmission = (
                subWedgeData["transmission"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.num_lines = noLines
            logger.debug("noLines: %r", noLines)
            dataCollection.acquisitions[0].acquisition_parameters.mesh_range = meshRange
            logger.debug("meshRange: %r", meshRange)

            if resolution is None:
                resolution = getResolution(beamline, token=token)
            resolution = limitResolutiom(resolution, lowResolution, highResolution)
            dataCollection.acquisitions[0].acquisition_parameters.resolution = (
                resolution
            )
            logger.debug("resolution: %r", resolution)

            energy = getEnergy(beamline, token=token)
            dataCollection.acquisitions[0].acquisition_parameters.energy = energy
            logger.debug("energy: %r", energy)

            dataCollection.acquisitions[0].path_template = default_path_template
            logger.debug("directory: %r", fileData["directory"])
            dataCollection.acquisitions[0].path_template.directory = fileData[
                "directory"
            ]
            logger.debug("process_directory: %r", fileData["process_directory"])
            dataCollection.acquisitions[0].path_template.process_directory = fileData[
                "process_directory"
            ]
            logger.debug(
                "expTypePrefix+prefix: %s%s",
                fileData["expTypePrefix"],
                fileData["prefix"],
            )
            dataCollection.acquisitions[0].path_template.base_prefix = (
                f'{fileData["expTypePrefix"]}{fileData["prefix"]}'
            )
            logger.debug(
                "name: %r", dataCollection.acquisitions[0].path_template.get_prefix()
            )
            dataCollection.set_name(
                dataCollection.acquisitions[0].path_template.get_prefix()
            )
            dataCollection.set_number(fileData["runNumber"])
            logger.debug("noImages: %r", subWedgeData["noImages"])
            dataCollection.acquisitions[0].path_template.num_files = subWedgeData[
                "noImages"
            ]
            dataCollection.acquisitions[0].path_template.start_num = fileData[
                "firstImage"
            ]
            logger.debug("runNumber: %r", fileData["runNumber"])
            dataCollection.acquisitions[0].path_template.run_number = fileData[
                "runNumber"
            ]
            dataCollection.set_enabled(True)
            dataCollection.ispyb_group_data_collections = True
            dataCollection.workflow_parameters = {
                "workflow_name": workflow_name,
                "workflow_type": workflow_type,
                "workflow_uid": bes_request_id,
                "workflow_characterisation_id": characterisation_id,
                "workflow_kappa_settings_id": kappa_settings_id,
                "workflow_position_id": position_id,
                "workflow_group_by": group_by,
                "sample_name": sample_name,
            }
            dataCollection.workflow_name = workflow_name
            dataCollection.workflow_type = workflow_type
            dataCollection.workflow_uid = bes_request_id
            dataCollection.workflow_position_id = position_id
            dataCollection.workflow_characterisation_id = characterisation_id
            dataCollection.workflow_kappa_settings_id = kappa_settings_id
            dataCollection.workflow_position_id = group_by
            dataCollection.workflow_group_by = group_by

            if not beamline == "simulator":
                nodeId = serverProxy.queue_add_child(
                    group_node_id, jsonpickle.encode(dataCollection)
                )
                listNodeId.append(nodeId)
                logger.debug("Data collection node id: %r", nodeId)
        if not beamline == "simulator":
            serverProxy.queue_execute_entry_with_id(group_node_id)
    return {"group_node_id": group_node_id, "listNodeId": listNodeId}


def executeQueueForFastMeshScan(
    beamline,
    workflow_index,
    workflow_type,
    meshQueueList,
    group_node_id,
    sample_node_id,
    resolution,
    motorPositions,
    noLines,
    meshRange,
    token=None,
    gridId=None,
    detector_roi_mode=None,
    highResolution=None,
    lowResolution=None,
    workflow_name=None,
    workflow_id=None,
    bes_request_id=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    group_by=None,
    sample_name=None,
):
    """
    This method loads and executes the mxCuBE queue for mesh scans
    """
    global QUEUE_MODEL_OBJECTS
    listNodeId = []
    logger = workflow_logging.getLogger()
    if isinstance(meshQueueList, str):
        meshQueueList = json.loads(meshQueueList)
    if meshQueueList is not None:
        serverProxy = getServerProxy(beamline, token)
        if beamline == "simulator":
            logger.info("Local queue_model_objects_v1 loaded")
            import queue_model_objects_v1 as QUEUE_MODEL_OBJECTS
        else:
            serverProxy = getServerProxy(beamline, token)
            importQueueModelObjects(beamline, serverProxy)
            if group_node_id is None:
                # Create new data collection group
                group_node_id = createNewDataCollectionGroup(
                    beamline,
                    workflow_type,
                    workflow_index,
                    True,
                    sample_node_id,
                    token=token,
                )
            else:
                logger.debug("Incoming group node id: %r", group_node_id)
            logger.debug("Sample node id: %r", sample_node_id)
        for queueEntry in meshQueueList:
            fileData = queueEntry["fileData"]
            subWedgeData = queueEntry["subWedgeData"]
            # Default parameters
            if beamline == "simulator":
                default_collection_parameters = (
                    QUEUE_MODEL_OBJECTS.AcquisitionParameters()
                )
                default_path_template = QUEUE_MODEL_OBJECTS.PathTemplate()
            else:
                default_collection_parameters = jsonpickle.decode(
                    serverProxy.beamline_setup_read(
                        "/beamline/default-acquisition-parameters/"
                    )
                )
                logger.debug(
                    "default_collection_parameters: %s",
                    pprint.pformat(default_collection_parameters),
                )
                default_path_template = jsonpickle.decode(
                    serverProxy.beamline_setup_read("/beamline/default-path-template/")
                )
                logger.debug(
                    "default_path_template: %s", pprint.pformat(default_path_template)
                )
            centredPosition = None
            startCentredPosition = None
            endCentredPosition = None
            motorPositionCopy = dict(motorPositions)
            if "kappa" not in motorPositionCopy:
                motorPositionCopy["kappa"] = 0.0
                motorPositionCopy["kappa_phi"] = 0.0
                logger.debug(motorPositionCopy)
            if "newMotorPositions" in queueEntry:
                newMotorPositions = queueEntry["newMotorPositions"]
                centredPosition = QUEUE_MODEL_OBJECTS.CentredPosition(motorPositionCopy)
                logger.debug("phi: %r", subWedgeData["rotationAxisStart"])
                centredPosition.phi = subWedgeData["rotationAxisStart"]
                logger.debug("sampx: %r", newMotorPositions["sampx"])
                centredPosition.sampx = newMotorPositions["sampx"]
                logger.debug("sampy: %r", newMotorPositions["sampy"])
                centredPosition.sampy = newMotorPositions["sampy"]
                logger.debug("phiy: %r", newMotorPositions["phiy"])
                centredPosition.phiy = newMotorPositions["phiy"]
                logger.debug("phiz: %r", newMotorPositions["phiz"])
                centredPosition.phiz = newMotorPositions["phiz"]
            elif "motorPositions4dscan" in queueEntry:
                motorPositions4dscan = queueEntry["motorPositions4dscan"]
                startCentredPosition = QUEUE_MODEL_OBJECTS.CentredPosition(
                    motorPositionCopy
                )
                endCentredPosition = QUEUE_MODEL_OBJECTS.CentredPosition(
                    motorPositionCopy
                )
                logger.debug("phi: %r", subWedgeData["rotationAxisStart"])
                startCentredPosition.phi = subWedgeData["rotationAxisStart"]
                endCentredPosition.phi = subWedgeData["rotationAxisStart"]
                logger.debug("sampx start: %r", motorPositions4dscan["sampx_s"])
                logger.debug("sampx end:   %r", motorPositions4dscan["sampx_e"])
                startCentredPosition.sampx = motorPositions4dscan["sampx_s"]
                endCentredPosition.sampx = motorPositions4dscan["sampx_e"]
                logger.debug("sampy start: %r", motorPositions4dscan["sampy_s"])
                logger.debug("sampy end:   %r", motorPositions4dscan["sampy_e"])
                startCentredPosition.sampy = motorPositions4dscan["sampy_s"]
                endCentredPosition.sampy = motorPositions4dscan["sampy_e"]
                logger.debug("phiy start: %r", motorPositions4dscan["phiy_s"])
                logger.debug("phiy end:   %r", motorPositions4dscan["phiy_e"])
                startCentredPosition.phiy = motorPositions4dscan["phiy_s"]
                endCentredPosition.phiy = motorPositions4dscan["phiy_e"]
                logger.debug("phiz start: %r", motorPositions4dscan["phiz_s"])
                logger.debug("phiz end:   %r", motorPositions4dscan["phiz_e"])
                startCentredPosition.phiz = motorPositions4dscan["phiz_s"]
                endCentredPosition.phiz = motorPositions4dscan["phiz_e"]
                # Robodiff on ID30a1
                # if "y" in motorPositions:
                #    startCentredPosition.y = None
                #    endCentredPosition.y = None
                # if "z" in motorPositions:
                #    startCentredPosition.z = None
                #    endCentredPosition.z = None
            processingParameters = QUEUE_MODEL_OBJECTS.ProcessingParameters()
            processingParameters.process_data = False

            dataCollection = QUEUE_MODEL_OBJECTS.DataCollection()
            if gridId is not None:
                dataCollection.shape = gridId
                logger.debug("shape / gridId: %r", gridId)
            dataCollection.experiment_type = 8
            dataCollection.processing_parameters = processingParameters
            dataCollection.acquisitions[0].acquisition_parameters = (
                default_collection_parameters
            )
            dataCollection.acquisitions[0].acquisition_parameters.centred_position = (
                startCentredPosition
            )
            dataCollection.acquisitions.append(QUEUE_MODEL_OBJECTS.Acquisition())
            dataCollection.acquisitions[1].acquisition_parameters.centred_position = (
                endCentredPosition
            )
            dataCollection.acquisitions[0].acquisition_parameters.shutterless = True
            # Dtector ROI mode
            if detector_roi_mode is not None:
                logger.debug("detector_roi_mode: %r", detector_roi_mode)
                dataCollection.acquisitions[
                    0
                ].acquisition_parameters.detector_roi_mode = detector_roi_mode
            dataCollection.acquisitions[0].acquisition_parameters.exp_time = (
                subWedgeData["exposureTime"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.num_images = (
                subWedgeData["noImages"] * noLines
            )
            dataCollection.acquisitions[0].acquisition_parameters.osc_start = (
                subWedgeData["rotationAxisStart"]
            )
            # if beamline in ["id23eh1", "id23eh2", "id30a2", "id30a3", "id29", "id30b"]:
            dataCollection.acquisitions[0].acquisition_parameters.osc_range = (
                subWedgeData["oscillationWidth"] / noLines
            )
            # else:
            #     dataCollection.acquisitions[0].acquisition_parameters.osc_range = subWedgeData["oscillationWidth"]
            dataCollection.acquisitions[0].acquisition_parameters.first_image = (
                fileData["firstImage"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.overlap = 0.0
            dataCollection.acquisitions[0].acquisition_parameters.take_snapshots = False
            logger.debug("transmission: %r", subWedgeData["transmission"])
            dataCollection.acquisitions[0].acquisition_parameters.transmission = (
                subWedgeData["transmission"]
            )
            dataCollection.acquisitions[0].acquisition_parameters.num_lines = noLines
            logger.debug("noLines: %r", noLines)
            dataCollection.acquisitions[0].acquisition_parameters.mesh_range = meshRange
            logger.debug("meshRange: %r", meshRange)
            if resolution is None:
                resolution = getResolution(beamline, token=token)
            resolution = limitResolutiom(resolution, lowResolution, highResolution)
            dataCollection.acquisitions[0].acquisition_parameters.resolution = (
                resolution
            )
            logger.debug("resolution: %r", resolution)

            energy = getEnergy(beamline, token=token)
            dataCollection.acquisitions[0].acquisition_parameters.energy = energy
            logger.debug("energy: %r", energy)

            dataCollection.acquisitions[0].path_template = default_path_template
            logger.debug("directory: %r", fileData["directory"])
            dataCollection.acquisitions[0].path_template.directory = fileData[
                "directory"
            ]
            logger.debug("process_directory: %r", fileData["process_directory"])
            dataCollection.acquisitions[0].path_template.process_directory = fileData[
                "process_directory"
            ]
            logger.debug(
                "expTypePrefix+prefix: %s%s",
                fileData["expTypePrefix"],
                fileData["prefix"],
            )
            dataCollection.acquisitions[0].path_template.base_prefix = (
                f'{fileData["expTypePrefix"]}{fileData["prefix"]}'
            )
            logger.debug(
                "name: %r", dataCollection.acquisitions[0].path_template.get_prefix()
            )
            dataCollection.set_name(
                dataCollection.acquisitions[0].path_template.get_prefix()
            )
            dataCollection.set_number(fileData["runNumber"])
            logger.debug("noImages: %r", subWedgeData["noImages"])
            dataCollection.acquisitions[0].path_template.num_files = subWedgeData[
                "noImages"
            ]
            dataCollection.acquisitions[0].path_template.start_num = fileData[
                "firstImage"
            ]
            logger.debug("runNumber: %r", fileData["runNumber"])
            dataCollection.acquisitions[0].path_template.run_number = fileData[
                "runNumber"
            ]
            logger.debug("dataCollection: %s", pprint.pformat(dataCollection.as_dict()))
            dataCollection.set_enabled(True)
            dataCollection.ispyb_group_data_collections = True
            dataCollection.workflow_parameters = {
                "workflow_name": workflow_name,
                "workflow_type": workflow_type,
                "workflow_uid": bes_request_id,
                "workflow_characterisation_id": characterisation_id,
                "workflow_kappa_settings_id": kappa_settings_id,
                "workflow_position_id": position_id,
                "workflow_group_by": group_by,
                "sample_name": sample_name,
            }
            dataCollection.workflow_name = workflow_name
            dataCollection.workflow_type = workflow_type
            dataCollection.workflow_uid = bes_request_id
            dataCollection.workflow_position_id = position_id
            dataCollection.workflow_characterisation_id = characterisation_id
            dataCollection.workflow_kappa_settings_id = kappa_settings_id
            dataCollection.workflow_position_id = position_id
            dataCollection.workflow_group_by = group_by

            if not beamline == "simulator":
                nodeId = serverProxy.queue_add_child(
                    group_node_id, jsonpickle.encode(dataCollection)
                )
                listNodeId.append(nodeId)
                logger.debug("Data collection node id: %r", nodeId)
        if not beamline == "simulator":
            serverProxy.queue_execute_entry_with_id(group_node_id)
    return {"group_node_id": group_node_id, "listNodeId": listNodeId}


def getBeamSize(beamline, token=None):
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    resolution = serverProxy.beamline_setup_read("/beamline/resolution")
    logger.info("Resolution: %r", resolution)
    beamsize = serverProxy.beamline_setup_read("/beamline/collect")
    logger.info("Beamsize: %r", beamsize)


def getDataCollectionInfo(
    beamline, directory, transmission, run_number, mxv1StrategyResult
):
    """
    This method returns a dictionary with data collection info like
    number of collection plans, number of subwedges etc.
    """
    logger = workflow_logging.getLogger()
    xsDataResultStrategy = edna_mxv1.parseMxv1StrategyResult(mxv1StrategyResult)
    noCollectionPlans = 0
    noSubWedges = 0
    noBurnSubWedges = 0
    totalExposureTime = 0.0
    totalStartAngle = 0.0
    totalEndAngle = 0.0
    totalNoImages = 0
    for xsDataCollectionPlan in xsDataResultStrategy.collectionPlan:
        noCollectionPlans += 1
        xsDataCollectionStrategy = xsDataCollectionPlan.collectionStrategy
        for xsDataSubWedge in xsDataCollectionStrategy.subWedge:
            if xsDataSubWedge.action is None:
                noSubWedges += 1
            elif xsDataSubWedge.action.value == "burn":
                noBurnSubWedges += 1
            else:
                noSubWedges += 1
            xsDataExperimentalCondition = xsDataSubWedge.experimentalCondition
            exposureTime = xsDataExperimentalCondition.beam.exposureTime.value
            oscillationWidth = (
                xsDataExperimentalCondition.goniostat.oscillationWidth.value
            )
            rotationAxisStart = (
                xsDataExperimentalCondition.goniostat.rotationAxisStart.value
            )
            if totalStartAngle is None:
                totalStartAngle = rotationAxisStart
            elif rotationAxisStart < totalStartAngle:
                totalStartAngle = rotationAxisStart
            rotationAxisEnd = (
                xsDataExperimentalCondition.goniostat.rotationAxisEnd.value
            )
            if totalEndAngle is None:
                totalEndAngle = rotationAxisEnd
            elif rotationAxisEnd > totalEndAngle:
                totalEndAngle = rotationAxisEnd
            if oscillationWidth > 0.0001:
                noImages = max(
                    1,
                    int((rotationAxisEnd - rotationAxisStart) / oscillationWidth + 0.5),
                )
            else:
                # Still image
                noImages = 1
            totalNoImages += noImages
            totalExposureTime += exposureTime * noImages

    collection_message = "Starting a reference image data collection..."
    collection_start_time = time.strftime(
        "%Y-%m-%dT%H:%M:%S", time.localtime(time.time())
    )
    dataCollectionComment = "Image created for EDNA characterisation"
    process_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")

    logger.info(collection_message)
    logger.info("Number of  sweeps (wedges): %d", noCollectionPlans)
    logger.info("Number of subwedges (data collections): %d", noSubWedges)
    logger.info("Number of burning subwedges (data collections): %d", noBurnSubWedges)
    logger.info("Total number of images: %d", totalNoImages)
    logger.info("Total exposure time: %.2f", totalExposureTime)
    logger.info("Total start angle: %.2f", totalStartAngle)
    logger.info("Total end angle: %.2f", totalEndAngle)
    logger.info("Transmission: %.2f", transmission)
    logger.info("Start run number: %d ", run_number)
    logger.debug("Data collection start time: %s", collection_start_time)
    logger.debug("Data collection comment: %s", dataCollectionComment)
    logger.info("Raw directory: %s", directory)
    logger.debug("Process directory: %s", process_directory)

    dictDataCollectionInfo = {
        "noCollectionPlans": noCollectionPlans,
        "noSubWedges": noSubWedges,
        "noBurnSubWedges": noBurnSubWedges,
        "totalNoImages": totalNoImages,
        "totalExposureTime": totalExposureTime,
    }

    return dictDataCollectionInfo


def displayNewHtmlPage(
    beamline, strHtmlPagePath, strPageName, list_node_id, strPageNumber=1, token=None
):
    """
    This method enables displaying an html page in mxCuBE
    """
    logger = workflow_logging.getLogger()
    logger.debug("Path to html page: %s", strHtmlPagePath)
    if os.path.exists(strHtmlPagePath):
        if beamline == "simulator":
            logger.debug("Not displaying html page on simulator")
        else:
            serverProxy = getServerProxy(beamline, token)
            if serverProxy is not None:
                try:
                    logger.debug(
                        "%r, %r, %r", strHtmlPagePath, strPageName, strPageNumber
                    )
                    for node_id in list_node_id:
                        serverProxy.queue_update_result(node_id, strHtmlPagePath)
                        logger.debug(
                            "New html page displayed, prefix = %r, run_number = %r, node_id = %r",
                            strPageName,
                            strPageNumber,
                            node_id,
                        )
                except Exception as e:
                    logger.error(
                        "collect.displayNewHtmlPage: Cannot display html page, error: %r",
                        e,
                    )
                    logger.error("Path to html page = %s", strHtmlPagePath)
                    raise
    else:
        logger.error(
            "collect.displayNewHtmlPage: Path to html page does not exist: %s",
            strHtmlPagePath,
        )


def getGridInfo(beamline, token=None, shape=None):
    """
    This method returns a dictionary with the current grid info
    """
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    dict_grid_info = {}
    if serverProxy is not None:
        logger.debug("getGridInfo")
        dict_grid_info = serverProxy.shape_history_get_grid(shape)
    if dict_grid_info != {}:
        logger.debug(json.dumps(dict_grid_info, indent=4))
        # Force angle to be zero
        dict_grid_info["angle"] = 0
        if "x1" in dict_grid_info:
            logger.info("Grid info:")
            logger.info(
                "x1 : %6.3f mm, y1 : %6.3f mm",
                dict_grid_info["x1"],
                dict_grid_info["y1"],
            )
            logger.info(
                "dx : %6.3f mm, dy : %6.3f mm",
                dict_grid_info["dx_mm"],
                dict_grid_info["dy_mm"],
            )
            logger.info(
                "nx : %6d,    ny : %6d",
                dict_grid_info["steps_x"],
                dict_grid_info["steps_y"],
            )
    else:
        logger.error("No grid info returned from beamline %s!", beamline)
    return dict_grid_info


def setImageGridData(beamline, base64data, key, token=None, data_file_path=None):
    serverProxy = getServerProxy(beamline, token)
    if serverProxy is not None:
        if beamline in ["id23eh1"]:
            serverProxy.shape_history_set_grid_data(key, base64data, data_file_path)
        else:
            serverProxy.shape_history_set_grid_data(key, base64data)


def setGridData(
    beamline,
    grid_info,
    mesh_positions,
    token=None,
    grid_info_mxcube=None,
    hexArray=None,
):
    logger = workflow_logging.getLogger()
    if "id" not in grid_info:
        logger.debug("Cannot set grid data due to missing grid_info id")
        return
    maxEstimate = 0.0
    for position in mesh_positions:
        if "dozor_score" in position:
            if maxEstimate < position["dozor_score"]:
                maxEstimate = position["dozor_score"]
    if maxEstimate < 0.1:
        logger.debug("Cannot set grid data due to missing diffraction signal")
        return
    logger.debug("Max estimate: %f", maxEstimate)
    key = grid_info["id"]
    result_data = _create_result_data(
        mesh_positions, hexArray, grid_info, grid_info_mxcube, maxEstimate, logger
    )

    serverProxy = getServerProxy(beamline, token)
    if serverProxy is not None:
        serverProxy.shape_history_set_grid_data(key, result_data)


def _get_cdict():
    cdict = {
        "red": (
            (0.0, 0.0, 0.0),
            (0.4, 1.0, 1.0),
            (0.6, 1.0, 1.0),
            (0.8, 1.0, 1.0),
            (1.0, 1.0, 1.0),
        ),
        "green": (
            (0.0, 0.0, 0.0),
            (0.4, 0.0, 0.0),
            (0.6, 0.4, 0.4),
            (0.8, 0.7, 0.7),
            (1.0, 1.0, 1.0),
        ),
        "blue": (
            (0.0, 0.0, 0.0),
            (0.4, 0.0, 0.0),
            (0.6, 0.0, 0.0),
            (0.8, 0.1, 0.1),
            (1.0, 0.3, 0.3),
        ),
    }
    return cdict


def _create_result_data(
    mesh_positions, hexArray, grid_info, grid_info_mxcube, maxEstimate, logger
):
    result_data = {}
    hm = {}
    cm = {}
    cdict = _get_cdict()
    cmap1 = matplotlib.colors.LinearSegmentedColormap("my_colormap", cdict, N=256)
    # Check if we have grid_info_mxcube
    if grid_info_mxcube is not None:
        maxStepX = grid_info_mxcube["steps_x"]
        maxStepY = grid_info_mxcube["steps_y"]
    else:
        maxStepX = None
        maxStepY = None
    for position in mesh_positions:
        doAdd = True
        if maxStepX is not None:
            if maxStepX < position["indexY"] or maxStepY < position["indexZ"]:
                doAdd = False
        if doAdd:
            cell = str(
                position["indexZ"] * grid_info["steps_x"] + position["indexY"] + 1
            )
            value = _get_position_value(position, maxEstimate, logger)
            v1, v2, v3, v4 = cmap1(value)
            result_data[cell] = (
                cell,
                (float(v1) * 255, float(v2) * 255, float(v3) * 255),
                value,
            )
            if hexArray is not None:
                hexValue = hexArray[position["indexZ"], position["indexY"]]
                _populate_cm_hm(hexValue, cell, v1, v2, v3, cm, hm)
    if hexArray is not None:
        result_data["cm"] = cm
        result_data["hm"] = hm
    return result_data


def _get_position_value(position, maxEstimate, logger):
    if "dozor_score" in position:
        try:
            value = int(position["dozor_score"] / maxEstimate * 255)
        except ValueError:
            logger.debug(
                "Problem converting dozor score to int: %r", position["dozor_score"]
            )
            value = 0
    else:
        value = 0
    return value


def _populate_cm_hm(hexValue, cell, v1, v2, v3, cm, hm):
    cm[cell] = [
        cell,
        [
            int(hexValue[1:3]) * 255,
            int(hexValue[3:5]) * 255,
            int(hexValue[5:7]) * 255,
        ],
    ]
    hm[cell] = [cell, [float(v1) * 255, float(v2) * 255, float(v3) * 255]]


def checkMotorSpeed(beamline, meshQueueList, gridExposureTime, transmission):
    """
    This method checks that the max speed of the 4dscan motors are not exceeded
    """
    logger = workflow_logging.getLogger()
    gridExposureTimeOrig = gridExposureTime
    transmissionOrig = transmission
    # Check speed of motors for all data collections
    bExposureTimeChanged = False
    for queueEntry in meshQueueList:
        tmpQueueEntry = dict(queueEntry)
        #            pprint.pprint(queueEntry)
        if "sampx_s" not in queueEntry:
            tmpQueueEntry["sampx_s"] = queueEntry["motorPositions4dscan"]["sampx_s"]
            tmpQueueEntry["sampx_e"] = queueEntry["motorPositions4dscan"]["sampx_e"]
            tmpQueueEntry["sampy_s"] = queueEntry["motorPositions4dscan"]["sampy_s"]
            tmpQueueEntry["sampy_e"] = queueEntry["motorPositions4dscan"]["sampy_e"]
            tmpQueueEntry["phiy_s"] = queueEntry["motorPositions4dscan"]["phiy_s"]
            tmpQueueEntry["phiy_e"] = queueEntry["motorPositions4dscan"]["phiy_e"]
            tmpQueueEntry["phiz_s"] = queueEntry["motorPositions4dscan"]["phiz_s"]
            tmpQueueEntry["phiz_e"] = queueEntry["motorPositions4dscan"]["phiz_e"]
            tmpQueueEntry["number_images"] = queueEntry["subWedgeData"]["noImages"]
        # phiy
        phiySpeed = abs(
            float(tmpQueueEntry["phiy_e"]) - float(tmpQueueEntry["phiy_s"])
        ) / (float(tmpQueueEntry["number_images"]) * gridExposureTime)
        phiyMaxSpeed = config.get_value(beamline, "Goniometer", "phiyMaxSpeed")
        if phiySpeed > phiyMaxSpeed:
            logger.warning(
                "Max speed (%.3f mm/s) exceeded for motor phiy: %.3f mm/s",
                phiyMaxSpeed,
                phiySpeed,
            )
            newGridExposureTime = abs(
                float(tmpQueueEntry["phiy_e"]) - float(tmpQueueEntry["phiy_s"])
            ) / (float(tmpQueueEntry["number_images"]) * phiyMaxSpeed)
            logger.warning(
                "New exposure time: %.3f s (was %.3f s)",
                newGridExposureTime,
                gridExposureTime,
            )
            gridExposureTime = newGridExposureTime
            bExposureTimeChanged = True
        # phiz
        phizSpeed = abs(
            float(tmpQueueEntry["phiz_e"]) - float(tmpQueueEntry["phiz_s"])
        ) / (float(tmpQueueEntry["number_images"]) * gridExposureTime)
        phizMaxSpeed = config.get_value(beamline, "Goniometer", "phizMaxSpeed")
        if phizSpeed > phizMaxSpeed:
            logger.warning(
                "Max speed (%.3f mm/s) exceeded for motor phiz: %.3f mm/s",
                phizMaxSpeed,
                phizSpeed,
            )
            newGridExposureTime = abs(
                float(tmpQueueEntry["phiz_e"]) - float(tmpQueueEntry["phiz_s"])
            ) / (float(tmpQueueEntry["number_images"]) * phizMaxSpeed)
            logger.warning(
                "New exposure time: %.3f s (was %.3f s)",
                newGridExposureTime,
                gridExposureTime,
            )
            gridExposureTime = newGridExposureTime
            bExposureTimeChanged = True
        # sampx
        sampxSpeed = abs(
            float(tmpQueueEntry["sampx_e"]) - float(tmpQueueEntry["sampx_s"])
        ) / (float(tmpQueueEntry["number_images"]) * gridExposureTime)
        sampxMaxSpeed = config.get_value(beamline, "Goniometer", "sampxMaxSpeed")
        if sampxSpeed > sampxMaxSpeed:
            logger.warning(
                "Max speed (%.3f mm/s) exceeded for motor sampx: %.3f mm/s",
                sampxMaxSpeed,
                sampxSpeed,
            )
            newGridExposureTime = abs(
                float(tmpQueueEntry["sampx_e"]) - float(tmpQueueEntry["sampx_s"])
            ) / (float(tmpQueueEntry["number_images"]) * sampxMaxSpeed)
            logger.warning(
                "New exposure time: %.3f s (was %.3f s)",
                newGridExposureTime,
                gridExposureTime,
            )
            gridExposureTime = newGridExposureTime
            bExposureTimeChanged = True
        # sampy
        sampySpeed = abs(
            float(tmpQueueEntry["sampy_e"]) - float(tmpQueueEntry["sampy_s"])
        ) / (float(tmpQueueEntry["number_images"]) * gridExposureTime)
        sampyMaxSpeed = config.get_value(beamline, "Goniometer", "sampyMaxSpeed")
        if sampySpeed > sampyMaxSpeed:
            logger.warning(
                "Max speed (%.3f mm/s) exceeded for motor sampy: %.3f mm/s",
                sampyMaxSpeed,
                sampySpeed,
            )
            newGridExposureTime = abs(
                float(tmpQueueEntry["sampy_e"]) - float(tmpQueueEntry["sampy_s"])
            ) / (float(tmpQueueEntry["number_images"]) * sampyMaxSpeed)
            logger.warning(
                "New exposure time: %.3f s (was %.3f s)",
                newGridExposureTime,
                gridExposureTime,
            )
            gridExposureTime = newGridExposureTime
            bExposureTimeChanged = True
    dictParameters = None
    if bExposureTimeChanged:
        dictParameters = {}
        transmission = transmission * gridExposureTimeOrig / gridExposureTime
        if transmission < config.get_value(beamline, "Beam", "minTransmission"):
            transmission = config.get_value(beamline, "Beam", "minTransmission")
            logger.warning(
                "New transmission set to minimum transmission: %.1f %% (was %.1f %%)",
                transmission,
                transmissionOrig,
            )
        else:
            logger.warning(
                "New transmission: %.1f %% (was %.1f %%)",
                transmission,
                transmissionOrig,
            )
        dictParameters["gridExposureTime"] = round(gridExposureTime, 3)
        dictParameters["transmission"] = round(transmission, 1)
    return dictParameters


def loadQueue(
    beamline,
    directory,
    process_directory,
    prefix,
    expTypePrefix,
    run_number,
    resolution,
    mxv1StrategyResult,
    motorPositions,
    workflow_type,
    workflow_index,
    group_node_id,
    sample_node_id,
    dictCellSpaceGroup=None,
    energy=None,
    enabled=True,
    snapShots=False,
    firstImageNo=1,
    inverseBeam=False,
    doProcess=True,
    highResolution=None,
    lowResolution=None,
    token=None,
    workflow_name=None,
    workflow_id=None,
    bes_request_id=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    group_by=None,
    sample_name=None,
):
    """
    This method loads and executes the mxCuBE queue (if enabled==True)
    for "normal" data collections
    """
    logger = workflow_logging.getLogger()
    # Force process_directory #TODO: Remove process_directory from arguments
    process_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")
    logger.debug("Forced process_directory: %s", process_directory)
    listNodeId = []
    serverProxy = getServerProxy(beamline, token)
    if serverProxy is not None:
        importQueueModelObjects(beamline, serverProxy)
        if group_node_id is None:
            group_node_id = createNewDataCollectionGroup(
                beamline,
                workflow_type,
                workflow_index,
                enabled,
                sample_node_id,
                token=token,
            )
        else:
            logger.debug("Incoming group node id: %r", group_node_id)
        logger.debug("Sample node id: %r", sample_node_id)
        listQueueModelDataCollections = _createQueueModelDataCollections(
            beamline=beamline,
            directory=directory,
            process_directory=process_directory,
            prefix=prefix,
            expTypePrefix=expTypePrefix,
            run_number=run_number,
            resolution=resolution,
            mxv1StrategyResult=mxv1StrategyResult,
            motorPositions=motorPositions,
            dictCellSpaceGroup=dictCellSpaceGroup,
            energy=energy,
            snapShots=snapShots,
            firstImageNo=firstImageNo,
            inverseBeam=inverseBeam,
            doProcess=doProcess,
            highResolution=highResolution,
            lowResolution=lowResolution,
            token=token,
            workflow_name=workflow_name,
            workflow_type=workflow_type,
            bes_request_id=bes_request_id,
            characterisation_id=characterisation_id,
            kappa_settings_id=kappa_settings_id,
            position_id=position_id,
            group_by=group_by,
            sample_name=sample_name,
        )
        for dataCollection in listQueueModelDataCollections:
            dataCollection.set_enabled(enabled)
            dataCollection.ispyb_group_data_collections = True
            nodeId = serverProxy.queue_add_child(
                group_node_id, jsonpickle.encode(dataCollection)
            )
            listNodeId.append(nodeId)
            logger.debug("Data node id: %r", nodeId)
            if not beamline == "simulator":
                serverProxy.queue_execute_entry_with_id(group_node_id)
    return {"group_node_id": group_node_id, "listNodeId": listNodeId}


def addCentringToQueue(
    beamline,
    workflow_type,
    workflow_index,
    group_node_id,
    sample_node_id,
    enabled=True,
    message="Center sample",
    token=None,
):
    """
    This method adds a centring task to the mxCuBE queue
    """
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    importQueueModelObjects(beamline, serverProxy)
    if QUEUE_MODEL_OBJECTS is not None:
        # if group_node_id is None:
        #     group_node_id = createNewDataCollectionGroup(
        #         beamline,
        #         workflow_type,
        #         workflow_index,
        #         enabled,
        #         sample_node_id,
        #         token=token,
        #     )
        # else:
        logger.debug("Group node id: %r", group_node_id)
        logger.debug("Sample node id: %r", sample_node_id)
        sampleCentring = QUEUE_MODEL_OBJECTS.SampleCentring()
        sampleCentring.set_name(message)
        nodeId = serverProxy.queue_add_child(
            group_node_id, jsonpickle.encode(sampleCentring)
        )
        logger.debug("Sample centring node id: %r", nodeId)
        serverProxy.queue_execute_entry_with_id(group_node_id)
    return group_node_id


def addSmallXrayCentringToQueue(
    beamline,
    workflow_type,
    workflow_index,
    group_node_id,
    sample_node_id,
    enabled=True,
    message="Small X-ray centring",
    token=None,
):
    """
    This method adds a small X-ray centring task to the mxCuBE queue
    """
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    importQueueModelObjects(beamline, serverProxy)
    if QUEUE_MODEL_OBJECTS is not None:
        if group_node_id is None:
            group_node_id = createNewDataCollectionGroup(
                beamline,
                workflow_type,
                workflow_index,
                enabled,
                sample_node_id,
                token=token,
            )
        else:
            logger.debug("Incoming group node id: %r", group_node_id)
        logger.debug("Sample node id: %r", sample_node_id)
        node_id = serverProxy.addXrayCentring(group_node_id)
        logger.info("Node id: {0}".format(node_id))
        xrayCentring2 = QUEUE_MODEL_OBJECTS.XrayCentring2()
        xrayCentring2.set_name(message)
        nodeId = serverProxy.queue_add_child(
            group_node_id, jsonpickle.encode(xrayCentring2)
        )
        logger.debug("X-ray centring node id: %r", nodeId)
        serverProxy.queue_execute_entry_with_id(group_node_id)
    return group_node_id


def runGphlWorkflow(
    beamline,
    task_dict,
    workflow_type,
    workflow_index,
    workflow_id,
    group_node_id,
    sample_node_id,
    enabled=True,
    message="GPhL workflow",
    token=None,
    bes_request_id=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
):
    """
    This method adds a GPhL workflow to the mxCuBE queue
    """
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    importQueueModelObjects(beamline, serverProxy)
    if QUEUE_MODEL_OBJECTS is not None:
        # if group_node_id is None:
        if True:
            group_node_id = createNewDataCollectionGroup(
                beamline,
                workflow_type,
                workflow_index,
                enabled,
                sample_node_id,
                token=token,
            )
            logger.info("Group node id: %r", group_node_id)
        else:
            logger.info("Incoming group node id: %r", group_node_id)
        logger.info("Sample node id: %r", sample_node_id)
        logger.debug("Gphl task_dict: %s", pprint.pformat(task_dict))
        node_id = serverProxy.add_gphl_workflow(group_node_id, task_dict, workflow_id)
        logger.info("Node id: {0}".format(node_id))
        serverProxy.queue_execute_entry_with_id(group_node_id, True)
        time.sleep(10)
        while serverProxy.get_gphl_workflow_status() == "RUNNING":
            logger.info("Gphl workflow still running...")
            time.sleep(5)
        logger.info("Gphl status: %s", serverProxy.get_gphl_workflow_status())
        # serverProxy.await_async_job()
    return group_node_id


def addTestDataCollectionToQueue(
    beamline,
    workflow_type,
    workflow_index,
    group_node_id,
    sample_node_id,
    enabled=True,
    message="Test data collection",
    token=None,
):
    """
    This method adds a small X-ray centring task to the mxCuBE queue
    """
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    importQueueModelObjects(beamline, serverProxy)
    if QUEUE_MODEL_OBJECTS is not None:
        if group_node_id is None:
            group_node_id = createNewDataCollectionGroup(
                beamline,
                workflow_type,
                workflow_index,
                enabled,
                sample_node_id,
                token=token,
            )
        else:
            logger.debug("Incoming group node id: %r", group_node_id)
        logger.debug("Sample node id: %r", sample_node_id)
        dataCollection = QUEUE_MODEL_OBJECTS.DataCollection()
        dataCollection.set_name(message)
        nodeId = serverProxy.queue_add_child(
            group_node_id, jsonpickle.encode(dataCollection)
        )
        logger.debug("Data collection node id: %r", nodeId)
        serverProxy.queue_execute_entry_with_id(group_node_id)
    return group_node_id


def readMotorPositions(beamline, read_focus=False, token=None):
    logger = workflow_logging.getLogger()
    list_motors_to_exclude = ["zoom"]
    if not read_focus:
        list_motors_to_exclude.append("focus")
    serverProxy = getServerProxy(beamline, token)
    dictPosition = serverProxy.get_diffractometer_positions()
    dictPositionCopy = {}
    # Round motor positions to four digits
    for motorName, position in dictPosition.items():
        if motorName in list_motors_to_exclude:
            dictPositionCopy[motorName] = None
        elif position is not None:
            dictPositionCopy[motorName] = round(position, 4)
            if motorName == "phi":
                dictPositionCopy[motorName] = position % 360
    logger.debug("Motor positions: %r", dictPositionCopy)
    return dictPositionCopy


def moveMotors(beamline, directory, dictPosition, move_focus=False, token=None):
    logger = workflow_logging.getLogger()
    list_motors_to_exclude = ["zoom"]
    if not move_focus:
        list_motors_to_exclude.append("focus")
    logger.debug("Incoming argument to move motors: %r", dictPosition)
    currentPositions = readMotorPositions(beamline, directory, token)
    checkBeamlineDirectory(beamline, directory)
    serverProxy = getServerProxy(beamline, token)
    # Check for 'string' motor positions
    newDict = {}
    for motorName in dictPosition:
        if (motorName not in list_motors_to_exclude) and (
            dictPosition[motorName] is not None
        ):
            try:
                new_position = float(dictPosition[motorName])
                if motorName in ["phi", "kappa", "kappa_phi"]:
                    newDict[motorName] = round(new_position, 2) % 360
                else:
                    newDict[motorName] = new_position
            except ValueError:
                logger.warning(
                    f"Motor position '{dictPosition[motorName]}' of motor '{motorName}' cannot be converted to float."
                )
                logger.warning(f"Not moving motor '{motorName}'")
        else:
            logger.debug(f"Not moving motor '{motorName}'")
    if newDict != {}:
        for motorName, newPosition in newDict.items():
            currentPosition = currentPositions[motorName]
            # BES-304: Try to get maxDelta from configuration
            maxDelta = config.get_value(
                beamline, "Goniometer", motorName + "_max_delta", default_value=None
            )
            if maxDelta is None:
                if motorName in ["phi", "kappa", "kappa_phi"]:
                    maxDelta = 0.1
                    currentPosition = round(currentPosition, 2) % 360
                    newPosition = round(newPosition, 2) % 360
                else:
                    maxDelta = 0.002
            if abs(newPosition - currentPosition) > maxDelta:
                successMove = _tryToMoveMotorUpToFiveTimes(
                    beamline,
                    directory,
                    serverProxy,
                    motorName,
                    currentPosition,
                    newPosition,
                    maxDelta,
                    logger,
                    token,
                )
            else:
                logger.info(
                    "Motor {0} not moved as already at position {1:.04f}.".format(
                        motorName, newPosition
                    )
                )
                successMove = True
            if not successMove:
                errorMessage = (
                    "Error when moving motor {0} to position {1:.04f}!".format(
                        motorName, newPosition
                    )
                )
                logger.error(errorMessage)
                raise RuntimeError(errorMessage)
            else:
                logger.debug("Motor {0} reached final destination.".format(motorName))


def _tryToMoveMotorUpToFiveTimes(
    beamline,
    directory,
    serverProxy,
    motorName,
    currentPosition,
    newPosition,
    maxDelta,
    logger,
    token,
):
    # BES-254 workaround: try 5 times before giving up
    successMove = False
    continueToTry = True
    noTrials = 5
    while continueToTry:
        noTrials -= 1
        try:
            serverProxy.move_diffractometer({motorName: newPosition})
            newCurrentPositions = readMotorPositions(beamline, directory, token)
            newCurrentPosition = newCurrentPositions[motorName]
            if motorName in ["phi", "kappa", "kappa_phi"]:
                newCurrentPosition = round(newCurrentPosition, 1) % 360
            if abs(newPosition - newCurrentPosition) > maxDelta:
                logger.warning(
                    "Motor {0} didn't reach final destination {1:.04f}!".format(
                        motorName, newPosition
                    )
                )
                logger.warning(
                    "Motor {0} still at position {1:.04f}. Trying again, trials left: {2}".format(
                        motorName, newCurrentPosition, noTrials
                    )
                )
                successMove = False
                time.sleep(2.0)
            else:
                logger.info(
                    "Motor {0} moved from {1:.04f} to {2:.04f}".format(
                        motorName, currentPosition, newPosition
                    )
                )
                successMove = True
                continueToTry = False
        except Exception as e:
            logger.warning(
                "Error when moving motors: {0}, trials left: {1}".format(e, noTrials)
            )
            time.sleep(1.0)
        if noTrials == 0:
            continueToTry = False
    return successMove


def moveSampleOrthogonallyToRotationAxis(beamline, directory, distance, token=None):
    """This method uses sampx and sampy to move the sample orthogonally to the rotation axis"""
    motorPositions = readMotorPositions(beamline, token=token)
    sampx = motorPositions["sampx"]
    sampy = motorPositions["sampy"]
    phi = motorPositions["phi"]
    phiRad = math.radians(phi)
    newMotorPositions = {}
    newMotorPositions["sampx"] = sampx - distance * math.sin(phiRad)
    newMotorPositions["sampy"] = sampy + distance * math.cos(phiRad)
    moveMotors(beamline, directory, newMotorPositions, token=token)


def checkBeamlineDirectory(beamline, directory):
    logger = workflow_logging.getLogger()
    if beamline in ["simulator", "mxcube3test"]:
        logger.debug("Simulator: not checking beamline and directory")
    else:
        error_message = None
        if not directory.startswith("/data"):
            error_message = (
                "Failed check of directory on beamline '%s'! Directory '%s' does not start with /data.",
                beamline,
                directory,
            )
        elif "visitor" not in directory and "inhouse" not in directory:
            error_message = (
                "Failed check of directory on beamline '%s'! Directory '%s' does not contain 'visitor' or 'inhouse'.",
                beamline,
                directory,
            )
        elif beamline not in directory:
            error_message = (
                "Failed check of directory on beamline '%s'! Directory '%s' does not contain beamline '%s'.",
                beamline,
                directory,
                beamline,
            )
        elif "RAW_DATA" not in directory:
            error_message = (
                "Failed check of directory on beamline '%s'! Directory '%s' does not contain 'RAW_DATA'.",
                beamline,
                directory,
            )
        if error_message is None:
            logger.debug(
                "Check of beamline '%s' and directory '%s' ok.", beamline, directory
            )
        else:
            logger.error(error_message)
            raise RuntimeError(error_message)


def getEnergy(beamline, token=None):
    energy = None
    if not beamline == "simulator":
        serverProxy = getServerProxy(beamline, token)
        if serverProxy is not None:
            energy = serverProxy.beamline_setup_read("/beamline/energy")
            if energy is not None:
                # Round energy to four decimals
                energy = round(energy, 4)
    return energy


def getTransmission(beamline, token=None):
    transmission = None
    serverProxy = getServerProxy(beamline, token)
    if serverProxy is not None:
        transmission = serverProxy.beamline_setup_read("/beamline/transmission")
        if transmission is not None:
            # Round transmission to four decimals
            transmission = round(transmission, 4)
    return transmission


def getResolution(beamline, token=None):
    resolution = None
    serverProxy = getServerProxy(beamline, token)
    if serverProxy is not None:
        resolution = serverProxy.beamline_setup_read("/beamline/resolution")
        if resolution is not None:
            # Round resolution to four decimals
            resolution = round(resolution, 4)
    return resolution


def saveSnapshot(beamline, snapShotFilePath, token=None):
    """This method asks mxCuBE to save a crystal snapshot."""
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    logger.debug("Saving snapshot to file %s", snapShotFilePath)
    if serverProxy is not None:
        try:
            serverProxy.save_snapshot(snapShotFilePath, False, False)
        except Exception as e:
            logger.error("Error when trying to save snapshot: {0}".format(e))
    return os.path.exists(snapShotFilePath)


def saveMultipleSnapshots(beamline, listAnglePath, token=None):
    """This method asks mxCuBE to save several crystal snapshot at different."""
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    logger.debug("List of angles and paths: {0}".format(listAnglePath))
    if serverProxy is not None:
        try:
            serverProxy.save_multiple_snapshots(listAnglePath)
        except Exception as e:
            logger.error("Error when trying to save multiple snapshots: {0}".format(e))
    pathsExist = True
    for angle, snapshotPath in listAnglePath:
        if not os.path.exists(snapshotPath):
            pathsExist = False
    return pathsExist


def createSnapShotDir(beamline, directory):
    strSnapShotRootDirectory = directory.replace("RAW_DATA", "PROCESSED_DATA")
    strTime = time.strftime("%Y%m%d-%H%M%S", time.localtime(time.time()))
    snapShotDir = tempfile.mkdtemp(
        prefix=f"snapshots_{strTime}_", dir=strSnapShotRootDirectory
    )
    os.chmod(snapShotDir, 0o755)
    return snapShotDir


def takeSnapshots(beamline, snapShotDir, background_image, snapShotAngles, token=None):
    # Take nPos snapshots, starting at phi = 0
    logger = workflow_logging.getLogger()
    listSnapshot = []
    listAnglePath = []
    shutil.copy(background_image, os.path.join(snapShotDir, "snapshot_background.png"))
    for phi in snapShotAngles:
        # phi = int(float(indexPosition) / nPos * 360.0)
        pathToSnapshot = os.path.join(snapShotDir, f"snapshot_{int(phi):03d}.png")
        listAnglePath.append([phi, pathToSnapshot])
        listSnapshot.append(pathToSnapshot)
    saveMultipleSnapshots(beamline, listAnglePath, token=token)
    # Create animated gif
    try:
        meshSnapShotPath = os.path.join(snapShotDir, "snapshot_animated.gif")
        listCommandLine = "convert -delay 25 -loop 0 -scale 150x150".split(" ")
        for snapShotFile in listSnapshot:
            listCommandLine.append(snapShotFile)
        listCommandLine.append(meshSnapShotPath)
        logger.debug(listCommandLine)
        subprocess.call(listCommandLine)
    except Exception as e:
        logger.warning("Error when creating animated gif: {0}".format(e))


def takeOneCrystalSnapshot(
    beamline,
    directory,
    prefix,
    run_number,
    snapShotFileName,
    snapShotDirectory,
    token=None,
):
    snapShotFilePath = None
    logger = workflow_logging.getLogger()
    if not os.path.exists(snapShotDirectory):
        os.makedirs(snapShotDirectory, 0o755)
    pathToRawDataSnapShotFile = os.path.join(directory, snapShotFileName)
    logger.debug("Saving snapshot to %s", pathToRawDataSnapShotFile)
    snapShotStatus = saveSnapshot(beamline, pathToRawDataSnapShotFile, token=token)
    logger.debug(
        "Status returned from mxCuBE after saving snapshot: %r", snapShotStatus
    )
    if os.path.exists(pathToRawDataSnapShotFile):
        # Copy the snapshot file to the pyarch directory
        snapShotFilePath = os.path.join(snapShotDirectory, snapShotFileName)
        try:
            shutil.copyfile(pathToRawDataSnapShotFile, snapShotFilePath)
            logger.debug("Snapshot copied to: %s", snapShotFilePath)
        except BaseException:
            logger.warning("Couldn't copy snapshot file to: %s", snapShotFilePath)
    return snapShotFilePath


def get_cp(beamline, token=None):
    dictResult = None
    serverProxy = getServerProxy(beamline, token)
    if serverProxy is not None:
        dictResult = json.loads(serverProxy.get_cp())
    return dictResult


def saveCurrentPos(beamline, token=None):
    serverProxy = getServerProxy(beamline, token)
    if serverProxy is not None:
        serverProxy.save_current_pos()


def compareEncoderValues(position, key, new_value, delta):
    logger = workflow_logging.getLogger()
    if abs(position[key] - new_value) < delta:
        position[key] = new_value
    else:
        logger.debug(
            "Delta too big for position number '{0}' motor '{1}' : {2:.4f}, encoder position : {3:.4f}, delta: {4:.4f}".format(
                position["index"],
                key,
                position[key],
                new_value,
                position[key] - new_value,
            )
        )


def correctUsingEncoderPositions(diagFile, meshPositions):
    logger = workflow_logging.getLogger()
    logger.debug("Correcting motor positions with encoder values")
    delta = 0.005
    newMeshPositions = copy.deepcopy(meshPositions)
    if meshPositions is not None and meshPositions != []:
        arrayValues = readDiagFile(diagFile)
        # Loop through all images
        for number, position in enumerate(newMeshPositions):
            # logger.debug("Encoder pos 1: {0}".format(arrayValues[number * 2]))
            # logger.debug("Encoder pos 2: {0}".format(arrayValues[number * 2+1]))
            averagePos = (arrayValues[number * 2] + arrayValues[number * 2 + 1]) / 2
            # logger.debug("Average pos: {0}".format(averagePos))
            # logger.debug("Position before correction: {0}".format(pprint.pformat(position)))
            compareEncoderValues(position, "sampx", averagePos[2], delta)
            compareEncoderValues(position, "sampy", averagePos[3], delta)
            compareEncoderValues(position, "phiy", averagePos[4], delta)
            # compareEncoderValues(position, "phiz", averagePos[5], delta)
            # logger.debug("Position after correction: {0}".format(pprint.pformat(position)))
            # pprint.pprint(position)
    logger.debug("Finished correcting motor positions with encoder values")
    return newMeshPositions


def correctUsingEncoderPositionsNew(directory, meshPositions):
    newMeshPositions = copy.deepcopy(meshPositions)
    if meshPositions is not None and meshPositions != []:
        prefixRunNumber = meshPositions[0]["imageName"][:-9]
        filePath = os.path.join(directory, prefixRunNumber + "_diag.dat")
        arrayValues = readDiagFile(filePath)
        # Loop through all images
        origIndexZ = None
        imageNoInScan = 1
        #########################################################################################
        # Workaround for issue on MASSIF 1 where more than one diag table are present in the diag file
        # for the same line scan
        maxYIndex = None
        for position in newMeshPositions:
            if maxYIndex is None or maxYIndex < position["indexY"]:
                maxYIndex = position["indexY"]
        if maxYIndex == 0 and arrayValues.shape[0] > 1:
            arrayValuesNew = numpy.ndarray(
                shape=(1, arrayValues.shape[1], arrayValues.shape[2])
            )
            # Take the last scan
            arrayValuesNew[0, :, :] = arrayValues[-1, :, :]
            arrayValues = arrayValuesNew
        #########################################################################################
        for position in newMeshPositions:
            indexZ = position["indexZ"]
            if arrayValues.shape[0] > 1:
                if origIndexZ is None or indexZ != origIndexZ:
                    imageNoInScan = 1
                    origIndexZ = indexZ
                else:
                    imageNoInScan += 1
                subArray = arrayValues[
                    indexZ, numpy.where(arrayValues[indexZ, :, 6] == imageNoInScan), :
                ]
            else:
                subArray = arrayValues[
                    0, numpy.where(arrayValues[0, :, 6] == imageNoInScan), :
                ]
                imageNoInScan += 1
            nz, ny, nx = subArray.shape
            midPosition = int(ny / 2)
            phiy = subArray[0, midPosition, 3]
            phiz = subArray[0, midPosition, 4]
            position["phiy"] = phiy
            position["phiz"] = phiz
    return newMeshPositions


def readDiagFile(filePath, timeOut=30):
    logger = workflow_logging.getLogger()
    logger.debug("Reading diag file: %s", filePath)
    # If necessary wait for the file to appear
    startTime = time.time()
    bContine = True
    fileDir = os.path.dirname(filePath)
    while bContine:
        if os.path.exists(filePath):
            bContine = False
        else:
            logger.info("Waiting for diag file to appear on disk...")
            elapsedTime = time.time() - startTime
            if elapsedTime > timeOut:
                errorMessage = "Time-out after {0} s waiting for file {1}.".format(
                    timeOut, filePath
                )
                logger.error(errorMessage)
                raise IOError(errorMessage)
        time.sleep(1)
        # Patch provided by Sebastien 2018/02/09 for forcing NFS cache:
        logger.debug("NFS cache clear, doing os.fstat on directory {0}".format(fileDir))
        fd = os.open(fileDir, os.O_DIRECTORY)
        statResult = os.fstat(fd)
        os.close(fd)
        logger.debug("Results of os.fstat: {0}".format(statResult))
    fileObject = open(filePath)
    listLines = fileObject.readlines()
    fileObject.close()
    arrayScan = []
    for strLine in listLines[1:]:
        listValues = []
        for number in strLine.split(";"):
            if number != "\n":
                listValues.append(float(number))
        arrayScan.append(listValues)
    arrayScans = numpy.array(arrayScan)
    logger.debug("arrayScans = %s", arrayScans.shape)
    return arrayScans


def id30a1PhiyCorrection(phiy):
    correctionTableX = [
        -0.50,
        -0.45,
        -0.40,
        -0.35,
        -0.30,
        -0.25,
        -0.20,
        -0.15,
        -0.10,
        -0.05,
        0.00,
        0.05,
        0.10,
        0.15,
        0.20,
    ]
    correctionTableY = [
        -0.0252,
        -0.0180,
        -0.0109,
        -0.0032,
        0.0066,
        0.0150,
        0.0179,
        0.0135,
        0.0117,
        -0.0097,
        0.0000,
        0.0031,
        -0.0015,
        -0.0094,
        -0.0182,
    ]
    interpolatedValue = numpy.interp(phiy, correctionTableX, correctionTableY)
    return interpolatedValue


def detectorDistanceToResolution(beamline, detectorDistance, wavelength):
    detectorRadius = config.get_value(beamline, "Detector", "radius")
    twoTheta = math.atan(detectorRadius / detectorDistance)
    resolution = wavelength / (2.0 * math.sin(twoTheta / 2.0))
    return resolution


def resolutionToDetectorDistance(beamline, resolution, wavelength):
    detectorRadius = config.get_value(beamline, "Detector", "radius")
    diameter = 2.0 * detectorRadius
    tmp0 = diameter - config.get_value(beamline, "Detector", "beamX")
    tmp1 = diameter - config.get_value(beamline, "Detector", "beamY")
    tmp2 = config.get_value(beamline, "Detector", "beamX")
    tmp3 = config.get_value(beamline, "Detector", "beamY")
    radius = min([tmp0, tmp1, tmp2, tmp3])
    twoTheta = 2.0 * math.asin(wavelength / (2 * resolution))
    detectorDistance = radius / math.tan(twoTheta)
    return detectorDistance


def energyScan(
    beamline,
    directory,
    process_directory,
    expTypePrefix,
    prefix,
    run_number,
    elementSymbol,
    edgeEnergy,
    workflow_type,
    workflow_index,
    sample_node_id,
    enabled=False,
    group_node_id=None,
    token=None,
):
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    importQueueModelObjects(beamline, serverProxy)
    if QUEUE_MODEL_OBJECTS is not None:
        if group_node_id is None:
            group_node_id = createNewDataCollectionGroup(
                beamline,
                "EnergyScan",
                workflow_index,
                enabled,
                sample_node_id,
                token=token,
            )
        else:
            logger.debug("Incoming group node id: %r", group_node_id)
        logger.debug("Sample node id: %r", sample_node_id)
        default_path_template = jsonpickle.decode(
            serverProxy.beamline_setup_read("/beamline/default-path-template/")
        )
        logger.debug("default_path_template: %s", pprint.pformat(default_path_template))
        energyScan = QUEUE_MODEL_OBJECTS.EnergyScan()
        energyScan.element_symbol = elementSymbol
        energyScan.edge = edgeEnergy
        energyScan.path_template = default_path_template
        energyScan.path_template.directory = directory
        energyScan.path_template.process_directory = process_directory
        energyScan.path_template.base_prefix = f"{expTypePrefix}{prefix}"
        energyScan.set_name(f"{elementSymbol} edge")
        energyScan.set_number(run_number)
        energyScan.set_enabled(enabled)
        nodeId = serverProxy.queue_add_child(
            group_node_id, jsonpickle.encode(energyScan)
        )
        logger.debug("Sample centring node id: %r", nodeId)


#         serverProxy.queue_execute_entry_with_id(group_node_id)


def importQueueModelObjects(beamline, serverProxy):
    # Retrieve code of queue model from server and compile and import it
    # Recipe from http://code.activestate.com/recipes/82234-importing-a-dynamically-generated-module/
    logger = workflow_logging.getLogger()
    global QUEUE_MODEL_OBJECTS
    if QUEUE_MODEL_OBJECTS is None and serverProxy is not None:
        serverProxy.queue_set_serialisation("json")
        logger.debug("Loading queue model objects from MXCuBE: {0}".format(serverProxy))
        userName = getpass.getuser()
        moduleDir = os.path.join("/tmp", userName, beamline, "queue_model_objects")
        if not os.path.exists(moduleDir):
            os.makedirs(moduleDir, 0o755)
        sys.path.insert(0, moduleDir)
        for module_name, module_code in serverProxy.queue_get_model_code():
            logger.debug("Module name: {0}".format(module_name))
            if "." in module_name:
                moduleLocalPath = module_name.replace(".", "/") + ".py"
                localDirs = os.path.dirname(moduleLocalPath).split("/")
                currentPath = moduleDir
                for localDir in localDirs:
                    currentPath = os.path.join(currentPath, localDir)
                    os.makedirs(currentPath, 0o755, exist_ok=True)
                    with open(os.path.join(currentPath, "__init__.py"), "w") as f:
                        f.write("# Queue model object")
            else:
                moduleLocalPath = module_name + ".py"
            modulePath = os.path.join(moduleDir, moduleLocalPath)
            logger.debug("Module path: {0}".format(modulePath))
            with open(modulePath, "w") as f:
                f.write(module_code)
            QUEUE_MODEL_OBJECTS = importlib.import_module(module_name)
            sys.modules[module_name] = QUEUE_MODEL_OBJECTS


def createNewDataCollectionGroup(
    beamline, groupName, workflow_index, enabled, sample_node_id, token=None
):
    # Create new data collection group
    serverProxy = getServerProxy(beamline, token)
    if QUEUE_MODEL_OBJECTS is None:
        importQueueModelObjects(beamline, serverProxy)
    dataCollectionGroup = QUEUE_MODEL_OBJECTS.TaskGroup()
    dataCollectionGroup.set_name(groupName)
    dataCollectionGroup.set_number(workflow_index)
    dataCollectionGroup.set_enabled(enabled)
    json_dcg = jsonpickle.encode(dataCollectionGroup)
    group_node_id = serverProxy.queue_add_child(sample_node_id, json_dcg)
    logger = workflow_logging.getLogger()
    logger.debug("Created new group node id: %r", group_node_id)
    return group_node_id


def getBeamsize(beamline, token=None):
    logger = workflow_logging.getLogger(beamline, token=token)
    serverProxy = getServerProxy(beamline, token)
    beam_width = None
    beam_height = None
    # beam_shape = None
    beam_label = None
    if serverProxy is not None:
        beamsize = serverProxy.get_beam_size()
        logger.debug(f"beamsize: {[beamsize]}")
        beam_width = beamsize[0]
        beam_height = beamsize[1]
        # beam_shape = beamsize[2]
        beam_label = beamsize[3]
        logger.info(f"Current beam label: '{beam_label}'")
        logger.info(f"Current beam size (width/height): {beam_width},{beam_height}")
    return beam_width, beam_height, beam_label


def setBeamsize(beamline, beam_label, token=None):
    logger = workflow_logging.getLogger(beamline, token=token)
    serverProxy = getServerProxy(beamline, token)
    if serverProxy is not None:
        logger.info(f"Setting beam label to: '{beam_label}'")
        serverProxy.set_beam_size(beam_label)


def getAvailableBeamsizes(beamline, token=None):
    logger = workflow_logging.getLogger(beamline, token=token)
    serverProxy = getServerProxy(beamline, token)
    available_beamsizes = None
    if serverProxy is not None:
        available_beamsizes = serverProxy.get_available_beam_size()
        logger.debug("available beamsizes: %s", pprint.pformat(available_beamsizes))
    return available_beamsizes


def getApertureNames(beamline: str, token=None) -> List[str]:
    logger = workflow_logging.getLogger(beamline, token=token)
    names = []
    serverProxy = getServerProxy(beamline, token)
    if serverProxy is None:
        logger.warning("Cannot get proxy to MXCuBE")
    else:
        names = serverProxy.get_aperture_list()
    return list(map(str, names))


def getApertureName(beamline: str, token=None) -> Optional[str]:
    if beamline == "id30a2":
        return "30"

    serverProxy = getServerProxy(beamline, token)
    if serverProxy is None:
        logger = workflow_logging.getLogger(beamline, token=token)
        logger.warning("Cannot get proxy to MXCuBE")
        return

    return str(serverProxy.get_aperture())


def getApertureSize(beamline: str, token=None) -> Optional[int]:
    aperture_name = getApertureName(beamline, token=token)
    if aperture_name is None:
        return

    try:
        aperture_size = aperture_utils.aperture_name_to_size(beamline, aperture_name)
    except ValueError as e:
        logger = workflow_logging.getLogger(beamline, token=token)
        logger.error(str(e))
        return

    return aperture_size


def setApertureName(beamline: str, aperture_name: str, token=None) -> bool:
    logger = workflow_logging.getLogger(beamline, token=token)

    serverProxy = getServerProxy(beamline, token)
    if serverProxy is None:
        logger.warning("Cannot get proxy to MXCuBE")
        current_aperture_name = None
    else:
        serverProxy.set_aperture(aperture_name)
        time.sleep(0.1)
        current_aperture_name = getApertureName(beamline, token=token)

    success = current_aperture_name == aperture_name

    if success:
        logger.info("Aperture set to %s", current_aperture_name)
    else:
        logger.warning(
            "Failed to set aperture to %s (currently %s)",
            aperture_name,
            current_aperture_name or "<unknown>",
        )

    return success


def setApertureSize(beamline: str, aperture_size: int, token=None) -> bool:
    logger = workflow_logging.getLogger(beamline, token=token)

    try:
        aperture_name = aperture_utils.aperture_size_to_name(beamline, aperture_size)
    except ValueError as e:
        logger.error(str(e))
        current_aperture_size = getApertureSize(beamline, token=token)
        logger.error(
            "Failed to set aperture to %s um (currently %s um)",
            aperture_size,
            current_aperture_size,
        )
        return

    return setApertureName(beamline, aperture_name, token=token)


def rpcGetSpectrum(host="massif3backup.esrf.fr", port=47351):
    serverproxy = ServerProxy("http://{0}:{1}".format(host, port))
    spectrum = json.loads(serverproxy.rpc_get_spectrum())
    return spectrum


def workflowEnd(beamline, token=None):
    serverProxy = getServerProxy(beamline, token)
    serverProxy.workflow_end()


def openDialog(beamline, dict_dialog, token=None):
    serverProxy = getServerProxy(beamline, token)
    returnMap = serverProxy.open_dialog(dict_dialog)
    return returnMap


def getZoomLevel(beamline, token=None):
    serverProxy = getServerProxy(beamline, token)
    return serverProxy.get_zoom_level()


def setZoomLevel(beamline, level, token=None):
    serverProxy = getServerProxy(beamline, token)
    return serverProxy.set_zoom_level(level)


def getAvailableZoomLevels(beamline, token=None):
    serverProxy = getServerProxy(beamline, token)
    return serverProxy.get_available_zoom_levels()


def getBackLightLevel(beamline, token=None):
    serverProxy = getServerProxy(beamline, token)
    return serverProxy.get_back_light_level()


def setBackLightLevel(beamline, level, token=None):
    if beamline == "id30a2":
        return False
    serverProxy = getServerProxy(beamline, token)
    returnValue = serverProxy.set_back_light_level(level)
    return returnValue


def getFrontLightLevel(beamline, token=None):
    serverProxy = getServerProxy(beamline, token)
    return serverProxy.get_front_light_level()


def setFrontLightLevel(beamline, level, token=None):
    if beamline == "id30a2":
        return False
    serverProxy = getServerProxy(beamline, token)
    return serverProxy.set_front_light_level(level)


def getPlateMode(beamline, token=None):
    # serverProxy = getServerProxy(beamline, token)
    # return serverProxy.get_plate_mode()
    return False


def getCurrentCdCrystalId(beamline, token=None):
    if beamline == "id30a2":
        crystalId = "479b51a0-9d40-45a3-9336-60d4d6ba67df"
    else:
        serverProxy = getServerProxy(beamline, token)
        crystalId = serverProxy.get_current_cd_crystal_id()
    return crystalId


def centreBeam(beamline, token=None):
    serverProxy = getServerProxy(beamline, token)
    serverProxy.centre_beam()


def getResolutionLimits(beamline, token=None):
    lowResolution = None
    highResolution = None
    logger = workflow_logging.getLogger()
    serverProxy = getServerProxy(beamline, token)
    try:
        lowResolution, highResolution = serverProxy.get_resolution_limits()
        lowResolution = round(lowResolution + 0.005, 2)
        highResolution = round(highResolution - 0.005, 2)
    except BaseException:
        logger.warning("Cannot read resolution limits on beamline {0}".format(beamline))
    return lowResolution, highResolution


def limitResolutiom(resolution, lowResolution, highResolution):
    logger = workflow_logging.getLogger()
    # Limit the resolution if necessary
    if lowResolution is None:
        logger.warning("Low resolution limit not configured!")
    elif resolution > lowResolution:
        logger.warning("Too low data collection resolution requested!")
        logger.warning("Requested resolution: {0} A".format(resolution))
        resolution = lowResolution
        logger.warning("Requested limited to: {0} A".format(resolution))
    if highResolution is None:
        logger.warning("High resolution limit not configured!")
    elif resolution < highResolution:
        logger.warning("Too high data collection resolution requested!")
        logger.warning("Requested resolution: {0} A".format(resolution))
        resolution = highResolution
        logger.warning("Requested limited to: {0} A".format(resolution))
    return resolution


def take_twelve_snapshots(beamline, snapshot_directory, token=None):
    proxy = getServerProxy(beamline, token)
    # proxy.save_twelve_snapshots_script(snapshot_directory)
    proxy.save_twelve_snapshots_script(snapshot_directory)


def clearISPyBClientGroupId(beamline, token=None):
    proxy = getServerProxy(beamline, token)
    proxy.clearISPyBClientGroupId()
