"""
Workflow library module for connecting to ISPyB
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013"
__updated__ = "2014-01-25"

import os
import json
import time

from bes.workflow_lib import grid
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import edna_kernel
from bes.workflow_lib import workflow_logging

from bes.workflow_lib.edna_kernel import EDFactoryPluginStatic

from XSDataCommon import XSDataInteger
from XSDataCommon import XSDataString
from XSDataCommon import XSDataImage
from XSDataCommon import XSDataResult
from XSDataCommon import XSDataFile
from XSDataCommon import XSDataDouble

EDFactoryPluginStatic.loadModule("XSDataISPyBv1_4")
from XSDataISPyBv1_4 import XSDataISPyBWorkflow  # noqa E402
from XSDataISPyBv1_4 import XSDataInputISPyBStoreWorkflow  # noqa E402
from XSDataISPyBv1_4 import XSDataInputRetrieveDataCollection  # noqa E402
from XSDataISPyBv1_4 import XSDataInputISPyBUpdateWorkflowStatus  # noqa E402
from XSDataISPyBv1_4 import XSDataInputISPyBGroupDataCollections  # noqa E402
from XSDataISPyBv1_4 import (  # noqa E402
    XSDataInputISPyBUpdateDataCollectionGroupWorkflowId,
)
from XSDataISPyBv1_4 import XSDataInputISPyBDataCollectionPosition  # noqa E402
from XSDataISPyBv1_4 import XSDataInputISPyBSetDataCollectionsPositions  # noqa E402
from XSDataISPyBv1_4 import XSDataISPyBSamplePosition  # noqa E402
from XSDataISPyBv1_4 import XSDataInputISPyBSetImagesPositions  # noqa E402
from XSDataISPyBv1_4 import XSDataISPyBImagePosition  # noqa E402
from XSDataISPyBv1_4 import XSDataInputISPyBStoreGridInfo  # noqa E402
from XSDataISPyBv1_4 import XSDataInputISPyBStoreWorkflowMesh  # noqa E402
from XSDataISPyBv1_4 import XSDataInputISPyBStoreMotorPosition  # noqa E402
from XSDataISPyBv1_4 import XSDataInputGetSampleInformation  # noqa E402
from XSDataISPyBv1_4 import XSDataInputStoreListOfImageQualityIndicators  # noqa E402
from XSDataISPyBv1_4 import XSDataISPyBImageQualityIndicators  # noqa E402
from XSDataISPyBv1_4 import XSDataInputUpdateSnapshots  # noqa E402

from EDHandlerESRFPyarchv1_0 import EDHandlerESRFPyarchv1_0  # noqa E402


def storeOrUpdateWorkflow(
    beamline,
    workflowTitle="",
    workflowType="Undefined",
    workflowPyarchLogFile=None,
    workflowLogFile=None,
    pyarchHtmlDir="",
    workflowWorkingDir="",
    workflowId=None,
):
    edna_kernel.initSimulation(beamline)

    if workflowType in [
        "Undefined",
        "BioSAXS Post Processing",
        "EnhancedCharacterisation",
        "LineScan",
        "MeshScan",
        "Dehydration",
        "KappaReorientation",
        "BurnStrategy",
        "XrayCentering",
        "DiffractionTomography",
        "TroubleShooting",
        "VisualReorientation",
        "HelicalCharacterisation",
        "GroupedProcessing",
        "MXPressE",
        "MXPressO",
        "MXPressL",
        "MXScore",
        "MXPressI",
        "MXPressM",
        "MXPressA",
        "CollectAndSpectra",
        "LowDoseDC",
        "MXPressH",
        "MXPressP",
        "MXPressP_SAD",
        "MXPressR",
        "MXPressR_180",
        "MXPressR_dehydration",
        "MXPressF",
        "MXScore",
        "MeshAndCollect",
        "MeshAndCollectFromFile",
    ]:
        ispyb_workflow_type = workflowType
    else:
        ispyb_workflow_type = "Undefined"
    xsDataISPyBWorkflow = XSDataISPyBWorkflow()
    xsDataISPyBWorkflow.status = XSDataString("Started")
    xsDataISPyBWorkflow.workflowTitle = XSDataString(workflowTitle)
    xsDataISPyBWorkflow.workflowType = XSDataString(ispyb_workflow_type)
    if workflowPyarchLogFile is not None:
        xsDataISPyBWorkflow.logFilePath = XSDataString(workflowPyarchLogFile)
    elif workflowLogFile is not None:
        xsDataISPyBWorkflow.logFilePath = XSDataString(workflowLogFile)
    if pyarchHtmlDir is not None:
        xsDataISPyBWorkflow.resultFilePath = XSDataString(pyarchHtmlDir)
    if workflowId is not None:
        xsDataISPyBWorkflow.workflowId = XSDataInteger(workflowId)

    xsDataInputISPyBStoreWorkflow = XSDataInputISPyBStoreWorkflow()
    xsDataInputISPyBStoreWorkflow.workflow = xsDataISPyBWorkflow

    # print xsDataInputISPyBStoreWorkflow.marshal()
    # print os.environ["EDNA_SITE"]
    workflowId = -1
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginISPyBStoreWorkflowv1_4",
        xsDataInputISPyBStoreWorkflow,
        workflowWorkingDir,
    )
    if xsDataResult is not None:
        if xsDataResult.workflowId is not None:
            workflowId = xsDataResult.workflowId.value
    # print "Workflow Id: %d" % workflow_id
    return workflowId


def getFluxBeamsize(
    beamline,
    mxv1StrategyResult,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    workflow_working_dir,
):
    edna_kernel.initSimulation(beamline)
    listImagePath = edna_mxv1.getListImagePath(
        mxv1StrategyResult, directory, run_number, expTypePrefix, prefix, suffix
    )
    if listImagePath != []:
        imagePath = listImagePath[0]
    else:
        imagePath = None
    return getFluxBeamsizeFromImage(
        imagePath,
        workflow_working_dir,
    )


def getFluxBeamsizeFromImage(
    imagePath,
    workflow_working_dir,
):
    logger = workflow_logging.getLogger()
    flux = None
    flux100 = None
    fBeamSizeAtSampleX = None
    fBeamSizeAtSampleY = None
    if imagePath is not None:
        strPluginISPyBRetrieveDataCollection = "EDPluginISPyBRetrieveDataCollectionv1_4"
        # Here we try to retrieve the flux from ISPyB
        xsDataInputRetrieveDataCollection = XSDataInputRetrieveDataCollection()
        xsDataInputRetrieveDataCollection.setImage(
            XSDataImage((XSDataString(imagePath)))
        )
        xsDataResultRetrieveDataCollection, ednaLogPath = edna_kernel.executeEdnaPlugin(
            strPluginISPyBRetrieveDataCollection,
            xsDataInputRetrieveDataCollection,
            workflow_working_dir,
        )

        if xsDataResultRetrieveDataCollection is not None:
            xsDataISPyBDataCollection = (
                xsDataResultRetrieveDataCollection.dataCollection
            )
            if xsDataISPyBDataCollection is not None:
                # print xsDataISPyBDataCollection.marshal()
                flux = xsDataISPyBDataCollection.flux_end
                if flux is not None:
                    transmission = xsDataISPyBDataCollection.transmission
                    logger.info(
                        "ISPyB reports flux to be: {0:.2e} photons/sec for transmission of {1} %".format(
                            flux, transmission
                        )
                    )
                    # Correct flux for transmission
                    flux100 = 100.0 / transmission * flux
                    logger.info(
                        "Flux normalised to 100% transmission: {0:.2e} photons/sec".format(
                            flux100
                        )
                    )
                fBeamSizeAtSampleX = xsDataISPyBDataCollection.beamSizeAtSampleX
                if fBeamSizeAtSampleX is not None:
                    logger.info(
                        "ISPyB reports beamsize X to be: %.3f mm" % fBeamSizeAtSampleX
                    )
                fBeamSizeAtSampleY = xsDataISPyBDataCollection.beamSizeAtSampleY
                if fBeamSizeAtSampleY is not None:
                    logger.info(
                        "ISPyB reports beamsize Y to be: %.3f mm" % fBeamSizeAtSampleY
                    )

    return {
        "flux": flux,
        "flux100": flux100,
        "beamSizeAtSampleX": fBeamSizeAtSampleX,
        "beamSizeAtSampleY": fBeamSizeAtSampleY,
    }


def getMeshBeamsize(beamline, directory, firstImagePath, workflow_working_dir):
    edna_kernel.initSimulation(beamline)
    logger = workflow_logging.getLogger()
    strPluginISPyBRetrieveDataCollection = "EDPluginISPyBRetrieveDataCollectionv1_4"
    bFoundValidBeamSize = False
    flux = None
    flux100 = None
    if firstImagePath != "":
        # We just need one image
        if beamline in [
            "id23eh1",
            "id23eh2",
            "id30a3",
            "id30b",
        ] and not firstImagePath.endswith(".h5"):
            firstImagePath = firstImagePath.replace(".cbf", ".h5")
            logger.debug("First image path converted for id30a3: %s" % firstImagePath)
        else:
            logger.debug("First image path: %s" % firstImagePath)
        xsDataInputRetrieveDataCollection = XSDataInputRetrieveDataCollection()
        xsDataInputRetrieveDataCollection.setImage(
            XSDataImage((XSDataString(firstImagePath)))
        )
        xsDataResultRetrieveDataCollection, ednaLogPath = edna_kernel.executeEdnaPlugin(
            strPluginISPyBRetrieveDataCollection,
            xsDataInputRetrieveDataCollection,
            workflow_working_dir,
        )

        if xsDataResultRetrieveDataCollection is not None:
            xsDataISPyBDataCollection = (
                xsDataResultRetrieveDataCollection.dataCollection
            )
            if xsDataISPyBDataCollection is not None:
                flux = xsDataISPyBDataCollection.flux_end
                if flux is not None:
                    transmission = xsDataISPyBDataCollection.transmission
                    logger.info(
                        "ISPyB reports flux to be: {0:.2e} photons/sec for transmission of {1} %".format(
                            flux, transmission
                        )
                    )
                    # Correct flux for transmission
                    flux100 = 100.0 / transmission * flux
                    logger.info(
                        "Flux normalised to 100% transmission: {0:.2e} photons/sec".format(
                            flux100
                        )
                    )
                fBeamSizeAtSampleX = xsDataISPyBDataCollection.beamSizeAtSampleX
                if fBeamSizeAtSampleX is not None:
                    logger.info(
                        "ISPyB reports beamsize X to be: %.3f mm" % fBeamSizeAtSampleX
                    )
                    bFoundValidBeamSize = True
                fBeamSizeAtSampleY = xsDataISPyBDataCollection.beamSizeAtSampleY
                if fBeamSizeAtSampleY is not None:
                    logger.info(
                        "ISPyB reports beamsize Y to be: %.3f mm" % fBeamSizeAtSampleY
                    )
                    bFoundValidBeamSize = True
                detectorDistance = xsDataISPyBDataCollection.detectorDistance
    if not bFoundValidBeamSize:
        logger.error("No valid beamsize could be retrieved from ISPyB!")
        if beamline == "simulator" or beamline == "id14eh1":
            fBeamSizeAtSampleX = 0.05
            fBeamSizeAtSampleY = 0.03
        elif beamline in ["id23eh2", "id30a2"]:
            fBeamSizeAtSampleX = 0.08
            fBeamSizeAtSampleY = 0.05
        else:
            raise Exception("ERROR! No beamsize could be retrieved from ISPyB!")

    return {
        "flux": flux,
        "flux100": flux100,
        "beamSizeAtSampleX": fBeamSizeAtSampleX,
        "beamSizeAtSampleY": fBeamSizeAtSampleY,
        "detectorDistance": detectorDistance,
    }


def setWorkflowStatus(beamline, workflow_working_dir, workflow_id, _strStatus):
    edna_kernel.initSimulation(beamline)
    if workflow_id is not None and workflow_id > 0:
        logger = workflow_logging.getLogger()
        logger.debug("Setting status in ISPyB to %s" % _strStatus)
        xsDataInputISPyBUpdateWorkflowStatus = XSDataInputISPyBUpdateWorkflowStatus()
        xsDataInputISPyBUpdateWorkflowStatus.workflowId = XSDataInteger(workflow_id)
        xsDataInputISPyBUpdateWorkflowStatus.newStatus = XSDataString(_strStatus)
        xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
            "EDPluginISPyBUpdateWorkflowStatusv1_4",
            xsDataInputISPyBUpdateWorkflowStatus,
            workflow_working_dir,
        )


def updateDataCollectionGroupWorkflowId(
    beamline,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    firstImagePath,
    workflow_id,
    workflow_working_dir,
):
    edna_kernel.initSimulation(beamline)
    strFileLocation = os.path.dirname(firstImagePath)
    strFileName = os.path.basename(firstImagePath)
    noIterations = 0
    iDataCollectionGroupId = -1
    while (noIterations < 5) and (iDataCollectionGroupId == -1):
        xsDataInputISPyBUpdateDataCollectionGroupWorkflowId = (
            XSDataInputISPyBUpdateDataCollectionGroupWorkflowId()
        )
        xsDataInputISPyBUpdateDataCollectionGroupWorkflowId.workflowId = XSDataInteger(
            workflow_id
        )
        xsDataInputISPyBUpdateDataCollectionGroupWorkflowId.fileLocation = XSDataString(
            strFileLocation
        )
        xsDataInputISPyBUpdateDataCollectionGroupWorkflowId.fileName = XSDataString(
            strFileName
        )
        # print xsDataInputISPyBUpdateDataCollectionGroupWorkflowId.marshal()
        xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
            "EDPluginISPyBUpdateDataCollectionGroupWorkflowIdv1_4",
            xsDataInputISPyBUpdateDataCollectionGroupWorkflowId,
            workflow_working_dir,
        )
        iDataCollectionGroupId = -1
        if xsDataResult is not None:
            if xsDataResult.dataCollectionGroupId is not None:
                iDataCollectionGroupId = xsDataResult.dataCollectionGroupId.value
        if iDataCollectionGroupId == -1:
            time.sleep(5)
        noIterations += 1
    return iDataCollectionGroupId


def groupDataCollections(
    beamline,
    mxv1StrategyResult,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    workflow_working_dir,
    iDataCollectionGroupId,
):
    edna_kernel.initSimulation(beamline)
    listImagePath = edna_mxv1.getListImagePath(
        mxv1StrategyResult,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        bIgnoreBurnImages=False,
        bFirstImageOnly=True,
    )
    xsDataInputISPyBGroupDataCollections = XSDataInputISPyBGroupDataCollections()
    xsDataInputISPyBGroupDataCollections.dataCollectionGroupId = XSDataInteger(
        iDataCollectionGroupId
    )
    for strImagePath in listImagePath:
        xsDataInputISPyBGroupDataCollections.addFilePath(XSDataString(strImagePath))
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginISPyBGroupDataCollectionsv1_4",
        xsDataInputISPyBGroupDataCollections,
        workflow_working_dir,
    )
    listDataCollectionId = []
    if xsDataResult is not None and type(xsDataResult) is not type(XSDataResult()):
        if xsDataResult.dataCollectionId is not None:
            for xsDataInteger in xsDataResult.dataCollectionId:
                listDataCollectionId.append(xsDataInteger.value)
    return listDataCollectionId


def groupMeshDataCollections(
    beamline, directory, meshPositions, workflow_working_dir, iDataCollectionGroupId
):
    edna_kernel.initSimulation(beamline)
    listImagePath = grid.getMeshListImagePath(directory, meshPositions)
    xsDataInputISPyBGroupDataCollections = XSDataInputISPyBGroupDataCollections()
    xsDataInputISPyBGroupDataCollections.dataCollectionGroupId = XSDataInteger(
        iDataCollectionGroupId
    )
    for strImagePath in listImagePath:
        xsDataInputISPyBGroupDataCollections.addFilePath(XSDataString(strImagePath))
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginISPyBGroupDataCollectionsv1_4",
        xsDataInputISPyBGroupDataCollections,
        workflow_working_dir,
    )
    listDataCollectionId = []
    if xsDataResult is not None and type(xsDataResult) is not type(XSDataResult()):
        if xsDataResult.dataCollectionId is not None:
            for xsDataInteger in xsDataResult.dataCollectionId:
                listDataCollectionId.append(xsDataInteger.value)
    return listDataCollectionId


def groupMeshDataCollectionsFromQueueEntry(
    beamline, directory, listImagePath, workflow_working_dir, iDataCollectionGroupId
):
    edna_kernel.initSimulation(beamline)
    xsDataInputISPyBGroupDataCollections = XSDataInputISPyBGroupDataCollections()
    xsDataInputISPyBGroupDataCollections.dataCollectionGroupId = XSDataInteger(
        iDataCollectionGroupId
    )
    for strImagePath in listImagePath:
        xsDataInputISPyBGroupDataCollections.addFilePath(XSDataString(strImagePath))
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginISPyBGroupDataCollectionsv1_4",
        xsDataInputISPyBGroupDataCollections,
        workflow_working_dir,
    )
    listDataCollectionId = []
    if xsDataResult is not None and type(xsDataResult) is not type(XSDataResult()):
        if xsDataResult.dataCollectionId is not None:
            for xsDataInteger in xsDataResult.dataCollectionId:
                listDataCollectionId.append(xsDataInteger.value)
    return listDataCollectionId


def _create_sample_position(dict_position):
    """
    Creates an XSDataISPyBSamplePosition instance with the motor mositions given
    in the dictionary dict_position
    """
    xsDataISPyBSamplePosition = XSDataISPyBSamplePosition()
    if "indexY" in dict_position:
        xsDataISPyBSamplePosition.gridIndexY = XSDataInteger(dict_position["indexY"])
    if "indexZ" in dict_position:
        xsDataISPyBSamplePosition.gridIndexZ = XSDataInteger(dict_position["indexZ"])
    xsDataISPyBSamplePosition.phiY = XSDataDouble(dict_position["phiy"])
    xsDataISPyBSamplePosition.phiZ = XSDataDouble(dict_position["phiz"])
    xsDataISPyBSamplePosition.sampX = XSDataDouble(dict_position["sampx"])
    xsDataISPyBSamplePosition.sampY = XSDataDouble(dict_position["sampy"])
    if "kappa" in dict_position:
        xsDataISPyBSamplePosition.kappa = XSDataDouble(dict_position["kappa"])
        xsDataISPyBSamplePosition.phi = XSDataDouble(dict_position["phi"])
    xsDataISPyBSamplePosition.omega = XSDataDouble(dict_position["omega"])
    xsDataISPyBSamplePosition.phiX = XSDataDouble(dict_position["focus"])
    return xsDataISPyBSamplePosition


def store_positions(beamline, directory, meshPositions, workflow_working_dir):
    """
    This method uses the EDNA plugin "EDPluginISPyBSetImagesPositionsv1_4"
    for uploading the mesh motor positions to ISPyB
    """
    logger = workflow_logging.getLogger()
    edna_kernel.initSimulation(beamline)
    if isinstance(meshPositions, str):
        meshPositions = json.loads(meshPositions)
    listImageCreation = []
    xsDataInputISPyBSetImagesPositions = XSDataInputISPyBSetImagesPositions()
    xsDataInputStoreListOfImageQualityIndicators = (
        XSDataInputStoreListOfImageQualityIndicators()
    )
    for position in meshPositions:
        strImageName = position["imageName"]
        strImagePath = os.path.join(directory, strImageName)
        xsDataISPyBImageQualityIndicators = XSDataISPyBImageQualityIndicators()
        xsDataISPyBImageQualityIndicators.image = XSDataImage(
            XSDataString(strImagePath)
        )
        if "goodBraggCandidates" in position:
            xsDataISPyBImageQualityIndicators.goodBraggCandidates = XSDataInteger(
                position["goodBraggCandidates"]
            )
        if "iceRings" in position:
            xsDataISPyBImageQualityIndicators.iceRings = XSDataInteger(
                position["iceRings"]
            )
        if "dozor_score" in position:
            xsDataISPyBImageQualityIndicators.dozor_score = XSDataDouble(
                position["dozor_score"]
            )
        if "totalIntegratedSignal" in position:
            xsDataISPyBImageQualityIndicators.totalIntegratedSignal = XSDataDouble(
                position["totalIntegratedSignal"]
            )
        if "inResTotal" in position:
            xsDataISPyBImageQualityIndicators.inResTotal = XSDataInteger(
                position["inResTotal"]
            )
        if "inResolutionOvrlSpots" in position:
            xsDataISPyBImageQualityIndicators.inResolutionOvrlSpots = XSDataInteger(
                position["inResolutionOvrlSpots"]
            )
        if "maxUnitCell" in position:
            xsDataISPyBImageQualityIndicators.maxUnitCell = XSDataDouble(
                position["maxUnitCell"]
            )
        if "method1Res" in position:
            xsDataISPyBImageQualityIndicators.method1Res = XSDataDouble(
                position["method1Res"]
            )
        if "method2Res" in position:
            xsDataISPyBImageQualityIndicators.method2Res = XSDataDouble(
                position["method2Res"]
            )
        if "pctSaturationTop50Peaks" in position:
            xsDataISPyBImageQualityIndicators.pctSaturationTop50Peaks = XSDataDouble(
                position["pctSaturationTop50Peaks"]
            )
        if "spotTotal" in position:
            xsDataISPyBImageQualityIndicators.spotTotal = XSDataInteger(
                position["spotTotal"]
            )
        xsDataInputStoreListOfImageQualityIndicators.addImageQualityIndicators(
            xsDataISPyBImageQualityIndicators
        )
        #
        xsDataISPyBImagePosition = XSDataISPyBImagePosition()
        xsDataISPyBImagePosition.fileName = XSDataFile(XSDataString(strImagePath))
        # Try to create path to pyarch
        strPyarchDirname = EDHandlerESRFPyarchv1_0.createPyarchFilePath(directory)
        if strPyarchDirname is not None:
            strImageNameWithoutExt = os.path.splitext(strImageName)[0]
            strJpegFileFullPath = os.path.join(
                strPyarchDirname, strImageNameWithoutExt + ".jpeg"
            )
            strThumbnailFileFullPath = os.path.join(
                strPyarchDirname, strImageNameWithoutExt + ".thumb.jpeg"
            )
            xsDataISPyBImagePosition.jpegFileFullPath = XSDataFile(
                XSDataString(strJpegFileFullPath)
            )
            xsDataISPyBImagePosition.jpegThumbnailFileFullPath = XSDataFile(
                XSDataString(strThumbnailFileFullPath)
            )
        xsDataISPyBSamplePosition = _create_sample_position(position)
        xsDataISPyBImagePosition.position = xsDataISPyBSamplePosition
        xsDataInputISPyBSetImagesPositions.addImagePosition(xsDataISPyBImagePosition)
    #     logger.debug(xsDataInputISPyBSetImagesPositions.marshal())
    xsDataResultISPyBSetImagesPositions, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginISPyBSetImagesPositionsv1_4",
        xsDataInputISPyBSetImagesPositions,
        workflow_working_dir,
    )
    time.sleep(1)
    (
        xsDataResultStoreListOfImageQualityIndicators,
        ednaLogPath,
    ) = edna_kernel.executeEdnaPlugin(
        "EDPluginISPyBStoreListOfImageQualityIndicatorsv1_4",
        xsDataInputStoreListOfImageQualityIndicators,
        workflow_working_dir,
    )
    if xsDataResultISPyBSetImagesPositions is not None and type(
        xsDataResultISPyBSetImagesPositions
    ) is not type(XSDataResult()):
        #         logger.debug(xsDataResultISPyBSetImagesPositions.marshal())
        for (
            xsDataISPyBImageCreation
        ) in xsDataResultISPyBSetImagesPositions.imageCreation:
            dictImageCreation = {}
            dictImageCreation["fileLocation"] = (
                xsDataISPyBImageCreation.fileLocation.value
            )
            dictImageCreation["fileName"] = xsDataISPyBImageCreation.fileName.value
            if xsDataISPyBImageCreation.imageId is None:
                logger.debug(
                    "No imageId for image %s/%s"
                    % (
                        xsDataISPyBImageCreation.fileLocation.value,
                        xsDataISPyBImageCreation.fileName.value,
                    )
                )
            else:
                dictImageCreation["imageId"] = xsDataISPyBImageCreation.imageId.value
            dictImageCreation["isCreated"] = xsDataISPyBImageCreation.isCreated.value
            listImageCreation.append(dictImageCreation)
    return listImageCreation


def store_workflow_mesh(
    beamline,
    workflow_id,
    grid_info,
    dict_html,
    best_position,
    best_image_id,
    dict_statistics,
    workflow_working_dir,
):
    edna_kernel.initSimulation(beamline)
    if isinstance(grid_info, str):
        grid_info = json.loads(grid_info)
    xsDataInputISPyBStoreWorkflowMesh = XSDataInputISPyBStoreWorkflowMesh()
    xsDataInputISPyBStoreWorkflowMesh.workflowId = XSDataInteger(workflow_id)
    if best_image_id is not None:
        xsDataInputISPyBStoreWorkflowMesh.bestImageId = XSDataInteger(best_image_id)
    if dict_html != {}:
        cartography_path = os.path.join(
            dict_html["html_directory_path"], dict_html["plot_file_name"]
        )
        xsDataInputISPyBStoreWorkflowMesh.cartographyPath = XSDataString(
            cartography_path
        )
    if "mesh_v1" in dict_statistics:
        xsDataInputISPyBStoreWorkflowMesh.value1 = XSDataDouble(
            dict_statistics["mesh_v1"]
        )
    if "mesh_v2" in dict_statistics:
        xsDataInputISPyBStoreWorkflowMesh.value2 = XSDataDouble(
            dict_statistics["mesh_v2"]
        )
    #     if "mesh_n" in dict_statistics:
    #         xsDataInputISPyBStoreWorkflowMesh.value3 = XSDataDouble(dict_statistics["mesh_n"])
    xsDataResultISpyBStoreWorkflowMesh, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginISPyBStoreWorkflowMeshv1_4",
        xsDataInputISPyBStoreWorkflowMesh,
        workflow_working_dir,
    )
    if xsDataResultISpyBStoreWorkflowMesh.workflowMeshId is not None:
        workflow_mesh_id = xsDataResultISpyBStoreWorkflowMesh.workflowMeshId.value
        xsDataInputISPyBStoreGridInfo = XSDataInputISPyBStoreGridInfo()
        xsDataInputISPyBStoreGridInfo.workflowMeshId = XSDataInteger(workflow_mesh_id)
        xsDataInputISPyBStoreGridInfo.dx_mm = XSDataDouble(grid_info["dx_mm"])
        xsDataInputISPyBStoreGridInfo.dy_mm = XSDataDouble(grid_info["dy_mm"])
        xsDataInputISPyBStoreGridInfo.xOffset = XSDataDouble(grid_info["x1"])
        xsDataInputISPyBStoreGridInfo.yOffset = XSDataDouble(grid_info["y1"])
        xsDataInputISPyBStoreGridInfo.steps_x = XSDataInteger(grid_info["steps_x"])
        xsDataInputISPyBStoreGridInfo.steps_y = XSDataInteger(grid_info["steps_y"])
        if "angle" in grid_info:
            xsDataInputISPyBStoreGridInfo.meshAngle = XSDataDouble(grid_info["angle"])
        else:
            xsDataInputISPyBStoreGridInfo.meshAngle = XSDataDouble(0.0)
        xsDataResultISpyBStoreGridInfo, ednaLogPath = edna_kernel.executeEdnaPlugin(
            "EDPluginISPyBStoreGridInfov1_4",
            xsDataInputISPyBStoreGridInfo,
            workflow_working_dir,
        )


def get_sample_information(beamline, lims_id, workflow_working_dir):
    edna_kernel.initSimulation(beamline)
    logger = workflow_logging.getLogger()
    logger.debug("ISPyB get_sample_information, lims id: %d" % lims_id)
    dict_sample_info = {}
    if lims_id is not None or lims_id > 0:
        xsDataInputISPyBGetSampleInformation = XSDataInputGetSampleInformation()
        xsDataInputISPyBGetSampleInformation.sampleId = XSDataInteger(lims_id)
        (
            xsDataResultISPyBGetSampleInformation,
            ednaLogPath,
        ) = edna_kernel.executeEdnaPlugin(
            "EDPluginISPyBGetSampleInformationv1_4",
            xsDataInputISPyBGetSampleInformation,
            workflow_working_dir,
        )
        logger.debug("Result: %s" % xsDataResultISPyBGetSampleInformation.marshal())
        # Sample name
        if xsDataResultISPyBGetSampleInformation.sampleName:
            dict_sample_info["sampleName"] = (
                xsDataResultISPyBGetSampleInformation.sampleName.value
            )
        # Protein name
        if xsDataResultISPyBGetSampleInformation.proteinAcronym:
            dict_sample_info["proteinAcronym"] = (
                xsDataResultISPyBGetSampleInformation.proteinAcronym.value
            )
        diffractionPlan = xsDataResultISPyBGetSampleInformation.diffractionPlan
        if diffractionPlan is not None:
            # Experiment kind
            if diffractionPlan.experimentKind is not None:
                dict_sample_info["experimentKind"] = (
                    diffractionPlan.experimentKind.value
                )

            # Forced space group
            if diffractionPlan.forcedSpaceGroup is not None:
                dict_sample_info["forcedSpaceGroup"] = (
                    diffractionPlan.forcedSpaceGroup.value
                )

            # Axis range
            if diffractionPlan.axisRange is not None:
                if diffractionPlan.axisRange.value:
                    dict_sample_info["axisRange"] = diffractionPlan.axisRange.value
                else:
                    logger.warning("ISPyB axis range is set to 0")

            # Aimed resolution
            if diffractionPlan.aimedResolution is not None:
                if diffractionPlan.aimedResolution.value > 0.1:
                    dict_sample_info["aimedResolution"] = (
                        diffractionPlan.aimedResolution.value
                    )

            # Observed resolution
            if diffractionPlan.observedResolution is not None:
                if diffractionPlan.observedResolution.value > 0.1:
                    dict_sample_info["observedResolution"] = (
                        diffractionPlan.observedResolution.value
                    )

            # Maximal resolution
            if diffractionPlan.maximalResolution is not None:
                if diffractionPlan.maximalResolution.value > 0.1:
                    dict_sample_info["maximalResolution"] = (
                        diffractionPlan.maximalResolution.value
                    )

            # Required resolution
            if diffractionPlan.requiredResolution is not None:
                if diffractionPlan.requiredResolution.value > 0.1:
                    dict_sample_info["requiredResolution"] = (
                        diffractionPlan.requiredResolution.value
                    )

            # Required completeness
            if diffractionPlan.requiredCompleteness is not None:
                requiredCompleteness = diffractionPlan.requiredCompleteness.value
                if requiredCompleteness > 0.1:
                    # Check if the user has entered the completeness in percent...
                    requiredCompletenessNew = None
                    if requiredCompleteness > 50.0 and requiredCompleteness <= 100.0:
                        requiredCompletenessNew = requiredCompleteness / 100.0
                    elif requiredCompleteness > 100.0:
                        requiredCompletenessNew = 1.0
                    if requiredCompletenessNew is not None:
                        logger.warning(
                            "Required completeness has been changed from {0} to {1}!".format(
                                requiredCompleteness, requiredCompletenessNew
                            )
                        )
                        requiredCompleteness = requiredCompletenessNew
                    dict_sample_info["requiredCompleteness"] = requiredCompleteness

            # Aimed completeness
            if diffractionPlan.aimedCompleteness is not None:
                aimedCompleteness = diffractionPlan.aimedCompleteness.value
                if aimedCompleteness > 0.1:
                    # Check if the user has entered the completeness in percent...
                    aimedCompletenessNew = None
                    if aimedCompleteness > 50.0 and aimedCompleteness <= 100.0:
                        aimedCompletenessNew = aimedCompleteness / 100.0
                    elif aimedCompleteness > 100.0:
                        aimedCompletenessNew = 1.0
                    if aimedCompletenessNew is not None:
                        logger.warning(
                            "Aimed completeness has been changed from {0} to {1}!".format(
                                aimedCompleteness, aimedCompletenessNew
                            )
                        )
                        aimedCompleteness = aimedCompletenessNew
                    dict_sample_info["aimedCompleteness"] = aimedCompleteness

            # Aimed multiplicity
            if diffractionPlan.aimedMultiplicity is not None:
                if diffractionPlan.aimedMultiplicity.value > 0.1:
                    dict_sample_info["aimedMultiplicity"] = (
                        diffractionPlan.aimedMultiplicity.value
                    )

            # Required multiplicity
            if diffractionPlan.requiredMultiplicity is not None:
                if diffractionPlan.requiredMultiplicity.value > 0.1:
                    dict_sample_info["requiredMultiplicity"] = (
                        diffractionPlan.requiredMultiplicity.value
                    )

            # Radiation sensitivity
            if diffractionPlan.radiationSensitivity is not None:
                if diffractionPlan.radiationSensitivity.value > 0.1:
                    dict_sample_info["radiationSensitivity"] = (
                        diffractionPlan.radiationSensitivity.value
                    )

            # Number of positions
            if diffractionPlan.numberOfPositions is not None:
                if diffractionPlan.numberOfPositions.value > 0:
                    dict_sample_info["numberOfPositions"] = (
                        diffractionPlan.numberOfPositions.value
                    )

            # Minimum oscillation width
            if diffractionPlan.minOscWidth is not None:
                if diffractionPlan.minOscWidth.value > 0.01:
                    dict_sample_info["minOscWidth"] = diffractionPlan.minOscWidth.value

            # Preferred beam diameter
            if diffractionPlan.preferredBeamDiameter is not None:
                if diffractionPlan.preferredBeamDiameter.value > 0.001:
                    dict_sample_info["preferredBeamDiameter"] = (
                        diffractionPlan.preferredBeamDiameter.value
                    )

            # Crystal space group and cell
            if xsDataResultISPyBGetSampleInformation.crystalSpaceGroup is not None:
                dict_sample_info["crystalSpaceGroup"] = (
                    xsDataResultISPyBGetSampleInformation.crystalSpaceGroup.value
                )
            for name in ["A", "B", "C", "Alpha", "Beta", "Gamma"]:
                cell_attribute = "cell" + name
                xs_data_attribute = getattr(
                    xsDataResultISPyBGetSampleInformation, cell_attribute
                )
                if xs_data_attribute is not None:
                    value = xs_data_attribute.value
                    if value:
                        dict_sample_info[cell_attribute] = value
                    else:
                        logger.warning(
                            f"ISPyB attribute {cell_attribute} is set to zero"
                        )
    return dict_sample_info


def updateSnapshot(
    beamline,
    directory,
    snapShotFilePath,
    grid_info,
    meshPositions,
    workflow_working_dir,
):
    # Update path for each line in grid scan:
    bReverse = True
    listImagePath = []
    nx = int(grid_info["steps_x"])
    if nx == 1:
        listImagePath.append(os.path.join(directory, meshPositions[0]["imageName"]))
    else:
        # Find out if we are going reverse or not
        for position in meshPositions:
            if position["index"] == 0:
                if position["indexY"] == 0:
                    bReverse = False
                else:
                    bReverse = True
                break
        for position in meshPositions:
            if not bReverse and position["indexY"] == 0:
                listImagePath.append(os.path.join(directory, position["imageName"]))
            elif bReverse and position["indexY"] == nx - 1:
                listImagePath.append(os.path.join(directory, position["imageName"]))
    for imagePath in listImagePath:
        xsDataInputUpdateSnapshot = XSDataInputUpdateSnapshots()
        xsDataInputUpdateSnapshot.image = XSDataImage(XSDataString(imagePath))
        xsDataInputUpdateSnapshot.xtalSnapshotFullPath1 = XSDataFile(
            XSDataString(snapShotFilePath)
        )
        xsDataResultISPyBUpdateSnapshots, ednaLogPath = edna_kernel.executeEdnaPlugin(
            "EDPluginISPyBUpdateSnapshotsv1_4",
            xsDataInputUpdateSnapshot,
            workflow_working_dir,
        )


def updateGroupWorkflowId(inData, listImagePath, logger):
    firstImagePath = listImagePath[0]
    if inData["beamline"] in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
        firstImagePath = os.path.splitext(firstImagePath)[0] + ".h5"
    logger.info("ISPyB path to first image: {0}".format(firstImagePath))
    dataCollectionGroupId = updateDataCollectionGroupWorkflowId(
        inData["beamline"],
        inData["directory"],
        inData["run_number"],
        inData["expTypePrefix"],
        inData["prefix"],
        inData["suffix"],
        firstImagePath,
        inData["workflow_id"],
        inData["workflowParameters"]["workingDir"],
    )
    return dataCollectionGroupId
