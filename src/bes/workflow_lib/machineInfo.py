# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2018 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "14/09/2018"


import PyTango

from bes import config
from bes.workflow_lib import workflow_logging


machineCurrentDict = {
    "24*8+1bunch": 140.0,
}


def getFeUrl(beamline):
    feUrl = None
    if beamline in ["id23eh1", "id23eh2"]:
        feUrl = "tango://acs.esrf.fr:10000/fe/master/id23"
    elif beamline == "id29":
        feUrl = "tango://acs.esrf.fr:10000/fe/master/id29"
    elif beamline in ["id30a1", "id30a3", "id30b"]:
        feUrl = "tango://acs.esrf.fr:10000/fe/master/id30"
    elif beamline in ["bm07"]:
        feUrl = "tango://acs.esrf.fr:10000/fe/master/d07"
    else:
        feUrl = "tango://acs.esrf.fr:10000/fe/master/id23"
    return feUrl


def getFeProxy(beamline):
    feUrl = getFeUrl(beamline)
    feProxy = PyTango.DeviceProxy(feUrl)
    return feProxy


def readMachineCurrent(beamline):
    feProxy = getFeProxy(beamline)
    machineCurrent = feProxy.read_attribute("SR_Current").value
    return float(machineCurrent)


def readFeState(beamline):
    feProxy = getFeProxy(beamline)
    feState = feProxy.read_attribute("FE_State").value
    return feState


def readAutoModeTime(beamline):
    feProxy = getFeProxy(beamline)
    autoModeTime = feProxy.read_attribute("Auto_Mode_Time").value
    return autoModeTime


def readFillingMode(beamline):
    feProxy = getFeProxy(beamline)
    fillingMode = feProxy.read_attribute("SR_Filling_Mode").value
    return fillingMode


def _checkFeBeamStatus(machineCurrent, feState, fillingMode):
    feBeamStatus = False
    if feState == "FE open":
        if fillingMode in machineCurrentDict:
            minMachineCurrent = machineCurrentDict[fillingMode]
        else:
            minMachineCurrent = 140.0
        if machineCurrent > minMachineCurrent:
            feBeamStatus = True
    return feBeamStatus


def checkFeBeamStatus(beamline):
    machineCurrent = readMachineCurrent(beamline)
    feState = readFeState(beamline)
    _ = readAutoModeTime(beamline)
    fillingMode = readFillingMode(beamline)
    feBeamStatus = _checkFeBeamStatus(machineCurrent, feState, fillingMode)
    return feBeamStatus


def correctExposureTimeForFillingMode(beamline, exposureTime):
    logger = workflow_logging.getLogger()
    fillingMode = readFillingMode(beamline)
    multiplicationFactor = None
    if fillingMode == "4 bunch":
        multiplicationFactor = config.get_value(
            beamline, "Beam", "4bunchMultiplicationFactor", default_value=None
        )
        if multiplicationFactor is None:
            multiplicationFactor = 5
    elif fillingMode == "16 bunch":
        multiplicationFactor = config.get_value(
            beamline, "Beam", "16bunchMultiplicationFactor", default_value=None
        )
        if multiplicationFactor is None:
            multiplicationFactor = 2.2
    else:
        # Unknown filling mode, don't change anything
        pass
    if multiplicationFactor is not None:
        logger.debug(
            "Default exposure time multiplication factor for {0} mode set to {1}".format(
                fillingMode, multiplicationFactor
            )
        )
        newExposureTime = round(
            exposureTime * multiplicationFactor, 3
        )  # 200 mA / 40 mA
        logger.debug(
            "Default exposure time was {0} s, new default expsoure time: {1} s".format(
                exposureTime, newExposureTime
            )
        )
    else:
        logger.debug(
            "Default exposure time not changed for {0} mode.".format(fillingMode)
        )
        newExposureTime = exposureTime
    return newExposureTime
