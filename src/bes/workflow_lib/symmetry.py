"""
Workflow library module for connecting to the STAC server
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2015"
__updated__ = "2015-11-12"

from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


symmetryTableFileName = (
    "/opt/pxsoft/ccp4/v7.0//linux-x86_64/ccp4-7.0/lib/data/symop.lib"
)


def checkForcedSpaceGroup(userInputForcedSpaceGroup, sampleInfo, directory=None):
    logger = workflow_logging.getLogger()
    newComment = ""
    forcedSpaceGroup = None
    ispybCrystalSpaceGroup = None
    ispybForcedSpaceGroup = None
    if sampleInfo is not None:
        # Check if we have crystalSpaceGroup and/or forcedSpaceGroup from ISPyB
        dictSampleInfo = common.jsonLoads(sampleInfo)
        if "crystalSpaceGroup" in dictSampleInfo:
            if dictSampleInfo["crystalSpaceGroup"] is not None:
                ispybCrystalSpaceGroup = dictSampleInfo["crystalSpaceGroup"].upper()
        if "forcedSpaceGroup" in dictSampleInfo:
            if dictSampleInfo["forcedSpaceGroup"] is not None:
                ispybForcedSpaceGroup = dictSampleInfo["forcedSpaceGroup"].upper()

    if directory is not None:
        mxpress_parameters = path.getDefaultParameters(
            directory, file_prefix="mxpress_setup"
        )
    else:
        mxpress_parameters = {}

    # Choose forced space group
    if userInputForcedSpaceGroup is not None:
        forcedSpaceGroup = userInputForcedSpaceGroup
        provenance = "user input"
    elif (
        "forcedSpaceGroup" in mxpress_parameters
        and mxpress_parameters["forcedSpaceGroup"] != "Diffraction plan"
    ):
        forcedSpaceGroup = mxpress_parameters["forcedSpaceGroup"]
        provenance = "mxpress setup"
    elif ispybForcedSpaceGroup is not None:
        forcedSpaceGroup = ispybForcedSpaceGroup
        provenance = "diffraction plan"
    elif ispybCrystalSpaceGroup is not None:
        forcedSpaceGroup = ispybCrystalSpaceGroup
        provenance = "crystal form"

    if forcedSpaceGroup is not None:
        # Check if valid space group
        forcedSpaceGroup = forcedSpaceGroup.upper()
        if forcedSpaceGroup in ["UNDEFINED", "UNKNOWN", "NONE", ""]:
            forcedSpaceGroup = None
        else:
            if forcedSpaceGroup in getListOfChiralSpaceGroups():
                newComment = "Using forced space group '{0}' from {1}.".format(
                    forcedSpaceGroup, provenance
                )
                logger.info(newComment)
            else:
                newComment = "Forced space group '{0}' from {1} not recognized!".format(
                    forcedSpaceGroup, provenance
                )
                logger.warning(newComment)
                forcedSpaceGroup = None

    return (forcedSpaceGroup, newComment)


def getITNumberFromSpaceGroupName(spaceGroupName):
    itNumber = None
    symmetryTableFile = open(symmetryTableFileName)
    for strLine in symmetryTableFile.readlines():
        listItems = strLine.split(" ")
        if len(listItems) > 3:
            if spaceGroupName.upper() == listItems[3].upper():
                itNumber = int(listItems[0])
    if itNumber > 1000:
        itNumber = itNumber - 1000
    return itNumber


def getSpaceGroupLongName(spaceGroupName):
    spaceGroupLongName = None
    symmetryTableFile = open(symmetryTableFileName)
    for strLine in symmetryTableFile.readlines():
        listItems = strLine.replace("\n", "").replace("'", "").split(" ")
        if len(listItems) > 3:
            if spaceGroupName.upper() == listItems[3].upper():
                spaceGroupLongName = " ".join(listItems[6:])
    return spaceGroupLongName


def getPointGroupFromSpaceGroupName(spaceGroupName):
    pointGroup = None
    symmetryTableFile = open(symmetryTableFileName)
    for strLine in symmetryTableFile.readlines():
        listItems = strLine.split(" ")
        if len(listItems) > 3:
            if spaceGroupName.upper() == listItems[3].upper():
                pointGroup = listItems[5]
    dictPointGroupName = {
        "TRICLINIC": "-1",
        "MONOCLINIC": "2/m",
        "ORTHORHOMBIC": "mmm",
        "TETRAGONAL": ["4/m", "4/mmm"],
        "TRIGONAL": ["-3", "-3m"],
        "HEXAGONAL": ["6/m", "6/mmm"],
        "CUBIC": ["m-3", "m-3m"],
    }
    return [pointGroup, dictPointGroupName[pointGroup]]


def getSpaceGroupNameFromITNumber(itNumber):
    spaceGroupName = None
    symmetryTableFile = open(symmetryTableFileName)
    for strLine in symmetryTableFile.readlines():
        listItems = strLine.split(" ")
        if len(listItems) > 3:
            if itNumber == int(listItems[0]):
                spaceGroupName = listItems[3].upper()
    return spaceGroupName


def getListOfAllSpaceGroups():
    listOfSpaceGroups = []
    for itNumber in range(1, 231):
        spaceGroup = getSpaceGroupNameFromITNumber(itNumber)
        listOfSpaceGroups.append(spaceGroup)
    return listOfSpaceGroups


def getAllSymmetryDict():
    dictSpaceGroup = {}
    listOfSpaceGroups = getListOfChiralSpaceGroups()
    for spaceGroup in listOfSpaceGroups:
        itNumber = getITNumberFromSpaceGroupName(spaceGroup)
        dictSpaceGroup[spaceGroup] = itNumber
    return dictSpaceGroup


def getChiralSymmetryDict():
    dictSpaceGroup = {}
    listOfSpaceGroups = getListOfChiralSpaceGroups()
    for spaceGroup in listOfSpaceGroups:
        itNumber = getITNumberFromSpaceGroupName(spaceGroup)
        dictSpaceGroup[spaceGroup] = itNumber
    return dictSpaceGroup


def getListOfChiralSpaceGroups():
    return [
        "P1",
        "P2",
        "P21",
        "C2",
        "P222",
        "P2221",
        "P21212",
        "P212121",
        "C222",
        "C2221",
        "F222",
        "I222",
        "I212121",
        "P4",
        "P41",
        "P42",
        "P43",
        "P422",
        "P4212",
        "P4122",
        "P41212",
        "P4222",
        "P42212",
        "P4322",
        "P43212",
        "I4",
        "I41",
        "I422",
        "I4122",
        "P3",
        "P31",
        "P32",
        "P312",
        "P321",
        "P3112",
        "P3121",
        "P3212",
        "P3221",
        "P6",
        "P61",
        "P65",
        "P62",
        "P64",
        "P63",
        "P622",
        "P6122",
        "P6522",
        "P6222",
        "P6422",
        "P6322",
        "H3",
        "R3",
        "H32",
        "R32",
        "P23",
        "P213",
        "P432",
        "P4232",
        "P4332",
        "P4132",
        "F23",
        "F432",
        "F4132",
        "I23",
        "I213",
        "I432",
        "I4132",
    ]
