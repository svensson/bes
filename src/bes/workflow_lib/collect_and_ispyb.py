"""
Workflow library module for connecting both to beamline control and ispyb
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2016"
__updated__ = "2015-01-26"

import os
import jsonpickle
from bes.workflow_lib import collect, edna_ispyb, workflow_logging


def collectAndGroup(
    beamline,
    directory,
    process_directory,
    prefix,
    expTypePrefix,
    run_number,
    resolution,
    mxv1StrategyResult,
    motorPositions,
    workflow_type,
    workflow_index,
    group_node_id,
    sample_node_id,
    suffix,
    workflow_id,
    workflow_working_dir,
    energy=None,
    snapShots=False,
    firstImageNo=1,
    dictCellSpaceGroup=None,
    fineSlicedReferenceImages=False,
    token=None,
    workflow_name=None,
    bes_request_id=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
):
    """
    This method loads and executes the mxCuBE queue (if enabled==True)
    for "normal" data collections
    """
    logger = workflow_logging.getLogger()
    # Force process_directory #TODO: Remove process_directory from arguments
    process_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")
    logger.debug("Forced process_directory: %s" % process_directory)
    listNodeId = []
    serverProxy = collect.getServerProxy(beamline, token)
    if serverProxy is not None:
        collect.importQueueModelObjects(beamline, serverProxy)
        if group_node_id is None:
            # Create new data collection group
            dataCollectionGroup = collect.QUEUE_MODEL_OBJECTS.TaskGroup()
            dataCollectionGroup.set_name(workflow_type)
            dataCollectionGroup.set_number(workflow_index)
            dataCollectionGroup.set_enabled(True)
            json_dcg = jsonpickle.encode(dataCollectionGroup)
            group_node_id = serverProxy.queue_add_child(sample_node_id, json_dcg)
            logger.debug("Created new group node id: %r" % group_node_id)
        else:
            logger.debug("Incoming group node id: %r" % group_node_id)
        logger.debug("Sample node id: %r" % sample_node_id)
        listQueueModelDataCollections = collect._createQueueModelDataCollections(
            beamline=beamline,
            directory=directory,
            process_directory=process_directory,
            prefix=prefix,
            expTypePrefix=expTypePrefix,
            run_number=run_number,
            resolution=resolution,
            mxv1StrategyResult=mxv1StrategyResult,
            motorPositions=motorPositions,
            energy=energy,
            snapShots=snapShots,
            firstImageNo=firstImageNo,
            dictCellSpaceGroup=dictCellSpaceGroup,
            fineSlicedReferenceImages=fineSlicedReferenceImages,
            token=token,
            workflow_name=workflow_name,
            workflow_type=workflow_type,
            bes_request_id=bes_request_id,
            characterisation_id=characterisation_id,
            kappa_settings_id=kappa_settings_id,
            position_id=position_id,
        )
        for dataCollection in listQueueModelDataCollections:
            dataCollection.set_enabled(True)
            dataCollection.ispyb_group_data_collections = True
            nodeId = serverProxy.queue_add_child(
                group_node_id, jsonpickle.encode(dataCollection)
            )
            listNodeId.append(nodeId)
            logger.debug("Data node id: %r" % nodeId)
            serverProxy.queue_execute_entry_with_id(group_node_id)
            firstImageNo = dataCollection.acquisitions[
                0
            ].acquisition_parameters.first_image
            base_prefix = dataCollection.acquisitions[0].path_template.base_prefix
            run_number = dataCollection.acquisitions[0].path_template.run_number
            firstImageName = "{0}_{1}_{2:04d}.{3}".format(
                base_prefix, run_number, firstImageNo, suffix
            )
            firstImagePath = os.path.join(directory, firstImageName)
            iDataCollectionGroupId = edna_ispyb.updateDataCollectionGroupWorkflowId(
                beamline,
                directory,
                run_number,
                expTypePrefix,
                prefix,
                suffix,
                firstImagePath,
                workflow_id,
                workflow_working_dir,
            )
            edna_ispyb.groupMeshDataCollectionsFromQueueEntry(
                beamline,
                directory,
                [firstImagePath],
                workflow_working_dir,
                iDataCollectionGroupId,
            )

    return {"group_node_id": group_node_id, "listNodeId": listNodeId}
