import re
from datetime import datetime
from typing import Tuple, Union, Dict, List

MiniCBFUnit = Union[str, None]
MiniCBFValue = Union[float, int, str, Tuple[float, float], datetime, None]
MiniCBFHeader = List[Tuple[str, MiniCBFValue, MiniCBFUnit]]
MiniCBFHeaderDict = Dict[str, Tuple[MiniCBFValue, MiniCBFUnit]]


def deserialize_mini_cbf_header(
    mini_cbf_header: str, as_dict: bool = False
) -> Union[MiniCBFHeader, MiniCBFHeaderDict]:
    header: MiniCBFHeader = list()
    # Normalize line endings to \n to handle both \n and \r\n
    lines = mini_cbf_header.replace("\r\n", "\n").splitlines()

    # Matches the key before the number (can include spaces)
    key_pattern = r"([^\d]*)"

    # Matches numbers, including scientific notation
    number_pattern = r"(\d*\.?\d+(?:[eE][+-]?\d+)?)"

    # Matches units with leading space
    unit_pattern = r"(\s[a-zA-Z\.]+)"

    # Matches numbers with optional units
    number_with_unit_pattern = rf"{number_pattern}{unit_pattern}?"

    # Pattern for "3.0000" or "3.0000 deg."
    numeric_value = re.compile(rf"^{number_with_unit_pattern}$")

    # Pattern for "2024/Nov/07 10:17:09"
    datetime_pattern = re.compile(r"^\d{4}/[A-Za-z]{3}/\d{2}\s\d{2}:\d{2}:\d{2}$")

    # Pattern for "Silicon sensor, thickness 0.001000 m"
    without_separator = re.compile(rf"^{key_pattern}{number_with_unit_pattern}$")

    # Pattern for "Pixel_size 172e-6 m x 172e-6 m"
    tuple_with_x = re.compile(
        rf"^{key_pattern}[:=]?\s*{number_with_unit_pattern}\s*x\s*{number_with_unit_pattern}$"
    )

    # Pattern for "Energy_range (0, 0) eV" or "Beam_xy (1198.07, 1305.20) pixels"
    tuple_with_brackets = re.compile(
        rf"^{key_pattern}[:=]?s*\({number_pattern}\s*,\s*{number_pattern}\){unit_pattern}?$"
    )

    for line in lines:
        key = ""
        value = None
        unit = None

        # Empty line
        if not line.strip():
            value = ""
            header.append((key, value, unit))
            continue

        # Remove '#' and any leading whitespace
        assert line.startswith("#"), line
        line = line[1:].strip()

        # DateTime format
        if datetime_pattern.match(line):
            value = datetime.strptime(line, "%Y/%b/%d %H:%M:%S")
            header.append((key, value, unit))
            continue

        # Split the key and value
        match_result = re.match(r"(?P<key>[^=]+)\s*=\s*(?P<value>.*)", line)
        if not match_result:
            match_result = re.match(r"(?P<key>[^:]+)\s*:\s*(?P<value>.*)", line)
        if match_result:
            key = match_result.group("key").strip()
            value = match_result.group("value").strip()
        else:
            key = line.strip()
            value = ""

        if value:

            if value == "(nil)":
                value = None
                header.append((key, value, unit))
                continue

        else:

            match_result = tuple_with_brackets.match(key)
            if match_result:
                key = match_result.group(1).strip()
                value = (
                    _as_number(match_result.group(2)),
                    _as_number(match_result.group(3)),
                )
                if match_result.group(4):
                    unit = match_result.group(4).strip()
                header.append((key, value, unit))
                continue

            match_result = tuple_with_x.match(key)
            if match_result:
                key = match_result.group(1).strip()
                value = (
                    _as_number(match_result.group(2)),
                    _as_number(match_result.group(4)),
                )
                if match_result.group(3):
                    unit = match_result.group(3).strip()
                if match_result.group(5):
                    _unit = match_result.group(5).strip()
                    if unit:
                        assert unit == _unit, (unit, _unit)
                    else:
                        unit = _unit
                header.append((key, value, unit))
                continue

            match_result = without_separator.match(key)
            if match_result:
                key = match_result.group(1).strip()
                value = _as_number(match_result.group(2).strip())
                if match_result.group(3):
                    unit = match_result.group(3).strip()
                header.append((key, value, unit))
                continue

            key, _, value = key.partition(" ")
            key = key.strip()
            value = value.strip()

        match_result = numeric_value.match(value)
        if match_result:
            value = _as_number(match_result.group(1).strip())
            if match_result.group(2):
                unit = match_result.group(2).strip()
            header.append((key, value, unit))
            continue

        header.append((key, value, unit))

    if as_dict:
        return {key: (value, unit) for key, value, unit in header if key}

    return header


def serialize_mini_cbf_header(header: MiniCBFHeader) -> str:
    formatted_lines = []

    for key, value, unit in header:
        if value is None:
            value = "(nil)"
        elif isinstance(value, datetime):
            value = value.strftime("%Y/%b/%d %H:%M:%S")
        elif isinstance(value, tuple):
            value = f"({value[0]}, {value[1]})"
        else:
            value = str(value)

        if unit:
            value += f" {unit}"

        if key and value:
            if not key.endswith(":"):
                key += ":"
            formatted_lines.append(f"# {key} {value}")
        elif key:
            formatted_lines.append(f"# {key}")
        elif value:
            formatted_lines.append(f"# {value}")
        else:
            formatted_lines.append("")

    return "\r\n".join(formatted_lines)


def modify_mini_cbf_header(mini_cbf_header: str, values: Dict[str, float]) -> str:
    """Replace values in a mini-CBF header in case sanitation by
    deserialize+serialize should be avoided."""
    for key, value in values.items():
        mini_cbf_header = _replace_mini_cbf_header_value(mini_cbf_header, key, value)
    return mini_cbf_header


def _as_number(value: str) -> Union[str, int, float]:
    try:
        try:
            return int(value.strip())
        except ValueError:
            return float(value.strip())
    except ValueError:
        return value


def _replace_mini_cbf_header_value(mini_cbf_header: str, key: str, value: float) -> str:
    """Replace a value in a mini-CBF header.

    .. code-block:: python

        key = "Angle_increment"
        value = 0.2000
        mini_cbf_header = "...# Angle_increment 0.1000 deg.\r\n..."

    Becomes

    .. code-block:: python

        mini_cbf_header = "...# Angle_increment 0.2000 deg.\r\n..."
    """
    index_key = mini_cbf_header.find(key)
    start = mini_cbf_header.find(" ", index_key) + 1
    stop = mini_cbf_header.find(" ", start)
    old_value = mini_cbf_header[start:stop]
    nb_decimals = len(old_value.split(".")[1])
    new_value = "{0:.0{nb_decimals}f}".format(value, nb_decimals=nb_decimals)
    return mini_cbf_header[:start] + new_value + mini_cbf_header[stop:]
