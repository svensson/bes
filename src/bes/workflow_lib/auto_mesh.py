"""
Workflow library module for auto mesh
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2015"
__updated__ = "2015-11-26"

import os
import numpy
import shutil
import imageio

from bes.workflow_lib.report import WorkflowStepReport
from bes.workflow_lib import edna2_tasks

import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as pyplot  # noqa E402
import pylab  # noqa E402


def gridInfoToPixels(beamline, grid_info, pixelsPerMM):
    x1Pixels = grid_info["x1"] * pixelsPerMM
    y1Pixels = grid_info["y1"] * pixelsPerMM
    dxPixels = grid_info["dx_mm"] * pixelsPerMM
    dyPixels = grid_info["dy_mm"] * pixelsPerMM
    return (x1Pixels, y1Pixels, dxPixels, dyPixels)


def plotMesh(
    beamline,
    imagePath,
    grid_info,
    pixelsPerMM,
    destinationDir,
    signPhiy=1,
    fileName="snapshot_automesh.png",
):
    (x1Pixels, y1Pixels, dxPixels, dyPixels) = gridInfoToPixels(
        beamline, grid_info, pixelsPerMM
    )
    img = imageio.v3.imread(imagePath)
    imgshape = img.shape
    extent = (0, imgshape[1], 0, imgshape[0])
    pylab.matshow(img / numpy.max(img), extent=extent)
    pylab.gray()
    ny = imgshape[0]
    nx = imgshape[1]
    if signPhiy < 0:
        meshXmin = nx / 2 - x1Pixels
    else:
        meshXmin = nx / 2 + x1Pixels
    meshYmin = ny / 2 - y1Pixels
    meshXmax = meshXmin + dxPixels
    meshYmax = meshYmin - dyPixels
    pylab.plot([meshXmin, meshXmin], [meshYmin, meshYmax], color="red", linewidth=2)
    pylab.plot([meshXmax, meshXmax], [meshYmin, meshYmax], color="red", linewidth=2)
    pylab.plot([meshXmin, meshXmax], [meshYmin, meshYmin], color="red", linewidth=2)
    pylab.plot([meshXmin, meshXmax], [meshYmax, meshYmax], color="red", linewidth=2)
    meshSnapShotPath = os.path.join(destinationDir, fileName)
    axes = pyplot.gca()
    axes.set_xlim([0, imgshape[1]])
    axes.set_ylim([0, imgshape[0]])
    pylab.savefig(meshSnapShotPath, bbox_inches="tight")
    pylab.close()
    return meshSnapShotPath


def createSnapshotHTMLPage(
    snapshotDir,
    snapShotAngles=[0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330],
    pyarchHtmlDir=None,
    prefix="snapshot",
    suffix="png",
):
    listFiles = []
    workflowStepReport = WorkflowStepReport("Snapshots", "Sample snapshots")
    if len(snapShotAngles) > 1:
        deltaAngle = snapShotAngles[1] - snapShotAngles[0]
        workflowStepReport.addInfo(
            f"{deltaAngle} degree rotation snapshots of the sample mount"
            + "after optical centring (using Lucid) and before X-ray centring"
        )
    workflowStepReport.startImageList()
    for omega in snapShotAngles:
        imageName = "{0}_{1:03d}.{2}".format(prefix, omega, suffix)
        listFiles.append(imageName)
        label = "Omega = {0} degrees".format(omega)
        pathToImage = os.path.join(snapshotDir, imageName)
        edna2_tasks.waitForFile(pathToImage)
        workflowStepReport.addImage(pathToImage, imageTitle=label)
    workflowStepReport.endImageList()
    animatedGifPath = os.path.join(snapshotDir, "snapshot_animated.gif")
    if os.path.exists(animatedGifPath):
        workflowStepReport.addImage(animatedGifPath, "Snapshots before mesh")
    # Copy files to pyarch
    if pyarchHtmlDir is None:
        pyarchHtmlDir = snapshotDir
    autoMeshPyarchHtmlDir = os.path.join(pyarchHtmlDir, "Snapshots")
    os.makedirs(autoMeshPyarchHtmlDir, 0o755)
    htmlFilePath = workflowStepReport.renderHtml(autoMeshPyarchHtmlDir)
    jsonFilePath = workflowStepReport.renderJson(autoMeshPyarchHtmlDir)
    shutil.copy(
        os.path.join(snapshotDir, "snapshot_animated.gif"), autoMeshPyarchHtmlDir
    )
    meshSnapShotPath = os.path.join(autoMeshPyarchHtmlDir, "snapshot_animated.gif")
    return meshSnapShotPath, htmlFilePath, jsonFilePath


def createAutomeshHTMLPage(
    beamline,
    newPhi,
    grid_info,
    PIXELS_PER_MM,
    snapshotDir,
    autoMeshWorkingDir,
    signPhiy=1,
    pyarchHtmlDir=None,
    prefix="snapshot",
    suffix="png",
):
    # Create the plot file
    imagePath = os.path.join(
        snapshotDir, "{0}_{1:03d}.{2}".format(prefix, newPhi, suffix)
    )
    meshSnapShotPath = plotMesh(
        beamline,
        imagePath,
        grid_info,
        PIXELS_PER_MM,
        autoMeshWorkingDir,
        signPhiy=signPhiy,
    )
    #
    workflowStepReport = WorkflowStepReport("Automesh", "Result of automesh")
    workflowStepReport.addInfo("The red box defines the area of the X-ray scan")
    pathToImage = os.path.join(autoMeshWorkingDir, "snapshot_automesh.png")
    workflowStepReport.addImage(pathToImage)
    dictTable = {"columns": [], "data": []}
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    dx_mm = grid_info["dx_mm"]
    dy_mm = grid_info["dy_mm"]
    if nx > 1:
        dictTable["columns"].append("Steps x")
    if ny > 1:
        dictTable["columns"].append("Steps y")
    if nx > 1:
        dictTable["columns"].append("Size x [mm]")
    if ny > 1:
        dictTable["columns"].append("Size y [mm]")
    listRow = []
    if nx > 1:
        listRow.append(nx)
    if ny > 1:
        listRow.append(ny)
    if nx > 1:
        value = round(dx_mm * nx / (nx - 1), 3)
        listRow.append(value)
    if ny > 1:
        value = round(dy_mm * ny / (ny - 1), 3)
        listRow.append(value)
    dictTable["data"].append(listRow)
    workflowStepReport.addTable("Grid size", dictTable["columns"], dictTable["data"])
    # Copy files to pyarch
    if pyarchHtmlDir is None:
        pyarchHtmlDir = autoMeshWorkingDir
    autoMeshPyarchHtmlDir = os.path.join(pyarchHtmlDir, "Automesh")
    os.makedirs(autoMeshPyarchHtmlDir, 0o755)
    htmlFilePath = workflowStepReport.renderHtml(autoMeshPyarchHtmlDir)
    jsonFilePath = workflowStepReport.renderJson(autoMeshPyarchHtmlDir)
    shutil.copy(meshSnapShotPath, autoMeshPyarchHtmlDir)
    meshSnapShotPath = os.path.join(
        autoMeshPyarchHtmlDir, os.path.basename(meshSnapShotPath)
    )
    return meshSnapShotPath, htmlFilePath, jsonFilePath
