"""
Workflow library module for running EDNA plugins
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013-2016"
__updated__ = "2016-07-12"

import os
import time
import getpass
import tempfile

from bes.workflow_lib import path
from bes.workflow_lib import workflow_logging

from EDVerbose import EDVerbose
from EDFactoryPluginStatic import EDFactoryPluginStatic

from XSDataCommon import XSDataString  # noqa F401
from XSDataCommon import XSDataImage  # noqa F401
from XSDataCommon import XSDataFile  # noqa F401
from XSDataCommon import XSDataInteger  # noqa F401
from XSDataCommon import XSDataVectorDouble  # noqa F401

EDFactoryPluginStatic.loadModule("markupv1_10")
import markupv1_10  # noqa F401


# Init simulation
def initSimulation(beamline):
    logger = workflow_logging.getLogger()
    if beamline.startswith("simulator"):  # or beamline == "id30a2":
        logger.debug("Setting EDNA site to ESRF - not using ISPyB validation server.")
        from EDUtilsPath import EDUtilsPath

        EDUtilsPath.setEdnaSite("ESRF")
        os.environ["EDNA_SITE"] = "ESRF"


#        EDVerbose.setVerboseDebugOn()
# EDNA base directory
def getEdnaBaseDirectory(directory):
    logger = workflow_logging.getLogger()
    strPluginBaseDir = path.createWorkflowWorkingDirectory(directory)
    logger.debug("EDNA base directory: %s" % strPluginBaseDir)
    os.chdir(strPluginBaseDir)
    return strPluginBaseDir


def executeEdnaPluginAsynchronous(
    strPluginName, xsDataInput, workflow_working_dir, timeout=None
):
    logger = workflow_logging.getLogger()
    user = None
    try:
        user = path.extractBeamlineFromDirectory(workflow_working_dir)
    except Exception:
        logger.debug(
            "Cannot determine beamline from path {0}".format(workflow_working_dir)
        )
    if user is None:
        user = getpass.getuser()
    if workflow_working_dir is None:
        workflow_working_dir = path.createWorkflowWorkingDirectory()
    elif not os.path.exists(workflow_working_dir):
        if "RAW_DATA" in workflow_working_dir:
            workflow_working_dir = workflow_working_dir.replace(
                "RAW_DATA", "PROCESSED_DATA"
            )
            # Try to get beamline from path
            user = path.extractBeamlineFromDirectory(workflow_working_dir)
        os.makedirs(workflow_working_dir, 0o755)
    logger.debug("Executing EDNA plugin %s" % strPluginName)
    logger.debug("EDNA_SITE %s" % os.environ["EDNA_SITE"])
    strPluginBaseDir = workflow_working_dir
    # strDate = time.strftime("%Y%m%d", time.localtime(time.time()))
    strTime = time.strftime("%H%M%S", time.localtime(time.time()))
    # strPluginBaseDir = os.path.join("/tmp_14_days", user, strDate)
    if not os.path.exists(strPluginBaseDir):
        try:
            os.makedirs(strPluginBaseDir, 0o755)
        except OSError as e:
            logger.warning(
                "Error when trying to create directory: {0}".format(strPluginBaseDir)
            )
            logger.warning("Error message: {0}".format(e))
            time.sleep(1)
            if not os.path.exists(strPluginBaseDir):
                raise BaseException(
                    "Cannot create directory: {0}".format(strPluginBaseDir)
                )
    strEDNAWorkDir = tempfile.mkdtemp(
        prefix=strTime + "_" + strPluginName.replace("EDPlugin", "") + "_",
        dir=strPluginBaseDir,
    )
    os.chmod(strEDNAWorkDir, 0o755)
    logger.debug("EDNA plugin working directory: %s" % strEDNAWorkDir)
    os.chmod(strEDNAWorkDir, 0o755)
    ednaBaseName = os.path.basename(strEDNAWorkDir)
    ednaLogName = "{0}.log".format(ednaBaseName)
    ednaLogPath = os.path.join(workflow_working_dir, ednaLogName)
    EDVerbose.setLogFileName(ednaLogPath)
    EDVerbose.setVerboseOff()
    # Load plugin
    edPlugin = EDFactoryPluginStatic.loadPlugin(strPluginName)
    edPlugin.setDataInput(xsDataInput)
    edPlugin.setBaseDirectory(strPluginBaseDir)
    edPlugin.setBaseName(ednaBaseName)
    if timeout is not None:
        edPlugin.setTimeOut(timeout)
        logger.debug("Timeout set to %f s" % timeout)
    logger.debug("Start of execution of EDNA plugin %s" % strPluginName)
    edPlugin.execute()
    return edPlugin, ednaLogPath


def synchronizeEdnaPlugin(edPlugin, bLogExecutiveSummary=False):
    logger = workflow_logging.getLogger()
    edPlugin.synchronize()
    logger.debug("EDNA plugin %s executed" % edPlugin.getPluginName())
    if bLogExecutiveSummary:
        for line in edPlugin.getListExecutiveSummaryLines():
            logger.info(line)
    xsDataResult = edPlugin.dataOutput
    logger.debug("XSDataResult type: %s" % type(xsDataResult))
    return xsDataResult


def executeEdnaPlugin(
    strPluginName, xsDataInput, workflow_working_dir, bLogExecutiveSummary=False
):
    edPlugin, ednaLogPath = executeEdnaPluginAsynchronous(
        strPluginName, xsDataInput, workflow_working_dir
    )
    xsDataResult = synchronizeEdnaPlugin(edPlugin, bLogExecutiveSummary)
    return xsDataResult, ednaLogPath
