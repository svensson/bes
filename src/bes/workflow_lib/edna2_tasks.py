# coding: utf8
#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Workflow library module for running EDNA2 tasks
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF"
__updated__ = "2020-12-17"

import os
import json
import pprint

from edna2.utils import UtilsImage
from edna2.utils import UtilsPath
from edna2.config import set_site as set_edna2_site

from edna2.tasks.DozorM import DozorM
from edna2.tasks.DozorM2 import DozorM2
from edna2.tasks.DozorRD import DozorRD
from edna2.tasks.ReadImageHeader import ReadImageHeader
from edna2.tasks.ControlIndexing import ControlIndexing
from edna2.tasks.Characterisation import Characterisation
from edna2.tasks.DiffractionThumbnail import DiffractionThumbnail
from edna2.tasks.ImageQualityIndicators import ImageQualityIndicators
from edna2.tasks.RadiationDamageProcessing import RadiationDamageProcessing

from bes.workflow_lib import workflow_logging


def initSimulation(beamline):
    logger = workflow_logging.getLogger()
    if beamline.startswith("simulator"):  # or beamline == "id30a2":
        logger.debug("Setting EDNA2 site to ESRF - not using ISPyB validation server.")
        set_edna2_site("ESRF")


def storeImageQualityIndicatorsFromMeshPositionsStart(
    beamline,
    directory,
    meshPositions,
    workflow_working_dir,
    grid_info=None,
    doIndexing=True,
    gap_x=None,
    doDistlSignalStrength=False,
    doUploadToIspyb=True,
    useFastMesh=True,
    doCrystfel=False,
):
    logger = workflow_logging.getLogger()
    logger.info("Starting processing of grid images")
    initSimulation(beamline)
    if type(meshPositions) is str:
        meshPositions = json.loads(meshPositions)
    if grid_info is None:
        raise RuntimeError("grid_info is None!")
    # if beamline in ["id30a3"] and gap_x is not None and gap_x > 0.001:
    #     logger.warning("Positive horizontal grid gap detected ({0} um) on id30a3,
    #     processing batch size set to 1".format(int(gap_x * 1000)))
    #     batchSize = 1
    meshType = None
    if beamline in ["id23eh1", "id30a1", "id30a3", "id30b"]:
        batchSize = 100
    elif beamline in ["id23eh2"]:
        batchSize = 500
    else:
        if grid_info["steps_x"] == 1:
            batchSize = grid_info["steps_y"]
            meshType = "vertical_line"
        elif grid_info["steps_y"] == 1:
            batchSize = grid_info["steps_x"]
            meshType = "horizontal_line"
        else:
            meshType = "mesh"
            # Batch size is number of images divided by number of processors
            noImages = grid_info["steps_x"] * grid_info["steps_y"]
            batchSize = max(
                int(noImages / 20), grid_info["steps_x"], grid_info["steps_y"]
            )
    logger.info("Batch size: {0}".format(batchSize))
    # Check size of grid for submit to cluster BES-293
    sizeOfGrid = grid_info["steps_x"] * grid_info["steps_y"]
    doSubmit = False
    minSlurmNoImages = 1000
    if sizeOfGrid > minSlurmNoImages:
        doSubmit = True
        logger.info("Submitting to SLURM cluster for faster processing.")
    inputImageQulatyIndicators = {
        "beamline": beamline,
        "doDistlSignalStrength": doDistlSignalStrength,
        "doIndexing": doIndexing,
        "doUploadToIspyb": doUploadToIspyb,
        "directory": directory,
        "fastMesh": useFastMesh,
        "batchSize": batchSize,
        "image": [],
        "workingDirectory": workflow_working_dir,
        "doSubmit": doSubmit,
        "prepareDozorAllFile": True,
        "runDozorM2": False,
        "doDistlSignalStrength": doDistlSignalStrength,
        "doTotalIntensity": True,
    }
    for position in meshPositions:
        imagePath = os.path.join(directory, position["imageName"])
        if beamline in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
            # Replace ".cbf" with ".h5" for ISPyB
            imagePath = imagePath.replace(".cbf", ".h5")
        inputImageQulatyIndicators["image"].append(imagePath)

    imageQualityIndicatorsTask = ImageQualityIndicators(
        inData=inputImageQulatyIndicators, workingDirectorySuffix=meshType
    )
    imageQualityIndicatorsTask.start()
    return imageQualityIndicatorsTask


def storeImageQualityIndicatorsFromMeshPositionsSynchronize(
    imageQualityIndicatorsTask, meshPositions
):
    imageQualityIndicatorsTask.join()
    outData = imageQualityIndicatorsTask.outData
    dozorAllFile = outData.get("dozorAllFile", None)
    (meshPositions, inputDozor) = convertXSDataImageQualityIndicatorsToDict(
        meshPositions, outData
    )
    return meshPositions, inputDozor, dozorAllFile


def startRadiationDamageDozorProcessing(
    beamline,
    directory,
    workflow_working_dir,
    list_image_path,
    dataCollectionId,
    working_directory_suffix,
):
    inputImageQualityIndicators = {
        "beamline": beamline,
        "doDistlSignalStrength": False,
        "doIndexing": False,
        "doIspybUpload": True,
        "directory": directory,
        "fastMesh": True,
        "batchSize": len(list_image_path),
        "image": list_image_path,
        "workingDirectory": workflow_working_dir,
        "doSubmit": False,
        "prepareDozorAllFile": True,
        "runDozorM2": False,
        "dataCollectionId": dataCollectionId,
    }
    imageQualityIndicatorsTask = ImageQualityIndicators(
        inData=inputImageQualityIndicators,
        workingDirectorySuffix=working_directory_suffix,
    )
    imageQualityIndicatorsTask.start()
    return imageQualityIndicatorsTask


def startXdsIndexAndIntegration(
    imagePath,
    workflow_working_dir,
    working_directory_suffix,
):
    in_data = {
        "imagePath": imagePath,
        "workingDirectory": workflow_working_dir,
    }
    edna2_task = RadiationDamageProcessing(
        inData=in_data,
        workingDirectorySuffix=working_directory_suffix,
    )
    edna2_task.start()
    return edna2_task


def convertXSDataImageQualityIndicatorsToDict(meshPositions, outData):
    logger = workflow_logging.getLogger()
    resultMeshPositions = []
    inputDozor = None
    if outData is not None and "imageQualityIndicators" in outData:
        for imageQualityIndicator in outData["imageQualityIndicators"]:
            if "image" in imageQualityIndicator:
                imageNameResult = os.path.basename(imageQualityIndicator["image"])
                for position in meshPositions:
                    position_copy = dict(position)
                    if "dozorScore" in imageQualityIndicator:
                        position_copy["dozor_score"] = imageQualityIndicator[
                            "dozorScore"
                        ]
                    if position_copy["imageName"] == imageNameResult:
                        position_copy.update(imageQualityIndicator)
                        resultMeshPositions.append(position_copy)
                        #                        pprint.pprint(position)
        for position in resultMeshPositions:
            if "dozor_score" in position:
                if str(position["dozor_score"]) == "nan":
                    position["dozor_score"] = 0.0
        if "inputDozor" in outData:
            inputDozor = outData["inputDozor"]
    logger.info("Processing of grid images finished")
    return resultMeshPositions, inputDozor


def controlIndexing(inputIndexing, workingDirectory):
    inputIndexing2 = dict(inputIndexing)
    inputIndexing2["workingDirectory"] = workingDirectory
    controlIndexing = ControlIndexing(inData=inputIndexing2)
    controlIndexing.execute()
    outData = {}
    if controlIndexing.isSuccess():
        outData = controlIndexing.outData
    return outData


def characterisation(inputCharacterisation, workingDirectory):
    inputCharacterisation = dict(inputCharacterisation)
    inputCharacterisation["workingDirectory"] = workingDirectory
    characterisation = Characterisation(inData=inputCharacterisation)
    characterisation.execute()
    outData = {}
    if characterisation.isSuccess():
        outData = characterisation.outData
    return outData


def runDozorM(
    beamline,
    firstImagePath,
    grid_info,
    meshZigZag,
    dozorAllFile,
    listApertureSizes,
    nominalBeamSizeX,
    nominalBeamSizeY,
    workingDirectory,
    isHorizontalRotationAxis=True,
):
    outDataDozorM = {}
    inDataReadImageHeader = {
        "imagePath": [firstImagePath],
        "workingDirectory": workingDirectory,
    }
    readImageHeader = ReadImageHeader(inData=inDataReadImageHeader)
    readImageHeader.execute()
    if readImageHeader.isSuccess():
        prefix = UtilsImage.getPrefix(firstImagePath)
        firstSubWedge = readImageHeader.outData["subWedge"][0]
        experimentalCondition = firstSubWedge["experimentalCondition"]
        detector = experimentalCondition["detector"]
        beam = experimentalCondition["beam"]
        stepsX = grid_info["steps_x"]
        stepsY = grid_info["steps_y"]
        length = grid_info["dx_mm"]
        width = grid_info["dy_mm"]
        numberApertures = len(listApertureSizes)
        apertureSizes = " ".join(list(map(str, listApertureSizes)))
        stepH = round(length / stepsX * 1000, 1)
        stepV = round(width / stepsY * 1000, 1)
        # Determine the scan direction.
        if stepsX > 1 and stepsY > 1:
            # If not a line scn the scan direction is
            # given by the rotation axis direction
            if isHorizontalRotationAxis:
                isHorizontalScan = True
            else:
                isHorizontalScan = False
        else:
            # If line scan determined by the direction of the line scan
            if stepsX == 1:
                isHorizontalScan = False
                stepH = stepV
            elif stepsY == 1:
                isHorizontalScan = True
                stepV = stepH
            else:
                # This should never happen!
                raise RuntimeError(
                    "Invalid values for steps_x: {0} and steps_y:{1}!".format(
                        stepsX, stepsY
                    )
                )

        inDataDozorM = {
            "detectorType": detector["type"],
            "beamline": beamline,
            "detector_distance": detector["distance"],
            "wavelength": beam["wavelength"],
            "orgx": detector["beamPositionX"] / detector["pixelSizeX"],
            "orgy": detector["beamPositionY"] / detector["pixelSizeY"],
            "number_row": stepsX,
            "number_images": stepsX * stepsY,
            "isZigZag": meshZigZag,
            "step_h": stepH,
            "step_v": stepV,
            "beam_shape": "G",
            "beam_h": nominalBeamSizeX,
            "beam_v": nominalBeamSizeY,
            "number_apertures": numberApertures,
            "aperture_size": apertureSizes,
            "reject_level": 10,
            "dozorAllFile": dozorAllFile,
            "number_scans": 1,
            "first_scan_number": 1,
            "workingDirectory": workingDirectory,
            "isHorizontalScan": isHorizontalScan,
        }
        dozorM = DozorM(inData=inDataDozorM, workingDirectorySuffix=prefix)
        dozorM.execute()
        if dozorM.isSuccess():
            outDataDozorM = dozorM.outData
    return outDataDozorM


def runDozorM2(
    beamline,
    firstImagePath,
    grid_info,
    meshZigZag,
    listApertureSizes,
    nominalBeamSizeX,
    nominalBeamSizeY,
    workingDirectory,
    phiValues=None,
    dozorAllFile=None,
    listDozorAllFile=None,
    isHorizontalRotationAxis=True,
    motorPositions=None,
    reject_level=None,
    loop_thickness=None,
    workingDirectorySuffix=None,
):
    logger = workflow_logging.getLogger()
    outDataDozorM2 = {}
    inDataReadImageHeader = {
        "imagePath": [firstImagePath],
        "workingDirectory": workingDirectory,
    }
    if phiValues is None:
        phiValues = []
    readImageHeader = ReadImageHeader(inData=inDataReadImageHeader)
    readImageHeader.execute()
    if readImageHeader.isSuccess():
        prefix = UtilsImage.getPrefix(firstImagePath)
        firstSubWedge = readImageHeader.outData["subWedge"][0]
        experimentalCondition = firstSubWedge["experimentalCondition"]
        detector = experimentalCondition["detector"]
        beam = experimentalCondition["beam"]
        x1 = grid_info["x1"]
        y1 = grid_info["y1"]
        stepsX = grid_info["steps_x"]
        stepsY = grid_info["steps_y"]
        numberApertures = len(listApertureSizes)
        apertureSizes = " ".join(list(map(str, listApertureSizes)))
        stepH = grid_info.get(
            "cell_width", round(grid_info["dx_mm"] * 1000 / stepsX, 0)
        )
        stepV = grid_info.get(
            "cell_height", round(grid_info["dy_mm"] * 1000 / stepsY, 0)
        )
        # Determine the scan direction.
        if stepsX > 1 and stepsY > 1:
            # If not a line scn the scan direction is
            # given by the rotation axis direction
            if isHorizontalRotationAxis:
                isHorizontalScan = True
            else:
                isHorizontalScan = False
        else:
            # If line scan determined by the direction of the line scan
            if stepsX == 1:
                isHorizontalScan = False
                stepH = stepV
            elif stepsY == 1:
                isHorizontalScan = True
                stepV = stepH
            else:
                # This should never happen!
                raise RuntimeError(
                    "Invalid values for steps_x: {0} and steps_y:{1}!".format(
                        stepsX, stepsY
                    )
                )
        # Calculate axis_zero, i.e. position of rotation axis in mesh coordinates
        gridX0 = -x1 / (stepH / 1000.0) + 1
        gridY0 = -y1 / (stepV / 1000.0) + 1

        inDataDozorM2 = {
            "detectorType": detector["type"],
            "beamline": beamline,
            "detector_distance": detector["distance"],
            "wavelength": beam["wavelength"],
            "orgx": detector["beamPositionX"] / detector["pixelSizeX"],
            "orgy": detector["beamPositionY"] / detector["pixelSizeY"],
            "number_row": stepsX,
            "number_images": stepsX * stepsY,
            "isZigZag": meshZigZag,
            "step_h": stepH,
            "step_v": stepV,
            "beam_shape": "G",
            "beam_h": nominalBeamSizeX,
            "beam_v": nominalBeamSizeY,
            "number_apertures": numberApertures,
            "aperture_size": apertureSizes,
            "reject_level": reject_level,
            "list_dozor_all": listDozorAllFile,
            "phi_values": phiValues,
            "number_scans": len(listDozorAllFile),
            "first_scan_number": 1,
            "workingDirectory": workingDirectory,
            "isHorizontalScan": isHorizontalScan,
            "grid_x0": gridX0,
            "grid_y0": gridY0,
            "loop_thickness": loop_thickness,
        }

        if motorPositions is not None:
            inDataDozorM2["sampx"] = motorPositions["sampx"]
            inDataDozorM2["sampy"] = motorPositions["sampy"]
            inDataDozorM2["phiy"] = motorPositions["phiy"]
            inDataDozorM2["phiz"] = motorPositions["phiz"]
        if workingDirectorySuffix is None:
            workingDirectorySuffix = prefix
        dozorM2 = DozorM2(
            inData=inDataDozorM2, workingDirectorySuffix=workingDirectorySuffix
        )
        logger.debug("Running DozorM2...")
        logger.debug("workingDirectory: %s", workingDirectory)
        logger.debug("inData DozorM2: %s", pprint.pformat(inDataDozorM2))
        logger.debug("DozorM2 workingDirectory: %s", dozorM2.getWorkingDirectory())
        dozorM2.execute()
        logger.debug("DozorM2 workingDirectory: %s", dozorM2.getWorkingDirectory())
        logger.info("DozorM2 finished: success=%s", dozorM2.isSuccess())
        if dozorM2.isSuccess():
            outDataDozorM2 = dozorM2.outData
    return outDataDozorM2


def updateDozorMMeshPositions(meshPositions, arrayScore):
    newMeshPositions = DozorM.updateMeshPositions(
        meshPositions=meshPositions, arrayScore=arrayScore
    )
    return newMeshPositions


def create_thumbnails(
    list_image_creation, targetDir, workflow_working_dir, format=None
):
    inDataDiffractionThumbnail = {
        "image": list_image_creation,
        "forcedOutputDirectory": targetDir,
        "workingDirectory": workflow_working_dir,
        "format": format,
    }
    diffractionThumbnail = DiffractionThumbnail(inData=inDataDiffractionThumbnail)
    diffractionThumbnail.execute()
    return diffractionThumbnail.outData


def create_thumbnails_for_pyarch(list_image_creation, workflow_working_dir):
    inDataDiffractionThumbnail = {
        "image": list_image_creation,
        "workingDirectory": workflow_working_dir,
    }
    diffractionThumbnail = DiffractionThumbnail(inData=inDataDiffractionThumbnail)
    diffractionThumbnail.execute()


def runDozorRD(
    wavelength, exposureTime, numberOfImages, listDozorAll, workingDirectory
):
    inDataDozorRD = {
        "list_dozor_all": listDozorAll,
        "wavelength": wavelength,
        "exposureTime": exposureTime,
        "numberOfImages": numberOfImages,
        "workingDirectory": workingDirectory,
    }
    dozorRD = DozorRD(inData=inDataDozorRD)
    dozorRD.execute()
    return dozorRD.outData


def waitForFile(file_path, timeOut=30):
    UtilsPath.waitForFile(file_path, timeOut=timeOut)
