from typing import List

import numpy
import fabio
from fabio import cbfimage

from bes.workflow_lib import workflow_logging
from bes.workflow_lib.mini_cbf_utils import modify_mini_cbf_header


def sum_cbf_images(
    filenames: List[str],
    angle_start: float,
    angle_increment: float,
    preserve_dtype: bool = True,
) -> cbfimage.CbfImage:
    logger = workflow_logging.getLogger()
    assert len(filenames), "No file names provided"

    image_name = filenames[0]
    logger.info("Loading %s", image_name)
    with fabio.open(image_name) as img:
        header = {k: v for k, v in img.header.items() if not _is_default_cbf_key(k)}
        data = img.data.copy()

    metadata = {"Angle_increment": angle_increment, "Start_angle": angle_start}
    header["_array_data.header_contents"] = modify_mini_cbf_header(
        header["_array_data.header_contents"], metadata
    )

    dtype = data.dtype
    max_value = numpy.iinfo(dtype).max
    sumImage = data.astype(numpy.int64)
    mask_to_sum = sumImage >= 0

    for image_name in filenames[1:]:
        logger.info("Loading %s", image_name)
        with fabio.open(image_name) as img:
            sumImage[mask_to_sum] += img.data[mask_to_sum]

    if preserve_dtype:
        overflow = sumImage > max_value
        if overflow.any():
            sumImage[overflow] = max_value
            logger.warning("%d pixels with value overflow", overflow.sum())

        sumImage = sumImage.astype(dtype)

    return cbfimage.CbfImage(data=sumImage, header=header)


def _is_default_cbf_key(key: str) -> bool:
    return key.startswith("X-") or key.startswith("Content-") or key == "conversions"
