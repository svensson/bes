"""
Workflow library module for id30a3 spectrum analyser
"""

__authors__ = ["David vonStetten", "Olof Svensson"]
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2015"
__updated__ = "2015-11-20"

import os
import PIL
import time
import shutil
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from edna_kernel import markupv1_10

from bes.workflow_lib import path
from bes.workflow_lib import collect

matplotlib.use("Agg")


def plotSpectra(allSpectra, totalSpectra, figFileName):
    noSpectra = len(allSpectra)
    rainbowcolors = plt.cm.hsv(np.linspace(0.0, 0.6, totalSpectra + 1))

    if allSpectra[0].shape[1] == 4:
        datacolumn = 3
    else:
        datacolumn = 1

    for i in range(noSpectra):
        npspectra = allSpectra[i]
        plot_xdata = npspectra[:, 0]
        plot_ydata = npspectra[:, datacolumn]
        plt.plot(
            plot_xdata[5:],
            plot_ydata[5:],
            linewidth=1,
            color=rainbowcolors[totalSpectra - i],
            label="spectrum #%d" % (i + 1),
        )

    plt.xlim([min(plot_xdata[5:]), max(plot_xdata[5:])])
    plt.legend(loc="upper right", shadow=True, prop={"size": 10})
    plt.xlabel("wavelength [nm]", fontsize=16)
    plt.ylabel("absorbance [OD]", fontsize=16)
    plt.savefig(figFileName, dpi=100)
    plt.close()


def createHtmlPage(plotFile, strPageDir, prefix, directory):
    strPagePath = os.path.join(strPageDir, "index.html")
    title = prefix
    plotTitle = "Spectra"
    plotImage = PIL.Image.open(os.path.join(strPageDir, plotFile))
    plotImageWidth, plotImageHeight = plotImage.size  # (width,height) tuple
    page = markupv1_10.page(mode="loose_html")
    page.init(title=title, footer="Generated on %s" % time.asctime())
    page.div(align_="CENTER")
    page.h1()
    page.strong(title)
    page.h1.close()
    page.img(
        src=os.path.basename(plotFile),
        title=plotTitle,
        height=plotImageHeight,
        width=plotImageWidth,
    )
    page.div.close()
    page.br()
    page.h3("Spectra file location: {0}".format(directory))
    page.br()
    strHTML = str(page)
    filePage = open(strPagePath, "w")
    filePage.write(strHTML)
    filePage.close()
    return strPagePath


def acquireSpectra(
    beamline,
    directory,
    prefix,
    host,
    port,
    allSpectra,
    figFileName,
    spectraNumpyFile,
    spectrum_start_phi,
    listNodeId,
    htmlDir,
    pyarch_html_dir,
    totalSpectra,
    token=None,
):
    collect.moveMotors(beamline, directory, {"phi": spectrum_start_phi}, token=token)
    spectrum = collect.rpcGetSpectrum(host, port)
    npspectra = np.asarray(spectrum)
    npspectra = np.transpose(npspectra)
    if allSpectra is None:
        allSpectra = np.array(npspectra, ndmin=3)
    else:
        allSpectra = np.vstack((allSpectra, np.array(npspectra, ndmin=3)))
    plotSpectra(allSpectra, totalSpectra, figFileName)
    np.save(spectraNumpyFile, allSpectra)
    index = len(allSpectra)
    spectraFileName = os.path.join(
        directory, "{0}_{1:02d}.txt".format(prefix, index - 1)
    )
    np.savetxt(spectraFileName, npspectra, fmt="%.2f", delimiter="\t")

    spectraWebPage = createHtmlPage(figFileName, htmlDir, prefix, directory)
    collect.displayNewHtmlPage(
        beamline, spectraWebPage, "Spectra", listNodeId, token=token
    )
    if pyarch_html_dir is not None:
        # Always copy the page to the first directory
        if os.path.exists(os.path.join(pyarch_html_dir, "html_01")):
            shutil.rmtree(os.path.join(pyarch_html_dir, "html_01"))
        dict_html = {}
        dict_html["html_directory_path"] = os.path.dirname(spectraWebPage)
        dict_html["index_file_name"] = os.path.basename(spectraWebPage)
        dict_html["plot_file_name"] = os.path.basename(figFileName)
        path.copyHtmlDirectoryToPyarch(pyarch_html_dir, dict_html)
    return allSpectra
