"""
Workflow library module for controlling the dehydration device
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013"
__updated__ = "2013-11-17"


import os
import time
import pprint

import PyTango

from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import workflow_logging

from bes.workflow_lib.edna_kernel import markupv1_10
from bes.workflow_lib.report import WorkflowStepReport


def read_hydration_level(beamline, target_rh):
    """
    Returns the current level of humidity
    """
    logger = workflow_logging.getLogger()
    current_rh = None
    hc_tango_url = config.get_value(
        beamline, "Dehydration", "device", default_value=None
    )
    if hc_tango_url is None:
        if not beamline.startswith("simulator") and beamline != "id30a2":
            raise Exception("Dehydraion device is None!")
        else:
            logger.info("Simulated beamline, not reading RH value")
            current_rh = target_rh
    else:
        logger.debug("hc_tango_url: %r" % hc_tango_url)
        hc_proxy = PyTango.DeviceProxy(hc_tango_url)
        n_tries = 10
        index = 0
        b_continue = True
        while b_continue:
            try:
                current_rh = hc_proxy.read_attribute("RH").value
                logger.info("Current RH = %.1f" % current_rh)
                b_continue = False
            except BaseException as error:
                logger.warning(
                    "WARNING! Could not read dehydration device RH value: %r" % error
                )
                time.sleep(10)
                index += 1
                if index == n_tries:
                    raise Exception(
                        "ERROR! Could not read dehydration device "
                        + "RH value after %d tries" % n_tries
                    )
    return current_rh


def set_hydration_level(beamline, target_rh, delay):
    """
    Sets the level of humidity
    """
    logger = workflow_logging.getLogger()
    precision = 1.0
    hc_tango_url = config.get_value(
        beamline, "Dehydration", "device", default_value=None
    )
    logger.info("Setting RH value to %.1f" % target_rh)
    if hc_tango_url is None:
        if not beamline.startswith("simulator") and beamline != "id30a2":
            raise Exception("Dehydraion device is None!")
        else:
            logger.info("Simulated beamline, not setting RH value")
            logger.info("Sleeping 2s")
            time.sleep(2)
    else:
        hc_proxy = PyTango.DeviceProxy(hc_tango_url)
        current_rh = read_hydration_level(beamline, target_rh)
        if abs(target_rh - current_rh) > precision:
            is_tango_failure = False
            try:
                hc_proxy.write_attribute("RH", target_rh)
                logger.info("RH value successfully transferred to dehydration device.")
            except BaseException as error:
                logger.warning("Tango failure: %r" % error)
                is_tango_failure = True
            if not is_tango_failure:
                logger.info("Waiting for dehydration device to reach target value.")
                btarget_rh_reached = False
                while not btarget_rh_reached:
                    n_tries = 10
                    index = 0
                    b_continue = True
                    while b_continue:
                        try:
                            current_rh = hc_proxy.read_attribute("RH").value
                            logger.info("Current RH = %.1f" % current_rh)
                            b_continue = False
                        except BaseException as error:
                            logger.warning(
                                "Could not read dehydration device RH value: "
                                + error
                                + "\nRe-trying in 10 seconds, please wait..."
                            )
                            time.sleep(10)
                            index += 1
                            if index == n_tries:
                                raise Exception(
                                    "ERROR! Could not read dehydration device "
                                    + "RH value after %d tries" % n_tries
                                )
                    if abs(target_rh - current_rh) < precision:
                        btarget_rh_reached = True
                    else:
                        logger.info(
                            "Target RH = %.1f, current RH = %.1f"
                            % (target_rh, current_rh)
                        )
                        time.sleep(1)
            if delay == -1:
                # Do not delay first iteration
                delay = 60
            else:
                logger.info("PAUSE: %f s" % delay)
                time.sleep(delay)
                logger.info("PAUSE: DONE")


def extract_results(beamline, startTime, currentRH, xsDataResultCharacterisation):
    """
    Extracts the results from the EDNA characterisation
    """
    logger = workflow_logging.getLogger()
    start_time = startTime
    current_rh = currentRH
    dict_result = {}
    dict_result["RH"] = current_rh
    dict_result["elapsedTime"] = round(time.time() - start_time, 3)
    if xsDataResultCharacterisation is not None:
        dataCollection = xsDataResultCharacterisation.dataCollection
        if dataCollection is not None:
            dict_result["imageName"] = os.path.basename(
                dataCollection.subWedge[0].image[0].path.value
            )
        if xsDataResultCharacterisation.imageQualityIndicators != []:
            imageQualityIndicators = (
                xsDataResultCharacterisation.imageQualityIndicators[0]
            )
            if imageQualityIndicators.dozor_score is not None:
                dict_result["dozor_score"] = imageQualityIndicators.dozor_score.value
            if imageQualityIndicators.method1Res is not None:
                dict_result["r1"] = imageQualityIndicators.method1Res.value
            if imageQualityIndicators.method2Res is not None:
                dict_result["r2"] = imageQualityIndicators.method2Res.value
            if imageQualityIndicators.spotTotal is not None:
                dict_result["spots"] = imageQualityIndicators.spotTotal.value
            if imageQualityIndicators.totalIntegratedSignal is not None:
                dict_result["TIS"] = imageQualityIndicators.totalIntegratedSignal.value
            if imageQualityIndicators.goodBraggCandidates is not None:
                dict_result["goodBraggCandidates"] = (
                    imageQualityIndicators.goodBraggCandidates.value
                )

            if xsDataResultCharacterisation.indexingResult is not None:
                indexingResult = xsDataResultCharacterisation.indexingResult
                selectedSolution = indexingResult.selectedSolution
                dict_result["length_a"] = selectedSolution.crystal.cell.length_a.value
                dict_result["length_b"] = selectedSolution.crystal.cell.length_b.value
                dict_result["length_c"] = selectedSolution.crystal.cell.length_c.value
                dict_result["mosaicity"] = selectedSolution.crystal.mosaicity.value
                if selectedSolution.crystal.spaceGroup is not None:
                    if selectedSolution.crystal.spaceGroup.name is not None:
                        dict_result["spaceGroup"] = (
                            selectedSolution.crystal.spaceGroup.name.value
                        )
                strategyResult = xsDataResultCharacterisation.strategyResult
                if strategyResult is not None:
                    listCollectionPlan = strategyResult.collectionPlan
                    if listCollectionPlan != []:
                        dict_result["rankingResolution"] = listCollectionPlan[
                            0
                        ].strategySummary.rankingResolution.value
    logger.debug("Dehydration analysis results: %s" % pprint.pformat(dict_result))
    return dict_result


def makePlotsAndWebPage(
    beamline, directory, workflow_working_dir, targetRH, currentRH, list_result
):
    """
    Creates the dehydration result plots and web page
    """
    logger = workflow_logging.getLogger()
    target_rh = targetRH
    current_rh = currentRH
    plot_dir = os.path.join(
        workflow_working_dir,
        "plots_target_rh_%.1f_current_rh_%.1f" % (target_rh, current_rh),
    )
    if os.path.exists(plot_dir):
        logger.warning("Directory %f already exists, plot files overwritten!")
    else:
        os.makedirs(plot_dir, 0o755)
    # Create input file
    result_text = ""
    for dict_result in list_result:
        elapsedTime = common.checkInputVariable(dict_result, "elapsedTime")
        current_rh = common.checkInputVariable(dict_result, "RH")
        dozor_score = common.checkInputVariable(
            dict_result, "dozor_score", 0.0, b_warn_if_default=False
        )
        method_1_res = common.checkInputVariable(
            dict_result, "r1", 0.0, b_warn_if_default=False
        )
        method_2_res = common.checkInputVariable(
            dict_result, "r2", 0.0, b_warn_if_default=False
        )
        spots = common.checkInputVariable(
            dict_result, "spots", 0, b_warn_if_default=False
        )
        goodBraggCandidates = common.checkInputVariable(
            dict_result, "goodBraggCandidates", 0, b_warn_if_default=False
        )
        tis = common.checkInputVariable(
            dict_result, "TIS", 0.0, b_warn_if_default=False
        )
        length_a = common.checkInputVariable(
            dict_result, "length_a", 0.0, b_warn_if_default=False
        )
        length_b = common.checkInputVariable(
            dict_result, "length_b", 0.0, b_warn_if_default=False
        )
        length_c = common.checkInputVariable(
            dict_result, "length_c", 0.0, b_warn_if_default=False
        )
        mosaicity = common.checkInputVariable(
            dict_result, "mosaicity", 0.0, b_warn_if_default=False
        )
        rankingResolution = common.checkInputVariable(
            dict_result, "rankingResolution", 0.0, b_warn_if_default=False
        )
        spaceGroup = common.checkInputVariable(
            dict_result, "spaceGroup", "", b_warn_if_default=False
        )
        result_text += (
            "%.2f %.2f %.2f %.2f %d %d %.1f %.3f %.3f %.3f %.2f %.3f %s  %.3f\n"
            % (
                elapsedTime,
                current_rh,
                method_1_res,
                method_2_res,
                spots,
                goodBraggCandidates,
                tis,
                length_a,
                length_b,
                length_c,
                mosaicity,
                rankingResolution,
                spaceGroup,
                dozor_score,
            )
        )
    data_file = open(os.path.join(plot_dir, "HC1_data.txt"), "w")
    data_file.write(result_text)
    data_file.close()

    print(result_text)
    hc1_gnu_cell_script = """#set termoption dash
    #
    #Plot 1 = RH vs Resolution and Mosaic spread
    #
    #set terminal postscript color eps enhanced
    #set output "HC1_expt_Res.ps"
    set terminal png
    set output "HC1_expt_Res.png"
    set grid x y2
    set xlabel "Relative Humidity (%)"
    set ylabel "Resolution (A)"
    set y2label "Mosaic Spread"
    set autoscale  y
    set xrange [] reverse
    set yrange [] reverse
    set autoscale  x
    set autoscale y2
    #set xrange [0:5000000]
    #set xrange [50:100]
    set bmargin 7
    set ytics
    set xtics
    set y2tics
    set style line 1 lt 1 lw 3
    set style line 2 lt 1 lw 3
    set style line 3 lt 1 lw 3
    set style line 4 lt 1 lw 3
    #
    set title "Effect of dehydration on resolution"
    unset colorbox

    plot          "HC1_data.txt" using 2:3 title 'labelit r1' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 2:4 title 'labelit r2' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 2:12 title 'Ranking resolution' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 2:11 title 'Mosaic Spread' axes x1y2 with linespoints

    #
    #Plot 2 = RH vs Cell edges and TIS
    #

    #set termoption dash
    #set terminal postscript color eps enhanced
    #set output "HC1_expt_Cell.ps"
    set terminal png
    set output "HC1_expt_Cell.png"
    set grid x y2
    set xlabel "Relative Humidity (%)"
    set ylabel "Unit cell axes (A)"
    set y2label "Total integrated signal"
    set autoscale y
    set yrange [] noreverse
    set xrange [] reverse
    set autoscale  x
    set autoscale y2
    #set xrange [0:5000000]
    #set xrange [50:100]
    set bmargin 7
    set ytics
    set xtics
    set y2tics
    set style line 1 lt 1 lw 3
    set style line 2 lt 1 lw 3
    set style line 3 lt 1 lw 3
    set style line 4 lt 1 lw 3
    #
    set title "Effect of dehydration on crystal parameters"
    unset colorbox

    plot          "HC1_data.txt" using 2:8 title 'a cell edge' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 2:9 title 'b cell edge' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 2:10 title 'c cell edge' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 2:7 title 'Total Integrated Signal' axes x1y2 with linespoints



    #
    #Plot 3 = RH vs spots and TIS
    #

    #set termoption dash
    #set terminal postscript color eps enhanced
    #set output "HC1_expt_Labelit.ps"
    set terminal png
    set output "HC1_expt_Labelit.png"
    set grid x y2
    set xlabel "Relative Humidity (%)"
    set ylabel "Number of Bragg peaks or spots"
    set y2label "Total Integrated Signal"
    set autoscale  y
    set yrange [] noreverse
    set xrange [] reverse
    set autoscale  x
    set autoscale y2
    #set xrange [0:5000000]
    #set xrange [50:100]
    set bmargin 7
    set ytics
    set xtics
    set y2tics
    set style line 1 lt 1 lw 3
    set style line 2 lt 1 lw 3
    set style line 3 lt 1 lw 3
    set style line 4 lt 1 lw 3
    #
    set title "Effect of dehydration on resolution"
    unset colorbox

    plot          "HC1_data.txt" using 2:5 title 'Number of Spots' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 2:6 title 'Bragg peaks' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 2:7 title 'Total integrated signal' axes x1y2 with linespoints
    #


    #
    #Plot 4 = Time vs RH and Resolution
    #

    #set termoption dash
    #set terminal postscript color eps enhanced
    #set output "HC1_expt_Resolution_time.ps"
    set terminal png
    set output "HC1_expt_Resolution_time.png"
    set grid x y2
    set xlabel "Time (seconds)"
    set ylabel "Relative humidity (%)"
    set y2label "Resolution (A)"
    set autoscale  y
    set xrange [] noreverse
    set yrange [] noreverse
    set y2range [] reverse
    set autoscale  x
    set autoscale y2
    #set xrange [0:5000000]
    #set xrange [50:100]
    set bmargin 7
    set ytics
    set xtics
    set y2tics
    set style line 1 lt 1 lw 3
    set style line 2 lt 1 lw 3
    set style line 3 lt 1 lw 3
    set style line 4 lt 1 lw 3
    #
    set title "Effect of dehydration on crystal parameters"
    unset colorbox

    plot          "HC1_data.txt" using 1:2 title 'Relative humidity' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 1:3 title 'Labelit r1' axes x1y2 with linespoints,\
                  "HC1_data.txt" using 1:4 title 'Labelit r2' axes x1y2 with linespoints,\
                  "HC1_data.txt" using 1:12 title 'BEST RR' axes x1y2 with linespoints



    #
    #Plot 5 = Time vs RH and Cell
    #

    #set termoption dash
    #set terminal postscript color eps enhanced
    #set output "HC1_expt_Cell_time.ps"
    set terminal png
    set output "HC1_expt_Cell_time.png"
    set grid x y2
    set xlabel "Time (seconds)"
    set ylabel "Relative humidity (%)"
    set y2label "Unit cell edges(A)"
    set autoscale  y
    set xrange [] noreverse
    set yrange [] noreverse
    set y2range [] noreverse
    set autoscale  x
    set autoscale y2
    #set xrange [0:5000000]
    #set xrange [50:100]
    set bmargin 7
    set ytics
    set xtics
    set y2tics
    set style line 1 lt 1 lw 3
    set style line 2 lt 1 lw 3
    set style line 3 lt 1 lw 3
    set style line 4 lt 1 lw 3
    #
    set title "Effect of dehydration on crystal parameters"
    unset colorbox

    plot          "HC1_data.txt" using 1:2 title 'Relative humidity' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 1:8 title 'a cell edge' axes x1y2 with linespoints,\
                  "HC1_data.txt" using 1:9 title 'b cell edge' axes x1y2 with linespoints,\
                  "HC1_data.txt" using 1:10 title 'c cell edge' axes x1y2 with linespoints



    #
    #Plot 6 = Time vs RH and Spots
    #


    #set termoption dash
    #set terminal postscript color eps enhanced
    #set output "HC1_expt_spots_time.ps"
    set terminal png
    set output "HC1_expt_spots_time.png"
    set grid x y2
    set xlabel "Time"
    set ylabel "Relative Humidity (%)"
    set y2label "Number of Bragg peaks or spots"
    set autoscale  y
    set xrange [] noreverse
    set yrange [] noreverse
    set y2range [] noreverse
    set autoscale  x
    set autoscale y2
    #set xrange [0:5000000]
    #set xrange [50:100]
    set bmargin 7
    set ytics
    set xtics
    set y2tics
    set style line 1 lt 1 lw 3
    set style line 2 lt 1 lw 3
    set style line 3 lt 1 lw 3
    set style line 4 lt 1 lw 3
    #
    set title "Effect of dehydration on resolution"
    unset colorbox

    plot          "HC1_data.txt" using 1:2 title 'Relative Humidity' axes x1y1 with linespoints,\
                  "HC1_data.txt" using 1:5 title 'Number of Spots' axes x1y2 with linespoints,\
                  "HC1_data.txt" using 1:6 title 'Bragg peaks' axes x1y2 with linespoints
    """
    strScriptGnuCell = os.path.join(plot_dir, "hc1_gnu_cell.sh")
    data_file = open(strScriptGnuCell, "w")
    data_file.write(hc1_gnu_cell_script)
    data_file.close()
    os.chdir(plot_dir)
    os.system(
        "/opt/pxsoft/tools/gnuplot/v4.6.6/debian80-x86_64/gnuplot %s" % strScriptGnuCell
    )
    # Create web page
    workflowStepReport = WorkflowStepReport("Dehydration", "Dehydration Results")
    page = markupv1_10.page(mode="loose_html")
    page.init(title="Mesh Results", footer="Generated on %s" % time.asctime())
    page.div(align_="CENTER")
    page.h1()
    page.strong("Dehydration Results")
    page.h1.close()
    page.table(class_="imageTable")
    dictGraphFile = {
        "HC1_expt_Res.png": "Effect of dehydration on resolution",
        "HC1_expt_Cell.png": "Effect of dehydration on crystal parameters",
        "HC1_expt_Labelit.png": "Effect of dehydration on resolution",
        "HC1_expt_Resolution_time.png": "Effect of dehydration on crystal parameters",
        "HC1_expt_Cell_time.png": "Effect of dehydration on crystal parameters",
        "HC1_expt_spots_time.png": "Effect of dehydration on resolution",
    }
    firstPlotName = list(dictGraphFile.keys())[0]
    indexX = 0
    iNumberOfImagesPerRow = 2
    page.tr(align_="CENTER")
    workflowStepReport.startImageList()
    for graphFile in dictGraphFile.keys():
        page.td()
        page.a(href=graphFile)
        page.img(
            src=os.path.basename(graphFile), width=512, title=dictGraphFile[graphFile]
        )
        page.a.close()
        page.td.close()
        indexX += 1
        workflowStepReport.addImage(graphFile, dictGraphFile[graphFile])
        if indexX == iNumberOfImagesPerRow:
            page.tr.close()
            page.tr(align_="CENTER")
            indexX = 0
            workflowStepReport.endImageList()
            workflowStepReport.startImageList()
    workflowStepReport.endImageList()
    page.tr.close()
    page.table.close()
    page.div.close()
    # Make a table of all values
    page.table(class_="dehydrationResults", border_="1", cellpadding_="0")
    page.tr(align_="CENTER", bgcolor_="#F0F0FF")
    listColumn = []
    listColumn.append("Rel. hum. [%]")
    listColumn.append("Time [s]")
    listColumn.append("Image")
    listColumn.append("Dozor score")
    listColumn.append("Res. 1")
    listColumn.append("Res. 2")
    listColumn.append("Spots")
    listColumn.append("Bragg cand.")
    listColumn.append("Cell a")
    listColumn.append("Cell b")
    listColumn.append("Cell c")
    listColumn.append("Mosaicity")
    listColumn.append("Ranking res.")
    listColumn.append("Sym.")
    listData = []
    for dict_result in list_result:
        listDataRow = []
        listDataRow.append(dict_result["RH"])
        listDataRow.append("%.1f" % dict_result["elapsedTime"])
        if "imageName" in dict_result:
            listDataRow.append(dict_result["imageName"])
        else:
            listDataRow.append("_")
        if "dozor_score" in dict_result:
            listDataRow.append(dict_result["dozor_score"])
        else:
            listDataRow.append("_")
        if "r1" in dict_result:
            listDataRow.append(dict_result["r1"])
        else:
            listDataRow.append("_")
        if "r2" in dict_result:
            listDataRow.append(dict_result["r2"])
        else:
            listDataRow.append("_")
        if "spots" in dict_result:
            listDataRow.append(dict_result["spots"])
        else:
            listDataRow.append("_")
        if "goodBraggCandidates" in dict_result:
            listDataRow.append(dict_result["goodBraggCandidates"])
        else:
            listDataRow.append("_")
        if "length_a" in dict_result:
            listDataRow.append("%.3f" % dict_result["length_a"])
        else:
            listDataRow.append("_")
        if "length_b" in dict_result:
            listDataRow.append("%.3f" % dict_result["length_b"])
        else:
            listDataRow.append("_")
        if "length_c" in dict_result:
            listDataRow.append("%.3f" % dict_result["length_c"])
        else:
            listDataRow.append("_")
        if "mosaicity" in dict_result:
            listDataRow.append(dict_result["mosaicity"])
        else:
            listDataRow.append("_")
        if "rankingResolution" in dict_result:
            listDataRow.append(dict_result["rankingResolution"])
        else:
            listDataRow.append("_")
        if "spaceGroup" in dict_result:
            listDataRow.append(dict_result["spaceGroup"])
        else:
            listDataRow.append("_")
        listData.append(listDataRow)
    workflowStepReport.addTable("Dehydration results", listColumn, listData)
    #
    listColumn = ["Raw data directory", "Dehydration working directory"]
    listData = [[directory, workflow_working_dir]]
    workflowStepReport.addTable("File info", listColumn, listData)
    #
    page_path = os.path.join(plot_dir, "index.html")
    workflowStepReport.renderHtml(plot_dir, "index.html")
    workflowStepReport.renderJson(plot_dir)
    return page_path, firstPlotName
