#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "11/10/2019"


import os
import shlex
import threading
import subprocess


# From http://stackoverflow.com/questions/1191374/using-module-subprocess-with-timeout
def runCommand(command, timeout_sec=5, cwd=None):
    stdout = None
    stderr = None
    proc = subprocess.Popen(
        shlex.split(command), stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd
    )

    def kill_proc(p):
        return p.kill()

    timer = threading.Timer(timeout_sec, kill_proc, [proc])
    try:
        timer.start()
        binaryStdout, binaryStderr = proc.communicate()
        stdout = binaryStdout.decode("utf-8")
        stderr = binaryStderr.decode("utf-8")
    finally:
        timer.cancel()
    return stdout, stderr


def submitJobToSLURM(
    command_line,
    working_directory,
    nodes=1,
    core=4,
    time="2:00:00",
    host=None,
    queue=None,
    name=None,
    mem=None,
):
    slurm_id = None
    script = "#!/bin/bash\n"
    if name is not None:
        script += '#SBATCH --job-name="{0}"\n'.format(name)
    if queue is None or queue == "mx":
        partition = "mx"
    else:
        partition = "{0}".format(queue)
    script += "#SBATCH --partition={0}\n".format(partition)
    if mem is None:
        mem = 4000  # 4 Gb memory by default
    script += "#SBATCH --mem={0}\n".format(mem)
    script += "#SBATCH --ntasks={0}\n".format(nodes)
    script += "#SBATCH --nodes=1\n"  # Necessary for not splitting jobs! See ATF-57
    script += "#SBATCH --cpus-per-task={0}\n".format(core)
    script += "#SBATCH --time={0}\n".format(time)
    script += "#SBATCH --output={0}/stdout.txt\n".format(working_directory)
    script += "#SBATCH --error={0}/stderr.txt\n".format(working_directory)
    script += command_line + "\n"
    scriptName = "slurm.sh"
    slurm_script_path = os.path.join(working_directory, scriptName)
    with open(slurm_script_path, "w") as f:
        f.write(script)
    stdout, stderr = runCommand("sbatch {0}".format(slurm_script_path))
    if "Submitted batch job" in stdout:
        slurm_id = int(stdout.split("job")[1])
    return slurm_script_path, slurm_id, stdout, stderr


def splitAtEqual(line, dictLine={}):
    if "=" in line:
        key, value = line.split("=", 1)
        if "=" in value:
            if " " in value:
                part1, part2 = value.split(" ", 1)
            elif "," in value:
                part1, part2 = value.split(",", 1)
            dictLine[key.strip()] = part1.strip()
            dictLine = splitAtEqual(part2, dictLine=dictLine)
        else:
            dictLine[key.strip()] = value.strip()
    return dictLine


def parseSLURMStat(slurmStat):
    listLines = slurmStat.split("\n")
    dictSLURMStat = {}
    for line in listLines:
        if line != "":
            dictLine = splitAtEqual(line)
            dictSLURMStat.update(dictLine)
    return dictSLURMStat


def getSLURMStat(slurmJobId):
    dictStat = None
    stdout, stderr = runCommand("scontrol show job {0}".format(slurmJobId))
    if stderr is None or stderr == "":
        dictStat = parseSLURMStat(stdout)
    return dictStat


def areJobsPending(partitionName=None):
    jobsArePending = False
    if partitionName is not None:
        stdout, stderr = runCommand("squeue -p {0} -t PENDING".format(partitionName))
    else:
        stdout, stderr = runCommand("squeue -t PENDING")
    listLines = stdout.split("\n")
    noPending = 0
    for line in listLines:
        if "Resources" in line or "Priority" in line:
            noPending += 1
    if noPending > 2:
        jobsArePending = True
    return jobsArePending
