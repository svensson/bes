#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "01/03/2021"

import os
import json
import time
import shutil
import tempfile
import xmltodict
import subprocess
from typing import Optional, Tuple, List

from bes.workflow_lib import path
from bes import config
from bes.workflow_lib import symmetry
from bes.workflow_lib import cbf_utils
from bes.workflow_lib import edna_kernel
from bes.workflow_lib import workflow_logging
from bes.workflow_lib.report import WorkflowStepReport
from bes.workflow_lib.edna_kernel import EDFactoryPluginStatic

from XSDataCommon import XSDataString
from XSDataCommon import XSDataImage
from XSDataCommon import XSDataInteger
from XSDataCommon import XSDataDouble
from XSDataCommon import XSDataFile
from XSDataCommon import XSDataTime
from XSDataCommon import XSDataAngularSpeed
from XSDataCommon import XSDataAngle
from XSDataCommon import XSDataLength
from XSDataCommon import XSDataFloat
from XSDataCommon import XSDataBoolean
from XSDataCommon import XSDataSize

EDFactoryPluginStatic.loadModule("XSDataMXv1")
from XSDataMXv1 import XSDataInputControlImageQualityIndicators  # noqa: E402
from XSDataMXv1 import XSDataResultControlImageQualityIndicators  # noqa: E402
from XSDataMXv1 import XSDataResultStrategy  # noqa: E402
from XSDataMXv1 import XSDataCollectionPlan  # noqa: E402
from XSDataMXv1 import XSDataCollection  # noqa: E402
from XSDataMXv1 import XSDataSubWedge  # noqa: E402
from XSDataMXv1 import XSDataExperimentalCondition  # noqa: E402
from XSDataMXv1 import XSDataBeam  # noqa: E402
from XSDataMXv1 import XSDataGoniostat  # noqa: E402
from XSDataMXv1 import XSDataResultCharacterisation  # noqa: E402
from XSDataMXv1 import XSDataDiffractionPlan  # noqa: E402
from XSDataMXv1 import XSDataSampleCrystalMM  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataInterfacev1_2")
import XSDataInterfacev1_2  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataInterfacev1_3")
import XSDataInterfacev1_3  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataMXv2")
from XSDataMXv2 import XSDataResultCharacterisationv2_0  # noqa: E402
from XSDataMXv2 import XSDataCollection as XSDataCollectionv2  # noqa: E402
from XSDataMXv2 import kappa_alignment_response  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataInterfacev2_2")
from XSDataInterfacev2_2 import XSDataInputInterfacev2_2  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataBurnStrategyv1_0")
from XSDataBurnStrategyv1_0 import XSDataInputXdsBurnStrategy  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataRdfitv1_0")
from XSDataRdfitv1_0 import XSDataInputRdfit  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataSimpleHTMLPagev1_1")
from XSDataSimpleHTMLPagev1_1 import XSDataInputSimpleHTMLPage  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataSimpleHTMLPagev2_1")
from XSDataSimpleHTMLPagev2_1 import (  # noqa: E402
    XSDataInputSimpleHTMLPage as XSDataInputSimpleHTMLPagev2_1,  # noqa: E402
)  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataPyarchThumbnailGeneratorv1_0")
from XSDataPyarchThumbnailGeneratorv1_0 import (  # noqa: E402
    XSDataInputPyarchThumbnailGeneratorParallel,  # noqa: E402
)  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataControlDozorv1_0")
from XSDataControlDozorv1_0 import XSDataInputControlDozor  # noqa: E402
from XSDataControlDozorv1_0 import XSDataResultControlDozor  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataControlH5ToCBFv1_1")
from XSDataControlH5ToCBFv1_1 import XSDataInputControlH5ToCBF  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataMXWaitFilev1_1")
from XSDataMXWaitFilev1_1 import XSDataInputMXWaitFile  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataCCP4v1_0")
from XSDataCCP4v1_0 import XSDataInputDimple  # noqa: E402

EDFactoryPluginStatic.loadModule("XSDataFbestv1_0")
from XSDataFbestv1_0 import XSDataInputFbest  # noqa: E402


def extractBeamlineFromDirectory(directory):
    beamline = None
    listDirectory = directory.split(os.sep)
    # First check: directory must start with "data":
    if listDirectory[1] == "data" and listDirectory[2] == "gz":
        # Then check if second level is "visitor":
        if listDirectory[3] == "visitor":
            beamline = listDirectory[5]
        elif listDirectory[4] == "inhouse":
            beamline = listDirectory[3]
    elif listDirectory[1] == "data":
        # Then check if second level is "visitor":
        if listDirectory[2] == "visitor":
            beamline = listDirectory[4]
        elif listDirectory[3] == "inhouse":
            beamline = listDirectory[2]
    if beamline is None:
        raise Exception("Could not extract beamline from path %s" % directory)
    return beamline


EDFactoryPluginStatic.loadModule("XSDataEDNAprocv1_0")
from XSDataEDNAprocv1_0 import XSDataEDNAprocInput  # noqa: E402


def create_ref_data_collect(
    exposureTime,
    transmission,
    osc_range,
    no_reference_images,
    angle_between_reference_images,
    phi,
):

    xsDataResultStrategy = XSDataResultStrategy()
    xsDataCollectionPlan = XSDataCollectionPlan()
    comment = XSDataString()
    comment.setValue("Workflow reference images")
    xsDataCollectionPlan.setComment(comment)
    plannum = XSDataInteger()
    plannum.setValue(1)
    xsDataCollectionPlan.setCollectionPlanNumber(plannum)

    subwedge = XSDataSubWedge()

    expCond = XSDataExperimentalCondition()

    beam = XSDataBeam()
    etime = XSDataTime()
    etime.setValue(exposureTime)
    beam.setExposureTime(etime)
    # xsBeamsize=XSDataSize()
    # d=XSDataLength();d.setValue(beamSize[0]);xsBeamsize.setX(d);d=XSDataLength();d.setValue(beamSize[1]);xsBeamsize.setY(d);d=XSDataLength();d.setValue(0);xsBeamsize.setZ(d);
    # beam.setSize(xsBeamsize)
    d = XSDataDouble()
    d.setValue(transmission)
    beam.setTransmission(d)
    expCond.setBeam(beam)

    goniostat = XSDataGoniostat()
    goniostat.setOscillationWidth(XSDataAngle(osc_range))
    goniostat.setRotationAxisStart(XSDataAngle(phi))
    goniostat.setRotationAxisEnd(XSDataAngle(phi + osc_range * no_reference_images))
    goniostat.setOverlap(XSDataAngle(osc_range - angle_between_reference_images))
    expCond.setGoniostat(goniostat)

    subwedge.setExperimentalCondition(expCond)

    xsDataCollectionStrategy = XSDataCollection()
    xsDataCollectionStrategy.addSubWedge(subwedge)
    xsDataCollectionPlan.setCollectionStrategy(xsDataCollectionStrategy)
    xsDataResultStrategy.addCollectionPlan(xsDataCollectionPlan)

    mxv1StrategyResult = str(xsDataResultStrategy.marshal())
    return mxv1StrategyResult


def create_fine_sliced_ref_data_collect(
    exposureTime,
    transmission,
    osc_range,
    no_reference_images,
    angle_between_reference_images,
    phi,
):

    xsDataResultStrategy = XSDataResultStrategy()
    xsDataCollectionPlan = XSDataCollectionPlan()
    comment = XSDataString()
    comment.setValue("Workflow reference images")
    xsDataCollectionPlan.setComment(comment)
    plannum = XSDataInteger()
    plannum.setValue(1)
    xsDataCollectionPlan.setCollectionPlanNumber(plannum)
    xsDataCollectionStrategy = XSDataCollection()

    for subwedgeNo in range(no_reference_images):
        subwedge = XSDataSubWedge()

        expCond = XSDataExperimentalCondition()

        beam = XSDataBeam()
        etime = XSDataTime()
        etime.setValue(exposureTime / 10)
        beam.setExposureTime(etime)
        # xsBeamsize=XSDataSize()
        # d=XSDataLength();d.setValue(beamSize[0]);xsBeamsize.setX(d);d=XSDataLength();d.setValue(beamSize[1]);xsBeamsize.setY(d);d=XSDataLength();d.setValue(0);xsBeamsize.setZ(d);
        # beam.setSize(xsBeamsize)
        d = XSDataDouble()
        d.setValue(transmission)
        beam.setTransmission(d)
        expCond.setBeam(beam)

        goniostat = XSDataGoniostat()
        goniostat.setOscillationWidth(XSDataAngle(osc_range / 10))
        goniostat.setRotationAxisStart(
            XSDataAngle(phi + subwedgeNo * angle_between_reference_images)
        )
        goniostat.setRotationAxisEnd(
            XSDataAngle(phi + subwedgeNo * angle_between_reference_images + osc_range)
        )

        expCond.setGoniostat(goniostat)

        subwedge.setExperimentalCondition(expCond)

        xsDataCollectionStrategy.addSubWedge(subwedge)
    xsDataCollectionPlan.setCollectionStrategy(xsDataCollectionStrategy)
    xsDataResultStrategy.addCollectionPlan(xsDataCollectionPlan)

    mxv1StrategyResult = str(xsDataResultStrategy.marshal())
    return mxv1StrategyResult


def create_n_images_data_collect(
    exposureTime, transmission, number_of_images, osc_range, phi
):
    xsDataResultStrategy = XSDataResultStrategy()
    xsDataCollectionPlan = XSDataCollectionPlan()
    comment = XSDataString()
    comment.setValue("Workflow %d image data collect" % number_of_images)
    xsDataCollectionPlan.setComment(comment)
    plannum = XSDataInteger()
    plannum.setValue(1)
    xsDataCollectionPlan.setCollectionPlanNumber(plannum)

    subwedge = XSDataSubWedge()

    expCond = XSDataExperimentalCondition()

    beam = XSDataBeam()
    etime = XSDataTime()
    etime.setValue(exposureTime)
    beam.setExposureTime(etime)
    # xsBeamsize=XSDataSize()
    # d=XSDataLength();d.setValue(beamSize[0]);xsBeamsize.setX(d);d=XSDataLength();d.setValue(beamSize[1]);xsBeamsize.setY(d);d=XSDataLength();d.setValue(0);xsBeamsize.setZ(d);
    # beam.setSize(xsBeamsize)
    d = XSDataDouble()
    d.setValue(transmission)
    beam.setTransmission(d)
    expCond.setBeam(beam)

    goniostat = XSDataGoniostat()
    # Horrible hack for passing number of images in case of zero osc range
    if osc_range < 0.001:
        osc_range = 0.0
        rotationAxisEnd = number_of_images
    else:
        rotationAxisEnd = phi + osc_range * number_of_images
    goniostat.setOscillationWidth(XSDataAngle(osc_range))
    goniostat.setRotationAxisStart(XSDataAngle(phi))
    goniostat.setRotationAxisEnd(XSDataAngle(rotationAxisEnd))
    goniostat.setOverlap(XSDataAngle(0.0))
    expCond.setGoniostat(goniostat)

    subwedge.setExperimentalCondition(expCond)

    xsDataCollectionStrategy = XSDataCollection()
    xsDataCollectionStrategy.addSubWedge(subwedge)
    xsDataCollectionPlan.setCollectionStrategy(xsDataCollectionStrategy)
    xsDataResultStrategy.addCollectionPlan(xsDataCollectionPlan)

    mxv1StrategyResult = str(xsDataResultStrategy.marshal())
    return mxv1StrategyResult


def parseMxv1StrategyResult(mxv1StrategyResult):
    xsDataResultStrategy = XSDataResultStrategy.parseString(mxv1StrategyResult)
    return xsDataResultStrategy


def getCellSpaceGroupFromCharacterisationResult(beamline, mxv1ResultCharacterisation):
    dictCellSpaceGroup = {}
    logger = workflow_logging.getLogger()
    try:
        if mxv1ResultCharacterisation is not None:
            xsDataResultCharacterisation = XSDataResultCharacterisation.parseString(
                mxv1ResultCharacterisation
            )
            xsDataResultIndexing = xsDataResultCharacterisation.indexingResult
            if xsDataResultIndexing is not None:
                xsDataSolutionSelected = xsDataResultIndexing.selectedSolution
                xsDataCrystal = xsDataSolutionSelected.crystal
                xsDataCell = xsDataCrystal.cell
                dictCellSpaceGroup = {
                    "spaceGroup": str(xsDataCrystal.spaceGroup.name.value),
                    "cell_a": float(xsDataCell.length_a.value),
                    "cell_b": float(xsDataCell.length_b.value),
                    "cell_c": float(xsDataCell.length_c.value),
                    "cell_alpha": float(xsDataCell.angle_alpha.value),
                    "cell_beta": float(xsDataCell.angle_beta.value),
                    "cell_gamma": float(xsDataCell.angle_gamma.value),
                }
    except BaseException:
        logger.warning("Couldn't parse characterisation results!")
        logger.debug("mxv1ResultCharacterisation: %r" % mxv1ResultCharacterisation)
    return dictCellSpaceGroup


def createEdnaImagePath(
    beamline, directory, prefix, expTypePrefix, suffix, no_reference_images, run_number
):
    ednaImagePath = ""
    logger = workflow_logging.getLogger()
    logger.info("Image paths to be collected and then processsed by EDNA:")
    for i in range(no_reference_images):
        ednaImagePath += "<imagePath>\n"
        ednaImagePath += "  <path>\n"
        strImageName = "%s/%s%s_%d_%04d.%s" % (
            directory,
            expTypePrefix,
            prefix,
            run_number,
            i + 1,
            suffix,
        )
        logger.info("Image %d: %s" % (i, strImageName))
        ednaImagePath += "    <value>%s</value>\n" % strImageName
        ednaImagePath += "  </path>\n"
        ednaImagePath += "</imagePath>\n"
    return ednaImagePath


def storeImageQualityIndicators(
    beamline,
    mxv1StrategyResult,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    workflow_working_dir,
):
    edna_kernel.initSimulation(beamline)
    listImagePath = getListImagePath(
        mxv1StrategyResult, directory, run_number, expTypePrefix, prefix, suffix
    )

    xsDataInputControlImageQualityIndicators = (
        XSDataInputControlImageQualityIndicators()
    )
    for strImagePath in listImagePath:
        xsDataInputControlImageQualityIndicators.addImage(
            XSDataImage(XSDataString(strImagePath))
        )

    edna_kernel.executeEdnaPlugin(
        "EDPluginControlImageQualityIndicatorsv1_2",
        xsDataInputControlImageQualityIndicators,
        workflow_working_dir,
    )


def getListImagePath(
    mxv1StrategyResult,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    bIgnoreBurnImages=True,
    bFirstImageOnly=False,
):
    # logger = workflow_logging.getLogger()
    xsDataResultStrategy = parseMxv1StrategyResult(mxv1StrategyResult)
    listImagePath = []
    for xsDataCollectionPlan in xsDataResultStrategy.getCollectionPlan():
        xsDataCollectionStrategy = xsDataCollectionPlan.getCollectionStrategy()
        runNumberDataCollection = (
            int(run_number)
            + xsDataCollectionPlan.getCollectionPlanNumber().getValue()
            - 1
        )
        # Then we loop over all the sub wedges
        iWedgeID = 0
        for xsDataSubWedge in xsDataCollectionStrategy.subWedge:
            listImageInSubWedge = []
            iWedgeID += 1
            #            logger.info(xsDataSubWedge.marshal()
            bAddImage = True
            if bIgnoreBurnImages:
                if xsDataSubWedge.action is not None:
                    if xsDataSubWedge.action.value == "burn":
                        bAddImage = False
            if bAddImage:
                #                logger.info("Wedge Number: "+str(wedgeID)
                if len(xsDataCollectionStrategy.subWedge) > 1:
                    wedgeID = "w%d" % iWedgeID
                else:
                    wedgeID = ""
                strTotalPrefix = "%s%s%s" % (expTypePrefix, prefix, wedgeID)
                #                logger.info(strTotalPrefix
                if bFirstImageOnly:
                    noImages = 1
                else:
                    xsDataExperimentalCondition = (
                        xsDataSubWedge.getExperimentalCondition()
                    )
                    oscillationWidth = (
                        xsDataExperimentalCondition.getGoniostat()
                        .getOscillationWidth()
                        .getValue()
                    )
                    rotationAxisStart = (
                        xsDataExperimentalCondition.getGoniostat()
                        .getRotationAxisStart()
                        .getValue()
                    )
                    rotationAxisEnd = (
                        xsDataExperimentalCondition.getGoniostat()
                        .getRotationAxisEnd()
                        .getValue()
                    )
                    if oscillationWidth > 0.05:
                        noImages = max(
                            1,
                            int(
                                (rotationAxisEnd - rotationAxisStart) / oscillationWidth
                                + 0.5
                            ),
                        )
                    else:
                        # Still image
                        noImages = 1
                for iImageNumber in range(1, noImages + 1):
                    strImageName = "%s_%s_%04d.%s" % (
                        strTotalPrefix,
                        runNumberDataCollection,
                        iImageNumber,
                        suffix,
                    )
                    strPathToImage = os.path.join(directory, strImageName)
                    # logger.info("Adding image {0}".format(strPathToImage))
                    listImageInSubWedge.append(strPathToImage)
                listImagePath += listImageInSubWedge
    return listImagePath


def getListFineSlicedReferenceImagePath(
    beamline: str,
    mxv1StrategyResult: str,
    characterisationOscillationRange: float,
    directory: str,
    run_number: int,
    expTypePrefix: str,
    prefix: str,
    suffix: str,
    dest_dir: Optional[str] = None,
) -> Tuple[List[str], str]:
    logger = workflow_logging.getLogger()

    if dest_dir is None:
        dest_dir = directory

    # Get the phi start
    result_strategy = parseMxv1StrategyResult(mxv1StrategyResult)
    collection_plan = result_strategy.collectionPlan[0]
    collection_strategy = collection_plan.collectionStrategy
    sub_wedge = collection_strategy.subWedge[0]
    experimental_condition = sub_wedge.experimentalCondition
    rotation_axis_start = experimental_condition.goniostat.rotationAxisStart.value

    no_sub_wedges = 4
    no_images_in_sub_wedge = 10
    sub_wedge_angle_increment = 90  # deg
    file_index_offset = 1

    first_image_path = None
    list_image_path = []
    for sub_wedge_no in range(no_sub_wedges):
        # File name to save the result
        reference_image_file_name = "ref-{0}_{1}_{2:04d}.cbf".format(
            prefix, run_number, sub_wedge_no + file_index_offset
        )
        logger.info("New reference file name: %s", reference_image_file_name)
        reference_image_path = os.path.join(dest_dir, reference_image_file_name)

        # Get all image paths in the sub wedge
        list_paths_in_sub_wedge = []
        for image_idx_in_sub_wedge in range(no_images_in_sub_wedge):
            if beamline in ["id30a1", "id30a2"]:
                image_number = (
                    sub_wedge_no * no_images_in_sub_wedge
                    + image_idx_in_sub_wedge
                    + file_index_offset
                )
                wedge_number = ""
            else:
                image_number = image_idx_in_sub_wedge + file_index_offset
                wedge_number = f"w{sub_wedge_no + file_index_offset}"
            image_name = "%s%s%s_%s_%04d.%s" % (
                expTypePrefix,
                prefix,
                wedge_number,
                run_number,
                image_number,
                suffix,
            )
            logger.info("Reference file name: %s", image_name)
            image_path = os.path.join(directory, image_name)
            if first_image_path is None:
                first_image_path = image_path
            list_paths_in_sub_wedge.append(image_path)

        # Sum all images in the sub wedge
        angle_start = rotation_axis_start + sub_wedge_angle_increment * sub_wedge_no
        angle_increment = characterisationOscillationRange
        sumCbfImage = cbf_utils.sum_cbf_images(
            list_paths_in_sub_wedge, angle_start, angle_increment, preserve_dtype=True
        )

        # Save the sub wedge image
        sumCbfImage.write(reference_image_path)
        list_image_path.append(reference_image_path)
        logger.info("%s file written!", reference_image_path)

    return list_image_path, first_image_path


def processBurnStrategy(
    beamline,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    mxv1ResultCharacterisation,
    workflow_working_dir,
):
    logger = workflow_logging.getLogger()
    xsDataResultCharacterisation = XSDataResultCharacterisation.parseString(
        mxv1ResultCharacterisation
    )
    xsDataResultStrategy = xsDataResultCharacterisation.getStrategyResult()
    xsDataIndexingResult = xsDataResultCharacterisation.indexingResult
    selectedSolution = xsDataIndexingResult.selectedSolution
    crystal = selectedSolution.crystal
    logger.debug("Selected indexing solution: %r" % crystal.marshal())
    iSpaceGroupITNumber = crystal.spaceGroup.ITNumber.value
    fCellLengthA = crystal.cell.length_a.value
    fCellLengthB = crystal.cell.length_b.value
    fCellLengthC = crystal.cell.length_c.value
    fCellAngleAlpha = crystal.cell.angle_alpha.value
    fCellAngleBeta = crystal.cell.angle_beta.value
    fCellAngleGamma = crystal.cell.angle_gamma.value

    xsDataInputRdfit = XSDataInputRdfit()
    if xsDataResultStrategy.bestLogFile is not None:
        strPathToBestLogFile = xsDataResultStrategy.bestLogFile.path.value
        strPathToBestXmlFile = os.path.join(
            os.path.dirname(strPathToBestLogFile), "Bestv1_2_dnaTables.xml"
        )
        logger.debug("Path to BEST XML file: %r" % crystal.marshal())

        # First we loop over all the collection plans
        for xsDataCollectionPlan in xsDataResultStrategy.getCollectionPlan():
            xsDataCollectionStrategy = xsDataCollectionPlan.getCollectionStrategy()
            runNumberDataCollection = (
                run_number
                + xsDataCollectionPlan.getCollectionPlanNumber().getValue()
                - 1
            )

            # Then we loop over all the sub wedges
            wedgeID = 0
            xsDataInputRdfit.bestXmlFile = XSDataFile(
                XSDataString(strPathToBestXmlFile)
            )

            for xsDataSubWedge in xsDataCollectionStrategy.getSubWedge():
                wedgeID += 1
                logger.debug(
                    "Subwedge Number: %d, action: %s"
                    % (wedgeID, xsDataSubWedge.action.value)
                )
                #            logger.debug(xsDataSubWedge.marshal())
                if xsDataSubWedge.action.value != "burn":

                    strPrefixRunNumber = (
                        expTypePrefix
                        + prefix
                        + "w"
                        + str(int(wedgeID))
                        + "_"
                        + str(runNumberDataCollection)
                    )
                    goniostat = xsDataSubWedge.experimentalCondition.goniostat
                    no_images = int(
                        (
                            goniostat.rotationAxisEnd.value
                            - goniostat.rotationAxisStart.value
                        )
                        / goniostat.oscillationWidth.value
                    )
                    image_name = os.path.join(
                        directory,
                        "%s_%04d.%s" % (strPrefixRunNumber, no_images, suffix),
                    )

                    if beamline == "bm14":
                        time_out = 1500  # s
                    else:
                        time_out = 500  # s

                    logger.debug("BURN STRATEGY ANALYSIS:")
                    logger.debug("Looking for image %s" % image_name)
                    while not os.path.exists(image_name) and time_out > 1:
                        logger.debug(
                            "Waiting for image %s, time out in %d s"
                            % (image_name, time_out)
                        )
                        time.sleep(1)
                        time_out = time_out - 1

                    if os.path.exists(image_name):
                        logger.debug(image_name)
                        strPathToXdsInp1 = os.path.join(
                            directory.replace("RAW_DATA", "PROCESSED_DATA"),
                            "xds_"
                            + expTypePrefix
                            + prefix
                            + "w"
                            + str(int(wedgeID))
                            + "_run"
                            + str(runNumberDataCollection)
                            + "_1",
                            "XDS.INP",
                        )
                        strPathToXdsInp2 = os.path.join(
                            directory.replace("RAW_DATA", "PROCESSED_DATA"),
                            "autoprocessing_"
                            + expTypePrefix
                            + prefix
                            + "w"
                            + str(int(wedgeID))
                            + "_run"
                            + str(runNumberDataCollection)
                            + "_1",
                            "XDS.INP",
                        )
                        logger.debug(strPathToXdsInp1)
                        logger.debug(strPathToXdsInp2)
                        while (
                            not os.path.exists(strPathToXdsInp1)
                            and not os.path.exists(strPathToXdsInp2)
                            and time_out > 1
                        ):
                            logger.debug(
                                "Waiting for XDS.INP, time out in %d s" % time_out
                            )
                            time.sleep(1)
                            time_out = time_out - 1
                        if os.path.exists(strPathToXdsInp1) or os.path.exists(
                            strPathToXdsInp2
                        ):
                            # logger.info(strPathToXdsInp
                            # logger.info(fCellLengthA, fCellAngleAlpha, iSpaceGroupITNumber
                            xsDataInputXdsBurnStrategy = XSDataInputXdsBurnStrategy()
                            if os.path.exists(strPathToXdsInp1):
                                xsDataInputXdsBurnStrategy.input_file = XSDataString(
                                    strPathToXdsInp1
                                )
                            else:
                                xsDataInputXdsBurnStrategy.input_file = XSDataString(
                                    strPathToXdsInp2
                                )
                            xsDataInputXdsBurnStrategy.space_group = XSDataInteger(
                                iSpaceGroupITNumber
                            )
                            xsDataInputXdsBurnStrategy.unit_cell_a = XSDataDouble(
                                fCellLengthA
                            )
                            xsDataInputXdsBurnStrategy.unit_cell_b = XSDataDouble(
                                fCellLengthB
                            )
                            xsDataInputXdsBurnStrategy.unit_cell_c = XSDataDouble(
                                fCellLengthC
                            )
                            xsDataInputXdsBurnStrategy.unit_cell_alpha = XSDataDouble(
                                fCellAngleAlpha
                            )
                            xsDataInputXdsBurnStrategy.unit_cell_beta = XSDataDouble(
                                fCellAngleBeta
                            )
                            xsDataInputXdsBurnStrategy.unit_cell_gamma = XSDataDouble(
                                fCellAngleGamma
                            )
                            # logger.info(xsDataInputXdsBurnStrategy.marshal()
                            xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
                                "EDPluginControlXdsBurnStrategyv1_0",
                                xsDataInputXdsBurnStrategy,
                                workflow_working_dir,
                            )
                            if (
                                xsDataResult is not None
                                and xsDataResult.status.isSuccess.value
                            ):
                                xsDataInputRdfit.addXdsHklFile(
                                    XSDataFile(xsDataResult.xds_hkl)
                                )
                            else:
                                logger.warning(
                                    "No results from processing data collection starting with %s!"
                                    % image_name
                                )
                            # logger.info(xsDataInputRdfit.marshal()
                            logger.debug("BURN STRATEGY FINISHED!")
            #            break
    # logger.info(xsDataInputRdfit.marshal()
    result_rdfit_beta = -1.0
    result_rdfit_gamma = -1.0
    result_rdfit_dose_half_th = -1.0
    result_rdfit_dose_half = -1.0
    result_rdfit_relative_radiation_sensitivity = -1.0
    result_rdfit_htmlPage = ""
    result_rdfit_jsonPath = ""

    if len(xsDataInputRdfit.xdsHklFile) == 0:
        logger.error("No results from data processing!")
    else:
        xsDataResultRdfit, ednaLogPath = edna_kernel.executeEdnaPlugin(
            "EDPluginRdfitv1_0",
            xsDataInputRdfit,
            workflow_working_dir,
        )
        # logger.info(edPluginRdfit.dataOutput.marshal()

        if xsDataResultRdfit is not None:

            if xsDataResultRdfit.beta is not None:
                result_rdfit_beta = xsDataResultRdfit.beta.value

            if xsDataResultRdfit.gama is not None:
                result_rdfit_gamma = xsDataResultRdfit.gama.value

            if xsDataResultRdfit.dose_half_th is not None:
                result_rdfit_dose_half_th = xsDataResultRdfit.dose_half_th.value

            if xsDataResultRdfit.dose_half is not None:
                result_rdfit_dose_half = xsDataResultRdfit.dose_half.value

            if xsDataResultRdfit.relative_radiation_sensitivity is not None:
                result_rdfit_relative_radiation_sensitivity = (
                    xsDataResultRdfit.relative_radiation_sensitivity.value
                )

            if xsDataResultRdfit.htmlPage is not None:
                result_rdfit_htmlPage = xsDataResultRdfit.htmlPage.path.value

            if xsDataResultRdfit.jsonPath is not None:
                result_rdfit_jsonPath = xsDataResultRdfit.jsonPath.path.value

            if xsDataResultRdfit.scaleIntensityPlot is not None:
                result_rdfit_scaleIntensityPlot = (
                    xsDataResultRdfit.scaleIntensityPlot.path.value
                )

        logger.info("Results from RDFIT:")
        logger.info("Beta: %.2f" % result_rdfit_beta)
        logger.info("Gamma: %.2f" % result_rdfit_gamma)
        logger.info("Dose_half_th: %.2f" % result_rdfit_dose_half_th)
        logger.info("Dose_half: %.2f" % result_rdfit_dose_half)
        logger.info(
            "Relative_radiation_sensitivity: %.2f"
            % result_rdfit_relative_radiation_sensitivity
        )
        logger.debug("Path to html page: %r" % result_rdfit_htmlPage)
        logger.debug("Path to json file: %r" % result_rdfit_jsonPath)
        logger.debug(
            "Path to result_rdfit_scaleIntensityPlot file: %r" % result_rdfit_jsonPath
        )
    dictResultRdfit = {}
    dictResultRdfit["result_rdfit_beta"] = result_rdfit_beta
    dictResultRdfit["result_rdfit_gamma"] = result_rdfit_gamma
    dictResultRdfit["result_rdfit_dose_half_th"] = result_rdfit_dose_half_th
    dictResultRdfit["result_rdfit_dose_half"] = result_rdfit_dose_half
    dictResultRdfit["result_rdfit_relative_radiation_sensitivity"] = (
        result_rdfit_relative_radiation_sensitivity
    )
    dictResultRdfit["result_rdfit_htmlPage"] = result_rdfit_htmlPage
    dictResultRdfit["result_rdfit_jsonPath"] = result_rdfit_jsonPath
    dictResultRdfit["result_rdfit_scaleIntensityPlot"] = result_rdfit_scaleIntensityPlot
    return dictResultRdfit


def mxv1ControlInterface(
    beamline,
    listImages,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    flux,
    beamSizeX,
    beamSizeY,
    strategyOption,
    minExposureTime,
    workflow_working_dir,
    transmission=None,
    crystalSizeX=None,
    crystalSizeY=None,
    crystalSizeZ=None,
    bLogExecutiveSummary=False,
    anomalousData=False,
    detectorDistanceMin=None,
    detectorDistanceMax=None,
    aimedCompleteness=None,
    aimedMultiplicity=None,
    sampleSusceptibility=None,
    minTransmission=None,
    omegaMin=None,
    apertureSize=None,
    complexity=None,
    forcedSpaceGroup=None,
    axisStart=None,
    axisRange=None,
    rFriedel=None,
    doseLimit=None,
    aimedIOverSigmaAtHighestResolution=1.0,
    minOscWidth=None,
    numberOfPositions=None,
    strategyType=None,
    edPluginControlInterfaceName="EDPluginControlInterfacev1_2",
):
    logger = workflow_logging.getLogger()

    xsDataDiffractionPlan = XSDataDiffractionPlan()
    if strategyType is not None:
        xsDataDiffractionPlan.strategyType = XSDataString(strategyType)
    if minExposureTime is not None:
        xsDataDiffractionPlan.minExposureTimePerImage = XSDataTime(minExposureTime)
    xsDataDiffractionPlan.goniostatMaxOscillationSpeed = XSDataAngularSpeed(
        config.get_value(beamline, "Goniometer", "maxOscillationSpeed")
    )
    xsDataDiffractionPlan.goniostatMinOscillationWidth = XSDataAngle(
        config.get_value(beamline, "Goniometer", "minOscillationWidth")
    )
    if strategyOption is not None:
        xsDataDiffractionPlan.strategyOption = XSDataString(strategyOption)
    if anomalousData:
        xsDataDiffractionPlan.anomalousData = XSDataBoolean(True)
    if detectorDistanceMin is not None:
        xsDataDiffractionPlan.detectorDistanceMin = XSDataLength(detectorDistanceMin)
    if detectorDistanceMax is not None:
        xsDataDiffractionPlan.detectorDistanceMax = XSDataLength(detectorDistanceMax)
    if aimedCompleteness is not None:
        xsDataDiffractionPlan.aimedCompleteness = XSDataDouble(aimedCompleteness)
    if aimedMultiplicity is not None:
        xsDataDiffractionPlan.aimedMultiplicity = XSDataDouble(aimedMultiplicity)
    if minTransmission is not None:
        xsDataDiffractionPlan.minTransmission = XSDataDouble(minTransmission)
    if minOscWidth is not None:
        xsDataDiffractionPlan.goniostatMinOscillationWidth = XSDataAngle(minOscWidth)
    if numberOfPositions is not None:
        xsDataDiffractionPlan.numberOfPositions = XSDataInteger(numberOfPositions)
    if rFriedel is not None:
        xsDataDiffractionPlan.rFriedel = XSDataDouble(rFriedel)
    if doseLimit is not None:
        xsDataDiffractionPlan.doseLimit = XSDataDouble(doseLimit)
    if aimedIOverSigmaAtHighestResolution is not None:
        xsDataDiffractionPlan.aimedIOverSigmaAtHighestResolution = XSDataDouble(
            aimedIOverSigmaAtHighestResolution
        )
    if axisStart is not None and axisRange is not None:
        xsDataDiffractionPlan.userDefinedRotationStart = XSDataAngle(axisStart)
        xsDataDiffractionPlan.userDefinedRotationRange = XSDataAngle(axisRange)
    if forcedSpaceGroup is not None:
        forcedSpaceGroup = forcedSpaceGroup.upper()
        if forcedSpaceGroup in symmetry.getListOfChiralSpaceGroups():
            xsDataDiffractionPlan.forcedSpaceGroup = XSDataString(forcedSpaceGroup)
        else:
            logger.warning("Forced space group {0} not recognised!")

    if complexity is not None:
        xsDataDiffractionPlan.complexity = XSDataString(complexity)

    if edPluginControlInterfaceName == "EDPluginControlInterfacev1_2":
        xsDataInputInterface = XSDataInterfacev1_2.XSDataInputInterface()
    elif edPluginControlInterfaceName == "EDPluginControlInterfacev1_3":
        xsDataInputInterface = XSDataInterfacev1_3.XSDataInputInterface()
    else:
        raise BaseException(
            "Unknown EDNA plugin control intreface name: {0}".format(
                edPluginControlInterfaceName
            )
        )

    if flux is not None:
        xsDataInputInterface.flux = XSDataFloat(flux)
    if transmission is not None:
        xsDataInputInterface.transmission = XSDataDouble(transmission)
    if beamSizeX is not None and beamSizeY is not None:
        xsDataInputInterface.beamSizeX = XSDataLength(beamSizeX)
        xsDataInputInterface.beamSizeY = XSDataLength(beamSizeY)

    if apertureSize is not None:
        xsDataInputInterface.apertureSize = XSDataLength(apertureSize)

    if (
        crystalSizeX is not None
        and crystalSizeY is not None
        and crystalSizeZ is not None
    ):
        xsDataSampleCrystalMM = XSDataSampleCrystalMM()
        xsDataSize = XSDataSize()
        xsDataSize.x = XSDataLength(crystalSizeX)
        xsDataSize.y = XSDataLength(crystalSizeY)
        xsDataSize.z = XSDataLength(crystalSizeZ)
        xsDataSampleCrystalMM.size = xsDataSize
        if omegaMin is not None:
            xsDataSampleCrystalMM.omegaMin = XSDataAngle(omegaMin)
        xsDataInputInterface.sample = xsDataSampleCrystalMM

    if sampleSusceptibility is not None:
        if xsDataInputInterface.sample is None:
            xsDataInputInterface.sample = XSDataSampleCrystalMM()
        xsDataInputInterface.sample.susceptibility = XSDataDouble(sampleSusceptibility)

    xsDataInputInterface.diffractionPlan = xsDataDiffractionPlan

    for image in listImages:
        xsDataInputInterface.addImagePath(XSDataImage(XSDataString(image)))

    logger.warning("Running EDNA characterisation...")
    xsDataResultInterface, ednaLogPath = edna_kernel.executeEdnaPlugin(
        edPluginControlInterfaceName,
        xsDataInputInterface,
        workflow_working_dir,
        bLogExecutiveSummary=bLogExecutiveSummary,
    )
    if not bLogExecutiveSummary and xsDataResultInterface is not None:
        if (
            xsDataResultInterface.resultCharacterisation is not None
            and xsDataResultInterface.resultCharacterisation.shortSummary is not None
        ):
            logger.info(
                "Characterisation short summary: \n%s"
                % xsDataResultInterface.resultCharacterisation.shortSummary.value
            )
    return xsDataResultInterface, ednaLogPath


def mxv2ControlInterface(
    beamline,
    mxv1StrategyResult,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    omega,
    kappa,
    phi,
    flux,
    beamSizeX,
    beamSizeY,
    kappaStrategyOption,
    workflow_working_dir,
    mxv1ResultCharacterisation,
    mxv1ResultCharacterisation_Reference,
    mxv2DataCollection,
    mxv2DataCollection_Reference,
    possibleOrientations,
    transmission=None,
    forcedSpaceGroup=None,
    anomalousData=None,
):

    logger = workflow_logging.getLogger()

    xsDataDiffractionPlan = XSDataDiffractionPlan()
    xsDataDiffractionPlan.minExposureTimePerImage = XSDataTime(
        config.get_value(beamline, "Beam", "minCharacterisationExposureTime")
    )
    xsDataDiffractionPlan.goniostatMaxOscillationSpeed = XSDataAngularSpeed(
        config.get_value(beamline, "Goniometer", "maxOscillationSpeed")
    )
    xsDataDiffractionPlan.goniostatMinOscillationWidth = XSDataAngle(
        config.get_value(beamline, "Goniometer", "minOscillationWidth")
    )
    if kappaStrategyOption:
        xsDataDiffractionPlan.addKappaStrategyOption(XSDataString(kappaStrategyOption))
        if kappaStrategyOption.lower() == "anomalous":
            anomalousData = True
    if anomalousData:
        xsDataDiffractionPlan.anomalousData = XSDataBoolean(True)

    if forcedSpaceGroup is not None:
        forcedSpaceGroup = forcedSpaceGroup.upper()
        if forcedSpaceGroup in symmetry.getListOfChiralSpaceGroups():
            xsDataDiffractionPlan.forcedSpaceGroup = XSDataString(forcedSpaceGroup)
        else:
            logger.warning(
                "Forced space group {0} not recognised!".format(forcedSpaceGroup)
            )

    xsDataInputInterfacev2_2 = XSDataInputInterfacev2_2()
    xsDataInputInterfacev2_2.flux = XSDataFloat(flux)
    xsDataInputInterfacev2_2.beamSizeX = XSDataLength(beamSizeX)
    xsDataInputInterfacev2_2.beamSizeY = XSDataLength(beamSizeY)
    xsDataInputInterfacev2_2.diffractionPlan = xsDataDiffractionPlan
    xsDataInputInterfacev2_2.omega = XSDataAngle(omega)
    xsDataInputInterfacev2_2.kappa = XSDataAngle(kappa)
    xsDataInputInterfacev2_2.phi = XSDataAngle(phi)
    if transmission is not None:
        xsDataInputInterfacev2_2.transmission = XSDataDouble(transmission)

    listImages = getListImagePath(
        mxv1StrategyResult, directory, run_number, expTypePrefix, prefix, suffix
    )
    for image in listImages:
        xsDataInputInterfacev2_2.addImagePath(XSDataFile(XSDataString(image)))

    if mxv1ResultCharacterisation is not None:
        xsDataInputInterfacev2_2.mxv1ResultCharacterisation = (
            XSDataResultCharacterisation.parseString(mxv1ResultCharacterisation)
        )
    if mxv1ResultCharacterisation_Reference is not None:
        xsDataInputInterfacev2_2.mxv1ResultCharacterisation_Reference = (
            XSDataResultCharacterisation.parseString(
                mxv1ResultCharacterisation_Reference
            )
        )
    if mxv2DataCollection is not None:
        xsDataInputInterfacev2_2.mxv2DataCollection = XSDataCollectionv2.parseString(
            mxv2DataCollection
        )
    if mxv2DataCollection_Reference is not None:
        xsDataInputInterfacev2_2.mxv2DataCollection_Reference = (
            XSDataCollectionv2.parseString(mxv2DataCollection_Reference)
        )
    if possibleOrientations is not None:
        xsDataInputInterfacev2_2.possibleOrientations = (
            kappa_alignment_response.parseString(possibleOrientations)
        )

    xsDataResultInterfacev2_2, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginControlInterfacev2_2",
        xsDataInputInterfacev2_2,
        workflow_working_dir,
        bLogExecutiveSummary=False,
    )
    #    if xsDataResultInterfacev2_2 is not None:
    #        if xsDataResultInterfacev2_2.mxv1ResultCharacterisation is not None:
    #            logger.info("Characterisation summary: \n%s" % xsDataResultInterfacev2_2.mxv1ResultCharacterisation.shortSummary.value)
    return xsDataResultInterfacev2_2, ednaLogPath


def createSimpleHtmlPage(
    beamline,
    mxv1ResultCharacterisation,
    workflow_working_dir,
    pyarch_html_dir,
    resolution=None,
    ednaCharacterisationLogPath=None,
):
    html_page_path = None
    json_path = None
    logger = workflow_logging.getLogger()
    xsDataResultCharacterisation = XSDataResultCharacterisation.parseString(
        mxv1ResultCharacterisation
    )
    xsDataInputSimpleHTMLPage = XSDataInputSimpleHTMLPage()
    xsDataInputSimpleHTMLPage.characterisationResult = xsDataResultCharacterisation
    if ednaCharacterisationLogPath is not None:
        xsDataInputSimpleHTMLPage.logFile = XSDataFile(
            XSDataString(ednaCharacterisationLogPath)
        )
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginExecSimpleHTMLPagev1_1",
        xsDataInputSimpleHTMLPage,
        workflow_working_dir,
    )
    if xsDataResult is not None:
        html_page_path = xsDataResult.pathToHTMLFile.path.value
        json_path = xsDataResult.pathToJsonFile.path.value
        logger.debug("Html page created: %s" % html_page_path)
        if pyarch_html_dir is not None:
            dict_html = {}
            dict_html["html_directory_path"] = os.path.dirname(html_page_path)
            dict_html["index_file_name"] = os.path.basename(html_page_path)
            dict_html_pyarch = path.copyHtmlDirectoryToPyarch(
                pyarch_html_dir, dict_html, directory_name="Characterisation"
            )
            html_page_path = os.path.join(
                dict_html_pyarch["html_directory_path"],
                dict_html_pyarch["index_file_name"],
            )
            json_path = os.path.join(
                dict_html_pyarch["html_directory_path"], os.path.basename(json_path)
            )
    return html_page_path, json_path


def createSimpleHtmlPagev2_1(
    beamline, mxv2ResultCharacterisation, workflow_working_dir, pyarch_html_dir
):
    strHtmlPagePath = None
    strJsonPath = None
    logger = workflow_logging.getLogger()
    characterisationResultv2_0 = XSDataResultCharacterisationv2_0.parseString(
        mxv2ResultCharacterisation
    )
    xsDataInputSimpleHTMLPagev2_1 = XSDataInputSimpleHTMLPagev2_1()
    xsDataInputSimpleHTMLPagev2_1.characterisationResultv2_0 = (
        characterisationResultv2_0
    )
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginExecSimpleHTMLPagev2_1",
        xsDataInputSimpleHTMLPagev2_1,
        workflow_working_dir,
    )
    if xsDataResult is not None and hasattr(xsDataResult, "pathToHTMLFile"):
        strHtmlPagePath = xsDataResult.pathToHTMLFile.path.value
        strJsonPath = xsDataResult.pathToJsonFile.path.value
        logger.debug("Html page created: %s" % strHtmlPagePath)
        if pyarch_html_dir is not None:
            dict_html = {}
            dict_html["html_directory_path"] = os.path.dirname(strHtmlPagePath)
            dict_html["index_file_name"] = os.path.basename(strHtmlPagePath)
            dict_html_pyarch = path.copyHtmlDirectoryToPyarch(
                pyarch_html_dir, dict_html, directory_name="Characterisation"
            )
            strHtmlPagePath = os.path.join(
                dict_html_pyarch["html_directory_path"],
                dict_html_pyarch["index_file_name"],
            )
            strJsonPath = os.path.join(
                dict_html_pyarch["html_directory_path"], os.path.basename(strJsonPath)
            )
    return strHtmlPagePath, strJsonPath


def storeImageQualityIndicatorsFromMeshPositionsStart(
    beamline,
    directory,
    meshPositions,
    workflow_working_dir,
    grid_info=None,
    doIndexing=True,
    gap_x=None,
    doDistlSignalStrength=True,
    doUploadToIspyb=True,
    useFastMesh=True,
):
    logger = workflow_logging.getLogger()
    logger.info("Starting processing of grid images")
    edna_kernel.initSimulation(beamline)
    if isinstance(meshPositions, str):
        meshPositions = json.loads(meshPositions)
    xsDataInputControlImageQualityIndicators = (
        XSDataInputControlImageQualityIndicators()
    )
    for position in meshPositions:
        imagePath = os.path.join(directory, position["imageName"])
        if beamline in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
            # Replace ".cbf" with ".h5" for ISPyB
            imagePath = imagePath.replace(".cbf", ".h5")
        xsDataInputControlImageQualityIndicators.addImage(
            XSDataImage(XSDataString(imagePath))
        )
    xsDataInputControlImageQualityIndicators.doIndexing = XSDataBoolean(doIndexing)
    xsDataInputControlImageQualityIndicators.doDistlSignalStrength = XSDataBoolean(
        doDistlSignalStrength
    )
    xsDataInputControlImageQualityIndicators.doUploadToIspyb = XSDataBoolean(
        doUploadToIspyb
    )
    xsDataInputControlImageQualityIndicators.fastMesh = XSDataBoolean(useFastMesh)
    if grid_info is not None:
        if beamline in ["id30a3"] and gap_x is not None and gap_x > 0.001:
            logger.warning(
                "Positive horizontal grid gap detected ({0} um) on id30a3, processing batch size set to 1".format(
                    int(gap_x * 1000)
                )
            )
            batchSize = 1
        else:
            if grid_info["steps_x"] == 1:
                batchSize = grid_info["steps_y"]
            else:
                batchSize = grid_info["steps_x"]
        xsDataInputControlImageQualityIndicators.batchSize = XSDataInteger(batchSize)

    edPlugin, ednaLogPath = edna_kernel.executeEdnaPluginAsynchronous(
        "EDPluginControlImageQualityIndicatorsv1_4",
        xsDataInputControlImageQualityIndicators,
        workflow_working_dir,
        timeout=3600.0,
    )
    return edPlugin


def storeImageQualityIndicatorsFromMeshPositionsSynchronize(edPlugin, meshPositions):
    xsDataResult = edna_kernel.synchronizeEdnaPlugin(edPlugin)
    return convertXSDataImageQualityIndicatorsToDict(meshPositions, xsDataResult)


def convertXSDataImageQualityIndicatorsToDict(meshPositions, xsDataResult):
    logger = workflow_logging.getLogger()
    resultMeshPositions = []
    inputDozor = None
    if xsDataResult is not None:
        for imageQualityIndicator in xsDataResult.imageQualityIndicators:
            if imageQualityIndicator.image is not None:
                imageNameResult = os.path.basename(
                    imageQualityIndicator.image.path.value
                )
                for position in meshPositions:
                    position_copy = dict(position)
                    if position_copy["imageName"] == imageNameResult:
                        if imageQualityIndicator.goodBraggCandidates:
                            position_copy["goodBraggCandidates"] = (
                                imageQualityIndicator.goodBraggCandidates.value
                            )
                        if imageQualityIndicator.iceRings:
                            position_copy["iceRings"] = (
                                imageQualityIndicator.iceRings.value
                            )
                        if imageQualityIndicator.dozor_score:
                            position_copy["dozor_score"] = (
                                imageQualityIndicator.dozor_score.value
                            )
                        if imageQualityIndicator.dozorSpotFile:
                            position_copy["dozorSpotFile"] = (
                                imageQualityIndicator.dozorSpotFile.path.value
                            )
                        if imageQualityIndicator.dozorSpotList:
                            position_copy["dozorSpotList"] = (
                                imageQualityIndicator.dozorSpotList.value
                            )
                        if imageQualityIndicator.dozorVisibleResolution:
                            position_copy["dozorVisibleResolution"] = (
                                imageQualityIndicator.dozorVisibleResolution.value
                            )
                        position_copy["dozorSpotListShape"] = []
                        for shape in imageQualityIndicator.dozorSpotListShape:
                            position_copy["dozorSpotListShape"].append(shape.value)
                        if imageQualityIndicator.dozorSpotsIntAver:
                            position_copy["dozorSpotsIntAver"] = (
                                imageQualityIndicator.dozorSpotsIntAver.value
                            )
                        if imageQualityIndicator.inResTotal:
                            position_copy["inResTotal"] = (
                                imageQualityIndicator.inResTotal.value
                            )
                        if imageQualityIndicator.inResolutionOvrlSpots:
                            position_copy["inResolutionOvrlSpots"] = (
                                imageQualityIndicator.inResolutionOvrlSpots.value
                            )
                        if imageQualityIndicator.maxUnitCell:
                            position_copy["maxUnitCell"] = (
                                imageQualityIndicator.maxUnitCell.value
                            )
                        if imageQualityIndicator.method1Res:
                            position_copy["method1Res"] = (
                                imageQualityIndicator.method1Res.value
                            )
                        if imageQualityIndicator.method2Res:
                            position_copy["method2Res"] = (
                                imageQualityIndicator.method2Res.value
                            )
                        if imageQualityIndicator.pctSaturationTop50Peaks:
                            position_copy["pctSaturationTop50Peaks"] = (
                                imageQualityIndicator.pctSaturationTop50Peaks.value
                            )
                        if imageQualityIndicator.spotTotal:
                            position_copy["spotTotal"] = (
                                imageQualityIndicator.spotTotal.value
                            )
                        if imageQualityIndicator.totalIntegratedSignal:
                            position_copy["totalIntegratedSignal"] = (
                                imageQualityIndicator.totalIntegratedSignal.value
                            )
                        if imageQualityIndicator.selectedIndexingSolution:
                            if imageQualityIndicator.selectedIndexingSolution.crystal:
                                if (
                                    imageQualityIndicator.selectedIndexingSolution.crystal.mosaicity
                                ):
                                    position_copy["mosaicity"] = (
                                        imageQualityIndicator.selectedIndexingSolution.crystal.mosaicity.value
                                    )
                                if (
                                    imageQualityIndicator.selectedIndexingSolution.crystal.spaceGroup
                                ):
                                    position_copy["spaceGroup"] = (
                                        imageQualityIndicator.selectedIndexingSolution.crystal.spaceGroup.name.value
                                    )
                        resultMeshPositions.append(position_copy)
                        #                        pprint.pprint(position)
        for position in resultMeshPositions:
            if "dozor_score" in position:
                if str(position["dozor_score"]) == "nan":
                    position["dozor_score"] = 0.0
        if xsDataResult.inputDozor is not None:
            inputDozor = {
                "detectorType": xsDataResult.inputDozor.detectorType.value,
                "exposureTime": xsDataResult.inputDozor.exposureTime.value,
                "spotSize": xsDataResult.inputDozor.spotSize.value,
                "detectorDistance": xsDataResult.inputDozor.detectorDistance.value,
                "wavelength": xsDataResult.inputDozor.wavelength.value,
                "orgx": xsDataResult.inputDozor.orgx.value,
                "orgy": xsDataResult.inputDozor.orgy.value,
                "oscillationRange": xsDataResult.inputDozor.oscillationRange.value,
            }
    logger.info("Processing of grid images finished")
    return (resultMeshPositions, inputDozor)


def extractKappaAnglesFromComment(beamline, mxv2ResultCharacterisation):
    logger = workflow_logging.getLogger()
    dictKappaAngles = {}
    if mxv2ResultCharacterisation is not None:
        xsDataResultCharacterisationv2_0 = XSDataResultCharacterisationv2_0.parseString(
            mxv2ResultCharacterisation
        )
        if xsDataResultCharacterisationv2_0.suggestedStrategy is not None:
            if xsDataResultCharacterisationv2_0.suggestedStrategy.collectionPlan != []:
                suggestedStrategyComment = (
                    xsDataResultCharacterisationv2_0.suggestedStrategy.collectionPlan[
                        0
                    ].comment.value
                )
                listValues = suggestedStrategyComment.split(" ")
                #    phi_reference = phi
                #    kappa_reference = kappa
                #    kappa_phi_reference = kappa_phi
                dictKappaAngles["omega"] = float(listValues[0].split("=")[1])
                dictKappaAngles["kappa"] = float(listValues[1].split("=")[1])
                dictKappaAngles["phi"] = float(listValues[2].split("=")[1])
                dictKappaAngles["strategy"] = str(
                    listValues[3].split("=")[1].split()[0]
                )
                if dictKappaAngles["strategy"] == "MERGED":
                    logger.info("Suggested new kappa angles:")
                    logger.info("Omega (motor 'phi'): %.3f" % dictKappaAngles["omega"])
                    logger.info(
                        "Kappa (motor 'kappa'): %.3f" % dictKappaAngles["kappa"]
                    )
                    logger.info(
                        "Phi (motor 'kappa_phi'): %.3f" % dictKappaAngles["phi"]
                    )
    return dictKappaAngles


def extractSpaceGroupFromMXv2Results(_mxv2ResultCharacterisation):
    xsDataResultCharacterisationv2_0 = XSDataResultCharacterisationv2_0.parseString(
        _mxv2ResultCharacterisation
    )
    strSpaceGroupName = None
    mxv1ResultCharacterisation = (
        xsDataResultCharacterisationv2_0.mxv1ResultCharacterisation
    )
    if mxv1ResultCharacterisation is not None:
        indexingResult = mxv1ResultCharacterisation.indexingResult
        if indexingResult is not None:
            selectedSolution = indexingResult.selectedSolution
            crystal = selectedSolution.crystal
            spaceGroup = crystal.spaceGroup
            strSpaceGroupName = spaceGroup.name.value
    return strSpaceGroupName


def checkRankingResolution(beamline, mxv1ResultCharacterisation):
    logger = workflow_logging.getLogger()
    newResolution = None
    xsDataResultCharacterisation = XSDataResultCharacterisation.parseString(
        mxv1ResultCharacterisation
    )
    xsDataResultStrategy = xsDataResultCharacterisation.getStrategyResult()
    higherResolutionDetected = False
    rankingResolution = None
    resolutionMax = None
    if xsDataResultStrategy is not None:
        listXSDataCollectionPlan = xsDataResultStrategy.collectionPlan
        # Check if ranking resolution is higher than the suggested strategy resolution(s)
        for xsDataCollectionPlan in listXSDataCollectionPlan:
            xsDataSummaryStrategy = xsDataCollectionPlan.strategySummary
            # Retrieve the resolution...
            resolution = xsDataSummaryStrategy.resolution.value
            if resolutionMax is None:
                resolutionMax = resolution
            elif resolution < resolutionMax:
                resolutionMax = resolution
            if xsDataSummaryStrategy.rankingResolution:
                # Retrieve the detector distance...
                rankingResolution = xsDataSummaryStrategy.rankingResolution.value

    if rankingResolution is not None and rankingResolution < resolutionMax:
        higherResolutionDetected = True
        logger.info("*" * 80)
        logger.info(
            "Best has detected that the sample can diffract to %.2f A!"
            % rankingResolution
        )
        logger.info("*" * 80)
        newResolution = rankingResolution
    else:
        newResolution = resolutionMax

    return (higherResolutionDetected, newResolution)


def getResolutionAndPhistart(beamline, mxv1ResultCharacterisation):
    logger = workflow_logging.getLogger()
    resolution = None
    phistart = None
    xsDataResultCharacterisation = XSDataResultCharacterisation.parseString(
        mxv1ResultCharacterisation
    )
    xsDataResultStrategy = xsDataResultCharacterisation.getStrategyResult()
    if xsDataResultStrategy is not None:
        xsDataCollectionPlan = xsDataResultStrategy.collectionPlan[0]
        xsDataSummaryStrategy = xsDataCollectionPlan.strategySummary
        resolution = xsDataSummaryStrategy.resolution.value
        xsDataSubwedge = xsDataCollectionPlan.collectionStrategy.subWedge[0]
        phistart = (
            xsDataSubwedge.experimentalCondition.goniostat.rotationAxisStart.value
        )
    logger.debug(
        "Resolution determined to {0} A, phistart to {1} degrees".format(
            resolution, phistart
        )
    )
    return (resolution, phistart)


def create_thumbnails(listImagePath, targetDir, workflow_working_dir, format=None):
    xsDataInputPyarchThumbnailGeneratorParallel = (
        XSDataInputPyarchThumbnailGeneratorParallel()
    )
    xsDataInputPyarchThumbnailGeneratorParallel.forcedOutputDirectory = XSDataFile(
        XSDataString(targetDir)
    )
    for imagePath in listImagePath:
        xsDataInputPyarchThumbnailGeneratorParallel.addDiffractionImage(
            XSDataFile(XSDataString(imagePath))
        )
    if xsDataInputPyarchThumbnailGeneratorParallel.diffractionImage != []:
        xsDataInputPyarchThumbnailGeneratorParallel.waitForFileTimeOut = XSDataTime(
            20.0
        )
        if format is not None:
            xsDataInputPyarchThumbnailGeneratorParallel.format = XSDataString(format)
        #         logger.debug(xsDataInputPyarchThumbnailGeneratorParallel.marshal())
        xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
            "EDPluginControlPyarchThumbnailGeneratorParallelv1_0",
            xsDataInputPyarchThumbnailGeneratorParallel,
            workflow_working_dir,
        )


def create_thumbnails_for_pyarch(list_image_creation, workflow_working_dir):
    xsDataInputPyarchThumbnailGeneratorParallel = (
        XSDataInputPyarchThumbnailGeneratorParallel()
    )
    for image in list_image_creation:
        strPathImage = None
        if "imagePath" in image:
            strPathImage = image["imagePath"]
        elif image["isCreated"]:
            strPathImage = os.path.join(image["fileLocation"], image["fileName"])
        if strPathImage is not None:
            xsDataInputPyarchThumbnailGeneratorParallel.addDiffractionImage(
                XSDataFile(XSDataString(strPathImage))
            )
    if xsDataInputPyarchThumbnailGeneratorParallel.diffractionImage != []:
        xsDataInputPyarchThumbnailGeneratorParallel.waitForFileTimeOut = XSDataTime(
            2000.0
        )
        #         logger.debug(xsDataInputPyarchThumbnailGeneratorParallel.marshal())
        xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
            "EDPluginControlPyarchThumbnailGeneratorParallelv1_0",
            xsDataInputPyarchThumbnailGeneratorParallel,
            workflow_working_dir,
        )


def create_thumbnails_for_pyarch_asynchronous(
    beamline, list_image_creation, thumbnailWorkingDir=None
):
    logger = workflow_logging.getLogger()
    directory = None
    list_image_names = []
    for image_creation in list_image_creation:
        if directory is None:
            directory = image_creation["fileLocation"]
        list_image_names.append(image_creation["fileName"])
    if list_image_names != []:
        environ_dict = os.environ
        if thumbnailWorkingDir is None:
            strDate = time.strftime("%Y%m%d", time.localtime(time.time()))
            strTime = time.strftime("%H%M%S", time.localtime(time.time()))
            strTmpBaseDir = os.path.join("/tmp", beamline, strDate)
            if not os.path.exists(strTmpBaseDir):
                os.makedirs(strTmpBaseDir, 0o755)
            thumbnailWorkingDir = tempfile.mkdtemp(
                prefix="{0}_thumbnail_".format(strTime), dir=strTmpBaseDir
            )
            os.chmod(thumbnailWorkingDir, 0o755)
        environ_dict["CREATE_THUMBNAIL_WORKING_DIR"] = thumbnailWorkingDir
        command = "/opt/pxsoft/bin/id29_create_thumbnail %s" % directory
        for image_name in list_image_names:
            command += " %s" % image_name
        logger.debug("Thumnail generation command: %s" % command)
        _ = subprocess.Popen(
            command,
            shell=True,
            stdout=subprocess.PIPE,
            close_fds=True,
            env=environ_dict,
        )


def isMutipleSweepDataCollection(mxv1StrategyResult):
    isMulipleSweep = False
    xsDataResultStrategy = parseMxv1StrategyResult(mxv1StrategyResult)
    if len(xsDataResultStrategy.getCollectionPlan()) > 1:
        isMulipleSweep = True
    return isMulipleSweep


#
# def create_thumbnails_for_pyarch_remote(list_image_creation, remote_host="mxhpc0301"):
#     logger = workflow_logging.getLogger()
#     xsDataInputPyarchThumbnailGeneratorParallel = XSDataInputPyarchThumbnailGeneratorParallel()
#     for image_creation in list_image_creation:
#         strPathImage = os.path.join(image_creation["fileLocation"], image_creation["fileName"])
#         xsDataInputPyarchThumbnailGeneratorParallel.addDiffractionImage(XSDataFile(XSDataString(strPathImage)))
#     xsDataInputPyarchThumbnailGeneratorParallel.waitForFileTimeOut = XSDataTime(200.0)
#     logger.debug(xsDataInputPyarchThumbnailGeneratorParallel.marshal())
#     # Get the host name
#     pipe1 = subprocess.Popen("ssh %s hostname" % remote_host, shell=True, stdout=subprocess.PIPE, close_fds=True)
#     hostname = pipe1.communicate()[0].replace('\n', '')
#     print [hostname]
#     # Create a temporary directory on hostname
#     pipe1 = subprocess.Popen("ssh %s mktemp -d" % hostname, shell=True, stdout=subprocess.PIPE, close_fds=True)
#     dir_name = pipe1.communicate()[0].replace('\n', '')
#     print [dir_name]
#
# #     xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin("EDPluginControlPyarchThumbnailGeneratorParallelv1_0",
# #                                                  xsDataInputPyarchThumbnailGeneratorParallel,
# #                                                  workflow_working_dir,
# #                                                  )


def runDozorForHalfDoseTime(beamline, listImages, wedgeNumber, workflow_working_dir):
    halfDoseTime = None
    xsDataInputControlDozor = XSDataInputControlDozor()
    for imagePath in listImages:
        if beamline in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
            # Replace ".cbf" with ".h5" for ISPyB
            imagePath = imagePath.replace(".cbf", ".h5")
        xsDataInputControlDozor.addImage(XSDataFile(XSDataString(imagePath)))
    xsDataInputControlDozor.batchSize = XSDataInteger(wedgeNumber)
    xsDataInputControlDozor.wedgeNumber = XSDataInteger(wedgeNumber)
    xsDataInputControlDozor.radiationDamage = XSDataBoolean(True)
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginControlDozorv1_0", xsDataInputControlDozor, workflow_working_dir
    )
    if xsDataResult is not None and xsDataResult.halfDoseTime is not None:
        halfDoseTime = xsDataResult.halfDoseTime.value
    return halfDoseTime, xsDataResult


def createRdExpressWebpage(xsDataResultControlDozor, halfDoseTime, pyarchHtmlDir):
    firstPlotPath = None
    if isinstance(xsDataResultControlDozor, str):
        xsDataResultControlDozor = XSDataResultControlDozor.parseString(
            xsDataResultControlDozor
        )
    workflowStepReport = WorkflowStepReport("RDExpress", "Result of RD Express")
    workflowStepReport.addInfo("Half dose time = {0} s".format(halfDoseTime))
    pathToImage = xsDataResultControlDozor.dozorPlot.path.value
    imageTitle = os.path.splitext(os.path.basename(pathToImage))[0].replace("_", " ")
    workflowStepReport.addImage(pathToImage, imageTitle=imageTitle)
    for pngPlot in xsDataResultControlDozor.pngPlots:
        pathToImage = pngPlot.path.value
        if firstPlotPath is None:
            firstPlotPath = pathToImage
        imageTitle = os.path.splitext(os.path.basename(pathToImage))[0].replace(
            "_", " "
        )
        workflowStepReport.addImage(pathToImage, imageTitle=imageTitle)
    autoMeshPyarchHtmlDir = os.path.join(pyarchHtmlDir, "RDExpress")
    os.makedirs(autoMeshPyarchHtmlDir, 0o755)
    htmlFilePath = workflowStepReport.renderHtml(autoMeshPyarchHtmlDir)
    jsonFilePath = workflowStepReport.renderJson(autoMeshPyarchHtmlDir)
    snapShotPath = os.path.join(autoMeshPyarchHtmlDir, os.path.basename(firstPlotPath))
    shutil.copy(firstPlotPath, snapShotPath)
    return snapShotPath, htmlFilePath, jsonFilePath


def h5ToCbf(beamline, listImages, workflow_working_dir, forcedOutputDirectory=None):
    list_plugin = []
    for imagePath in listImages:
        xsDataInputControlH5ToCBF = XSDataInputControlH5ToCBF()
        xsDataInputControlH5ToCBF.hdf5File = XSDataFile(XSDataString(imagePath))
        xsDataInputControlH5ToCBF.imageNumber = XSDataInteger(
            path.getImageNumber(imagePath)
        )
        if forcedOutputDirectory is not None:
            xsDataInputControlH5ToCBF.forcedOutputDirectory = XSDataFile(
                XSDataString(forcedOutputDirectory)
            )
        edPlugin, ednaLogPath = edna_kernel.executeEdnaPluginAsynchronous(
            "EDPluginControlH5ToCBFv1_1",
            xsDataInputControlH5ToCBF,
            workflow_working_dir,
        )
        list_plugin.append(edPlugin)
    for edPlugin in list_plugin:
        edna_kernel.synchronizeEdnaPlugin(edPlugin)


def waitForFile(beamline, filePath, workflow_working_dir, fileSize=None, timeOut=None):
    timedOut = True
    xsDataInputMXWaitFile = XSDataInputMXWaitFile()
    xsDataInputMXWaitFile.file = XSDataFile(XSDataString(filePath))
    if fileSize is not None:
        xsDataInputMXWaitFile.size = XSDataInteger(fileSize)
    if timeOut is not None:
        xsDataInputMXWaitFile.timeOut = XSDataTime(timeOut)
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginMXWaitFilev1_1", xsDataInputMXWaitFile, workflow_working_dir
    )
    if xsDataResult is not None:
        if xsDataResult.timedOut is not None:
            if xsDataResult.timedOut.value:
                timedOut = True
            else:
                timedOut = False
        else:
            timedOut = False
    return timedOut


def runDimple(mtzFilePath, pdbFilePath, workflowWorkingDir=None):
    xsDataInputDimple = XSDataInputDimple()
    xsDataInputDimple.mtz = XSDataFile(XSDataString(mtzFilePath))
    xsDataInputDimple.pdb = XSDataFile(XSDataString(pdbFilePath))

    xsDataResultExecDimple, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginExecDimplev1_0", xsDataInputDimple, workflowWorkingDir
    )

    fileList = []

    if xsDataResultExecDimple is None:
        extISPyBStatus = "ERROR"

    else:
        extISPyBStatus = "FINISHED"
        # Files to be uploaded
        for blob in xsDataResultExecDimple.blob:
            fileList.append(["blob", blob.path.value])

        if xsDataResultExecDimple.finalMtz is not None:
            fileList.append(["finalMtz", xsDataResultExecDimple.finalMtz.path.value])
        if xsDataResultExecDimple.finalPdb is not None:
            fileList.append(["finalPdb", xsDataResultExecDimple.finalPdb.path.value])
        if xsDataResultExecDimple.log is not None:
            fileList.append(["dimpleLog", xsDataResultExecDimple.log.path.value])
        if xsDataResultExecDimple.findBlobsLog is not None:
            fileList.append(
                ["findBlobsLog", xsDataResultExecDimple.findBlobsLog.path.value]
            )
        if xsDataResultExecDimple.refmac5restrLog is not None:
            fileList.append(
                ["refmac5restrLog", xsDataResultExecDimple.refmac5restrLog.path.value]
            )

    return extISPyBStatus, fileList


def createEDNAprocInput(
    dataCollectionId=None,
    startImage=None,
    endImage=None,
    resolutionCutoff=None,
    reprocess=True,
    outputFile=None,
):
    xsDataEDNAprocInput = XSDataEDNAprocInput()
    if dataCollectionId is not None:
        xsDataEDNAprocInput.data_collection_id = XSDataInteger(dataCollectionId)
    if startImage is not None:
        xsDataEDNAprocInput.start_image = XSDataInteger(startImage)
    if endImage is not None:
        xsDataEDNAprocInput.end_image = XSDataInteger(endImage)
    if resolutionCutoff is not None:
        if not isinstance(resolutionCutoff, str):
            xsDataEDNAprocInput.low_resolution_limit = XSDataDouble(resolutionCutoff)
    if outputFile is not None:
        xsDataEDNAprocInput.output_file = XSDataFile(XSDataString(outputFile))
    xsDataEDNAprocInput.unit_cell = XSDataString("0,0,0,0,0,0")
    xsDataEDNAprocInput.reprocess = XSDataBoolean(reprocess)
    return xsDataEDNAprocInput


def parseXSDataResultControlImageQualityIndicators(pathToFile):
    xsDataResult = XSDataResultControlImageQualityIndicators.parseFile(pathToFile)
    return xsDataResult


def runFbest(
    flux,
    resolution,
    beamH=None,
    beamV=None,
    wavelength=None,
    aperture=None,
    slitX=None,
    slitY=None,
    rotationRange=None,
    rotationWidth=None,
    minExposureTime=None,
    doseLimit=None,
    doseRate=None,
    sensitivity=None,
    crystalSize=None,
    sampleSusceptibility=None,
    workflow_working_dir=None,
):

    dictResult = {}

    xsDataInputFbest = XSDataInputFbest()
    xsDataInputFbest.flux = XSDataDouble(flux)
    xsDataInputFbest.resolution = XSDataDouble(resolution)
    if beamH is not None:
        xsDataInputFbest.beamH = XSDataDouble(beamH)
    if beamV is not None:
        xsDataInputFbest.beamV = XSDataDouble(beamV)
    if wavelength is not None:
        xsDataInputFbest.wavelength = XSDataDouble(wavelength)
    if aperture is not None:
        xsDataInputFbest.aperture = XSDataDouble(aperture)
    if slitX is not None:
        xsDataInputFbest.slitX = XSDataDouble(slitX)
    if slitY is not None:
        xsDataInputFbest.slitY = XSDataDouble(slitY)
    if rotationRange is not None:
        xsDataInputFbest.rotationRange = XSDataDouble(rotationRange)
    if rotationWidth is not None:
        xsDataInputFbest.rotationWidth = XSDataDouble(rotationWidth)
    if minExposureTime is not None:
        xsDataInputFbest.minExposureTime = XSDataDouble(minExposureTime)
    if doseLimit is not None:
        xsDataInputFbest.doseLimit = XSDataDouble(doseLimit)
    if doseRate is not None:
        xsDataInputFbest.doseRate = XSDataDouble(doseRate)
    if sensitivity is not None:
        xsDataInputFbest.sensitivity = XSDataDouble(sensitivity)
    if crystalSize is not None:
        xsDataInputFbest.crystalSize = XSDataDouble(crystalSize)
    if sampleSusceptibility is not None:
        xsDataInputFbest.sensitivity = XSDataDouble(sampleSusceptibility)

    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginFbestv1_0", xsDataInputFbest, workflow_working_dir
    )
    dictResult["logPath"] = ednaLogPath
    with open(ednaLogPath) as f:
        dictResult["log"] = f.read()
    dictResult["success"] = False
    if xsDataResult is not None:
        if xsDataResult.exposureTimePerImage is not None:
            dictResult["exposureTimePerImage"] = xsDataResult.exposureTimePerImage.value
            dictResult["success"] = True
        if xsDataResult.transmission is not None:
            dictResult["transmission"] = xsDataResult.transmission.value
        if xsDataResult.numberOfImages is not None:
            dictResult["numberOfImages"] = xsDataResult.numberOfImages.value
        if xsDataResult.rotationWidth is not None:
            dictResult["rotationWidth"] = xsDataResult.rotationWidth.value
        if xsDataResult.resolution is not None:
            dictResult["resolution"] = xsDataResult.resolution.value
        if xsDataResult.totalDose is not None:
            dictResult["totalDose"] = xsDataResult.totalDose.value
        if xsDataResult.totalExposureTime is not None:
            dictResult["totalExposureTime"] = xsDataResult.totalExposureTime.value
        if xsDataResult.doseRate is not None:
            dictResult["doseRate"] = xsDataResult.doseRate.value
        if xsDataResult.sensitivity is not None:
            dictResult["sensitivity"] = xsDataResult.sensitivity.value
        if xsDataResult.minExposure is not None:
            dictResult["minExposure"] = xsDataResult.minExposure.value
    return dictResult


def convertToIntFloatString(stringValue):
    newValue = None
    if stringValue is not None:
        if stringValue.isdigit():
            newValue = int(stringValue)
        else:
            try:
                newValue = float(stringValue)
            except ValueError:
                newValue = stringValue
    return newValue


def removeValueFromDict(inDict):
    newDict = {}
    for key, value in inDict.items():
        if isinstance(value, dict):
            if list(value.keys())[0] == "value":
                newDict[key] = convertToIntFloatString(value["value"])
            else:
                newDict[key] = removeValueFromDict(value)
        elif isinstance(value, list):
            newDict[key] = []
            for item in value:
                if isinstance(item, dict):
                    if list(item.keys())[0] == "value":
                        if isinstance(value, list):
                            for entry in value:
                                newDict[key].append(
                                    convertToIntFloatString(entry["value"])
                                )
                        else:
                            newDict[key].append(convertToIntFloatString(value["value"]))
                    else:
                        newDict[key].append(removeValueFromDict(item))
        else:
            newDict[key] = value
    return newDict


def createEdna2CharacterisationInput(inputFile):
    characterisationInputEdna = xmltodict.parse(open(str(inputFile)).read())
    characterisationInputEdna = json.loads(
        json.dumps(characterisationInputEdna["XSDataInputMXCuBE"])
    )
    characterisationInputEdna2 = removeValueFromDict(characterisationInputEdna)
    # Convert to image path
    if isinstance(characterisationInputEdna2["dataSet"], list):
        listDataSet = characterisationInputEdna2["dataSet"]
    else:
        listDataSet = [characterisationInputEdna2["dataSet"]]
    del characterisationInputEdna2["dataSet"]
    imagePath = []
    for dictDataSet in listDataSet:
        if isinstance(dictDataSet["imageFile"], list):
            listImageFile = dictDataSet["imageFile"]
        else:
            listImageFile = [dictDataSet["imageFile"]]
        for dictImageFile in listImageFile:
            imagePath.append(dictImageFile["path"])
    characterisationInputEdna2["imagePath"] = imagePath
    return characterisationInputEdna2


def createEdna2CharacterisationOutput(outputFile):
    characterisationOutputEdna = xmltodict.parse(open(str(outputFile)).read())
    characterisationOutputEdna = json.loads(
        json.dumps(characterisationOutputEdna["XSDataResultMXCuBE"])
    )
    characterisationOutputEdna = removeValueFromDict(characterisationOutputEdna)
    characterisationOutputEdna2 = {}
    if "characterisationResult" in characterisationOutputEdna:
        characterisationResult = characterisationOutputEdna["characterisationResult"]
        if "indexingResult" in characterisationResult:
            indexingResult = characterisationResult["indexingResult"]
            if "selectedSolution" in indexingResult:
                selectedSolution = indexingResult["selectedSolution"]
                characterisationOutputEdna2["selectedSolution"] = selectedSolution
                characterisationOutputEdna2["statusMessage"] = (
                    characterisationResult.get("statusMessage", None)
                )
    return characterisationOutputEdna2
