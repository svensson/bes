#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

"""
Common module for all workflow library modules.
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013"

import os
import sys
import json
import pprint
import smtplib

try:
    from httplib import HTTPConnection
except Exception:
    from http.client import HTTPConnection

import traceback
import subprocess

from email.mime.text import MIMEText

from bes.workflow_lib import workflow_logging


def checkInputVariable(
    dict_input, str_parameter, o_default="NoDefaultParameter", b_warn_if_default=False
):
    """
    Checks if a key (str_parameter) exists in the dictionary.
    If it doesn't exist the o_default value is used.
    The method warns on the logger if the default value is used
    if b_warn_if_default is set to True.
    """
    o_value = None
    if str_parameter not in dict_input:
        if o_default == "NoDefaultParameter":
            raise Exception("No input parameter '%s'" % str_parameter)
        else:
            o_value = o_default
            if b_warn_if_default:
                logger = workflow_logging.getLogger()
                logger.warning(
                    str_parameter
                    + " parameter is not defined,"
                    + " using default value : %r" % o_default
                )
    else:
        o_value = dict_input[str_parameter]
        if o_value == "null":
            o_value = o_default
    return o_value


def sendEmail(
    beamline,
    listTo,
    subject,
    message,
    replyTo="workflow@esrf.fr",
    listCC=[],
    listBCC=[],
):
    """
    Method for sending an email with a subject and a message.
    """
    logger = workflow_logging.getLogger()

    if beamline not in ["simulator", "simulator_mxcube"]:  # , "id30a2"]:
        logger.debug(
            "Sending email from "
            + replyTo
            + " to XXX@XX.XX with subject '"
            + subject
            + "'"
        )
        logger.debug("Email message: \n" + message)

        COMMASPACE = ", "

        mime_text_message = MIMEText(message)
        mime_text_message["Subject"] = subject
        mime_text_message["From"] = replyTo
        mime_text_message["To"] = COMMASPACE.join(listTo)
        if len(listCC) > 0:
            mime_text_message["CC"] = COMMASPACE.join(listCC)
        if len(listBCC) > 0:
            mime_text_message["BCC"] = COMMASPACE.join(listBCC)

        try:
            smtp = smtplib.SMTP("localhost")
            smtp.sendmail(
                replyTo, listTo + listCC + listBCC, mime_text_message.as_string()
            )
            smtp.quit()
        except BaseException as error:
            logger.error("ERROR when trying to send email: %r" % error)


def send_email(
    beamline,
    list_receiver,
    str_subject,
    directory,
    workflow_title,
    workflow_type,
    workflow_working_dir,
    str_message="",
    str_me="workflow@esrf.fr",
):
    """
    Method for sending an email with a subject and a message.
    """
    logger = workflow_logging.getLogger()
    if len(list_receiver) > 0:
        logger.debug(
            "Sending email from "
            + str_me
            + " to XXX@XX.XX with subject '"
            + str_subject
            + "'"
        )
        text = ""
        text += "Title: %s\n" % workflow_title
        text += "Type: %s\n" % workflow_type
        text += "Image directory: %s\n" % directory
        text += "Working directory: %s\n" % workflow_working_dir
        text += "\n"
        if str_message is not None:
            text += str_message
        # logger.debug(text)

        sendEmail(
            beamline,
            list_receiver,
            "Workflow %s on %s: %s" % (workflow_type, beamline, str_subject),
            text,
            str_me,
        )


def getListPsAux():
    logger = workflow_logging.getLogger()
    #     pipe1 = subprocess.Popen("ps -aef", shell=True, stdout=subprocess.PIPE, close_fds=True)
    pipe1 = subprocess.Popen(
        "ps -aef | grep mscisoft | grep -v grep",
        shell=True,
        stdout=subprocess.PIPE,
        close_fds=True,
    )
    list_ps_aux = pipe1.communicate()[0].split("\n")
    list_ps_aux.remove("")
    logger.debug("Result of 'ps -aux': \n%s" % pprint.pformat(list_ps_aux))
    return list_ps_aux


def getDefunctPythonIds(list_ps_aux_line, user):
    """
    Exctract process id from ps -aux line if of type:
    'opid23   20482     1  0 11:06 ?        00:00:01 /usr/bin/python2.6 -u -mscisoftpy.rpc 8715'
    """
    list_id = []
    for ps_aux_line in list_ps_aux_line:
        list_line = ps_aux_line.split(" ")
        list_line[:] = (value for value in list_line if value != "")
        if len(list_line) > 9:
            if (
                list_line[0] == user
                and list_line[2] == "1"
                and "mscisoft" in list_line[9]
            ):
                list_id.append(int(list_line[1]))
    return list_id


def jsonLoads(inputObject):
    if type(inputObject) in [str, bytes]:
        returnObject = json.loads(inputObject)
    else:
        returnObject = inputObject
    return returnObject


def logStackTrace(workflowParameters):
    fileStackTrace = None
    if isinstance(workflowParameters, str):
        workflowParameters = jsonLoads(workflowParameters)
    if workflowParameters is not None:
        if "stackTraceFile" in workflowParameters:
            fileStackTrace = open(workflowParameters["stackTraceFile"], "w")
        logger = workflow_logging.getLogger()
        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        errorMessage = "{0} {1}".format(exc_type, exc_value)
        if fileStackTrace is not None:
            fileStackTrace.write(errorMessage + os.linesep)
        logger.error(errorMessage)
        listTrace = traceback.extract_tb(exc_traceback)
        logger.error("Traceback (most recent call last): %s" % os.linesep)
        for listLine in listTrace:
            errorLine = '  File "%s", line %d, in %s%s' % (
                listLine[0],
                listLine[1],
                listLine[2],
                os.linesep,
            )
            logger.error(errorLine)
            if fileStackTrace is not None:
                fileStackTrace.write(errorLine)


def interpretStacktrace(stacktrace):
    message = ""
    # Check for 'Connection refused'
    if "Connection refused" in stacktrace:
        message = "ERROR connecting to mxCuBE!\n"
        message += "\n"
        message += "Please restart mxCuBE and try again."
    elif "state is not ready" in stacktrace:
        message = "ERROR when trying to move motor!\n"
        message += "\n"
        message += stacktrace.split("\n")[0]
    else:
        message = "ERROR during workflow execution:\n"
        message += "\n"
        message += stacktrace

    return message


def getBESWorkflowStatus(bes_parameters):
    if not bes_parameters:
        raise ValueError("BES parameters are required")

    bes_host = bes_parameters["host"]
    bes_port = int(bes_parameters["port"])
    bes_request_id = bes_parameters["request_id"]
    url = f"/STATUS/{bes_request_id}"

    logger = workflow_logging.getLogger()
    logger.info("Get workflow status from %s:%s%s", bes_host, bes_port, url)

    conn = HTTPConnection(bes_host, bes_port)
    conn.request("GET", url)
    response = conn.getresponse()
    logger.info(response.status)

    status = None
    if response.status == 200:
        status = response.read().decode("utf-8")
    return status
