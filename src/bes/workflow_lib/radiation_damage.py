#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
"""
Workflow library module for connecting to ISPyB
"""

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "20/04/2022"

import os
import pprint
import subprocess

from bes.workflow_lib import workflow_logging


def run_flux2dose(flux, wavelength, aperture):
    dose_rate = None
    logger = workflow_logging.getLogger()
    command_line = "flux2dose -f {0} -w {1} -a {2}".format(flux, wavelength, aperture)
    logger.debug("flux2dose command line: '{0}'".format(command_line))
    process = subprocess.run(
        command_line.split(" "), stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    list_lines = process.stdout.decode("utf-8").split("\n")
    logger.debug("flux2dose result: '{0}'".format(list_lines))
    if len(list_lines) > 0:
        result = list_lines[0]
        if "Dose_Rate" in result:
            dose_rate = float(result.split()[2])
    return dose_rate


def run_rdfit_plan(dose_rate, visible_resolution, sensitivity):
    logger = workflow_logging.getLogger()
    command_line = "rdfit -plan {0} {1} {2}".format(
        dose_rate, visible_resolution, sensitivity
    )
    logger.debug("rdfit command line: '{0}'".format(command_line))
    process = subprocess.run(
        command_line.split(" "), stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    list_lines = process.stdout.decode("utf-8").split("\n")
    logger.debug("rdfit result: '{0}'".format(pprint.pformat(list_lines)))
    rd_plan = {}
    for line in list_lines:
        if "=" in line:
            key, value = line.split("=")
            new_key = key.lower().strip().replace(" ", "_")
            if "." in value:
                rd_plan[new_key] = float(value)
            else:
                rd_plan[new_key] = int(value)
    return rd_plan


def run_rdfit(dose_per_wedge, list_xds_ascii_hkl, working_directory):
    logger = workflow_logging.getLogger()
    rdfit_working_directory = os.path.join(working_directory, "RDFIT")
    os.makedirs(rdfit_working_directory, mode=0o755)
    logger.debug("RDFIT working dir: {0}".format(rdfit_working_directory))
    command_line = "rdfit -dD {0} -gr rd.mtv".format(dose_per_wedge)
    for xds_ascii_hkl in list_xds_ascii_hkl:
        command_line += " {0}".format(xds_ascii_hkl)
    logger.debug("rdfit command line: '{0}'".format(command_line))
    process = subprocess.run(
        command_line.split(" "),
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        cwd=rdfit_working_directory,
    )
    list_lines = process.stdout.decode("utf-8").split("\n")
    logger.debug("rdfit result: '{0}'".format(pprint.pformat(list_lines)))
    return list_lines
