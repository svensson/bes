"""
Workflow library module for grid / mesh
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2015"
__updated__ = "2015-11-26"

import os
import pathlib
import threading

import math
import json
import time
import numpy
import shutil
import pprint
import imageio
import tempfile
import operator
import circle_fit
import scipy.ndimage

import matplotlib
import matplotlib.pyplot as pyplot
import pylab
from PIL import Image

from bes.workflow_lib import path
from bes import config
from bes.workflow_lib import edna2_tasks
from bes.workflow_lib import workflow_logging

from bes.workflow_lib.report import WorkflowStepReport

matplotlib.use("Agg")

PIXELS_PER_MM_HOR = 320


def determineMeshPosition(
    beamline,
    motorPositions,
    osc_range,
    grid_info,
    iIndexY,
    iIndexZ,
    logger=None,
    isHorizontalRotationAxis=True,
    isRotationAxisScan=False,
):
    """
    Determines motor positions of the specified grid point given
    by iIndexY, iIndexZ at phi angle (in degrees),
    and returns the values in a dictionary, which contains these keywords:
    iIndexY, iIndexZ, sampx, sampy, phix, phiy, phiz, omega, kappa, phi.
    Normally iIndexY, iIndexZ are integers, but this method is also capable of
    processing float indices, using linear interpolation.
    """
    phi = motorPositions["phi"]
    sampx = motorPositions["sampx"]
    sampy = motorPositions["sampy"]
    focus = motorPositions["focus"]
    phiy = motorPositions["phiy"]
    phiz = motorPositions["phiz"]
    if "kappa" in motorPositions:
        kappa = motorPositions["kappa"]
        kappa_phi = motorPositions["kappa_phi"]
    else:
        kappa = None
        kappa_phi = None
    isHorizontalRotationAxis = True
    if beamline in ["id23eh2"]:
        isHorizontalRotationAxis = False

    if isinstance(grid_info, str):
        dict_grid_info = json.loads(grid_info)
    else:
        dict_grid_info = grid_info
    if dict_grid_info == {}:
        return None
    # dict_grid_info["x1"] = dict_grid_info["x1"] +  cell_width / 2
    # dict_grid_info["y1"] = dict_grid_info["y1"] + cell_height / 2
    # dict_grid_info["dx_mm"] = dict_grid_info["dx_mm"] - cell_width
    # dict_grid_info["dy_mm"] = dict_grid_info["dy_mm"] - cell_height
    if "cell_width" in dict_grid_info:
        cell_width = dict_grid_info["cell_width"] / 1000.0
    else:
        cell_width = dict_grid_info["dx_mm"] / int(dict_grid_info["steps_x"])
    if "cell_height" in dict_grid_info:
        cell_height = dict_grid_info["cell_height"] / 1000.0
    else:
        cell_height = dict_grid_info["dy_mm"] / int(dict_grid_info["steps_y"])
    x1_mxcube = float(dict_grid_info["x1"])
    dx_mxcube = float(dict_grid_info["dx_mm"])
    nx = int(dict_grid_info["steps_x"])
    y1_mxcube = float(dict_grid_info["y1"])
    dy_mxcube = float(dict_grid_info["dy_mm"])
    ny = int(dict_grid_info["steps_y"])
    if isHorizontalRotationAxis:
        if nx == 1:
            x1_grid = float(dict_grid_info["x1"])
            dx_grid = float(dict_grid_info["dx_mm"])
            y1_grid = float(dict_grid_info["y1"] + cell_height / 2)
            dy_grid = float(dict_grid_info["dy_mm"] - cell_height)
        else:
            x1_grid = float(dict_grid_info["x1"] + cell_width / 2)
            dx_grid = float(dict_grid_info["dx_mm"] - cell_width)
            y1_grid = float(dict_grid_info["y1"])
            dy_grid = float(dict_grid_info["dy_mm"])
    else:
        if ny == 1:
            x1_grid = float(dict_grid_info["x1"] + cell_width / 2)
            dx_grid = float(dict_grid_info["dx_mm"] - cell_width)
            y1_grid = float(dict_grid_info["y1"])
            dy_grid = float(dict_grid_info["dy_mm"])
        else:
            x1_grid = float(dict_grid_info["x1"])
            dx_grid = float(dict_grid_info["dx_mm"])
            y1_grid = float(dict_grid_info["y1"] + cell_height / 2)
            dy_grid = float(dict_grid_info["dy_mm"] - cell_height)

    phiRad = math.radians(phi)
    positions = {}
    positions["indexY"] = iIndexY
    positions["indexZ"] = iIndexZ
    # Determine amplitude of movement along x and y
    if beamline in ["id23eh1", "id30a1", "id30a3", "id30b"]:
        sign = -1
    else:
        sign = 1
    if nx > 1:
        xMovement_grid = sign * float(iIndexY) / (nx - 1) * dx_grid
        xMovement_mxcube = sign * float(iIndexY) / (nx - 1) * dx_mxcube
    else:
        xMovement_grid = 0
        xMovement_mxcube = 0
    if ny > 1:
        yMovement_grid = float(iIndexZ) / (ny - 1) * dy_grid
        yMovement_mxcube = float(iIndexZ) / (ny - 1) * dy_mxcube
    else:
        yMovement_grid = 0
        yMovement_mxcube = 0
    # Determine rotated amplitudes:
    xRotMovement_grid = x1_grid * sign + xMovement_grid
    yRotMovement_grid = y1_grid + yMovement_grid
    xRotMovement_mxcube = x1_mxcube * sign + xMovement_mxcube
    yRotMovement_mxcube = y1_mxcube + yMovement_mxcube
    # if logger is not None:
    #    logger.debug("phiRad: %f" % phiRad)
    #    logger.debug("phiy: %f" % phiy)
    # Finally apply phi rotation:
    positions["focus"] = focus
    if nx > 1:
        positions["omega"] = round(
            phi + osc_range * (float(iIndexY) / nx + 1.0 / nx / 2.0 - 1 / 2.0), 3
        )
    else:
        positions["omega"] = round(
            phi + osc_range * (float(iIndexZ) / ny + 1.0 / ny / 2.0 - 1 / 2.0), 3
        )
    # logger.debug("omega: {0}, iIndexY: {1}, iIndexZ: {2}".format(positions["omega"], iIndexY, iIndexZ))
    if kappa is not None:
        positions["kappa"] = kappa
        positions["phi"] = kappa_phi
    if isHorizontalRotationAxis:
        # Horizontal rotation axis
        if isRotationAxisScan:
            positions["sampx"] = sampx
            positions["sampy"] = sampy
            positions["phiz"] = phiz + yRotMovement_grid
            positions["sampx_mxcube"] = sampx
            positions["sampy_mxcube"] = sampy
            positions["phiz_mxcube"] = phiz + yRotMovement_mxcube
        else:
            positions["sampx"] = sampx - yRotMovement_grid * math.sin(phiRad)
            positions["sampy"] = sampy + yRotMovement_grid * math.cos(phiRad)
            positions["phiz"] = phiz
            positions["sampx_mxcube"] = sampx - yRotMovement_mxcube * math.sin(phiRad)
            positions["sampy_mxcube"] = sampy + yRotMovement_mxcube * math.cos(phiRad)
            positions["phiz_mxcube"] = phiz
        positions["phiy"] = phiy + xRotMovement_grid
        positions["phiy_mxcube"] = phiy + xRotMovement_mxcube
    elif beamline in ["id23eh2"]:
        # Vertical rotation axis
        if isRotationAxisScan:
            positions["sampx"] = sampx
            positions["sampy"] = sampy
            positions["phiy"] = phiy + xRotMovement_grid
            positions["sampx_mxcube"] = sampx
            positions["sampy_mxcube"] = sampy
            positions["phiy_mxcube"] = phiy + xRotMovement_mxcube
        else:
            positions["sampx"] = sampx - xRotMovement_grid * math.sin(phiRad)
            positions["sampy"] = sampy + xRotMovement_grid * math.cos(phiRad)
            positions["phiy"] = phiy
            positions["sampx_mxcube"] = sampx - xRotMovement_mxcube * math.sin(phiRad)
            positions["sampy_mxcube"] = sampy + xRotMovement_mxcube * math.cos(phiRad)
            positions["phiy_mxcube"] = phiy
        positions["phiz"] = phiz + yRotMovement_grid
        positions["phiz_mxcube"] = phiz + yRotMovement_mxcube
    else:
        raise RuntimeError(
            "Not implemented: vertical rotation axis on beamline %s" % beamline
        )
    return positions


def determineMeshPositions(
    beamline,
    motorPositions,
    osc_range,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    grid_info,
    meshZigZag=False,
    multi_wedge=False,
    isRotationAxisScan=False,
    phiyOffset=None,
    phiyOffsetDirection=1,
):
    meshPositions = None
    logger = workflow_logging.getLogger()
    if beamline in ["id23eh2"]:
        isHorizontalRotationAxis = False
    else:
        isHorizontalRotationAxis = True
    if isinstance(grid_info, str):
        dict_grid_info = json.loads(grid_info)
    else:
        dict_grid_info = json.loads(json.dumps(grid_info))

    if phiyOffset is None:
        phiyOffset = config.get_value(
            beamline, "Goniometer", "phiyOffset", default_value=None
        )
        logger.info(f"phiy offset set to : {phiyOffset} mm")
        phiyOffsetDirection = config.get_value(
            beamline, "Goniometer", "phiyOffsetDirection", default_value=1
        )
        logger.info(f"phiy offset direction set to {phiyOffsetDirection}")

    if dict_grid_info != {}:
        x1 = float(dict_grid_info["x1"])
        dx = float(dict_grid_info["dx_mm"])
        nx = int(dict_grid_info["steps_x"])
        y1 = float(dict_grid_info["y1"])
        dy = float(dict_grid_info["dy_mm"])
        ny = int(dict_grid_info["steps_y"])
        angle = 0.0  # Could be float(dict_grid_info["angle"]), but not used now

        logger.debug("Grid x1=%.3f" % x1)
        logger.debug("Grid dx=%.3f" % dx)
        logger.debug("Grid nx=%d" % nx)
        logger.debug("Grid y1=%.3f" % y1)
        logger.debug("Grid dy=%.3f" % dy)
        logger.debug("Grid ny=%d" % ny)
        logger.debug("Grid angle=%.1f" % angle)

        iGlobalIndex = 0
        bContinue = True
        meshPositions = []
        scan_direction_positive = True
        iIndexY = 0
        iIndexZ = 0
        while bContinue:
            # Image name
            positions = determineMeshPosition(
                beamline,
                motorPositions,
                osc_range,
                dict_grid_info,
                iIndexY,
                iIndexZ,
                logger,
                isHorizontalRotationAxis=isHorizontalRotationAxis,
                isRotationAxisScan=isRotationAxisScan,
            )
            positions["index"] = iGlobalIndex
            if multi_wedge:
                if suffix == "h5":
                    positions["imageName"] = "%s%s_w%03d_%d_%06d.h5" % (
                        expTypePrefix,
                        prefix,
                        iIndexZ + 1,
                        run_number,
                        iIndexY + 1,
                    )
                else:
                    positions["imageName"] = "%s%s_w%03d_%d_%04d.%s" % (
                        expTypePrefix,
                        prefix,
                        iIndexZ + 1,
                        run_number,
                        iIndexY + 1,
                        suffix,
                    )
            else:
                if suffix == "h5":
                    positions["imageName"] = "%s%s_%d_%06d.h5" % (
                        expTypePrefix,
                        prefix,
                        run_number,
                        iGlobalIndex + 1,
                    )
                else:
                    positions["imageName"] = "%s%s_%d_%04d.%s" % (
                        expTypePrefix,
                        prefix,
                        run_number,
                        iGlobalIndex + 1,
                        suffix,
                    )
            if isHorizontalRotationAxis:
                # Horizontal rotation axis
                if phiyOffset is not None:
                    if (scan_direction_positive and phiyOffsetDirection < 0) or (
                        not scan_direction_positive and phiyOffsetDirection > 0
                    ):
                        # logger.debug("Applying phiyOffset")
                        # logger.debug(f"phiy before offset : {positions['phiy']}")
                        positions["phiy"] += phiyOffset
                        # logger.debug(f"phiy after offset : {positions['phiy']}")
                        positions["phiy_mxcube"] += phiyOffset
                if scan_direction_positive:
                    iIndexY += 1
                    if iIndexY == nx:
                        if meshZigZag:
                            iIndexY = nx - 1
                            scan_direction_positive = False
                        else:
                            iIndexY = 0
                        iIndexZ += 1
                else:
                    iIndexY -= 1
                    if iIndexY < 0:
                        if meshZigZag:
                            iIndexY = 0
                            scan_direction_positive = True
                        else:
                            iIndexY = nx - 1
                        iIndexZ += 1
                if iIndexZ >= ny:
                    bContinue = False
            else:
                # Vertical rotation axis on ID30A1
                if scan_direction_positive:
                    iIndexZ += 1
                    if iIndexZ == ny:
                        if meshZigZag:
                            iIndexZ = ny - 1
                            scan_direction_positive = False
                        else:
                            iIndexZ = 0
                        iIndexY += 1
                else:
                    iIndexZ -= 1
                    if iIndexZ < 0:
                        iIndexZ = 0
                        iIndexY += 1
                        scan_direction_positive = True
                if iIndexY >= nx:
                    bContinue = False
            meshPositions.append(positions)
            iGlobalIndex += 1
    # logger.debug("meshPositions: %s" % pprint.pformat(meshPositions))
    return meshPositions


def _createDataArray(
    dict_grid_info,
    meshPositions,
    data_threshold=None,
    diffractionSignalDetection="Dozor",
):
    nx = dict_grid_info["steps_x"]
    ny = dict_grid_info["steps_y"]
    dataArray = numpy.zeros((nx, ny))
    # First check that we have at least one dozor_score:
    has_dozor_score = False
    for position in meshPositions:
        if "dozor_score" in position:
            has_dozor_score = True
    for position in meshPositions:
        indexY = position["indexY"]
        indexZ = position["indexZ"]
        dataValue = 0.0
        if diffractionSignalDetection.startswith("Dozor"):
            if has_dozor_score:
                # Not all data points may have dozor_score...
                if "dozor_score" in position:
                    dataValue = position["dozor_score"]
                    if dataValue < 0:
                        dataValue = 0.0
            elif "totalIntegratedSignal" in position:
                dataValue = position["totalIntegratedSignal"]
        elif (
            diffractionSignalDetection.startswith("Spotfinder")
            and "totalIntegratedSignal" in position
        ):
            dataValue = position["totalIntegratedSignal"]
        elif (
            diffractionSignalDetection.startswith("TotalIntensity")
            and "totalIntensity" in position
        ):
            dataValue = position["totalIntensity"]
        else:
            dataValue = 0.0
        if data_threshold:
            if dataValue > data_threshold:
                dataValue = data_threshold
        dataArray[indexY, indexZ] = dataValue
    data = dataArray.transpose()
    return data


def _createIceRingsDataArray(dict_grid_info, meshPositions):
    nx = dict_grid_info["steps_x"]
    ny = dict_grid_info["steps_y"]
    dataArray = numpy.zeros((nx, ny))
    for position in meshPositions:
        indexY = position["indexY"]
        indexZ = position["indexZ"]
        dataValue = position.get("iceRings", 0)
        dataArray[indexY, indexZ] = dataValue
    data = dataArray.transpose()
    return data


def remove_position_from_array(dataArray, excludedPosition, radius=2):
    excludedIndexY = excludedPosition["indexY"]
    excludedIndexZ = excludedPosition["indexZ"]
    ny, nx = dataArray.shape
    y, x = numpy.ogrid[
        -excludedIndexZ : ny - excludedIndexZ, -excludedIndexY : nx - excludedIndexY
    ]
    mask = x * x + y * y > radius * radius
    newDataArray = numpy.where(mask, dataArray, 0.0)
    return newDataArray


def find_max_signal(
    grid_info, meshPositions, upper_threshold=None, diffractionSignalDetection="Dozor"
):
    data = _createDataArray(
        grid_info,
        meshPositions,
        upper_threshold=upper_threshold,
        diffractionSignalDetection=diffractionSignalDetection,
    )
    return numpy.max(data)


def find_best_position(
    grid_info,
    meshPositions,
    dataArray=None,
    data_threshold=None,
    upper_threshold=None,
    lower_threshold=0.1,
    isHorizontalRotationAxis=True,
    diffractionSignalDetection="Dozor",
    correct_dozorm2=False,
):
    if upper_threshold is None:
        upper_threshold = 0.5
    logger = workflow_logging.getLogger()
    if dataArray is None:
        dataArray = _createDataArray(
            grid_info,
            meshPositions,
            data_threshold=data_threshold,
            diffractionSignalDetection=diffractionSignalDetection,
        )
    best_position = find_strongest_position(
        grid_info,
        meshPositions,
        dataArray=dataArray,
        data_threshold=data_threshold,
        upper_threshold=upper_threshold,
        lower_threshold=lower_threshold,
        isHorizontalRotationAxis=isHorizontalRotationAxis,
        diffractionSignalDetection=diffractionSignalDetection,
        correct_dozorm2=correct_dozorm2,
    )
    if best_position is not None:
        max_signal = numpy.max(dataArray)
        if (
            diffractionSignalDetection.startswith("Dozor")
            and "dozor_score" in best_position["nearest_image"]
        ):
            nearest_image_signal = best_position["nearest_image"]["dozor_score"]
        elif (
            diffractionSignalDetection.startswith("Spotfinder")
            and "totalIntegratedSignal" in best_position["nearest_image"]
        ):
            nearest_image_signal = best_position["nearest_image"][
                "totalIntegratedSignal"
            ]
        elif (
            diffractionSignalDetection.startswith("TotalIntensity")
            and "totalIntegratedSignal" in best_position["nearest_image"]
        ):
            nearest_image_signal = best_position["nearest_image"]["totalIntensity"]
        else:
            nearest_image_signal = None
        if nearest_image_signal is not None:
            logger.debug(
                "find_best_position, lower_threshold=%.2f, max_signal=%.1f, nearest_image_signal=%.1f"
                % (lower_threshold, max_signal, nearest_image_signal)
            )
            while nearest_image_signal < (max_signal / 2.0) and upper_threshold <= 1.0:
                upper_threshold += 0.05
                best_position = find_strongest_position(
                    grid_info,
                    meshPositions,
                    dataArray=dataArray,
                    data_threshold=data_threshold,
                    upper_threshold=upper_threshold,
                    lower_threshold=lower_threshold,
                    isHorizontalRotationAxis=isHorizontalRotationAxis,
                    diffractionSignalDetection=diffractionSignalDetection,
                    correct_dozorm2=correct_dozorm2,
                )
                if (
                    diffractionSignalDetection.startswith("Dozor")
                    and "dozor_score" in best_position["nearest_image"]
                ):
                    nearest_image_signal = best_position["nearest_image"]["dozor_score"]
                elif (
                    diffractionSignalDetection.startswith("Spotfinder")
                    and "totalIntegratedSignal" in best_position["nearest_image"]
                ):
                    nearest_image_signal = best_position["nearest_image"][
                        "totalIntegratedSignal"
                    ]
                elif (
                    diffractionSignalDetection.startswith("TotalIntensity")
                    and "totalIntegratedSignal" in best_position["nearest_image"]
                ):
                    nearest_image_signal = best_position["nearest_image"][
                        "totalIntensity"
                    ]
                else:
                    nearest_image_signal = None
                logger.debug(
                    "find_best_position, lower_threshold=%.2f, max_signal=%.1f, nearest_image_signal=%.1f"
                    % (lower_threshold, max_signal, nearest_image_signal)
                )
        # Remove any numpy objects
        best_position = json.loads(json.dumps(best_position))
    return best_position


def _find_strongest_position(
    meshPositions,
    index_x,
    index_y,
    index_fraction_x,
    index_fraction_y,
    com_index_x,
    com_index_y,
    isHorizontalRotationAxis=False,
    grid_info=None,
    correct_dozorm2=False,
):
    logger = workflow_logging.getLogger()
    logger.debug(f"_find_strongest_position correct_dozorm2: {correct_dozorm2}")
    # Find peaks
    start_position = None
    hor_position = None
    ver_position = None
    hor_and_ver_position = None
    for position in meshPositions:
        indexY = position["indexY"]
        indexZ = position["indexZ"]
        # print(indexY, indexZ, index_x, index_y)
        if index_x == indexY and index_y == indexZ:
            start_position = dict(position)
        #                     logger.trace("Start position:")
        #                     logger.trace(pprint.pformat(start_position))
        elif index_x + 1 == indexY and index_y == indexZ:
            hor_position = dict(position)
        #                     logger.trace("Horizontal position:")
        #                     logger.trace(pprint.pformat(hor_position))
        elif index_x == indexY and index_y + 1 == indexZ:
            ver_position = dict(position)
        #                     logger.trace("Vertical position:")
        #                     logger.trace(pprint.pformat(ver_position))
        elif index_x + 1 == indexY and index_y + 1 == indexZ:
            hor_and_ver_position = dict(position)
    #                     logger.trace("Horizontal and vertical position:")
    #                     logger.trace(pprint.pformat(hor_and_ver_position))
    # Find best position
    logger.debug(f"index_x, index_y: ({index_x},{index_y})")
    logger.debug(
        f"index_fraction_x, index_fraction_x: ({index_fraction_x:.2f},{index_fraction_y:.2f})"
    )
    if correct_dozorm2:
        logger.debug("Checking if correction for dozorm2 needed")
        dir_x, dir_y = getDirections(
            meshPositions=meshPositions,
            grid_info=grid_info,
            index_x=index_x,
            index_y=index_y,
        )
        if dir_x is None or dir_y is None:
            logger.debug("No correction needed for dozorm2")
        else:
            corr_x = dir_x / 4.0
            corr_y = dir_y / 4.0
            steps_x = grid_info["steps_x"]
            steps_y = grid_info["steps_y"]
            logger.info(f"Dozorm2 fraction correction: ({corr_x:.2f},{corr_y:.2f})")
            index_fraction_x += corr_x
            index_fraction_y += corr_y
            if index_fraction_x < 0:
                if index_x == 0:
                    index_fraction_x = 0
                else:
                    index_x -= 1.0
                    index_fraction_x += 1.0
            elif index_fraction_x > 1.0:
                if index_x == steps_x - 1:
                    index_fraction_x = 1
                else:
                    index_x += 1.0
                    index_fraction_x -= 1.0
            if index_fraction_y < 0:
                if index_y == 0:
                    index_fraction_y = 0
                else:
                    index_y -= 1.0
                    index_fraction_y += 1.0
            elif index_fraction_y > 1.0:
                if index_y == steps_y - 1:
                    index_fraction_y = 1
                else:
                    index_y += 1.0
                    index_fraction_y -= 1.0
            com_index_x = index_x + index_fraction_x
            com_index_y = index_y + index_fraction_y
            logger.debug(f"index after correction: ({com_index_x},{com_index_y})")
            logger.debug(
                f"index_fraction after correction: ({index_fraction_x:.2f},{index_fraction_y:.2f})"
            )
            logger.info(f"Dozorm2 fraction correction: ({corr_x:.2f},{corr_y:.2f})")
    # # Make sure index_fraction_x, index_fraction_> 0
    # if index_fraction_x < 0 or index_fraction_y < 0:
    #     if index_fraction_x < 0:
    #         index_fraction_x += 1.0
    #         index_x -= 1
    #     if index_fraction_y < 0:
    #         index_fraction_y += 1.0
    #         index_y -= 1
    #     logger.debug(f"index_x, index_y: ({index_x},{index_y})")
    #     logger.debug(f"index_fraction_x, index_fraction_x: ({index_fraction_x:.2f},{index_fraction_y:.2f})")
    if index_fraction_x < 0.05 and index_fraction_y < 0.05:
        strongest_position = start_position
        strongest_position["nearest_image"] = dict(start_position)
    elif index_fraction_x > 0.95 and index_fraction_y < 0.05:
        strongest_position = hor_position
        strongest_position["nearest_image"] = dict(hor_position)
    elif index_fraction_x < 0.05 and index_fraction_y > 0.95:
        strongest_position = ver_position
        strongest_position["nearest_image"] = dict(ver_position)
    elif index_fraction_x > 0.95 and index_fraction_y > 0.95:
        strongest_position = hor_and_ver_position
        strongest_position["nearest_image"] = dict(hor_and_ver_position)
    else:
        strongest_position = {}

        # Vertical position
        if ver_position is None:
            if isHorizontalRotationAxis:
                strongest_position["sampx"] = start_position["sampx"]
                strongest_position["sampy"] = start_position["sampy"]
            else:
                strongest_position["phiy"] = start_position["phiy"]
                strongest_position["phiz"] = start_position["phiz"]
        elif isHorizontalRotationAxis:
            strongest_position["sampx"] = (
                start_position["sampx"]
                + (ver_position["sampx"] - start_position["sampx"]) * index_fraction_y
            )
            strongest_position["sampy"] = (
                start_position["sampy"]
                + (ver_position["sampy"] - start_position["sampy"]) * index_fraction_y
            )
        else:
            strongest_position["phiy"] = start_position["phiy"]
            strongest_position["phiz"] = (
                start_position["phiz"]
                + (ver_position["phiz"] - start_position["phiz"]) * index_fraction_y
            )

        # Horizontal position
        if hor_position is None:
            if isHorizontalRotationAxis:
                strongest_position["phiy"] = start_position["phiy"]
                strongest_position["phiz"] = start_position["phiz"]
            else:
                strongest_position["sampx"] = start_position["sampx"]
                strongest_position["sampy"] = start_position["sampy"]
        elif isHorizontalRotationAxis:
            strongest_position["phiy"] = (
                start_position["phiy"]
                + (hor_position["phiy"] - start_position["phiy"]) * index_fraction_x
            )
            strongest_position["phiz"] = start_position["phiz"]
        else:
            strongest_position["sampx"] = (
                start_position["sampx"]
                + (hor_position["sampx"] - start_position["sampx"]) * index_fraction_x
            )
            strongest_position["sampy"] = (
                start_position["sampy"]
                + (hor_position["sampy"] - start_position["sampy"]) * index_fraction_x
            )

        strongest_position["omega"] = start_position["omega"]
        if "kappa" in start_position:
            strongest_position["kappa"] = start_position["kappa"]
            strongest_position["phi"] = start_position["phi"]
        strongest_position["focus"] = start_position["focus"]
        strongest_position["indexY"] = com_index_x
        strongest_position["indexZ"] = com_index_y
        # Find closest image
        if index_fraction_x <= 0.5 and index_fraction_y <= 0.5:
            strongest_position["nearest_image"] = dict(start_position)
        elif (
            index_fraction_x > 0.5
            and index_fraction_y <= 0.5
            and hor_position is not None
        ):
            strongest_position["nearest_image"] = dict(hor_position)
        elif (
            index_fraction_x < 0.5
            and index_fraction_y > 0.5
            and ver_position is not None
        ):
            strongest_position["nearest_image"] = dict(ver_position)
        else:
            strongest_position = {}
            if ver_position is None:
                strongest_position["sampx"] = start_position["sampx"]
                strongest_position["sampy"] = start_position["sampy"]
            else:
                strongest_position["sampx"] = (
                    start_position["sampx"]
                    + (ver_position["sampx"] - start_position["sampx"])
                    * index_fraction_y
                )
                strongest_position["sampy"] = (
                    start_position["sampy"]
                    + (ver_position["sampy"] - start_position["sampy"])
                    * index_fraction_y
                )
            if hor_position is None:
                strongest_position["phiy"] = start_position["phiy"]
            else:
                strongest_position["phiy"] = (
                    start_position["phiy"]
                    + (hor_position["phiy"] - start_position["phiy"]) * index_fraction_x
                )
            strongest_position["phiz"] = start_position["phiz"]
            strongest_position["omega"] = start_position["omega"]
            if "kappa" in start_position:
                strongest_position["kappa"] = start_position["kappa"]
                strongest_position["phi"] = start_position["phi"]
            strongest_position["focus"] = start_position["focus"]
            strongest_position["indexY"] = com_index_x
            strongest_position["indexZ"] = com_index_y
            # Find closest image
            if index_fraction_x <= 0.5 and index_fraction_y <= 0.5:
                strongest_position["nearest_image"] = dict(start_position)
            elif (
                index_fraction_x > 0.5
                and index_fraction_y <= 0.5
                and hor_position is not None
            ):
                strongest_position["nearest_image"] = dict(hor_position)
            elif (
                index_fraction_x < 0.5
                and index_fraction_y > 0.5
                and ver_position is not None
            ):
                strongest_position["nearest_image"] = dict(ver_position)
            elif hor_and_ver_position is not None:
                strongest_position["nearest_image"] = dict(hor_and_ver_position)
            else:
                strongest_position["nearest_image"] = dict(start_position)
    return strongest_position


def find_strongest_position(
    grid_info,
    meshPositions,
    dataArray=None,
    data_threshold=None,
    upper_threshold=0.25,
    lower_threshold=0.1,
    isHorizontalRotationAxis=True,
    diffractionSignalDetection="Dozor",
    correct_dozorm2=False,
):
    numpy.set_printoptions(threshold=10000, linewidth=10000)
    logger = workflow_logging.getLogger()
    if dataArray is None:
        dataArray = _createDataArray(
            grid_info,
            meshPositions,
            diffractionSignalDetection=diffractionSignalDetection,
        )
    strongest_position = None
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    dx_mm = grid_info["dx_mm"]
    dy_mm = grid_info["dy_mm"]
    #     data_copy = numpy.array(data)
    # Offset data points which are indexed
    #     for position in meshPositions:
    #         if "spaceGroup" in position:
    #             indexY = position["indexY"]
    #             indexZ = position["indexZ"]
    #             data_copy[indexZ, indexY] *= 4
    #             logger.info("Increasing intensity for data point (%d,%d), space group = %s" % (indexY, indexZ, position["spaceGroup"]))
    logger.debug("Data:")
    #     logger.debug("Data (json): %s" % json.dumps(data.tolist()))
    #    logger.debug("dataArray: {0}".format(dataArray))
    #     logger.debug("Data after spacegroup weighting:")
    #     logger.debug(data_copy)
    # Find strongest position
    max_data = numpy.max(dataArray)
    logger.debug("Max data: %r" % max_data)
    if max_data > 0.001:
        index_max = numpy.where(dataArray == max_data)
        index_max_x = index_max[1][0]
        index_max_y = index_max[0][0]
        logger.debug("Index of max position: (%d, %d)" % (index_max_x, index_max_y))
        #     max_data_copy = numpy.max(data_copy)
        #     logger.debug("Max data after weighting: %r" % max_data_copy)
        #     index_max_copy = numpy.where(data_copy == max_data_copy)
        #     index_max_x_copy = index_max_copy[1][0]
        #     index_max_y_copy = index_max_copy[0][0]
        for position in meshPositions:
            indexY = position["indexY"]
            indexZ = position["indexZ"]
            if index_max_x == indexY and index_max_y == indexZ:
                strongest_position = dict(position)
                logger.debug("Strongest position:")
                logger.debug(pprint.pformat(strongest_position))
                break
    # Check if we have any signal!
    if strongest_position is None:
        bDiffractionSignal = False
    else:
        logger.info("Strongest grid position:")
        strPos = ""
        if "indexY" in strongest_position and "indexZ" in strongest_position:
            strPos += "x=%2d y=%2d " % (
                strongest_position["indexY"] + 1,
                strongest_position["indexZ"] + 1,
            )
            strPos += "%s " % strongest_position["imageName"]
        strPos += "sampx=%.3f " % strongest_position["sampx"]
        strPos += "sampy=%.3f " % strongest_position["sampy"]
        if "phiy" in position:
            strPos += "phiy=%.3f " % strongest_position["phiy"]
        if "phiz" in position:
            strPos += "phiz=%.3f " % strongest_position["phiz"]
        if "totalIntegratedSignal" in strongest_position:
            strPos += "totIntSign=%.3g " % strongest_position["totalIntegratedSignal"]
        if "dozor_score" in strongest_position:
            strPos += "dozor_score=%.3f " % strongest_position["dozor_score"]
        if "spaceGroup" in strongest_position:
            strPos += "spaceGroup=%s " % strongest_position["spaceGroup"]
        logger.info(strPos)
        logger.debug(pprint.pformat(strongest_position))
        # Check that we have diffraction signal
        if "spaceGroup" in strongest_position:
            bDiffractionSignal = True
        else:
            bDiffractionSignal = False
            if (
                diffractionSignalDetection.startswith("Dozor")
                and "dozor_score" in strongest_position
            ):
                if strongest_position["dozor_score"] > 0.001:
                    bDiffractionSignal = True
            elif (
                diffractionSignalDetection.startswith("Spotfinder")
                and "totalIntegratedSignal" in strongest_position
            ):
                if strongest_position["totalIntegratedSignal"] > 1.0e4:
                    bDiffractionSignal = True
            elif (
                diffractionSignalDetection.startswith("TotalIntensity")
                and "totalIntensity" in strongest_position
            ):
                if strongest_position["totalIntensity"] > 1.0e4:
                    bDiffractionSignal = True
            if not bDiffractionSignal:
                logger.info("No diffraction signal detected!")
                strongest_position = None
    if bDiffractionSignal:
        # Filter data
        T = numpy.ones((ny, nx)) * max_data * upper_threshold
        data_threshold = numpy.greater(dataArray, T)
        # Find contiguous regions
        # From http://stackoverflow.com/questions/3310681/finding-blank-regions-in-image:
        filled = scipy.ndimage.morphology.binary_fill_holes(data_threshold)
        objects, num_objects = scipy.ndimage.label(filled)
        logger.debug("Number of objects: %d" % num_objects)
        logger.debug("Objects: %r" % objects)
        # Loop over all objects
        for object_no in range(1, num_objects + 1):
            # Centre of mass of object
            index_object = numpy.where(objects == object_no)
            index_x_object = index_object[1]
            index_y_object = index_object[0]
            data_object = dataArray[index_object]
            max_peak = numpy.max(data_object)
            if abs(max_peak - max_data) < 0.00001:
                break
        logger.debug("Object: %r" % data_object)
        sum_data_object = numpy.sum(data_object)
        sum_data_object_x = numpy.dot(index_x_object, data_object)
        sum_data_object_y = numpy.dot(index_y_object, data_object)
        com_index_x = sum_data_object_x / sum_data_object
        com_index_y = sum_data_object_y / sum_data_object
        logger.debug("Centre of mass index: (%.3f,%.3f)" % (com_index_x, com_index_y))
        index_x = int(com_index_x)
        index_y = int(com_index_y)
        index_fraction_x = com_index_x - index_x
        index_fraction_y = com_index_y - index_y
        #             logger.debug("Position: %s" % pprint.pformat(strongest_position))
        strongest_position = _find_strongest_position(
            meshPositions,
            index_x,
            index_y,
            index_fraction_x,
            index_fraction_y,
            com_index_x,
            com_index_y,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
            grid_info=grid_info,
            correct_dozorm2=correct_dozorm2,
        )
    if strongest_position is not None:
        # Size of peak
        closest_index_x = int(com_index_x + 0.5)
        closest_index_y = int(com_index_y + 0.5)
        index_x_cross = index_x_object[numpy.where(index_y_object == closest_index_y)]
        index_y_cross = index_y_object[numpy.where(index_x_object == closest_index_x)]
        logger.debug(
            "Indices of cross: %d, %r and %d, %r"
            % (closest_index_x, index_y_cross, closest_index_y, index_x_cross)
        )
        no_pixels_cross_x = len(index_x_cross)
        no_pixels_cross_y = len(index_y_cross)
        logger.debug(
            "Number of pixels in cross: x = %d, y = %d"
            % (no_pixels_cross_x, no_pixels_cross_y)
        )
        min_index_x = numpy.min(index_x_cross)
        max_index_x = numpy.max(index_x_cross)
        min_index_y = numpy.min(index_y_cross)
        max_index_y = numpy.max(index_y_cross)
        if nx > 1:
            x_size = float(dx_mm / (nx - 1) * (max_index_x - min_index_x + 1))
            logger.debug("x size = %.3f mm" % x_size)
        else:
            x_size = None
        if ny > 1:
            y_size = float(dy_mm / (ny - 1) * (max_index_y - min_index_y + 1))
            logger.debug("y size = %.3f mm" % y_size)
        else:
            y_size = None
        strongest_position["max_peak"] = max_peak
        strongest_position["no_pixels_cross_x"] = no_pixels_cross_x
        strongest_position["no_pixels_cross_y"] = no_pixels_cross_y
        strongest_position["peak_size_x"] = x_size
        strongest_position["peak_size_y"] = y_size
        strongest_position["nearest_image"]["peak_size_x"] = x_size
        strongest_position["nearest_image"]["peak_size_y"] = y_size
        # Find crystal size using lower_threshold
        T = numpy.ones((ny, nx)) * max_data * lower_threshold
        data_threshold = numpy.greater(dataArray, T)
        # Find contiguous regions
        # From http://stackoverflow.com/questions/3310681/finding-blank-regions-in-image:
        lower_filled = scipy.ndimage.morphology.binary_fill_holes(data_threshold)
        lower_objects, lower_num_objects = scipy.ndimage.label(lower_filled)
        logger.debug("Lower threshold - number of objects: %d" % lower_num_objects)
        logger.debug("Lower threshold - objects: %r" % lower_objects)
        logger.debug(pprint.pformat(strongest_position))
        # Find object
        if "nearest_image" in strongest_position:
            lower_indexY = strongest_position["nearest_image"]["indexY"]
            lower_indexZ = strongest_position["nearest_image"]["indexZ"]
        else:
            lower_indexY = strongest_position["indexY"]
            lower_indexZ = strongest_position["indexZ"]
        lower_index_object = numpy.where(
            lower_objects == lower_objects[lower_indexZ, lower_indexY]
        )
        # Size of peak
        lower_index_x_object = lower_index_object[1]
        lower_index_y_object = lower_index_object[0]
        lower_index_x_cross = lower_index_x_object[
            numpy.where(lower_index_y_object == lower_indexZ)
        ]
        lower_index_y_cross = lower_index_y_object[
            numpy.where(lower_index_x_object == lower_indexY)
        ]
        logger.debug(
            "Indices of cross: %d, %r and %d, %r"
            % (lower_indexY, lower_index_y_cross, lower_indexZ, lower_index_x_cross)
        )
        lower_min_index_x = numpy.min(lower_index_x_cross)
        lower_max_index_x = numpy.max(lower_index_x_cross)
        lower_min_index_y = numpy.min(lower_index_y_cross)
        lower_max_index_y = numpy.max(lower_index_y_cross)
        if nx > 1:
            lower_x_size = float(
                dx_mm / (nx - 1) * (lower_max_index_x - lower_min_index_x + 1)
            )
            logger.debug("Lower x size = %.3f mm" % lower_x_size)
        else:
            lower_x_size = None
        if ny > 1:
            lower_y_size = float(
                dy_mm / (ny - 1) * (lower_max_index_y - lower_min_index_y + 1)
            )
            logger.debug("Lower y size = %.3f mm" % lower_y_size)
        else:
            lower_y_size = None
        strongest_position["crystal_size_x"] = lower_x_size
        strongest_position["crystal_size_y"] = lower_y_size
        strongest_position["nearest_image"]["crystal_size_x"] = lower_x_size
        strongest_position["nearest_image"]["crystal_size_y"] = lower_y_size
    logger.debug("Strongest position: %s" % pprint.pformat(strongest_position))
    return strongest_position


def find_all_positions(
    grid_info,
    meshPositions,
    maxNoPositions=100,
    data_threshold=None,
    upper_threshold=0.25,
    lower_threshold=0.1,
    beamSizeX=None,
    beamSizeY=None,
    radius=3,
    diffractionSignalDetection=None,
    correct_dozorm2=False,
):
    dataArray = _createDataArray(
        grid_info, meshPositions, diffractionSignalDetection=diffractionSignalDetection
    )
    #     dx_mm = grid_info["dx_mm"]
    #     dy_mm = grid_info["dx_mm"]
    #     nx = grid_info["steps_x"]
    #     ny = grid_info["steps_y"]
    #     stepSizeX = dx_mm / (nx - 1 )
    #     stepSizeY = dx_mm / (ny - 1 )
    allPositions = []
    for index in range(int(maxNoPositions)):
        best_position = find_best_position(
            grid_info,
            meshPositions,
            dataArray=dataArray,
            upper_threshold=upper_threshold,
            diffractionSignalDetection=diffractionSignalDetection,
            correct_dozorm2=correct_dozorm2,
        )
        if best_position is None:
            break
        else:
            dataArray = remove_position_from_array(
                dataArray, best_position, radius=radius
            )
            allPositions.append(best_position)
    return allPositions


def createColourMap():
    cdict = {
        "red": (
            (0.0, 0.0, 0.0),
            (0.4, 1.0, 1.0),
            (0.6, 1.0, 1.0),
            (0.8, 1.0, 1.0),
            (1.0, 1.0, 1.0),
        ),
        "green": (
            (0.0, 0.0, 0.0),
            (0.4, 0.0, 0.0),
            (0.6, 0.4, 0.4),
            (0.8, 0.7, 0.7),
            (1.0, 1.0, 1.0),
        ),
        "blue": (
            (0.0, 0.0, 0.0),
            (0.4, 0.0, 0.0),
            (0.6, 0.0, 0.0),
            (0.8, 0.1, 0.1),
            (1.0, 0.3, 0.3),
        ),
    }
    return matplotlib.colors.LinearSegmentedColormap("my_colormap", cdict, N=256)


def create_image(
    grid_info,
    meshPositions,
    listBestPosition,
    plot_directory,
    data_threshold=None,
    diffractionSignalDetection="Dozor",
):
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    dx = grid_info["dx_mm"]
    dy = grid_info["dy_mm"]
    if nx == 1 or ny == 1:
        image_pixel_size_x = 20
        image_pixel_size_y = 20
    elif (dx / nx) > (dy / ny):
        image_pixel_size_x = 20
        image_pixel_size_y = int((dy / ny) / (dx / nx) * 20)
    else:
        image_pixel_size_x = int((dx / nx) / (dy / ny) * 20)
        image_pixel_size_y = 20
    # Size of cross indicating position
    if len(listBestPosition) > 1 and (nx < 10 or ny < 5):
        deltaIndexY = 0.75
        deltaIndexZ = 0.75
    else:
        deltaIndexY = 2
        deltaIndexZ = 2
    plot_file = "grid_image.png"
    data = _createDataArray(
        grid_info,
        meshPositions,
        data_threshold,
        diffractionSignalDetection=diffractionSignalDetection,
    )
    max_data = numpy.max(data)
    data = data / max_data
    cmap = createColourMap()
    pprint.pprint(data)
    data2 = cmap(data) * 255
    data2[:, :, -1] = numpy.asarray(data[:, :] * 100 + 100, numpy.uint8)
    img = Image.fromarray(numpy.uint8(data2), "RGBA")
    newsize_x = grid_info["steps_x"] * image_pixel_size_x
    newsize_y = grid_info["steps_y"] * image_pixel_size_y
    img = img.resize((newsize_x, newsize_y), Image.NEAREST)
    # Plot the cross or crosses
    pixels = img.load()  # create the pixel map
    if nx != 1 and ny != 1:
        for bestPosition in listBestPosition:
            if bestPosition is not None:
                indexY = bestPosition["indexY"]
                indexZ = bestPosition["indexZ"]
                startIndexY = indexY - deltaIndexY
                if startIndexY < 0:
                    startIndexY = 0
                stopIndexY = indexY + deltaIndexY
                if stopIndexY > nx - 1:
                    stopIndexY = nx - 1
                startIndexZ = indexZ - deltaIndexZ
                if startIndexZ < 0:
                    startIndexZ = 0
                stopIndexZ = indexZ + deltaIndexZ
                if stopIndexZ > ny - 1:
                    stopIndexZ = ny - 1
                pixelY = int(indexY * image_pixel_size_x + image_pixel_size_x / 2)
                pixelZ = int(indexZ * image_pixel_size_y + image_pixel_size_y / 2)
                startPixelY = int(
                    startIndexY * image_pixel_size_x + image_pixel_size_x / 2
                )
                stopPixelY = int(
                    stopIndexY * image_pixel_size_x + image_pixel_size_x / 2
                )
                startPixelZ = int(
                    startIndexZ * image_pixel_size_y + image_pixel_size_y / 2
                )
                stopPixelZ = int(
                    stopIndexZ * image_pixel_size_y + image_pixel_size_y / 2
                )
                if stopPixelY > nx - 1:
                    stopPixelY = nx - 1
                if stopPixelZ > ny - 1:
                    stopPixelZ = ny - 1
                for z in range(startPixelZ, stopPixelZ):
                    pixels[pixelY, z] = (255, 255, 255, 255)
                for y in range(startPixelY, stopPixelY):
                    pixels[y, pixelZ] = (255, 255, 255, 255)
                # pprint.pprint(pixels[startIndexY:stopIndexY, ])
                # pprint.pprint(pixels[int(indexY * 50 + 25), startIndexZ:stopIndexZ])
    plot_path = os.path.join(plot_directory, plot_file)
    img.save(plot_path)
    return plot_path


def create_ice_rings_image(
    grid_info,
    meshPositions,
    listBestPosition,
    plot_directory,
    data_threshold=None,
    diffractionSignalDetection="Dozor",
):
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    dx = grid_info["dx_mm"]
    dy = grid_info["dy_mm"]
    if nx == 1 or ny == 1:
        image_pixel_size_x = 20
        image_pixel_size_y = 20
    elif (dx / nx) > (dy / ny):
        image_pixel_size_x = 20
        image_pixel_size_y = int((dy / ny) / (dx / nx) * 20)
    else:
        image_pixel_size_x = int((dx / nx) / (dy / ny) * 20)
        image_pixel_size_y = 20
    plot_file = "grid_ice_rings_image.png"
    data = _createIceRingsDataArray(grid_info, meshPositions)
    max_data = numpy.max(data)
    data = data / max_data
    cmap = createColourMap()
    pprint.pprint(data)
    data2 = cmap(data) * 255
    data2[:, :, -1] = numpy.asarray(data[:, :] * 100 + 100, numpy.uint8)
    img = Image.fromarray(numpy.uint8(data2), "RGBA")
    newsize_x = grid_info["steps_x"] * image_pixel_size_x
    newsize_y = grid_info["steps_y"] * image_pixel_size_y
    img = img.resize((newsize_x, newsize_y), Image.NEAREST)
    # Plot the cross or crosses
    _ = img.load()  # create the pixel map
    plot_path = os.path.join(plot_directory, plot_file)
    img.save(plot_path)
    crop_image(plot_path)
    return plot_path


def create_plot_file(
    grid_info,
    meshPositions,
    listBestPosition,
    plot_directory,
    data_threshold=None,
    diffractionSignalDetection="Dozor",
):
    plot_file = "grid.png"
    logger = workflow_logging.getLogger()
    data = _createDataArray(
        grid_info,
        meshPositions,
        data_threshold,
        diffractionSignalDetection=diffractionSignalDetection,
    )
    deltaX = grid_info["dx_mm"]
    deltaY = grid_info["dy_mm"]
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    # Size of cross indicating position
    if len(listBestPosition) > 1 and (nx < 10 or ny < 5):
        deltaIndexY = 0.75
        deltaIndexZ = 0.75
    else:
        deltaIndexY = 2
        deltaIndexZ = 2
    pyplot.ioff()
    if nx == 1 or ny == 1:
        plotSizeX = 15
        plotSizeY = 6
    else:
        plotSizeX = 10 * deltaX / deltaY
        plotSizeY = 12
        if plotSizeX > 20:
            plotSizeX = 20
            plotSizeY = 20 * deltaY / deltaX + 2.5
    pyplot.figure(figsize=(plotSizeX * 0.65 * 1.2, plotSizeY * 0.65))
    ax = pyplot.gca()
    # Move left and bottom spines outward by 10 points
    ax.spines["left"].set_position(("outward", 10))
    ax.spines["bottom"].set_position(("outward", 10))
    # Hide the right and top spines
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position("left")
    ax.xaxis.set_ticks_position("bottom")
    if nx == 1:
        logger.debug("nx=1, ny=%d" % ny)
        logger.debug("data: %r" % data)
        pyplot.plot(range(1, ny + 1), data[:, 0])
        for bestPosition in listBestPosition:
            if bestPosition is not None:
                indexZ = bestPosition["indexZ"]
                pyplot.plot(
                    [indexZ + 1, indexZ + 1], [numpy.min(data), numpy.max(data)]
                )
        for position in meshPositions:
            if "spaceGroup" in position:
                indexZ = position["indexZ"]
                pyplot.text(indexZ + 1, data[indexZ, 0], position["spaceGroup"])
        pyplot.xlabel("X")
        xtickRange = range(1, int(ny) + 1)
        pyplot.xticks(xtickRange, xtickRange)
        pyplot.xlim([0.5, ny + 0.5])
        pyplot.ylabel("Diffraction signal")
    #        pyplot.title("Result and Bravais lattice")
    elif ny == 1:
        pyplot.plot(range(1, nx + 1), data[0, :])
        for bestPosition in listBestPosition:
            if bestPosition is not None:
                indexY = bestPosition["indexY"]
                pyplot.plot(
                    [indexY + 1, indexY + 1], [numpy.min(data), numpy.max(data)]
                )
        for position in meshPositions:
            if "spaceGroup" in position:
                indexY = position["indexY"]
                pyplot.text(indexY + 1, data[0, indexY], position["spaceGroup"])
        pyplot.xlabel("X")
        xtickRange = range(1, int(nx) + 1)
        pyplot.xticks(xtickRange, xtickRange)
        pyplot.xlim([0.5, nx + 0.5])
        pyplot.ylabel("Diffraction signal")
    #        pyplot.title("Result and Bravais lattice")
    else:
        #    if True:
        xPos = [i * deltaX / float(nx - 1) for i in range(nx)]
        yPos = [i * deltaY / float(ny - 1) for i in range(ny)][::-1]
        # Determine plot size
        # ax.imshow(X, cmap=cm.jet, interpolation='nearest')
        imgplot = pyplot.imshow(
            data,
            extent=[
                -1 / 2.0 * deltaX / float(nx - 1),
                deltaX + 1 / 2.0 * deltaX / float(nx - 1),
                -1 / 2.0 * deltaY / float(ny - 1),
                deltaY + 1 / 2.0 * deltaY / float(ny - 1),
            ],
            #                             extent=[0.5, nx+0.5, ny+0.5, 0.5],
            aspect="equal",
            interpolation="nearest",
            cmap="bone",
            origin="upper",
        )
        if nx < 20:
            xPosRange = xPos
            xtickRange = range(1, int(nx + 1))
        else:
            xPosRange = list(xPos[i] for i in range(4, int(nx), 5))
            xtickRange = range(5, int(nx + 1), 5)
        if ny < 20:
            yPosRange = yPos
            ytickRange = range(1, int(ny + 1))
        else:
            yPosRange = list(yPos[i] for i in range(4, int(ny), 5))
            ytickRange = range(5, int(ny + 1), 5)
        # Move left and bottom spines outward by 10 points
        ax.spines["left"].set_position(("outward", 10))
        ax.spines["bottom"].set_position(("outward", 10))
        # Hide the right and top spines
        ax.spines["right"].set_visible(False)
        ax.spines["top"].set_visible(False)
        # Only show ticks on the left and bottom spines
        ax.yaxis.set_ticks_position("left")
        ax.xaxis.set_ticks_position("bottom")
        pyplot.xticks(xPosRange, xtickRange)
        pyplot.yticks(yPosRange, ytickRange)
        for position in meshPositions:
            if "spaceGroup" in position:
                indexY = position["indexY"]
                indexZ = position["indexZ"]
                pyplot.text(indexY + 1, indexZ + 1, position["spaceGroup"])
        cm_name = "hotmodify"
        if cm_name not in matplotlib.colormaps:
            matplotlib.colormaps.register(name=cm_name, cmap=createColourMap())
        imgplot.set_cmap(cm_name)
        pyplot.colorbar(orientation="vertical", shrink=0.75, pad=0.1)
        #        pyplot.plot([xPos[pos] for pos in x], [yPos[pos] for pos in y], 'wo', markersize=20)
        pyplot.xlim(
            -1 / 2.0 * deltaX / float(nx - 1), deltaX + 1 / 2.0 * deltaX / float(nx - 1)
        )
        pyplot.ylim(
            -1 / 2.0 * deltaY / float(ny - 1), deltaY + 1 / 2.0 * deltaY / float(ny - 1)
        )
        for bestPosition in listBestPosition:
            if bestPosition is not None:
                indexY = bestPosition["indexY"]
                indexZ = bestPosition["indexZ"]
                startIndexY = indexY - deltaIndexY
                if startIndexY < 0:
                    startIndexY = -0.5
                stopIndexY = indexY + deltaIndexY
                if stopIndexY > nx - 1:
                    stopIndexY = nx - 0.5
                startIndexZ = indexZ - deltaIndexZ
                if startIndexZ < 0:
                    startIndexZ = -0.5
                stopIndexZ = indexZ + deltaIndexZ
                if stopIndexZ > ny - 1:
                    stopIndexZ = ny - 0.5
                #                 pyplot.plot(numpy.array([indexY,indexY])*deltaX/float(nx-1), (ny - numpy.array([startIndexZ,stopIndexZ]) - 1)*deltaY/float(ny-1), color="#ADDFFF", linewidth=2)
                pyplot.plot(
                    numpy.array([indexY, indexY]) * deltaX / float(nx - 1),
                    (ny - numpy.array([startIndexZ, stopIndexZ]) - 1)
                    * deltaY
                    / float(ny - 1),
                    color="#ADDFFF",
                    linewidth=2,
                )
                pyplot.plot(
                    numpy.array([startIndexY, stopIndexY]) * deltaX / float(nx - 1),
                    (ny - numpy.array([indexZ, indexZ]) - 1) * deltaY / float(ny - 1),
                    color="#ADDFFF",
                    linewidth=2,
                )
        pyplot.xlabel("X")
        pyplot.ylabel("Y")
        pyplot.title("Diffraction signal")
    logger.debug("Plot directory: %s, plot file: %s" % (plot_directory, plot_file))
    #    pyplot.savefig(os.path.join(plot_directory, plot_file), dpi=75)
    pyplot.savefig(os.path.join(plot_directory, plot_file), dpi=80)
    pyplot.close()
    #    pprint.pprint(resultPositions)
    # #        pyplot.savefig('/tmp/result.png', bbox_inches = 'tight')
    crop_image(os.path.join(plot_directory, plot_file))
    return plot_file


def create_ice_rings_plot_file(grid_info, meshPositions, plot_directory):
    plot_file = "grid_ice_rings.png"
    logger = workflow_logging.getLogger()
    data = _createIceRingsDataArray(grid_info, meshPositions)
    deltaX = grid_info["dx_mm"]
    deltaY = grid_info["dy_mm"]
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    plotSizeX = 10 * deltaX / deltaY
    plotSizeY = 12
    if plotSizeX > 20:
        plotSizeX = 20
        plotSizeY = 20 * deltaY / deltaX + 2.5
    pyplot.figure(figsize=(plotSizeX * 0.65 * 1.2, plotSizeY * 0.65))
    ax = pyplot.gca()
    # Move left and bottom spines outward by 10 points
    ax.spines["left"].set_position(("outward", 10))
    ax.spines["bottom"].set_position(("outward", 10))
    # Hide the right and top spines
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position("left")
    ax.xaxis.set_ticks_position("bottom")
    xPos = [i * deltaX / float(nx - 1) for i in range(nx)]
    yPos = [i * deltaY / float(ny - 1) for i in range(ny)][::-1]
    # Determine plot size
    # ax.imshow(X, cmap=cm.jet, interpolation='nearest')
    imgplot = pyplot.imshow(
        data,
        extent=[
            -1 / 2.0 * deltaX / float(nx - 1),
            deltaX + 1 / 2.0 * deltaX / float(nx - 1),
            -1 / 2.0 * deltaY / float(ny - 1),
            deltaY + 1 / 2.0 * deltaY / float(ny - 1),
        ],
        #                             extent=[0.5, nx+0.5, ny+0.5, 0.5],
        aspect="equal",
        interpolation="nearest",
        cmap="bone",
        origin="upper",
    )
    if nx < 20:
        xPosRange = xPos
        xtickRange = range(1, int(nx + 1))
    else:
        xPosRange = list(xPos[i] for i in range(4, int(nx), 5))
        xtickRange = range(5, int(nx + 1), 5)
    if ny < 20:
        yPosRange = yPos
        ytickRange = range(1, int(ny + 1))
    else:
        yPosRange = list(yPos[i] for i in range(4, int(ny), 5))
        ytickRange = range(5, int(ny + 1), 5)
    # Move left and bottom spines outward by 10 points
    ax.spines["left"].set_position(("outward", 10))
    ax.spines["bottom"].set_position(("outward", 10))
    # Hide the right and top spines
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position("left")
    ax.xaxis.set_ticks_position("bottom")
    pyplot.xticks(xPosRange, xtickRange)
    pyplot.yticks(yPosRange, ytickRange)
    imgplot.set_cmap("hotmodify")
    pyplot.colorbar(orientation="vertical", shrink=0.75, pad=0.1)
    #        pyplot.plot([xPos[pos] for pos in x], [yPos[pos] for pos in y], 'wo', markersize=20)
    pyplot.xlim(
        -1 / 2.0 * deltaX / float(nx - 1), deltaX + 1 / 2.0 * deltaX / float(nx - 1)
    )
    pyplot.ylim(
        -1 / 2.0 * deltaY / float(ny - 1), deltaY + 1 / 2.0 * deltaY / float(ny - 1)
    )
    pyplot.xlabel("X")
    pyplot.ylabel("Y")
    pyplot.title("Ice rings")
    logger.debug("Plot directory: %s, plot file: %s" % (plot_directory, plot_file))
    #    pyplot.savefig(os.path.join(plot_directory, plot_file), dpi=75)
    pyplot.savefig(os.path.join(plot_directory, plot_file), dpi=80)
    pyplot.close()
    #    pprint.pprint(resultPositions)
    # #        pyplot.savefig('/tmp/result.png', bbox_inches = 'tight')
    crop_image(os.path.join(plot_directory, plot_file))
    return plot_file


def grid_create_working_directory(workflow_working_dir):
    grid_working_directory_name = "grid_" + time.strftime(
        "%H%M%S", time.localtime(time.time())
    )
    grid_working_directory = os.path.join(
        workflow_working_dir, grid_working_directory_name
    )
    if not os.path.exists(grid_working_directory):
        os.makedirs(grid_working_directory, 0o755)
    return grid_working_directory


def _commandsForMovingMotors(beamline, position):
    if beamline in ["id23eh2"]:
        commands = "sampx = %.3f mm, sampy = %.3f mm, phiz = %.3f mm" % (
            position["sampx"],
            position["sampy"],
            position["phiz"],
        )
    else:
        commands = "sampx = %.3f mm, sampy = %.3f mm, phiy = %.3f mm" % (
            position["sampx"],
            position["sampy"],
            position["phiy"],
        )
    return commands


def _createTable(
    beamline,
    listPositions,
    do_list_positions=False,
    isHorizontalRotationAxis=True,
    diffractionSignalDetection="Dozor",
    tableTitle="",
    workflowStepReport=None,
    doDistlSignalStrength=False,
):
    logger = workflow_logging.getLogger()
    dictTable = {"type": "table", "title": tableTitle, "columns": [], "data": []}
    dictTable["columns"].append("No")
    dictTable["columns"].append("X")
    dictTable["columns"].append("Y")
    dictTable["columns"].append("Image file")
    dictTable["columns"].append("Dozor score")
    dictTable["columns"].append("Dozor resolution")
    if doDistlSignalStrength:
        dictTable["columns"].append("Signal 2")
        dictTable["columns"].append("No ice rings")
    if do_list_positions:
        dictTable["columns"].append("Peak x size [mm]")
        dictTable["columns"].append("Peak y size [mm]")
        dictTable["columns"].append("Crystal x size [mm]")
        dictTable["columns"].append("Crystal y size [mm]")
    dictTable["columns"].append("Motor positions for moving to sample position")
    copyListPositions = []
    for position in listPositions:
        if position is not None:
            new_position = position
            if "nearest_image" not in position:
                new_position["nearest_image"] = dict(position)
            copyListPositions.append(dict(new_position))
            if "dozor_score" not in position["nearest_image"]:
                position["nearest_image"]["dozor_score"] = 0.0
            if "totalIntegratedSignal" not in position["nearest_image"]:
                position["nearest_image"]["totalIntegratedSignal"] = 0.0
    sortedListPosition = copyListPositions
    try:
        if diffractionSignalDetection.startswith("Dozor"):
            sortedListPosition = sorted(
                copyListPositions,
                key=lambda position: position["nearest_image"]["dozor_score"],
                reverse=True,
            )
        elif diffractionSignalDetection.startswith("Spotfinder"):
            sortedListPosition = sorted(
                copyListPositions,
                key=lambda position: position["nearest_image"]["totalIntegratedSignal"],
                reverse=True,
            )
        elif diffractionSignalDetection.startswith("TotalIntensity"):
            sortedListPosition = sorted(
                copyListPositions,
                key=lambda position: position["nearest_image"]["totalIntensity"],
                reverse=True,
            )
    except Exception:
        logger.debug("Error when sorting positions")
    for index, position in enumerate(sortedListPosition):
        listLine = []
        listLine.append(index + 1)
        if isinstance(position["indexY"], int):
            listLine.append(position["indexY"] + 1)
        else:
            listLine.append(round(position["indexY"] + 1, 1))
        if isinstance(position["indexY"], int):
            listLine.append(position["indexZ"] + 1)
        else:
            listLine.append(round(position["indexZ"] + 1, 1))
        listLine.append(position["nearest_image"]["imageName"])
        if "dozor_score" in position:
            listLine.append(position["dozor_score"])
        elif "dozor_score" in position["nearest_image"]:
            listLine.append(position["nearest_image"]["dozor_score"])
        else:
            listLine.append(None)
        if "dozorVisibleResolution" in position:
            listLine.append(round(position["dozorVisibleResolution"], 1))
        elif "dozorVisibleResolution" in position["nearest_image"]:
            listLine.append(
                round(position["nearest_image"]["dozorVisibleResolution"], 1)
            )
        else:
            listLine.append(" ")
        if doDistlSignalStrength:
            if "totalIntegratedSignal" in position["nearest_image"]:
                listLine.append(position["nearest_image"]["totalIntegratedSignal"])
            else:
                listLine.append(None)
            if "iceRings" in position["nearest_image"]:
                listLine.append(position["nearest_image"]["iceRings"])
            else:
                listLine.append(None)
        if do_list_positions:
            if "peak_size_x" in position and position["peak_size_x"] is not None:
                listLine.append(round(position["peak_size_x"], 3))
            else:
                listLine.append(None)
            if "peak_size_y" in position and position["peak_size_y"] is not None:
                listLine.append(round(position["peak_size_y"], 3))
            else:
                listLine.append(None)
            if "crystal_size_x" in position and position["crystal_size_x"] is not None:
                listLine.append(round(position["crystal_size_x"], 3))
            else:
                listLine.append(None)
            if "crystal_size_y" in position and position["crystal_size_y"] is not None:
                listLine.append(round(position["crystal_size_y"], 3))
            else:
                listLine.append(None)
        commands = _commandsForMovingMotors(beamline, position)
        listLine.append(commands)
        dictTable["data"].append(listLine)
    if workflowStepReport is not None:
        workflowStepReport.addTable(tableTitle, dictTable["columns"], dictTable["data"])


def _createTableWithoutImageName(
    beamline,
    listPosition,
    isHorizontalRotationAxis=True,
    tableTitle="",
    workflowStepReport=None,
):
    #
    dictTable = {"columns": ["Motor positions:"], "data": []}
    listRow = []
    for position in listPosition:
        commands = _commandsForMovingMotors(beamline, position)
        listRow.append(commands)
    dictTable["data"].append(listRow)
    if workflowStepReport is not None:
        workflowStepReport.addTable(tableTitle, dictTable["columns"], dictTable["data"])


def _createMeshBestTable(beamline, listPositions, workflowStepReport=None):
    tableTitle = "Results from MeshBest"
    dictTable = {"type": "table", "title": tableTitle, "columns": [], "data": []}
    dictTable["columns"].append("X centre")
    dictTable["columns"].append("Y centre")
    if "FWHM" in listPositions[0]:
        dictTable["columns"].append("FWHM [mm]")
    else:
        dictTable["columns"].append("Long axis [mm]")
        dictTable["columns"].append("Short axis [mm]")
        dictTable["columns"].append("Average Dozor Score")
    dictTable["columns"].append("Integral Dozor Score")
    dictTable["columns"].append("Dozor visible resolution")
    dictTable["columns"].append("Closest image")
    dictTable["columns"].append("Motor positions for moving to sample position")
    for meshBestPosition in listPositions:
        listLine = []
        if isinstance(meshBestPosition["indexY"], int):
            listLine.append(meshBestPosition["indexY"] + 1)
        else:
            listLine.append(round(meshBestPosition["indexY"] + 1, 1))
        if isinstance(meshBestPosition["indexZ"], int):
            listLine.append(meshBestPosition["indexZ"] + 1)
        else:
            listLine.append(round(meshBestPosition["indexZ"] + 1, 1))
        if "FWHM" in meshBestPosition:
            listLine.append(meshBestPosition["FWHM"])
        else:
            listLine.append(meshBestPosition["longAxis"])
            listLine.append(meshBestPosition["shortAxis"])
            listLine.append(meshBestPosition["averageDozorScore"])
        listLine.append(meshBestPosition["integralDozorScore"])
        if "dozorVisibleResolution" in meshBestPosition:
            listLine.append(round(meshBestPosition["dozorVisibleResolution"], 1))
        elif "dozorVisibleResolution" in meshBestPosition["nearest_image"]:
            listLine.append(
                round(meshBestPosition["nearest_image"]["dozorVisibleResolution"], 1)
            )
        else:
            listLine.append(" ")
        if "nearest_image" in meshBestPosition:
            listLine.append(meshBestPosition["nearest_image"]["imageName"])
        else:
            listLine.append(meshBestPosition["imageName"])
        listLine.append(_commandsForMovingMotors(beamline, meshBestPosition))
        dictTable["data"].append(listLine)
    if workflowStepReport is not None:
        workflowStepReport.addTable(tableTitle, dictTable["columns"], dictTable["data"])


def _createTableDozorM(dozormListPositions, workflowStepReport=None):
    tableTitle = "dozorm results"
    dictTable = {"type": "table", "title": tableTitle, "columns": [], "data": []}
    dictTable["columns"].append("Crystal number")
    dictTable["columns"].append("Aperture")
    dictTable["columns"].append("Resolution")
    dictTable["columns"].append("Central image")
    dictTable["columns"].append("X")
    dictTable["columns"].append("Y")
    dictTable["columns"].append("Int/Sig")
    dictTable["columns"].append("Images all")
    dictTable["columns"].append("Images dX")
    dictTable["columns"].append("Images dY")
    dictTable["columns"].append("Score sum")
    dictTable["columns"].append("Helical")
    dictTable["columns"].append("Start X")
    dictTable["columns"].append("Start Y")
    dictTable["columns"].append("End X")
    dictTable["columns"].append("End Y")
    dictTable["columns"].append("Helical Int/Sig")
    for position in dozormListPositions:
        listLine = []
        listLine.append(position["number"])
        listLine.append(position["apertureSize"])
        listLine.append(position["dmin"])
        listLine.append(position["imageNumber"])
        listLine.append(position["xPosition"])
        listLine.append(position["yPosition"])
        listLine.append(position["iOverSigma"])
        listLine.append(position["numberOfImagesTotal"])
        listLine.append(position["numberOfImagesTotalX"])
        listLine.append(position["numberOfImagesTotalY"])
        listLine.append(position["score"])
        if position["helical"]:
            listLine.append("Yes")
            listLine.append(position["helicalStartX"])
            listLine.append(position["helicalStartY"])
            listLine.append(position["helicalStopX"])
            listLine.append(position["helicalStopY"])
            listLine.append(position["helicalIoverSigma"])
        else:
            listLine.append("No")
            listLine.append("")
            listLine.append("")
            listLine.append("")
            listLine.append("")
            listLine.append("")
        dictTable["data"].append(listLine)
    if workflowStepReport is not None:
        workflowStepReport.addTable(tableTitle, dictTable["columns"], dictTable["data"])


def _displayThumbnailImage(
    imageName, directory, strPageDir, workflow_working_dir, workflowStepReport=None
):
    imagePath = os.path.join(directory, imageName)
    imageNameWithoutSuffix, imageSuffix = os.path.splitext(imageName)
    if imageSuffix == ".h5":
        h5MasterFilePath, h5DataFilePath, h5FileNumber = path.getH5FilePath(
            imagePath, isFastMesh=True
        )
        realPath = h5DataFilePath
    else:
        realPath = imagePath
    if os.path.exists(realPath):
        # Display thumbnail image of image closest to best position
        diffractionThumbnailOutData = edna2_tasks.create_thumbnails(
            [imagePath], strPageDir, workflow_working_dir, format="jpg"
        )
        if "pathToJPEGImage" in diffractionThumbnailOutData:
            pathToBigJpgImage = diffractionThumbnailOutData["pathToJPEGImage"][0]
            pathToSmallJpgImage = diffractionThumbnailOutData["pathToThumbImage"][0]
            if workflowStepReport is not None:
                workflowStepReport.addImage(
                    pathToBigJpgImage, imageName, pathToSmallJpgImage, 256, 256
                )


def grid_create_result_html_page(
    beamline,
    grid_info,
    meshPositions,
    listPositions,
    directory,
    workflow_working_dir,
    data_threshold=None,
    isHorizontalRotationAxis=True,
    title="Results",
    diffractionSignalDetection="Dozor",
    dozormListPositions=None,
    dozormCrystalMapPath=None,
    dozormColourMapPath=None,
    dozormImageNumberMapPath=None,
    doDistlSignalStrength=False,
):
    workflowStepReport = WorkflowStepReport("Mesh", title)
    plot_file_image = None
    plot_ice_rings_file_image = None
    pathToIceRingsImage = None
    if listPositions is None:
        listPositions = []
    elif isinstance(listPositions, dict):
        listPositions = [listPositions]
    grid_working_directory = grid_create_working_directory(workflow_working_dir)
    index_file = "index.html"
    strPageDir = os.path.join(workflow_working_dir, grid_working_directory)
    plot_file = create_plot_file(
        grid_info,
        meshPositions,
        listPositions,
        os.path.join(workflow_working_dir, grid_working_directory),
        data_threshold,
        diffractionSignalDetection=diffractionSignalDetection,
    )
    # Create base64 image
    pathToImage = os.path.join(strPageDir, plot_file)
    # im = PIL.Image.open(pathToImage)

    if grid_info["steps_x"] != 1 or grid_info["steps_y"] != 1:
        plot_file_image = create_image(
            grid_info,
            meshPositions,
            listPositions,
            os.path.join(workflow_working_dir, grid_working_directory),
            data_threshold,
            diffractionSignalDetection=diffractionSignalDetection,
        )

    if doDistlSignalStrength:
        plot_ice_rings_file = create_ice_rings_plot_file(
            grid_info,
            meshPositions,
            os.path.join(workflow_working_dir, grid_working_directory),
        )
        # Create base64 image
        pathToIceRingsImage = os.path.join(strPageDir, plot_ice_rings_file)
        # im_ice_rings = PIL.Image.open(pathToIceRingsImage)
        if grid_info["steps_x"] != 1 or grid_info["steps_y"] != 1:
            plot_ice_rings_file_image = create_ice_rings_image(
                grid_info,
                meshPositions,
                listPositions,
                os.path.join(workflow_working_dir, grid_working_directory),
                data_threshold,
                diffractionSignalDetection=diffractionSignalDetection,
            )

    # Check if we have a dozorm crystal map
    if dozormCrystalMapPath is not None and os.path.exists(dozormCrystalMapPath):
        shutil.copy(dozormCrystalMapPath, strPageDir)
        pathToDozormCrystalMapPath = os.path.join(
            strPageDir, os.path.basename(dozormCrystalMapPath)
        )
    else:
        pathToDozormCrystalMapPath = None

    # Check if we have a dozorm colour map
    if dozormColourMapPath is not None and os.path.exists(dozormColourMapPath):
        shutil.copy(dozormColourMapPath, strPageDir)
        pathToDozormColourMapPath = os.path.join(
            strPageDir, os.path.basename(dozormColourMapPath)
        )
    else:
        pathToDozormColourMapPath = None

    # Check if we have a dozorm image number map
    if dozormImageNumberMapPath is not None and os.path.exists(
        dozormImageNumberMapPath
    ):
        shutil.copy(dozormImageNumberMapPath, strPageDir)
        pathToDozormImageNumberMap = os.path.join(
            strPageDir, os.path.basename(dozormImageNumberMapPath)
        )
    else:
        pathToDozormImageNumberMap = None

    workflowStepReport.startImageList()
    workflowStepReport.addImage(pathToImage, "Mesh plot")
    if doDistlSignalStrength:
        workflowStepReport.addImage(pathToIceRingsImage, "Ice rings")

    # Table with mesh plot and thumbnail image
    if (
        len(listPositions) >= 1
        and listPositions[0] is not None
        and listPositions[0] != "null"
    ):
        bestPosition = listPositions[0]
        if "imageName" in bestPosition:
            imageName = bestPosition["imageName"]
        else:
            imageName = bestPosition["nearest_image"]["imageName"]
        _displayThumbnailImage(
            imageName,
            directory,
            strPageDir,
            workflow_working_dir,
            workflowStepReport=workflowStepReport,
        )
    workflowStepReport.endImageList()

    if dozormListPositions is not None:
        _createTableDozorM(dozormListPositions, workflowStepReport=workflowStepReport)

    workflowStepReport.startImageList()

    if pathToDozormCrystalMapPath is not None:
        workflowStepReport.addImage(
            pathToDozormCrystalMapPath, "Dozorm map of crystals"
        )

    if pathToDozormColourMapPath is not None:
        workflowStepReport.addImage(
            pathToDozormColourMapPath, "Dozorm colour map of crystals"
        )

    workflowStepReport.endImageList()

    if pathToDozormImageNumberMap is not None:
        workflowStepReport.addImage(
            pathToDozormImageNumberMap, "Dozorm map of image numbers"
        )

    if len(listPositions) == 1:
        bestPosition = listPositions[0]
        if bestPosition is not None:
            tableTitle = "Best position - the sample has automatically been moved to this position"
            if "imageName" in bestPosition:
                _createTable(
                    beamline,
                    [bestPosition],
                    do_list_positions=True,
                    isHorizontalRotationAxis=isHorizontalRotationAxis,
                    diffractionSignalDetection=diffractionSignalDetection,
                    tableTitle=tableTitle,
                    workflowStepReport=workflowStepReport,
                    doDistlSignalStrength=doDistlSignalStrength,
                )
            else:
                _createTableWithoutImageName(
                    beamline,
                    [bestPosition],
                    isHorizontalRotationAxis=isHorizontalRotationAxis,
                    tableTitle=tableTitle,
                    workflowStepReport=workflowStepReport,
                )
                message = "Image closest to best position:"
                _createTable(
                    beamline,
                    [bestPosition["nearest_image"]],
                    do_list_positions=True,
                    isHorizontalRotationAxis=isHorizontalRotationAxis,
                    diffractionSignalDetection=diffractionSignalDetection,
                    tableTitle=message,
                    workflowStepReport=workflowStepReport,
                    doDistlSignalStrength=doDistlSignalStrength,
                )
    #
    # if pathToMeshBestPlotPath is not None:
    if False:
        _createMeshBestTable(beamline, [], workflowStepReport=workflowStepReport)

        workflowStepReport.addImage(pathToImage, "Mesh plot")
    if len(listPositions) > 1:
        _createTable(
            beamline,
            listPositions,
            do_list_positions=True,
            diffractionSignalDetection=diffractionSignalDetection,
            workflowStepReport=workflowStepReport,
            doDistlSignalStrength=doDistlSignalStrength,
        )
    # Explinations...
    signal1text = "Dozor: Criteria that uses intensities over background vs resolution. Popov 2014, to be published."
    workflowStepReport.addInfo(signal1text)
    signal2text = "Signal 2: Labelit distl spotfinder total integrated intensity."
    workflowStepReport.addInfo(signal2text)
    #
    _createTable(
        beamline,
        meshPositions,
        isHorizontalRotationAxis=isHorizontalRotationAxis,
        diffractionSignalDetection=diffractionSignalDetection,
        tableTitle="All positions",
        workflowStepReport=workflowStepReport,
        doDistlSignalStrength=doDistlSignalStrength,
    )
    #
    dictTable = {"columns": [], "data": []}
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    dx_mm = grid_info["dx_mm"]
    dy_mm = grid_info["dy_mm"]
    if nx > 1:
        dictTable["columns"].append("Steps x")
    if ny > 1:
        dictTable["columns"].append("Steps y")
    if nx > 1:
        dictTable["columns"].append("Size x [mm]")
    if ny > 1:
        dictTable["columns"].append("Size y [mm]")
    listRow = []
    if nx > 1:
        listRow.append(nx)
    if ny > 1:
        listRow.append(ny)
    if nx > 1:
        value = round(dx_mm * nx / (nx - 1), 3)
        listRow.append(value)
    if ny > 1:
        value = round(dy_mm * ny / (ny - 1), 3)
        listRow.append(value)
    dictTable["data"].append(listRow)
    workflowStepReport.addTable("Grid size", dictTable["columns"], dictTable["data"])
    #
    dictTable = {"type": "table", "title": "File info", "columns": [], "data": []}
    listRow = []
    dictTable["columns"].append("Raw data directory")
    listRow.append(directory)
    dictTable["columns"].append("Grid working directory")
    listRow.append(grid_working_directory)
    dictTable["data"].append(listRow)
    workflowStepReport.addTable("File info", dictTable["columns"], dictTable["data"])
    #
    workflowStepReport.renderHtml(strPageDir, index_file)
    dict_html = {}
    dict_html["html_directory_path"] = grid_working_directory
    dict_html["index_file_name"] = index_file
    dict_html["plot_file_name"] = plot_file
    # if pathToMeshBestPlotPath is None:
    #     dict_html["plot_file_name"] = plot_file
    # else:
    #     dict_html["plot_file_name"] = os.path.basename(pathToMeshBestPlotPath)
    if plot_file_image is not None:
        dict_html["plot_file_image"] = plot_file_image
    if plot_ice_rings_file_image is not None:
        dict_html["plot_ice_rings_file_image"] = plot_ice_rings_file_image
    dict_html["workflowStep"] = workflowStepReport.getDictReport()
    return dict_html


def getMeshListImagePath(directory, meshPositions):
    if isinstance(meshPositions, str):
        meshPositions = json.loads(meshPositions)
    listImage = []
    for position in meshPositions:
        listImage.append(os.path.join(directory, position["imageName"]))
    return listImage


def mesh_scan_statistics(
    meshPositions,
    _emailAddress=None,
    _threshold=1000,
    _threshold_background3D=1,
    workingDir=None,
):
    # logger = workflow_logging.getLogger()
    if isinstance(meshPositions, str):
        meshPositions = json.loads(meshPositions)
    listTIS = []
    for position in meshPositions:
        if "totalIntegratedSignal" in position:
            if position["totalIntegratedSignal"] >= _threshold:
                listTIS.append(position["totalIntegratedSignal"])
    strMessage = "\n"
    listB3D = []
    for position in meshPositions:
        if "dozor_score" in position:
            if position["dozor_score"] >= _threshold_background3D:
                listB3D.append(position["dozor_score"])
    strMessage += "\n"
    strMessage += "Background 3D\n"
    strMessage += "\n"
    strMessage += "B3D threshold: %.2g\n" % _threshold_background3D
    strMessage += "\n"
    dict_statistics = {}
    try:
        meshAverage = numpy.mean(listB3D)
        dict_statistics["mesh_average"] = float(meshAverage)
        strMessage += "Average = %.3g\n" % round(meshAverage, 2)
        meshVariance = numpy.var(listB3D)
        dict_statistics["mesh_variance"] = float(meshVariance)
        strMessage += "Variance = %.3g\n" % round(meshVariance, 2)
        meshMax = numpy.amax(listB3D)
        dict_statistics["mesh_max"] = float(meshMax)
        strMessage += "Max TIS = %.2g\n" % round(meshMax, 2)
        meshV1 = meshVariance / meshAverage**2
        dict_statistics["mesh_v1"] = float(meshV1)
        strMessage += "V1 = %.2f\n" % round(meshV1, 2)
        meshV2 = meshMax / meshAverage
        dict_statistics["mesh_v2"] = float(meshV2)
        strMessage += "V2 = %.2f\n" % round(meshV2, 2)
        N = ((meshV2 - 1) ** 2) / meshV1
        dict_statistics["mesh_n"] = float(N)
        strMessage += "N = %.2f" % N
    #        meshV3 = (meshMax/meshAverage)/(meshVariance/meshAverage**2)
    #        strMessage += "V3 = %.2f\n" % round(meshV3, 2)
    except Exception as e:
        strMessage += "\n"
        strMessage += "Exception caught during processing: %r" % e
        strMessage += "\n"
    strMessage += "\n\n\n"
    strMeshTable = "Mesh table:\n"
    for position in meshPositions:
        strMeshTable += "y=%2d, z=%2d, " % (
            position["indexY"] + 1,
            position["indexZ"] + 1,
        )
        strMeshTable += "%s" % position["imageName"]
        if "totalIntegratedSignal" in position:
            strMeshTable += ", TIS=%.3g" % position["totalIntegratedSignal"]
        if "dozor_score" in position:
            strMeshTable += ", Dozor=%.3f" % position["dozor_score"]
        if "spaceGroup" in position:
            strMeshTable += ", %s " % position["spaceGroup"]
        strMeshTable += "\n"
    if workingDir is None:
        strMessage += strMeshTable
    else:
        fd, meshTableFile = tempfile.mkstemp(
            suffix=".txt", prefix="meshTable_", dir=workingDir
        )
        os.write(fd, bytes(strMessage + strMeshTable, "utf-8"))
        os.close(fd)
        os.chmod(meshTableFile, 0o644)
        strMessage += "Mesh table file: {0}\n".format(meshTableFile)
    # logger.debug("Mesh scan statistics: \n%s" % strMessage)
    dict_statistics["message"] = strMessage
    return dict_statistics


def writeTISResultFile(meshPositions2D, workflow_working_dir, fileName):
    filePath = os.path.join(workflow_working_dir, fileName)
    f = open(filePath, "w")
    for position in meshPositions2D:
        f.write("%f\n" % position["totalIntegratedSignal"])
    f.close()


def find_best_image_id(mesh_positions, list_image_creation, best_position):
    best_image_id = None
    if best_position is not None:
        if "imageName" in best_position:
            image_name = best_position["imageName"]
        else:
            best_sampx = best_position["sampx"]
            best_sampy = best_position["sampy"]
            best_phiy = best_position["phiy"]
            image_name = None
            min_dist = None
            for mesh_position in mesh_positions:
                sampx = mesh_position["sampx"]
                sampy = mesh_position["sampy"]
                phiy = mesh_position["phiy"]
                distance = math.sqrt(
                    (best_sampx - sampx) ** 2
                    + (best_sampy - sampy) ** 2
                    + (best_phiy - phiy) ** 2
                )
                if min_dist is None or min_dist > distance:
                    min_dist = distance
                    image_name = mesh_position["imageName"]
        for image_creation in list_image_creation:
            if image_name == image_creation["fileName"] and "imageId" in image_creation:
                best_image_id = image_creation["imageId"]
                break
    return best_image_id


def find_ispyb_positions(
    meshPositions, max_no=50, dozor_threshold=-1.0, distl_threshold=-1.0
):
    """Selects the strongest positions for upload to ISPyB"""
    ispyb_positions = []
    dozor_positions = []
    distl_positions = []
    for position in meshPositions:
        if "dozor_score" in position:
            if position["dozor_score"] > dozor_threshold:
                dozor_positions.append(dict(position))
        if "totalIntegratedSignal" in position:
            if position["totalIntegratedSignal"] > distl_threshold:
                distl_positions.append(dict(position))
    if dozor_positions != []:
        ispyb_positions = sorted(
            dozor_positions, key=lambda position: position["dozor_score"], reverse=True
        )
    elif distl_positions != []:
        ispyb_positions = sorted(
            distl_positions,
            key=lambda position: position["totalIntegratedSignal"],
            reverse=True,
        )
    if len(ispyb_positions) > max_no:
        ispyb_positions = ispyb_positions[0:max_no]
    return ispyb_positions


def findRotationAxisOffset(grid_info, fistMeshPositions, secondMeshPositions):
    """This method finds the offset of the rotation axis by correlating two 1D scans taken 180 degrees apart"""
    deltaX = grid_info["dx_mm"]
    deltaY = grid_info["dy_mm"]
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    firstData = _createDataArray(grid_info, fistMeshPositions).flatten()
    secondData = _createDataArray(grid_info, secondMeshPositions).flatten()
    # Turn data around
    inversedSecondData = secondData[::-1]
    corr = numpy.correlate(firstData, inversedSecondData, mode="full")
    if nx == 1:
        offset = -(len(corr) / 2 - numpy.argmax(corr)) * deltaY / ny
    else:
        offset = -(len(corr) / 2 - numpy.argmax(corr)) * deltaX / nx
    return offset


def plot_img(img, plotPath):
    imgshape = img.shape
    extent = (0, imgshape[1], 0, imgshape[0])
    pyplot.imshow(img, extent=extent)
    pyplot.gray()
    # pyplot.colorbar()
    pyplot.savefig(plotPath)
    pyplot.close()
    crop_image(plotPath)
    return


def rgb2gray(rgb):
    return numpy.dot(rgb[..., :3], [0.2989, 0.5870, 0.1140])


def loopExam(
    img, background_image, omega, debug, autoMeshWorkingDir, isVerticalAxis, dictLoop
):
    if isinstance(img, str):
        if img.endswith(".npy"):
            raw_img = numpy.load(img)
        else:
            raw_img = imageio.v3.imread(img)
            if len(raw_img.shape) > 2:
                raw_img = rgb2gray(raw_img)
            else:
                raw_img = raw_img.astype(numpy.int16)
    else:
        # already a numpy array
        raw_img = img

    imgshape = raw_img.shape
    if isVerticalAxis:
        extent = (0, imgshape[0], 0, imgshape[1])
    else:
        extent = (0, imgshape[1], 0, imgshape[0])

    if debug:
        plot_img(raw_img, os.path.join(autoMeshWorkingDir, "rawImage_%03d.png" % omega))

    if background_image.endswith(".npy"):
        background = numpy.load(background_image)
    else:
        background = imageio.v3.imread(background_image)
        if len(background.shape) > 2:
            background = rgb2gray(background)
        else:
            background = background.astype(numpy.int16)

    differenceImage = numpy.abs(background - raw_img)

    if debug:
        plot_img(
            differenceImage,
            plotPath=os.path.join(
                autoMeshWorkingDir, "differenceImage_%03d.png" % omega
            ),
        )

    thresholdValue = 30
    binaryImage = differenceImage >= thresholdValue
    filteredImage = scipy.ndimage.binary_erosion(binaryImage).astype(binaryImage.dtype)
    filteredImage = scipy.ndimage.binary_erosion(filteredImage).astype(
        binaryImage.dtype
    )
    filteredImage = scipy.ndimage.binary_dilation(filteredImage).astype(
        binaryImage.dtype
    )
    filteredImage = scipy.ndimage.binary_dilation(filteredImage).astype(
        binaryImage.dtype
    )
    #     filteredImage = scipy.ndimage.median_filter(binaryImage , size =5)

    if debug:
        plot_img(
            filteredImage,
            plotPath=os.path.join(autoMeshWorkingDir, "filteredImage_%03d.png" % omega),
        )

    #    print filteredImage.shape
    ny, nx = filteredImage.shape
    shapeListIndex = []
    shapeListUpper = []
    shapeListLower = []
    if isVerticalAxis:
        for indexY in range(ny):
            column = filteredImage[ny - indexY - 1, :]
            indices = numpy.where(column)[0]
            if len(indices) > 0:
                shapeListIndex.append(indexY)
                shapeListUpper.append(indices[0])
                shapeListLower.append(indices[-1])
    else:
        for indexX in range(nx):
            column = filteredImage[:, indexX]
            indices = numpy.where(column)[0]
            if len(indices) > 0:
                shapeListIndex.append(indexX)
                shapeListUpper.append(indices[0])
                shapeListLower.append(indices[-1])
    ny, nx = filteredImage.shape
    arrayIndex = numpy.array(shapeListIndex)
    if isVerticalAxis:
        arrayUpper = numpy.array(shapeListLower)
        arrayLower = numpy.array(shapeListUpper)
    else:
        arrayUpper = ny - numpy.array(shapeListUpper)
        arrayLower = ny - numpy.array(shapeListLower)
    if debug:
        pylab.plot(arrayIndex, arrayUpper, "+")
        pylab.plot(arrayIndex, arrayLower, "+")
        pylab.axes(extent)
        pylab.savefig(os.path.join(autoMeshWorkingDir, "shapePlot_%03d.png" % omega))
        pyplot.close()
    # return (arrayIndex.tolist(), arrayUpper.tolist(), arrayLower.tolist())
    dictLoop["%d" % omega] = (
        arrayIndex.tolist(),
        arrayUpper.tolist(),
        arrayLower.tolist(),
    )


def findOptimalMesh(
    dictLoop,
    snapshotDir,
    nx,
    ny,
    autoMeshWorkingDir,
    snapShotAngles=[0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330],
    loopMaxWidth=300,
    loopMinWidth=150,
    debug=False,
    findLargestMesh=False,
    isVerticalAxis=False,
):
    loopMinWidth = numpy.int64(loopMinWidth)
    loopMaxWidth = numpy.int64(loopMaxWidth)
    logger = workflow_logging.getLogger()
    arrayPhiz = None
    minThickness = None
    angleMinThickness = None
    maxThickness = None
    angleMaxThickness = None
    meshMax = None
    stdPhiz = None
    for omega in snapShotAngles:
        strOmega1 = "%d" % omega
        # strOmega2 = "%03d" % (omega + 180)
        (listIndex1, listUpper1, listLower1) = dictLoop[strOmega1]
        # (listIndex2, listUpper2, listLower2) = dictLoop[strOmega2]
        arrayIndex1 = numpy.array(listIndex1)
        # arrayIndex2 = numpy.array(listIndex2)
        # if len(arrayIndex1) == 0 or len(arrayIndex2) == 0:
        if len(arrayIndex1) == 0:
            break
        if meshMax is None:
            meshMax = numpy.max(arrayIndex1)
        elif meshMax > numpy.max(arrayIndex1):
            meshMax = numpy.max(arrayIndex1)
        # if meshMax > numpy.max(arrayIndex2):
        #     meshMax = numpy.max(arrayIndex2)
    if meshMax is None:
        meshMax = loopMaxWidth
    meshMin = meshMax - loopMaxWidth
    print(meshMin, meshMax)
    nPhi = 0
    # for omega in [0, 30, 60, 90, 120, 150]:
    for omega in snapShotAngles:
        strOmega1 = "%d" % omega
        # strOmega2 = "%03d" % (omega + 180)
        (arrayIndex1, arrayUpper1, arrayLower1) = dictLoop[strOmega1]
        # (arrayIndex2, arrayUpper2, arrayLower2) = dictLoop[strOmega2]
        arrayIndex = numpy.arange(meshMin, meshMax - 1)
        # Exclude regio in horizontal center
        meshMin = numpy.int64(meshMin)
        meshMax = numpy.int64(meshMax)
        xExcludMin = numpy.int64(nx / 2 - 20)
        xExcludMax = numpy.int64(nx / 2 + 20)
        indices1 = numpy.where(
            ((arrayIndex1 > meshMin) & (arrayIndex1 < xExcludMin))
            | ((arrayIndex1 < meshMax) & (arrayIndex1 > xExcludMax))
        )
        # indices2 = numpy.where(
        #     ((arrayIndex2 > meshMin) & (arrayIndex2 < xExcludMin))
        #     | ((arrayIndex2 < meshMax) & (arrayIndex2 > xExcludMax))
        # )
        arrayUpper1 = numpy.array(arrayUpper1)[indices1]
        # arrayUpper2 = numpy.array(arrayUpper2)[indices2]
        arrayLower1 = numpy.array(arrayLower1)[indices1]
        # arrayLower2 = numpy.array(arrayLower2)[indices2]
        if True:
            pylab.plot(arrayUpper1, "+", color="red")
            # pylab.plot(arrayUpper2, "+", color="blue")
            pylab.plot(arrayLower1, "+", color="red")
            # pylab.plot(arrayLower2, "+", color="blue")
            pylab.savefig(
                os.path.join(
                    autoMeshWorkingDir, "shapePlot_%03d_%03d.png" % (omega, omega + 180)
                )
            )
            pylab.close()
        # phiz = None
        # if (arrayUpper1.shape == arrayLower2.shape) and (
        #     arrayUpper2.shape == arrayLower1.shape
        # ):
        #     phiz1 = (arrayUpper1 + arrayLower2) / 2.0
        #     phiz2 = (arrayUpper2 + arrayLower1) / 2.0
        #     phiz = (phiz1 + phiz2) / 2.0
        # elif arrayUpper1.shape == arrayLower2.shape:
        #     phiz = (arrayUpper1 + arrayLower2) / 2.0
        # elif arrayUpper2.shape == arrayLower1.shape:
        #     phiz = (arrayUpper2 + arrayLower1) / 2.0
        # if phiz is not None:
        #     if debug:
        #         pylab.plot(phiz, "+")
        #         pylab.savefig(
        #             os.path.join(
        #                 autoMeshWorkingDir, "phiz_%03d_%03d.png" % (omega, omega + 180)
        #             )
        #         )
        #         pylab.close()
        #     if arrayPhiz is None:
        #         arrayPhiz = numpy.array(phiz)
        #         nPhi += 1
        #     else:
        #         if arrayPhiz.shape == phiz.shape:
        #             arrayPhiz += phiz
        #             nPhi += 1
    if nPhi > 0:
        arrayPhiz /= nPhi
        # Cut off 20 points from each side of array in order to remove artifacts at end points
        arrayPhiz = arrayPhiz[20:-20]
        _ = arrayIndex[20:-20]
        if True:
            pylab.plot(arrayPhiz, "+")
            strPhizPath = os.path.join(autoMeshWorkingDir, "phiz.png")
            pylab.savefig(strPhizPath)
            pylab.close()
        averagePhiz = numpy.mean(arrayPhiz)
        stdPhiz = numpy.std(arrayPhiz)
        if isVerticalAxis:
            deltaPhiz = nx / 2 - averagePhiz
        else:
            deltaPhiz = ny / 2 - averagePhiz
    else:
        averagePhiz = None
        deltaPhiz = None
    # Sample thickness
    listThicknessIndex = []
    # for omega in [0, 30, 60, 90, 120, 150, 180, 210, 270, 300, 330]:
    for omega in snapShotAngles:
        strOmega = "%d" % omega
        (arrayIndex, arrayUpper, arrayLower) = dictLoop[strOmega]
        indices = numpy.where((arrayIndex > meshMin) & (arrayIndex < meshMax))
        arrayUpper = numpy.array(arrayUpper)[indices]
        arrayLower = numpy.array(arrayLower)[indices]
        arrayThickness = arrayUpper - arrayLower
        # Look for a minima between 100 and 300 pixels from the right:
        print(arrayThickness.shape)
        print(arrayThickness)
        print([-loopMaxWidth, -loopMinWidth])
        print([type(loopMaxWidth), type(loopMinWidth)])
        arrayThicknessCrop = arrayThickness[-loopMaxWidth:-loopMinWidth]
        indicesThicknessCrop = indices[0][-loopMaxWidth:-loopMinWidth]
        # Remove "thick" sample holder, i.e where thickness > 75
        index_sample_holder = 0
        min_thickness = arrayThicknessCrop[0]

        for index in range(len(arrayThicknessCrop)):
            if arrayThicknessCrop[index] < 100:
                if arrayThicknessCrop[index] < min_thickness:
                    min_thickness = arrayThicknessCrop[index]
                    index_sample_holder = index
                elif arrayThicknessCrop[index] > min_thickness + 5:
                    index_sample_holder = index
                    break
        arrayThicknessCrop = arrayThicknessCrop[index_sample_holder:]
        indicesThicknessCrop = indicesThicknessCrop[index_sample_holder:]
        loopMaxWidth -= index_sample_holder
        if loopMaxWidth < loopMinWidth:
            loopMaxWidth = loopMinWidth
        if debug:
            pylab.plot(arrayUpper, "-")
            pylab.plot(arrayLower, "*")
            pylab.plot(arrayThicknessCrop, "+")
            strPhizPath = os.path.join(
                autoMeshWorkingDir, "thickness_%s.png" % strOmega
            )
            pylab.savefig(strPhizPath)
            pylab.close()
        if len(arrayThicknessCrop) > 0:
            tmpMin = numpy.argmin(arrayThicknessCrop)
            #            if tmpMin > 75:
            #                tmpMin = 75
            indexMin = indicesThicknessCrop[tmpMin]
            # Minimum mesh length: 150 pixels
            listThicknessIndex.append(indexMin)
            logger.debug("Index min: %d for omega = %d" % (indexMin, omega))
    logger.debug("List of thicknesses: %r" % listThicknessIndex)
    if not findLargestMesh and len(listThicknessIndex) > 0:
        maxIndexThickness = max(listThicknessIndex)
        meshMin = maxIndexThickness
        logger.debug("Max index for thickess: %d" % maxIndexThickness)
    else:
        meshMin = meshMax - loopMaxWidth
        if meshMin < 0:
            meshMin = 0
    logger.debug("meshMin: %d" % meshMin)
    minThicknessMeshMin = None
    minThicknessMeshMax = None
    maxThicknessMeshMin = None
    maxThicknessMeshMax = None
    # for omega in [0, 30, 60, 90, 120, 150, 180, 210, 270, 300, 330]:
    for omega in snapShotAngles:
        strOmega = "%d" % omega
        (arrayIndex, arrayUpper, arrayLower) = dictLoop[strOmega]
        indices = numpy.where((arrayIndex > meshMin) & (arrayIndex < meshMax))
        arrayUpper = numpy.array(arrayUpper)[indices]
        arrayLower = numpy.array(arrayLower)[indices]
        if len(arrayLower) == 0 or len(arrayUpper) == 0:
            break
        maxThick = numpy.max(arrayUpper) - numpy.min(arrayLower)
        logger.debug(f"maxThick: {maxThick}")
        arrayIndex = numpy.arange(meshMin, meshMax - 1)
        if minThickness is None or minThickness > maxThick:
            minThickness = maxThick
            angleMinThickness = omega
            minThicknessMeshMin = numpy.min(arrayLower)
            minThicknessMeshMax = numpy.max(arrayUpper)
        #             if True:
        #                 pylab.plot(arrayUpper, '+')
        #                 pylab.plot(arrayLower, '+')
        #                 pylab.show()
        if maxThickness is None or maxThickness < maxThick:
            maxThickness = maxThick
            angleMaxThickness = omega
            maxThicknessMeshMin = numpy.min(arrayLower)
            maxThicknessMeshMax = numpy.max(arrayUpper)
    meshMin -= 50
    logger.debug(
        "Max thickness = %r pxiels at omega %r" % (maxThickness, angleMaxThickness)
    )
    logger.debug(
        "Mesh: meshMin=%r, meshMax=%r, maxThicknessMeshMin=%r, maxThicknessMeshMax = %r"
        % (meshMin, meshMax, maxThicknessMeshMin, maxThicknessMeshMax)
    )
    logger.debug(
        "Min thickness = %r pixels at omega %r" % (minThickness, angleMinThickness)
    )
    logger.debug(
        "Mesh: meshMin=%r, meshMax=%r, minThicknessMeshMin=%r, minThicknessMeshMax = %r"
        % (meshMin, meshMax, minThicknessMeshMin, minThicknessMeshMax)
    )
    if meshMin < 10:
        meshMin = 10

    if isVerticalAxis:
        x1Pixels = None
        dxPixels = None
        y1Pixels = meshMin - ny / 2
        dyPixels = meshMax - meshMin
    else:
        x1Pixels = meshMin - nx / 2
        dxPixels = meshMax - meshMin
        y1Pixels = None
        dyPixels = None
    angle = None
    if (
        (minThicknessMeshMin is not None)
        and (minThicknessMeshMax is not None)
        and (maxThicknessMeshMin is not None)
        and (maxThicknessMeshMax is not None)
    ):
        if findLargestMesh:
            if isVerticalAxis:
                x1Pixels = maxThicknessMeshMin - nx / 2
                dxPixels = maxThicknessMeshMax - maxThicknessMeshMin
            else:
                y1Pixels = maxThicknessMeshMin - ny / 2
                dyPixels = maxThicknessMeshMax - maxThicknessMeshMin
            angle = angleMaxThickness
        else:
            if isVerticalAxis:
                x1Pixels = minThicknessMeshMin - nx / 2
                dxPixels = minThicknessMeshMax - minThicknessMeshMin
            else:
                y1Pixels = minThicknessMeshMin - ny / 2
                dyPixels = minThicknessMeshMax - minThicknessMeshMin
            angle = angleMinThickness
    logger.debug(
        "angle={0}, x1Pixels={1}, y1Pixels={2}, dxPixels={3}, dyPixels={4}, deltaPhiz={5}, stdPhiz={6}".format(
            angle, x1Pixels, y1Pixels, dxPixels, dyPixels, deltaPhiz, stdPhiz
        )
    )
    return (angle, x1Pixels, y1Pixels, dxPixels, dyPixels, deltaPhiz, stdPhiz)


def findCentrePin(
    dictLoop,
    snapshotDir,
    nx,
    ny,
    autoMeshWorkingDir,
    loopWidth=100,
    imageSize=None,
    doCircleFit=False,
    debug=False,
    isVerticalAxis=False,
):
    dictVertical = {}
    listHorizontal = []
    for omega in [0, 90, 180, 270]:
        print("*" * 80)
        strOmega1 = "%d" % omega
        print(strOmega1)
        (listIndex1, listUpper1, listLower1) = dictLoop[strOmega1]
        arrayIndex1 = numpy.array(listIndex1)
        indexMax = len(arrayIndex1)
        if indexMax == 0:
            raise RuntimeError("No pin found for omega {0}!".format(strOmega1))
        # loopArrayIndex = numpy.arange(meshMax-30, meshMax - 1)
        indexMin = indexMax - int(loopWidth / 3)
        # indices1 = numpy.where(((arrayIndex1 > meshMin) & (arrayIndex1 < meshMax)))
        # print(indices1)
        indexLoop = listIndex1[indexMin : indexMax - 1]
        arrayUpper1 = numpy.array(listUpper1[indexMin : indexMax - 1])
        arrayLower1 = numpy.array(listLower1[indexMin : indexMax - 1])
        meanArray = (arrayLower1 + arrayUpper1) / 2
        pylab.plot(indexLoop, arrayUpper1, "+", color="blue")
        pylab.plot(indexLoop, arrayLower1, "+", color="blue")

        if doCircleFit:
            data = []
            for index, x in enumerate(indexLoop):
                y1 = arrayUpper1[index]
                y2 = arrayLower1[index]
                data.append([x, y1])
                data.append([x, y2])
            circleX = []
            circleY = []
            xc, yc, r, _ = circle_fit.least_squares_circle(data)
            for theta in range(0, 360, 5):
                x = xc + r * math.cos(theta)
                y = yc + r * math.sin(theta)
                circleX.append(x)
                circleY.append(y)
            pylab.plot(circleX, circleY, ".", color="red")
            print(xc, yc, r)
        else:
            pylab.plot(indexLoop, meanArray, "+", color="green")
            xc = listIndex1[-1] - int(loopWidth / 2)
            yc = numpy.mean(meanArray)
            print(xc, yc)

        pylab.plot([xc], [yc], "+", color="black")
        pylab.gca().set_aspect("equal")
        pylab.savefig(
            os.path.join(autoMeshWorkingDir, "shapePlot_mean_%03d.png" % (omega))
        )
        pylab.close()
        # Calculate deltaZ (phiy)
        deltaZ = int(imageSize[1] / 2) - xc
        dictVertical[strOmega1] = yc
        listHorizontal.append(deltaZ)

    meanDeltaX = (dictVertical["90"] - dictVertical["270"]) / 2
    meanDeltaY = (dictVertical["180"] - dictVertical["0"]) / 2
    meanDeltaZ = numpy.mean(listHorizontal)
    return meanDeltaX, meanDeltaY, meanDeltaZ


def autoMesh(
    snapshotDir,
    workflowWorkingDir,
    autoMeshWorkingDir,
    loopMaxWidth=300,
    loopMinWidth=150,
    snapShotAngles=[0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330],
    prefix="snapshot",
    debug=False,
    findLargestMesh=False,
    isVerticalAxis=False,
    suffix="png",
):
    logger = workflow_logging.getLogger()
    os.chmod(autoMeshWorkingDir, 0o755)
    background_image = os.path.join(
        snapshotDir, "{0}_background.{1}".format(prefix, suffix)
    )
    dictLoop = {}
    list_thread = []
    for omega in snapShotAngles:
        logger.info("Analysing snapshot image at omega = {0} degrees".format(omega))
        imagePath = os.path.join(
            snapshotDir, "{0}_{1:03d}.{2}".format(prefix, omega, suffix)
        )
        thread = threading.Thread(
            target=loopExam,
            args=(
                imagePath,
                background_image,
                omega,
                debug,
                autoMeshWorkingDir,
                isVerticalAxis,
                dictLoop,
            ),
        )
        list_thread.append(thread)
        thread.start()
        if debug:
            thread.join()
    for thread in list_thread:
        thread.join()
    areTheSameImage = checkForCorrelatedImages(dictLoop)
    image000Path = os.path.join(
        snapshotDir, "{0}_{1:03d}.{2}".format(prefix, 0, suffix)
    )
    image_shape = imageio.v3.imread(image000Path).shape
    ny = image_shape[0]
    nx = image_shape[1]
    (
        angleMinThickness,
        x1Pixels,
        y1Pixels,
        dxPixels,
        dyPixels,
        deltaPhiz,
        stdPhiz,
    ) = findOptimalMesh(
        dictLoop,
        snapshotDir,
        nx,
        ny,
        autoMeshWorkingDir,
        debug=debug,
        snapShotAngles=snapShotAngles,
        loopMaxWidth=loopMaxWidth,
        loopMinWidth=loopMinWidth,
        findLargestMesh=findLargestMesh,
        isVerticalAxis=isVerticalAxis,
    )
    imagePath = None
    if angleMinThickness is not None:
        imagePath = os.path.join(
            snapshotDir, "{0}_{1:03d}.{2}".format(prefix, angleMinThickness, suffix)
        )
    return (
        angleMinThickness,
        x1Pixels,
        y1Pixels,
        dxPixels,
        dyPixels,
        deltaPhiz,
        stdPhiz,
        imagePath,
        areTheSameImage,
    )


def centrePin(
    snapshotDir,
    autoMeshWorkingDir,
    prefix="snapshot",
    loopWidth=100,
    doCircleFit=False,
    debug=False,
    isVerticalAxis=False,
):
    logger = workflow_logging.getLogger()
    os.chmod(autoMeshWorkingDir, 0o755)
    background_image = os.path.join(snapshotDir, "%s_background.png" % prefix)
    # Get size of image
    background = imageio.v3.imread(background_image)
    if len(background.shape) > 2:
        background = rgb2gray(background)
    imageSize = background.shape
    dictLoop = {}
    list_thread = []
    for omega in [0, 90, 180, 270]:
        logger.info("Analysing snapshot image at omega = %d degrees" % omega)
        imagePath = os.path.join(snapshotDir, "%s_%03d.png" % (prefix, omega))
        thread = threading.Thread(
            target=loopExam,
            args=(
                imagePath,
                background_image,
                omega,
                debug,
                autoMeshWorkingDir,
                isVerticalAxis,
                dictLoop,
            ),
        )
        list_thread.append(thread)
        thread.start()
    for thread in list_thread:
        thread.join()
        # (listIndex, listUpper, listLower) = loopExam(
        #     imagePath,
        #     background_image,
        #     omega,
        #     autoMeshWorkingDir=autoMeshWorkingDir,
        #     isVerticalAxis=isVerticalAxis,
        #     debug=debug,
        # )
        # dictLoop["%d" % omega] = (listIndex, listUpper, listLower)
    _ = checkForCorrelatedImages(dictLoop)
    image000Path = os.path.join(snapshotDir, "%s_%03d.png" % (prefix, 0))
    image_shape = imageio.v3.imread(image000Path).shape
    ny = image_shape[0]
    nx = image_shape[1]
    deltaX, deltaY, deltaZ = findCentrePin(
        dictLoop,
        snapshotDir,
        nx,
        ny,
        autoMeshWorkingDir,
        doCircleFit=doCircleFit,
        debug=debug,
        isVerticalAxis=isVerticalAxis,
        imageSize=imageSize,
        loopWidth=loopWidth,
    )
    return deltaX, deltaY, deltaZ


def determineMeshGaps(grid_info):
    gap_x = None
    gap_y = None
    if "steps_x" in grid_info:
        steps_x = grid_info["steps_x"]
        if "beam_width" in grid_info and steps_x > 1:
            dx_mm = grid_info["dx_mm"]
            beam_width = grid_info["beam_width"]
            gap_x = dx_mm / (steps_x - 1) - beam_width
    if "steps_y" in grid_info:
        steps_y = grid_info["steps_y"]
        if "beam_height" in grid_info and steps_y > 1:
            dy_mm = grid_info["dy_mm"]
            beam_height = grid_info["beam_height"]
            gap_y = dy_mm / (steps_y - 1) - beam_height
    return gap_x, gap_y


def saveMeshResults(**meshResults):
    logger = workflow_logging.getLogger()
    # First create corresponding PROCESSED_DATA path
    raw_directory = meshResults["raw_directory"]
    if "RAW_DATA" not in raw_directory:
        raise BaseException(
            "grid.saveMeshResults: raw data path doesn't contain 'RAW_DATA': {0}".format(
                raw_directory
            )
        )
    processedDir = raw_directory.replace("RAW_DATA", "PROCESSED_DATA")
    resultMeshDir = os.path.join(processedDir, "MeshResults")
    if not os.path.exists(resultMeshDir):
        os.makedirs(resultMeshDir, 0o755)
    # Then create file with date stamp
    resultMeshFileName = "MeshResults_{0}.json".format(
        time.strftime("%Y%m%d-%H%M%S", time.localtime(time.time()))
    )
    resultMeshPath = os.path.join(resultMeshDir, resultMeshFileName)
    # Unlikely, but the file might exist!
    if os.path.exists(resultMeshPath):
        time.sleep(1)
        resultMeshFileName = "MeshResults_{0}.json".format(
            time.strftime("%Y%m%d-%H%M%S", time.localtime(time.time()))
        )
        resultMeshPath = os.path.join(resultMeshDir, resultMeshFileName)
    logger.info("Mesh results saved to file {0}".format(resultMeshPath))
    # Create file
    jsonFile = open(resultMeshPath, "w")
    jsonFile.write(json.dumps(meshResults, indent=4))
    jsonFile.close()
    os.chmod(resultMeshPath, 0o644)
    return resultMeshPath


def findMeshResults(raw_directory):
    processed_dir = pathlib.Path(raw_directory.replace("RAW_DATA", "PROCESSED_DATA"))
    if processed_dir.name.startswith("run"):
        # Find all "MeshResults"
        list_result_path = list(
            map(str, processed_dir.parent.glob("*/MeshResults/*.json"))
        )
    else:
        result_mesh_dir = processed_dir / "MeshResults"
        list_result_path = list(map(str, result_mesh_dir.glob("*.json")))
    return list_result_path


def readMeshResults(raw_directory, resultMeshFileName):
    meshResults = None
    processedDir = raw_directory.replace("RAW_DATA", "PROCESSED_DATA")
    resultMeshDir = os.path.join(processedDir, "MeshResults")
    resultMeshPath = os.path.join(resultMeshDir, resultMeshFileName)
    if os.path.exists(resultMeshPath):
        jsonFile = open(resultMeshPath)
        meshResults = json.loads(jsonFile.read())
        jsonFile.close()
    return meshResults


def cmp(a, b):
    return (a > b) - (a < b)


def checkForCorrelatedImages(dictLoop):
    # Check if all the indices are the same
    firstListIndex = None
    firstUpperIndex = None
    firstLowerIndex = None
    areTheSame = True
    for loopIndex in dictLoop:
        listIndex, listUpper, listLower = dictLoop[loopIndex]
        if firstListIndex is None:
            firstListIndex = listIndex
            firstUpperIndex = listUpper
            firstLowerIndex = listLower
        elif cmp(firstListIndex, listIndex):
            areTheSame = False
            break
        elif cmp(firstUpperIndex, listUpper):
            areTheSame = False
            break
        elif cmp(firstLowerIndex, listLower):
            areTheSame = False
            break
    return areTheSame


def deletePositions(listPositions, dozorScoreThreshold):
    returnList = []
    for position in listPositions:
        dozorScore = None
        if "dozor_score" in position:
            dozorScore = position["dozor_score"]
        elif "dozor_score" in position["nearest_image"]:
            dozorScore = position["nearest_image"]["dozor_score"]
        else:
            raise BaseException("Cannot find dozor score for position!!!")
        if dozorScore >= dozorScoreThreshold:
            returnList.append(position)
    return returnList


def extendGridToMultipleOf100(grid_info):
    logger = workflow_logging.getLogger()
    new_grid_info = dict(grid_info)
    if grid_info != {}:
        logger.debug("extendGridToMultipleOf100, old gridInfo: {0}".format(grid_info))
        steps_x = grid_info["steps_x"]
        steps_y = grid_info["steps_y"]
        if steps_x * steps_y > 100:
            dx_mm = grid_info["dx_mm"]
            dy_mm = grid_info["dy_mm"]
            listNewGrids = []
            if steps_x == 1 and (steps_y % 100):
                # Vertical line scan
                new_steps_x = 1
                new_steps_y = int(steps_y / 100 + 1) * 100
            elif steps_y == 1:
                # Horizontal line scan
                new_steps_x = int(steps_x / 100 + 1) * 100
                new_steps_y = 1
            else:
                for y in range(steps_y, steps_y + 10):
                    for x in range(steps_x, steps_x + 10):
                        noGridPoints = x * y
                        isMultipleOf100 = (noGridPoints % 100) == 0
                        if isMultipleOf100:
                            listNewGrids.append([noGridPoints, x, y])
                sortedList = sorted(listNewGrids, key=operator.itemgetter(0))
                new_steps_x = sortedList[0][1]
                new_steps_y = sortedList[0][2]
            new_dx_mm = dx_mm * new_steps_x / steps_x
            new_dy_mm = dy_mm * new_steps_y / steps_y
            logger.debug(
                "extendGridToMultipleOf100, old grid: {0}, {1}; new grid: {2},{3}".format(
                    steps_x, steps_y, new_steps_x, new_steps_y
                )
            )
            new_grid_info["steps_x"] = new_steps_x
            new_grid_info["steps_y"] = new_steps_y
            new_grid_info["dx_mm"] = new_dx_mm
            new_grid_info["dy_mm"] = new_dy_mm
        logger.debug(
            "extendGridToMultipleOf100, new gridInfo: {0}".format(new_grid_info)
        )
    return new_grid_info


def find_best_position_dozorm(
    listPositions,
    grid_info,
    meshPositions,
    isHorizontalRotationAxis=False,
    workflowWorkingDir=None,
    correct_dozorm2=False,
):
    allPositions = find_all_position_dozorm(
        listPositions=listPositions,
        grid_info=grid_info,
        meshPositions=meshPositions,
        maxNoPositions=1,
        isHorizontalRotationAxis=isHorizontalRotationAxis,
        workflowWorkingDir=workflowWorkingDir,
        correct_dozorm2=correct_dozorm2,
    )
    if len(allPositions) > 0:
        bestPosition = allPositions[0]
    else:
        bestPosition = None
    return bestPosition


def find_all_position_dozorm(
    listPositions,
    grid_info,
    meshPositions,
    maxNoPositions=100,
    isHorizontalRotationAxis=False,
    workflowWorkingDir=None,
    correct_dozorm2=False,
):
    logger = workflow_logging.getLogger()
    logger.debug(f"find_all_position_dozorm correct_dozorm2: {correct_dozorm2}")
    allPositions = []
    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    dx_mm = grid_info["dx_mm"]
    dy_mm = grid_info["dy_mm"]
    maxIndex = min(len(listPositions), maxNoPositions)
    for dozormPosition in listPositions[0:maxIndex]:
        bestPositionY = float(dozormPosition["xPosition"]) - 1
        bestPositionZ = float(dozormPosition["yPosition"]) - 1
        # See https://stackoverflow.com/questions/31818050/python-2-7-round-number-to-nearest-integer
        indexMaxY = int(round(bestPositionY - 10 ** (-len(str(bestPositionY)) - 1)))
        indexMaxZ = int(round(bestPositionZ - 10 ** (-len(str(bestPositionZ)) - 1)))
        index_fraction_x = bestPositionY - indexMaxY
        index_fraction_y = bestPositionZ - indexMaxZ
        bestPosition = _find_strongest_position(
            meshPositions,
            index_x=indexMaxY,
            index_y=indexMaxZ,
            index_fraction_x=index_fraction_x,
            index_fraction_y=index_fraction_y,
            com_index_x=bestPositionY,
            com_index_y=bestPositionZ,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
            grid_info=grid_info,
            correct_dozorm2=correct_dozorm2,
        )
        #
        bestPosition["crystal_size_x"] = (
            dozormPosition["numberOfImagesTotalX"] * dx_mm / nx
        )
        bestPosition["crystal_size_y"] = (
            dozormPosition["numberOfImagesTotalY"] * dy_mm / ny
        )
        bestPosition["FWHM"] = None
        bestPosition["integralDozorScore"] = None
        bestPosition["longAxis"] = None
        bestPosition["shortAxis"] = None
        bestPosition["averageDozorScore"] = None
        # Target aperture only set for 2D scans
        if nx != 1 and ny != 1:
            bestPosition["dozormTargetApertureSize"] = dozormPosition["apertureSize"]
        else:
            bestPosition["dozormTargetApertureSize"] = None
        # if meshBestType == "linescan":
        #     meshBestPosition["FWHM"] = round(float(meshClusterResult[2]), 3)
        #     meshBestPosition["integralDozorScore"] = round(float(meshClusterResult[3]), 3)
        # else:
        #     meshBestPosition["longAxis"] = round(float(meshClusterResult[2]) * float(dx_mm / (nx - 1)), 3)
        #     meshBestPosition["shortAxis"] = round(float(meshClusterResult[3]) * float(dy_mm / (ny - 1)), 3)
        #     meshBestPosition["averageDozorScore"] = round(float(meshClusterResult[4]), 3)
        #     meshBestPosition["integralDozorScore"] = round(float(meshClusterResult[5]), 3)
        allPositions.append(bestPosition)
    return allPositions


def getLineScanGridInfo(
    beamX, beamY, length, overSampling, isOrthogonal=True, isHorizontalRotationAxis=True
):
    logger = workflow_logging.getLogger()
    if (not isHorizontalRotationAxis and isOrthogonal) or (
        isHorizontalRotationAxis and not isOrthogonal
    ):
        # Horizontal scan
        noStepsX = int(length / beamX * overSampling + 0.5)
        if noStepsX < 10:
            # Keep the length but increase the oversampling
            noStepsX = 10
        noStepsY = 1
        deltaX = length
        deltaY = 0.0
    else:
        # Vertical scan
        noStepsX = 1
        noStepsY = int(length / beamY * overSampling + 0.5)
        if noStepsY < 10:
            # Keep the length but increase the oversampling
            noStepsY = 10
        deltaX = 0.0
        deltaY = length
    gridInfo = {
        "x1": -deltaX / 2.0,
        "y1": -deltaY / 2.0,
        "dx_mm": deltaX,
        "dy_mm": deltaY,
        "steps_x": noStepsX,
        "steps_y": noStepsY,
    }
    logger.debug("Line scan grid info: {0}".format(gridInfo))
    return gridInfo


def getSmallXrayCentringGrid(
    beamSizeAtSampleX,
    beamSizeAtSampleY,
    do1D=False,
    isHorizontalRotationAxis=True,
    lineScanStep=0,
    lineScanLength=None,
):
    # If lineScanStep is zero it's determined automatically
    if do1D and lineScanStep > 0 and lineScanLength is not None:
        if isHorizontalRotationAxis:
            steps_y = int(lineScanLength / lineScanStep)
            if steps_y < 10:
                steps_y = 10
            grid_info = {
                "x1": 0,
                "y1": -lineScanLength / 2,
                "steps_x": 1,
                "steps_y": steps_y,
                "dx_mm": lineScanLength,
                "dy_mm": lineScanLength,
            }
        else:
            steps_x = int(lineScanLength / lineScanStep)
            if steps_x < 10:
                steps_x = 10
            grid_info = {
                "x1": -lineScanLength / 2,
                "y1": 0,
                "steps_x": steps_x,
                "steps_y": 1,
                "dx_mm": lineScanLength,
                "dy_mm": lineScanLength,
            }
    else:
        overSampling = 1
        if beamSizeAtSampleX > 0.045 and beamSizeAtSampleY > 0.045:
            dx_mm = 0.50
            dy_mm = 0.50
            overSampling = 2
        elif beamSizeAtSampleX > 0.025 and beamSizeAtSampleY > 0.025:
            dx_mm = 0.40
            dy_mm = 0.40
            overSampling = 2
        elif beamSizeAtSampleX > 0.015 and beamSizeAtSampleY > 0.015:
            dx_mm = 0.30
            dy_mm = 0.30
        elif beamSizeAtSampleX > 0.010 and beamSizeAtSampleY > 0.010:
            dx_mm = 0.15
            dy_mm = 0.15
        else:
            dx_mm = 0.10
            dy_mm = 0.10
        if do1D:
            if isHorizontalRotationAxis:
                steps_y = int(dy_mm / beamSizeAtSampleY * overSampling)
                if steps_y < 10:
                    steps_y = 10
                grid_info = {
                    "x1": 0,
                    "y1": -dy_mm,
                    "steps_x": 1,
                    "steps_y": steps_y * 2,
                    "dx_mm": dx_mm * 2,
                    "dy_mm": dy_mm * 2,
                }
            else:
                steps_x = int(dx_mm / beamSizeAtSampleX * overSampling)
                if steps_x < 10:
                    steps_x = 10
                grid_info = {
                    "x1": -dx_mm,
                    "y1": 0,
                    "steps_x": steps_x * 2,
                    "steps_y": 1,
                    "dx_mm": dx_mm * 2,
                    "dy_mm": dy_mm * 2,
                }
        else:
            steps_x = int(dx_mm / beamSizeAtSampleX * overSampling)
            steps_y = int(dy_mm / beamSizeAtSampleY * overSampling)
            grid_info = {
                "x1": -dx_mm / 2,
                "y1": -dy_mm / 2,
                "steps_x": steps_x,
                "steps_y": steps_y,
                "dx_mm": dx_mm,
                "dy_mm": dy_mm,
            }
    return grid_info


def newSizeStepsWithGaps(grid_size, gap_size, no_steps):
    beam_size = grid_size / no_steps
    new_no_steps = int(round((grid_size - beam_size) / (beam_size + gap_size), 0) + 1)
    new_grid_size = (new_no_steps - 1) * (beam_size + gap_size) + beam_size
    return round(new_grid_size, 4), new_no_steps


def newGridInfoWithGaps(grid_info, gap_horizontal, gap_vertical):
    dx_mm = grid_info["dx_mm"]
    dy_mm = grid_info["dy_mm"]
    steps_x = grid_info["steps_x"]
    steps_y = grid_info["steps_y"]
    new_dx_mm, new_steps_x = newSizeStepsWithGaps(dx_mm, gap_horizontal, steps_x)
    new_dy_mm, new_steps_y = newSizeStepsWithGaps(dy_mm, gap_vertical, steps_y)
    new_grid_info = dict(grid_info)
    new_grid_info["dx_mm"] = new_dx_mm
    new_grid_info["dy_mm"] = new_dy_mm
    new_grid_info["steps_x"] = new_steps_x
    new_grid_info["steps_y"] = new_steps_y
    return new_grid_info


def crop_image(image_path):
    #
    # Crop image
    #
    os.system(f"convert -trim -border 5 -bordercolor white {image_path} {image_path}")


def getDirections(meshPositions, grid_info, index_x, index_y):
    logger = workflow_logging.getLogger()
    dir_x = 0
    dir_y = 0
    found_edge = False
    for position in meshPositions:
        indexY = position["indexY"]
        indexZ = position["indexZ"]
        # Check all nine directions
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                if indexY == index_x + dx and indexZ == index_y + dy:
                    dozor_score = position["dozor_score"]
                    logger.debug(f"({dx},{dy}) : {dozor_score}")
                    if dozor_score > 0.1:
                        dir_x += dx * dozor_score
                        dir_y += dy * dozor_score
                    else:
                        found_edge = True
                    break
    if found_edge and max([dir_x, dir_y]) > 0:
        norm = math.sqrt(dir_x**2 + dir_y**2)
        dir_x = dir_x / norm
        dir_y = dir_y / norm
    else:
        dir_x = None
        dir_y = None
    return dir_x, dir_y
