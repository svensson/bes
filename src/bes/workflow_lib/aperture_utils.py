"""Apertures in MXcube have a name (string) and a size in micron (integer).
Names are unique but sizes might not be.

Aperture names could be "A20", "20", "Outbeam", ...

The aperture size could be

- the edge of a square aperture
- the diameter of a circular aperture
"""

from typing import Dict
from bes import config


def aperture_size_to_name(beamline: str, aperture_size: int) -> str:
    """
    :raises TypeError: wrong :code:`aperture_size` type
    :raises ValueError: unknown :code:`aperture_size` value
    """
    if not isinstance(aperture_size, int):
        raise TypeError(
            f"Aperture size {aperture_size!r} is of type '{type(aperture_size)}' instead of 'int'"
        )
    size_to_name = _aperture_size_to_name(beamline)
    if aperture_size not in size_to_name:
        raise ValueError(
            f"Aperture size {aperture_size} not in list of apertures sizes {list(size_to_name)}!"
        )
    return size_to_name[aperture_size]


def aperture_name_to_size(beamline: str, aperture_name: str) -> int:
    """
    :raises TypeError: wrong :code:`aperture_name` type
    :raises ValueError: unknown :code:`aperture_name` value
    """
    if not isinstance(aperture_name, str):
        raise TypeError(
            f"Aperture name {aperture_name!r} is of type '{type(aperture_name)}' instead of 'int'"
        )
    name_to_size = _aperture_name_to_size(beamline)
    if aperture_name not in name_to_size:
        raise ValueError(
            f"Aperture name {aperture_name} not in list of apertures names {list(name_to_size)}!"
        )
    return name_to_size[aperture_name]


def aperture_name_to_correction_factor(beamline: str, aperture_name: float) -> float:
    """
    :raises TypeError: wrong :code:`aperture_name` type
    :raises ValueError: unknown :code:`aperture_name` value
    """
    if not isinstance(aperture_name, str):
        raise TypeError(
            f"Aperture name {aperture_name!r} is of type '{type(aperture_name)}' instead of 'int'"
        )
    name_to_correction_factor = _aperture_name_to_correction_factor(beamline)
    if aperture_name not in name_to_correction_factor:
        raise ValueError(
            f"Aperture size {aperture_name} not in list of apertures sizes {list(name_to_correction_factor)}!"
        )
    return name_to_correction_factor[aperture_name]


def aperture_size_to_correction_factor(beamline: str, aperture_size: int) -> float:
    """
    :raises TypeError: wrong :code:`aperture_size` type
    :raises ValueError: unknown :code:`aperture_size` value
    """
    if not isinstance(aperture_size, int):
        raise TypeError(
            f"Aperture size {aperture_size!r} is of type '{type(aperture_size)}' instead of 'int'"
        )
    size_to_correction_factor = _aperture_size_to_correction_factor(beamline)
    if aperture_size not in size_to_correction_factor:
        raise ValueError(
            f"Aperture size {aperture_size} not in list of apertures sizes {list(size_to_correction_factor)}!"
        )
    return size_to_correction_factor[aperture_size]


def closest_aperture_size(beamline: str, aperture_size: int) -> int:
    sizes = config.get_value(beamline, "Beam", "apertureSizes")
    return min(sizes, key=lambda x: abs(x - aperture_size))


def _aperture_name_to_size(beamline: str) -> Dict[str, int]:
    names = config.get_value(beamline, "Beam", "apertureNames")
    sizes = config.get_value(beamline, "Beam", "apertureSizes")
    return dict(zip(map(str, names), map(int, sizes)))


def _aperture_size_to_name(beamline: str) -> Dict[int, str]:
    names = config.get_value(beamline, "Beam", "apertureNames")
    sizes = config.get_value(beamline, "Beam", "apertureSizes")
    return dict(zip(map(int, sizes), map(str, names)))


def _aperture_size_to_correction_factor(beamline: str) -> Dict[int, float]:
    sizes = config.get_value(beamline, "Beam", "apertureSizes")
    correction_factors = config.get_value(beamline, "Beam", "apertureCorrectionFactor")
    return dict(zip(map(int, sizes), map(float, correction_factors)))


def _aperture_name_to_correction_factor(beamline: str) -> Dict[str, float]:
    names = config.get_value(beamline, "Beam", "apertureNames")
    correction_factors = config.get_value(beamline, "Beam", "apertureCorrectionFactor")
    return dict(zip(map(str, names), map(float, correction_factors)))
