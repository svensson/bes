"""
Created on Jan 25, 2017

@author: svensson
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2017"
__updated__ = "2017-01-25"

import os
import shlex
import subprocess

from bes.workflow_lib import ispyb
from bes.workflow_lib import mxnice
from bes.workflow_lib import workflow_logging


def startAutoProcessing(
    beamline,
    firstImagePath,
    dataCollection,
    grenades_fastproc=False,
    grenades_parallelproc=False,
    EDNA_proc=False,
    XIA2_DIALS=False,
    autoPROC=False,
    XDSAPP=False,
):
    logger = workflow_logging.getLogger(beamline)
    # Check choice
    autoProcessingLaunchers = [("/cvmfs/sb.esrf.fr/bin/dozorLauncher", "after")]
    if grenades_fastproc:
        autoProcessingLaunchers.append(
            ("/cvmfs/sb.esrf.fr/bin/GrenadesFastProcLauncher", "before")
        )
    if grenades_parallelproc:
        autoProcessingLaunchers.append(
            ("/cvmfs/sb.esrf.fr/bin/GrenadesParallelProcLauncher", "after")
        )
    if EDNA_proc:
        autoProcessingLaunchers.append(
            ("/cvmfs/sb.esrf.fr/bin/EDNA_procLauncher", "after")
        )
    if XIA2_DIALS:
        autoProcessingLaunchers.append(
            ("/cvmfs/sb.esrf.fr/bin/XIA2_DIALS_Launcher", "after")
        )
    if autoPROC:
        autoProcessingLaunchers.append(
            ("/cvmfs/sb.esrf.fr/bin/autoPROCLauncher", "after")
        )
    if XDSAPP:
        autoProcessingLaunchers.append(
            ("/cvmfs/sb.esrf.fr/bin/XDSAPPLauncher", "after")
        )
    logger.debug("Auto processing launchers: {0}".format(autoProcessingLaunchers))
    logger.debug("Auto processing: firstImagePath = {0}".format(firstImagePath))
    # Create data processing path
    processedDataDir = dataCollection.imageDirectory.replace(
        "RAW_DATA", "PROCESSED_DATA"
    )
    autoProcessingDirName = "autoprocessing_{0}_run{1}_1".format(
        dataCollection.imagePrefix, dataCollection.dataCollectionNumber
    )
    dataProcessingPath = os.path.join(processedDataDir, autoProcessingDirName)
    # Create autoprocessing argument list
    for autoProcessingLauncher, mode in autoProcessingLaunchers:
        args = autoProcessingLauncher
        args += " -path {0} -mode ".format(dataProcessingPath)
        args += mode
        args += " -datacollectionID {0}".format(dataCollection.dataCollectionId)
        args += ' -residues 200 -anomalous False -cell "0,0,0,0,0,0"'
        args += " -mesh_collect "
        logger.debug("Auto processing: args = {0}".format(args))
        argList = shlex.split(args)
        # Submit processing
        subprocess.Popen(argList)
    if any(
        [
            grenades_fastproc,
            grenades_parallelproc,
            EDNA_proc,
            XIA2_DIALS,
            autoPROC,
            XDSAPP,
        ]
    ):
        logger.info(
            "Auto-processing started: grenades_fastproc={0}, grenades_parallelproc={1}, EDNA_proc={2}, XIA2_DIALS={3}, autoPROC={4}, XDSAPP={5}".format(
                grenades_fastproc,
                grenades_parallelproc,
                EDNA_proc,
                XIA2_DIALS,
                autoPROC,
                XDSAPP,
            )
        )
    else:
        logger.info("No auto-processing started.")
    return dataProcessingPath


def startCodgasMeshAndCollect(
    beamline, firstImagePath, firstDataCollectionId, listAutoProcessingDir
):
    logger = workflow_logging.getLogger(beamline)
    dataCollection = ispyb.findDataCollectionFromFileLocationAndFileName(
        beamline, firstImagePath
    )
    if dataCollection is None:
        raise RuntimeError(
            "Cannot find data collection for {0} in ISPyB - the data collection most likely failed.".format(
                firstImagePath
            )
        )
    processedDataDir = dataCollection.imageDirectory.replace(
        "RAW_DATA", "PROCESSED_DATA"
    )
    listDirFile = os.path.join(processedDataDir, "listProcessingDirs.txt")
    with open(listDirFile, "w") as fd:
        for autoProcessingDir in listAutoProcessingDir:
            fd.write("{0}\n".format(autoProcessingDir))
    # argList = ["/opt/pxsoft/bin/codgas_mesh_and_collect.pl", "2>1"]
    # p = subprocess.Popen(argList, cwd=processedDataDir, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    # p = subprocess.Popen(argList, cwd="/tmp", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    commandLine = "/opt/pxsoft/bin/codgas_mesh_and_collect.pl {0} {1}".format(
        listDirFile, firstDataCollectionId
    )
    slurmCommand, slurmJobId, stdout, stderr = mxnice.submitJobToSLURM(
        commandLine, working_directory=processedDataDir
    )
    logger.info(
        "Codgas mesh and collect started in directory {0}".format(processedDataDir)
    )
    logger.info("Cmmand line: '{0}'".format(commandLine))
    logger.info("slurmCommand : {0}".format(slurmCommand))
    logger.info("slurmJobId   : {0}".format(slurmJobId))
    logger.info("stdout: {0}".format(stdout))
    logger.info("stderr: {0}".format(stderr))
