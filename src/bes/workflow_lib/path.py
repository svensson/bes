"""
Workflow library module defining path help methods for workflows.
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2013"

import os
import pathlib
import re
import math
import time
import glob
import json
import pprint
import tempfile
import subprocess
from contextlib import contextmanager


from bes.workflow_lib import workflow_logging


def createWorkflowLogFilePath(working_dir):
    """Returns the path of the workflow log file"""
    logFilePath = os.path.join(working_dir, "workflow_log.txt")
    return logFilePath


def createWorkflowDebugLogFilePath(working_dir):
    """Returns the path of the workflow debug log file"""
    debugLogFilePath = os.path.join(working_dir, "workflow_debug_log.txt")
    return debugLogFilePath


def _createWorkflowDirWithTimeStamp():
    """
    Returns a name consisting of 'Workflow_' followed by the
    current date/time (YYYYMMDD-HHMMSS)
    """
    return "Workflow_" + time.strftime("%Y%m%d-%H%M%S", time.localtime(time.time()))


def _createWorkflowWorkingDirectoryName(directory):
    """Creates a new name for the workflow working directory"""
    dirName = _createWorkflowDirWithTimeStamp()
    process_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")
    working_dir = os.path.join(process_directory, dirName)
    return working_dir


def createWorkflowWorkingDirectory(directory=None):
    """Creates and returns the path of the workflow working directory"""
    # First try to determine name:
    if directory is None:
        workingDir = tempfile.mkdtemp(prefix=_createWorkflowDirWithTimeStamp())
    else:
        workingDir = _createWorkflowWorkingDirectoryName(directory)
        if workingDir is None or not os.path.exists(workingDir):
            # Try to create directory
            try:
                os.makedirs(workingDir, 0o755)
            except Exception:
                # Couldn't create directory, we must create a temporary directory
                workingDir = tempfile.mkdtemp(prefix=_createWorkflowDirWithTimeStamp())
        else:
            # Check that we have permissions to write in directory
            if not os.access(workingDir, os.W_OK):
                # Couldn't create directory, we must create a temporary directory
                workingDir = tempfile.mkdtemp(prefix=_createWorkflowDirWithTimeStamp())
    return workingDir


def createStackTraceFile(directory):
    with _open_random_file(directory, "stackTrace_", "") as (filename, _):
        pass
    return filename


def getDictPath(directory):
    """
    This function splits up an ESRF path into the following components:
      beamline
      proposal
      date
      rest (remaining path)
    """
    dictPath = {}
    dictPath["beamline"] = None
    dictPath["proposal"] = None
    dictPath["date"] = None
    dictPath["rest"] = None
    # ESRF beamlines
    listBeamline = [
        "bm07",
        "id23eh1",
        "id23eh2",
        "id29",
        "id30a1",
        "id30a2",
        "id30a3",
        "id30b",
        "id31",
        "cm01",
    ]
    # Split the path
    listDir = directory.split("/")
    # print(listDir)
    if len(listDir) > 2:
        if (listDir[1] == "data" and listDir[2] == "visitor") or (
            listDir[1] == "mntdirect" and listDir[2] == "_data_visitor"
        ):
            # /data/visitor/mx415/id30a1/20180213/RAW_DATA
            if len(listDir) > 3:
                dictPath["proposal"] = listDir[3]
            if len(listDir) > 4 and listDir[4] in listBeamline:
                dictPath["beamline"] = listDir[4]
            if len(listDir) > 5:
                dictPath["date"] = listDir[5]
            if len(listDir) > 6:
                dictPath["rest"] = os.path.join(*listDir[6:])
        elif len(listDir) > 3:
            if listDir[1] == "data" and listDir[3] == "inhouse":
                # /data/id30a1/inhouse/opid30a1/id30a1/20180213/RAW_DATA
                if listDir[2] in listBeamline:
                    dictPath["beamline"] = listDir[2]
                if len(listDir) > 4:
                    dictPath["proposal"] = listDir[4]
                if len(listDir) > 5:
                    dictPath["date"] = listDir[5]
                if len(listDir) > 6:
                    dictPath["rest"] = os.path.join(*listDir[6:])
            elif (
                listDir[1] == "mntdirect"
                and "data" in listDir[2]
                and "inhouse" in listDir[2]
            ):
                # /mntdirect/_data_id30a1_inhouse/opid30a1/20180213/RAW_DATA
                beamline = listDir[2].split("_")[2]
                if beamline in listBeamline:
                    dictPath["beamline"] = beamline
                if len(listDir) > 3:
                    dictPath["proposal"] = listDir[3]
                if len(listDir) > 4:
                    dictPath["date"] = listDir[4]
                if len(listDir) > 5:
                    dictPath["rest"] = os.path.join(*listDir[5:])
    return dictPath


def _getPyarchFilePath(workingDir):
    """
    This method translates from a "visitor" path to a "pyarch" path:
    /data/visitor/mx415/id14eh1/20100209 -> /data/pyarch/2010/id14eh1/mx415/20100209
    """
    pyarchFilePath = None
    dictPath = getDictPath(workingDir)
    if "beamline" in dictPath:
        beamline = dictPath["beamline"]
    else:
        beamline = None
    if "proposal" in dictPath:
        proposal = dictPath["proposal"]
    else:
        proposal = None
    if "date" in dictPath:
        date = dictPath["date"]
    else:
        date = None
    if "rest" in dictPath:
        rest = dictPath["rest"]
    else:
        rest = None
    # print([beamline, proposal, date])
    if all([beamline, proposal, date]):
        year = date[0:4]
        pyarchFilePath = os.path.join(os.sep, "data")
        pyarchFilePath = os.path.join(pyarchFilePath, "pyarch")
        pyarchFilePath = os.path.join(pyarchFilePath, year)
        pyarchFilePath = os.path.join(pyarchFilePath, beamline)
        pyarchFilePath = os.path.join(pyarchFilePath, proposal)
        pyarchFilePath = os.path.join(pyarchFilePath, date)
        if rest is not None:
            pyarchFilePath = os.path.join(pyarchFilePath, dictPath["rest"])
    return pyarchFilePath


def createPyarchFilePath(workingDir):
    """Returns the path of the pyarch directory"""
    pyarchFilePath = _getPyarchFilePath(workingDir)
    if pyarchFilePath is not None:
        logger = workflow_logging.getLogger()
        if not os.path.exists(pyarchFilePath):
            try:
                os.makedirs(pyarchFilePath, 0o755)
                logger.debug("Pyarch directory created: %s" % pyarchFilePath)
            except Exception:
                logger.warning(
                    "Pyarch directory could not be created: %s" % pyarchFilePath
                )
                pyarchFilePath = None
        else:
            # Check that we have permissions to write in directory
            if not os.access(pyarchFilePath, os.W_OK):
                logger.warning(
                    "No write access to pyarch directory: %s" % pyarchFilePath
                )
                pyarchFilePath = None
    return pyarchFilePath


def createUniqueICATDirectory(
    raw_directory, run_number, workflow_index=None, expTypePrefix="datacollection"
):
    # Create unique directory for ICAT
    logger = workflow_logging.getLogger()
    uniqueDirFound = False
    raw_path = pathlib.Path(raw_directory)
    while not uniqueDirFound:
        if expTypePrefix is None:
            if workflow_index is None:
                directory = raw_path / f"run_{run_number:02d}"
            else:
                directory = raw_path / f"run_{workflow_index:02d}_{run_number:02d}"
        else:
            if expTypePrefix.endswith("-"):
                exp_type = expTypePrefix[:-1]
            else:
                exp_type = expTypePrefix
            if exp_type.startswith("ref"):
                exp_type = "characterisation"
            if workflow_index is None:
                directory = raw_path / f"run_{run_number:02d}_{exp_type}"
            else:
                directory = (
                    raw_path / f"run_{workflow_index:02d}_{run_number:02d}_{exp_type}"
                )

        if workflow_index is None:
            if len(glob.glob(f"{raw_path}/run_{run_number:02d}_*")) > 0:
                run_number += 1
            else:
                directory.mkdir(mode=0o755, exist_ok=False, parents=True)
                uniqueDirFound = True
        else:
            if (
                len(
                    glob.glob(f"{raw_path}/run_{workflow_index:02d}_{run_number:02d}_*")
                )
                > 0
            ):
                run_number += 1
            else:
                directory.mkdir(mode=0o755, exist_ok=False, parents=True)
                uniqueDirFound = True
    logger.info(f"New run number: {run_number}")
    logger.info(f"New directory: {directory}")
    return str(directory), run_number


def addUniqueSubDirToPath(directory, subDirName, makeDirectory=True):
    """Adds a unique dub directory to the path"""
    logger = workflow_logging.getLogger()
    newDirectory = None
    index = 1
    doContinue = True
    while doContinue:
        unique_sub_dir = "%s_%02d" % (subDirName, index)
        newDirectory = os.path.join(directory, unique_sub_dir)
        logger.debug("addUniqueSubDirToPath: trying directory {0}".format(newDirectory))
        if os.path.exists(newDirectory):
            logger.debug("addUniqueSubDirToPath: directory exists, increasing index")
            index += 1
        else:
            logger.debug("addUniqueSubDirToPath: found unique directory")
            doContinue = False
            if makeDirectory:
                logger.debug("addUniqueSubDirToPath: creating directory")
                os.makedirs(newDirectory, 0o755)
    return (newDirectory, index)


def addUniqueSubDirPrefixToPath(str_directory, subDirName, makeDirectory=True):
    """Adds a unique dub directory prefix to the path"""
    logger = workflow_logging.getLogger()
    newDirectory = None
    index = 1
    doContinue = True
    if not os.path.exists(str_directory):
        os.makedirs(str_directory, 0o755)
    listDir = os.listdir(str_directory)
    while doContinue:
        unique_sub_dir_prefix = "{0:02d}".format(index)
        doContinue = False
        for directory in listDir:
            if directory.startswith(unique_sub_dir_prefix):
                logger.debug(
                    "addUniqueSubDirPrefixToPath: directory prefix exists, increasing index"
                )
                index += 1
                doContinue = True
            else:
                logger.debug(
                    "addUniqueSubDirPrefixToPath: found unique directory prefix"
                )
    newDirectory = os.path.join(
        str_directory, "{0}_{1}".format(unique_sub_dir_prefix, subDirName)
    )
    if makeDirectory:
        logger.debug("addUniqueSubDirPrefixToPath: creating directory")
        os.makedirs(newDirectory, 0o755)
    return (newDirectory, index)


def copyHtmlDirectoryToPyarch(
    pyarch_html_dir, dict_html, directory_name="html", indexHtml="index.html"
):
    """Copies a dirctory containing html pages to pyarch"""
    dict_html_pyarch = {}
    if pyarch_html_dir != "":
        # Create a new sub-directory
        if not os.path.exists(pyarch_html_dir):
            os.makedirs(pyarch_html_dir, mode=0o755)
        pyarch_html_directory_path = addUniqueSubDirToPath(
            pyarch_html_dir, directory_name, makeDirectory=False
        )[0]
        systemCopyTree(dict_html["html_directory_path"], pyarch_html_directory_path)
        p = subprocess.Popen(
            "find " + pyarch_html_dir + r" -type d -exec chmod 755 {} \;", shell=True
        )
        p.wait()
        p = subprocess.Popen(
            "find " + pyarch_html_dir + r" -type f -exec chmod 644 {} \;", shell=True
        )
        p.wait()

        # Check if we have an index.html file...
        if indexHtml != "index.html":
            pyarchIndexHtmlPath = os.path.join(pyarch_html_directory_path, "index.html")
            systemCopyFile(
                os.path.join(pyarch_html_directory_path, indexHtml), pyarchIndexHtmlPath
            )
            os.chmod(pyarchIndexHtmlPath, mode=0o644)
        dict_html_pyarch["html_directory_path"] = pyarch_html_directory_path
        dict_html_pyarch["index_file_name"] = dict_html["index_file_name"]
        if "plot_file_name" in dict_html:
            dict_html_pyarch["plot_file_name"] = dict_html["plot_file_name"]
    #         destination_html_index_path = os.path.join(pyarch_html_directory_path, dict_html["index_file_name"])
    #         if not os.path.exists(destination_html_index_path):
    #             os.symlink(os.path.basename(str_html_index_path),
    #                        destination_html_index_path)
    return dict_html_pyarch


def extractBeamlineFromDirectory(directory):
    """Returns the name of a beamline given a directory path"""
    this_beamline = None
    list_directory = directory.split(os.sep)
    # First check: directory must start with "data":
    if list_directory[1] == "data":
        # Then check if second level is "visitor":
        if list_directory[2] == "visitor":
            this_beamline = list_directory[4]
        elif list_directory[3] == "inhouse":
            this_beamline = list_directory[2]
    if this_beamline is None:
        raise Exception("Could not extract beamline from path %s" % directory)
    return this_beamline


def findUniqueRunNumber(directory, prefix, expTypePrefix="", run_number=1):
    """Returns a unique run_number for the directory."""
    logger = workflow_logging.getLogger()
    found_unique_run_number = False
    tmp_run_number = run_number
    while not found_unique_run_number:
        image_name = "%s%s_%d_*.*" % (expTypePrefix, prefix, tmp_run_number)
        logger.debug("Looking for files: %s" % os.path.join(directory, image_name))
        list_files = glob.glob(os.path.join(directory, image_name))
        if list_files == []:
            unique_run_number = tmp_run_number
            logger.debug("No file found, unique run number: %d" % unique_run_number)
            found_unique_run_number = True
        else:
            logger.debug("Files found: %r" % list_files[0])
            tmp_run_number += 1
    logger.debug(
        "Unique run number = %d found for directory %s" % (unique_run_number, directory)
    )
    return unique_run_number


def findProcessedSampleDirectory(directory):
    processedSampleDirectory = None
    if "RAW_DATA" in directory:
        listSplitDirectory = directory.split("RAW_DATA")
        processedSampleDirectory = os.path.join(
            listSplitDirectory[0],
            "PROCESSED_DATA",
            listSplitDirectory[1].split(os.sep)[1],
        )
    else:
        processedSampleDirectory = directory
    return processedSampleDirectory


@contextmanager
def _open_random_file(dir, prefix, suffix):
    os.makedirs(dir, exist_ok=True)
    with tempfile.NamedTemporaryFile(
        "w", dir=dir, prefix=prefix, suffix=suffix, delete=False
    ) as fd:
        filename = fd.name
        yield filename, fd
    os.chmod(filename, 0o644)


def createNewJsonFile(workflow_working_dir, jsonData, filePrefix="data"):
    with _open_random_file(workflow_working_dir, filePrefix + "_", ".json") as (
        filename,
        fd,
    ):
        json.dump(jsonData, fd, indent=4)
    return filename


def parseJsonFile(jsonFilePath):
    with open(jsonFilePath) as jsonFile:
        return json.load(jsonFile)


def createNewXSDataFile(workflow_working_dir, xsData, filePrefix="xsData"):
    if xsData is None:
        return
    with _open_random_file(workflow_working_dir, filePrefix + "_", ".xml") as (
        filename,
        fd,
    ):
        if isinstance(xsData, str):
            fd.write(xsData)
        else:
            fd.write(xsData.marshal())
    return filename


def readXSDataFile(xsDataFilePath):
    if xsDataFilePath is None or xsDataFilePath == "None":
        return
    with open(xsDataFilePath) as fd:
        return fd.read()


def getImagePathFromQueueEntry(queueEntry, suffix, imageNumber=None):
    formatDict = dict(queueEntry["fileData"])
    formatDict["suffix"] = suffix
    if imageNumber is None:
        formatDict["imageNumber"] = formatDict["firstImage"]
    else:
        formatDict["imageNumber"] = imageNumber
    imagePath = "{directory}/{expTypePrefix}{prefix}_{runNumber}_{imageNumber:04d}.{suffix}".format(
        **formatDict
    )
    return imagePath


def getQueueEntryImagePathList(queueEntry, suffix):
    logger = workflow_logging.getLogger()
    # logger.debug(pprint.pformat(queueEntry))
    listImagePath = []
    logger.debug(pprint.pformat(queueEntry["subWedgeData"]))
    numberOfImages = queueEntry["subWedgeData"]["noImages"]
    firstImage = queueEntry["fileData"]["firstImage"]
    for imageNumber in range(firstImage, firstImage + numberOfImages):
        imagePath = getImagePathFromQueueEntry(queueEntry, suffix, imageNumber)
        listImagePath.append(imagePath)
    return listImagePath


def getProposalNameNumber(directory):
    """Returns the name and number of a proposal name given a directory path"""
    proposal = None
    proposal_name = None
    proposal_number = None
    list_directory = directory.split(os.sep)
    # First check: directory must start with "data":
    if list_directory[1] == "data":
        # Then check if second level is "visitor":
        if list_directory[2] == "visitor":
            proposal = list_directory[3]
        elif list_directory[3] == "inhouse":
            proposal = list_directory[4]
        if proposal is not None:
            list_proposal_name = [
                "fx",
                "ifx",
                "ihls",
                "ihmx",
                "ih",
                "im",
                "ix",
                "ls",
                "mxihr",
                "mx",
                "opid",
            ]
            for proposal_name in list_proposal_name:
                if proposal.startswith(proposal_name):
                    proposal_number = proposal.split(proposal_name)[1]
                    break
    # Check that we have a valid proposal number, i.e. the first character is a digit
    if proposal_number is not None and not proposal_number[0].isnumeric():
        proposal_name = None
        proposal_number = None
    return proposal_name, proposal_number


def _compileAndMatchRegexpTemplate(_strPathToImage):
    listResult = []
    baseImageName = os.path.basename(_strPathToImage)
    regexp = re.compile(r"(.*)([^0^1^2^3^4^5^6^7^8^9])([0-9]*)\.(.*)")
    match = regexp.match(baseImageName)
    if match is not None:
        listResult = [
            match.group(0),
            match.group(1),
            match.group(2),
            match.group(3),
            match.group(4),
        ]
    return listResult


def getImageNumber(_strPathToImage):
    imageNumber = None
    listResult = _compileAndMatchRegexpTemplate(_strPathToImage)
    if listResult is not None:
        imageNumber = int(listResult[3])
    return imageNumber


def getPrefix(_strPathToImage):
    strPrefix = None
    listResult = _compileAndMatchRegexpTemplate(_strPathToImage)
    if listResult is not None:
        strPrefix = listResult[1]
    return strPrefix


def eigerTemplateToImage(fileTemplate, imageNumber, overlap=0):
    if math.fabs(overlap) < 1:
        fileNumber = int(imageNumber / 100)
        if fileNumber == 0:
            fileNumber = 1
        eigerFileTemplate = fileTemplate.replace("%04d", "1_data_%06d" % fileNumber)
    else:
        eigerFileTemplate = fileTemplate.replace("%04d", "%d_data_000001" % imageNumber)
    return eigerFileTemplate


def _checkTimeStampFile(directory, timeStampFileName, timeOut=3600):
    logger = workflow_logging.getLogger()
    hasNoImageFlag = False
    # Create time stamp just after PROCESSED_DATA
    if "PROCESSED_DATA" in directory:
        processedDataDir = os.path.join(
            directory.split("PROCESSED_DATA")[0], "PROCESSED_DATA"
        )
        timeStampPath = os.path.join(processedDataDir, timeStampFileName)
        if not os.path.exists(timeStampPath):
            # Create an empty file
            logger.debug("Creating stamp path: {0}".format(timeStampPath))
            with open(timeStampPath, "w"):
                pass
        else:
            # Get time stamp
            createdTimeStamp = os.path.getctime(timeStampPath)
            timeElapsed = time.time() - createdTimeStamp
            logger.debug(
                "Time stamp path: {0}, elapsed time {1} s, timeout = {2} s".format(
                    timeStampPath, int(timeElapsed), timeOut
                )
            )
            if timeElapsed < timeOut:
                hasNoImageFlag = True
    return hasNoImageFlag


def checkNoImageFlag(directory, timeOut=3600):
    timeStampFileName = ".noImagesDetectedTimeStamp"
    return _checkTimeStampFile(directory, timeStampFileName, timeOut)


def checkSameSnapShotImageFlag(directory, timeOut=3600):
    timeStampFileName = ".sameSnapShotImageTimeStamp"
    return _checkTimeStampFile(directory, timeStampFileName, timeOut)


def _checkCountTimeStampFile(directory, timeStampFileName, timeOut=3600):
    logger = workflow_logging.getLogger()
    count = None
    # Create time stamp just after PROCESSED_DATA
    if "PROCESSED_DATA" in directory:
        processedDataDir = os.path.join(
            directory.split("PROCESSED_DATA")[0], "PROCESSED_DATA"
        )
        timeStampPath = os.path.join(processedDataDir, timeStampFileName)
        if not os.path.exists(timeStampPath):
            # Create a file with count 1
            logger.debug("Creating count stamp path: {0}".format(timeStampPath))
            with open(timeStampPath, "w") as fd:
                count = 1
                fd.write(str(count))
        else:
            # Get time stamp
            createdTimeStamp = os.path.getctime(timeStampPath)
            with open(timeStampPath) as fd:
                count = int(fd.read())
            timeElapsed = time.time() - createdTimeStamp
            logger.debug(
                "Time stamp path: {0}, elapsed time {1} s, timeout = {2} s".format(
                    timeStampPath, int(timeElapsed), timeOut
                )
            )
            if timeElapsed < timeOut:
                # Increase the count and write the new count to the file
                count += 1
            else:
                # Time out - reset the counter
                count = 1
            with open(timeStampPath, "w") as fd:
                fd.write(str(count))
    return count


def checkSameSnapShotImageCountFlag(directory, timeOut=3600):
    timeStampFileName = ".sameSnapShotImageTimeCountStamp"
    return _checkCountTimeStampFile(directory, timeStampFileName, timeOut)


def getH5FilePath(filePath, batchSize=1, isFastMesh=False):
    imageNumber = getImageNumber(filePath)
    prefix = getPrefix(filePath)
    if isFastMesh:
        h5ImageNumber = int((imageNumber - 1) / 100) + 1
        h5FileNumber = 1
    else:
        h5ImageNumber = 1
        h5FileNumber = int((imageNumber - 1) / batchSize) * batchSize + 1
    h5MasterFileName = "{prefix}_{h5FileNumber}_master.h5".format(
        prefix=prefix, h5FileNumber=h5FileNumber
    )
    h5MasterFilePath = os.path.join(os.path.dirname(filePath), h5MasterFileName)
    h5DataFileName = "{prefix}_{h5FileNumber}_data_{h5ImageNumber:06d}.h5".format(
        prefix=prefix, h5FileNumber=h5FileNumber, h5ImageNumber=h5ImageNumber
    )
    h5DataFilePath = os.path.join(os.path.dirname(filePath), h5DataFileName)
    return h5MasterFilePath, h5DataFilePath, h5FileNumber


def getListH5FilePath(listFilePath, batchSize=1, isFastMesh=False):
    listH5FilePath = []
    for filePath in listFilePath:
        h5MasterFilePath, h5DataFilePath, h5FileNumber = getH5FilePath(
            filePath, batchSize=batchSize, isFastMesh=isFastMesh
        )
        if h5MasterFilePath not in listH5FilePath:
            listH5FilePath.append(h5MasterFilePath)
        if h5DataFilePath not in listH5FilePath:
            listH5FilePath.append(h5DataFilePath)
    return sorted(listH5FilePath)


def getDefaultParametersPath(directory, file_prefix="workflow"):
    parameters_path = None
    if directory is not None:
        if "RAW_DATA" in directory:
            processed_data_dir = directory.split("RAW_DATA")[0] + "PROCESSED_DATA"
        else:
            processed_data_dir = directory
        parameters_path = os.path.join(
            processed_data_dir, f"{file_prefix}_defaults.json"
        )
    return parameters_path


def getDefaultParameters(directory, file_prefix="workflow"):
    parameters = {}
    # Try to retrieve existing parameters if directory is given
    try:
        if directory is not None:
            parameters_path = getDefaultParametersPath(
                directory, file_prefix=file_prefix
            )
            if os.path.exists(parameters_path):
                with open(parameters_path) as fd:
                    parameters = json.loads(fd.read())
    except Exception:
        # Something went wrong when reading parameters, most likely the
        # file is corrupt. We therefore reset the parameters
        pass
    return parameters


def saveDefaultParameters(directory, parameters, file_prefix="workflow"):
    parameters_path = getDefaultParametersPath(directory, file_prefix=file_prefix)
    if parameters_path is not None:
        with open(parameters_path, "w") as fd:
            fd.write(json.dumps(parameters, indent=4))


def systemCopyFile(fromPath, toPath):
    p = subprocess.Popen(["cp", fromPath, toPath])
    p.wait()


def systemRmTree(treePath, ignore_errors=False):
    try:
        if ignore_errors:
            subprocess.check_call(f"rm -rf {treePath}", shell=True)
        else:
            subprocess.check_call(f"rm -r {treePath} 2>&1 > /dev/null", shell=True)
    except subprocess.CalledProcessError:
        if not ignore_errors:
            raise


def systemCopyTree(from_path, to_path, dirs_exists_ok=False):
    if os.path.exists(to_path):
        if dirs_exists_ok:
            systemRmTree(to_path)
        else:
            raise FileExistsError(to_path)
    p = subprocess.Popen(["cp", "-r", from_path, to_path])
    p.wait()


def getSampleNameFromPath(path):
    if isinstance(path, str):
        path = pathlib.Path(path)
    if not path.is_dir():
        path = path.parent
    dir1_name = path.name
    if dir1_name.startswith("run"):
        dir2 = path.parent
        dir2_name = dir2.name
        if dir2_name.startswith("run"):
            sample_name = dir2.parent.name
        else:
            sample_name = dir2_name
    else:
        sample_name = dir1_name
    return sample_name
