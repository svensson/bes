"""
Created on Jan 5, 2015

@author: svensson
"""

import pprint

from bes.workflow_lib import path
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def createLiformDict(listDialog, dialogName="Dialog"):
    propertiesDict = {}
    listRequired = []
    dictInitialValues = {}
    for dictDialog in listDialog:
        tmpDict = {}
        tmpDict["title"] = dictDialog["label"]
        # Set type
        if dictDialog["type"] == "int":
            # For the moment set the type of int to "string"
            tmpDict["type"] = "string"
        #            tmpDict["type"] = "number"
        #            tmpDict["widget"] = "number"
        elif dictDialog["type"] == "float":
            # For the moment set the type of float to "string"
            tmpDict["type"] = "string"
        elif dictDialog["type"] == "text":
            tmpDict["type"] = "string"
        elif dictDialog["type"] in ["combo", "boolean"]:
            tmpDict["type"] = "string"
            if dictDialog["type"] == "boolean":
                dictDialog["textChoices"] = ["true", "false"]
            tmpDict["enum"] = dictDialog["textChoices"]
        propertiesDict[dictDialog["variableName"]] = tmpDict
        listRequired.append(dictDialog["variableName"])
        convertBoolean(dictDialog, dictInitialValues)
    return {
        "properties": propertiesDict,
        "required": listRequired,
        "initialValues": dictInitialValues,
        "dialogName": dialogName,
    }


def convertBoolean(dictDialog, dictInitialValues):
    if "value" in dictDialog:
        if dictDialog["type"] == "boolean":
            if dictDialog["value"]:
                dictInitialValues[dictDialog["variableName"]] = "true"
            else:
                dictInitialValues[dictDialog["variableName"]] = "false"
        else:
            dictInitialValues[dictDialog["variableName"]] = str(dictDialog["value"])
    else:
        if dictDialog["type"] == "boolean":
            if dictDialog["defaultValue"]:
                dictInitialValues[dictDialog["variableName"]] = "true"
            else:
                dictInitialValues[dictDialog["variableName"]] = "false"
        else:
            dictInitialValues[dictDialog["variableName"]] = str(
                dictDialog["defaultValue"]
            )


def openDialog(
    beamline,
    listDialog,
    directory=None,
    token=None,
    automaticMode=False,
    collection_software=None,
    file_prefix="workflow",
):
    logger = workflow_logging.getLogger()
    parameters = path.getDefaultParameters(directory)
    # Check which parameters that need to be updated
    for dialog in listDialog:
        if dialog.get("useDefaults", True):
            variableName = dialog["variableName"]
            if variableName in parameters:
                dialog["value"] = parameters[variableName]
    # Open dialog
    dictValues = openDialogMXCuBE3(beamline, listDialog, logger, automaticMode, token)

    # Adjust types and check that values are withing lower and higher bounds
    adjustTypesAndCheckBoundaries(
        beamline=beamline,
        listDialog=listDialog,
        dictValues=dictValues,
        logger=logger,
        token=token,
        collection_software=collection_software,
    )
    # Save parameters to disk
    parameters.update(dictValues)
    path.saveDefaultParameters(directory, parameters, file_prefix=file_prefix)
    return dictValues


def openDialogMXCuBE3(beamline, listDialog, logger, automaticMode, token):
    dictValues = {}
    if not automaticMode:
        liformDict = createLiformDict(listDialog)
        dictDialog = {}
        dictDialog["reviewData"] = liformDict
        listDialogCopy = []
        for dialog in listDialog:
            dialogCopy = dict(dialog)
            if dialogCopy["type"] == "boolean":
                if dialogCopy["value"]:
                    dialogCopy["value"] = "true"
                else:
                    dialogCopy["value"] = "false"
            listDialogCopy.append(dialogCopy)
        dictDialog["inputMap"] = listDialogCopy
        logger.debug(
            "dictDialog before mxcube3: {0}".format(pprint.pformat(dictDialog))
        )
        dictValues = collect.openDialog(beamline, dictDialog, token=token)
        logger.debug("dictValues from mxcube3: {0}".format(pprint.pformat(dictValues)))
    else:
        logger.debug("Automatic mode")
        # Just create an output dictionary with values
        for dictDialog in listDialog:
            if "value" in dictDialog:
                dictValues[dictDialog["variableName"]] = dictDialog["value"]
            else:
                dictValues[dictDialog["variableName"]] = dictDialog["defaultValue"]
    return dictValues


def adjustTypesAndCheckBoundaries(
    beamline,
    listDialog,
    dictValues,
    logger,
    token=None,
    collection_software=None,
):
    for dictDialog in listDialog:
        logger.debug(dictDialog)
        variableName = dictDialog["variableName"]
        if dictDialog["type"] == "float":
            # BES-307: automatically convert "," to "."
            strValue = str(dictValues[variableName])
            strValue = strValue.replace(",", ".")
            dictValues[variableName] = float(strValue)
        elif dictDialog["type"] == "int":
            try:
                dictValues[variableName] = int(float(dictValues[variableName]))
            except ValueError:
                error_message = (
                    "Variable '{0}' of value '{1}' should be an integer value!".format(
                        dictDialog["label"], dictValues[variableName]
                    )
                )
                logger.error(error_message)
                raise BaseException(error_message)
        elif dictDialog["type"] == "boolean":
            if isinstance(dictValues[variableName], str):
                dictValues[variableName] = dictValues[variableName] == "true"
        if variableName in dictValues:
            stop_workflow = "Stop the workflow"
            if "lowerBound" in dictDialog and float(dictDialog["lowerBound"]) > float(
                dictValues[variableName]
            ):
                error_message = "Lower bound of the variable '{1}' exceed! The lower bound is {0} and the entered value is {2}.".format(
                    dictDialog["lowerBound"],
                    dictDialog["label"],
                    dictValues[variableName],
                )
                default_value = "Use the value '{0}'".format(dictDialog["lowerBound"])
                list_dialog = (
                    {
                        "variableName": "choice",
                        "label": error_message,
                        "type": "combo",
                        "defaultValue": default_value,
                        "textChoices": [default_value, stop_workflow],
                    },
                )
                lower_bound_dict = openDialog(
                    beamline,
                    list_dialog,
                    automaticMode=False,
                    token=token,
                    collection_software=collection_software,
                )
                if lower_bound_dict["choice"] == stop_workflow:
                    raise RuntimeError(error_message)
                else:
                    dictValues[variableName] = dictDialog["lowerBound"]
            elif "upperBound" in dictDialog and float(dictDialog["upperBound"]) < float(
                dictValues[variableName]
            ):
                error_message = "Upper bound of the variable '{1}' exceed! The upper bound is {0} and the entered value is {2}.".format(
                    dictDialog["upperBound"],
                    dictDialog["label"],
                    dictValues[variableName],
                )
                default_value = "Use the value '{0}'".format(dictDialog["upperBound"])
                upper_bound_dialog = (
                    {
                        "variableName": "choice",
                        "label": error_message,
                        "type": "combo",
                        "defaultValue": default_value,
                        "textChoices": [default_value, stop_workflow],
                    },
                )
                upper_bound_dict = openDialog(
                    beamline,
                    upper_bound_dialog,
                    automaticMode=False,
                    token=token,
                    collection_software=collection_software,
                )
                if upper_bound_dict["choice"] == stop_workflow:
                    raise RuntimeError(error_message)
                else:
                    dictValues[variableName] = dictDialog["upperBound"]


def displayMessage(
    beamline,
    title,
    typeString,
    messageUnescaped,
    automaticMode=False,
    collection_software=None,
    token=None,
):
    logger = workflow_logging.getLogger()
    dictValues = {}
    if beamline.startswith("simulator"):
        return
    elif collection_software == "MXCuBE - 3.0":
        # Use the dialog for displaying the message
        list_dialog = []
        list_message = messageUnescaped.split("\n")
        index = 0
        while index < len(list_message):
            message = list_message[index]
            while len(message) == 0 and index < len(list_message):
                index += 1
                message = list_message[index]
            index += 1
            if len(message) > 0:
                if index >= len(list_message):
                    text_choice = "Ok"
                else:
                    text_choice = list_message[index]
                    while len(text_choice) == 0 and index < len(list_message):
                        text_choice = list_message[index]
                        index += 1
                    index += 1
                if len(text_choice) == 0:
                    text_choice = "Ok"
                variableName = "message_line_" + str(index)
                list_dialog.append(
                    {
                        "variableName": variableName,
                        "label": message,
                        "type": "combo",
                        "defaultValue": text_choice,
                        "textChoices": [text_choice],
                    }
                )
        logger.debug(pprint.pformat(list_dialog))
        dictValues = openDialog(
            beamline,
            list_dialog,
            automaticMode=automaticMode,
            token=token,
            collection_software=collection_software,
        )
    else:
        message = messageUnescaped.replace("<", "&lt;").replace(">", "&gt;")
        logger.debug("Title " + title + "; message " + message)
        if typeString.lower() == "warning":
            logger.warning(messageUnescaped)
        elif typeString.lower() == "error":
            logger.error(messageUnescaped)
        else:
            logger.debug(messageUnescaped)
        if not automaticMode:
            xmlMessage = '<?xml version="1.0" encoding="UTF-8"?>\n'
            xmlMessage += "<message>\n"
            xmlMessage += "  <type>" + typeString + "</type>\n"
            xmlMessage += "  <text>" + message + "</text>\n"
            xmlMessage += "</message>\n"
            logger.debug("XML message: " + xmlMessage)
            dictDialog = {}
            dictDialog["reviewData"] = xmlMessage
            dictDialog["inputMap"] = []
            dictValues = collect.openDialog(beamline, dictDialog, token=token)
    return dictValues


def createDawnBeanDecoderXML(listDialog):
    strXML = """<?xml version="1.0" encoding="UTF-8"?>\n"""
    strXML += """<java version="1.7.0_40" class="java.beans.XMLDecoder">\n"""
    strXML += (
        """ <object class="org.dawb.passerelle.actors.ui.config.FieldContainer">\n"""
    )
    strXML += addCustomLabel()
    strXML += """  <void property="fields">\n"""
    for dictDialog in listDialog:
        strXML += addFieldProperty(dictDialog)
    strXML += """ </object>\n"""
    strXML += """</java>"""
    return strXML


def addCustomLabel(customLabel=""):
    strXML = """  <void property="customLabel">\n"""
    strXML += """   <string></string>\n"""
    strXML += """  </void>\n"""
    return strXML


def addFieldProperty(dictDialog):
    strXML = ""
    strXML += """   <void method="add">\n"""
    strXML += (
        """    <object class="org.dawb.passerelle.actors.ui.config.FieldBean">\n"""
    )
    if dictDialog["type"] in ["float", "double", "int"]:
        strXML += addProperty(dictDialog, "defaultValue")
        strXML += addProperty(dictDialog, "lowerBound", bForceType=True)
        strXML += addUiClass(dictDialog)
        strXML += addProperty(dictDialog, "uiLabel", "label")
        strXML += addProperty(dictDialog, "unit")
        strXML += addProperty(dictDialog, "upperBound", bForceType=True)
        strXML += addProperty(dictDialog, "variableName")
    elif dictDialog["type"] in ["text", "combo", "boolean"]:
        if dictDialog["type"] == "combo":
            strXML += addTextChoices(dictDialog["textChoices"])
        elif dictDialog["type"] == "boolean":
            strXML += addTextChoices(["true", "false"])
        else:
            strXML += addProperty(dictDialog, "defaultValue")
        strXML += addUiClass(dictDialog)
        strXML += addProperty(dictDialog, "uiLabel", "label")
        strXML += addProperty(dictDialog, "variableName")
    else:
        raise BaseException(
            "dialog.py: Undefinded type in dictDialog: {0}".format(dictDialog["type"])
        )
    strXML += """    </object>\n"""
    strXML += """   </void>\n"""
    return strXML


def addUiClass(dictDialog):
    strXML = """     <void property="uiClass">\n"""
    if dictDialog["type"] == "float":
        strXML += """      <string>org.dawnsci.common.richbeans.components.scalebox.StandardBox</string>\n"""
    elif dictDialog["type"] == "int":
        strXML += """      <string>org.dawnsci.common.richbeans.components.wrappers.SpinnerWrapper</string>\n"""
    elif dictDialog["type"] == "text":
        strXML += """      <string>org.dawnsci.common.richbeans.components.wrappers.StandardBox</string>\n"""
    elif dictDialog["type"] == "combo" or dictDialog["type"] == "boolean":
        strXML += """      <string>org.dawnsci.common.richbeans.components.wrappers.ComboWrapper</string>\n"""
    else:
        raise BaseException("dialog.py: Undefinded type in dictDialog")
    strXML += """     </void>\n"""
    return strXML


def addProperty(dictDialog, propertyName, valueName=None, bForceType=False):
    strXML = ""
    if valueName is None:
        valueName = propertyName
    if valueName in dictDialog.keys():
        value = dictDialog[valueName]
        strXML += """     <void property="{0}">\n""".format(propertyName)
        if bForceType:
            if dictDialog["type"] == "float" or dictDialog["type"] == "double":
                strType = "double"
            elif dictDialog["type"] == "int":
                strType = "int"
            else:
                raise BaseException("dialog.py: Undefinded type in dictDialog")
        else:
            strType = "string"
        strXML += """      <{0}>{1}</{0}>\n""".format(strType, value)
        strXML += """     </void>\n"""
    return strXML


def addTextChoices(listTextChoice):
    strXML = """     <void property="textChoices">\n"""
    for textChoice in listTextChoice:
        strXML += """      <void method="add">\n"""
        strXML += """       <object class="org.dawb.passerelle.actors.ui.config.StringValueBean">\n"""
        strXML += """        <void property="textValue">\n"""
        strXML += """         <string>{0}</string>\n""".format(textChoice)
        strXML += """        </void>\n"""
        strXML += """       </object>\n"""
        strXML += """      </void>\n"""
    strXML += """     </void>\n"""
    return strXML
