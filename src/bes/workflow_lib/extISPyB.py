"""
Workflow library module for connecting to extISPyB
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2016"
__updated__ = "2016-07-21"

import os
import json
import datetime
import requests

SPACE_GROUP_NAMES = {
    1: " P 1 ",
    2: " P -1 ",
    3: " P 1 2 1 ",
    4: " P 1 21 1 ",
    5: " C 1 2 1 ",
    6: " P 1 m 1 ",
    7: " P 1 c 1 ",
    8: " C 1 m 1 ",
    9: " C 1 c 1 ",
    10: " P 1 2/m 1 ",
    11: " P 1 21/m 1 ",
    12: " C 1 2/m 1 ",
    13: " P 1 2/c 1 ",
    14: " P 1 21/c 1 ",
    15: " C 1 2/c 1 ",
    16: " P 2 2 2 ",
    17: " P 2 2 21 ",
    18: " P 21 21 2 ",
    19: " P 21 21 21 ",
    20: " C 2 2 21 ",
    21: " C 2 2 2 ",
    22: " F 2 2 2 ",
    23: " I 2 2 2 ",
    24: " I 21 21 21 ",
    25: " P m m 2 ",
    26: " P m c 21 ",
    27: " P c c 2 ",
    28: " P m a 2 ",
    29: " P c a 21 ",
    30: " P n c 2 ",
    31: " P m n 21 ",
    32: " P b a 2 ",
    33: " P n a 21 ",
    34: " P n n 2 ",
    35: " C m m 2 ",
    36: " C m c 21 ",
    37: " C c c 2 ",
    38: " A m m 2 ",
    39: " A b m 2 ",
    40: " A m a 2 ",
    41: " A b a 2 ",
    42: " F m m 2 ",
    43: " F d d 2 ",
    44: " I m m 2 ",
    45: " I b a 2 ",
    46: " I m a 2 ",
    47: " P m m m ",
    48: " P n n n ",
    49: " P c c m ",
    50: " P b a n ",
    51: " P m m a1 ",
    52: " P n n a1 ",
    53: " P m n a1 ",
    54: " P c c a1 ",
    55: " P b a m1 ",
    56: " P c c n1 ",
    57: " P b c m1 ",
    58: " P n n m1 ",
    59: " P m m n1 ",
    60: " P b c n1 ",
    61: " P b c a1 ",
    62: " P n m a1 ",
    63: " C m c m1 ",
    64: " C m c a1 ",
    65: " C m m m ",
    66: " C c c m ",
    67: " C m m a ",
    68: " C c c a ",
    69: " F m m m ",
    70: " F d d d ",
    71: " I m m m ",
    72: " I b a m ",
    73: " I b c a1 ",
    74: " I m m a1 ",
    75: " P 4 ",
    76: " P 41 ",
    77: " P 42 ",
    78: " P 43 ",
    79: " I 4 ",
    80: " I 41 ",
    81: " P -4 ",
    82: " I -4 ",
    83: " P 4/m ",
    84: " P 42/m ",
    85: " P 4/n ",
    86: " P 42/n ",
    87: " I 4/m ",
    88: " I 41/a ",
    89: " P 4 2 2 ",
    90: " P 4 21 2 ",
    91: " P 41 2 2 ",
    92: " P 41 21 2 ",
    93: " P 42 2 2 ",
    94: " P 42 21 2 ",
    95: " P 43 2 2 ",
    96: " P 43 21 2 ",
    97: " I 4 2 2 ",
    98: " I 41 2 2 ",
    99: " P 4 m m ",
    100: " P 4 b m ",
    101: " P 42 c m ",
    102: " P 42 n m ",
    103: " P 4 c c ",
    104: " P 4 n c ",
    105: " P 42 m c ",
    106: " P 42 b c ",
    107: " I 4 m m ",
    108: " I 4 c m ",
    109: " I 41 m d ",
    110: " I 41 c d ",
    111: " P -4 2 m ",
    112: " P -4 2 c ",
    113: " P -4 21 m ",
    114: " P -4 21 c ",
    115: " P -4 m 2 ",
    116: " P -4 c 2 ",
    117: " P -4 b 2 ",
    118: " P -4 n 2 ",
    119: " I -4 m 2 ",
    120: " I -4 c 2 ",
    121: " I -4 2 m ",
    122: " I -4 2 d ",
    123: " P 4/m m m ",
    124: " P 4/m c c ",
    125: " P 4/n b m ",
    126: " P 4/n n c ",
    127: " P 4/m b m1 ",
    128: " P 4/m n c1 ",
    129: " P 4/n m m1 ",
    130: " P 4/n c c1 ",
    131: " P 42/m m c ",
    132: " P 42/m c m ",
    133: " P 42/n b c ",
    134: " P 42/n n m ",
    135: " P 42/m b c ",
    136: " P 42/m n m ",
    137: " P 42/n m c ",
    138: " P 42/n c m ",
    139: " I 4/m m m ",
    140: " I 4/m c m ",
    141: " I 41/a m d ",
    142: " I 41/a c d ",
    143: " P 3 ",
    144: " P 31 ",
    145: " P 32 ",
    146: " H 3 ",
    147: " P -3 ",
    148: " H -3 ",
    149: " P 3 1 2 ",
    150: " P 3 2 1 ",
    151: " P 31 1 2 ",
    152: " P 31 2 1 ",
    153: " P 32 1 2 ",
    154: " P 32 2 1 ",
    155: " H 3 2 ",
    156: " P 3 m 1 ",
    157: " P 3 1 m ",
    158: " P 3 c 1 ",
    159: " P 3 1 c ",
    160: " H 3 m ",
    161: " H 3 c ",
    162: " P -3 1 m ",
    163: " P -3 1 c ",
    164: " P -3 m 1 ",
    165: " P -3 c 1 ",
    166: " H -3 m ",
    167: " H -3 c ",
    168: " P 6 ",
    169: " P 61 ",
    170: " P 65 ",
    171: " P 62 ",
    172: " P 64 ",
    173: " P 63 ",
    174: " P -6 ",
    175: " P 6/m ",
    176: " P 63/m ",
    177: " P 6 2 2 ",
    178: " P 61 2 2 ",
    179: " P 65 2 2 ",
    180: " P 62 2 2 ",
    181: " P 64 2 2 ",
    182: " P 63 2 2 ",
    183: " P 6 m m ",
    184: " P 6 c c ",
    185: " P 63 c m ",
    186: " P 63 m c ",
    187: " P -6 m 2 ",
    188: " P -6 c 2 ",
    189: " P -6 2 m ",
    190: " P -6 2 c ",
    191: " P 6/m m m ",
    192: " P 6/m c c ",
    193: " P 63/m c m ",
    194: " P 63/m m c ",
    195: " P 2 3 ",
    196: " F 2 3 ",
    197: " I 2 3 ",
    198: " P 21 3 ",
    199: " I 21 3 ",
    200: " P m -3 ",
    201: " P n -3 ",
    202: " F m -3 ",
    203: " F d -3 ",
    204: " I m -3 ",
    205: " P a -31 ",
    206: " I a -31 ",
    207: " P 4 3 2 ",
    208: " P 42 3 2 ",
    209: " F 4 3 2 ",
    210: " F 41 3 2 ",
    211: " I 4 3 2 ",
    212: " P 43 3 2 ",
    213: " P 41 3 2 ",
    214: " I 41 3 2 ",
    215: " P -4 3 m ",
    216: " F -4 3 m ",
    217: " I -4 3 m ",
    218: " P -4 3 n ",
    219: " F -4 3 c ",
    220: " I -4 3 d ",
    221: " P m -3 m ",
    222: " P n -3 n ",
    223: " P m -3 n1 ",
    224: " P n -3 m1 ",
    225: " F m -3 m ",
    226: " F m -3 c ",
    227: " F d -3 m1 ",
    228: " F d -3 c1 ",
    229: " I m -3 m ",
    230: " I a -3 d1 ",
}


SPACE_GROUP_NUMBERS = {
    "P1": 1,
    "P-1": 2,
    "P121": 3,
    "P1211": 4,
    "C121": 5,
    "P1M1": 6,
    "P1C1": 7,
    "C1M1": 8,
    "C1C1": 9,
    "P12/M1": 10,
    "P121/M1": 11,
    "C12/M1": 12,
    "P12/C1": 13,
    "P121/C1": 14,
    "C12/C1": 15,
    "P222": 16,
    "P2221": 17,
    "P21212": 18,
    "P212121": 19,
    "C2221": 20,
    "C222": 21,
    "F222": 22,
    "I222": 23,
    "I212121": 24,
    "PMM2": 25,
    "PMC21": 26,
    "PCC2": 27,
    "PMA2": 28,
    "PCA21": 29,
    "PNC2": 30,
    "PMN21": 31,
    "PBA2": 32,
    "PNA21": 33,
    "PNN2": 34,
    "CMM2": 35,
    "CMC21": 36,
    "CCC2": 37,
    "AMM2": 38,
    "ABM2": 39,
    "AMA2": 40,
    "ABA2": 41,
    "FMM2": 42,
    "FDD2": 43,
    "IMM2": 44,
    "IBA2": 45,
    "IMA2": 46,
    "PMMM": 47,
    "PNNN": 48,
    "PCCM": 49,
    "PBAN": 50,
    "PMMA1": 51,
    "PNNA1": 52,
    "PMNA1": 53,
    "PCCA1": 54,
    "PBAM1": 55,
    "PCCN1": 56,
    "PBCM1": 57,
    "PNNM1": 58,
    "PMMN1": 59,
    "PBCN1": 60,
    "PBCA1": 61,
    "PNMA1": 62,
    "CMCM1": 63,
    "CMCA1": 64,
    "CMMM": 65,
    "CCCM": 66,
    "CMMA": 67,
    "CCCA": 68,
    "FMMM": 69,
    "FDDD": 70,
    "IMMM": 71,
    "IBAM": 72,
    "IBCA1": 73,
    "IMMA1": 74,
    "P4": 75,
    "P41": 76,
    "P42": 77,
    "P43": 78,
    "I4": 79,
    "I41": 80,
    "P-4": 81,
    "I-4": 82,
    "P4/M": 83,
    "P42/M": 84,
    "P4/N": 85,
    "P42/N": 86,
    "I4/M": 87,
    "I41/A": 88,
    "P422": 89,
    "P4212": 90,
    "P4122": 91,
    "P41212": 92,
    "P4222": 93,
    "P42212": 94,
    "P4322": 95,
    "P43212": 96,
    "I422": 97,
    "I4122": 98,
    "P4MM": 99,
    "P4BM": 100,
    "P42CM": 101,
    "P42NM": 102,
    "P4CC": 103,
    "P4NC": 104,
    "P42MC": 105,
    "P42BC": 106,
    "I4MM": 107,
    "I4CM": 108,
    "I41MD": 109,
    "I41CD": 110,
    "P-42M": 111,
    "P-42C": 112,
    "P-421M": 113,
    "P-421C": 114,
    "P-4M2": 115,
    "P-4C2": 116,
    "P-4B2": 117,
    "P-4N2": 118,
    "I-4M2": 119,
    "I-4C2": 120,
    "I-42M": 121,
    "I-42D": 122,
    "P4/MMM": 123,
    "P4/MCC": 124,
    "P4/NBM": 125,
    "P4/NNC": 126,
    "P4/MBM1": 127,
    "P4/MNC1": 128,
    "P4/NMM1": 129,
    "P4/NCC1": 130,
    "P42/MMC": 131,
    "P42/MCM": 132,
    "P42/NBC": 133,
    "P42/NNM": 134,
    "P42/MBC": 135,
    "P42/MNM": 136,
    "P42/NMC": 137,
    "P42/NCM": 138,
    "I4/MMM": 139,
    "I4/MCM": 140,
    "I41/AMD": 141,
    "I41/ACD": 142,
    "P3": 143,
    "P31": 144,
    "P32": 145,
    "H3": 146,
    "P-3": 147,
    "H-3": 148,
    "P312": 149,
    "P321": 150,
    "P3112": 151,
    "P3121": 152,
    "P3212": 153,
    "P3221": 154,
    "H32": 155,
    "P3M1": 156,
    "P31M": 157,
    "P3C1": 158,
    "P31C": 159,
    "H3M": 160,
    "H3C": 161,
    "P-31M": 162,
    "P-31C": 163,
    "P-3M1": 164,
    "P-3C1": 165,
    "H-3M": 166,
    "H-3C": 167,
    "P6": 168,
    "P61": 169,
    "P65": 170,
    "P62": 171,
    "P64": 172,
    "P63": 173,
    "P-6": 174,
    "P6/M": 175,
    "P63/M": 176,
    "P622": 177,
    "P6122": 178,
    "P6522": 179,
    "P6222": 180,
    "P6422": 181,
    "P6322": 182,
    "P6MM": 183,
    "P6CC": 184,
    "P63CM": 185,
    "P63MC": 186,
    "P-6M2": 187,
    "P-6C2": 188,
    "P-62M": 189,
    "P-62C": 190,
    "P6/MMM": 191,
    "P6/MCC": 192,
    "P63/MCM": 193,
    "P63/MMC": 194,
    "P23": 195,
    "F23": 196,
    "I23": 197,
    "P213": 198,
    "I213": 199,
    "PM-3": 200,
    "PN-3": 201,
    "FM-3": 202,
    "FD-3": 203,
    "IM-3": 204,
    "PA-31": 205,
    "IA-31": 206,
    "P432": 207,
    "P4232": 208,
    "F432": 209,
    "F4132": 210,
    "I432": 211,
    "P4332": 212,
    "P4132": 213,
    "I4132": 214,
    "P-43M": 215,
    "F-43M": 216,
    "I-43M": 217,
    "P-43N": 218,
    "F-43C": 219,
    "I-43D": 220,
    "PM-3M": 221,
    "PN-3N": 222,
    "PM-3N1": 223,
    "PN-3M1": 224,
    "FM-3M": 225,
    "FM-3C": 226,
    "FD-3M1": 227,
    "FD-3C1": 228,
    "IM-3M": 229,
    "IA-3D1": 230,
}


def setStatus(projectId, urlExtISPyB, status, runId, token):
    url = os.path.join(
        urlExtISPyB, token, "project", projectId, "run", runId, "status", status, "save"
    )
    _ = requests.post(url)


def setFinished(projectId, urlExtISPyB, runId, token):
    setStatus(projectId, urlExtISPyB, "FINISHED", runId, token)


def setError(projectId, urlExtISPyB, runId, token):
    setStatus(projectId, urlExtISPyB, "ERROR", runId, token)


def setStarted(projectId, urlExtISPyB, runId, token):
    setStatus(projectId, urlExtISPyB, "STARTED", runId, token)


def uploadFile(urlExtISPyB, filePath, fileType):
    if os.path.isfile(filePath):
        url = os.path.join(
            urlExtISPyB, "file", "filename", os.path.basename(filePath), "upload"
        )
        files = {"file": open(filePath)}
        r = requests.post(url, files=files)
        if r.status_code == 200:
            return {
                "targetId": r.text,
                "name": os.path.basename(filePath),
                "value": os.path.basename(filePath),
                "type": fileType,
            }
    return None


def uploadResult(
    name,
    version,
    urlExtISPyB,
    startDate,
    projectId,
    runId,
    fileList,
    extISPyBStatus,
    token,
):
    ids = []
    for fileType, filePath in fileList:
        ids.append(uploadFile(urlExtISPyB, filePath, fileType))

    url = os.path.join(
        urlExtISPyB, token, "project", projectId, "run", runId, "job", "add"
    )
    params = {
        "name": name,
        "startDate": startDate,
        "endDate": datetime.datetime.now(),
        "version": version,
        "output": json.dumps(ids),
        "status": extISPyBStatus,
    }

    r = requests.post(url, data=params)

    if r.status_code == 200 and extISPyBStatus == "FINISHED":
        setFinished(projectId, urlExtISPyB, runId, token)
    else:
        setError(projectId, urlExtISPyB, runId, token)


def getFromURL(url):
    listResponse = None
    if "http_proxy" in os.environ:
        os.environ["http_proxy"] = ""
    response = requests.get(url)
    if response.status_code == 200:
        listResponse = json.loads(response.text)
    return listResponse


def getListDataCollection(urlExtISPyB, token, proposal, workflowId):
    ispybWebServiceURL = os.path.join(
        urlExtISPyB,
        token,
        "proposal",
        str(proposal),
        "mx",
        "datacollection",
        "workflow",
        str(workflowId),
        "list",
    )
    listDataCollections = getFromURL(ispybWebServiceURL)
    return listDataCollections


def getListAutoprocIntegration(urlExtISPyB, token, proposal, dataCollectionId):
    ispybWebServiceURL = os.path.join(
        urlExtISPyB,
        token,
        "proposal",
        str(proposal),
        "mx",
        "autoprocintegration",
        "datacollection",
        str(dataCollectionId),
        "view",
    )
    listAutoprocIntegration = getFromURL(ispybWebServiceURL)[0]
    return listAutoprocIntegration


def findHighestSymmetrySpaceGroup(listSpaceGroups):
    highestSymmetrySpaceGroup = None
    # Translate into space group numbers
    listSpaceGroupNumbers = []
    for spaceGroup in listSpaceGroups:
        #        print([spaceGroup])
        for spaceGroupNumber in SPACE_GROUP_NAMES:
            #            print(spaceGroupNumber)
            if spaceGroup in SPACE_GROUP_NAMES[spaceGroupNumber]:
                listSpaceGroupNumbers.append(spaceGroupNumber)
                break
    if len(listSpaceGroupNumbers) > 0:
        highestSymmetrySpaceGroup = SPACE_GROUP_NAMES[
            max(listSpaceGroupNumbers)
        ].strip()
    return highestSymmetrySpaceGroup


def selectBestAutoprocIntegration(listAutoprocIntegration):
    bestAutoprocIntegration = None
    # Find all space groups
    listSpaceGroups = []
    for autoprocIntegration in listAutoprocIntegration:
        if (
            "v_datacollection_summary_phasing_autoproc_space_group"
            in autoprocIntegration
        ):
            if (
                autoprocIntegration[
                    "v_datacollection_summary_phasing_autoproc_space_group"
                ]
                is not None
            ):
                listSpaceGroups.append(
                    autoprocIntegration[
                        "v_datacollection_summary_phasing_autoproc_space_group"
                    ]
                )
    # Find highest symmetry space group
    highestSymmetrySpaceGroup = findHighestSymmetrySpaceGroup(listSpaceGroups)
    if highestSymmetrySpaceGroup is not None:
        # Select corresponding auto proc results
        listHighestSymmetryAutoproc = []
        for autoprocIntegration in listAutoprocIntegration:
            if (
                "v_datacollection_summary_phasing_autoproc_space_group"
                in autoprocIntegration
            ):
                spaceGroup = autoprocIntegration[
                    "v_datacollection_summary_phasing_autoproc_space_group"
                ]
                if spaceGroup is not None and highestSymmetrySpaceGroup in spaceGroup:
                    listHighestSymmetryAutoproc.append(autoprocIntegration)
        dictCompleteness = {"innerShell": [], "outerShell": [], "overall": []}
        dictMeanIOverSigI = {"innerShell": [], "outerShell": [], "overall": []}
        dictMultiplicity = {"innerShell": [], "outerShell": [], "overall": []}
        dictRMerge = {"innerShell": [], "outerShell": [], "overall": []}
        dictResolutionLimitHigh = {"innerShell": [], "outerShell": [], "overall": []}
        listProgram = []
        for autoproc in listHighestSymmetryAutoproc:
            #            pprint.pprint(autoproc)
            # For the moment only non-anom:
            if "0" in autoproc["anomalous"]:
                # Statistics type
                listScalingStatisticsType = autoproc["scalingStatisticsType"].split(",")
                listCompleteness = autoproc["completeness"].split(",")
                listMeanIOverSigI = autoproc["meanIOverSigI"].split(",")
                listMultiplicity = autoproc["multiplicity"].split(",")
                listRMerge = autoproc["rMerge"].split(",")
                listResolutionLimitHigh = autoproc["resolutionLimitHigh"].split(",")
                listProgram.append(autoproc["v_datacollection_processingPrograms"])
                for index in range(len(listScalingStatisticsType)):
                    scalingStatisticsType = listScalingStatisticsType[index]
                    dictCompleteness[scalingStatisticsType].append(
                        listCompleteness[index]
                    )
                    dictMeanIOverSigI[scalingStatisticsType].append(
                        listMeanIOverSigI[index]
                    )
                    dictMultiplicity[scalingStatisticsType].append(
                        listMultiplicity[index]
                    )
                    dictRMerge[scalingStatisticsType].append(listRMerge[index])
                    dictResolutionLimitHigh[scalingStatisticsType].append(
                        listResolutionLimitHigh[index]
                    )
        #        print(listProgram)
        #        print(dictCompleteness)
        #        print(dictMeanIOverSigI)
        #        print(dictMultiplicity)
        #        print(dictRMerge)
        #        print(dictResolutionLimitHigh)
        # Choose the first result being not fastproc
        for index in range(len(listProgram)):
            if "fastproc" not in listProgram[index]:
                break
        else:
            index = 0
        bestAutoprocIntegration = {
            "program": listProgram[index],
            "completeness": dictCompleteness["overall"][index],
            "meanIOverSigI": dictMeanIOverSigI["overall"][index],
            "multiplicity": dictMultiplicity["overall"][index],
            "rMerge": dictRMerge["overall"][index],
            "resolutionLimitHigh": dictResolutionLimitHigh["overall"][index],
        }

    return bestAutoprocIntegration


def getProcessResults(urlExtISPyB, token, proposal, workflowId):
    listDataCollections = getListDataCollection(
        urlExtISPyB, token, proposal, workflowId
    )
    for dataCollection in listDataCollections:
        if "rankingResolution" in dataCollection:
            rankingResolution = dataCollection["rankingResolution"]
        elif "xtalSnapshotFullPath1" in dataCollection:
            dataCollectionId = dataCollection["dataCollectionId"]
    print(rankingResolution, dataCollectionId)
    listAutoprocIntegration = getListAutoprocIntegration(
        urlExtISPyB, token, proposal, dataCollectionId
    )
    bestAutoprocIntegration = selectBestAutoprocIntegration(listAutoprocIntegration)
    bestAutoprocIntegration["rankingResolution"] = rankingResolution
    return bestAutoprocIntegration
