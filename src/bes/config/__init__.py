import pathlib
import yaml
from typing import Any
from functools import lru_cache

DEFAULT_VALUE_NOT_DEFINED = "default_value_not_defined"


def get_value(
    beamline: str,
    section_name: str,
    key_name: str,
    default_value=DEFAULT_VALUE_NOT_DEFINED,
) -> Any:
    """Retrieve a value from the BES configuration for a beamline."""
    config = _load_config(beamline)
    section = config.get(section_name, dict())
    if key_name not in section:
        return default_value
    return section.get(key_name, default_value)


@lru_cache(maxsize=None)
def _load_config(beamline: str) -> dict:
    """Load and cache the YAML configuration for a specific beamline."""
    config_path = pathlib.Path(__file__).parent / f"{beamline}.yaml".lower()
    with open(config_path, "r") as f:
        return yaml.safe_load(f)
