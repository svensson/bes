#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "15/09/2023"

import os
import requests

from bes.workflow_lib import mxnice

from ewokscore import Task


class SlurmSubmit(
    Task,
    input_names=["pipeline_name", "slurm_params"],
    output_names=[
        "slurm_params",
    ],
):

    def run(self):
        pipeline_name = self.inputs.pipeline_name
        slurm_params = self.inputs.slurm_params
        icat_callback_url = slurm_params["icat_callback_url"]
        environ = slurm_params.get("environ", None)
        script_file_path = slurm_params["script_file_path"]
        command_line = ""
        if environ is not None:
            for key, value in environ.items():
                command_line += f"export {key}={value}; "
        command_line += script_file_path
        working_directory = os.path.dirname(script_file_path)
        slurm_params["working_directory"] = working_directory
        slurm_script_path, slurm_id, stdout, stderr = mxnice.submitJobToSLURM(
            command_line=command_line,
            working_directory=working_directory,
            queue=slurm_params["queue"],
            mem=slurm_params["mem"],
            nodes=slurm_params["nodes"],
            core=slurm_params["core"],
            time=slurm_params["time"],
            name=pipeline_name,
        )

        requests.put(
            icat_callback_url,
            json={
                "step": {"name": pipeline_name, "status": "SUBMITTED"},
            },
        )

        requests.put(
            icat_callback_url, json={"logs": {"message": f"Slurm id: {slurm_id}"}}
        )

        slurm_params["script_path"] = slurm_script_path
        slurm_params["slurm_id"] = slurm_id
        slurm_params["pipeline_name"] = pipeline_name

        self.outputs.slurm_params = slurm_params
