#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "15/09/2023"

import os
import time

import requests
from ewokscore import Task


class WaitForStarted(
    Task,
    input_names=["slurm_params"],
    output_names=[
        "slurm_params",
    ],
):

    def run(self):
        started = False
        slurm_params = self.inputs.slurm_params
        working_directory = slurm_params["working_directory"]
        slurm_id = slurm_params["slurm_id"]
        if "grenades" in working_directory:

            started = True

        elif (
            working_directory is not None
            and slurm_id is not None
            and os.path.exists(working_directory)
        ):

            startedFile = os.path.join(working_directory, "STARTED")

            while not os.path.exists(startedFile):
                time.sleep(1)
                fd = os.open(working_directory, os.O_DIRECTORY)
                _ = os.fstat(fd)
                os.close(fd)

            started = True
        requests.put(
            slurm_params["icat_callback_url"],
            json={"step": {"name": slurm_params["pipeline_name"], "status": "STARTED"}},
        )
        slurm_params["started"] = started
        self.outputs.slurm_params = slurm_params
