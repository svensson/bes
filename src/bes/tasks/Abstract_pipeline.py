#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "15/09/2023"

import os
import string
import pathlib

from ewokscore import Task
from bes.workflow_lib import mxnice

MODULE_PATH = os.path.abspath(__file__)
MODULE_DIR = os.path.dirname(MODULE_PATH)


class Abstract_pipeline(
    Task,
    input_names=[],
    output_names=[],
):

    def create_icat_dir(self, metadata, pipeline_name):
        icat_dir = pathlib.Path(metadata["reprocess_path"]) / pipeline_name
        return icat_dir

    def create_edna_launch_script(self, metadata, pipeline_name, edna_plugin_name):
        icat_dir = self.create_icat_dir(metadata, pipeline_name)
        edna_base_dir = icat_dir / "nobackup"
        edna_base_dir.mkdir(parents=True, exist_ok=False, mode=0o755)
        input_file_path = edna_base_dir / f"{pipeline_name}_input.xml"
        input_xml = self.create_input_xml(metadata, pipeline_name)
        with open(input_file_path, "w") as f:
            f.write(input_xml)
        path_to_template = os.path.join(MODULE_DIR, "edna_script.template")
        with open(path_to_template) as f:
            scriptTemplate = f.read()
        template = string.Template(scriptTemplate)
        script = template.substitute(
            beamline=metadata["beamline"],
            proposal=metadata["proposal"],
            scriptDir=edna_base_dir,
            dataCollectionId=metadata["MX_dataCollectionId"],
            inputFile=input_file_path,
            ispybUserName=os.environ["ISPyB_user"],
            ispybPassword=os.environ["ISPyB_pass"],
            EDNA_SITE=os.environ["EDNA_SITE"],
            pluginName=edna_plugin_name,
            name=pipeline_name,
            residues=200,
            workingDir=edna_base_dir,
        )
        script_file_name = f"{pipeline_name}_launcher.sh"
        script_file_path = os.path.join(edna_base_dir, script_file_name)
        with open(script_file_path, "w") as f:
            f.write(script)
        os.chmod(script_file_path, 0o755)
        return script_file_path

    def submit_slurm_job(
        self,
        pipeline_name,
        script_file_path,
        queue="mx",
        mem=8000,
        nodes=1,
        core=8,
        time="2:00:00",
        environ=None,
    ):
        command_line = ""
        if environ is not None:
            for key, value in environ.items():
                command_line += f"export {key}={value}; "
        command_line += script_file_path
        working_directory = os.path.dirname(script_file_path)
        slurm_script_path, slurm_id, stdout, stderr = mxnice.submitJobToSLURM(
            command_line=command_line,
            working_directory=working_directory,
            queue=queue,
            mem=mem,
            nodes=nodes,
            core=core,
            time=time,
            name=pipeline_name,
        )
        return slurm_script_path, slurm_id, stdout, stderr
