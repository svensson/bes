#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "15/09/2023"

from XSDataCommon import XSDataFile
from XSDataCommon import XSDataString
from XSDataCommon import XSDataDouble
from XSDataCommon import XSDataBoolean
from XSDataCommon import XSDataInteger

from bes.tasks.Abstract_pipeline import Abstract_pipeline

from EDFactoryPluginStatic import EDFactoryPluginStatic

EDFactoryPluginStatic.loadModule("XSDataEDNAprocv1_0")
from XSDataEDNAprocv1_0 import XSDataEDNAprocInput  # noqa E402


class EDNA_proc_pipeline(
    Abstract_pipeline,
    input_names=["metadata", "EDNA_proc"],
    output_names=["pipeline_name", "slurm_params"],
):

    def create_input_xml(self, metadata, pipeline_name):
        xsDataEDNAprocInput = XSDataEDNAprocInput()
        xsDataEDNAprocInput.reprocess = XSDataBoolean(True)
        xsDataEDNAprocInput.data_collection_id = XSDataInteger(
            metadata["MX_dataCollectionId"]
        )
        xsDataEDNAprocInput.input_file = XSDataFile(
            XSDataString(metadata["xds_inp_path"])
        )
        icat_dir = self.create_icat_dir(metadata, pipeline_name)
        xsDataEDNAprocInput.icat_processed_data_dir = XSDataString(str(icat_dir))
        if (
            "forced_spacegroup" in metadata
            and metadata["forced_spacegroup"] is not None
        ):
            xsDataEDNAprocInput.spacegroup = XSDataString(metadata["forced_spacegroup"])
        if "anomalous" in metadata and metadata["anomalous"] is not None:
            xsDataEDNAprocInput.doAnom = XSDataBoolean(metadata["anomalous"])
        if "high_res_limit" in metadata and metadata["high_res_limit"] is not None:
            xsDataEDNAprocInput.res_override = XSDataDouble(metadata["high_res_limit"])
        if "start_image" in metadata and metadata["start_image"] is not None:
            xsDataEDNAprocInput.start_image = XSDataInteger(metadata["start_image"])
        if "end_image" in metadata and metadata["end_image"] is not None:
            xsDataEDNAprocInput.end_image = XSDataInteger(metadata["end_image"])
        return xsDataEDNAprocInput.marshal()

    def run(self):
        pipeline_name = "EDNA_proc"
        edna_plugin_name = "EDPluginControlEDNAprocv1_0"

        script_file_path = self.create_edna_launch_script(
            self.inputs.metadata, pipeline_name, edna_plugin_name
        )

        slurm_params = {
            "script_file_path": script_file_path,
            "queue": "nice",
            "mem": 8000,
            "nodes": 1,
            "core": 8,
            "time": "2:00:00",
            "icat_callback_url": self.inputs.metadata["icat_callback_url"],
            "no_pipelines": self.inputs.metadata["no_pipelines"],
        }
        self.outputs.pipeline_name = pipeline_name
        self.outputs.slurm_params = slurm_params
