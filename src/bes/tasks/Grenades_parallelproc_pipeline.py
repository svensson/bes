#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "15/09/2023"

import os
import pathlib
import requests

from ewokscore import Task


class Grenades_parallelproc_pipeline(
    Task,
    input_names=["metadata", "grenades_parallelproc"],
    output_names=["slurm_params"],
):
    def run(self):
        metadata = self.inputs.metadata
        icat_dir = pathlib.Path(metadata["reprocess_path"]) / "grenades_parallelproc"
        grenades_working_dir = icat_dir / "nobackup"
        grenades_working_dir.mkdir(parents=True, exist_ok=False, mode=0o755)
        # Read XDS.INP file
        xds_inp_path = pathlib.Path(metadata["xds_inp_path"])
        with open(xds_inp_path) as f:
            xds_inp = f.read()
        # Replace "../links" with "../../../links"
        xds_inp = xds_inp.replace("../links", "../../../links")
        new_xds_inp_path = grenades_working_dir / "XDS.INP"
        with open(new_xds_inp_path, "w") as f:
            f.write(xds_inp)
        command_line = "/opt/pxsoft/bin/xdsproc.pl"
        command_line += f" -path {str(grenades_working_dir)}"
        command_line += f" -datacollectID {metadata['MX_dataCollectionId']}"
        command_line += " -mode after"
        command_line += " -slurm"
        with open(str(grenades_working_dir / "command_line.txt"), "w") as f:
            f.write(command_line)
        os.system(command_line)
        slurm_params = {
            "pipeline_name": "grenades_parallelproc",
            "icat_callback_url": self.inputs.metadata["icat_callback_url"],
            "no_pipelines": self.inputs.metadata["no_pipelines"],
        }
        requests.put(
            slurm_params["icat_callback_url"],
            json={
                "step": {"name": slurm_params["pipeline_name"], "status": "FINISHED"}
            },
        )
        self.outputs.slurm_params = slurm_params
