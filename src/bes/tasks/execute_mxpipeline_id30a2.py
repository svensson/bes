import datetime
import requests

beamline = "id30a2"
workflow = "MXDataReductionPipelines_ID30A-2"
pre_script = "module load bes_id30a2; basename /tmp/test"

payload = {
    "execute_arguments": {
        "engine": "ppf",
        "startargs": {
            "callback": "http://lalex.esrf.fr:8000/ewoks/27f84266-2676-47b5-9c02-fcdd91ba086c/jobs?id=651c22078c1fa7654e0a7def",
            "mx_pipeline_name": [
                "EDNA_proc",
                # "autoPROC",
                "XIA2_DIALS",
                # "grenades_fastproc",
                # "grenades_parallelproc",
            ],
            "raw_data_path": ["/data/visitor/mx2112/id30a2/20230922/RAW_DATA/ww/ww-1/"],
            "datasets": [
                {
                    "id": 125066392,
                    "name": "iPfPRS-CD038256_H12-2_2",
                    "startDate": "2023-08-31T15:03:53.747+02:00",
                    "endDate": "2023-08-31T15:03:53.747+02:00",
                    "location": "/tmp/ingestion/MX2405/RAW_DATA/iPfPRS/iPfPRS-CD038256_H12-2_2/DATACOLLECTION-iPfPRS-CD038256_H12-2_2-3023144",
                }
            ],
        },
    },
    "worker_options": {
        "queue": beamline,
    },
}

response = requests.post(
    f"http://ewoksserver1:5000/execute/{workflow}", json=payload, timeout=5
)
response.raise_for_status()

ct = datetime.datetime.now()
print(ct)
print(response.json())
