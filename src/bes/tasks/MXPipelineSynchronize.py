#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "12/10/2023"

import json

import requests
from ewokscore import Task


class MXPipelineSynchronize(
    Task,
    input_names=["slurm_params"],
    optional_input_names=[],
    output_names=["slurm_params"],
):
    def run(self):
        slurm_params = self.inputs.slurm_params
        response = requests.get(slurm_params["icat_callback_url"])
        json_message = response.text

        dict_message = json.loads(json_message)

        job_id = self.job_id

        no_finished = 0
        for dict_investigation in dict_message:
            if "jobId" in dict_investigation:
                if dict_investigation["jobId"] == job_id:
                    for dict_step in dict_investigation["steps"]:
                        if dict_step["status"] == "FINISHED":
                            no_finished += 1

        if self.inputs.slurm_params["no_pipelines"] == no_finished:
            requests.put(slurm_params["icat_callback_url"], json={"status": "FINISHED"})
            slurm_params["workflow_status"] = "FINISHED"

        self.outputs.slurm_params = slurm_params
