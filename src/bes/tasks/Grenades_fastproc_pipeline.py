#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "15/09/2023"

import pathlib

from ewokscore import Task

SPACE_GROUP_NUMBERS = {
    "P1": 1,
    "P-1": 2,
    "P121": 3,
    "P1211": 4,
    "C121": 5,
    "P1M1": 6,
    "P1C1": 7,
    "C1M1": 8,
    "C1C1": 9,
    "P12/M1": 10,
    "P121/M1": 11,
    "C12/M1": 12,
    "P12/C1": 13,
    "P121/C1": 14,
    "C12/C1": 15,
    "P222": 16,
    "P2221": 17,
    "P21212": 18,
    "P212121": 19,
    "C2221": 20,
    "C222": 21,
    "F222": 22,
    "I222": 23,
    "I212121": 24,
    "PMM2": 25,
    "PMC21": 26,
    "PCC2": 27,
    "PMA2": 28,
    "PCA21": 29,
    "PNC2": 30,
    "PMN21": 31,
    "PBA2": 32,
    "PNA21": 33,
    "PNN2": 34,
    "CMM2": 35,
    "CMC21": 36,
    "CCC2": 37,
    "AMM2": 38,
    "ABM2": 39,
    "AMA2": 40,
    "ABA2": 41,
    "FMM2": 42,
    "FDD2": 43,
    "IMM2": 44,
    "IBA2": 45,
    "IMA2": 46,
    "PMMM": 47,
    "PNNN": 48,
    "PCCM": 49,
    "PBAN": 50,
    "PMMA1": 51,
    "PNNA1": 52,
    "PMNA1": 53,
    "PCCA1": 54,
    "PBAM1": 55,
    "PCCN1": 56,
    "PBCM1": 57,
    "PNNM1": 58,
    "PMMN1": 59,
    "PBCN1": 60,
    "PBCA1": 61,
    "PNMA1": 62,
    "CMCM1": 63,
    "CMCA1": 64,
    "CMMM": 65,
    "CCCM": 66,
    "CMMA": 67,
    "CCCA": 68,
    "FMMM": 69,
    "FDDD": 70,
    "IMMM": 71,
    "IBAM": 72,
    "IBCA1": 73,
    "IMMA1": 74,
    "P4": 75,
    "P41": 76,
    "P42": 77,
    "P43": 78,
    "I4": 79,
    "I41": 80,
    "P-4": 81,
    "I-4": 82,
    "P4/M": 83,
    "P42/M": 84,
    "P4/N": 85,
    "P42/N": 86,
    "I4/M": 87,
    "I41/A": 88,
    "P422": 89,
    "P4212": 90,
    "P4122": 91,
    "P41212": 92,
    "P4222": 93,
    "P42212": 94,
    "P4322": 95,
    "P43212": 96,
    "I422": 97,
    "I4122": 98,
    "P4MM": 99,
    "P4BM": 100,
    "P42CM": 101,
    "P42NM": 102,
    "P4CC": 103,
    "P4NC": 104,
    "P42MC": 105,
    "P42BC": 106,
    "I4MM": 107,
    "I4CM": 108,
    "I41MD": 109,
    "I41CD": 110,
    "P-42M": 111,
    "P-42C": 112,
    "P-421M": 113,
    "P-421C": 114,
    "P-4M2": 115,
    "P-4C2": 116,
    "P-4B2": 117,
    "P-4N2": 118,
    "I-4M2": 119,
    "I-4C2": 120,
    "I-42M": 121,
    "I-42D": 122,
    "P4/MMM": 123,
    "P4/MCC": 124,
    "P4/NBM": 125,
    "P4/NNC": 126,
    "P4/MBM1": 127,
    "P4/MNC1": 128,
    "P4/NMM1": 129,
    "P4/NCC1": 130,
    "P42/MMC": 131,
    "P42/MCM": 132,
    "P42/NBC": 133,
    "P42/NNM": 134,
    "P42/MBC": 135,
    "P42/MNM": 136,
    "P42/NMC": 137,
    "P42/NCM": 138,
    "I4/MMM": 139,
    "I4/MCM": 140,
    "I41/AMD": 141,
    "I41/ACD": 142,
    "P3": 143,
    "P31": 144,
    "P32": 145,
    "H3": 146,
    "P-3": 147,
    "H-3": 148,
    "P312": 149,
    "P321": 150,
    "P3112": 151,
    "P3121": 152,
    "P3212": 153,
    "P3221": 154,
    "H32": 155,
    "P3M1": 156,
    "P31M": 157,
    "P3C1": 158,
    "P31C": 159,
    "H3M": 160,
    "H3C": 161,
    "P-31M": 162,
    "P-31C": 163,
    "P-3M1": 164,
    "P-3C1": 165,
    "H-3M": 166,
    "H-3C": 167,
    "P6": 168,
    "P61": 169,
    "P65": 170,
    "P62": 171,
    "P64": 172,
    "P63": 173,
    "P-6": 174,
    "P6/M": 175,
    "P63/M": 176,
    "P622": 177,
    "P6122": 178,
    "P6522": 179,
    "P6222": 180,
    "P6422": 181,
    "P6322": 182,
    "P6MM": 183,
    "P6CC": 184,
    "P63CM": 185,
    "P63MC": 186,
    "P-6M2": 187,
    "P-6C2": 188,
    "P-62M": 189,
    "P-62C": 190,
    "P6/MMM": 191,
    "P6/MCC": 192,
    "P63/MCM": 193,
    "P63/MMC": 194,
    "P23": 195,
    "F23": 196,
    "I23": 197,
    "P213": 198,
    "I213": 199,
    "PM-3": 200,
    "PN-3": 201,
    "FM-3": 202,
    "FD-3": 203,
    "IM-3": 204,
    "PA-31": 205,
    "IA-31": 206,
    "P432": 207,
    "P4232": 208,
    "F432": 209,
    "F4132": 210,
    "I432": 211,
    "P4332": 212,
    "P4132": 213,
    "I4132": 214,
    "P-43M": 215,
    "F-43M": 216,
    "I-43M": 217,
    "P-43N": 218,
    "F-43C": 219,
    "I-43D": 220,
    "PM-3M": 221,
    "PN-3N": 222,
    "PM-3N1": 223,
    "PN-3M1": 224,
    "FM-3M": 225,
    "FM-3C": 226,
    "FD-3M1": 227,
    "FD-3C1": 228,
    "IM-3M": 229,
    "IA-3D1": 230,
}


class Grenades_fastproc_pipeline(
    Task,
    input_names=["metadata", "grenades_fastproc"],
    output_names=["pipeline_name", "slurm_params"],
):
    def run(self):
        pipeline_name = "grenades_fastproc"
        metadata = self.inputs.metadata
        icat_dir = pathlib.Path(metadata["reprocess_path"]) / pipeline_name
        grenades_top_dir = icat_dir / "nobackup"
        grenades_top_dir.mkdir(parents=True, exist_ok=False, mode=0o755)
        grenades_working_dir = grenades_top_dir / pipeline_name
        grenades_working_dir.mkdir(parents=True, exist_ok=False, mode=0o755)
        # Read XDS.INP file
        xds_inp_path = pathlib.Path(metadata["xds_inp_path"])
        with open(xds_inp_path) as f:
            list_xds_inp = f.readlines()
        space_group_number = None
        if (
            "forced_spacegroup" in metadata
            and metadata["forced_spacegroup"] is not None
        ):
            if metadata["forced_spacegroup"].strip() in SPACE_GROUP_NUMBERS:
                space_group_number = SPACE_GROUP_NUMBERS[
                    metadata["forced_spacegroup"].strip()
                ]
        for index, line in enumerate(list_xds_inp):
            if "NAME_TEMPLATE_OF_DATA_FRAMES=" in line:
                template = line.split("/")[-1]
                directory = metadata["MX_directory"]
                list_xds_inp[index] = (
                    f"NAME_TEMPLATE_OF_DATA_FRAMES={directory}/{template}\n"
                )
            if "SPACE_GROUP_NUMBER" in line and space_group_number is not None:
                list_xds_inp[index] = f"SPACE_GROUP_NUMBER={space_group_number}\n"
                space_group_number = None
        if space_group_number is not None:
            list_xds_inp.append(f"SPACE_GROUP_NUMBER={space_group_number}\n")
        new_xds_inp_path = grenades_working_dir / "XDS.INP"
        with open(new_xds_inp_path, "w") as f:
            f.write("".join(list_xds_inp))
        dcolid_path = grenades_working_dir / "DCOLID.txt"
        with open(dcolid_path, "w") as f:
            f.write(f"datacollectionID:{metadata['MX_dataCollectionId']}\n")
        command_line = "/opt/pxsoft/bin/xds_fullrun.pl"
        command_line += f" -datacollectID {metadata['MX_dataCollectionId']}"
        command_line += " -incremental"
        command_line += " -anom 1"
        command_line += " -slurm"
        command_line += "\n"
        script_file_path = grenades_top_dir / "command_line.txt"
        with open(script_file_path, "w") as f:
            f.write(f"rm -f {grenades_top_dir}/STARTED\n")
            f.write(f"rm -f {grenades_top_dir}/FINISHED\n")
            f.write(f"echo Started > {grenades_top_dir}/STARTED\n")
            f.write(f"cd {grenades_working_dir}\n")
            f.write(command_line)
            f.write(f"echo Finished > {grenades_top_dir}/FINISHED\n")
        slurm_params = {
            "script_file_path": str(script_file_path),
            "queue": "mx",
            "mem": 12000,
            "nodes": 1,
            "core": 20,
            "time": "2:00:00",
            "icat_callback_url": self.inputs.metadata["icat_callback_url"],
            "no_pipelines": self.inputs.metadata["no_pipelines"],
        }
        self.outputs.pipeline_name = pipeline_name
        self.outputs.slurm_params = slurm_params
