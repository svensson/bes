import os
import datetime
import requests

beamline = "id23"
workflow = "MXDataReductionPipelines_ID23-1"
pre_script = "module load bes_id23eh1"

slurm_parameters = {
    "partition": "nice",
    "time_limit": 360,  # minutes
    "current_working_directory": "/tmp_14_days/opid23",
    "environment": {
        "USER": "opid23",
        "EDNA_SITE": "ESRF_ID23EH1",
        "PATH": os.environ["PATH"],
    },
    # "environment": {"USER": "opid30"},
}

payload = {
    "execute_arguments": {
        "engine": "ppf",
        "startargs": {
            "mx_pipeline_name": [
                "EDNA_proc",
                # "autoPROC",
                # "XIA2_DIALS",
                # "grenades_fastproc",
                # "grenades_parallelproc",
            ],
            # "raw_data_path": ["/data/visitor/mx2112/id23eh1/20230919/RAW_DATA/Sample-2:1:03/run_02_MXPressE/run_02_05_datacollection"],
            "raw_data_path": [
                "/data/visitor/mx2112/id23eh1/20230919/RAW_DATA/Sample-2:1:04/run_02_MXPressE/run_02_05_datacollection"
            ],
            # "do_anom": True,
            "forced_spacegroup": "P1",
            # "start_image": 2,
            # "end_image": 48,
            # "high_res_limit": 2.0
        },
        "_slurm_spawn_arguments": {
            "parameters": slurm_parameters,
            "pre_script": pre_script,
        },
    },
    "worker_options": {
        "queue": beamline,
    },
}

response = requests.post(
    f"http://ewoksserver1:5000/execute/{workflow}", json=payload, timeout=5
)
response.raise_for_status()

ct = datetime.datetime.now()
print(ct)
print(response.json())
