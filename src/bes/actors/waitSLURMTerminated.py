# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["Olof Svensson"]
__license__ = "MIT"
__date__ = "30/11/2018"


import os
import time


def run(workingDirectory=None, slurmJobId=None, **_):

    success = False

    if "grenades" in workingDirectory:

        success = True

    elif (
        workingDirectory is not None
        and slurmJobId is not None
        and os.path.exists(workingDirectory)
    ):

        finishedFile = os.path.join(workingDirectory, "FINISHED")

        while not os.path.exists(finishedFile):
            time.sleep(1)
            fd = os.open(workingDirectory, os.O_DIRECTORY)
            _ = os.fstat(fd)
            os.close(fd)

        success = True

    return {"success": success}
