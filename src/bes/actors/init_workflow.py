#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

"""
Initialisation of a workflow
"""

import os
import pprint

from bes.workflow_lib import ispyb
from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes import config
from bes.workflow_lib import mongodb
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    prefix,
    workflow_title=None,
    workflow_type=None,
    workflow_id=None,
    workflowParameters={},
    sample_lims_id=None,
    sample_node_id=None,
    REFID=None,
    REQUESTID=None,
    token=None,
    besParameters=None,
    sample_name=None,
    **_,
):
    logger = None

    # Save directory as raw_directory
    raw_directory = directory
    run_number = 1
    dict_path = path.getDictPath(directory)
    proposal = dict_path["proposal"]

    if sample_node_id is not None:
        sample_node_id = int(sample_node_id)
    if sample_lims_id is not None:
        sample_lims_id = int(sample_lims_id)
    if workflowParameters == {}:
        workflowParameters = {
            "title": workflow_title,
            "type": workflow_type,
        }
    else:
        workflowParameters = common.jsonLoads(workflowParameters)

    workflowTypeUnderscore = workflow_type.replace(" ", "_")
    raw_directory, run_number = path.createUniqueICATDirectory(
        raw_directory=raw_directory,
        run_number=run_number,
        expTypePrefix=workflowTypeUnderscore,
    )
    prefix = prefix + f"_{run_number}"

    if not os.path.exists(raw_directory):
        os.makedirs(raw_directory, mode=0o755)

    # Create the working directory with respect to the image directory
    workflowWorkingDir = path.createWorkflowWorkingDirectory(raw_directory)

    workflowParameters["workingDir"] = workflowWorkingDir

    # Create stack trace file
    workflowStackTraceFile = path.createStackTraceFile(workflowWorkingDir)
    workflowParameters["stackTraceFile"] = workflowStackTraceFile

    workflowLogFile = path.createWorkflowLogFilePath(workflowWorkingDir)
    workflowDebugLogFile = path.createWorkflowDebugLogFilePath(workflowWorkingDir)

    workflowParameters["logFile"] = workflowLogFile
    workflowParameters["debugLogFile"] = workflowDebugLogFile
    workflowParameters["index"] = run_number
    workflowParameters["pyarchLogFile"] = None
    # workflowParameters["isSimulator"] = isSimulator

    # Update MongoDB database
    if besParameters:
        bes_request_id = besParameters["request_id"]
        mongodb.setMongoAttribute(
            bes_request_id=bes_request_id, attribute="logFile", value=workflowLogFile
        )
        mongodb.setMongoAttribute(
            bes_request_id=bes_request_id,
            attribute="debugLogFile",
            value=workflowDebugLogFile,
        )
    # Then set up log file
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    returnDict = {
        "workflow_working_dir": workflowWorkingDir,
        "workflowLogFile": workflowLogFile,
        "workflowDebugLogFile": workflowDebugLogFile,
        "directory": directory,
        "raw_directory": raw_directory,
        "proposal": proposal,
        "prefix": prefix,
        "workflow_index": run_number,
        "workflowParameters": workflowParameters,
    }

    # Try to create pyarchdir
    workflowPyarchLogFile = None
    pyarch_html_dir = None
    tmpPyarch = path.createPyarchFilePath(workflowWorkingDir)
    if tmpPyarch is not None:
        pyarchDir = tmpPyarch
        workflowPyarchLogFile = os.path.join(
            pyarchDir, os.path.basename(workflowLogFile)
        )
        pyarch_html_dir = os.path.join(pyarchDir, "html")
        workflowParameters["pyarchLogFile"] = workflowPyarchLogFile
        logger = workflow_logging.getLogger(beamline, workflowParameters)
        logger.debug("pyarchDir: %s" % pyarchDir)
        logger.debug("workflowPyarchLogFile: %s" % workflowPyarchLogFile)
        logger.debug("pyarch_html_dir: %s" % pyarch_html_dir)
        returnDict["pyarchDir"] = pyarchDir
        returnDict["workflowParameters"] = workflowParameters
        returnDict["pyarch_html_dir"] = pyarch_html_dir
        returnDict["workflowPyarchLogFile"] = workflowPyarchLogFile
    workflowParameters["workflowPyarchLogFile"] = workflowPyarchLogFile
    workflowParameters["pyarch_html_dir"] = pyarch_html_dir
    workflowParameters["workflowWorkingDir"] = workflowWorkingDir
    workflowParameters["workflowLogFile"] = workflowLogFile

    # Initialise log file
    logger.info("Starting new workflow '%s'" % workflow_title)
    # logger.debug(kwargs)
    if besParameters:
        REQUESTID = besParameters["request_id"]
    if REQUESTID is not None:
        logger.info(
            "Beamline Expert System REFID=%r, REQUESTID=%r" % (REFID, REQUESTID)
        )
    logger.info("Image directory: %s" % directory)
    if sample_node_id is not None:
        logger.info("Sample_node_id: %r" % sample_node_id)
    logger.info("Workflow type: %s" % workflow_type)
    # logger.info("Workflow index: %r" % workflowIndex)
    logger.info("Workflow working directory: %r" % workflowWorkingDir)
    logger.info("Prefix = %r" % prefix)

    # Try to retrieve sample info
    if sample_lims_id is not None and sample_lims_id > 0:
        dictSampleInfo = edna_ispyb.get_sample_information(
            beamline, sample_lims_id, workflowWorkingDir
        )
        logger.info(
            "Sample info obtained from ISPyB: %s" % pprint.pformat(dictSampleInfo)
        )
    else:
        dictSampleInfo = {}
        logger.info("No sample info could be obtained from ISPyB")

    # dictSampleInfo["forcedSpaceGroup"] = "P1"
    # logger.info(
    #     "Sample info forced: %s" % pprint.pformat(dictSampleInfo)
    # )
    # dictSampleInfo["preferredBeamDiameter"] = "30"
    # logger.info(
    #     "Sample info forced: %s" % pprint.pformat(dictSampleInfo)
    # )

    dictSampleInfo["limsId"] = sample_lims_id
    dictSampleInfo["nodeId"] = sample_node_id
    dictSampleInfo["groupNodeId"] = None
    returnDict["sampleInfo"] = dictSampleInfo

    # Sample name from ISPyB
    if "sampleName" in dictSampleInfo and "proteinAcronym" in dictSampleInfo:
        sample_name = (
            f"{dictSampleInfo['proteinAcronym']}-{dictSampleInfo['sampleName']}"
        )
    returnDict["sample_name"] = sample_name

    logger.debug("beamline: {0}".format(beamline))
    logger.debug("workflowTitle: {0}".format(workflowParameters["title"]))
    logger.debug("workflowType: {0}".format(workflowParameters["type"]))
    logger.debug(
        "workflowPyarchLogFile: {0}".format(workflowParameters["workflowPyarchLogFile"])
    )
    logger.debug("workflowLogFile: {0}".format(workflowParameters["workflowLogFile"]))
    logger.debug("pyarchHtmlDir: {0}".format(workflowParameters["pyarch_html_dir"]))
    logger.debug(
        "workflowWorkingDir: {0}".format(workflowParameters["workflowWorkingDir"])
    )

    if workflow_id is None:
        # Clear the MXCuBE ISPyBClient group_id
        try:
            collect.clearISPyBClientGroupId(beamline, token=token)
        except Exception:
            logger.warning("Cannot clear ISPyB group id in MXCuBE")
        # Initialise ispyb
        workflow_id = ispyb.storeOrUpdateWorkflow(
            beamline=beamline,
            workflowTitle=workflowParameters["title"],
            workflowType=workflowParameters["type"],
            workflowPyarchLogFile=workflowParameters["workflowPyarchLogFile"],
            workflowLogFile=workflowParameters["workflowLogFile"],
            pyarchHtmlDir=workflowParameters["pyarch_html_dir"],
            workflowWorkingDir=workflowParameters["workflowWorkingDir"],
        )

        if workflow_id == -1:
            error_message = "ISPyB workflow ID is -1!"
            logger.error(error_message)
            logger.error(
                "This means that no workflow meta-data can be stored in ISPyB."
            )
            # logger.error("Workflow aborted.")
            # raise RuntimeError(error_message)

    logger.info("ISPyB workflow id: %r" % workflow_id)
    workflowParameters["ispyb_id"] = workflow_id
    returnDict["workflow_id"] = workflow_id
    returnDict["workflowParameters"] = workflowParameters
    emailList = config.get_value(beamline, "Email", "workflowStarted", default_value=[])
    common.send_email(
        beamline,
        emailList,
        "Started",
        directory,
        workflow_title,
        workflow_type,
        workflowWorkingDir,
    )

    return returnDict
