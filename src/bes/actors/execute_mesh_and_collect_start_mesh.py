#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "25/06/2019"

import time

from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna2_tasks
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    workflow_index,
    workflow_type,
    grid_info,
    meshQueueListFile,
    motorPositions,
    directory,
    meshPositionFile,
    workflow_working_dir,
    group_node_id=None,
    sample_node_id=1,
    resolution=None,
    force_not_shutterless=False,
    token=None,
    highResolution=None,
    lowResolution=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    besParameters=None,
    **_,
):
    if besParameters:
        bes_request_id = besParameters["request_id"]
    else:
        bes_request_id = None

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    meshPositions = path.parseJsonFile(meshPositionFile)
    meshQueueList = path.parseJsonFile(meshQueueListFile)
    grid_info = common.jsonLoads(grid_info)

    logger.warning(
        "Starting online data processing, this can take up till one minute..."
    )
    edPlugin = edna2_tasks.storeImageQualityIndicatorsFromMeshPositionsStart(
        beamline,
        directory,
        meshPositions,
        workflow_working_dir,
        grid_info=grid_info,
        doIndexing=False,
        doDistlSignalStrength=True,
        doUploadToIspyb=False,
    )

    # Sleep 2 s in order to let thread start
    time.sleep(2)

    if force_not_shutterless:
        logger.info("Forcing non-shutterless data collection")
    else:
        logger.debug("Non-shutterless data collection not forced")

    dict_id = collect.executeQueueForMeshScan(
        beamline,
        workflow_index,
        workflow_type,
        meshQueueList,
        group_node_id,
        sample_node_id,
        resolution,
        motorPositions,
        force_not_shutterless=force_not_shutterless,
        highResolution=highResolution,
        lowResolution=lowResolution,
        token=token,
        workflow_name=workflowParameters["title"],
        bes_request_id=bes_request_id,
        characterisation_id=characterisation_id,
        kappa_settings_id=kappa_settings_id,
        position_id=position_id,
    )
    logger.warning("Data collection done, waiting for data processing to finish...")
    group_node_id = dict_id["group_node_id"]
    list_node_id = dict_id["listNodeId"]

    (
        meshPositions,
        inputDozor,
        dozorAllFile,
    ) = edna2_tasks.storeImageQualityIndicatorsFromMeshPositionsSynchronize(
        edPlugin, meshPositions
    )
    meshPositions = (meshPositions,)

    return {
        "group_node_id": group_node_id,
        "list_node_id": list_node_id,
        "meshPositions": meshPositions,
    }
