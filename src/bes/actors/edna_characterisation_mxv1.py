#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "01/03/2021"

import os
import shutil

from bes.workflow_lib import path
from bes.workflow_lib import icat
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging

from edna2.tasks.ControlIndexing import ControlIndexing


def run(
    beamline,
    proposal,
    mxv1StrategyResultFile,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    flux,
    beamSizeX,
    beamSizeY,
    workflow_type,
    workflow_working_dir,
    list_node_id,
    workflowParameters,
    sampleInfo=None,
    characterisationTransmission=None,
    resolution=None,
    pyarch_html_dir=None,
    strategyOption="-low never",
    anomalousData=None,
    crystalSizeX=None,
    crystalSizeY=None,
    crystalSizeZ=None,
    aimedCompleteness=None,
    aimedMultiplicity=None,
    sampleSusceptibility=None,
    minExposureTime=None,
    forcedSpaceGroup=None,
    minOscWidth=None,
    fineSlicedReferenceImages=False,
    aimedIOverSigmaAtHighestResolution=1.0,
    characterisationOscillationRange=None,
    do_data_collect=None,
    do_data_collect_orig=None,
    diffractionSignalDetection="Dozor (macro-molecules)",
    doseLimit=None,
    misalignedCrystalDetected=False,
    axisStart=0.0,
    axisRange=None,
    runGphlWorkflow=False,
    token=None,
    strategyType=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    simulated_characterisation_results=False,
    besParameters=None,
    **_,
):
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    cellSpaceGroup = {}
    spot_xds_path = None
    workflowParameters = common.jsonLoads(workflowParameters)
    sampleInfo = common.jsonLoads(sampleInfo)

    if besParameters:
        bes_request_id = besParameters["request_id"]
    else:
        bes_request_id = None

    if characterisation_id is None:
        characterisation_id = 1
    else:
        characterisation_id += 1

    # Enable logging in mxCuBE
    # os.environ["mxCuBE_XMLRPC_log"] = "Enabled"

    useDozor = "dozor" in diffractionSignalDetection.lower()
    run_number = int(run_number)
    flux = float(flux)
    beamSizeX = float(beamSizeX)
    beamSizeY = float(beamSizeY)
    if characterisationTransmission is not None:
        characterisationTransmission = float(characterisationTransmission)
    if resolution is not None:
        resolution = float(resolution)
    aimedIOverSigmaAtHighestResolution = float(aimedIOverSigmaAtHighestResolution)
    if do_data_collect_orig is not None:
        # Reset do_data_collect
        do_data_collect = do_data_collect_orig

    if characterisationOscillationRange is None:
        logger.warning("No characterisation oscillation range given as input!")
        logger.warning("Defaulting to 1 degree.")
        characterisationOscillationRange = 1.0

    mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)

    message_text = ""
    characterisationComment = None

    if fineSlicedReferenceImages:
        listImages, firstImagePath = edna_mxv1.getListFineSlicedReferenceImagePath(
            beamline=beamline,
            mxv1StrategyResult=mxv1StrategyResult,
            characterisationOscillationRange=characterisationOscillationRange,
            directory=directory,
            run_number=run_number,
            expTypePrefix=expTypePrefix,
            prefix=prefix,
            suffix=suffix,
        )
        indexing_input = {
            "fastCharacterisation": {
                "listSubWedgeAngles": [0, 90, 180, 270],
                "noImagesInSubWedge": 10,
                "firstImagePath": firstImagePath,
            },
            "workingDirectory": workflow_working_dir,
        }
        control_indexing = ControlIndexing(
            inData=indexing_input, workingDirectorySuffix="local_user_1"
        )
        control_indexing.execute()
        if control_indexing.isFailure():
            runGphlWorkflow = False
        out_data = control_indexing.outData
        if (
            out_data is not None
            and "resultIndexing" in out_data
            and out_data["resultIndexing"] is not None
            and "spaceGroupNumber" in out_data["resultIndexing"]
            and "spotXdsPath" in out_data
        ):
            spot_xds_path = os.path.dirname(out_data["spotXdsPath"])
            logger.debug("spot_xds_path: {0}".format(spot_xds_path))
        elif runGphlWorkflow:
            runGphlWorkflow = False
            logger.warning(
                "GPhL workflow not run due to indexing failure and/or no spot_xds_path in XDS indexing results"
            )
    else:
        listImages = edna_mxv1.getListImagePath(
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            bFirstImageOnly=False,
        )
        firstImagePath = listImages[0]

    if beamline in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
        edna_mxv1.h5ToCbf(beamline, listImages, workflow_working_dir)
        suffix = "cbf"
        newListImages = []
        # Change suffix to cbf...
        for imagePath in listImages:
            newListImages.append(imagePath.replace(".h5", ".cbf"))
        listImages = newListImages

    (forcedSpaceGroup, newCharacterisationComment) = symmetry.checkForcedSpaceGroup(
        forcedSpaceGroup, sampleInfo, directory
    )

    if (
        characterisationComment is None
        and firstImagePath is not None  # noqa W503
        and forcedSpaceGroup is not None  # noqa W503
    ):
        characterisationComment = newCharacterisationComment
        if characterisationComment == "":
            characterisationComment = (
                "Characterisation: Space group {0} forced.".format(forcedSpaceGroup)
            )
        else:
            characterisationComment += (
                " Characterisation: Space group {0} forced.".format(forcedSpaceGroup)
            )

    if minExposureTime is None:
        minExposureTime = config.get_value(beamline, "Beam", "minCollectExposureTime")

    detectorDistanceMin = config.get_value(beamline, "Detector", "distanceMin")
    detectorDistanceMax = config.get_value(beamline, "Detector", "distanceMax")
    minTransmission = config.get_value(beamline, "Beam", "minTransmission")

    xsDataInterfaceResult, ednaLogPath = edna_mxv1.mxv1ControlInterface(
        beamline,
        listImages,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        flux,
        beamSizeX,
        beamSizeY,
        strategyOption,
        minExposureTime,
        workflow_working_dir,
        transmission=characterisationTransmission,
        crystalSizeX=crystalSizeX,
        crystalSizeY=crystalSizeY,
        crystalSizeZ=crystalSizeZ,
        bLogExecutiveSummary=False,
        anomalousData=anomalousData,
        aimedCompleteness=aimedCompleteness,
        aimedMultiplicity=aimedMultiplicity,
        sampleSusceptibility=sampleSusceptibility,
        detectorDistanceMin=detectorDistanceMin,
        detectorDistanceMax=detectorDistanceMax,
        minTransmission=minTransmission,
        forcedSpaceGroup=forcedSpaceGroup,
        aimedIOverSigmaAtHighestResolution=aimedIOverSigmaAtHighestResolution,
        minOscWidth=minOscWidth,
        axisStart=axisStart,
        axisRange=axisRange,
        doseLimit=doseLimit,
        strategyType=strategyType,
    )
    mxv1StrategyResult = ""
    mxv1ResultCharacterisation = ""
    characterisation_success = False
    xsDataResultCharacterisation = xsDataInterfaceResult.resultCharacterisation
    if xsDataResultCharacterisation is None:
        message_text = "No results obtained from EDNA characterisation!"
        logger.error(message_text)
    else:
        mxv1ResultCharacterisation = xsDataResultCharacterisation.marshal()
        strHtmlPagePath, strJsonPath = edna_mxv1.createSimpleHtmlPage(
            beamline,
            mxv1ResultCharacterisation,
            workflow_working_dir,
            pyarch_html_dir,
            resolution=resolution,
            ednaCharacterisationLogPath=ednaLogPath,
        )
        if strHtmlPagePath is not None:
            if list_node_id is not None:
                list_node_id = common.jsonLoads(list_node_id)
                # Copy html files to workflow working dir
                newHtmlDir = os.path.join(
                    workflow_working_dir,
                    os.path.basename(os.path.dirname(strHtmlPagePath)),
                )
                shutil.copytree(os.path.dirname(strHtmlPagePath), newHtmlDir)
                newHtmlPagePath = os.path.join(
                    newHtmlDir, os.path.basename(strHtmlPagePath)
                )
                collect.displayNewHtmlPage(
                    beamline,
                    newHtmlPagePath,
                    "Characterisation",
                    list_node_id,
                    token=token,
                )

        averageDozorScore = None
        if useDozor:
            # Check dozor scores
            dozor_scores = []
            dozor_visible_resolutions = []
            for (
                imageQualityIndicator
            ) in xsDataResultCharacterisation.imageQualityIndicators:
                if imageQualityIndicator.dozor_score is not None:
                    dozor_scores.append(imageQualityIndicator.dozor_score.value)
                    dozor_visible_resolutions.append(
                        imageQualityIndicator.dozorVisibleResolution.value
                    )

            if len(dozor_scores) > 0:
                # Dozor visible resolutions
                min_visible_resolution = min(dozor_visible_resolutions)
                max_visible_resolution = max(dozor_visible_resolutions)
                logger.info(
                    "Min / max dozor visible resolution: {0:.2f} {1:.2f}".format(
                        min_visible_resolution, max_visible_resolution
                    )
                )
                # Calculate average dozor score
                averageDozorScore = sum(dozor_scores) / len(dozor_scores)
                logger.info(
                    "Average dozor score after characterisation: {0:.3f}".format(
                        averageDozorScore
                    )
                )
                if (
                    abs(min_visible_resolution - max_visible_resolution) > 0.2
                    and averageDozorScore > 10
                    and (min(dozor_scores) / max(dozor_scores) < 0.05)  # noqa W503
                    and not misalignedCrystalDetected  # noqa W503
                ):
                    # Check max / min dozor score
                    logger.warning("Ratio min / max dozor score less than 20 %!")
                    logger.warning(
                        "This means that probably the crystal is not well aligned in beam."
                    )
                    misalignedCrystalDetected = True
                    message_text = "Possible crystal misalignment detected."
                else:
                    misalignedCrystalDetected = False
                if message_text != "":
                    if (
                        characterisationComment is None
                        or characterisationComment == ""  # noqa W503
                    ):
                        characterisationComment = message_text
                    else:
                        characterisationComment += " " + message_text
            else:
                logger.warning("No dozor scores obtained from reference images!")
                misalignedCrystalDetected = False
        else:
            misalignedCrystalDetected = False

        if xsDataResultCharacterisation.strategyResult is None or (  # noqa W503
            len(xsDataResultCharacterisation.strategyResult.collectionPlan) == 0
        ):
            status = "Failure"
            message_text = "No strategy results obtained from EDNA characterisation."
            logger.error(message_text)
            if averageDozorScore is not None and averageDozorScore < 0.1:
                if do_data_collect:
                    message_text = "The average Dozor score after "
                    message_text += " characterisation ({0:.3f})".format(
                        averageDozorScore
                    )
                    message_text += " is below the threshold of 0.1."
                    message_text += " Very low diffracting sample."
                    logger.error(message_text)
                    # do_data_collect_orig = do_data_collect
                    # do_data_collect = False
                    if (
                        characterisationComment is None
                        or characterisationComment == ""  # noqa W503
                    ):
                        characterisationComment = message_text
                    else:
                        characterisationComment += " " + message_text
                misalignedCrystalDetected = False
            else:
                if characterisationComment is None or characterisationComment == "":
                    characterisationComment = message_text
                else:
                    characterisationComment += " " + message_text

        else:
            status = "Success"
            characterisation_success = True
            mxv1StrategyResult = xsDataResultCharacterisation.strategyResult.marshal()
            # Extract cell and space group
            indexingResult = xsDataResultCharacterisation.indexingResult
            if indexingResult is not None:
                selectedSolution = indexingResult.selectedSolution
                crystal = selectedSolution.crystal
                spaceGroup = crystal.spaceGroup
                cell = crystal.cell
                cellSpaceGroup["spaceGroup"] = spaceGroup.name.value
                cellSpaceGroup["cell_a"] = cell.length_a.value
                cellSpaceGroup["cell_b"] = cell.length_b.value
                cellSpaceGroup["cell_c"] = cell.length_c.value
                cellSpaceGroup["cell_alpha"] = cell.angle_alpha.value
                cellSpaceGroup["cell_beta"] = cell.angle_beta.value
                cellSpaceGroup["cell_gamma"] = cell.angle_gamma.value

        workflowStepType = "Characterisation"
        workflowStepImagePyarchPath = None
        bestWilsonPath = os.path.join(os.path.dirname(strHtmlPagePath), "B.jpg")
        if xsDataResultCharacterisation.strategyResult is not None and os.path.exists(
            bestWilsonPath
        ):  # noqa W503
            workflowStepImagePyarchPath = bestWilsonPath
        elif len(xsDataResultCharacterisation.thumbnailImage) > 1:
            workflowStepImage = xsDataResultCharacterisation.thumbnailImage[
                0
            ].path.value
            if os.path.exists(workflowStepImage):
                shutil.copy(workflowStepImage, os.path.dirname(strHtmlPagePath))
                workflowStepImagePyarchPath = os.path.join(
                    os.path.dirname(strHtmlPagePath),
                    os.path.basename(workflowStepImage),
                )
        ispyb.storeWorkflowStep(
            beamline,
            workflowParameters["ispyb_id"],
            workflowStepType,
            status,
            imageResultFilePath=workflowStepImagePyarchPath,
            htmlResultFilePath=strHtmlPagePath,
            resultFilePath=strJsonPath,
        )
        logger.info("Storing workflow step in ICAT")
        metadata_urls = config.get_value(
            beamline, "ICAT", "metadata_urls", default_value=[]
        )
        if len(metadata_urls) > 0:
            sample_name = icat.get_sample_name(directory)
            icat.storeWorkflowStep(
                beamline=beamline,
                proposal=proposal,
                directory=directory,
                workflowStepType=workflowStepType.lower(),
                sample_name=sample_name,
                workflow_name=workflowParameters["title"],
                workflow_type=workflowParameters["type"],
                bes_request_id=bes_request_id,
                snap_shot_path=workflowStepImagePyarchPath,
                json_path=strJsonPath,
                icat_sub_dir=f"characterisation_id_{characterisation_id}",
                characterisation_id=characterisation_id,
                kappa_settings_id=kappa_settings_id,
                position_id=position_id,
            )

    if (
        characterisationComment is not None
        and characterisationComment != ""  # noqa W503
        and firstImagePath is not None  # noqa W503
    ):
        ispyb.updateDataCollectionComment(
            beamline, firstImagePath, characterisationComment
        )
        ispyb.updateDataCollectionGroupComment(
            beamline, firstImagePath, characterisationComment
        )

    mxv1StrategyResultFile = path.createNewXSDataFile(
        workflowParameters["workingDir"],
        mxv1StrategyResult,
        filePrefix="mxv1StrategyResult",
    )
    mxv1ResultCharacterisationFile = path.createNewXSDataFile(
        workflowParameters["workingDir"],
        mxv1ResultCharacterisation,
        filePrefix="mxv1ResultCharacterisation",
    )

    # Fake values for testing of GPhL workflows
    # runGphlWorkflow = True
    # spot_xds_path = "/data/id30a2/inhouse/opid30a2/20230324/PROCESSED_DATA/b/b-b/MXPressA_01/Workflow_20230324-122321/ControlIndexing_local_user_1/XDSIndexing_wyntn_ne"
    # If runGphlWorkflow, set characterisation_success to True
    if not characterisation_success and runGphlWorkflow:
        logger.warning("Characterisation failed but we have an XDS indexing spot file!")
        logger.warning("Therefore we run the Gphl workflow.")
        characterisation_success = True
        edna_characterisation_success = False
    else:
        edna_characterisation_success = characterisation_success

    if not characterisation_success and simulated_characterisation_results:
        logger.warning("Using simulated characterisation results!")
        characterisation_success = True

    return {
        "characterisation_success": characterisation_success,
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
        "mxv1ResultCharacterisationFile": mxv1ResultCharacterisationFile,
        "message_text": message_text,
        "characterisationComment": characterisationComment,
        "cellSpaceGroup": cellSpaceGroup,
        "do_data_collect": do_data_collect,
        "do_data_collect_orig": do_data_collect_orig,
        "misalignedCrystalDetected": misalignedCrystalDetected,
        "spot_xds_path": spot_xds_path,
        "runGphlWorkflow": runGphlWorkflow,
        "edna_characterisation_success": edna_characterisation_success,
        "characterisation_images": listImages,
        "characterisation_id": characterisation_id,
        "characterisation_raw_directory": directory,
    }
