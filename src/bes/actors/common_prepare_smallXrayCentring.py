def run(
    beamline, besParameters=None, workflow_parameters=None, expTypePrefix="mesh-", **_
):

    if not besParameters:
        besParameters = {}

    characterisation_id = None
    kappa_settings_id = None
    position_id = None
    sample_name = None
    workflow_type = "XrayCentering"
    workflow_title = "Small X-ray centering on %s" % beamline

    if workflow_parameters:
        characterisation_id = workflow_parameters["workflow_characterisation_id"]
        kappa_settings_id = workflow_parameters["workflow_kappa_settings_id"]
        workflow_title = workflow_parameters["workflow_name"]
        position_id = workflow_parameters["workflow_position_id"]
        workflow_type = workflow_parameters["workflow_type"]
        besParameters["request_id"] = workflow_parameters["workflow_uid"]
        sample_name = workflow_parameters.get("sample_name")

    return {
        "besParameters": besParameters,
        "characterisation_id": characterisation_id,
        "kappa_settings_id": kappa_settings_id,
        "position_id": position_id,
        "workflow_title": workflow_title,
        "workflow_type": workflow_type,
        "expTypePrefix": expTypePrefix,
        "do2DMeshOnly": False,
        "diffractionSignalDetection": "DozorM2 (macro-molecules crystal detection)",
        "workflow_parameters": workflow_parameters,
        "sample_name": sample_name,
    }
