from bes.workflow_lib import path, workflow_logging, edna_mxv1, edna_kernel, common


def run(
    beamline,
    workflowParameters,
    mxv1StrategyResultFile,
    sampx1,
    sampy1,
    phiy1,
    phiz1,
    sampx2,
    sampy2,
    phiy2,
    phiz2,
    noPositions,
    dataCollectionType="Helical",
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        sampx1 = float(sampx1)
        sampy1 = float(sampy1)
        phiy1 = float(phiy1)
        phiz1 = float(phiz1)
        sampx2 = float(sampx2)
        sampy2 = float(sampy2)
        phiy2 = float(phiy2)
        phiz2 = float(phiz2)
        if noPositions is not None and noPositions != "null":
            noPositions = int(noPositions)

        workflowParameters = common.jsonLoads(workflowParameters)
        workflow_working_dir = workflowParameters["workingDir"]

        mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)
        resultStrategy = edna_mxv1.parseMxv1StrategyResult(mxv1StrategyResult)

        # Fill in helical position in data collection strategy
        if dataCollectionType == "Helical":
            for collectionPlan in resultStrategy.collectionPlan:
                dataCollection = collectionPlan.collectionStrategy
                for subWedge in dataCollection.subWedge:
                    goniostat = subWedge.experimentalCondition.goniostat
                    position1 = edna_kernel.XSDataVectorDouble()
                    position1.v1 = sampx1
                    position1.v2 = sampy1
                    if beamline == "id23eh2":
                        position1.v3 = phiz1
                    else:
                        position1.v3 = phiy1
                    goniostat.addSamplePosition(position1)
                    position2 = edna_kernel.XSDataVectorDouble()
                    position2.v1 = sampx2
                    position2.v2 = sampy2
                    if beamline == "id23eh2":
                        position2.v3 = phiz2
                    else:
                        position2.v3 = phiy2
                    goniostat.addSamplePosition(position2)
        elif dataCollectionType == "Multi-position":
            index = 0
            for collectionPlan in resultStrategy.collectionPlan:
                dataCollection = collectionPlan.collectionStrategy
                for subWedge in dataCollection.subWedge:
                    goniostat = subWedge.experimentalCondition.goniostat
                    position1 = edna_kernel.XSDataVectorDouble()
                    position1.v1 = sampx1 + (sampx2 - sampx1) * index / (
                        noPositions - 1
                    )
                    position1.v2 = sampy1 + (sampy2 - sampy1) * index / (
                        noPositions - 1
                    )
                    if beamline == "id23eh2":
                        position1.v3 = phiz1 + (phiz2 - phiz1) * index / (
                            noPositions - 1
                        )
                    else:
                        position1.v3 = phiy1 + (phiy2 - phiy1) * index / (
                            noPositions - 1
                        )
                    goniostat.addSamplePosition(position1)
                    index += 1

        mxv1StrategyResult = resultStrategy.marshal()
        logger.debug(mxv1StrategyResult)

        mxv1StrategyResultFile = path.createNewXSDataFile(
            workflow_working_dir, resultStrategy, "mxv1StrategyResult_"
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
    }
