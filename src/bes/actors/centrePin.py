#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "27/05/2021"

import os
import time

from bes.workflow_lib import grid
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    findLargestMesh=False,
    pyarch_html_dir=None,
    isHorizontalRotationAxis=True,
    numberOfPositions=1,
    targetApertureName=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    motorPositions = collect.readMotorPositions(beamline, token=token)

    snapshotDir = workflowParameters["workingDir"]
    logger.debug("Snapshot directory: {0}".format(snapshotDir))

    background_image = os.path.join(snapshotDir, "background.png")
    # Move sample relative 1.5 mm for background snapshot
    if isHorizontalRotationAxis:
        deltaPhiy = config.get_value(beamline, "Goniometer", "deltaPhiy")
        signPhiy = config.get_value(beamline, "Goniometer", "signPhiy")
        phiyOrig = motorPositions["phiy"]
        phiyNew = phiyOrig + deltaPhiy * signPhiy
        collect.moveMotors(beamline, directory, {"phiy": phiyNew}, token=token)
        time.sleep(1)
        collect.saveSnapshot(beamline, background_image, token=token)
        collect.moveMotors(beamline, directory, {"phiy": phiyOrig}, token=token)
    else:
        deltaPhiz = config.get_value(beamline, "Goniometer", "deltaPhiy")
        signPhiz = config.get_value(beamline, "Goniometer", "signPhiy")
        phizOrig = motorPositions["phiz"]
        phizNew = phizOrig + deltaPhiz * signPhiz
        collect.moveMotors(beamline, directory, {"phiz": phizNew}, token=token)
        time.sleep(1)
        collect.saveSnapshot(beamline, background_image, token=token)
        collect.moveMotors(beamline, directory, {"phiz": phizOrig}, token=token)

    while not os.path.exists(background_image):
        logger.info("Waiting for background image %s" % background_image)
        os.system("ls {0}> /dev/null".format(os.path.dirname(background_image)))
        time.sleep(1)

    time.sleep(1)
    collect.takeSnapshots(
        beamline, directory, snapshotDir, background_image, nPos=4, token=token
    )
    zoomLevel = collect.getZoomLevel(beamline, token)
    logger.info("Zoom level: {0}".format(zoomLevel))
    pixelPerMm = config.get_value(beamline, "SnapShotZoomMmPerPixel", str(zoomLevel))
    logger.info("Pixels per mm: {0}".format(pixelPerMm))
    tungstenPixels = 0.020 * pixelPerMm
    logger.info("Number of pixels for tungsten pin: {0}".format(tungstenPixels))
    if int(zoomLevel) >= 5:
        doCircleFit = True
    else:
        doCircleFit = False
    deltaX, deltaY, deltaZ = grid.centrePin(
        snapshotDir,
        autoMeshWorkingDir=snapshotDir,
        loopWidth=tungstenPixels,
        doCircleFit=doCircleFit,
        debug=False,
    )
    logger.info(
        "DeltaX = {0}, DeltaY = {1}, DeltaZ = {2} (pixels)".format(
            round(deltaX, 1), round(deltaY, 2), round(deltaZ, 2)
        )
    )
    logger.info(
        "DeltaX = {0}, DeltaY = {1}, DeltaZ = {2} (mm)".format(
            round(deltaX / pixelPerMm, 4),
            round(deltaY / pixelPerMm, 4),
            round(deltaZ / pixelPerMm, 4),
        )
    )
    sampx = motorPositions["sampx"]
    sampy = motorPositions["sampy"]
    phiy = motorPositions["phiy"]
    logger.info("Old sampx: {0}, sampy: {1}, phiy: {2}".format(sampx, sampy, phiy))
    sampx = round(sampx + deltaX / pixelPerMm, 4)
    sampy = round(sampy + deltaY / pixelPerMm, 4)
    phiy = round(phiy + deltaZ / pixelPerMm, 4)
    logger.info("New sampx: {0}, sampy: {1}, phiy: {2}".format(sampx, sampy, phiy))
    newPositions = {"sampx": sampx, "sampy": sampy, "phiy": phiy}
    collect.moveMotors(beamline, directory, newPositions, token=token)

    return {}
