from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    osc_range=1,
    numberOfImages=10,
    noDegreesPerWedge=10,
    energy1=0,
    energy2=0,
    energy3=0,
    energy4=0,
    collection_software=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    osc_range = float(osc_range)
    numberOfImages = int(numberOfImages)
    noDegreesPerWedge = float(noDegreesPerWedge)
    energy1 = float(energy1)
    energy2 = float(energy2)
    energy3 = float(energy3)
    energy4 = float(energy4)

    dictValues = {}

    listDialog = [
        {
            "variableName": "osc_range",
            "label": "Total oscillation range",
            "type": "float",
            "value": osc_range,
            "unit": "degree",
            "lowerBound": 0.05,
            "upperBound": 360.0,
        },
        {
            "variableName": "numberOfImages",
            "label": "Total number of images",
            "type": "int",
            "value": numberOfImages,
            "unit": "",
            "lowerBound": 1,
            "upperBound": 10000,
        },
        {
            "variableName": "noDegreesPerWedge",
            "label": "Number of degrees per wedge",
            "type": "float",
            "value": noDegreesPerWedge,
            "unit": "degrees",
            "lowerBound": 0.01,
            "upperBound": 10.0,
        },
        {
            "variableName": "energy1",
            "label": "Energy 1",
            "type": "float",
            "value": energy1,
            "unit": "eV",
            "lowerBound": 0.1,
            "upperBound": 100000.0,
        },
        {
            "variableName": "energy2",
            "label": "Energy 2",
            "type": "float",
            "value": energy2,
            "unit": "eV",
            "lowerBound": 0.1,
            "upperBound": 100000.0,
        },
        {
            "variableName": "energy3",
            "label": "Energy 3",
            "type": "float",
            "value": energy3,
            "unit": "eV",
            "lowerBound": 0.0,
            "upperBound": 100000.0,
        },
        {
            "variableName": "energy4",
            "label": "Energy 4",
            "type": "float",
            "value": energy4,
            "unit": "eV",
            "lowerBound": 0.0,
            "upperBound": 100000.0,
        },
        {
            "variableName": "isInverseBeam",
            "label": "Inverse beam?",
            "type": "boolean",
            "value": False,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        collection_software=collection_software,
    )

    return dictValues
