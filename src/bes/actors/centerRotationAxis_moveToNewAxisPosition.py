import time
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    newMotorPositions,
    motorInitialPositions,
    grid_info,
    phiy_scan1,
    phiz_scan1,
    run_number,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    try:

        # Read phiz position
        # newMotorPositions = common.jsonLoads(newMotorPositions)
        # savedMotorPositions = common.jsonLoads(savedMotorPositions)
        run_number = int(run_number)

        if beamline in ["id23eh2", "id30a2"]:
            phiy_orig = motorInitialPositions["phiy"]
            phiy_scan2 = newMotorPositions["phiy"]
            delta_phiy1 = phiy_orig - phiy_scan1
            delta_phiy2 = phiy_orig - phiy_scan2
            delta_phiy = -(delta_phiy1 + delta_phiy2) / 2.0
            logger.info("Delta phiy: %.3f mm" % delta_phiy)
            new_phiy = phiy_orig + delta_phiy
            logger.info("New phiy position: %f" % new_phiy)
            # collect.moveMotors(beamline, directory, savedMotorPositions, token=token)
            # collect.moveSampleOrthogonallyToRotationAxis(beamline, directory, -distance, token=token)
        else:
            phiz_orig = motorInitialPositions["phiz"]
            phiz_scan2 = newMotorPositions["phiz"]
            delta_phiz1 = phiz_orig - phiz_scan1
            delta_phiz2 = phiz_orig - phiz_scan2
            delta_phiz = (delta_phiz1 + delta_phiz2) / 2.0
            logger.info("Delta phiz: %.3f mm" % delta_phiz)
            new_phiz = phiz_orig + delta_phiz
            logger.info("New phiz position: %f" % new_phiz)
            # collect.moveMotors(beamline, directory, savedMotorPositions, token=token)
            # collect.moveSampleOrthogonallyToRotationAxis(beamline, directory, -distance, token=token)
        time.sleep(1)

        motorPositions = collect.readMotorPositions(beamline, token=token)

        # run_number += 1

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {"run_number": run_number, "motorPositions": motorPositions}
