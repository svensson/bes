from bes import config
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(beamline, phi, flux, token=None, **_):

    phi = float(phi)
    flux = float(flux)

    logger = workflow_logging.getLogger(beamline, token=token)

    maxFlux = 1.7e12 / 2.0

    collectExposureTime = config.get_value(
        beamline, "Beam", "defaultCollectExposureTime"
    )
    if flux < maxFlux:
        transmission = 100
    else:
        transmission = 100 * maxFlux / flux
    logger.info("Transmission set to %.1f %%" % transmission)
    number_of_images = 1800
    osc_range = 0.2

    mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
        collectExposureTime, transmission, number_of_images, osc_range, phi
    )
    logger.debug(mxv1StrategyResult)

    return {
        "expTypePrefix": "",
        "do_data_collect": True,
        "automatic_mode": True,
        "mxv1StrategyResult": mxv1StrategyResult,
    }
