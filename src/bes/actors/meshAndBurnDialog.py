#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "01/02/2021"

from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    transmission,
    gridSnapShots,
    collection_software=None,
    token=None,
    resolution=None,
    data_collection_osc_range=None,
    data_collection_exposure_time=None,
    data_collection_no_images=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    if resolution is None:
        resolution = collect.getResolution(beamline, token)

    listDialog = [
        {
            "variableName": "data_collection_no_images",
            "label": "Data collection number of images",
            "type": "int",
            "value": data_collection_no_images,
            "lowerBound": 1,
            "upperBound": 10000.0,
        },
        {
            "variableName": "data_collection_exposure_time",
            "label": "Data collection exposure time (s)",
            "type": "float",
            "defaultValue": data_collection_exposure_time,
            "lowerBound": config.get_value(beamline, "Beam", "minGridExposureTime"),
            "upperBound": 100.0,
        },
        {
            "variableName": "data_collection_osc_range",
            "label": "Data collection oscillation range (degrees)",
            "type": "float",
            "defaultValue": data_collection_osc_range,
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "resolution",
            "label": "Resolution (A)",
            "type": "float",
            "value": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 10.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission (%)",
            "type": "float",
            "defaultValue": transmission,
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "meshHorizontalSeparation",
            "label": "Mesh horizontal separation (um)",
            "type": "float",
            "defaultValue": 0.0,
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "meshVerticalSeparation",
            "label": "Mesh vertical separation (um)",
            "type": "float",
            "defaultValue": 0.0,
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "gridSnapShots",
            "label": "Take single snapshot at end of WF",
            "type": "boolean",
            "value": gridSnapShots,
        },
    ]

    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        collection_software=collection_software,
    )

    return dictValues
