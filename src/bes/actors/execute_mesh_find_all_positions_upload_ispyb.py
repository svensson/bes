#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF"
__updated__ = "2021-02-15"

import os
import sys
import json
import base64
import pprint
import traceback

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes import config
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna2_tasks
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    meshPositionFile,
    run_number,
    expTypePrefix,
    workflow_working_dir,
    prefix,
    suffix,
    workflow_id,
    grid_info,
    workflow_title,
    workflow_type,
    data_threshold,
    data_collection_max_positions,
    radius,
    shape=None,
    list_node_id=None,
    firstImagePath=None,
    pyarch_html_dir=None,
    diffractionSignalDetection="Dozor (macro-molecules)",
    resultMeshPath=None,
    token=None,
    meshZigZag=False,
    dozorAllFile=None,
    isHorizontalRotationAxis=False,
    reject_level=None,
    **_,
):

    if reject_level is None:
        reject_level = config.get_value(
            beamline, "Mesh", "rejectLevel", default_value=40
        )
    loop_thickness = config.get_value(
        beamline, "Mesh", "loopThickness", default_value=500
    )
    crystalSizeX = None
    crystalSizeY = None
    peakSizeX = None
    peakSizeY = None
    dozorVisibleResolution = None
    meshResultsJsonPath = None
    workflowParameters = common.jsonLoads(workflowParameters)
    workingDirectory = workflowParameters["workingDir"]
    workflowId = workflowParameters["ispyb_id"]
    dozormCrystalMapPath = None
    dozormImageNumberMapPath = None
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    radius = float(radius)
    meshPositions = path.parseJsonFile(meshPositionFile)
    if firstImagePath is None:
        firstImagePath = os.path.join(directory, meshPositions[0]["imageName"])
    meshPositions = path.parseJsonFile(meshPositionFile)
    dozormListPositions = []
    if dozorAllFile is None:
        listDozorAllFile = []
    else:
        listDozorAllFile = [dozorAllFile]

    if diffractionSignalDetection.startswith("DozorM2"):
        try:
            listApertureSizes = config.get_value(beamline, "Beam", "apertureSizes")
            nominalBeamSizeX = config.get_value(
                beamline, "Beam", key_name="nominalSizeX"
            )
            nominalBeamSizeY = config.get_value(
                beamline, "Beam", key_name="nominalSizeY"
            )
            logger.info("Running DozorM2...")
            outDataDozorM2 = edna2_tasks.runDozorM2(
                beamline=beamline,
                firstImagePath=firstImagePath,
                grid_info=grid_info,
                meshZigZag=meshZigZag,
                listDozorAllFile=listDozorAllFile,
                listApertureSizes=listApertureSizes,
                nominalBeamSizeX=nominalBeamSizeX,
                nominalBeamSizeY=nominalBeamSizeY,
                workingDirectory=workingDirectory,
                isHorizontalRotationAxis=isHorizontalRotationAxis,
                reject_level=reject_level,
                loop_thickness=loop_thickness,
            )
            # TODO: if outDataDozorM2 is an empty dict the processing failed
            logger.info("DozorM2 finished")
            dozormListPositions = outDataDozorM2["dictCoord"]["scan1"]
            arrayScore = outDataDozorM2["score"]
            meshPositions = edna2_tasks.updateDozorMMeshPositions(
                arrayScore=arrayScore, meshPositions=meshPositions
            )
            prefix = path.getPrefix(firstImagePath)
            meshPositionFile = path.createNewJsonFile(
                workingDirectory,
                meshPositions,
                "meshPositionsDozorM_{0}".format(prefix),
            )
            dozormCrystalMapPath = outDataDozorM2["crystalMapPath"]
            dozormImageNumberMapPath = outDataDozorM2["imageNumberMapPath"]
            all_positions = grid.find_all_position_dozorm(
                listPositions=dozormListPositions,
                grid_info=grid_info,
                meshPositions=meshPositions,
                isHorizontalRotationAxis=isHorizontalRotationAxis,
                workflowWorkingDir=workflow_working_dir,
            )
        except Exception:
            logger.debug("Error when running DozorM!")
            logger.error("Falling back to 'Dozor' diffraction signal detection.")
            diffractionSignalDetection = "Dozor (macro-molecules)"
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            listTrace = traceback.extract_tb(exc_traceback)
            logger.debug(exc_type)
            logger.debug(exc_value)
            logger.debug("Traceback (most recent call last): %s" % os.linesep)
            for listLine in listTrace:
                errorLine = '  File "%s", line %d, in %s%s' % (
                    listLine[0],
                    listLine[1],
                    listLine[2],
                    os.linesep,
                )
                logger.debug(errorLine)

    if not diffractionSignalDetection.startswith("DozorM2"):
        all_positions = grid.find_all_positions(
            grid_info,
            meshPositions,
            data_threshold=data_threshold,
            upper_threshold=0.90,
            radius=radius,
            diffractionSignalDetection=diffractionSignalDetection,
            maxNoPositions=data_collection_max_positions,
        )

    if len(all_positions) > data_collection_max_positions:
        all_positions = all_positions[0:data_collection_max_positions]
    logger.debug("all positions: %s" % pprint.pformat(all_positions))

    dict_html = grid.grid_create_result_html_page(
        beamline,
        grid_info,
        meshPositions,
        all_positions,
        directory,
        workflow_working_dir,
        data_threshold,
        diffractionSignalDetection=diffractionSignalDetection,
        dozormListPositions=dozormListPositions,
        dozormCrystalMapPath=dozormCrystalMapPath,
        dozormImageNumberMapPath=dozormImageNumberMapPath,
    )
    # Send plot image to MXCuBE
    if (
        beamline in ["id23eh1", "id23eh2"]
        and shape is not None
        and shape != "-1"
        and "plot_file_image" in dict_html
    ):
        plot_file_image = dict_html["plot_file_image"]
        base64data = base64.b64encode(open(plot_file_image, "rb").read()).decode(
            "utf-8"
        )
        collect.setImageGridData(beamline, base64data, key=shape, token=token)
        logger.debug("Image {0} sent to MXCuBE".format(plot_file_image))
    dict_html_pyarch = {}
    html_page_path = os.path.join(
        dict_html["html_directory_path"], dict_html["index_file_name"]
    )
    if os.path.exists(html_page_path):
        if list_node_id is not None:
            list_node_id = common.jsonLoads(list_node_id)
            collect.displayNewHtmlPage(
                beamline,
                html_page_path,
                "Grid results",
                list_node_id,
                strPageNumber=run_number,
                token=token,
            )
        if pyarch_html_dir is not None:
            if expTypePrefix == "mesh-":
                directory_name = "Mesh"
            elif expTypePrefix == "line-":
                directory_name = "Line"
            else:
                directory_name = "Html"
            dict_html_pyarch = path.copyHtmlDirectoryToPyarch(
                pyarch_html_dir, dict_html, directory_name
            )
            workflowStepType = directory_name
            status = "Success"
            imageResultFilePath = os.path.join(
                dict_html_pyarch["html_directory_path"],
                dict_html_pyarch["plot_file_name"],
            )
            htmlResultFilePath = os.path.join(
                dict_html_pyarch["html_directory_path"],
                dict_html_pyarch["index_file_name"],
            )
            resultFilePath = os.path.join(
                dict_html_pyarch["html_directory_path"], "result.json"
            )
            open(resultFilePath, "w").write(json.dumps(dict_html["workflowStep"]))
            ispyb.storeWorkflowStep(
                beamline,
                workflowId,
                workflowStepType,
                status,
                imageResultFilePath=imageResultFilePath,
                htmlResultFilePath=htmlResultFilePath,
                resultFilePath=resultFilePath,
            )

    best_position = grid.find_best_position(grid_info, meshPositions)
    if best_position is not None:
        if "crystal_size_x" in best_position and "crystal_size_y" in best_position:
            crystalSizeX = best_position["crystal_size_x"]
            crystalSizeY = best_position["crystal_size_y"]
        if "peak_size_x" in best_position and "peak_size_y" in best_position:
            peakSizeX = best_position["peak_size_x"]
            peakSizeY = best_position["peak_size_y"]
        if "dozorVisibleResolution" in best_position:
            dozorVisibleResolution = best_position["dozorVisibleResolution"]
        elif "dozorVisibleResolution" in best_position["nearest_image"]:
            dozorVisibleResolution = best_position["nearest_image"][
                "dozorVisibleResolution"
            ]
    dictDataCollection = ispyb.findDataCollectionFromFileLocationAndFileName(
        beamline, firstImagePath
    )
    if dictDataCollection is None:
        raise RuntimeError(
            f"Cannot find data collection for {firstImagePath} in ISPyB"
            + " - the data collection most likely failed."
        )
    flux = dictDataCollection["flux_end"]
    if flux is None:
        raise BaseException(f"No flux recorded in ISPyB for image {firstImagePath}")
    beamSizeAtSampleX = dictDataCollection["beamSizeAtSampleX"]
    beamSizeAtSampleY = dictDataCollection["beamSizeAtSampleY"]
    logger.info("Data processing finished.")

    data_collection_max_positions = len(all_positions)

    allPositionFile = path.createNewJsonFile(
        workflow_working_dir, all_positions, "all_positions_"
    )

    if best_position is not None:
        bestPositionFound = True
    else:
        bestPositionFound = False

    # Dataset outputs:
    return {
        "flux": flux,
        "bestPosition": json.dumps(best_position, indent=4),
        "bestPositionFound": bestPositionFound,
        "allPositionFile": allPositionFile,
        "beamSizeAtSampleX": beamSizeAtSampleX,
        "beamSizeAtSampleY": beamSizeAtSampleY,
        "data_collection_max_positions": data_collection_max_positions,
        "dict_html_pyarch": json.dumps(dict_html_pyarch, indent=4),
        "meshResultsJsonPath": meshResultsJsonPath,
        "dozormListPositions": dozormListPositions,
        "meshPositionFile": meshPositionFile,
        "dozormCrystalMapPath": dozormCrystalMapPath,
        "dozormImageNumberMapPath": dozormImageNumberMapPath,
        "crystalSizeX": crystalSizeX,
        "crystalSizeY": crystalSizeY,
        "peakSizeX": peakSizeX,
        "peakSizeY": peakSizeY,
        "dozorVisibleResolution": dozorVisibleResolution,
    }
