#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

import os

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    meshPositions,
    run_number,
    expTypePrefix,
    workflow_working_dir,
    prefix,
    suffix,
    workflow_id,
    grid_info,
    workflow_title,
    workflow_type,
    firstImagePath,
    pyarch_html_dir=None,
    list_node_id=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        meshPositions = common.jsonLoads(meshPositions)

        isHorizontalRotationAxis = True
        best_position = grid.find_best_position(
            grid_info, meshPositions, isHorizontalRotationAxis=isHorizontalRotationAxis
        )
        dictBeamSize = edna_ispyb.getMeshBeamsize(
            beamline, directory, firstImagePath, workflow_working_dir
        )
        beamSizeAtSampleX = dictBeamSize["beamSizeAtSampleX"]
        beamSizeAtSampleY = dictBeamSize["beamSizeAtSampleY"]
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            [best_position],
            directory,
            workflow_working_dir,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
        )

        if list_node_id is not None:
            list_node_id = common.jsonLoads(list_node_id)
            html_page_path = os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
            if os.path.exists(html_page_path):
                collect.displayNewHtmlPage(
                    beamline,
                    html_page_path,
                    "Grid results",
                    list_node_id,
                    strPageNumber=run_number,
                    token=token,
                )
                if pyarch_html_dir is not None:
                    if expTypePrefix == "mesh-":
                        directory_name = "Mesh"
                    elif expTypePrefix == "line-":
                        directory_name = "Line"
                    else:
                        directory_name = "Html"
                    _ = path.copyHtmlDirectoryToPyarch(
                        pyarch_html_dir, dict_html, directory_name
                    )
        dict_statistics = grid.mesh_scan_statistics(
            meshPositions, workingDir=workflow_working_dir
        )
        if beamline == "id30a1":
            emailList = config.get_value(
                beamline, "Email", "meshStatistics", default_value=[]
            )
            common.send_email(
                beamline,
                emailList,
                "Mesh statistics",
                directory,
                workflow_title,
                workflow_type,
                workflow_working_dir,
                str_message=dict_statistics["message"],
            )
        logger.info("Data processing finished.")
        logger.debug("Uploading data collection parameters to ISPyB...")
        iDataCollectionGroupId = edna_ispyb.updateDataCollectionGroupWorkflowId(
            beamline,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            firstImagePath,
            workflow_id,
            workflow_working_dir,
        )
        edna_ispyb.groupMeshDataCollections(
            beamline,
            directory,
            meshPositions,
            workflow_working_dir,
            iDataCollectionGroupId,
        )
        # BES-251 : Remove call to store_positions
        # ispyb_positions = grid.find_ispyb_positions(meshPositions)
        # list_image_creation = edna_ispyb.store_positions(beamline, directory, ispyb_positions, workflow_working_dir)
        # best_image_id = grid.find_best_image_id(meshPositions, list_image_creation, best_position)
        # edna_mxv1.create_thumbnails_for_pyarch_asynchronous(beamline, list_image_creation)
        list_image_creation = []

        logger.debug("ISPyB uploading finished.")

        perform_extra_1D_scan = False
        if (
            isHorizontalRotationAxis
            and best_position is not None
            and grid_info["steps_x"] > 1
        ):
            if "no_pixels_cross_y" in best_position:
                if best_position["no_pixels_cross_y"] == 1:
                    logger.info(
                        "Only one pixel used for vertical beam position determination - extra vertical scan might be needed"
                    )
                    perform_extra_1D_scan = True
        elif (
            (not isHorizontalRotationAxis)
            and best_position is not None
            and grid_info["steps_y"] > 1
        ):
            if "no_pixels_cross_x" in best_position:
                if best_position["no_pixels_cross_x"] == 1:
                    logger.info(
                        "Only one pixel used for horizontal beam position determination - extra horizontal scan might be needed"
                    )
                    perform_extra_1D_scan = True

        # Dataset outputs:
        dict_output = {
            "bestPosition": best_position,
            "beamSizeAtSampleX": beamSizeAtSampleX,
            "beamSizeAtSampleY": beamSizeAtSampleY,
            "list_image_creation": list_image_creation,
            "perform_extra_1D_scan": perform_extra_1D_scan,
        }

        if best_position is not None:
            if grid_info["steps_x"] == 1:
                if "crystal_size_y" in best_position:
                    # Vertical scan
                    dict_output["crystalSizeZ"] = best_position["crystal_size_y"]
            elif (
                "crystal_size_x" in best_position and "crystal_size_y" in best_position
            ):
                dict_output["crystalSizeX"] = best_position["crystal_size_x"]
                dict_output["crystalSizeY"] = best_position["crystal_size_y"]
    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return dict_output
