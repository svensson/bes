from bes.workflow_lib import common


def run(beamline, directory, **kwargs):

    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressS: automated mesh and collect on %s" % beamline
    )
    workflowParameters["type"] = "MXPressS"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "automatic_mode": True,
        "data_threshold": 100000.0,
        "redo_find_positions": False,
        "data_collection_exposure_time": 0.037,
        "data_collection_osc_range": 10.0,
        "data_collection_no_images": 100,
        "data_collection_max_positions": 100,
        "multi_wedge": True,
        "radius": 3.0,
        "aimedIOverSigmaAtHighestResolution": 1.0,
        "doMeshIndexing": True,
        "plateMode": True,
        "gridSnapShots": True,
    }
