#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "01/03/2021"

from bes.workflow_lib import dialog
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    data_collection_exposure_time,
    data_collection_osc_range,
    data_collection_no_images,
    data_collection_max_positions,
    transmission,
    radius,
    aimedIOverSigmaAtHighestResolution=1.0,
    redo_find_positions=False,
    data_threshold=100000.0,
    resolution=None,
    timeToReachHendersonLimit=None,
    flux=None,
    diffractionSignalDetection="DozorM2 (macro-molecules crystal detection)",
    collection_software=None,
    token=None,
    automatic_mode=False,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    data_collection_exposure_time = float(data_collection_exposure_time)
    data_collection_osc_range = float(data_collection_osc_range)
    data_collection_no_images = int(data_collection_no_images)
    data_collection_max_positions = int(data_collection_max_positions)
    aimedIOverSigmaAtHighestResolution = float(aimedIOverSigmaAtHighestResolution)
    transmission = float(transmission)
    data_threshold = float(data_threshold)
    radius = float(radius)

    if resolution is None:
        resolution = collect.getResolution(beamline, token=token)
    else:
        resolution = float(resolution)

    if timeToReachHendersonLimit is None or timeToReachHendersonLimit == "null":
        logger.warning(
            "No time to reach Henderson limit available! Please enter manually a value."
        )
        timeToReachHendersonLimit = 0.0
    else:
        timeToReachHendersonLimit = float(timeToReachHendersonLimit)

    if flux is None:
        logger.warning("No flux available! Please enter manually a new flux value.")
        flux = 0.0
    else:
        flux = float(flux)
        if flux < 1e8:
            logger.warning("Flux is too low! Please enter manually a new flux value.")

    listDialog = [
        {
            "variableName": "diffractionSignalDetection",
            "label": "Diffraction signal detection",
            "type": "combo",
            "textChoices": [
                "DozorM2 (macro-molecules crystal detection)",
                "Dozor (macro-molecules)",
                "Spotfinder (small molecules)",
            ],
            "value": diffractionSignalDetection,
        },
        {
            "variableName": "resolution",
            "label": "Resolution",
            "type": "float",
            "value": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 10.0,
        },
        {
            "variableName": "data_threshold",
            "label": "Data threshold",
            "type": "float",
            "value": data_threshold,
            "unit": "",
            "lowerBound": 0.0,
            "upperBound": 1.0e8,
        },
        {
            "variableName": "redo_find_positions",
            "label": "Redo find positions?",
            "type": "boolean",
            "value": redo_find_positions,
        },
        {
            "variableName": "data_collection_exposure_time",
            "label": "Exposure time",
            "type": "float",
            "value": data_collection_exposure_time,
            "unit": "s",
            "lowerBound": config.get_value(beamline, "Beam", "minCollectExposureTime"),
            "upperBound": 50.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "value": transmission,
            "unit": "s",
            "lowerBound": 0.01,
            "upperBound": 100.0,
        },
        {
            "variableName": "data_collection_osc_range",
            "label": "Total oscillation range",
            "type": "float",
            "value": data_collection_osc_range,
            "unit": "degrees",
            "lowerBound": 0.01,
            "upperBound": 30.0,
        },
        {
            "variableName": "data_collection_no_images",
            "label": "No images",
            "type": "int",
            "value": data_collection_no_images,
            "lowerBound": 1,
            "upperBound": 10000.0,
            "useDefaults": False,
        },
        {
            "variableName": "data_collection_max_positions",
            "label": "Max no data collection positions",
            "type": "int",
            "value": data_collection_max_positions,
            "lowerBound": 1,
            "upperBound": 1000,
            "useDefaults": False,
        },
        {
            "variableName": "data_collection_iterations",
            "label": "Number of data collection iterations",
            "type": "int",
            "value": 1,
            "lowerBound": 1,
            "upperBound": 1000,
        },
        {
            "variableName": "radius",
            "label": "Radius",
            "type": "float",
            "value": radius,
            "unit": "pixels",
            "lowerBound": 0.1,
            "upperBound": 10.0,
        },
        {
            "variableName": "aimedIOverSigmaAtHighestResolution",
            "label": "Aimed I/Sigma at highest resolution",
            "type": "float",
            "value": aimedIOverSigmaAtHighestResolution,
            "lowerBound": 0.5,
            "upperBound": 10,
        },
        {
            "variableName": "inverseBeam",
            "label": "Inverse beam data collection",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "flux",
            "label": "Flux",
            "type": "float",
            "value": flux,
            "unit": "photos/s",
        },
        {
            "variableName": "timeToReachHendersonLimit",
            "label": "Time to reach Henderson limit",
            "type": "float",
            "value": round(timeToReachHendersonLimit, 1),
            "unit": "s",
        },
        {
            "variableName": "XDSAPP",
            "label": "XDSAPP auto-processing",
            "type": "boolean",
            "value": True,
        },
        {
            "variableName": "grenades_fastproc",
            "label": "Grenades_fastproc auto-processing",
            "type": "boolean",
            "value": True,
        },
        {
            "variableName": "grenades_parallelproc",
            "label": "Grenades_parallelproc auto-processing",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "EDNA_proc",
            "label": "EDNA_proc auto-processing",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "XIA2_DIALS",
            "label": "XIA2_DIALS auto-processing",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "autoPROC",
            "label": "autoPROC auto-processing",
            "type": "boolean",
            "value": False,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
