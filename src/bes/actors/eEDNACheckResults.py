#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "15/02/2021"

from bes.workflow_lib import path
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    run_number,
    mxv1ResultCharacterisationFile,
    doRefDataCollectionReview,
    lowResolution,
    highResolution,
    resolution=None,
    newDataCollection=False,
    token=None,
    runGphlWorkflow=False,
    edna_characterisation_success=False,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    if resolution is not None and resolution != "null":
        resolution = float(resolution)
    else:
        resolution = collect.getResolution(beamline, token=token)

    if runGphlWorkflow and not edna_characterisation_success:
        # We don't check for a second iteration in case of Gphl workflow execution
        # and EDNA characterisation failure
        newDataCollection = False
    elif newDataCollection:
        # This means that we have already done one iteration - we therefore stop
        newDataCollection = False
    else:
        mxv1ResultCharacterisation = path.readXSDataFile(mxv1ResultCharacterisationFile)
        higherResolutionDetected, newResolution = edna_mxv1.checkRankingResolution(
            beamline, mxv1ResultCharacterisation
        )
        logger.debug(
            "higherResolutionDetected: {0}, newResolution: {1}".format(
                higherResolutionDetected, newResolution
            )
        )
        if newResolution is not None and newResolution > lowResolution:
            logger.warning("Best strategy resolution is {0} [A]".format(newResolution))
            logger.warning(
                "The {0} beamline lowest resolution is {1} [A]".format(
                    beamline, lowResolution
                )
            )
            newResolution = lowResolution
            logger.warning(
                "The strategy resolution is therefore set to {0} [A]".format(
                    newResolution
                )
            )
        if higherResolutionDetected:
            newDataCollection = True
            rankingResolution = newResolution
            resolutionOffset = round(rankingResolution * 0.10, 2)
            newResolution = rankingResolution - resolutionOffset
            if newResolution > highResolution:
                logger.info(
                    "Recollecting reference images at new resolution %.2f"
                    % newResolution
                )
            else:
                logger.info(
                    "New resolution obtained from BEST = %.2f A" % newResolution
                )
                # Max resolution
                logger.warning("Limiting resolution to %.2f A" % highResolution)
                # Check that we are not close to current resolution
                if (
                    highResolution < (resolution - round(resolution * 0.1, 2))
                    and abs(highResolution - resolution) > 0.2
                ):
                    newResolution = highResolution
                else:
                    newDataCollection = False
            if newDataCollection:
                # run_number = run_number + 1
                doRefDataCollectionReview = False
                resolution = newResolution
        elif newResolution is not None and newResolution > 4.0 and resolution <= 3.0:
            logger.info("New resolution obtained from BEST = %.2f A" % newResolution)
            resolution = 4.0
            newDataCollection = True
            # run_number = run_number + 1
            logger.info(
                "Recollecting reference images at new resolution %.2f" % resolution
            )

    return {
        "newDataCollection": newDataCollection,
        "resolution": resolution,
        "highResolution": highResolution,
        "run_number": run_number,
        "doRefDataCollectionReview": doRefDataCollectionReview,
    }
