import math

from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    currentRH,
    endRH,
    step,
    steppingSign,
    automatic_mode,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    continue_dehydration = None
    currentRH = float(currentRH)
    endRH = float(endRH)
    step = float(step)
    steppingSign = int(steppingSign)
    logger.info("Current relative humidity: %.2f" % currentRH)
    logger.info("End relative humidity: %.2f" % endRH)
    if math.fabs(currentRH - endRH) > step or (
        math.fabs(currentRH - endRH) == step and (currentRH - endRH) * steppingSign < 0
    ):
        continue_dehydration = True
    else:
        logger.info("Stopping automatic dehydration")
        continue_dehydration = False

    return {
        "automatic_mode": automatic_mode,
        "continue_dehydration": continue_dehydration,
    }
