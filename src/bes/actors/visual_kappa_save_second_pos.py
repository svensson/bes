from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, directory, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        dict_diffractometer_positions = collect.readMotorPositions(
            beamline, token=token
        )

        logger.debug("Motor positions: %r" % dict_diffractometer_positions)

        sampx = dict_diffractometer_positions["sampx"]
        sampy = dict_diffractometer_positions["sampy"]
        phiy = dict_diffractometer_positions["phiy"]
        phiz = dict_diffractometer_positions["phiz"]

        if beamline == "id23eh2":
            logger.info(
                "Second position: sampx=%.3f mm, sampy=%.3f mm and phiz=%.3f mm"
                % (sampx, sampy, phiz)
            )
        else:
            logger.info(
                "Second position: sampx=%.3f mm, sampy=%.3f mm and phiy=%.3f mm"
                % (sampx, sampy, phiy)
            )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "sampx2": sampx,
        "sampy2": sampy,
        "phiy2": phiy,
        "phiz2": phiz,
    }
