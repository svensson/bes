#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"


from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    workflow_title,
    workflow_type,
    workflow_working_dir,
    workflow_id,
    message_text="No error message.",
    token=None,
    end_workflow_in_mxcube=True,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    logger.info("Workflow finished successfully.")
    edna_ispyb.setWorkflowStatus(beamline, workflow_working_dir, workflow_id, "Success")

    emailList = config.get_value(beamline, "Email", "success", default_value=[])
    common.send_email(
        beamline,
        emailList,
        "Success",
        directory,
        workflow_title,
        workflow_type,
        workflow_working_dir,
        message_text,
    )

    if end_workflow_in_mxcube:
        logger.debug("Ending workflow in MXCuBE")
        collect.workflowEnd(beamline, token=token)
        logger.debug("Workflow ended in MXCuBE")

    return {}
