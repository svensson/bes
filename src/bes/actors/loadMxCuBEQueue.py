import os
import time

from edna2.utils import UtilsPath

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging

from edna2.tasks.AutoProcessingWrappers import AutoPROCWrapper


def run(
    beamline,
    proposal,
    workflowParameters,
    directory,
    run_number,
    raw_directory,
    process_directory,
    prefix,
    expTypePrefix,
    mxv1StrategyResultFile,
    motorPositions,
    sampleInfo=None,
    firstImagePath=None,
    resolution=None,
    mxv1ResultCharacterisationFile=None,
    forcedSpaceGroup=None,
    do_data_collect=False,
    createNewDataCollectionGroup=None,
    suffix=None,
    highResolution=None,
    lowResolution=None,
    token=None,
    collection_software=None,
    workflow_index=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    process_partial_data_sets=False,
    list_partial_data_sets=None,
    moreSamples=False,
    doPseudoHelical=False,
    doOpenKappa=False,
    besParameters=None,
    **_,
):

    dataCollectionId = None
    dataCollectionGroupId = None
    expTypePrefix = ""
    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    motorPositions = common.jsonLoads(motorPositions)
    sampleInfo = common.jsonLoads(sampleInfo)
    mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)
    dictCellSpaceGroup = {}

    if besParameters:
        bes_request_id = besParameters["request_id"]
    else:
        bes_request_id = None

    if mxv1ResultCharacterisationFile is not None:
        mxv1ResultCharacterisation = path.readXSDataFile(mxv1ResultCharacterisationFile)
    else:
        mxv1ResultCharacterisation = None

    if sampleInfo is None:
        groupNodeId = None
        sampleNodeId = -1
    else:
        groupNodeId = sampleInfo["groupNodeId"]
        sampleNodeId = sampleInfo["nodeId"]

    (forcedSpaceGroup, newCharacterisationComment) = symmetry.checkForcedSpaceGroup(
        forcedSpaceGroup, sampleInfo, directory
    )
    if forcedSpaceGroup is not None:
        if (
            forcedSpaceGroup.lower() != "unknown"
            and forcedSpaceGroup.lower() != "undefined"
        ):
            if (
                sampleInfo is not None
                and "cellA" in sampleInfo
                and sampleInfo["cellA"] > 1.0
                and "cellB" in sampleInfo
                and sampleInfo["cellB"] > 1.0
                and "cellC" in sampleInfo
                and sampleInfo["cellC"] > 1.0
                and "cellAlpha" in sampleInfo
                and sampleInfo["cellAlpha"] > 1.0
                and "cellBeta" in sampleInfo
                and sampleInfo["cellBeta"] > 1.0
                and "cellGamma" in sampleInfo
                and sampleInfo["cellGamma"] > 1.0
            ):
                logger.debug(
                    "Forcing user defined space group and cell for auto processing: {0}".format(
                        sampleInfo
                    )
                )
                dictCellSpaceGroup["spaceGroup"] = sampleInfo["crystalSpaceGroup"]
                dictCellSpaceGroup["cell_a"] = sampleInfo["cellA"]
                dictCellSpaceGroup["cell_b"] = sampleInfo["cellB"]
                dictCellSpaceGroup["cell_c"] = sampleInfo["cellC"]
                dictCellSpaceGroup["cell_alpha"] = sampleInfo["cellAlpha"]
                dictCellSpaceGroup["cell_beta"] = sampleInfo["cellBeta"]
                dictCellSpaceGroup["cell_gamma"] = sampleInfo["cellGamma"]
            elif mxv1ResultCharacterisation is not None:
                # Try to make use of cell from EDNA characterisation:
                ednaCellSpaceGroup = (
                    edna_mxv1.getCellSpaceGroupFromCharacterisationResult(
                        beamline, mxv1ResultCharacterisation
                    )
                )
                if "spaceGroup" in ednaCellSpaceGroup:
                    if ednaCellSpaceGroup["spaceGroup"] == forcedSpaceGroup:
                        logger.debug(
                            "Forcing user defined space group and EDNA characterisation cell for auto processing: {0}".format(
                                forcedSpaceGroup
                            )
                        )
                        dictCellSpaceGroup = ednaCellSpaceGroup
                    else:
                        logger.debug(
                            "Space group not forced for auto processing due to different user defined and EDNA characterisation space groups."
                        )
                else:
                    logger.debug(
                        "Space group not forced for auto processing due to missing user defined and EDNA characterisation cell."
                    )
            else:
                logger.debug(
                    "Space group not forced for auto processing due to missing user defined and EDNA characterisation cell."
                )

    if resolution is not None:
        resolution = float(resolution)

    if not do_data_collect:
        # As of February 2022, MXCuBE 3 does not take into account 'enabled',
        # which means that a data collection will be launched even if
        # enabled is set to False. We must therefore give the user the
        # possibility to stop the workflow here.
        listDialog = [
            {
                "variableName": "proceedWithDataCollection",
                "label": "Proceed with data collection? If you stop here"
                + "the strategy will not be uploaded to the "
                + "MXCuBE queue.",
                "type": "boolean",
                "value": False,
            }
        ]
        dictValues = dialog.openDialog(
            beamline,
            listDialog,
            directory=directory,
            automaticMode=False,
            token=token,
            collection_software=collection_software,
        )
        proceedWithDataCollection = dictValues["proceedWithDataCollection"]
        enabled = proceedWithDataCollection
    else:
        logger.info("Automatic data collection enabled.")
        enabled = True
        proceedWithDataCollection = True
        # Check if this is a two-sweep collection
        if (
            edna_mxv1.isMutipleSweepDataCollection(mxv1StrategyResult)
            and firstImagePath is not None
        ):
            newComment = "2 passes collected : low and high resolution."
            ispyb.updateDataCollectionGroupComment(beamline, firstImagePath, newComment)

    if proceedWithDataCollection:
        if createNewDataCollectionGroup is not None:
            groupNodeId = collect.createNewDataCollectionGroup(
                beamline,
                "Diffraction plan",
                workflowParameters["index"],
                enabled,
                sampleNodeId,
                token=token,
            )

        # Find unique run number for data collect
        directory, run_number = path.createUniqueICATDirectory(
            raw_directory=raw_directory,
            run_number=run_number,
            workflow_index=workflow_index,
        )
        # data_collect_run_number = path.findUniqueRunNumber(
        #     raw_directory, prefix, expTypePrefix, run_number=1
        # )

        firstImagePath = edna_mxv1.getListImagePath(
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            bFirstImageOnly=True,
        )[0]
        logger.info("Path to first image: {0}".format(firstImagePath))
        if beamline in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
            firstImagePath = os.path.splitext(firstImagePath)[0] + ".h5"
            logger.info("ISPyB path to first image: {0}".format(firstImagePath))

        dictId = collect.loadQueue(
            beamline=beamline,
            directory=directory,
            process_directory=process_directory,
            prefix=prefix,
            expTypePrefix=expTypePrefix,
            run_number=run_number,
            resolution=resolution,
            mxv1StrategyResult=mxv1StrategyResult,
            motorPositions=motorPositions,
            workflow_type=workflowParameters["type"],
            workflow_index=workflowParameters["index"],
            group_node_id=groupNodeId,
            sample_node_id=sampleNodeId,
            dictCellSpaceGroup=dictCellSpaceGroup,
            enabled=enabled,
            snapShots=True,
            highResolution=highResolution,
            lowResolution=lowResolution,
            token=token,
            workflow_name=workflowParameters["title"],
            bes_request_id=bes_request_id,
            characterisation_id=characterisation_id,
            kappa_settings_id=kappa_settings_id,
            position_id=position_id,
        )

        if enabled:
            logger.info("Data collection done.")
            # Give some time for data to be uploaded to ISPyB
            time.sleep(5)
            if dictCellSpaceGroup != {}:
                if suffix is None:
                    suffix = "cbf"
                if "cell_a" in dictCellSpaceGroup:
                    newComment = "Auto processing (except Grenades): Forced space group {spaceGroup} and cell (a={cell_a:.1f}, b={cell_b:.1f}, c={cell_c:.1f}, alpha={cell_alpha:.1f}, beta={cell_beta:.1f}, gamma={cell_gamma:.1f}).".format(
                        **dictCellSpaceGroup
                    )
                else:
                    newComment = "autoPROC auto processing: Forced space group {spaceGroup}.".format(
                        **dictCellSpaceGroup
                    )
                dataCollectionId = ispyb.updateDataCollectionComment(
                    beamline, firstImagePath, newComment
                )
                dataCollectionGroupId = ispyb.updateDataCollectionGroupComment(
                    beamline, firstImagePath, newComment
                )
                logger.debug("Data collection id: {0}".format(dataCollectionId))
                logger.debug(
                    "Data collection group id: {0}".format(dataCollectionGroupId)
                )
            else:
                dataCollectionWS3VO = (
                    ispyb.findDataCollectionFromFileLocationAndFileName(
                        beamline, firstImagePath
                    )
                )
                if dataCollectionWS3VO is None:
                    raise RuntimeError(
                        "Cannot find data collection for {0} in ISPyB - the data collection most likely failed.".format(
                            firstImagePath
                        )
                    )
                dataCollectionId = dataCollectionWS3VO.dataCollectionId
                dataCollectionGroupId = dataCollectionWS3VO.dataCollectionGroupId
                logger.debug("Data collection id: {0}".format(dataCollectionId))
                logger.debug(
                    "Data collection group id: {0}".format(dataCollectionGroupId)
                )

        else:
            logger.warning(
                "Optimal strategy has been calculated and been transferred to the mxCuBE queue."
            )

        lastNodeId = None
        if "listNodeId" in dictId:
            for nodeId in dictId["listNodeId"]:
                if lastNodeId is None or nodeId > lastNodeId:
                    lastNodeId = nodeId
        groupNodeId = dictId["group_node_id"]
        sampleInfo["groupNodeId"] = groupNodeId

        logger.debug("Last node id: %r" % lastNodeId)

        if list_partial_data_sets is None:
            list_partial_data_sets = []
        list_partial_data_sets.append(directory)

        if len(list_partial_data_sets) > 1:
            if (process_partial_data_sets and doOpenKappa) or (
                doPseudoHelical and not moreSamples
            ):
                processed_data_dir = UtilsPath.getProcessedDataPath(
                    directory, raise_error=True
                )
                list_runs = "runs"
                for data_set in list_partial_data_sets:
                    data_set_run_number = data_set.split("_")[-2]
                    list_runs += "_" + data_set_run_number
                autoproc_processed_data_dir = (
                    processed_data_dir.parent / f"autoPROC_merge_{list_runs}"
                )
                working_dir = autoproc_processed_data_dir / "nobackup"
                working_dir.mkdir(mode=0o755, parents=True, exist_ok=False)
                logger.info(
                    f"autoPROC merged processing data dir: {autoproc_processed_data_dir}"
                )
                in_data = {
                    "beamline": beamline,
                    "proposal": proposal,
                    "raw_data": list_partial_data_sets,
                    "icat_processed_data_dir": str(autoproc_processed_data_dir),
                    "workingDirectory": str(working_dir),
                }
                # autoProcessingWrappers = AutoPROCWrapper(inData=in_data)
                # autoProcessingWrappers.execute()
                environment = {
                    "EDNA2_SITE": os.environ["EDNA_SITE"],
                    "ISPyB_user": os.environ["ISPyB_user"],
                    "ISPyB_pass": os.environ["ISPyB_pass"],
                }
                AutoPROCWrapper.launch_on_slurm(
                    working_dir=working_dir,
                    in_data=in_data,
                    partition="mx",
                    environment=environment,
                    list_modules=["AutoProcessingLaunchers", "autoPROC"],
                )
                list_partial_data_sets = None
                process_partial_data_sets = False

    # Get current resolution
    resolution = collect.getResolution(beamline, token=token)

    return {
        "expTypePrefix": expTypePrefix,
        "sampleInfo": sampleInfo,
        "dataCollectionId": dataCollectionId,
        "dataCollectionGroupId": dataCollectionGroupId,
        "resolution": resolution,
        "directory": directory,
        "run_number": run_number,
        "last_data_collection_directory": directory,
        "process_partial_data_sets": process_partial_data_sets,
        "list_partial_data_sets": list_partial_data_sets,
    }
