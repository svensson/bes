import math
import base64

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import collect


def run(
    directory,
    beamline,
    motorPositions,
    gridOscillationRange,
    run_number,
    prefix,
    suffix,
    workflowParameters,
    shape=None,
    token=None,
    **_,
):

    workingDir = workflowParameters["workingDir"]

    # Read current grid data

    if beamline == "id30a2":
        grid_info = {
            "x1": -0.5,
            "dx_mm": 1.0,
            "dy_mm": 1.0,
            "y1": -0.5,
            "steps_y": 20,
            "steps_x": 20,
            "id": "G2",
        }
    else:
        grid_info = collect.getGridInfo(beamline, token=token, shape=shape)

    grid_id = grid_info["id"]

    # Create dummy grid scan results
    expTypePrefix = "mesh"
    meshZigZag = False
    multi_wedge = False
    meshPositions = grid.determineMeshPositions(
        beamline,
        motorPositions,
        gridOscillationRange,
        int(run_number),
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
        meshZigZag,
        multi_wedge=multi_wedge,
    )

    nx = grid_info["steps_x"]
    ny = grid_info["steps_y"]
    for position in meshPositions:
        x = position["indexY"]
        y = position["indexZ"]
        dx = x - nx / 2
        dy = y - ny / 2
        radius_from_centre = math.sqrt(dx * dx + dy * dy)
        if radius_from_centre == 0:
            radius_from_centre = 1
        position["dozor_score"] = 100.0 / math.sqrt(radius_from_centre)

    meshPositionFile = path.createNewJsonFile(
        workingDir, meshPositions, "meshPositions"
    )

    plot_path = grid.create_image(grid_info, meshPositions, [], workingDir)

    # Set grid data
    base64data = base64.b64encode(open(plot_path, "rb").read()).decode("utf-8")
    collect.setImageGridData(beamline, base64data, key=grid_id, token=token)

    return {
        "grid_info": grid_info,
        "meshPositionFile": meshPositionFile,
        "plot_path": plot_path,
    }
