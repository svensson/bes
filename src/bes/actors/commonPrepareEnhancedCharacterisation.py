def run(beamline, **_):

    return {
        "workflow_title": "Enhanced characterisation on %s" % beamline,
        "workflow_type": "EnhancedCharacterisation",
        "doRefDataCollectionReview": False,
        "no_reference_images": 2,
        "createNewDataCollectionGroup": True,
    }
