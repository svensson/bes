"""
Created on Feb 17, 2015

@author: svensson
"""

import os
import shutil
import pathlib

from bes.workflow_lib import icat
from bes import config
from bes.workflow_lib import edna2_tasks

from edna2.utils import UtilsPath
from edna2.utils import UtilsImage

from pyicat_plus.client.main import IcatClient


def run(image_path, jpeg_path, jpeg_thumbnail_path, **_):

    thumbnailsCreated = False

    raw_data_dir = pathlib.Path(image_path).parent
    dir_parts = list(raw_data_dir.parts)
    if "RAW_DATA" in dir_parts:
        index_raw_data = dir_parts.index("RAW_DATA")
        dir_parts[index_raw_data] = "PROCESSED_DATA"
    processed_data_dir = pathlib.Path(*dir_parts).parent
    image_no = UtilsImage.getImageNumber(image_path)
    pre_prefix, run_number = UtilsImage.splitPrefixRunNumber(image_path)
    icat_dataset_name = f"diffraction_thumbnail_{run_number}_{image_no:04d}"
    sample_name = icat.get_sample_name(raw_data_dir)
    icat_dir = processed_data_dir / icat_dataset_name
    index = 1
    while icat_dir.exists():
        icat_dataset_name = (
            f"diffraction_thumbnail_{run_number}_{image_no:04d}_{index:02d}"
        )
        icat_dir = processed_data_dir / icat_dataset_name
        index += 1
    beamline, proposal = UtilsPath.getBeamlineProposal(raw_data_dir)
    thumbnail_working_dir = icat_dir / "nobackup"
    thumbnail_working_dir.mkdir(mode=0o755, exist_ok=False, parents=True)
    thumbnail_working_dir = str(thumbnail_working_dir)

    edna2_tasks.create_thumbnails_for_pyarch([image_path], thumbnail_working_dir)

    if os.path.exists(jpeg_path) and os.path.exists(jpeg_thumbnail_path):
        thumbnailsCreated = True

        # Store in ICAT
        gallery_dir = icat_dir / "gallery"
        gallery_dir.mkdir(parents=True, exist_ok=False, mode=0o755)
        suffix = pathlib.Path(jpeg_path).suffix
        icat_jpeg_path = gallery_dir / f"diffraction_thumbnail_large{suffix}"
        icat_jpeg_thumbnail_path = gallery_dir / f"diffraction_thumbnail{suffix}"
        shutil.copy(jpeg_thumbnail_path, icat_jpeg_thumbnail_path)
        shutil.copy(jpeg_path, icat_jpeg_path)

        icat_beamline = UtilsPath.getIcatBeamline(beamline)
        metadata_urls = config.get_value(
            beamline, "ICAT", "metadata_urls", default_value=[]
        )
        if len(metadata_urls) > 0:
            metadata = {
                "scanType": "diffraction_thumbnail",
                "Sample_name": sample_name,
            }
            client = IcatClient(metadata_urls=metadata_urls)
            client.store_processed_data(
                beamline=icat_beamline,
                proposal=proposal,
                dataset=icat_dataset_name,
                path=str(icat_dir),
                metadata=metadata,
                raw=[str(raw_data_dir)],
            )

    return {
        "thumbnailsCreated": thumbnailsCreated,
        "thumbnail_working_dir": thumbnail_working_dir,
    }
