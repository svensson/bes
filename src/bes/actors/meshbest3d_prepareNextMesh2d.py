import time

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

EPSILON = 2.5


def run(
    beamline,
    workflowParameters,
    directory,
    indexMesh2d,
    run_number,
    newPhi=None,
    noMesh2d=None,
    token=None,
    meshResultsJsonPath=None,
    **_,
):

    try:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        motorPositions = collect.readMotorPositions(beamline, token=token)
        indexMesh2d = int(indexMesh2d)

        phi = motorPositions["phi"]
        run_number = int(run_number)

        currentPhi = phi
        if newPhi is None and noMesh2d is not None:
            newPhi = currentPhi + int(180.0 / noMesh2d)
        logger.info(
            "Rotating sample omega from %.3f to %.3f degrees" % (currentPhi, newPhi)
        )

        collect.moveMotors(beamline, directory, {"phi": newPhi}, token=token)
        time.sleep(1)

        dict_diffractometer_positions = collect.readMotorPositions(
            beamline, token=token
        )
        phi = dict_diffractometer_positions["phi"]

        logger.info("Omega position after move: phi=%.3f" % phi)
        if not beamline == "simulator":
            deltaPhi = abs(phi - newPhi)
            while deltaPhi > (360.0 - EPSILON):
                deltaPhi -= 360.0
            logger.debug("deltaPhi=%.3f" % deltaPhi)
            if deltaPhi > EPSILON:
                raise Exception("Sample didn't move to new omega position!")
            logger.info("Sample moved to the new omega position")
        else:
            logger.info("Simulator: sample omega not moved to the new position")

        # run_number += 1

        indexMesh2d += 1
        if indexMesh2d == noMesh2d:
            doMesh2d = False
        else:
            doMesh2d = True

        titleMeshHtmlPage = (
            "MeshBest 3D mesh no {0}: centre of sample at phi={1:.1f} deg".format(
                indexMesh2d, phi
            )
        )
        meshHtmlDirectoryName = "MeshBest3D_{0}".format(indexMesh2d)

        if meshResultsJsonPath is not None:
            meshResultsJsonPath1 = meshResultsJsonPath
        else:
            meshResultsJsonPath1 = None

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "motorPositions": dict_diffractometer_positions,
        "run_number": run_number,
        "titleMeshHtmlPage": titleMeshHtmlPage,
        "meshHtmlDirectoryName": meshHtmlDirectoryName,
        "indexMesh2d": indexMesh2d,
        "doMesh2d": doMesh2d,
        "meshResultsJsonPath1": meshResultsJsonPath1,
    }
