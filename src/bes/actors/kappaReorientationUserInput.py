from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    characterisationOscillationRange,
    characterisationExposureTime,
    transmission,
    automatic_mode=False,
    resolution=None,
    collection_software=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    characterisationExposureTime = float(characterisationExposureTime)
    characterisationOscillationRange = float(characterisationOscillationRange)
    transmission = float(transmission)

    if resolution is None:
        resolution = collect.getResolution(beamline, token=token)
    else:
        resolution = float(resolution)

    listSpaceGroups = symmetry.getListOfChiralSpaceGroups()
    listSpaceGroups.insert(0, "None")

    listDialog = [
        {
            "variableName": "kappaStrategyOption",
            "label": "Kappa strategy option",
            "type": "combo",
            "defaultValue": "Cell",
            "textChoices": [
                "Cell",
                "Anomalous",
                "SmartSpotSeparation",
                "SmallestOverallOscillation",
            ],
        },
        {
            "variableName": "forcedSpaceGroup",
            "label": "Forced space group",
            "type": "combo",
            "defaultValue": "None",
            "textChoices": listSpaceGroups,
        },
        {
            "variableName": "no_reference_images",
            "label": "Number of reference images",
            "type": "int",
            "defaultValue": 2,
            "unit": "",
            "lowerBound": 0.0,
            "upperBound": 4.0,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "combo",
            "defaultValue": "90",
            "textChoices": ["30", "45", "60", "90"],
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": characterisationExposureTime,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "characterisationOscillationRange",
            "label": "Characterisation oscillation range",
            "type": "float",
            "value": characterisationOscillationRange,
            "unit": "%",
            "lowerBound": 0.1,
            "upperBound": 10.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "value": transmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "resolution",
            "label": "Resolution",
            "type": "float",
            "defaultValue": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 7.0,
        },
        {
            "variableName": "do_data_collect",
            "label": "Do data collection?",
            "type": "boolean",
            "value": False,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
