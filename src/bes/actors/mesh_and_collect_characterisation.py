"""
Workflow module for mesh and collect characterisations
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2015"
__updated__ = "2015-11-30"

from bes.workflow_lib import path
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect

# from bes.workflow_lib import edna_kernel
from bes.workflow_lib import workflow_logging

# from bes.workflow_lib.edna_mxv1 import XSDataDiffractionPlan
# from bes.workflow_lib.edna_mxv1 import XSDataTime
# from bes.workflow_lib.edna_mxv1 import XSDataAngularSpeed
# from bes.workflow_lib.edna_mxv1 import XSDataAngle
# from bes.workflow_lib.edna_mxv1 import XSDataLength
# from bes.workflow_lib.edna_mxv1 import XSDataDouble
# from bes.workflow_lib.edna_mxv1 import XSDataInterfacev1_2
# from bes.workflow_lib.edna_mxv1 import XSDataSampleCrystalMM
# from bes.workflow_lib.edna_mxv1 import XSDataSize
# from bes.workflow_lib.edna_mxv1 import XSDataString
# from bes.workflow_lib.edna_mxv1 import XSDataFile
# from bes.workflow_lib.edna_mxv1 import XSDataFloat


def run(
    beamline,
    workflowParameters,
    directory,
    allPositionFile,
    transmission,
    aimedIOverSigmaAtHighestResolution=1.0,
    flux=None,
    beamSizeAtSampleX=None,
    beamSizeAtSampleY=None,
    resolution=None,
    crystalSizeX=None,
    crystalSizeY=None,
    crystalSizeZ=None,
    dozorVisibleResolution=None,
    data_collection_no_images=100,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    timeToReachHendersonLimit = None
    data_collection_exposure_time = None

    allPositions = path.parseJsonFile(allPositionFile)
    aimedIOverSigmaAtHighestResolution = float(aimedIOverSigmaAtHighestResolution)

    defaultCollectExposureTime = config.get_value(
        beamline, "Beam", "defaultCollectExposureTime"
    )
    defaultCollectTransmission = config.get_value(
        beamline, "Beam", "defaultCollectTransmission"
    )

    if dozorVisibleResolution is None:
        resolution = collect.getResolution(beamline, token=token)
    else:
        resolution = dozorVisibleResolution
        logger.info(
            "Data collection resolution set to {0:.2f} A from mesh scan".format(
                resolution
            )
        )

    if beamSizeAtSampleX is None:
        beamSizeAtSampleX = 0.050
    else:
        beamSizeAtSampleX = float(beamSizeAtSampleX)

    if beamSizeAtSampleY is None:
        beamSizeAtSampleY = 0.050
    else:
        beamSizeAtSampleY = float(beamSizeAtSampleY)

    if crystalSizeX is not None:
        crystalSizeX = float(crystalSizeX)
    if crystalSizeY is not None:
        crystalSizeY = float(crystalSizeY)
    if crystalSizeZ is not None:
        crystalSizeZ = float(crystalSizeZ)

    data_collection_no_images = int(data_collection_no_images)

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    logger.info("Running EDNA characterisation")
    allPositions = common.jsonLoads(allPositions)

    # Disabled characterisation as of 2020/10/28
    #     # Only try the 10 strongest positions
    #     for position in allPositions[:10]:
    #
    #         crystalSizeX = min(position["crystal_size_x"], position["crystal_size_y"])
    #         crystalSizeY = crystalSizeX
    #         crystalSizeZ = crystalSizeX
    #
    #         xsDataDiffractionPlan = XSDataDiffractionPlan()
    #         xsDataDiffractionPlan.minExposureTimePerImage = XSDataTime(beamline_parameters.DICT_PARAMETER[beamline]["minExposureTime"])
    #         xsDataDiffractionPlan.goniostatMaxOscillationSpeed = XSDataAngularSpeed(beamline_parameters.DICT_PARAMETER[beamline]["maxOscillationSpeed"])
    #         xsDataDiffractionPlan.goniostatMinOscillationWidth = XSDataAngle(beamline_parameters.DICT_PARAMETER[beamline]["minOscillationWidth"])
    #         xsDataDiffractionPlan.detectorDistanceMin = XSDataLength(beamline_parameters.DICT_PARAMETER[beamline]["detectorDistanceMin"])
    #         xsDataDiffractionPlan.detectorDistanceMax = XSDataLength(beamline_parameters.DICT_PARAMETER[beamline]["detectorDistanceMax"])
    #         xsDataDiffractionPlan.minTransmission = XSDataDouble(beamline_parameters.DICT_PARAMETER[beamline]["minTransmission"])
    #         xsDataDiffractionPlan.aimedIOverSigmaAtHighestResolution = XSDataDouble(aimedIOverSigmaAtHighestResolution)
    #
    #         xsDataInputInterface = XSDataInterfacev1_2.XSDataInputInterface()
    #         if flux is not None and flux != "null":
    #             xsDataInputInterface.flux = XSDataFloat(flux)
    #
    #         xsDataInputInterface.transmission = XSDataDouble(transmission)
    #
    #         if beamSizeAtSampleX is not None and beamSizeAtSampleY is not None:
    #             xsDataInputInterface.beamSizeX = XSDataLength(beamSizeAtSampleX)
    #             xsDataInputInterface.beamSizeY = XSDataLength(beamSizeAtSampleY)
    #
    #         if crystalSizeX is not None and crystalSizeY is not None and crystalSizeZ is not None:
    #             xsDataSampleCrystalMM = XSDataSampleCrystalMM()
    #             xsDataSize = XSDataSize()
    #             xsDataSize.x = XSDataLength(crystalSizeX)
    #             xsDataSize.y = XSDataLength(crystalSizeY)
    #             xsDataSize.z = XSDataLength(crystalSizeZ)
    #             xsDataSampleCrystalMM.size = xsDataSize
    #             xsDataInputInterface.sample = xsDataSampleCrystalMM
    #
    #
    #         xsDataInputInterface.diffractionPlan = xsDataDiffractionPlan
    #
    #         if "imageName" in position:
    #             imageName = position["imageName"]
    #         else:
    #             imageName = position["nearest_image"]["imageName"]
    #         imagePath = os.path.join(directory, imageName)
    #         xsDataInputInterface.addImagePath(XSDataFile(XSDataString(imagePath)))
    #
    #         logger.warning("Running EDNA characterisation...")
    #         xsDataResultInterface, ednaLogPath = edna_kernel.executeEdnaPlugin("EDPluginControlInterfacev1_2",
    #                                                               xsDataInputInterface,
    #                                                               workflowParameters["workingDir"])
    #         resultCharacterisation = xsDataResultInterface.resultCharacterisation
    #         if resultCharacterisation is not None:
    #             logger.info("Characterisation short summary: \n%s" % resultCharacterisation.shortSummary.value)
    #             strategyResult = resultCharacterisation.strategyResult
    #             if strategyResult is not None:
    #                 listCollectionPlan = strategyResult.collectionPlan
    #                 if len(listCollectionPlan) > 0:
    #                     collectionPlan = listCollectionPlan[0]
    #                     collectionStrategy = collectionPlan.collectionStrategy
    #                     if collectionStrategy is not None:
    #                         listSubWedge = collectionStrategy.subWedge
    #                         if len(listSubWedge) > 0:
    #                             subWedge = listSubWedge[0]
    #                             experimentalCondition = subWedge.experimentalCondition
    #                             beam = experimentalCondition.beam
    #                             xsDataTransmission = beam.transmission
    #                             if xsDataTransmission is not None:
    #                                 transmission = xsDataTransmission.value
    #                     strategySummary = collectionPlan.strategySummary
    #                     if strategySummary is not None:
    #                         resolution = strategySummary.resolution.value
    #                 xsDataTimeToReachHendersonLimit = strategyResult.timeToReachHendersonLimit
    #                 if xsDataTimeToReachHendersonLimit is not None:
    #                     timeToReachHendersonLimit = xsDataTimeToReachHendersonLimit.value
    #                     logger.info("Time to reach Henderson limit: %.1f s" % timeToReachHendersonLimit)
    #                     break

    # if timeToReachHendersonLimit is not None:
    #     data_collection_exposure_time = timeToReachHendersonLimit / data_collection_no_images

    if data_collection_exposure_time is None:
        data_collection_exposure_time = defaultCollectExposureTime
        transmission = defaultCollectTransmission

    return {
        "timeToReachHendersonLimit": timeToReachHendersonLimit,
        "data_collection_exposure_time": data_collection_exposure_time,
        "transmission": transmission,
        "resolution": resolution,
    }
