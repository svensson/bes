import os.path
import sys
import traceback

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import edna2_tasks
from bes.workflow_lib import workflow_logging
from bes.workflow_lib import doublemesh_lib


def run(
    beamline,
    workflowParameters,
    directory,
    grid_info,
    firstImagePath,
    meshZigZag,
    isHorizontalRotationAxis,
    phi1,
    phi2,
    token,
    dozorAllFile1,
    dozorAllFile,
    motorInitialPositions,
    numberOfPositions=None,
    reject_level=None,
    **_,
):
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    current_motor_positions = collect.readMotorPositions(beamline, token=token)

    # DozorM2 parameters
    workflow_working_dir = workflowParameters["workingDir"]
    listApertureSizes = config.get_value(beamline, "Beam", "apertureSizes")
    nominalBeamSizeX = config.get_value(beamline, "Beam", "nominalSizeX")
    nominalBeamSizeY = config.get_value(beamline, "Beam", "nominalSizeY")
    if reject_level is None:
        reject_level = config.get_value(
            beamline, "Mesh", "rejectLevel", default_value=40
        )
    loop_thickness = config.get_value(
        beamline, "Mesh", "loopThickness", default_value=500
    )
    logger.debug(f"workflow_working_dir: {workflow_working_dir}")

    outDataDozorM2 = edna2_tasks.runDozorM2(
        beamline=beamline,
        firstImagePath=firstImagePath,
        grid_info=grid_info,
        meshZigZag=meshZigZag,
        listDozorAllFile=[dozorAllFile1, dozorAllFile],
        listApertureSizes=listApertureSizes,
        nominalBeamSizeX=nominalBeamSizeX,
        nominalBeamSizeY=nominalBeamSizeY,
        workingDirectory=workflow_working_dir,
        isHorizontalRotationAxis=isHorizontalRotationAxis,
        phiValues=[phi1, phi2],
        motorPositions=motorInitialPositions,
        reject_level=reject_level,
        loop_thickness=loop_thickness,
        workingDirectorySuffix="two_meshes",
    )

    # Check output of DozorM2
    dozorm2_failure = False
    unsuccessful_scans = False
    if outDataDozorM2 is None:
        dozorm2_failure = True
    elif "dictCoord" not in outDataDozorM2:
        dozorm2_failure = True
    elif "unsuccessful_scans" in outDataDozorM2["dictCoord"]:
        # Check that we don't have "unsuccessful_scans"
        unsuccessful_scans = outDataDozorM2["dictCoord"]["unsuccessful_scans"]
        if unsuccessful_scans:
            dozorm2_failure = True

    all_positions = []
    if not dozorm2_failure:
        try:
            logger.info("Now running analyseDoubleMeshscan...")
            dozorm2_working_directory = outDataDozorM2["workingDirectory"]
            collect_positions, potentialMatches = doublemesh_lib.analyseDoubleMeshscan(
                dozorm2_working_directory
            )
            if beamline == "id30a2":
                collect_positions = potentialMatches[:, 7:]
            logger.info("Done running analyseDoubleMeshscan.")

            # dozorm_pair_final_name = "dozorm_pair_final.dat"
            # dozorm_pair_final_path = os.path.join(
            #     dozorm2_working_directory, dozorm_pair_final_name
            # )
            # if os.path.exists(dozorm_pair_final_path):
            #     with open(dozorm_pair_final_path) as file_descriptor:
            #         data = file_descriptor.read()
            #     logger.info("")
            #     logger.info("Positions as calculated by analyseDoubleMeshscan:")
            #     logger.info(data)
            #     logger.info("")

            if len(collect_positions) > 0:
                # logger.info("")
                # logger.info("Positions found by analyseDoubleMeshscan:")
                # logger.info("")
                # logger.info(collect_positions)

                phiz = current_motor_positions["phiz"]
                for collect_position in collect_positions:
                    resolution = collect_position[0]
                    beam_size = collect_position[1]
                    sampx = collect_position[2]
                    sampy = collect_position[3]
                    phiy = collect_position[4]
                    # logger.info(
                    #    "Resolution: %s, beam size: %s, sampx: %s, sampy: %s, phiy: %s",
                    #    resolution,
                    #    beam_size,
                    #    sampx,
                    #    sampy,
                    #    phiy,
                    # )
                    position = {
                        "sampx": sampx,
                        "sampy": sampy,
                        "phiy": phiy,
                        "phiz": phiz,
                        "beam_size": beam_size,
                        "dozor2m_resolution": resolution,
                    }
                    all_positions.append(position)

                # logger.info("")
            else:
                logger.warning("")
                logger.warning("No positions found by analyseDoubleMeshscan!")
                logger.warning("")

        except Exception as e:
            logger.error("Error when running analyseDoubleMeshscan:")
            logger.error(e)
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            logger.error("%s %s", exc_type, exc_value)
            listTrace = traceback.extract_tb(exc_traceback)
            logger.error("Traceback (most recent call last): %s", os.linesep)
            for listLine in listTrace:
                logger.error(
                    '  File "%s", line %d, in %s%s',
                    listLine[0],
                    listLine[1],
                    listLine[2],
                    os.linesep,
                )

    bestPosition = None
    if not dozorm2_failure and all_positions:
        if numberOfPositions is not None:
            all_positions = all_positions[:numberOfPositions]
        if all_positions:
            bestPosition = all_positions[0]
        else:
            # Take the first position
            if "dictCoord" in outDataDozorM2:
                listCoord = outDataDozorM2["dictCoord"]["coord"]
                if listCoord is not None and len(listCoord) > 0:
                    bestPosition = listCoord[0]

    if bestPosition:
        newMotorPositions = {
            "sampx": bestPosition["sampx"],
            "sampy": bestPosition["sampy"],
            "phiy": bestPosition["phiy"],
        }
    else:
        newMotorPositions = {}

    # Write the log file to logger
    if "logPath" in outDataDozorM2 and os.path.exists(outDataDozorM2["logPath"]):
        with open(outDataDozorM2["logPath"]) as fd:
            logText = fd.read()
            logger.info("")
            logger.info("DozorM2 working directory:")
            logger.info(os.path.dirname(outDataDozorM2["logPath"]))
            logger.info("")
            logger.info("DozorM2 log text:\n\n%s", logText)

    # Save all positions to file
    allPositionFile = path.createNewJsonFile(
        workflow_working_dir, all_positions, "all_positions_"
    )

    dozormTargetApertureSize = None
    more_samples = False
    if dozorm2_failure:
        if unsuccessful_scans:
            collection_comment = "Bad /weak diffraction! Please try to recollect using longer exposure and lower resolution."
        else:
            collection_comment = "Error occured during processing - workflow aborted."
    else:
        nb_positions = len(all_positions)
        if nb_positions == 0:
            collection_comment = "No positions detected by DozorM2."
        else:
            more_samples = True
            # Aperture size for first position (float to int)
            dozormTargetApertureSize = int(all_positions[0]["dozor2m_resolution"])
            if nb_positions == 1:
                collection_comment = "One position detected by DozorM2."
            else:
                collection_comment = f"{nb_positions} positions detected by DozorM2."

    ispyb.updateDataCollectionComment(beamline, firstImagePath, collection_comment)

    if dozorm2_failure and not unsuccessful_scans:
        raise RuntimeError(collection_comment)

    return {
        # "outDataDozorM2": outDataDozorM2,
        "newMotorPositions": newMotorPositions,
        "do1DXrayCentring": True,
        "allPositionFile": allPositionFile,
        "data_collection_max_positions": len(all_positions),
        "remainingPositions": all_positions,
        "moreSamples": more_samples,
        "dozormTargetApertureSize": dozormTargetApertureSize,
    }
