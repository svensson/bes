#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "01/03/2021"

import os

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    raw_directory,
    process_directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    motorPositions,
    gridExposureTime,
    gridOscillationRange,
    transmission,
    lineScanLength,
    lineScanOverSampling,
    isHorizontalRotationAxis,
    workflow_index=None,
    beamSizeAtSampleX=0.05,
    beamSizeAtSampleY=0.05,
    shortLineScan=False,
    gridTransmission=None,
    allowZeroDegreeMeshScan=False,
    token=None,
    lineScanStep=0,
    bestPosition=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    expTypePrefix = "line-"
    directory, run_number = path.createUniqueICATDirectory(
        raw_directory=raw_directory,
        run_number=run_number,
        workflow_index=workflow_index,
        expTypePrefix=expTypePrefix,
    )

    motorPositions = common.jsonLoads(motorPositions)
    workflowParameters = common.jsonLoads(workflowParameters)
    gridExposureTime = float(gridExposureTime)
    gridOscillationRange = float(gridOscillationRange)
    run_number = int(run_number)
    beamSizeAtSampleX = float(beamSizeAtSampleX)
    beamSizeAtSampleY = float(beamSizeAtSampleY)
    if bestPosition is not None and "beam_size" in bestPosition:
        beamSizeAtSampleX = bestPosition["beam_size"] / 1000
        beamSizeAtSampleY = beamSizeAtSampleX

    logger.info("Preparing vertical line scan")

    if shortLineScan:
        shortLineScanLength = 50 * beamSizeAtSampleY / 4
        if lineScanLength > shortLineScanLength:
            lineScanLength = shortLineScanLength
        lineScanStep = beamSizeAtSampleY / 4
        grid_info = grid.getSmallXrayCentringGrid(
            beamSizeAtSampleX,
            beamSizeAtSampleY,
            do1D=True,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
            lineScanStep=lineScanStep,
            lineScanLength=lineScanLength,
        )
    else:
        grid_info = grid.getLineScanGridInfo(
            beamX=beamSizeAtSampleX,
            beamY=beamSizeAtSampleY,
            length=lineScanLength,
            overSampling=lineScanOverSampling,
            isOrthogonal=True,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
        )

    # Check minimum oscillation range
    if not allowZeroDegreeMeshScan:
        maxDistPerDegree = config.get_value(
            beamline, "Goniometer", "maxDistPerDegree", default_value=None
        )
        if maxDistPerDegree is not None:
            dy_mm = grid_info["dy_mm"]
            if dy_mm / gridOscillationRange > maxDistPerDegree:
                logger.warning(
                    "Oscillation range %.1f degrees too short for beamline %s!"
                    % (gridOscillationRange, beamline)
                )
                gridOscillationRange = dy_mm / maxDistPerDegree
                logger.warning(
                    "New oscillation range: %.1f degrees" % gridOscillationRange
                )
        minOscillationWidth = config.get_value(
            beamline, "Goniometer", "minOscillationWidth"
        )
        noStepsX = grid_info["steps_x"]
        noStepsY = grid_info["steps_y"]
        if gridOscillationRange / max(noStepsX, noStepsY) < minOscillationWidth:
            logger.warning(
                "Oscillation range %.3f degrees too short for beamline %s!"
                % (gridOscillationRange / max(noStepsX, noStepsY), beamline)
            )
            gridOscillationRange = minOscillationWidth * (max(noStepsX, noStepsY) + 1)
            logger.warning("New oscillation range: %.1f degrees" % gridOscillationRange)

    if transmission is None:
        transmission = config.get_value(beamline, "Beam", "defaultGridTransmission")
    else:
        transmission = float(transmission)

    meshPositions = grid.determineMeshPositions(
        beamline,
        motorPositions,
        gridOscillationRange,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
    )

    firstImagePath = None
    for position in meshPositions:
        if position["index"] == 0:
            firstImagePath = os.path.join(directory, position["imageName"])
            logger.info("First image path %s" % firstImagePath)
            break

    meshQueueList = collect.createHelicalQueueList(
        beamline,
        directory,
        process_directory,
        run_number,
        expTypePrefix,
        prefix,
        meshPositions,
        motorPositions,
        gridExposureTime,
        gridOscillationRange,
        transmission,
        grid_info,
    )

    dictNewParameters = collect.checkMotorSpeed(
        beamline, meshQueueList, gridExposureTime, transmission
    )
    if dictNewParameters is not None:
        gridExposureTime = dictNewParameters["gridExposureTime"]
        transmission = dictNewParameters["transmission"]
        meshQueueList = collect.createHelicalQueueList(
            beamline,
            directory,
            process_directory,
            run_number,
            expTypePrefix,
            prefix,
            meshPositions,
            motorPositions,
            gridExposureTime,
            gridOscillationRange,
            transmission,
            grid_info,
        )
        newExposureTimeMessage = (
            "In order to perform the scan with optimal motor speed\n"
        )
        newExposureTimeMessage += (
            "the exposure time has been changed to %.3f s\n" % gridExposureTime
        )
        newExposureTimeMessage += (
            "and the transmission changed accordingly to %.1f %%.\n" % transmission
        )
        logger.info(newExposureTimeMessage)

    meshPositionFile = path.createNewJsonFile(
        workflowParameters["workingDir"], meshPositions, "meshPositions90degrees"
    )
    meshQueueListFile = path.createNewJsonFile(
        workflowParameters["workingDir"], meshQueueList, "meshQueueList90degrees"
    )

    return {
        "grid_info": grid_info,
        "shape": None,
        "meshPositionFile": meshPositionFile,
        "expTypePrefix": expTypePrefix,
        "meshQueueListFile": meshQueueListFile,
        "firstImagePath": firstImagePath,
        "directory": directory,
        "run_number": run_number,
        "synchronizeBestPosition": True,
    }
