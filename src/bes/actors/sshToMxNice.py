"""
Created on Mar 3, 2015

@author: svensson
"""

import os
import sys
import traceback
import logging
import subprocess
import StringIO

from bes.workflow_lib import mxnice


def run(ednaDpLaunchPath=None, launchPath=None, **_):

    hostName = None
    listSorted = None

    try:
        # To be removed when all launchers use launchPath instead of ednaDpLaunchPath
        if launchPath is None:
            launchPath = ednaDpLaunchPath

        errorMessage = ""
        ednaSuccess = False
        command_line = None

        stringBuffer = StringIO.StringIO()
        logger = logging.getLogger()
        logHandler = logging.StreamHandler(stringBuffer)
        formatter = logging.Formatter("%(asctime)s : %(message)s", "%Y-%m-%d %H:%M:%S")
        logHandler.setFormatter(formatter)
        logHandler.setLevel(logging.DEBUG)
        logger.addHandler(logHandler)
        logger.setLevel(logging.DEBUG)

        listSorted = mxnice.checkLoadOnMxNICE()
        hostName = listSorted[0][0]

        command_line = "ssh -f {0} 'export AutoPROCWorkFlowUser=True; {1}'".format(
            hostName, launchPath
        )
        logger.info("Starting script '{0}'".format(command_line))
        process = subprocess.Popen(
            command_line,
            shell=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            close_fds=True,
        )
        for line in iter(process.stdout.readline, ""):
            logger.info(line.replace("\n", ""))

        ednaSuccess = True
        logHandler.flush()
        stringBuffer.flush()
    except Exception:

        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        errorMessage = "{0} {1}".format(exc_type, exc_value)
        listTrace = traceback.extract_tb(exc_traceback)
        for listLine in listTrace:
            errorMessage += '  File "%s", line %d, in %s%s' % (
                listLine[0],
                listLine[1],
                listLine[2],
                os.linesep,
            )
        logHandler.flush()
        stringBuffer.flush()

    return {
        "errorMessage": errorMessage,
        "ednaSuccess": ednaSuccess,
        "log": stringBuffer.getvalue(),
        "command_line": command_line,
        "hostName": hostName,
        "user": os.environ["USER"],
        "listSorted": listSorted,
    }
