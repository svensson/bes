from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import collect_and_ispyb
from bes.workflow_lib import workflow_logging, path


def run(
    beamline,
    directory,
    process_directory,
    beamParameters,
    prefix,
    expTypePrefix,
    run_number,
    mxv1StrategyResultFile,
    motorPositions,
    suffix,
    workflowParameters,
    sampleInfo=None,
    resolution=None,
    mxv1ResultCharacterisationFile=None,
    cellSpaceGroup={},
    autoMeshPhiPosition=None,
    token=None,
    fineSlicedReferenceImages=False,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    besParameters=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        sampleInfo = common.jsonLoads(sampleInfo)
        if besParameters:
            bes_request_id = besParameters["request_id"]
        else:
            bes_request_id = None

        run_number = int(run_number)
        motorPositions = common.jsonLoads(motorPositions)
        beamParameters = common.jsonLoads(beamParameters)
        dictCellSpaceGroup = common.jsonLoads(cellSpaceGroup)
        workflowParameters = common.jsonLoads(workflowParameters)
        workflow_id = workflowParameters["ispyb_id"]
        workflow_working_dir = workflowParameters["workingDir"]
        snapShots = False

        if sampleInfo is None:
            groupNodeId = None
            sampleNodeId = 1
        else:
            groupNodeId = sampleInfo["groupNodeId"]
            sampleNodeId = sampleInfo["nodeId"]

        if resolution is not None:
            resolution = float(resolution)

        mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)
        if mxv1ResultCharacterisationFile is not None:
            _ = path.readXSDataFile(mxv1ResultCharacterisationFile)

        dictId = collect_and_ispyb.collectAndGroup(
            beamline=beamline,
            directory=directory,
            process_directory=process_directory,
            prefix=prefix,
            expTypePrefix=expTypePrefix,
            run_number=run_number,
            resolution=resolution,
            mxv1StrategyResult=mxv1StrategyResult,
            motorPositions=motorPositions,
            workflow_type=workflowParameters["type"],
            workflow_index=workflowParameters["index"],
            group_node_id=groupNodeId,
            sample_node_id=sampleNodeId,
            suffix=suffix,
            workflow_id=workflow_id,
            workflow_working_dir=workflow_working_dir,
            energy=beamParameters["energy"],
            snapShots=snapShots,
            dictCellSpaceGroup=dictCellSpaceGroup,
            fineSlicedReferenceImages=fineSlicedReferenceImages,
            token=token,
            workflow_name=workflowParameters["title"],
            bes_request_id=bes_request_id,
            characterisation_id=characterisation_id,
            kappa_settings_id=kappa_settings_id,
            position_id=position_id,
        )

        groupNodeId = dictId["group_node_id"]
        sampleInfo["groupNodeId"] = groupNodeId
        list_node_id = dictId["listNodeId"]

        # Find data collection id
        firstImage = edna_mxv1.getListImagePath(
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            bFirstImageOnly=True,
        )[0]
        dataCollection = ispyb.findDataCollectionFromFileLocationAndFileName(
            beamline, firstImage
        )
        if dataCollection is not None:
            dataCollectionId = dataCollection.dataCollectionId
        else:
            dataCollectionId = None

        # Move back phi to autMeshPosition
        if autoMeshPhiPosition is not None:
            logger.info(
                "Moving oscillation axis to auto-mesh position {0} degrees.".format(
                    autoMeshPhiPosition
                )
            )
            collect.moveMotors(
                beamline, directory, {"phi": autoMeshPhiPosition}, token=token
            )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "sampleInfo": sampleInfo,
        "list_node_id": list_node_id,
        "dataCollectionId": dataCollectionId,
    }
