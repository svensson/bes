def run(beamline, **_):

    return {
        "workflow_title": "Visual kappa reorientation on %s" % beamline,
        "workflow_type": "VisualReorientation",
    }
