#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

import os
import sys
import json
import base64
import traceback

from bes.workflow_lib import icat
from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    proposal,
    meshPositionFile,
    run_number,
    expTypePrefix,
    workflow_working_dir,
    prefix,
    suffix,
    workflow_id,
    grid_info,
    workflow_title,
    workflow_type,
    firstImagePath,
    bestPosition,
    workflowParameters,
    list_node_id=None,
    pyarch_html_dir=None,
    snapShotFilePath=None,
    titleMeshHtmlPage="Results",
    meshHtmlDirectoryName="",
    diffractionSignalDetection="DozorM2 (macro-molecules crystal detection)",
    shape=None,
    remainingPositions=None,
    resultMeshPath=None,
    workflowStepType=None,
    dozormListPositions=None,
    token=None,
    dozormCrystalMapPath=None,
    dozormColourMapPath=None,
    dozormImageNumberMapPath=None,
    doDistlSignalStrength=False,
    besParameters=None,
    **_,
):

    dataCollectionId = None
    workflowParameters = common.jsonLoads(workflowParameters)
    workflowId = workflowParameters["ispyb_id"]
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    bestPosition = common.jsonLoads(bestPosition)
    meshPositions = path.parseJsonFile(meshPositionFile)
    grid_info = common.jsonLoads(grid_info)
    remainingPositions = common.jsonLoads(remainingPositions)

    if besParameters:
        bes_request_id = besParameters["request_id"]
    else:
        bes_request_id = None

    logger.debug("Starting creating HTML page")
    meshPositions = path.parseJsonFile(meshPositionFile)

    isHorizontalRotationAxis = True

    listPositions = [bestPosition]
    if remainingPositions is not None and grid_info["steps_x"] != 1:
        listPositions += remainingPositions
    dict_html = grid.grid_create_result_html_page(
        beamline,
        grid_info,
        meshPositions,
        listPositions,
        directory,
        workflow_working_dir,
        isHorizontalRotationAxis=isHorizontalRotationAxis,
        title=titleMeshHtmlPage,
        diffractionSignalDetection=diffractionSignalDetection,
        dozormListPositions=dozormListPositions,
        dozormCrystalMapPath=dozormCrystalMapPath,
        dozormColourMapPath=dozormColourMapPath,
        dozormImageNumberMapPath=dozormImageNumberMapPath,
        doDistlSignalStrength=doDistlSignalStrength,
    )
    # Send plot image to MXCuBE
    data_file_path = firstImagePath
    # Get the "real" data file path if h5
    if data_file_path.endswith(".h5"):
        h5MasterFilePath, h5DataFilePath, h5FileNumber = path.getH5FilePath(
            data_file_path, isFastMesh=True
        )
        data_file_path = h5DataFilePath
    if (
        beamline in ["id23eh1", "id23eh2", "id30a1", "id30a3", "id30b"]
        and shape is not None
        and shape != "-1"
        and "plot_file_image" in dict_html
        # and grid_info["steps_x"] != 1
        # and grid_info["steps_y"] != 1
    ):
        plot_file_image = dict_html["plot_file_image"]
        base64data = base64.b64encode(open(plot_file_image, "rb").read()).decode(
            "utf-8"
        )
        collect.setImageGridData(
            beamline, base64data, key=shape, token=token, data_file_path=data_file_path
        )
        logger.debug("Image {0} sent to MXCuBE".format(plot_file_image))
    logger.debug("HTML page created, uploading results to mxCuBE")
    dict_html_pyarch = {}
    if list_node_id is not None:
        list_node_id = common.jsonLoads(list_node_id)
        html_page_path = os.path.join(
            dict_html["html_directory_path"], dict_html["index_file_name"]
        )
        if os.path.exists(html_page_path):
            collect.displayNewHtmlPage(
                beamline,
                html_page_path,
                "Grid results",
                list_node_id,
                strPageNumber=run_number,
                token=token,
            )
            if pyarch_html_dir is not None:
                #                    if meshHtmlDirectoryName == "":
                if expTypePrefix == "mesh-":
                    meshHtmlDirectoryName = "Mesh"
                elif expTypePrefix == "line-":
                    meshHtmlDirectoryName = "Line"
                else:
                    meshHtmlDirectoryName = "Html"
                dict_html_pyarch = path.copyHtmlDirectoryToPyarch(
                    pyarch_html_dir, dict_html, meshHtmlDirectoryName
                )
                if workflowStepType is None:
                    workflowStepType = meshHtmlDirectoryName
                status = "Success"
                imageResultFilePath = os.path.join(
                    dict_html_pyarch["html_directory_path"],
                    dict_html_pyarch["plot_file_name"],
                )
                htmlResultFilePath = os.path.join(
                    dict_html_pyarch["html_directory_path"],
                    dict_html_pyarch["index_file_name"],
                )
                resultFilePath = os.path.join(
                    dict_html_pyarch["html_directory_path"], "result.json"
                )
                open(resultFilePath, "w").write(json.dumps(dict_html["workflowStep"]))
                os.chmod(resultFilePath, 0o644)
                ispyb.storeWorkflowStep(
                    beamline,
                    workflowId,
                    workflowStepType,
                    status,
                    imageResultFilePath=imageResultFilePath,
                    htmlResultFilePath=htmlResultFilePath,
                    resultFilePath=resultFilePath,
                )
                # sample_name = os.path.basename(os.path.dirname(os.path.dirname(directory)))
                sample_name = icat.get_sample_name(directory)
                metadata_urls = config.get_value(
                    beamline, "ICAT", "metadata_urls", default_value=[]
                )
                try:
                    if len(metadata_urls) > 0:
                        icat.storeWorkflowStep(
                            beamline,
                            proposal,
                            directory,
                            workflowStepType.lower(),
                            sample_name,
                            workflowParameters["title"],
                            workflowParameters["type"],
                            bes_request_id,
                            imageResultFilePath,
                            resultFilePath,
                            icat_sub_dir="icat",
                        )
                except Exception as e:
                    logger.error("Error when trying to store workflow step in ICAT:")
                    logger.error(e)
                    (exc_type, exc_value, exc_traceback) = sys.exc_info()
                    errorMessage = "{0} {1}".format(exc_type, exc_value)
                    logger.debug(errorMessage)
                    listTrace = traceback.extract_tb(exc_traceback)
                    logger.debug("Traceback (most recent call last): %s" % os.linesep)
                    for listLine in listTrace:
                        errorLine = '  File "%s", line %d, in %s%s' % (
                            listLine[0],
                            listLine[1],
                            listLine[2],
                            os.linesep,
                        )
                        logger.debug(errorLine)
                # Upload grid to ISPyB data collection
                dataCollection = ispyb.findDataCollectionFromFileLocationAndFileName(
                    beamline, firstImagePath
                )
                plotPath = os.path.join(
                    dict_html_pyarch["html_directory_path"],
                    dict_html_pyarch["plot_file_name"],
                )
                csvPath = None
                dataCollectionId = ispyb.setImageQualityIndicatorsPlot(
                    beamline, dataCollection.dataCollectionId, plotPath, csvPath
                )
    logger.debug("Calculating statistics")
    best_image_id = None
    dict_statistics = grid.mesh_scan_statistics(
        meshPositions, workingDir=workflow_working_dir
    )
    logger.debug("Sending email")
    if expTypePrefix == "mesh-":
        emailList = config.get_value(
            beamline, "Email", "meshStatistics", default_value=[]
        )
        common.send_email(
            beamline,
            emailList,
            "Mesh statistics",
            directory,
            workflow_title,
            workflow_type,
            workflow_working_dir,
            str_message=dict_statistics["message"],
        )
    logger.debug("Storing workflow mesh")
    edna_ispyb.store_workflow_mesh(
        beamline,
        workflow_id,
        grid_info,
        dict_html_pyarch,
        bestPosition,
        best_image_id,
        dict_statistics,
        workflow_working_dir,
    )

    return {
        "Done": True,
        "dict_html_pyarch": dict_html_pyarch,
        "dataCollectionId": dataCollectionId,
        "data_file_path": data_file_path,
        # "dataCollection": dict(dataCollection)
    }
