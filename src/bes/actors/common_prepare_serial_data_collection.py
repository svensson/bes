def run(beamline, expTypePrefix="mesh-", **_):
    return {
        "workflow_title": "Serial data collection on %s" % beamline,
        "workflow_type": "MeshScan",
        "expTypePrefix": expTypePrefix,
        "osc_range": 90.0,
    }
