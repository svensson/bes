import os
import tempfile
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging

# from bes.workflow_lib import Dozor2_XrayCentering2_2


def run(
    beamline,
    directory,
    grid_info,
    workflowParameters,
    workflow_working_dir,
    resultMeshPath=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        grid_info = common.jsonLoads(grid_info)

        try:
            if resultMeshPath is not None and grid_info["steps_x"] != 1:
                logger.debug("resultMeshPath: {0}".format(resultMeshPath))
                fileDescriptor, plotFileName = tempfile.mkstemp(
                    prefix="Dozor2_XrayCentring_",
                    suffix=".png",
                    dir=workflow_working_dir,
                )
                os.close(fileDescriptor)
                plotPath = os.path.join(workflow_working_dir, plotFileName)
                os.chmod(plotPath, 0o755)
                # Dozor2_XrayCentering2_2.main(resultMeshPath, plotPath)
                logger.debug("Dozor2_XrayCentering2_2 executed: {0}.".format(plotPath))
        except BaseException as e:
            logger.debug("Execution of Dozor2_XrayCentering2_2 failed.")
            logger.debug(e)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
