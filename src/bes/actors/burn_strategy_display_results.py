import os
import shutil
from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect


def run(
    beamline,
    workflowParameters,
    result_rdfit_htmlPage,
    result_rdfit_jsonPath,
    result_rdfit_scaleIntensityPlot,
    pyarch_html_dir=None,
    list_node_id=None,
    token=None,
    **_,
):

    try:
        workflowParameters = common.jsonLoads(workflowParameters)
        if list_node_id is not None:
            list_node_id = common.jsonLoads(list_node_id)
        rdfitResultHtmlDir = os.path.join(
            workflowParameters["workingDir"], "rdfitResultHtmlDir"
        )
        shutil.copytree(os.path.dirname(result_rdfit_htmlPage), rdfitResultHtmlDir)
        resultRdfitHtmlPage = os.path.join(
            rdfitResultHtmlDir, os.path.basename(result_rdfit_htmlPage)
        )
        collect.displayNewHtmlPage(
            beamline,
            resultRdfitHtmlPage,
            "ResultBurnStrategy",
            list_node_id,
            strPageNumber=1,
            token=token,
        )
        if pyarch_html_dir is not None:
            dict_html = {}
            dict_html["html_directory_path"] = os.path.dirname(resultRdfitHtmlPage)
            dict_html["index_file_name"] = os.path.basename(resultRdfitHtmlPage)
            dictHtmlPyarch = path.copyHtmlDirectoryToPyarch(
                pyarch_html_dir,
                dict_html,
                indexHtml=resultRdfitHtmlPage,
                directory_name="RDFit",
            )

            workflowStepType = "RDFit"
            status = "Success"
            shutil.copy(
                result_rdfit_scaleIntensityPlot, dictHtmlPyarch["html_directory_path"]
            )
            imageResultFilePath = os.path.join(
                dictHtmlPyarch["html_directory_path"],
                os.path.basename(result_rdfit_scaleIntensityPlot),
            )
            htmlResultFilePath = os.path.join(
                dictHtmlPyarch["html_directory_path"], dictHtmlPyarch["index_file_name"]
            )
            resultFilePath = os.path.join(
                dictHtmlPyarch["html_directory_path"],
                os.path.basename(result_rdfit_jsonPath),
            )
            ispyb.storeWorkflowStep(
                beamline,
                workflowParameters["ispyb_id"],
                workflowStepType,
                status,
                imageResultFilePath=imageResultFilePath,
                htmlResultFilePath=htmlResultFilePath,
                resultFilePath=resultFilePath,
            )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "resultRdfitHtmlPage": resultRdfitHtmlPage,
        "imageResultFilePath": imageResultFilePath,
        "htmlResultFilePath": htmlResultFilePath,
        "resultFilePath": resultFilePath,
    }
