import sys
import traceback

from bes.workflow_lib import edna_mxv1


def run(inputFile, **_):

    indexingInput = None
    errorMessage = None
    listTrace = None

    try:
        indexingInput = edna_mxv1.createEdna2CharacterisationInput(inputFile)
    except Exception:
        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        errorMessage = "{0} {1}".format(exc_type, exc_value)
        listTrace = traceback.extract_tb(exc_traceback)

    return {
        "indexingInput": indexingInput,
        "errorMessage": errorMessage,
        "listTrace": listTrace,
    }
