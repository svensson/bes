from bes.workflow_lib import common


def run(directory, beamline, **kwargs):

    workflowParameters = {}
    workflowParameters["title"] = "MXPress_setup"
    workflowParameters["type"] = "MXPress_setup"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "automatic_mode": True,
        "runGphlWorkflow": False,
    }
