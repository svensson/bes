from bes.workflow_lib import dialog


def run(
    beamline,
    workflowParameters,
    directory,
    motorPositions,
    prefix,
    collection_software=None,
    token=None,
    **_,
):

    listDialog = [
        {
            "variableName": "prefix",
            "label": "Prefix",
            "type": "text",
            "value": "Si",
        },
        {
            "variableName": "csvOfResolutions",
            "label": "Comma separated list of resolutions [A]",
            "type": "text",
            "value": "1.506,1.783,2.072,2.670,3.914",
        },
        {
            "variableName": "osc_range",
            "label": "Oscillation range [degrees]",
            "type": "float",
            "value": 180.0,
            "unit": "degree",
            "lowerBound": 0.05,
            "upperBound": 360.0,
        },
        {
            "variableName": "collectExposureTime",
            "label": "Collect exposure time [s]",
            "type": "float",
            "value": 3.0,
            "lowerBound": 0.004,
            "upperBound": 100.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "value": 0.1,
            "lowerBound": 0.01,
            "upperBound": 100.0,
        },
    ]

    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        collection_software=collection_software,
    )

    return dictValues
