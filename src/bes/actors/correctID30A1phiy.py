from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, directory, motorPositions, token=None, **_):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    try:

        motorPositions = common.jsonLoads(motorPositions)

        if beamline in ["simulator"]:

            logger.debug("Correcting for id30a1 phiy translation table")
            phiy = motorPositions["phiy"]
            phiz = motorPositions["phiz"]
            correctionPhiz = collect.id30a1PhiyCorrection(phiy, token=token)
            logger.debug(
                "Phiz correction = %.4f for phiy=%.4f" % (correctionPhiz, phiy)
            )
            newPhiz = phiz - correctionPhiz
            logger.debug("New phiz position: %.4f" % newPhiz)
            #         collect.moveMotors(beamline, directory, {"phiz": newPhiz}, token=token)
            #         collect.moveSampleOrthogonallyToRotationAxis(beamline, directory, correctionPhiz, token=token)
            motorPositions = collect.readMotorPositions(beamline, token=token)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {"motorPositions": motorPositions}
