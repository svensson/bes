from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(beamline, directory, prefix, token=None, **kwargs):

    logger = workflow_logging.getLogger(beamline, token=token)

    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressR: no auto-mesh, X-centre, eEDNA + dc on %s" % beamline
    )
    workflowParameters["type"] = "MXPressR"

    try:
        prefix = collect.getCurrentCdCrystalId(beamline, token=token)
        logger.warning("Prefix set to: {0}".format(prefix))
    except Exception:
        logger.error("Cannot read crystal id! Prefix set to: {0}".format(prefix))

    beamline = edna_mxv1.extractBeamlineFromDirectory(directory)
    parameters = path.getDefaultParameters(directory)
    aimed_resolution = parameters.get("aimedResolution", 2.0)
    number_of_positions = parameters.get("numberOfPositions", 1)
    preferred_aperture = parameters.get(
        "preferredApertureName", aperture_utils.aperture_size_to_name(beamline, 30)
    )
    # aimed_multiplicity = parameters.get("aimedMultiplicity", 2.5)
    # aimed_completeness = parameters.get("aimedCompleteness", 1.0)
    grid_transmission = parameters.get("gridTransmission", 50)
    grid_size = parameters.get("gridSize", 300) / 1000.0  # mm
    grid_steps = parameters.get("gridSteps", 10)
    diffraction_signal_detection = parameters.get(
        "diffractionSignalDetection", "Dozor (macro-molecules)"
    )
    forced_space_group = parameters.get("forcedSpaceGroup", None)

    grid_info = {
        "x1": -grid_size / 2,
        "y1": -grid_size / 2,
        "dx_mm": grid_size,
        "dy_mm": grid_size,
        "steps_x": grid_steps,
        "steps_y": grid_steps,
        "angle": 0.0,
    }

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "grid_info": grid_info,
        "automatic_mode": True,
        "doAutoMesh": False,
        "prefix": prefix,
        "aimedIOverSigmaAtHighestResolution": 1.0,
        "collectTransmission": 20,
        "collectExposureTime": 0.02,
        "gridTransmission": grid_transmission,
        "preferredApertureName": preferred_aperture,
        "aimedResolution": aimed_resolution,
        # "aimedMultiplicity": aimed_multiplicity,
        # "aimedCompleteness": aimed_completeness,
        "numberOfPositions": number_of_positions,
        "diffractionSignalDetection": diffraction_signal_detection,
        "forcedSpaceGroup": forced_space_group,
        "fineSlicedReferenceImages": False,
    }
