def run(beamline, **_):

    return {
        "workflow_title": "Time resolved mesh on %s" % beamline,
        "workflow_type": "TimeResolvedMesh",
    }
