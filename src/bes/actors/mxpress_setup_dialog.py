import os

from bes.workflow_lib import path
from bes.workflow_lib import dialog
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def check_integer(value, label):
    if value != "Diffraction plan" and not value.startswith("Default value:"):
        try:
            _ = int(value)
        except ValueError:
            raise RuntimeError(f"{label} must be an integer!")


def check_float(value, label):
    if value != "Diffraction plan" and not value.startswith("Default value:"):
        try:
            _ = float(value)
        except ValueError:
            raise RuntimeError(f"{label} must be a float!")


def run(
    beamline,
    directory,
    workflowParameters,
    collection_software=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(  # Noqa F841
        beamline, workflowParameters, token=token
    )

    parameters = path.getDefaultParameters(directory, file_prefix="mxpress_setup")
    list_dialog = [
        {
            "variableName": "delete_mxpress_setup",
            "label": "Delete MXPress setup file?",
            "type": "boolean",
            "value": False,
        }
    ]

    # Diffraction signal detection
    default_diffraction_signal_detection = config.get_value(
        beamline,
        "Mesh",
        "diffractionSignalDetection",
        default_value="DozorM2 (macro-molecules crystal detection)",
    )
    diffraction_signal_choises = [
        "DozorM2 (macro-molecules crystal detection)",
        "Dozor (macro-molecules)",
        # "TotalIntensity (any diffraction)",
        "Spotfinder (small molecules, slow processing)",
    ]
    for index, choice in enumerate(diffraction_signal_choises):
        first_word_choice = choice.split(" ")[0]
        first_word_config = default_diffraction_signal_detection.split(" ")[0]
        logger.debug(f"{first_word_choice} {first_word_config}")
        if first_word_choice.lower() == first_word_config.lower():
            default_diffraction_signal_detection = f"Default: {choice}"
            diffraction_signal_choises[index] = default_diffraction_signal_detection
            break

    diffraction_signal_detection = parameters.get(
        "diffractionSignalDetection", default_diffraction_signal_detection
    )
    list_dialog.append(
        {
            "variableName": "diffractionSignalDetection",
            "label": "Diffraction signal detection",
            "type": "combo",
            "textChoices": diffraction_signal_choises,
            "value": diffraction_signal_detection,
        }
    )

    # Aperture
    if beamline not in ["id23eh2", "id30a2"]:
        aperture_names = collect.getApertureNames(beamline, token)

        fixedTargetApertureSize = config.get_value(
            beamline, "AutoMesh", "fixedTargetApertureSize"
        )
        fixedTargetApertureName = aperture_utils.aperture_size_to_name(
            beamline, fixedTargetApertureSize
        )
        if fixedTargetApertureName not in aperture_names:
            raise RuntimeError(
                f"Aperture {fixedTargetApertureName} not in list of apertures {aperture_names}!"
            )

        default_value = f"Default value: {fixedTargetApertureName}"
        aperture_names[aperture_names.index(fixedTargetApertureName)] = default_value
        target_aperture = parameters.get("targetApertureName", default_value)
        list_dialog.append(
            {
                "variableName": "targetApertureName",
                "label": "Aperture",
                "type": "combo",
                "value": target_aperture,
                "textChoices": aperture_names,
            }
        )

    # Forced space group
    list_of_chiral_spacegroups = symmetry.getListOfChiralSpaceGroups()
    list_of_chiral_spacegroups.insert(0, "Diffraction plan")
    forced_space_group_value = parameters.get("forcedSpaceGroup", "Diffraction plan")
    list_dialog.append(
        {
            "variableName": "forcedSpaceGroup",
            "label": "Forced space group",
            "type": "combo",
            "textChoices": list_of_chiral_spacegroups,
            "value": forced_space_group_value,
        }
    )

    # Aimed resolution and number of positions from Diffraction plan
    aimed_resolution = parameters.get("aimedResolution", "Diffraction plan")
    list_dialog.append(
        {
            "variableName": "aimedResolution",
            "label": "Aimed resolution",
            "type": "text",
            "value": aimed_resolution,
            "unit": "photon/s",
        }
    )
    number_of_positions = parameters.get("numberOfPositions", "Diffraction plan")
    list_dialog.append(
        {
            "variableName": "numberOfPositions",
            "label": "Number of positions",
            "type": "text",
            "value": number_of_positions,
        }
    )

    # Grid transmission
    default_grid_transmission = config.get_value(
        beamline, "Beam", "defaultGridTransmission"
    )
    grid_transmission = parameters.get(
        "gridTransmission", f"Default value: {default_grid_transmission}"
    )
    list_dialog.append(
        {
            "variableName": "gridTransmission",
            "label": "Grid transmission",
            "type": "text",
            "value": grid_transmission,
        }
    )

    # Loop min and max widths
    default_loop_min_width = str(config.get_value(beamline, "AutoMesh", "loopMinWidth"))
    default_loop_max_width = str(config.get_value(beamline, "AutoMesh", "loopMaxWidth"))

    loop_min_width = str(
        parameters.get("loopMinWidth", f"Default value: {default_loop_min_width}")
    )
    if "Default" not in loop_min_width and loop_min_width == default_loop_min_width:
        loop_min_width = f"Default value: {default_loop_min_width}"

    loop_max_width = str(
        parameters.get("loopMaxWidth", f"Default value: {default_loop_max_width}")
    )
    if "Default" not in loop_max_width and loop_max_width == default_loop_max_width:
        loop_max_width = f"Default value: {default_loop_max_width}"

    list_dialog.append(
        {
            "variableName": "loopMinWidth",
            "label": "Auto mesh loop min width",
            "type": "text",
            "value": loop_min_width,
        }
    )
    list_dialog.append(
        {
            "variableName": "loopMaxWidth",
            "label": "Auto mesh loop max width",
            "type": "text",
            "value": loop_max_width,
        }
    )

    # Fake positions:
    list_dialog.append(
        {
            "variableName": "simulated_grid_positions",
            "label": "Use simulated grid position(s)?",
            "type": "boolean",
            "value": False,
        }
    )
    list_dialog.append(
        {
            "variableName": "simulated_characterisation_results",
            "label": "Use simulated characterisation results?",
            "type": "boolean",
            "value": False,
        }
    )

    dict_values = dialog.openDialog(
        beamline,
        list_dialog,
        directory=directory,
        token=token,
        automaticMode=False,
        collection_software=collection_software,
        file_prefix="mxpress_setup",
    )

    if dict_values["delete_mxpress_setup"]:
        parameters_path = path.getDefaultParametersPath(
            directory, file_prefix="mxpress_setup"
        )
        if os.path.exists(parameters_path):
            os.remove(parameters_path)
            logger.warning("Removed MXPress setup parameter file")
        else:
            logger.warning("MXPress setup parameter file doesn't exist")
        logger.info(parameters_path)
        dict_values = {}
    else:
        # Check types
        check_float(dict_values["aimedResolution"], "Aimed resolution")
        check_integer(dict_values["numberOfPositions"], "Number of positions")
        check_float(dict_values["loopMinWidth"], "Auto mesh loop min width")
        check_float(dict_values["loopMaxWidth"], "Auto mesh loop max width")

    return dict_values
