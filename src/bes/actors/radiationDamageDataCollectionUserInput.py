from bes import config
from bes.workflow_lib import dialog


def run(
    beamline,
    directory,
    data_collection_no_images,
    data_collection_exposure_time,
    data_collection_osc_range,
    transmission,
    data_collection_iterations,
    resolution=None,
    automatic_mode=False,
    collection_software=None,
    token=None,
    **_,
):

    listDialog = [
        {
            "variableName": "resolution",
            "label": "Resolution (A)",
            "type": "float",
            "value": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 10.0,
            "useDefaults": False,
        },
        {
            "variableName": "transmission",
            "label": "Transmission (%)",
            "type": "float",
            "defaultValue": transmission,
            "lowerBound": 0,
            "upperBound": 100.0,
            "useDefaults": False,
        },
        {
            "variableName": "data_collection_iterations",
            "label": "Number of data collection iterations",
            "type": "int",
            "value": data_collection_iterations,
            "lowerBound": 1,
            "upperBound": 10000.0,
            "useDefaults": False,
        },
        {
            "variableName": "data_collection_no_images",
            "label": "Data collection number of images",
            "type": "int",
            "value": data_collection_no_images,
            "lowerBound": 1,
            "upperBound": 10000.0,
            "useDefaults": False,
        },
        {
            "variableName": "data_collection_exposure_time",
            "label": "Data collection exposure time (s)",
            "type": "float",
            "defaultValue": data_collection_exposure_time,
            "lowerBound": config.get_value(beamline, "Beam", "minCollectExposureTime"),
            "upperBound": 100.0,
            "useDefaults": False,
        },
        {
            "variableName": "data_collection_osc_range",
            "label": "Data collection oscillation range (degrees)",
            "type": "float",
            "defaultValue": data_collection_osc_range,
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
