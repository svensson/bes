import os

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    run_number,
    process_directory,
    prefix,
    suffix,
    osc_range,
    gridExposureTime,
    gridTransmission=None,
    token=None,
    **_,
):

    try:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        motorPositions = collect.readMotorPositions(beamline, token=token)
        apertureSize = collect.getApertureSize(beamline, token=token)
        logger.info("Current aperture size: %d um", apertureSize)

        apertureInMm = apertureSize / 1000.0

        # 1D vertical scan
        logger.info("Preparing first vertical line scan")

        overSampling = 4
        if beamline == "id30a2":
            noSteps = 40
        else:
            noSteps = 100

        if beamline in ["id23eh2", "id30a2"]:
            deltaX = noSteps * apertureInMm / overSampling
            grid_info = {
                "x1": -deltaX / 2.0,
                "y1": 0.0,
                "dx_mm": deltaX,
                "dy_mm": 0.1,
                "steps_x": noSteps,
                "steps_y": 1,
                "angle": 0.0,
            }
        else:
            deltaY = noSteps * apertureInMm / overSampling
            grid_info = {
                "x1": 0.0,
                "y1": -deltaY / 2.0,
                "dx_mm": 0.1,
                "dy_mm": deltaY,
                "steps_x": 1,
                "steps_y": noSteps,
                "angle": 0.0,
            }

        expTypePrefix = "line-"

        if gridTransmission is None:
            transmission = config.get_value(beamline, "Beam", "defaultGridTransmission")
        else:
            transmission = float(gridTransmission)

        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositions,
            osc_range,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )

        firstImagePath = None
        for position in meshPositions:
            if position["index"] == 0:
                firstImagePath = os.path.join(directory, position["imageName"])
                logger.info("First image path %s" % firstImagePath)
                break

        meshQueueList = collect.createHelicalQueueList(
            beamline,
            directory,
            process_directory,
            run_number,
            expTypePrefix,
            prefix,
            meshPositions,
            motorPositions,
            gridExposureTime,
            osc_range,
            transmission,
            grid_info,
        )

        dictNewParameters = collect.checkMotorSpeed(
            beamline, meshQueueList, gridExposureTime, transmission
        )
        if dictNewParameters is not None:
            gridExposureTime = dictNewParameters["gridExposureTime"]
            transmission = dictNewParameters["transmission"]
            meshQueueList = collect.createHelicalQueueList(
                beamline,
                directory,
                process_directory,
                run_number,
                expTypePrefix,
                prefix,
                meshPositions,
                motorPositions,
                gridExposureTime,
                osc_range,
                transmission,
                grid_info,
            )
            newExposureTimeMessage = (
                "In order to perform the scan with optimal motor speed\n"
            )
            newExposureTimeMessage += (
                "the exposure time has been changed to %.3f s\n" % gridExposureTime
            )
            newExposureTimeMessage += (
                "and the transmission changed accordingly to %.1f %%.\n" % transmission
            )
            logger.info(newExposureTimeMessage)

        meshPositionFile = path.createNewJsonFile(
            workflowParameters["workingDir"], meshPositions, "meshPositionsFirst"
        )
        meshQueueListFile = path.createNewJsonFile(
            workflowParameters["workingDir"], meshQueueList, "meshQueueListFirst"
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "grid_info": grid_info,
        "shape": None,
        "meshPositionFile": meshPositionFile,
        "expTypePrefix": expTypePrefix,
        "meshQueueListFile": meshQueueListFile,
        "firstImagePath": firstImagePath,
    }
