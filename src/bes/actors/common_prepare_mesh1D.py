def run(beamline, expTypePrefix="line-", **_):
    return {
        "workflow_title": "Line scan on %s" % beamline,
        "workflow_type": "LineScan",
        "expTypePrefix": expTypePrefix,
    }
