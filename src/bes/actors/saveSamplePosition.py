from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        collect.saveCurrentPos(beamline, token=token)
        logger.info("Optimal position saved in mxCuBE")

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
