# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2018 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "2018/09/18"

from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import machineInfo
from bes.workflow_lib import workflow_logging


def run(
    directory,
    prefix,
    run_number,
    beamline,
    motorPositions,
    workflowParameters,
    expTypePrefix="ref-",
    suffix=None,
    characterisationExposureTime=None,
    gridExposureTime=None,
    collectExposureTime=None,
    transmission=None,
    gridTransmission=None,
    collectTransmission=None,
    characterisationTransmission=None,
    doRefDataCollectionReview=True,
    plateMode=None,
    gridSnapShots=False,
    lineScanLength=None,
    fineSlicedReferenceImages=None,
    process_partial_data_sets=False,
    characterisation_id=None,
    position_id=None,
    kappa_settings_id=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters)
    workflowParameters = common.jsonLoads(workflowParameters)
    workflowType = workflowParameters["type"]
    motorPositions = common.jsonLoads(motorPositions)

    if run_number == 0:
        run_number = 1
    # logger.info(f"Using first run number {run_number}")

    logger.info("Initial motor settings:")
    for motorName in motorPositions.keys():
        if motorPositions[motorName] is not None:
            logger.info("  %10s: %.04f" % (motorName, motorPositions[motorName]))

    logger.debug("expTypePrefix = %r" % expTypePrefix)

    if suffix is None:
        suffix = config.get_value(beamline, "Detector", "suffix")
    logger.debug("suffix = %r" % suffix)

    try:
        fillingMode = machineInfo.readFillingMode(beamline)
    except Exception:
        fillingMode = "Unknown"
    logger.info("Filling mode: {0}".format(fillingMode))

    if characterisationExposureTime is None:
        characterisationExposureTime = config.get_value(
            beamline, "Beam", "defaultCharacterisationExposureTime"
        )
        try:
            characterisationExposureTime = (
                machineInfo.correctExposureTimeForFillingMode(
                    beamline, characterisationExposureTime
                )
            )
        except Exception:
            logger.warning(
                "Cannot correct characterisation exposure time for filling mode"
            )
    else:
        characterisationExposureTime = float(characterisationExposureTime)
    logger.info(
        "Default characterisation exposure time = %r s" % characterisationExposureTime
    )

    if gridExposureTime is None:
        gridExposureTime = config.get_value(beamline, "Beam", "defaultGridExposureTime")
        try:
            gridExposureTime = machineInfo.correctExposureTimeForFillingMode(
                beamline, gridExposureTime
            )
        except Exception:
            logger.warning("Cannot correct grid exposure time for filling mode")
    else:
        gridExposureTime = float(gridExposureTime)
    logger.info("Default grid exposure time = %r s" % gridExposureTime)

    if collectExposureTime is None:
        collectExposureTime = config.get_value(
            beamline, "Beam", "defaultCollectExposureTime"
        )
        try:
            collectExposureTime = machineInfo.correctExposureTimeForFillingMode(
                beamline, collectExposureTime
            )
        except Exception:
            logger.warning("Cannot correct collect exposure time for filling mode")
    else:
        collectExposureTime = float(collectExposureTime)
    logger.info("Default collect exposure time = %r s" % collectExposureTime)

    gridOscillationRange = config.get_value(
        beamline, "Goniometer", "defaultGridOscillationRange"
    )
    logger.info("Default grid oscillation range = %r degree" % gridOscillationRange)

    characterisationOscillationRange = config.get_value(
        beamline, "Goniometer", "defaultCharacterisationOscillationRange"
    )
    logger.info(
        "Default characterisation oscillation range = %r degree"
        % characterisationOscillationRange
    )

    dataCollectionOscillationRange = config.get_value(
        beamline, "Goniometer", "defaultDataCollectionOscillationRange"
    )
    logger.info(
        "Default data collection oscillation range = %r degree"
        % dataCollectionOscillationRange
    )

    if transmission is None:
        transmission = config.get_value(beamline, "Beam", "defaultGridTransmission")
    else:
        transmission = float(transmission)
    logger.info("Default transmission = %r %%" % transmission)

    if gridTransmission is None:
        gridTransmission = config.get_value(beamline, "Beam", "defaultGridTransmission")
    else:
        gridTransmission = float(gridTransmission)
    logger.info("Default grid transmission = %r %%" % gridTransmission)

    if characterisationTransmission is None:
        characterisationTransmission = config.get_value(
            beamline, "Beam", "defaultCharacterisationTransmission"
        )
    else:
        characterisationTransmission = float(characterisationTransmission)
    logger.info(
        "Default characterisation transmission = %r %%" % characterisationTransmission
    )

    if collectTransmission is None:
        collectTransmission = config.get_value(
            beamline, "Beam", "defaultCollectTransmission"
        )
    else:
        collectTransmission = float(collectTransmission)
    logger.info("Default collect transmission = %r %%" % characterisationTransmission)

    process_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")
    logger.debug("process_directory = %r" % process_directory)

    centerRotationAxis = False
    logger.debug("centerRotationAxis = %r" % centerRotationAxis)

    # Check plateMode
    if plateMode is None:
        try:
            plateMode = collect.getPlateMode(beamline, token=token)
            if isinstance(plateMode, str):
                plateMode = plateMode.lower() == "true"
        except Exception as e:
            logger.error("Error reading plate mode! Error message: {0}".format(e))
            plateMode = False
    logger.info("Plate mode is set to: %r" % plateMode)
    if plateMode and workflowType in [
        "BurnStrategy",
        "HelicalCharacterisation",
        "KappaReorientation",
        "VisualReorientation",
        "XrayCentering",
        "EnhancedCharacterisation",
    ]:
        raise RuntimeError(
            "Cannot start WF {0} because plate mode is set to true!".format(
                workflowType
            )
        )

    gridSnapShots = True
    logger.debug("gridSnapShots = %r" % gridSnapShots)

    # Line scan defaults
    if lineScanLength is None:
        lineScanLength = config.get_value(beamline, "Goniometer", "lineScanLength")

    lineScanOverSampling = config.get_value(
        beamline, "Goniometer", "lineScanOverSampling"
    )

    # Rotation axis
    rotationAxis = config.get_value(beamline, "Goniometer", "rotationAxis")
    isHorizontalRotationAxis = rotationAxis == "horizontal"

    # Resolution limits
    lowResolution = None
    highResolution = None
    if beamline in ["id23eh1", "id30a2", "id30b"]:
        highResolution, lowResolution = collect.getResolutionLimits(
            beamline, token=token
        )
        logger.info(
            "Highest resolution possible as calculated by MXCuBE: {0} A".format(
                highResolution
            )
        )
        logger.info(
            "Lowes resolution possible as calculated by MXCuBE: {0} A".format(
                lowResolution
            )
        )
        # Add extra margin in order to avoid border cases
        resolution_margin = 0.01
        lowResolution = round(lowResolution - resolution_margin, 2)
        highResolution = round(highResolution + resolution_margin, 2)
        logger.info(
            f"Highest resolution modified with {resolution_margin} A margin: {highResolution} A"
        )
        logger.info(
            f"Lowest resolution modified with {resolution_margin} A margin: {lowResolution} A"
        )

    if lowResolution is None or highResolution is None:
        lowResolution = config.get_value(beamline, "Detector", "lowResolution")
        highResolution = config.get_value(beamline, "Detector", "highResolution")
        logger.warning("Cannot read min/max resolution from MXCuBE!")
        logger.warning(
            "Using configured values: min={0} A and max={1} A".format(
                lowResolution, highResolution
            )
        )

    # Allow zero degree mesh scan
    allowZeroDegreeMeshScan = config.get_value(
        beamline, "Goniometer", "allowZeroDegreeMeshScan", default_value=False
    )
    logger.debug("allowZeroDegreeMeshScan = {0}".format(allowZeroDegreeMeshScan))

    # Fine sliced reference images
    if fineSlicedReferenceImages is None:
        fineSlicedReferenceImages = config.get_value(
            beamline, "Goniometer", "fineSlicedReferenceImages", default_value=False
        )
    logger.debug("fineSlicedReferenceImages = {0}".format(fineSlicedReferenceImages))

    if not kappa_settings_id:
        kappa_settings_id = 1

    return {
        "characterisationExposureTime": characterisationExposureTime,
        "gridExposureTime": gridExposureTime,
        "collectExposureTime": collectExposureTime,
        "gridOscillationRange": gridOscillationRange,
        "gridTransmission": gridTransmission,
        "characterisationOscillationRange": characterisationOscillationRange,
        "dataCollectionOscillationRange": dataCollectionOscillationRange,
        "collectTransmission": collectTransmission,
        "motorInitialPositions": motorPositions,
        "process_directory": process_directory,
        "transmission": transmission,
        "characterisationTransmission": characterisationTransmission,
        "suffix": suffix,
        "expTypePrefix": expTypePrefix,
        "doRefDataCollectionReview": doRefDataCollectionReview,
        "prefix": prefix,
        "centerRotationAxis": centerRotationAxis,
        "plateMode": plateMode,
        "gridSnapShots": gridSnapShots,
        "lineScanLength": lineScanLength,
        "lineScanOverSampling": lineScanOverSampling,
        "isHorizontalRotationAxis": isHorizontalRotationAxis,
        "lowResolution": lowResolution,
        "highResolution": highResolution,
        "allowZeroDegreeMeshScan": allowZeroDegreeMeshScan,
        "run_number": run_number,
        "fineSlicedReferenceImages": fineSlicedReferenceImages,
        "characterisation_id": characterisation_id,
        "position_id": position_id,
        "kappa_settings_id": kappa_settings_id,
        "process_partial_data_sets": process_partial_data_sets,
    }
