import pprint

import jsonpickle
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    prefix,
    workflowParameters,
    sampleInfo=None,
    directory=None,
    process_directory=None,
    run_number=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    expTypePrefix = "mesh"
    if sampleInfo is None:
        group_node_id = None
        sampleNodeId = -1
    else:
        group_node_id = sampleInfo["groupNodeId"]
        sampleNodeId = sampleInfo["nodeId"]
    runNumberDataCollection = run_number
    # Prepare collect of type IMAGING
    serverProxy = collect.getServerProxy(beamline, token)
    collect.importQueueModelObjects(beamline, serverProxy)
    if group_node_id is None:
        group_node_id = collect.createNewDataCollectionGroup(
            beamline=beamline,
            groupName="Time resolved mesh",
            workflow_index=workflowParameters["index"],
            enabled=True,
            sample_node_id=sampleNodeId,
            token=token,
        )
    dataCollection = collect.QUEUE_MODEL_OBJECTS.DataCollection()
    dataCollection.experiment_type = 11  # IMAGING
    default_collection_parameters = jsonpickle.decode(
        serverProxy.beamline_setup_read("/beamline/default-acquisition-parameters/")
    )
    logger.debug(
        "default_collection_parameters: %r"
        % pprint.pformat(default_collection_parameters)
    )
    dataCollection.acquisitions[0].acquisition_parameters = (
        default_collection_parameters
    )
    default_path_template = jsonpickle.decode(
        serverProxy.beamline_setup_read("/beamline/default-path-template/")
    )
    logger.debug("default_path_template: %r" % pprint.pformat(default_path_template))
    dataCollection.acquisitions[0].path_template = default_path_template
    dataCollection.acquisitions[0].acquisition_parameters.shutterless = True
    dataCollection.acquisitions[0].path_template.directory = directory
    logger.debug("process_directory: %r" % process_directory)
    dataCollection.acquisitions[0].path_template.process_directory = process_directory
    logger.debug("expTypePrefix+prefix+wedgeId: %s%s" % (expTypePrefix, prefix))
    dataCollection.acquisitions[0].path_template.base_prefix = "%s%s" % (
        expTypePrefix,
        prefix,
    )
    logger.debug("name: %r" % dataCollection.acquisitions[0].path_template.get_prefix())
    dataCollection.set_name(dataCollection.acquisitions[0].path_template.get_prefix())
    dataCollection.set_number(runNumberDataCollection)
    dataCollection.set_enabled(True)
    dataCollection.ispyb_group_data_collections = True
    nodeId = serverProxy.queue_add_child(
        group_node_id, jsonpickle.encode(dataCollection)
    )
    logger.debug("Data node id: %r" % nodeId)
    serverProxy.queue_execute_entry_with_id(group_node_id)

    return {}
