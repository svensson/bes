def run(doTwoMeshes=False, **_):

    if doTwoMeshes:
        shortLineScan = True
    else:
        shortLineScan = False

    return {"shortLineScan": shortLineScan}
