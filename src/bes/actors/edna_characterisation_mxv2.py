import os
import shutil

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging
from bes.workflow_lib import kappa_reorientation


def run(
    beamline,
    workflowParameters,
    mxv1StrategyResultFile,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    motorPositions,
    flux,
    beamSizeX,
    beamSizeY,
    kappaStrategyOption,
    workflow_working_dir,
    characterisationTransmission=None,
    pyarch_html_dir=None,
    list_node_id=None,
    forcedSpaceGroup=None,
    resolution=None,
    sampleInfo=None,
    characterisationComment=None,
    diffractionSignalDetection="Dozor (macro-molecules)",
    anomalousData=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        workflowParameters = common.jsonLoads(workflowParameters)
        sampleInfo = common.jsonLoads(sampleInfo)

        # Enable logging in mxCuBE
        # os.environ["mxCuBE_XMLRPC_log"] = "Enabled"

        motorPositions = common.jsonLoads(motorPositions)
        phi = motorPositions["phi"]
        kappa = motorPositions["kappa"]
        kappa_phi = motorPositions["kappa_phi"]
        run_number = int(run_number)
        flux = float(flux)
        beamSizeX = float(beamSizeX)
        beamSizeY = float(beamSizeY)
        if characterisationTransmission is not None:
            characterisationTransmission = float(characterisationTransmission)
        if resolution is not None:
            resolution = float(resolution)

        workflowStepImagePyarchPath = None

        mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)
        mxv1StrategyResultBackup = mxv1StrategyResult

        kappaAction = None
        kappaStrategy = None
        newPhi = None
        newKappa = None
        newKappa_phi = None

        characterisationComment = None

        listImages = edna_mxv1.getListImagePath(
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            bFirstImageOnly=False,
        )

        firstImagePath = listImages[0]

        if beamline == "id30a3":
            edna_mxv1.h5ToCbf(beamline, listImages, workflow_working_dir)
            suffix = "cbf"

        (forcedSpaceGroup, newCharacterisationComment) = symmetry.checkForcedSpaceGroup(
            forcedSpaceGroup, sampleInfo, directory
        )

        if (
            characterisationComment is None
            and firstImagePath is not None
            and forcedSpaceGroup is not None
        ):
            characterisationComment = newCharacterisationComment
            if characterisationComment == "":
                characterisationComment = (
                    "Characterisation: Space group {0} forced.".format(forcedSpaceGroup)
                )
            else:
                characterisationComment += (
                    " Characterisation: Space group {0} forced.".format(
                        forcedSpaceGroup
                    )
                )
            ispyb.updateDataCollectionComment(
                beamline, firstImagePath, characterisationComment
            )
            ispyb.updateDataCollectionGroupComment(
                beamline, firstImagePath, characterisationComment
            )

        logger.info("Calculating kappa reorientation strategy using EDNA MXv2")
        logger.info("Kappa strategy option: %s" % kappaStrategyOption)
        if kappaStrategyOption.lower() == "anomalous":
            anomalousData = True
        if anomalousData:
            logger.info("Anomalous characterisation.")
        logger.warning("This operation can take up till one minute...")

        mxv1ResultCharacterisation = None
        mxv1ResultCharacterisation_Reference = None
        mxv2ResultCharacterisation = None
        mxv2DataCollection = None
        mxv2DataCollection_Reference = None
        suggestedStrategy = None
        possibleOrientations = None
        spaceGroup = None

        xsDataInterfaceResultv2_0, _ = edna_mxv1.mxv2ControlInterface(
            beamline,
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            phi,
            kappa,
            kappa_phi,
            flux,
            beamSizeX,
            beamSizeY,
            kappaStrategyOption,
            workflow_working_dir,
            mxv1ResultCharacterisation,
            mxv1ResultCharacterisation_Reference,
            mxv2DataCollection,
            mxv2DataCollection_Reference,
            possibleOrientations,
            transmission=characterisationTransmission,
            forcedSpaceGroup=forcedSpaceGroup,
            anomalousData=anomalousData,
        )
        xsDataResultCharacterisationv2_0 = (
            xsDataInterfaceResultv2_0.mxv2ResultCharacterisation
        )
        if xsDataResultCharacterisationv2_0 is None:
            logger.error(
                "No characterisation results obtained from the reference images!"
            )
            kappaAction = "NoStrategyResults"
        else:
            mxv2ResultCharacterisation = xsDataResultCharacterisationv2_0
            if xsDataInterfaceResultv2_0.mxv1ResultCharacterisation is not None:
                mxv1ResultCharacterisation = (
                    xsDataInterfaceResultv2_0.mxv1ResultCharacterisation
                )
            if (
                xsDataInterfaceResultv2_0.mxv1ResultCharacterisation_Reference
                is not None
            ):
                mxv1ResultCharacterisation_Reference = (
                    xsDataInterfaceResultv2_0.mxv1ResultCharacterisation_Reference
                )
            if xsDataInterfaceResultv2_0.mxv2DataCollection is not None:
                mxv2DataCollection = xsDataInterfaceResultv2_0.mxv2DataCollection
            if xsDataInterfaceResultv2_0.mxv2DataCollection_Reference is not None:
                mxv2DataCollection_Reference = (
                    xsDataInterfaceResultv2_0.mxv2DataCollection_Reference
                )
            if xsDataInterfaceResultv2_0.suggestedStrategy is not None:
                suggestedStrategy = xsDataInterfaceResultv2_0.suggestedStrategy
                mxv1StrategyResult = suggestedStrategy
            if xsDataInterfaceResultv2_0.possibleOrientations is not None:
                possibleOrientations = xsDataInterfaceResultv2_0.possibleOrientations
            strHtmlPagePath, strJsonPath = edna_mxv1.createSimpleHtmlPagev2_1(
                beamline,
                mxv2ResultCharacterisation.marshal(),
                workflow_working_dir,
                pyarch_html_dir,
            )
            if strHtmlPagePath is not None:
                if list_node_id is not None:
                    list_node_id = common.jsonLoads(list_node_id)
                    # Copy html files to workflow working dir
                    newHtmlDir = os.path.join(
                        workflow_working_dir,
                        os.path.basename(os.path.dirname(strHtmlPagePath)),
                    )
                    shutil.copytree(os.path.dirname(strHtmlPagePath), newHtmlDir)
                    newHtmlPagePath = os.path.join(
                        newHtmlDir, os.path.basename(strHtmlPagePath)
                    )
                    collect.displayNewHtmlPage(
                        beamline,
                        newHtmlPagePath,
                        "Kappa characterisation",
                        list_node_id,
                        token=token,
                    )

                bestWilsonPath = os.path.join(os.path.dirname(strHtmlPagePath), "B.jpg")
                if (
                    mxv1ResultCharacterisation.strategyResult is not None
                    and os.path.exists(bestWilsonPath)
                ):
                    workflowStepImagePyarchPath = bestWilsonPath
                elif len(mxv1ResultCharacterisation.thumbnailImage) > 1:
                    workflowStepImage = mxv1ResultCharacterisation.thumbnailImage[
                        0
                    ].path.value
                    if os.path.exists(workflowStepImage):
                        shutil.copy(workflowStepImage, os.path.dirname(strHtmlPagePath))
                        workflowStepImagePyarchPath = os.path.join(
                            os.path.dirname(strHtmlPagePath),
                            os.path.basename(workflowStepImage),
                        )

            spaceGroup = edna_mxv1.extractSpaceGroupFromMXv2Results(
                mxv2ResultCharacterisation.marshal()
            )
            if kappa_reorientation.isIncompatibleKappaStrategy(
                beamline, kappaStrategyOption, spaceGroup
            ):
                logger.warning("Incompatible kappa strategy!")
                kappaAction = "IncompatibleKappaStrategy"
                mxv1ResultCharacterisation = None
                mxv1ResultCharacterisation_Reference = None
                mxv2ResultCharacterisation = None
                mxv2DataCollection = None
                mxv2DataCollection_Reference = None
                mxv1StrategyResult = mxv1StrategyResultBackup
                suggestedStrategy = None
                possibleOrientations = None
                kappaStrategyOption = "Cell"
                status = "failure"
                workflowStepType = "Incompatible kappa strategy"
            else:
                status = "success"
                workflowStepType = "Kappa Characterisation"
                dictKappaAngles = edna_mxv1.extractKappaAnglesFromComment(
                    beamline, mxv2ResultCharacterisation.marshal()
                )
                if "strategy" in dictKappaAngles:
                    kappaStrategy = dictKappaAngles["strategy"]
                    if kappaStrategy == "MERGED" and expTypePrefix == "ref-":
                        logger.info("New kappa angles calculated!")
                        kappaAction = "NewKappaAngles"
                        newPhi = dictKappaAngles["omega"]
                        newKappa = dictKappaAngles["kappa"]
                        newKappa_phi = dictKappaAngles["phi"]
                    elif kappaStrategy == "MERGED" and expTypePrefix == "ref-kappa-":
                        logger.info("Optimal data collecting strategy found.")
                        kappaAction = "OptimalDataCollectionStrategy"
                    elif kappaStrategy == "COPIED":
                        logger.info("Optimal data collecting strategy found.")
                        kappaAction = "OptimalDataCollectionStrategy"
                    else:
                        logger.error(
                            "No kappa angles could be calculated from these reference images! Calculated strategy has nevertheless been transferred to mxCuBE queue."
                        )
                        kappaAction = "NoNewKappaAngles"
                else:
                    logger.error("No kappa strategy calculation results!")
                    kappaAction = "NoStrategyResults"

            if workflowStepImagePyarchPath is not None:
                ispyb.storeWorkflowStep(
                    beamline,
                    workflowParameters["ispyb_id"],
                    workflowStepType,
                    status,
                    imageResultFilePath=workflowStepImagePyarchPath,
                    htmlResultFilePath=strHtmlPagePath,
                    resultFilePath=strJsonPath,
                )

        logger.debug("Kappa action: %s" % kappaAction)

        mxv1StrategyResultFile = path.createNewXSDataFile(
            workflow_working_dir, mxv1StrategyResult, filePrefix="mxv1StrategyResult"
        )
        mxv1ResultCharacterisationFile = path.createNewXSDataFile(
            workflow_working_dir,
            mxv1ResultCharacterisation,
            filePrefix="mxv1ResultCharacterisation",
        )
        mxv2ResultCharacterisationFile = path.createNewXSDataFile(
            workflow_working_dir,
            mxv2ResultCharacterisation,
            filePrefix="mxv2ResultCharacterisation",
        )
        mxv1ResultCharacterisation_ReferenceFile = path.createNewXSDataFile(
            workflow_working_dir,
            mxv1ResultCharacterisation_Reference,
            filePrefix="mxv1ResultCharacterisation_Reference",
        )
        mxv2DataCollectionFile = path.createNewXSDataFile(
            workflow_working_dir, mxv2DataCollection, filePrefix="mxv2DataCollection"
        )
        mxv2DataCollection_ReferenceFile = path.createNewXSDataFile(
            workflow_working_dir,
            mxv2DataCollection_Reference,
            filePrefix="mxv2DataCollection_Reference",
        )
        suggestedStrategyFile = path.createNewXSDataFile(
            workflow_working_dir, suggestedStrategy, filePrefix="suggestedStrategy"
        )
        possibleOrientationsFile = path.createNewXSDataFile(
            workflow_working_dir,
            possibleOrientations,
            filePrefix="possibleOrientations",
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "kappaAction": kappaAction,
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
        "mxv1ResultCharacterisationFile": mxv1ResultCharacterisationFile,
        "mxv2ResultCharacterisationFile": mxv2ResultCharacterisationFile,
        "kappaStrategy": kappaStrategy,
        "newPhi": newPhi,
        "newKappa": newKappa,
        "newKappa_phi": newKappa_phi,
        "kappaAction": kappaAction,
        "mxv1ResultCharacterisation_ReferenceFile": mxv1ResultCharacterisation_ReferenceFile,
        "mxv2DataCollectionFile": mxv2DataCollectionFile,
        "mxv2DataCollection_ReferenceFile": mxv2DataCollection_ReferenceFile,
        "suggestedStrategyFile": suggestedStrategyFile,
        "possibleOrientationsFile": possibleOrientationsFile,
        "kappaStrategyOption": kappaStrategyOption,
        "spaceGroup": spaceGroup,
        "characterisationComment": characterisationComment,
        "anomalousData": anomalousData,
    }
