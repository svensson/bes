import os
import json
import pprint

from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, raw_directory, token, **_):
    logger = workflow_logging.getLogger(
        beamline, workflowParameters=workflowParameters, token=token
    )
    motorPositions = collect.readMotorPositions(beamline, token=token)
    logger.debug(pprint.pformat(motorPositions))
    raw_process_directory = raw_directory.replace("RAW_DATA", "PROCESSED_DATA")
    if not os.path.exists(raw_process_directory):
        os.makedirs(raw_process_directory, exist_ok=True, mode=0o755)
    file_path = os.path.join(raw_process_directory, "left_pos.json")
    logger.debug(file_path)
    if os.path.exists(file_path):
        logger.warning("Overwring existing left position file")
    with open(file_path, "w") as fd:
        fd.write(json.dumps(motorPositions, indent=4))
    logger.info("File {0} written.".format(file_path))

    return {"left_pos_path": file_path}
