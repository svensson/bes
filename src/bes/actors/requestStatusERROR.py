from bes.workflow_lib import mongodb


def run(besParameters=None, **_):
    # Temporary solution - ideally this should be done through eworks events

    if besParameters:
        bes_request_id = besParameters["request_id"]
        mongodb.setMongoStatus(bes_request_id, "error")

    return {}
