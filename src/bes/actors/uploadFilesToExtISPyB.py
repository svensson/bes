from bes.workflow_lib import common
from bes.workflow_lib import extISPyB


def run(
    workflowParameters,
    extispyb,
    startDate,
    projectId,
    runId,
    extISPyBStatus,
    token,
    fileList=[],
    **_,
):

    fileList = common.jsonLoads(fileList)
    workflowParameters = common.jsonLoads(workflowParameters)

    # Append workflow log file
    fileList.append(["workflowLog", workflowParameters["logFile"]])

    # Upload results
    extISPyB.uploadResult(
        workflowParameters["type"],
        workflowParameters["version"],
        extispyb,
        startDate,
        projectId,
        runId,
        fileList,
        extISPyBStatus,
        token,
    )

    return {"fileList": fileList}
