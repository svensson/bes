#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "15/02/2021"

from bes.workflow_lib import path
from bes import config
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    raw_directory,
    characterisationExposureTime,
    beamParameters,
    characterisationOscillationRange,
    no_reference_images,
    prefix,
    expTypePrefix,
    suffix,
    run_number,
    highResolution,
    motorPositions,
    sampleInfo=None,
    characterisationTransmission=None,
    angle_between_reference_images=90.0,
    resolution=None,
    token=None,
    newDataCollection=False,
    wavelength=None,
    dozorVisibleResolutionVertical=None,
    dozorVisibleResolutionHorizontal=None,
    fineSlicedReferenceImages=False,
    lowResolution=None,
    aimedResolution=None,
    workflow_index=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    beamParameters = common.jsonLoads(beamParameters)
    motorPositions = common.jsonLoads(motorPositions)
    angle_between_reference_images = float(angle_between_reference_images)
    wavelength = float(wavelength)

    characterisationExposureTime = float(characterisationExposureTime)
    if characterisationTransmission is not None:
        characterisationTransmission = float(characterisationTransmission)
    else:
        characterisationTransmission = beamParameters[
            "defaultCharacterisationTransmission"
        ]
    logger.info(
        "Characterisation reference image tranmsission set to : {0} %".format(
            characterisationTransmission
        )
    )

    if not newDataCollection:
        if sampleInfo is not None and aimedResolution is not None:
            resolution = aimedResolution
        elif (
            dozorVisibleResolutionVertical is not None
            and dozorVisibleResolutionHorizontal is not None
        ):
            minDozorVisibleResolution = min(
                dozorVisibleResolutionVertical, dozorVisibleResolutionHorizontal
            )
            logger.debug(
                "minDozorVisibleResolution : {0} A".format(minDozorVisibleResolution)
            )
            if minDozorVisibleResolution < 2.0:
                minDozorVisibleResolution = minDozorVisibleResolution - 0.4
                logger.debug(
                    "minDozorVisibleResolution decreased to : {0} A".format(
                        minDozorVisibleResolution
                    )
                )
            if lowResolution is None:
                maxDetectorDistance = config.get_value(
                    beamline, "Detector", "distanceMax"
                )
                minDetectorResolution = collect.detectorDistanceToResolution(
                    beamline, maxDetectorDistance, wavelength
                )
                # Round to one decimal down
                minDetectorResolution = int(minDetectorResolution * 10) / 10.0
                logger.debug(
                    "minDetectorResolution : {0} A".format(minDetectorResolution)
                )
                resolution = min(minDozorVisibleResolution, minDetectorResolution, 4.0)
            else:
                logger.debug("lowResolution : {0} A".format(lowResolution))
                resolution = min(minDozorVisibleResolution, lowResolution, 4.0)
            logger.debug("min resolution : {0} A".format(resolution))
    elif resolution is not None:
        resolution = float(resolution)

    # Max resolution
    if highResolution is not None:
        logger.debug("highResolution : {0} A".format(highResolution))
        if resolution is None:
            resolution = highResolution
        else:
            resolution = max(resolution, highResolution)
    logger.info(
        "Characterisation reference image resolution set to : {0:.02f} A".format(
            resolution
        )
    )

    # Decrease characterisationOscillationRange if at high resolution
    if (
        resolution is not None
        and resolution <= 1.6
        and characterisationOscillationRange > 0.5
    ):
        characterisationOscillationRange = 0.5
        logger.info(
            "Characterisation reference image oscillation range set to : {0} degrees".format(
                characterisationOscillationRange
            )
        )

    if fineSlicedReferenceImages:
        if beamline in ["id30a1", "id30a2"]:
            exposure_time = characterisationExposureTime / 10
            logger.debug(
                "Fast characterisationexposure time: {0}".format(exposure_time)
            )
            osc_range = characterisationOscillationRange / 10
            logger.debug("Fast characterisation osc range time: {0}".format(osc_range))
            mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
                exposureTime=exposure_time,
                transmission=characterisationTransmission,
                number_of_images=1,
                osc_range=osc_range,
                phi=0.0,
            )
        else:
            mxv1StrategyResult = edna_mxv1.create_fine_sliced_ref_data_collect(
                characterisationExposureTime,
                characterisationTransmission,
                characterisationOscillationRange,
                no_reference_images,
                angle_between_reference_images,
                motorPositions["phi"],
            )
        expTypePrefix = "ref-fineslice-"
    else:
        mxv1StrategyResult = edna_mxv1.create_ref_data_collect(
            characterisationExposureTime,
            characterisationTransmission,
            characterisationOscillationRange,
            no_reference_images,
            angle_between_reference_images,
            motorPositions["phi"],
        )
        if expTypePrefix != "ref-":
            logger.warning(
                "ExpTypePrefix for reference images was '{0}', reset to 'ref-'".format(
                    expTypePrefix
                )
            )
            expTypePrefix = "ref-"

    directory, run_number = path.createUniqueICATDirectory(
        raw_directory=raw_directory,
        run_number=run_number,
        workflow_index=workflow_index,
        expTypePrefix=expTypePrefix,
    )

    logger.debug("Reference image strategy:")
    logger.debug(mxv1StrategyResult)

    ednaImagePath = edna_mxv1.createEdnaImagePath(
        beamline,
        directory,
        prefix,
        expTypePrefix,
        suffix,
        no_reference_images,
        run_number,
    )
    mxv1StrategyResultFile = path.createNewXSDataFile(
        workflowParameters["workingDir"],
        mxv1StrategyResult,
        "referenceImageDataCollection",
    )

    # Fake value for testing of GPhL workflows
    # resolution = 1.363

    return {
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
        "mxv1CharacterisationStrategyResultFile": mxv1StrategyResultFile,
        "characterisationOscillationRange": characterisationOscillationRange,
        "ednaImagePath": ednaImagePath,
        "directory": directory,
        "expTypePrefix": expTypePrefix,
        "resolution": resolution,
        "directory": directory,
        "run_number": run_number,
    }
