from bes.workflow_lib import workflow_logging, common


def run(beamline, workflowParameters, phi, run_number, token=None, **_):

    try:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        currentPhi = phi
        newPhi = currentPhi + 90
        logger.info(
            "Rotating sample omega from %.3f to %.3f degrees" % (currentPhi, newPhi)
        )
        # logger.info("Increasing run number to: %d" % run_number)
    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "newPhi": newPhi,
    }
