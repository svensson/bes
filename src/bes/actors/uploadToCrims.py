"""
Created on Feb 9, 2015

@author: svensson
"""

# from bes.workflow_lib import crims
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    prefix,
    dataCollectionId=None,
    dataCollectionGroupId=None,
    token=None,
    **_,
):

    crimsStatus = None
    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    logger.info("No upload to CRIMS")

    # try:
    #     crimsStatus = None
    #     workflowParameters = common.jsonLoads(workflowParameters)
    #     logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    #     if beamline in ["id30a2", "id30b"] and dataCollectionId is not None:
    #         logger.info("Uploading crystal_uuid {0} to CRIMS".format(prefix))
    #         url = "http://htx1.embl.fr:8026/api/v1/ispyb_datacollections"
    #         crimsStatus = crims.sendCrystalDataCollectionIds(url, prefix, dataCollectionId)
    #         logger.debug("CRIMS return status: {0}".format(crimsStatus))
    #         if crimsStatus is not None:
    #             crimsStatus = json.loads(crimsStatus)
    #             if "data" in crimsStatus:
    #                 if "success" in crimsStatus["data"]:
    #                     if "message" in crimsStatus["data"]["success"]:
    #                         logger.info("CRIMS: {0}".format(crimsStatus["data"]["success"]["message"]))
    #
    # except Exception:
    #     logger.info()

    return {"crimsStatus": crimsStatus}
