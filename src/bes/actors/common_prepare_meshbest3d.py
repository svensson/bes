def run(beamline, expTypePrefix="mesh-", **_):
    return {
        "workflow_title": "MeshBest 3D on %s" % beamline,
        "workflow_type": "MeshScan",
        "expTypePrefix": expTypePrefix,
        "diffractionSignalDetection": "DozorMeshBest",
        "doMesh2d": True,
        "indexMesh2d": 1,
        "bestPosition": None,
        "noMesh2d": 2,
        "meshBestType": "simple",
    }
