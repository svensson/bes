import tempfile
from bes.workflow_lib import edna_mxv1


def run(directory=None, beamline=None, **_):

    return_dict = {}

    if directory is None:
        beamline = "simulator"
        return_dict = {
            "directory": tempfile.mkdtemp(prefix="WorkflowTroubleShooting_"),
            "beamline": beamline,
            "prefix": "testAutoMesh",
            "run_number": 1,
        }
    elif beamline is None:
        try:
            beamline = edna_mxv1.extractBeamlineFromDirectory(directory)
        except Exception:
            beamline = "unknown"

    return_dict["workflow_title"] = "Test auto mesh on {0}".format(beamline)
    return_dict["workflow_type"] = "TroubleShooting"
    return_dict["beamline"] = beamline.lower()

    return return_dict
