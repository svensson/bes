import time
import smtplib

from email.mime.text import MIMEText

from bes.workflow_lib import common


def run(**kwargs):
    doContinue = True

    while doContinue:
        statusBES = common.getBESWorkflowStatus(kwargs["besParameters"])

        message = "Input kwargs arguments: {0}.\n".format(kwargs)
        message += "Status BES: {0}".format(statusBES)

        mime_text_message = MIMEText(message)
        mime_text_message["Subject"] = "Test message"
        mime_text_message["From"] = "workflow@esrf.fr"
        mime_text_message["To"] = "svensson@esrf.fr"

        smtp = smtplib.SMTP("localhost")
        smtp.sendmail(
            "workflow@esrf.fr", ["svensson@esrf.fr"], mime_text_message.as_string()
        )
        smtp.quit()

        if statusBES != "STARTED":
            doContinue = False
        else:
            time.sleep(5)


if __name__ == "__main__":
    run(beamline="simulator_mxcube")
