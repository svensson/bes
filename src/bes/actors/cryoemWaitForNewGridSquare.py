"""
Initialisation of a workflow
"""

import time
import json

from bes.workflow_lib import common
from bes.workflow_lib import cryoem
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    gridSquaresJsonFile,
    noGridSquaresArchived=None,
    maxNoGridSquares=None,
    noMoviesArchived=None,
    maxNoMoviesToArchive=None,
    currentMovie=None,
    currentGridSquare=None,
    REFID=None,
    REQUESTID=None,
    **_,
):

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(None, workflowParameters)
    logger.info("*********** cryoemWaitForNewGridSquare **********")

    dictGridSquares = json.loads(open(gridSquaresJsonFile).read())

    continueToWait = True
    timeOut = False

    if noGridSquaresArchived is not None and maxNoGridSquares is not None:
        if noGridSquaresArchived == maxNoGridSquares:
            logger.warning("Max no archived grid squares reached!")
            continueToWait = False
            timeOut = True

    if noMoviesArchived is not None and maxNoMoviesToArchive is not None:
        if noMoviesArchived == maxNoMoviesToArchive:
            logger.warning("Max no archived movies reached!")
            continueToWait = False
            timeOut = True

    startTime = time.time()
    index = 0
    archiveGridSquare = None
    while continueToWait:
        # Check if grid square started:
        if currentGridSquare is None:
            for gridSquare in dictGridSquares:
                if dictGridSquares[gridSquare]["status"] == "started":
                    if "movies" in dictGridSquares[gridSquare]:
                        noMovies = len(dictGridSquares[gridSquare]["movies"])
                        if noMovies > 0:
                            logger.info(
                                "Already started grid square: {0}, {1} movies".format(
                                    gridSquare, noMovies
                                )
                            )
                            currentGridSquare = gridSquare
                            archiveGridSquare = gridSquare
                            continueToWait = False
        else:
            # Check for new grid squares
            if currentGridSquare is None:
                listGridSquareCreatedSorted = cryoem.updateDictForNewGridSquares(
                    directory, dictGridSquares
                )
                if len(listGridSquareCreatedSorted) > 0:
                    ctimeCreated, currentGridSquare = listGridSquareCreatedSorted[0]
            if currentGridSquare is not None:
                dictGridSquare = dictGridSquares[currentGridSquare]
                # Check for new movies
                listMovieCreatedSorted = cryoem.updateDictGridSquareForNewMovies(
                    dictGridSquare
                )
                if len(listMovieCreatedSorted) > 0:
                    for movie in listMovieCreatedSorted:
                        logger.info("New movie detected: {0}".format(movie))
                    continueToWait = False
                else:
                    # No new movies in the current grid square so check for new grid squares
                    listGridSquareCreatedSorted = cryoem.updateDictForNewGridSquares(
                        directory, dictGridSquares
                    )
                    if len(listGridSquareCreatedSorted) > 0:
                        # Mark current grid square as finished and to be archived
                        archiveGridSquare = currentGridSquare
                        dictGridSquare["status"] = "finished"
                        dictGridSquare["endTime"] = time.ctime(time.time())
                        ctimeCreated, currentGridSquare = listGridSquareCreatedSorted[0]
                        dictGridSquare = dictGridSquares[currentGridSquare]
                        dictGridSquare["status"] = "started"
                        dictGridSquare["startTime"] = time.ctime(time.time())
                        logger.info(
                            "New current grid square: {0}".format(currentGridSquare)
                        )
                        continueToWait = False
                    else:
                        logger.info("Waiting for movie / grid square, sleeping 30 s")
                        time.sleep(30)
                        timeElapsed = time.time() - startTime
                        if timeElapsed > 10:
                            continueToWait = False
                            if dictGridSquare["status"] == "started":
                                logger.info(
                                    "Stopped waiting for new files, archiving current grid square."
                                )
                                archiveGridSquare = currentGridSquare
                                dictGridSquare["status"] = "finished"
                                dictGridSquare["endTime"] = time.ctime(time.time())
                            else:
                                logger.info("Time-out after 120 s!")
                                timeOut = True

        index += 1
        if index > 100:
            continueToWait = False
            timeOut = True

    open(gridSquaresJsonFile, "w").write(json.dumps(dictGridSquares, indent=4))

    return {
        "currentGridSquare": currentGridSquare,
        "archiveGridSquare": archiveGridSquare,
        "timeOut": timeOut,
    }
