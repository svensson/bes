from bes.workflow_lib import common


def run(beamline, directory, **kwargs):

    workflowParameters = {}
    workflowParameters["title"] = "MXPressE: X-centre, eEDNA + dc on %s" % beamline
    workflowParameters["type"] = "MXPressE"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "do_data_collect": True,
        "anomalousData": False,
        "doCharacterisation": True,
        "numberOfPositions": 2,
        "automatic_mode": True,
    }
