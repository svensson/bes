def run(beamline, directory, expTypePrefix="mesh-", **_):

    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressERed: X-centre, eEDNA high red. + dc on %s" % beamline
    )
    workflowParameters["type"] = "MXPressE"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": expTypePrefix,
        "resolution": 2.0,
        "meshZigZag": False,
        "do_data_collect": True,
        "aimedMultiplicity": 6,
        "sampleSusceptibility": 4,
        "anomalousData": False,
        "doCharacterisation": True,
        "automatic_mode": True,
    }
