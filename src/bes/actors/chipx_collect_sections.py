import os
import sys
import math
import pprint
import matplotlib.pyplot as plt

from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

from chipx.image_tools import read_grayscale_image
from chipx.image_tools import get_capillary_vertical_position

sys.path.insert(0, "/opt/pxsoft/bes/vgit/linux-x86_64/id30b/chipx")


def run(
    beamline,
    prefix,
    run_number,
    exposureTime,
    oscillationWidth,
    workflowParameters,
    transmission,
    resolution,
    raw_directory,
    sample_node_id,
    aperture,
    start_phi,
    end_phi,
    no_sections,
    no_lines=7,
    token=None,
    **_,
):

    capillary_length = 40.0  # mm

    logger = workflow_logging.getLogger(
        beamline, workflowParameters=workflowParameters, token=token
    )
    raw_process_directory = raw_directory.replace("RAW_DATA", "PROCESSED_DATA")

    workflow_working_dir = workflowParameters["workingDir"]

    list_motor_positions = []
    # Collect motor positions
    logger.info("Collectiong motor positions for all sections")
    motor_positions = collect.readMotorPositions(
        beamline, read_focus=False, token=token
    )
    start_phiy = motor_positions["phiy"]
    for section_index in range(no_sections + 1):
        logger.info("Section {0}".format(section_index))
        motor_positions = collect.readMotorPositions(
            beamline, read_focus=False, token=token
        )
        # Move phi and phiy to the section start
        if (section_index % 2) == 0:
            phi = start_phi
        else:
            phi = end_phi
        # Move to the right of the capillary is in the negative direction
        delta_phiy = capillary_length * section_index / no_sections
        new_phiy = start_phiy - delta_phiy
        collect.moveMotors(
            beamline, raw_directory, {"phi": phi, "phiy": new_phiy}, token=token
        )
        # Centre capillary
        snap_shot_path = os.path.join(
            workflow_working_dir, "snapshot_{0:02d}.png".format(section_index + 1)
        )
        collect.saveSnapshot(beamline, snap_shot_path, token=token)
        logger.debug(snap_shot_path)
        numpy_image = read_grayscale_image(snap_shot_path)
        y_size, x_size = numpy_image.shape
        vertical_pixel_pos = get_capillary_vertical_position(numpy_image)
        logger.info("Vertical position: {0}".format(vertical_pixel_pos))
        if vertical_pixel_pos is None:
            logger.warning("No capillary position could be obtained!")
            logger.warning("Using current position.")
        else:
            plt.imshow(numpy_image, cmap=plt.get_cmap("gray"))
            y_size, x_size = numpy_image.shape
            plt.plot(
                [0, x_size - 1],
                [vertical_pixel_pos, vertical_pixel_pos],
                linewidth=2,
                color="blue",
            )
            result_file_name = "result_" + os.path.basename(snap_shot_path)
            result_file_path = os.path.join(workflow_working_dir, result_file_name)
            plt.savefig(result_file_path, dpi=300)
            plt.close()
            delta_y = y_size / 2 - vertical_pixel_pos
            logger.info("Delta y (in pixels): {0}".format(delta_y))
            delta_y_mm = 1.9 * delta_y / 1000
            logger.info("Delta y (in mm): {0}".format(delta_y_mm))
            sampx = motor_positions["sampx"]
            sampy = motor_positions["sampy"]
            delta_sampx = delta_y_mm * math.sin(math.radians(motor_positions["phi"]))
            delta_sampy = delta_y_mm * math.cos(math.radians(motor_positions["phi"]))
            new_sampx = float(sampx + delta_sampx)
            new_sampy = float(sampy - delta_sampy)
            dict_position = {"sampx": new_sampx, "sampy": new_sampy}
            collect.moveMotors(beamline, raw_directory, dict_position, token=token)
        # Save the position
        motor_positions = collect.readMotorPositions(
            beamline, read_focus=False, token=token
        )
        list_motor_positions.append(motor_positions)

    # Create list of data collections - section by section
    for section_index in range(no_sections):
        first_image = 1
        logger.info("Collecting section {0}".format(section_index + 1))
        # Create list of data collections - line by line
        direction = 1
        list_queue_entries = []
        line_prefix = "section_{0}_{1}".format(section_index + 1, prefix)
        for index_line in range(no_lines):
            logger.info(
                "Collecting line {0}, direction {1}".format(index_line + 1, direction)
            )
            if direction > 0:
                start_pos = list_motor_positions[section_index]
                end_pos = list_motor_positions[section_index + 1]
            else:
                start_pos = list_motor_positions[section_index + 1]
                end_pos = list_motor_positions[section_index]
            phiy_s = start_pos["phiy"]
            phiz_s = start_pos["phiz"]
            phiy_e = end_pos["phiy"]
            phiz_e = end_pos["phiz"]
            delta_h = (index_line - int(no_lines / 2)) * 0.025
            delta_sampx_s = -delta_h * math.sin(math.radians(start_pos["phi"]))
            delta_sampy_s = delta_h * math.cos(math.radians(start_pos["phi"]))
            delta_sampx_e = -delta_h * math.sin(math.radians(end_pos["phi"]))
            delta_sampy_e = delta_h * math.cos(math.radians(end_pos["phi"]))
            sampx_s = start_pos["sampx"] + delta_sampx_s
            sampy_s = start_pos["sampy"] + delta_sampy_s
            sampx_e = end_pos["sampx"] + delta_sampx_e
            sampy_e = end_pos["sampy"] + delta_sampy_e
            rotation_axis_start = round(start_pos["phi"], 2)
            motorPositions4dscan = {
                "phiy_s": phiy_s,
                "phiz_s": phiz_s,
                "sampx_s": sampx_s,
                "sampy_s": sampy_s,
                "phiy_e": phiy_e,
                "phiz_e": phiz_e,
                "sampx_e": sampx_e,
                "sampy_e": sampy_e,
            }
            # logger.debug(pprint.pformat(motorPositions4dscan)
            fileData = {
                "directory": raw_directory,
                "expTypePrefix": "",
                "firstImage": first_image,
                "prefix": line_prefix,
                "process_directory": raw_process_directory,
                "runNumber": run_number,
            }
            # logger.debug(pprint.pformat(fileData)
            noImages = int(
                abs(end_pos["phi"] - start_pos["phi"]) / oscillationWidth + 0.5
            )
            if end_pos["phi"] > start_pos["phi"]:
                phi_direction = 1
            else:
                phi_direction = -1
            subWedgeData = {
                "doProcessing": True,
                "exposureTime": exposureTime,
                "noImages": noImages,
                "oscillationWidth": oscillationWidth * phi_direction,
                "rotationAxisStart": rotation_axis_start,
                "transmission": transmission,
            }
            # logger.debug(pprint.pformat(subWedgeData)
            queue_entry = {
                "fileData": fileData,
                "motorPositions4dscan": motorPositions4dscan,
                "subWedgeData": subWedgeData,
            }
            list_queue_entries.append(queue_entry)
            first_image += noImages
            direction = direction * -1
            # End of line
        # Collect section
        logger.debug(pprint.pformat(list_queue_entries))
        motorPositions = collect.readMotorPositions(beamline, token=token)
        force_not_shutterless = False
        sample_node_id = 1
        _ = collect.executeQueueForMeshScan(
            beamline=beamline,
            workflow_index=workflowParameters["index"],
            workflow_type=workflowParameters["type"],
            meshQueueList=list_queue_entries,
            group_node_id=None,
            sample_node_id=sample_node_id,
            resolution=resolution,
            motorPositions=motorPositions,
            force_not_shutterless=force_not_shutterless,
            process_data=True,
            token=token,
        )
        # End of line

    # Create list of data collections - line by line
    # direction = 1
    # for index_line in range(no_lines):
    #     logger.info("Collecting line {0}".format(index_line + 1))
    #     list_queue_entries = []
    #     first_image = 1
    #     line_prefix = "line_{0}_{1}".format(index_line + 1, prefix)
    #     delta_h = (index_line - int(no_lines / 2)) * 0.025
    #     if direction == 1:
    #         section_range = range(no_sections)
    #     else:
    #         section_range = range(no_sections, 0, -1)
    #     for section_index in section_range:
    #         logger.info("Collecting section {0}".format(section_index + 1))
    #         logger.debug("direction: {0}".format(direction))
    #         start_pos = list_motor_positions[section_index]
    #         end_pos = list_motor_positions[section_index + direction]
    #         phiy_s = start_pos["phiy"]
    #         phiz_s = start_pos["phiz"]
    #         phiy_e = end_pos["phiy"]
    #         phiz_e = end_pos["phiz"]
    #         delta_sampx_s = -delta_h * math.sin(math.radians(start_pos["phi"]))
    #         delta_sampy_s = delta_h * math.cos(math.radians(start_pos["phi"]))
    #         delta_sampx_e = -delta_h * math.sin(math.radians(end_pos["phi"]))
    #         delta_sampy_e = delta_h * math.cos(math.radians(end_pos["phi"]))
    #         sampx_s = start_pos["sampx"] + delta_sampx_s
    #         sampy_s = start_pos["sampy"] + delta_sampy_s
    #         sampx_e = end_pos["sampx"] + delta_sampx_e
    #         sampy_e = end_pos["sampy"] + delta_sampy_e
    #         rotation_axis_start = round(start_pos["phi"], 2)
    #         motorPositions4dscan = {
    #             "phiy_s": phiy_s,
    #             "phiz_s": phiz_s,
    #             "sampx_s": sampx_s,
    #             "sampy_s": sampy_s,
    #             "phiy_e": phiy_e,
    #             "phiz_e": phiz_e,
    #             "sampx_e": sampx_e,
    #             "sampy_e": sampy_e,
    #         }
    #         # logger.debug(pprint.pformat(motorPositions4dscan)
    #         fileData = {
    #             "directory": raw_directory,
    #             "expTypePrefix": "",
    #             "firstImage": first_image,
    #             "prefix": line_prefix,
    #             "process_directory": raw_process_directory,
    #             "runNumber": run_number,
    #         }
    #         # logger.debug(pprint.pformat(fileData)
    #         noImages = int(
    #             abs(end_pos["phi"] - start_pos["phi"]) / oscillationWidth + 0.5
    #         )
    #         if end_pos["phi"] > start_pos["phi"]:
    #             phi_direction = 1
    #         else:
    #             phi_direction = -1
    #         subWedgeData = {
    #             "doProcessing": True,
    #             "exposureTime": exposureTime,
    #             "noImages": noImages,
    #             "oscillationWidth": oscillationWidth * phi_direction,
    #             "rotationAxisStart": rotation_axis_start,
    #             "transmission": transmission,
    #         }
    #         # logger.debug(pprint.pformat(subWedgeData)
    #         queue_entry = {
    #             "fileData": fileData,
    #             "motorPositions4dscan": motorPositions4dscan,
    #             "subWedgeData": subWedgeData,
    #         }
    #         list_queue_entries.append(queue_entry)
    #         first_image += noImages
    #         # End of section
    #     # Collect line
    #     logger.debug(pprint.pformat(list_queue_entries))
    #     motorPositions = collect.readMotorPositions(beamline, token=token)
    #     force_not_shutterless = False
    #     sample_node_id = 1
    #     _ = collect.executeQueueForMeshScan(
    #         beamline=beamline,
    #         workflow_index=workflowParameters["index"],
    #         workflow_type=workflowParameters["type"],
    #         meshQueueList=list_queue_entries,
    #         group_node_id=None,
    #         sample_node_id=sample_node_id,
    #         resolution=resolution,
    #         motorPositions=motorPositions,
    #         force_not_shutterless=force_not_shutterless,
    #         process_data=True,
    #         token=token,
    #     )
    #     direction = direction * -1
    #     # End of line

    return {}
