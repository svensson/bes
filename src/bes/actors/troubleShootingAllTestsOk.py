from bes.workflow_lib import workflow_logging, common


def run(beamline, workflowParameters, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        # All tests ok!

        logger.info("Trouble shooting: all tests ok!")

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
