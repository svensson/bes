#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

import os
import time

from bes.workflow_lib import path
from bes.workflow_lib import grid
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import edna2_tasks
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


# beamline,
# workflowParameters,
# grid_info,
# meshQueueListFile,
# motorPositions,
# directory,
# meshPositionFile,
# prefix,
# suffix,
# workflow_id,
# run_number,
# expTypePrefix,
# sampleInfo=None,
# doMeshIndexing=False,
# dataCollectionGroupComment="",
# firstImagePath="",
# waitForMeshResults=True,
# force_not_shutterless=False,
# raw_directory=None,
# pyarch_html_dir=None,
# diffractionSignalDetection="DozorM2 (macro-molecules crystal detection)",
# resolution=None,
# useFastMesh=False,
# meshZigZag=False,
# token=None,
# detector_roi_mode=None,


def run(**inData):

    inputDozor = None
    dozorAllFile = None
    message_text = ""
    meshPositionsAcquired = False
    resultMeshPath = None

    if "besParameters" not in inData:
        raise BaseException("No BES parameters!")

    beamline = inData["beamline"]
    workflowParameters = inData["workflowParameters"]
    token = inData["token"]
    workflow_working_dir = workflowParameters["workingDir"]
    useFastMesh = inData.get("useFastMesh", True)
    sampleInfo = inData.get("sampleInfo")
    synchronizeBestPosition = inData.get("synchronizeBestPosition", True)

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    meshPositions = path.parseJsonFile(inData["meshPositionFile"])
    meshQueueList = path.parseJsonFile(inData["meshQueueListFile"])

    if sampleInfo is None:
        groupNodeId = None
        sampleNodeId = -1
    else:
        groupNodeId = sampleInfo["groupNodeId"]
        sampleNodeId = sampleInfo["nodeId"]

    logger.info("Starting online data processing")
    edPlugin = startProcessing(inData, logger, meshPositions, useFastMesh)

    logger.info("Starting data collection")
    listNodeId, listImagePath, groupNodeId, dataCollectionGroupId = executeMesh(
        inData, logger, useFastMesh, meshQueueList, groupNodeId, sampleNodeId
    )
    logger.info("Data collection finished")

    sampleInfo["groupNodeId"] = groupNodeId
    firstImagePath = listImagePath[0]

    dataCollectionGroupComment = updateDataCollectionGroupComment(
        inData, firstImagePath
    )

    dataCollectionId = findDataCollectionId(beamline, firstImagePath)

    # Correct for encoder positions if run on id30a1
    if beamline == "id30a1":
        try:
            diagFile = config.get_value(beamline, "EncoderPositionFile", "path")
            if os.path.exists(diagFile):
                meshPositions = collect.correctUsingEncoderPositions(
                    diagFile, meshPositions
                )
        except Exception as e:
            logger.warning("Cannot read diagnostic file: {0}".format(e))

    waitForMeshResults = inData.get("waitForMeshResults", True)
    # Check if we should do processing
    if not waitForMeshResults:
        message_text = "Wait for processing disabled (user input)"
        logger.debug(message_text)
        meshPositionsAcquired = False
    else:
        logger.info("Checking that images have been written to disk")
        waitForImages(inData, logger, listImagePath, useFastMesh)
        logger.info("Waiting for data processing to finish...")
        (
            meshPositions,
            inputDozor,
            dozorAllFile,
        ) = edna2_tasks.storeImageQualityIndicatorsFromMeshPositionsSynchronize(
            edPlugin, meshPositions
        )
        if len(meshPositions) > 0:
            meshPositionsAcquired = True
            logger.info("Processing finished")
        else:
            message_text = "No processing results!"
            logger.info(message_text)

    logger.info("Saving mesh results")
    if meshPositionsAcquired and dozorAllFile is not None:
        saveMeshResults(inData, logger, meshPositions, inputDozor, dozorAllFile)

    # Remove dozorSpotList and dozorSpotFile if present
    for position in meshPositions:
        if "dozorSpotList" in position:
            del position["dozorSpotList"]
        if "dozorSpotFile" in position:
            del position["dozorSpotFile"]

    meshPositionFile = path.createNewJsonFile(
        workflow_working_dir, meshPositions, "meshPositions2D"
    )
    logger.debug("execute_mesh_startDataCollection.py finished")

    return {
        "sampleInfo": sampleInfo,
        "list_node_id": listNodeId,
        "meshPositionFile": meshPositionFile,
        "meshPositionsAcquired": meshPositionsAcquired,
        "dataCollectionGroupComment": dataCollectionGroupComment,
        "resultMeshPath": resultMeshPath,
        "dataCollectionId": dataCollectionId,
        "firstImagePath": firstImagePath,
        "dozorAllFile": dozorAllFile,
        "message_text": message_text,
        "synchronizeBestPosition": synchronizeBestPosition,
    }


def startProcessing(inData, logger, meshPositions, useFastMesh):
    gap_x, gap_y = grid.determineMeshGaps(inData["grid_info"])
    doMeshIndexing = inData.get("doMeshIndexing", False)
    doDistlSignalStrength = inData.get("doDistlSignalStrength", False)
    edPlugin = edna2_tasks.storeImageQualityIndicatorsFromMeshPositionsStart(
        inData["beamline"],
        inData["directory"],
        meshPositions,
        inData["workflowParameters"]["workingDir"],
        grid_info=inData["grid_info"],
        doIndexing=doMeshIndexing,
        gap_x=gap_x,
        doDistlSignalStrength=doDistlSignalStrength,
        doUploadToIspyb=False,
        useFastMesh=useFastMesh,
    )
    # Sleep 2 s in order to let thread start
    time.sleep(2)
    return edPlugin


def executeMesh(inData, logger, useFastMesh, meshQueueList, groupNodeId, sampleNodeId):
    grid_info = inData["grid_info"]
    force_not_shutterless = inData.get("force_not_shutterless", False)
    if force_not_shutterless:
        logger.info("Forcing non-shutterless data collection")
    else:
        logger.debug("Non-shutterless data collection not forced")
    if (
        useFastMesh
        and grid_info["steps_x"] != 1
        and grid_info["steps_y"] != 1
        and not force_not_shutterless
    ):
        # Fast mesh!
        listNodeId, listImagePath, groupNodeId, dataCollectionGroupId = executeFastMesh(
            inData, logger, meshQueueList, groupNodeId, sampleNodeId
        )
    else:
        # Normal mesh
        (
            listNodeId,
            listImagePath,
            groupNodeId,
            dataCollectionGroupId,
        ) = executeNormalMesh(inData, logger, meshQueueList, groupNodeId, sampleNodeId)
    return listNodeId, listImagePath, groupNodeId, dataCollectionGroupId


def executeFastMesh(inData, logger, meshQueueList, groupNodeId, sampleNodeId):
    listNodeId = []
    listImagePath = []
    beamline = inData["beamline"]
    grid_info = inData["grid_info"]
    suffix = inData.get("suffix")

    if inData.get("besParameters"):
        bes_request_id = inData["besParameters"]["request_id"]
    else:
        bes_request_id = None

    workflowParameters = inData["workflowParameters"]
    gridId = grid_info.get("id")
    detector_roi_mode = inData.get("detector_roi_mode")
    highResolution = inData.get("highResolution")
    lowResolution = inData.get("lowResolution")
    characterisation_id = inData.get("characterisation_id")
    kappa_settings_id = inData.get("kappa_settings_id")
    position_id = inData.get("position_id")
    sample_name = inData.get("sample_name")
    firstQueueEntry = meshQueueList[0]
    lastQueueEntry = meshQueueList[-1]
    if beamline in ["id23eh2"]:
        noLines = grid_info["steps_x"]
    else:
        noLines = grid_info["steps_y"]
    meshRange = {
        "horizontal_range": grid_info["dx_mm"],
        "vertical_range": grid_info["dy_mm"],
    }
    meshQueueEntry = firstQueueEntry
    meshQueueEntry["motorPositions4dscan"]["sampx_e"] = lastQueueEntry[
        "motorPositions4dscan"
    ]["sampx_e"]
    meshQueueEntry["motorPositions4dscan"]["sampy_e"] = lastQueueEntry[
        "motorPositions4dscan"
    ]["sampy_e"]
    # meshQueueEntry["motorPositions4dscan"]["phiy_e"] = lastQueueEntry[
    #     "motorPositions4dscan"
    # ]["phiy_e"]
    # meshQueueEntry["motorPositions4dscan"]["phiz_e"] = lastQueueEntry[
    #     "motorPositions4dscan"
    # ]["phiz_e"]
    # logger.debug("Fast-mesh queue entry:")
    # logger.debug(pprint.pformat(meshQueueEntry))
    resolution = inData.get("resolution", inData["beamParameters"]["resolution"])
    dictId = collect.executeQueueForFastMeshScan(
        beamline,
        workflowParameters["index"],
        workflowParameters["type"],
        [meshQueueEntry],
        groupNodeId,
        sampleNodeId,
        resolution,
        inData["motorPositions"],
        noLines,
        meshRange,
        token=inData["token"],
        gridId=gridId,
        detector_roi_mode=detector_roi_mode,
        highResolution=highResolution,
        lowResolution=lowResolution,
        workflow_name=workflowParameters["title"],
        bes_request_id=bes_request_id,
        characterisation_id=characterisation_id,
        kappa_settings_id=kappa_settings_id,
        position_id=position_id,
        sample_name=sample_name,
    )
    groupNodeId = dictId["group_node_id"]
    listNodeId += dictId["listNodeId"]
    for queueEntry in meshQueueList:
        listImagePath += path.getQueueEntryImagePathList(queueEntry, suffix)
    dataCollectionGroupId = edna_ispyb.updateGroupWorkflowId(
        inData, listImagePath, logger
    )
    return listNodeId, listImagePath, groupNodeId, dataCollectionGroupId


def executeNormalMesh(inData, logger, meshQueueList, groupNodeId, sampleNodeId):
    listNodeId = []
    listImagePath = []
    dataCollectionGroupId = None
    beamline = inData["beamline"]
    besParameters = inData.get("besParameters")
    if besParameters:
        bes_request_id = besParameters["request_id"]
    else:
        bes_request_id = None
    grid_info = inData["grid_info"]
    gridId = grid_info.get("id")
    suffix = inData["suffix"]
    workflowParameters = inData["workflowParameters"]
    force_not_shutterless = inData.get("force_not_shutterless", False)
    highResolution = inData.get("highResolution")
    lowResolution = inData.get("lowResolution")
    resolution = inData.get("resolution", inData["beamParameters"]["resolution"])
    characterisation_id = inData.get("characterisation_id")
    kappa_settings_id = inData.get("kappa_settings_id")
    position_id = inData.get("position_id")
    sample_name = inData.get("sample_name")
    for queueEntry in meshQueueList:
        statusBES = common.getBESWorkflowStatus(besParameters)
        logger.debug("Workflow id: %s, status: %s", bes_request_id, statusBES)
        if statusBES != "STARTED":
            raise RuntimeError("Workflow no longer running!")

        dictId = collect.executeQueueForMeshScan(
            beamline,
            workflowParameters["index"],
            workflowParameters["type"],
            [queueEntry],
            groupNodeId,
            sampleNodeId,
            resolution,
            inData["motorPositions"],
            force_not_shutterless=force_not_shutterless,
            token=inData["token"],
            gridId=gridId,
            highResolution=highResolution,
            lowResolution=lowResolution,
            workflow_name=workflowParameters["title"],
            bes_request_id=bes_request_id,
            characterisation_id=characterisation_id,
            kappa_settings_id=kappa_settings_id,
            position_id=position_id,
            sample_name=sample_name,
        )
        groupNodeId = dictId["group_node_id"]
        listNodeId += dictId["listNodeId"]
        listImagePath += path.getQueueEntryImagePathList(queueEntry, suffix)

        if dataCollectionGroupId is None:
            dataCollectionGroupId = edna_ispyb.updateGroupWorkflowId(
                inData, listImagePath, logger
            )
    return listNodeId, listImagePath, groupNodeId, dataCollectionGroupId


def waitForImages(inData, logger, listImagePath, useFastMesh):
    beamline = inData["beamline"]
    workflowParameters = inData["workflowParameters"]
    workflow_working_dir = workflowParameters["workingDir"]
    # BES-199: Check that images appear on disk. Interrupt workflow if no images appear before 5 minutes.
    startTime = time.time()
    if beamline in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
        listImagePath = path.getListH5FilePath(listImagePath, isFastMesh=useFastMesh)
    for imagePath in listImagePath:
        if not os.path.exists(imagePath):
            # Wait for image
            continueToWait = True
            while continueToWait:
                # Force a "ls" in the image directory
                os.system("ls {0}> /dev/null".format(os.path.dirname(str(imagePath))))
                time.sleep(1)
                elapsedTime = time.time() - startTime
                if int(elapsedTime) % 5 == 0:
                    logger.warning(
                        "Waiting for image path {0} since {1} s".format(
                            imagePath, int(elapsedTime)
                        )
                    )
                if os.path.exists(imagePath):
                    continueToWait = False
                    logger.info("Image path {0} exists.".format(imagePath))
                    continue
                if elapsedTime > 30:
                    # Check if this has happened for the previous sample
                    if workflowParameters["type"].startswith(
                        "MXPress"
                    ) and path.checkNoImageFlag(workflow_working_dir):
                        # Block the workflow here, send email every hour till the workflow is stopped
                        blockWorkflowAndSendEmail(inData, logger)
                    else:
                        # First time or not MXPress-type WF
                        raise RuntimeError(
                            "Waiting for more than 300 s for image path {0} - abandoning".format(
                                imagePath
                            )
                        )


def saveMeshResults(inData, logger, meshPositions, inputDozor, dozorAllFile):
    beamline = inData["beamline"]
    directory = inData["directory"]
    raw_directory = inData.get("raw_directory", directory)
    # beamline info
    beamlineInfo = {}
    beamlineInfo["detectorPixelSize"] = config.get_value(
        beamline, "Detector", "pixelSize"
    )
    beamlineApertures = config.get_value(beamline, "Beam", "apertureSizes")
    beamlineInfo["beamlineApertures"] = list(map(str, beamlineApertures))
    resultMeshPath = grid.saveMeshResults(
        directory=directory,
        raw_directory=raw_directory,
        grid_info=inData["grid_info"],
        meshPositions=meshPositions,
        meshPhiPosition=inData["motorPositions"]["phi"],
        pyarch_html_dir=inData["pyarch_html_dir"],
        diffractionSignalDetection=inData["diffractionSignalDetection"],
        inputDozor=inputDozor,
        beamlineInfo=beamlineInfo,
        dozorAllFile=dozorAllFile,
    )
    logger.debug("Mesh result file: {0}".format(resultMeshPath))


def blockWorkflowAndSendEmail(inData, logger):
    beamline = inData["beamline"]
    besParameters = inData.get("besParameters")
    workflow_parameters = inData["workflowParameters"]
    token = inData.get("token")
    do_continue = True
    start_time = time.time()
    while do_continue:
        # Check that we still can contact MXCuBE, i.e. that no new workflow
        # has been started
        _ = collect.readMotorPositions(beamline, token=token)
        # Then check that we are not running this more than 8 h
        current_time = time.time()
        if current_time - start_time > 8 * 60 * 60:
            do_continue = False
        elif besParameters is not None:
            status_bes = common.getBESWorkflowStatus(besParameters)
            if status_bes != "STARTED":
                do_continue = False
        if do_continue:
            error_message = "No images written from detector for two samples in a row! Workflow halted."
            logger.error(error_message)
            email_list = config.get_value(
                beamline,
                "Email",
                "workflowHalted",
                default_value=[],
            )
            common.send_email(
                beamline,
                email_list,
                error_message,
                inData["directory"],
                workflow_parameters["title"],
                workflow_parameters["type"],
                workflow_parameters["workingDir"],
            )
            time.sleep(3600)


def updateDataCollectionGroupComment(inData, firstImagePath):
    dataCollectionGroupComment = inData.get("dataCollectionGroupComment", "")
    # Update data collection group comment if necessary
    if dataCollectionGroupComment != "":
        ispyb.updateDataCollectionGroupComment(
            inData["beamline"], firstImagePath, dataCollectionGroupComment
        )
        dataCollectionGroupComment = ""
    return dataCollectionGroupComment


def findDataCollectionId(beamline, firstImagePath):
    # Retrieve the data collection id for the first image
    dataCollectionId = None
    dataCollection = ispyb.findDataCollectionFromFileLocationAndFileName(
        beamline, firstImagePath
    )
    if dataCollection is not None:
        dataCollectionId = dataCollection.dataCollectionId
    return dataCollectionId
