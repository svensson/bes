def run(beamline, expTypePrefix="mesh-", **_):

    workflowParameters = {}
    workflowParameters["title"] = "MXPressL: 180 degree data collect on %s" % beamline
    workflowParameters["type"] = "MXPressL"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": expTypePrefix,
        "resolution": 2.0,
        "meshZigZag": False,
        "do_data_collect": True,
        "anomalousData": False,
        "doCharacterisation": False,
        "mxpressoNoImages": 900,
        "mxpressoOscRange": 0.2,
        "automatic_mode": True,
        "doAutoMesh": False,
    }
