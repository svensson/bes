def run(**_):

    return {
        "expTypePrefix": "ref-",
        "do_data_collect": True,
        "doRefDataCollectionReview": False,
        "automatic_mode": True,
        "no_reference_images": 4,
        "anomalousData": True,
    }
