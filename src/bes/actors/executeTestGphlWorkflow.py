from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

EPSILON = 0.1


def run(
    beamline,
    workflowParameters,
    prefix,
    suffix,
    directory,
    raw_directory,
    motorPositions,
    workflow_type,
    sampleInfo,
    workflow_node_id,
    resolution=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    message_text = ""

    # Test of GPhL workflow
    sample_node_id = sampleInfo["nodeId"]
    workflow_index = workflowParameters["index"]
    logger.info("#" * 80)
    logger.info("#" * 80)
    logger.info("#" * 80)

    spot_xds_path = "/data/visitor/mx2357/id30a1/20220603/PROCESSED_DATA/test/test-GphL/MXPressA_02/Workflow_20220603-134842/ControlIndexing_local_user_1_01/XDSIndexing_clmbfoeg"
    logger.info(spot_xds_path)

    task_dict = {
        "wfpath": "Gphl",  # Mandatory – specifies  GΦL workflow
        "automation_mode": "MASSIF1",  # Optional,  defaults to None (manual)
        "strategy_name": "Native data collection",  # (str) Mandatory.
        # Options are: ‘Native data collection’, ‘Phasing (SAD)’
        # ‘Two-wavelength MAD’, ‘Three-wavelength MAD’, ‘Diffractometer calibration’
        "point_group": "4",  # (str) Optional. Guides indexing solution. Overridden by space group
        # One of ‘1’, ‘2’, ‘3’, ‘4’, ‘6’, ‘222’, ‘32’. ‘312’, ‘422’, ‘622’, ‘23’, ‘432’
        # "crystal_system": "t",  # (str) Optional. Guides indexing solution. Overridden by space group
        # One of ‘a’, ‘m’, ‘o’, ‘t’, ‘h’, ‘c’
        # directory and file names. All optional, default to session default
        # See File Locations in manual
        "prefix": prefix,  # (str)
        "suffix": suffix,  # (str) Optional
        "subdir": raw_directory,  # (str) Optional
        # Optional, overriding configured defaults:
        "decay_limit": 25,  # (%).
        # Minimum % intensity remaining at target resolution if using recommended dose budget
        # dose_budget = 2 * resolution**2 * log(100. / decay_limit) / relative_sensitivity
        "maximum_dose_budget": 20,  # (MGy). Maximum when calculating budget from resolution
        "characterisation_budget_fraction": 0.05,  # Dose to use for characterisation
        # Relevant for MASSIF1
        "characterisation_strategy": "Char_4_by_10",
        # Strategy name, matching strategylib.nml.
        "auto_acq_parameters": [
            {
                # (list of dict) automation parameters for acquisitions. Optional.
                # The standard workflows have two acquisitions, characterisation and main;
                # diffractometer calibration has only one, and future workflows may have more.
                # The dictionaries are applied to the acquisitions in order;
                # if there is only one dictionary in the list, it applies to all acquisitions.
                # The following appear in task dictionaries in interactive MXCuBE3, but are NOT used by the  GΦL workflow:
                # wfname, wftype, strategies, type
                # The first dictionary are the parameters needed for MASSIF1 characterisation
                # where characterisation and first XDS run are done BEFORE starting the workflow.
                # init_spot_dir can be set ONLY for characterisation
                # init_spot_dir, if set, turns off the workflow-driven characterisation
                # resolution, energies, and transmission are taken from the current values in this case
                # unless explicitly set
                "exposure_time": 0.04,  # (s) Optional, default configured. Mandatory if init_spot_dir is set
                "image_width": 0.1,  # (°) Optional, default configured. Mandatory if init_spot_dir is set
                "init_spot_dir": spot_xds_path,  # Directory containing SPOTS.XDS file from preliminary XDS calculation
                # acquisition parameters, for successive acquisition steps (characterisation or main).
                # This example lists all options
            },
            {
                "resolution": 1.84,  # (Å) Optional. Defaults to current value, or to diffraction plan aimed_resolution
                "energies": (
                    12.842,
                ),  # tuple(keV) List of energies  to use in experiment. two or three for MAD (only).
                # First wavelength will be used for calculating strategies, dose budget, etc.
                # Optional, except for MAD - will defaults to current beamline energy.
                "exposure_time": 0.04,  # (s) Optional, default configured. Mandatory if init_spot_dir si set
                "image_width": 1.0,  # (°) Optional, default configured. Mandatory if init_spot_dir is set
                "wedge_width": 15.0,  # (°)  Optional, default configured. Wedge  width (for interleaving)
                "snapshot_count": 2,  # Optional, default configured.
                # "dose_budget": 20.0,  # (MGy) NB Optional. Overrides transmission and recommended dose budget,
                # and resets transmission and maybe exposure time
                # "transmission": 80.0,  # (%) NB Optional  Defaults to current valuee if init_spot_dir is set
                # Otherwise is set from dose budget and resolution.
                # If set this value will reset the dose budget
                "strategy_options": {  # (dict) parameters strategy calculation (stratcal). See below
                    # strategy options. Control the stratcal strategy calculation program.
                    "variant": "minimal",  # Optional. Strategy variant, “full”, or “quick”.
                    # Default is “quick” for MAD, “full” for native or SAD
                    "maximum_chi": 48.0,  # (°) Optional. Maximum chi value allowed for strategies. Default 48
                    "angular_tolerance": 1.0,  # (°) Optional. Tolerance to distinguish strategy orientations
                },
            },
        ],
    }
    group_node_id = collect.runGphlWorkflow(
        beamline,
        task_dict,
        workflow_type,
        workflow_index,
        int(workflow_node_id),
        sample_node_id,
        message="GPhL workflow",
        token=token,
    )
    logger.info("#" * 80)
    logger.info("#" * 80)
    logger.info("#" * 80)

    return {
        "phi": 0.0,
        "flagPhiMoved": True,
        "message_text": message_text,
        "group_node_id": group_node_id,
    }
