def run(beamline, **_):

    return {
        "workflow_title": "Test Best 3D characterisation on %s" % beamline,
        "workflow_type": "Undefined",
        "doRefDataCollectionReview": False,
        "no_reference_images": 2,
        "createNewDataCollectionGroup": True,
    }
