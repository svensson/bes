import time
from bes.workflow_lib import collect
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging
from bes.workflow_lib import kappa_reorientation


def run(
    beamline,
    workflowParameters,
    directory,
    newKappa,
    newKappa_phi,
    newPhi,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        newKappa = float(newKappa)
        newKappa_phi = float(newKappa_phi)
        newPhi = float(newPhi)

        motorPositions = collect.readMotorPositions(beamline, token=token)
        sampx = motorPositions["sampx"]
        sampy = motorPositions["sampy"]
        phiy = motorPositions["phiy"]

        new_sample_pos = {"sampx": sampx, "sampy": sampy, "phiy": phiy}
        if beamline in ["id30b"]:
            new_sample_pos = kappa_reorientation.getNewSamplePosition(
                beamline, 0.0, 0.0, sampx, sampy, phiy, newKappa, newKappa_phi
            )

        logger.info(
            "New sample position: sampx=%.3f mm, sampy=%.3f mm and phiy=%.3f mm"
            % (new_sample_pos["sampx"], new_sample_pos["sampy"], new_sample_pos["phiy"])
        )

        newSampx = new_sample_pos["sampx"]
        newSampy = new_sample_pos["sampy"]
        newPhiy = new_sample_pos["phiy"]

        if newSampx is None:
            logger.info("Setting new kappa angles.")
            collect.moveMotors(
                beamline,
                directory,
                {"kappa": newKappa, "kappa_phi": newKappa_phi, "phi": newPhi},
                token=token,
            )
        else:
            logger.info("Setting new kappa angles and sample position.")
            collect.moveMotors(
                beamline,
                directory,
                {
                    "kappa": newKappa,
                    "kappa_phi": newKappa_phi,
                    "phi": newPhi,
                    "sampx": newSampx,
                    "sampy": newSampy,
                    "phiy": newPhiy,
                },
                token=token,
            )
        time.sleep(1)

        motorPositions = collect.readMotorPositions(beamline, token=token)

        logger.debug("Motor positions: %r" % motorPositions)
        phi = motorPositions["phi"]
        kappa = motorPositions["kappa"]
        kappa_phi = motorPositions["kappa_phi"]

        logger.info(
            "Kappa goiniostat after move: phi=%.3f kappa=%.3f kappa_phi=%.3f"
            % (phi, kappa, kappa_phi)
        )
        epsilon = 0.1
        if not beamline == "simulator":
            if (
                abs(kappa - newKappa) > epsilon
                or abs(kappa_phi - newKappa_phi) > epsilon
            ):
                raise Exception("Kappa goniostat didn't move to new angles!")
            logger.info("Kappa goniostat moved to new angles.")
        else:
            logger.info("Simulator: kappa goniostat didn't move to new angles")

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "motorPositions": motorPositions,
    }
