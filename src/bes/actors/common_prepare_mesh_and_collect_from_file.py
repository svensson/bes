def run(beamline, expTypePrefix="mesh-", **_):
    return {
        "workflow_title": "Mesh and collect from fileon %s" % beamline,
        "workflow_type": "MeshAndCollectFromFile",
        "expTypePrefix": expTypePrefix,
        "data_threshold": 100000.0,
        "redo_find_positions": False,
        "data_collection_exposure_time": 0.037,
        "data_collection_osc_range": 10.0,
        "data_collection_no_images": 100,
        "data_collection_max_positions": 200,
        "data_collection_max_positions_orig": 200,
        "multi_wedge": True,
        "radius": 3.0,
        "aimedIOverSigmaAtHighestResolution": 1.0,
        "doMeshIndexing": True,
        "plateMode": True,
    }
