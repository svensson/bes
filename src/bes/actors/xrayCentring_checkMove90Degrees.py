from bes.workflow_lib import workflow_logging, common

EPSILON = 2.5


def run(beamline, workflowParameters, phi, newPhi, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        logger.info("Omega position after move: phi=%.3f" % phi)
        if not beamline == "simulator":
            deltaPhi = abs(phi - newPhi)
            while deltaPhi > (360.0 - EPSILON):
                deltaPhi -= 360.0
            logger.debug("deltaPhi=%.3f" % deltaPhi)
            if deltaPhi > EPSILON:
                raise Exception("Sample didn't move to new omega position!")
            logger.info("Sample moved to the new omega position")
        else:
            logger.info("Simulator: sample omega not moved to the new position")
    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
