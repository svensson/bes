from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    automatic_mode=False,
    collection_software=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    listDialog = [
        {
            "variableName": "kappaStrategyOption",
            "label": "Kappa strategy option",
            "type": "combo",
            "defaultValue": "Cell",
            "textChoices": [
                "Cell",
                "Anomalous",
                "SmartSpotSeparation",
                "SmallestOverallOscillation",
            ],
        }
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
