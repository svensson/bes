from bes.workflow_lib import collect
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    workflow_working_dir,
    transmission=None,
    firstImagePath=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    newTransmission = collect.getTransmission(beamline, token=token)
    if newTransmission < 0.01:
        if transmission is not None:
            logger.warning(
                "Transmission read from MXCuBE is very low:{0} - using previous transmission: {1}".format(
                    newTransmission, transmission
                )
            )
            newTransmission = transmission
        else:
            raise RuntimeError(
                "ERROR reading transmission from MXCuBE: {0}".format(newTransmission)
            )

    if firstImagePath is not None:

        if beamline in ["id23eh1", "id23eh2", "id30a3", "id30b"]:
            firstImagePath = firstImagePath.replace(".cbf", ".h5")

        dictResult = edna_ispyb.getFluxBeamsizeFromImage(
            firstImagePath,
            workflow_working_dir,
        )

        flux = dictResult["flux"]
    else:
        flux = None

    message_text = ""

    if flux is None:
        if firstImagePath is not None:
            message_text = "No flux could be retrieved from ISPyB for these images."
        else:
            message_text = "Cannot obtain flux."
        logger.warning(message_text)
        fluxStatus = "NoFluxFromISPyB"
    elif flux < 1e8:
        message_text = (
            "The flux retrieved from ISPyB for these images is low: %.1f photons/s."
            % flux
        )
        logger.warning(message_text)
        fluxStatus = "FluxTooLow"
    else:
        fluxStatus = "Ok"
        # Correct flux for transmission
        flux = flux * 100.0 / newTransmission
        logger.info(
            "Flux corrected for transmission (%.1f %%): %g" % (newTransmission, flux)
        )

    return {
        "flux": flux,
        "fluxStatus": fluxStatus,
        "message_text": message_text,
    }
