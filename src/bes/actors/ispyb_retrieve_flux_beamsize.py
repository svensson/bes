from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    mxv1StrategyResultFile,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    workflow_working_dir,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        run_number = int(run_number)

        mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)

        dictResult = edna_ispyb.getFluxBeamsize(
            beamline,
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            workflow_working_dir,
        )

        flux = dictResult["flux"]
        flux100 = dictResult["flux100"]
        beamSizeX = dictResult["beamSizeAtSampleX"]
        beamSizeY = dictResult["beamSizeAtSampleY"]
        message_text = ""
        if beamline == "id30a2":
            flux = 1e12
            logger.warning("Using fixed flux for simulated beamline")
            fluxStatus = "Ok"
        elif beamline == "bm14":
            if flux is None or flux < 1.0:
                flux = 1e11
            logger.warning(
                "No valid flux could be retrieved from ISPyB! Using pre-defined value for BM14: %.1g photons/s"
                % flux
            )
            fluxStatus = "Ok"
        elif flux is None:
            message_text = "No flux could be retrieved from ISPyB for these images. Characterisation aborted."
            logger.error(message_text)
            fluxStatus = "NoFluxFromISPyB"
        elif flux < 1e8:
            message_text = (
                "The flux retrieved from ISPyB for these images is too low: %.1f photons/s. Characterisation aborted."
                % flux
            )
            logger.error(message_text)
            fluxStatus = "FluxTooLow"
        else:
            fluxStatus = "Ok"

        if beamSizeX is None or beamSizeX < 0 or beamSizeY is None or beamSizeY < 0:
            logger.warning("Beam size X returned from ISPyB is {0}".format(beamSizeX))
            logger.warning("Beam size Y returned from ISPyB is {0}".format(beamSizeY))

            apertureSize = collect.getApertureSize(beamline, token)
            if apertureSize is None:
                if beamline == "id23eh2":
                    apertureSize = 5
                else:
                    apertureSize = 10

            beamSizeX = apertureSize / 1000
            beamSizeY = apertureSize / 1000
            logger.warning(
                "Setting the beam size X and Y to the aperture size %d um", apertureSize
            )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "flux": flux,
        "flux100": flux100,
        "beamSizeX": beamSizeX,
        "beamSizeY": beamSizeY,
        "fluxStatus": fluxStatus,
        "message_text": message_text,
    }
