from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, bestPosition=None, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        newMotorPositions = {}
        if bestPosition is not None:
            dictBestPosition = common.jsonLoads(bestPosition)
            newMotorPositions["sampx"] = round(dictBestPosition["sampx"], 3)
            newMotorPositions["sampy"] = round(dictBestPosition["sampy"], 3)
            newMotorPositions["phiy"] = round(dictBestPosition["phiy"], 3)
            newMotorPositions["phiz"] = round(dictBestPosition["phiz"], 3)
            position_string = "Best position:"
            for motorName in newMotorPositions:
                position_string += " %s=%.3f" % (
                    motorName,
                    newMotorPositions[motorName],
                )
            logger.info(position_string)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "newMotorPositions": newMotorPositions,
        "titleMeshHtmlPage": "",
        "meshHtmlDirectoryName": "",
    }
