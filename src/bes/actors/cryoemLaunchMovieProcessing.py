"""
Initialisation of a workflow
"""

import os
import json
import time
import pprint

from bes.workflow_lib import cryoem
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    processDirectory,
    workflowParameters,
    currentMovie,
    currentGridSquare,
    gridSquaresJsonFile,
    proteinAcronym,
    sampleAcronym,
    doseInitial,
    firstMoviesCreated,
    dosePerFrame,
    cwd,
    timeout_sec=600,
    REFID=None,
    REQUESTID=None,
    **_,
):

    workflowParameters = common.jsonLoads(workflowParameters)
    firstMoviesCreated = common.jsonLoads(firstMoviesCreated)
    logger = workflow_logging.getLogger(None, workflowParameters)
    logger.info("*********** cryoemLaunchMovieProcessing.py **********")

    dictGridSquares = json.loads(open(gridSquaresJsonFile).read())
    logger.debug(
        "cryoemLaunchMovieProcessing dictGridSquares before {0}".format(
            pprint.pformat(dictGridSquares)
        )
    )
    dictGridSquare = dictGridSquares[currentGridSquare]
    movies = dictGridSquare["movies"]
    listRequestId = []
    for movie in firstMoviesCreated:
        dictMovie = movies[movie]
        dictMovie["startTime"] = time.ctime(time.time())
        dictMovie["status"] = "started"
        logger.info("Processing move: {0}".format(dictMovie["path"]))
        bes_request_id = cryoem.startCryoEMProcessWF(
            beamline,
            os.path.dirname(dictMovie["path"]),
            os.path.basename(dictMovie["path"]),
            proteinAcronym,
            sampleAcronym,
            doseInitial,
            dosePerFrame,
            processDirectory,
            timeout_sec=600,
            workflow_name="CryoEMProcessMovie",
        )
        listRequestId.append(bes_request_id)
    for bes_request_id in listRequestId:
        cryoem.waitTillWFEnd(bes_request_id)
    for movie in firstMoviesCreated:
        dictMovie = movies[movie]
        dictMovie["status"] = "finished"
        dictMovie["endTime"] = time.ctime(time.time())

    logger.debug(
        "cryoemLaunchMovieProcessing dictGridSquares after {0}".format(
            pprint.pformat(dictGridSquares)
        )
    )

    open(gridSquaresJsonFile, "w").write(json.dumps(dictGridSquares, indent=4))

    return {}
