#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

import time

from bes.workflow_lib import collect
from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline="unknown",
    directory=None,
    workflowParameters=None,
    errorMessage=None,
    message_text=None,
    automatic_mode=False,
    collection_software=None,
    token=None,
    **_,
):

    logger = None
    if workflowParameters is None:
        # Logs have not been set up!
        # Try to set them up for error reporting
        logger = workflow_logging.getLogger(beamline, {}, token=token)
    else:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    new_message_text = (
        "An unrecoverable error happened during the workflow execution.\n"
    )
    if message_text is None:
        if errorMessage is not None:
            new_message_text += "Error message:\n"
            new_message_text += errorMessage + "\n"
    else:
        for message_line in message_text.split("\n"):
            if (
                "error" in message_line.lower()
                and message_line not in new_message_text
                and "raise" not in message_line
            ):
                new_message_text += message_line + "\n"
    new_message_text += "Please see the MXCuBE log for more details."
    dialog.displayMessage(
        beamline,
        "Exception caught during execution of workflow",
        "error",
        new_message_text,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    # # Prevent MASSIF 1 from unloading the sample for 1 hour if the error contains LIMS
    # if beamline in ["id30a1", "id30a2"] and "LIMS" in message_text:
    #     if workflowParameters != {}:
    #         logger = workflow_logging.getLogger(
    #             beamline, workflowParameters, token=token
    #         )
    #         logger.warning("Sleeping for 1 hour")
    #     emailList = config.get_value(
    #         beamline, "Email", "sleepingForOneHour", default_value=[]
    #     )
    #     common.send_email(
    #         beamline,
    #         emailList,
    #         "WARNING: sleeping for 1 hour",
    #         directory,
    #         workflow_title,
    #         workflow_type,
    #         workflow_working_dir,
    #         "WARNING: sleeping for 1 hour because of:\n\n" + message_text,
    #     )
    #     time.sleep(3600)

    time.sleep(1)
    logger.debug("Ending workflow in MXCuBE")
    collect.workflowEnd(beamline, token=token)
    logger.debug("Workflow ended in MXCuBE")

    return {"new_message_text": new_message_text}
