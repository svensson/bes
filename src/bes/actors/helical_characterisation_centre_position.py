import math
from bes.workflow_lib import workflow_logging, common


def run(
    beamline,
    workflowParameters,
    sampx1,
    sampy1,
    phiy1,
    phiz1,
    sampx2,
    sampy2,
    phiy2,
    phiz2,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        sampx1 = float(sampx1)
        sampy1 = float(sampy1)
        phiy1 = float(phiy1)
        phiz1 = float(phiz1)
        sampx2 = float(sampx2)
        sampy2 = float(sampy2)
        phiy2 = float(phiy2)
        phiz2 = float(phiz2)

        newSampx = (sampx1 + sampx2) / 2.0
        newSampy = (sampy1 + sampy2) / 2.0

        if beamline == "id23eh2":
            newPhiz = (phiz1 + phiz2) / 2.0
            helicalDistance = math.fabs(phiz2 - phiz1)
            # Check that the two sample positions are different
            if (
                abs(sampx1 - sampy1) < 0.01
                and abs(sampx2 - sampy2) < 0.01
                and abs(phiz1 - phiz2) < 0.01
            ):
                centred_status = "other"
                message_text = "Sample positions too close"
                logger.error("Sample positions too close!")
                logger.error(
                    "Please restart the workflow and move the crystal to each centred position."
                )
            else:
                centred_status = "ok"
                message_text = ""
                logger.info(
                    "Centre position: sampx=%.3f, sampy=%.3f, phiz=%.3f"
                    % (newSampx, newSampy, newPhiz)
                )
                logger.info("Helical distance: %.3f mm" % helicalDistance)

            newMotorPositions = {"sampx": newSampx, "sampy": newSampy, "phiz": newPhiz}
        else:
            newPhiy = (phiy1 + phiy2) / 2.0
            helicalDistance = math.fabs(phiy2 - phiy1)
            # Check that the two sample positions are different
            if (
                abs(sampx1 - sampy1) < 0.01
                and abs(sampx2 - sampy2) < 0.01
                and abs(phiy1 - phiy2) < 0.01
            ):
                centred_status = "other"
                message_text = "Sample positions too close"
                logger.error("Sample positions too close!")
                logger.error(
                    "Please restart the workflow and move the crystal to each centred position."
                )
            else:
                centred_status = "ok"
                message_text = ""
                logger.info(
                    "Centre position: sampx=%.3f, sampy=%.3f, phiy=%.3f"
                    % (newSampx, newSampy, newPhiy)
                )
                logger.info("Helical distance: %.3f mm" % helicalDistance)

            newMotorPositions = {"sampx": newSampx, "sampy": newSampy, "phiy": newPhiy}

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "newMotorPositions": newMotorPositions,
        "helicalDistance": helicalDistance,
        "message_text": message_text,
        "centred_status": centred_status,
    }
