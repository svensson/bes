#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "01/02/2021"

from bes import config
from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    gridOscillationRange,
    gridExposureTime,
    transmission,
    plateMode,
    gridSnapShots,
    lineScanLength,
    lineScanOverSampling,
    lowResolution,
    highResolution,
    doLineScans=True,
    lineScanStep=0,
    beamParameters=None,
    collection_software=None,
    token=None,
    do2MeshScans=False,
    allowZeroDegreeMeshScan=False,
    doCharacterisation=False,
    characterisationOscillationRange=None,
    characterisationExposureTime=None,
    characterisationTransmission=None,
    automatic_mode=False,
    doDistlSignalStrength=False,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    logger.debug("In prepareMeshCheckExperimentSettings")
    gridExposureTime = float(gridExposureTime)
    gridOscillationRange = float(gridOscillationRange)
    transmission = float(transmission)

    workflowType = workflowParameters["type"]

    diffractionSignalDetection = None

    config_value = config.get_value(
        beamline,
        "Mesh",
        "diffractionSignalDetection",
        default_value="DozorM2 (macro-molecules crystal detection)",
    )

    diffractionSignalChoises = [
        "DozorM2 (macro-molecules crystal detection)",
        "Dozor (macro-molecules)",
        # "TotalIntensity (any diffraction)",
        "Spotfinder (small molecules, slow processing)",
    ]

    for choice in diffractionSignalChoises:
        first_word_choice = choice.split(" ")[0]
        first_word_config = config_value.split(" ")[0]
        logger.debug(f"{first_word_choice} {first_word_config}")
        if first_word_choice.lower() == first_word_config.lower():
            diffractionSignalDetection = choice
            break

    if diffractionSignalDetection is None:
        raise RuntimeError(
            f"Invalid config for diffractionSignalDetection: {config_value}"
        )

    listDialog = [
        {
            "variableName": "diffractionSignalDetection",
            "label": "Diffraction signal detection",
            "type": "combo",
            "textChoices": diffractionSignalChoises,
            "value": diffractionSignalDetection,
        },
    ]

    if beamParameters is not None and "resolution" in beamParameters:
        listDialog += resolutionDialog(
            beamline, beamParameters, highResolution, lowResolution
        )

    listDialog += meshDialog(
        beamline,
        gridExposureTime,
        gridOscillationRange,
        allowZeroDegreeMeshScan,
        transmission,
        doDistlSignalStrength,
    )

    if "mesh" not in workflowType.lower():
        listDialog += lineScanDialog(lineScanLength, lineScanOverSampling)

    # Angle between two meshes
    if do2MeshScans:
        listDialog += twoMeshScanDialog(
            beamline, doLineScans, lineScanLength, lineScanStep
        )

    # For X-ray Centring and Characterisation:
    if doCharacterisation:
        listDialog += characterisationDialog(
            characterisationExposureTime,
            characterisationOscillationRange,
            characterisationTransmission,
        )

    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        collection_software=collection_software,
        automaticMode=automatic_mode,
    )

    if dictValues["diffractionSignalDetection"].startswith("Spotfinder"):
        dictValues["doDistlSignalStrength"] = True

    return dictValues


def resolutionDialog(beamline, beamParameters, highResolution, lowResolution):
    if beamline == "id30a3":
        resolution = 2.0
    else:
        resolution = beamParameters["resolution"]
    listDialog = [
        {
            "variableName": "resolution",
            "label": "Resolution [A]",
            "type": "float",
            "defaultValue": resolution,
            "unit": "A",
            "lowerBound": highResolution,
            "upperBound": lowResolution,
            "useDefaults": False,
        }
    ]
    return listDialog


def meshDialog(
    beamline,
    gridExposureTime,
    gridOscillationRange,
    allowZeroDegreeMeshScan,
    transmission,
    doDistlSignalStrength,
):

    # If doDistlSignalStrength is set to True we disable default value,
    # i.e. a 'False' value from the defaults will be overridden
    distlSignalStrengthDefault = not doDistlSignalStrength
    listDialog = [
        {
            "variableName": "gridExposureTime",
            "label": "Grid exposure time",
            "type": "float",
            "defaultValue": gridExposureTime,
            "lowerBound": config.get_value(beamline, "Beam", "minGridExposureTime"),
            "upperBound": 100.0,
        },
        {
            "variableName": "gridOscillationRange",
            "label": "Grid oscillation range",
            "type": "float",
            "defaultValue": gridOscillationRange,
            "lowerBound": 0.0,
            "upperBound": 90.0,
        },
        {
            "variableName": "allowZeroDegreeMeshScan",
            "label": "Allow zero degree mesh scan",
            "type": "boolean",
            "value": allowZeroDegreeMeshScan,
        },
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "defaultValue": transmission,
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "doDistlSignalStrength",
            "label": "Use distl signal strength",
            "type": "boolean",
            "value": doDistlSignalStrength,
            "useDefaults": distlSignalStrengthDefault,
        },
        {
            "variableName": "waitForMeshResults",
            "label": "Wait for results (disable for large meshes)",
            "type": "boolean",
            "value": True,
            "useDefaults": False,
        },
    ]
    return listDialog


def characterisationDialog(
    characterisationExposureTime,
    characterisationOscillationRange,
    characterisationTransmission,
):
    listDialog = [
        {
            "variableName": "doChangeAperture",
            "label": "Force change of aperture as result of mesh scan",
            "type": "boolean",
            "value": True,
        },
        {
            "variableName": "no_reference_images",
            "label": "Number of reference images",
            "type": "int",
            "defaultValue": 2,
            "unit": "",
            "lowerBound": 0.0,
            "upperBound": 4.0,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "combo",
            "defaultValue": "90",
            "textChoices": ["30", "45", "60", "90"],
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": characterisationExposureTime,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "characterisationOscillationRange",
            "label": "Characterisation oscillation range",
            "type": "float",
            "value": characterisationOscillationRange,
            "unit": "%",
            "lowerBound": 0.1,
            "upperBound": 10.0,
        },
        {
            "variableName": "characterisationTransmission",
            "label": "Transmission for characterisation reference images",
            "type": "float",
            "value": characterisationTransmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "do_data_collect",
            "label": "Do data collection?",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "snapShots",
            "label": "Characterisation snap shots",
            "type": "boolean",
            "value": True,
        },
        {
            "variableName": "fineSlicedReferenceImages",
            "label": "Fine-sliced reference images",
            "type": "boolean",
            "value": False,
        },
    ]
    return listDialog


def twoMeshScanDialog(beamline, doLineScans, lineScanLength, lineScanStep):
    listDialog = [
        {
            "variableName": "deltaOmega",
            "label": "Delta omega between two meshes",
            "type": "float",
            "defaultValue": 90.0,
            "unit": "degrees",
            "lowerBound": 0,
            "upperBound": 360.0,
        },
        {
            "variableName": "reject_level",
            "label": "Reject level",
            "type": "int",
            "defaultValue": 40,
        },
        {
            "variableName": "doLineScans",
            "label": "Do extra line scans",
            "type": "boolean",
            "value": doLineScans,
        },
        {
            "variableName": "lineScanLength",
            "label": "Line scan length [mm]",
            "type": "float",
            "defaultValue": lineScanLength,
            "unit": "mm",
            "lowerBound": 0.01,
            "upperBound": 1.0,
        },
        {
            "variableName": "lineScanStep",
            "label": "Line scan step size in mm (0 means automatic step scan determination)",
            "type": "float",
            "defaultValue": lineScanStep,
            "unit": "mm",
            "lowerBound": 0,
            "upperBound": 1.0,
        },
        {
            "variableName": "doMeshAndCollect",
            "label": "Do 'Mesh and collect'",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "adaptiveAperture",
            "label": "Adaptive aperture",
            "type": "boolean",
            "value": True,
        },
    ]
    return listDialog


def lineScanDialog(lineScanLength, lineScanOverSampling):
    listDialog = [
        {
            "variableName": "lineScanLength",
            "label": "Line scan length",
            "type": "float",
            "defaultValue": lineScanLength,
            "unit": "mm",
            "lowerBound": 0.05,
            "upperBound": 2.0,
        },
        {
            "variableName": "lineScanOverSampling",
            "label": "Line scan over-sampling",
            "type": "int",
            "defaultValue": lineScanOverSampling,
            "unit": "",
            "lowerBound": 1,
            "upperBound": 10,
        },
    ]
    return listDialog
