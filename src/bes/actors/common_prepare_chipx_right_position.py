def run(beamline, **_):
    return {
        "workflow_title": "ChipX right position on %s" % beamline,
        "workflow_type": "ChipX_right_position",
    }
