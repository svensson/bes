from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes import config
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    collection_software,
    workflowParameters,
    transmission=100,
    no_lines=7,
    exposureTime=0.1,
    resolution=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(
        beamline, workflowParameters=workflowParameters, token=token
    )

    if resolution is None:
        resolution = collect.getResolution(beamline, token)

    aperture_names = collect.getApertureNames(beamline, token)

    listDialog = [
        {
            "variableName": "aperture",
            "label": "Aperture",
            "type": "combo",
            "value": aperture_names[0],
            "textChoices": aperture_names,
        },
        {
            "variableName": "exposureTime",
            "label": "Exposure Time",
            "type": "float",
            "value": exposureTime,
            "lowerBound": config.get_value(beamline, "Beam", "minGridExposureTime"),
            "upperBound": 1.0,
        },
        {
            "variableName": "no_sections",
            "label": "Number of sections",
            "type": "int",
            "value": 10,
            "lowerBound": 1,
            "upperBound": 100,
        },
        {
            "variableName": "start_phi",
            "label": "Start oscillation angle",
            "type": "float",
            "value": 296.0,
            "lowerBound": 275,
            "upperBound": 345,
        },
        {
            "variableName": "end_phi",
            "label": "End oscillation angle",
            "type": "float",
            "value": 316.0,
            "lowerBound": 275,
            "upperBound": 345,
        },
        {
            "variableName": "oscillationWidth",
            "label": "Oscillation width per image",
            "type": "float",
            "value": 0.1,
        },
        {
            "variableName": "no_lines",
            "label": "Number of lines",
            "type": "int",
            "value": no_lines,
            "lowerBound": 1,
            "upperBound": 20,
        },
        {
            "variableName": "resolution",
            "label": "Resolution (A)",
            "type": "float",
            "value": resolution,
        },
        {
            "variableName": "transmission",
            "label": "Transmission (%)",
            "type": "float",
            "value": transmission,
            "lowerBound": 1,
            "upperBound": 100,
        },
    ]

    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=False,
        collection_software=collection_software,
    )

    return dictValues
