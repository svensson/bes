from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(beamline, directory, prefix, token=None, **kwargs):
    _ = workflow_logging.getLogger(beamline, token=token)
    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressR_dehydration: X-centre + dehydration on %s" % beamline
    )
    workflowParameters["type"] = "MXPressR_dehydration"

    parameters = path.getDefaultParameters(directory)
    startRH = parameters.get("startRH", 91.0)
    endRH = parameters.get("endRH", 70.0)
    step = parameters.get("step", 1.0)
    delay = parameters.get("delay", 120.0)
    aimedResolution = parameters.get("aimedResolution", 2.0)
    transmission = parameters.get("transmission", 20.0)
    no_reference_images = parameters.get("no_reference_images", 1)
    angle_between_reference_images = parameters.get("angle_between_reference_images", 1)
    snapShots = parameters.get("snapShots", False)

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": aimedResolution,
        "aimedResolution": aimedResolution,
        "automatic_mode": True,
        "doAutoMesh": False,
        "prefix": prefix,
        "anomalousData": False,
        "doCharacterisation": False,
        "checkForFlux": False,
        "do_data_collect": False,
        "startRH": startRH,
        "endRH": endRH,
        "step": step,
        "delay": delay,
        "transmission": transmission,
        "no_reference_images": no_reference_images,
        "angle_between_reference_images": angle_between_reference_images,
        "snapShots": snapShots,
        "continue_dehydration": True,
        "doDataCollection": True,
        "targetApertureName": aperture_utils.aperture_size_to_name(beamline, 30),
    }
