import time

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

EPSILON = 0.01


def run(
    beamline, workflowParameters, directory, motorInitialPositions, token=None, **_
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        logger.info("Moving sample back to start position.")
        # Move sample back to original position
        motorInitialPositions = common.jsonLoads(motorInitialPositions)

        collect.moveMotors(beamline, directory, motorInitialPositions, token=token)
        time.sleep(1)

        dict_diffractometer_positions = collect.readMotorPositions(
            beamline, token=token
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return dict_diffractometer_positions
