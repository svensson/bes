from bes.workflow_lib import common


def run(beamline, directory, **kwargs):
    workflowParameters = {}
    workflowParameters["title"] = "MXPressA: X-centre, eEDNA + dc on %s" % beamline
    workflowParameters["type"] = "MXPressA"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "automatic_mode": True,
        "loopMaxWidth": 0.15,
        "loopMinWidth": 0.10,
        "runGphlWorkflow": True,
        "gphl_strategy_variant": "quick",
    }
