from bes.workflow_lib import edna_mxv1

from edna2.utils.UtilsPath import getIcatBeamline


def run(directory, run_number, beamline=None, **_):

    if beamline is None:
        beamline = edna_mxv1.extractBeamlineFromDirectory(directory)
    else:
        # ID29 -> id29
        beamline = beamline.lower()

    # ICAT beamline
    icat_beamline = getIcatBeamline(beamline)

    return {
        "beamline": beamline,
        "icat_beamline": icat_beamline,
        "run_number": int(run_number),
    }
