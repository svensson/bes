from bes.workflow_lib import dialog
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    spaceGroup,
    automatic_mode=False,
    collection_software=None,
    token=None,
    **_,
):
    try:
        _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        dictValues = {}

        dialog.displayMessage(
            beamline,
            "Incompatible strategy",
            "error",
            "The space group (bravais lattice) {} obtained from indexing is incompatible with the kappa strategy option 'Anomalous'! Please choose a new kappa strategy option.".format(
                spaceGroup
            ),
            token=token,
            automaticMode=automatic_mode,
            collection_software=collection_software,
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return dictValues
