"""
Created on Feb 9, 2015

@author: svensson
"""

import os
from bes.workflow_lib import workflow_logging, common, path, ispyb

MESSAGE_TEMPLATE = """Dear {massifBeamline} User,

Automatic data collection for protein {acronymName} has started on beamline {massifBeamline}, the first sample is {sampleName}. Data collection can be followed in ISPyB or in the new EXI interface at http://exi.esrf.fr/mx.

The ESRF will pay full transport costs for 1 dewar per allocated shift of academic peer reviewed beamtime on MASSIF-1, see http://www.esrf.fr/MXDewarReimbursement for details.

Best wishes,

The {massifBeamline} team.


If results based on diffraction data from {massifBeamline} are used in publications, please cite one or all of the following papers:

Svensson, O., Gilski, M., Nurizzo, D. & Bowler, M. W. (2018) Multi-position data collection and dynamic beam sizing: recent improvements to the automatic data-collection algorithms on MASSIF-1, Acta Cryst. D74, 433-440 https://doi.org/10.1107/S2059798318003728

Svensson, O., Monaco, S., Popov, A. N., Nurizzo, D. & Bowler, M. W. (2015). The fully automatic characterization and data collection from crystals of biological macromolecules, Acta Cryst. D71, 1757-1767, http://dx.doi.org/10.1107/S1399004715011918

Bowler, M.W., Nurizzo, D., et al. (2015). MASSIF-1: A beamline dedicated to the fully automatic characterisation and data collection from crystals of biological macromolecules, J. Synchrotron Rad. 22, 1540-1547, http://dx.doi.org/10.1107/S1600577515016604.

Bowler, M.W., Svensson, O. & Nurizzo, D. (2016): Fully automatic macromolecular crystallography: the impact of MASSIF-1 on the optimum acquisition and quality of data, Cryst. Rev. 22, 233-249, http://dx.doi.org/10.1080/0889311X.2016.1155050.

Svensson, O., Gilski, M., Nurizzo, D. & Bowler, M. W. (2019) A comparative anatomy of protein crystals: lessons from the automatic processing of 56,000 samples IUCrJ  6, 822-831 https://doi.org/10.1107/S2052252519008017

Nurizzo, D., Bowler M.W., et al. (2016) RoboDiff: combining a sample changer and goniometer for highly automated macromolecular crystallography experiments, Acta Cryst D72, 966-975, http://dx.doi.org/10.1107/S205979831601158X.

Hutin, S., Van Laer, B., Mueller-Dieckmann, C., Leonard, G., Nurizzo, D., Bowler, M. W. (2019). Fully Autonomous Characterization and Data Collection from Crystals of Biological Macromolecules. J. Vis. Exp. 145, e59032, doi:10.3791/59032 https://www.jove.com/video/59032/fully-autonomous-characterization-data-collection-from-crystals

Web: http://www.esrf.eu/MASSIF1
FAQ: http://www.esrf.eu/MASSIF_FAQ
Software: http://www.esrf.eu/MXPressE
Twitter: https://twitter.com/ID30_MASSIF1
"""


def run(
    beamline,
    workflowParameters,
    directory,
    raw_directory,
    sessionId=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    messageText = ""
    processedSampleDirectory = path.findProcessedSampleDirectory(directory)
    emailCheckFile = os.path.join(processedSampleDirectory, ".emailSent")
    listContactEmailAddress = []

    if beamline not in ["id30a1", "id30a2", "id29"]:
        messageText = (
            "No email sent to user because beamline {0} is not part of MASSIF.".format(
                beamline
            )
        )
        logger.debug(messageText)
    elif os.path.exists(emailCheckFile):
        messageText = "No email sent to user because emailCheckFile {0} exists.".format(
            emailCheckFile
        )
        logger.debug(messageText)
    else:
        try:
            # Make directory if it doesn't exist
            if not os.path.exists(processedSampleDirectory):
                os.makedirs(processedSampleDirectory, 0o755)
            # Create emailCheckFile
            fileObject = open(emailCheckFile, "w")
            fileObject.write("Email sent!")
            fileObject.close()
        except (IOError, OSError) as ex:
            logger.warning(ex)
        # Send email only if emailCheckFile was successfully created
        if not os.path.exists(emailCheckFile):
            messageText = "No email sent to user because emailCheckFile {0} couldn't be created.".format(
                emailCheckFile
            )
            logger.warning(messageText)
        else:
            acronymName = os.path.basename(processedSampleDirectory)
            if acronymName.endswith("-"):
                messageText = "Not sending email because acronym name ends with '-': '{0}'".format(
                    acronymName
                )
                logger.warning(messageText)
                return {
                    "messageText": messageText,
                    "listContactEmailAddress": listContactEmailAddress,
                }
            sampleName = path.getSampleNameFromPath(raw_directory)
            proposalCode, proposalNumber = path.getProposalNameNumber(directory)
            logger.debug(f"Looking for acronym contact persion: beamline={beamline}")
            logger.debug(
                f"Proposal code: {proposalCode} number: {proposalNumber} Acronym name: {acronymName}"
            )
            contactPersonAcronym = ispyb.findPersonByProteinAcronym(
                beamline, proposalCode, proposalNumber, acronymName
            )
            if contactPersonAcronym is not None:
                acronymEmailAddress = contactPersonAcronym.get("emailAddress")
                logger.debug(f"Acronym email address: {acronymEmailAddress}")
                if acronymEmailAddress:
                    listContactEmailAddress.append(acronymEmailAddress)
                else:
                    messageText = "Couldn't find email address for acronym contact person for proposal {0}{1}".format(
                        proposalCode, proposalNumber
                    )
                    logger.warning(messageText)
            contactPersonProposal = ispyb.findPersonByProposal(
                beamline, proposalCode, proposalNumber
            )
            if contactPersonProposal is not None:
                proposalEmailAddress = contactPersonProposal.get("emailAddress")
                logger.debug(f"Proposal email address: {proposalEmailAddress}")
                if (
                    proposalEmailAddress
                    and proposalEmailAddress not in listContactEmailAddress
                ):
                    listContactEmailAddress.append(proposalEmailAddress)
                else:
                    messageText = "Couldn't find email address for proposal contact person for proposal {0}{1}".format(
                        proposalCode, proposalNumber
                    )
                    logger.warning(messageText)
            if len(listContactEmailAddress) == 0:
                messageText = (
                    "Couldn't find contact persons for proposal {0}{1}".format(
                        proposalCode, proposalNumber
                    )
                )
                logger.warning(messageText)
            else:
                if beamline == "id30a1":
                    logger.info("Sending email to contact person.")
                    listTo = listContactEmailAddress
                    replyTo = "massif1@esrf.fr"
                    listBCC = ["massif1@esrf.fr"]
                    massifBeamline = "MASSIF1"
                else:
                    logger.info(
                        "Sending email to local developer instead of contact person"
                    )
                    listTo = ["svensson@esrf.fr"]
                    replyTo = "svensson@esrf.fr"
                    listBCC = []
                    if beamline == "id30a2":
                        massifBeamline = "MASSIF2"
                    else:
                        massifBeamline = beamline

                subject = "{proposalCode}-{proposalNumber}: Data collection for {acronymName} started on beamline {massifBeamline}".format(
                    proposalCode=proposalCode.upper(),
                    proposalNumber=proposalNumber,
                    acronymName=acronymName,
                    massifBeamline=massifBeamline,
                )
                message = MESSAGE_TEMPLATE.format(
                    massifBeamline=massifBeamline,
                    acronymName=acronymName,
                    sampleName=sampleName,
                )

                common.sendEmail(
                    beamline, listTo, subject, message, replyTo=replyTo, listBCC=listBCC
                )

    return {
        "messageText": messageText,
        "listContactEmailAddress": listContactEmailAddress,
    }
