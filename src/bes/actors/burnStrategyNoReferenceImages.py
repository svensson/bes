from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    characterisationOscillationRange,
    characterisationExposureTime,
    characterisationTransmission,
    resolution=None,
    automatic_mode=False,
    collection_software=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    characterisationExposureTime = float(characterisationExposureTime)
    characterisationOscillationRange = float(characterisationOscillationRange)
    characterisationTransmission = float(characterisationTransmission)
    dictValues = {}

    if resolution is None:
        resolution = collect.getResolution(beamline, token=token)
    else:
        resolution = float(resolution)

    if automatic_mode:
        automaticMode = True
    else:
        automaticMode = False

    listDialog = [
        {
            "variableName": "no_reference_images",
            "label": "Number of reference images",
            "type": "int",
            "defaultValue": 2,
            "unit": "",
            "lowerBound": 0.0,
            "upperBound": 4.0,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "combo",
            "defaultValue": "90",
            "textChoices": ["30", "45", "60", "90"],
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": characterisationExposureTime,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "characterisationOscillationRange",
            "label": "Characterisation oscillation range",
            "type": "float",
            "value": characterisationOscillationRange,
            "unit": "%",
            "lowerBound": 0.1,
            "upperBound": 10.0,
        },
        {
            "variableName": "characterisationTransmission",
            "label": "Characterisation transmission",
            "type": "float",
            "value": characterisationTransmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "resolution",
            "label": "Resolution",
            "type": "float",
            "defaultValue": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 7.0,
        },
        {
            "variableName": "radiation_damage_beta",
            "label": "Radiation damage beta",
            "type": "float",
            "value": 1.0,
            "lowerBound": 0.0,
            "upperBound": 1000.0,
        },
        {
            "variableName": "radiation_damage_gamma",
            "label": "Radiation damage gamma",
            "type": "float",
            "value": 0.06,
            "lowerBound": 0.0,
            "upperBound": 1000.0,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automaticMode,
        collection_software=collection_software,
    )

    return dictValues
