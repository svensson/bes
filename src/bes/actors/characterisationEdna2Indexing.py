import sys
import traceback

from bes.workflow_lib.edna2_tasks import characterisation


def run(indexingInput, workingDirectory, **_):

    indexingOutput = None
    errorMessage = None
    listTrace = None

    if indexingInput is not None:
        try:
            indexingOutput = characterisation(indexingInput, workingDirectory)
        except Exception:
            (exc_type, exc_value, exc_traceback) = sys.exc_info()
            errorMessage = "{0} {1}".format(exc_type, exc_value)
            listTrace = traceback.extract_tb(exc_traceback)

    return {
        "indexingOutput": indexingOutput,
        "errorMessage": errorMessage,
        "listTrace": listTrace,
    }
