from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    automatic_mode=False,
    resolution=None,
    collection_software=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    transmission = collect.getTransmission(beamline, token=token)

    listDialog = [
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "value": transmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "noCycles",
            "label": "Number of cycles",
            "type": "int",
            "defaultValue": 20,
            "unit": "",
            "lowerBound": 1,
            "upperBound": 1000,
        },
        {
            "variableName": "osc_range",
            "label": "Total oscillation range",
            "type": "float",
            "defaultValue": 0.2,
            "unit": "degrees",
            "lowerBound": 0.05,
            "upperBound": 10.0,
        },
        {
            "variableName": "noImagesPerCycle",
            "label": "Number of images per cycle",
            "type": "int",
            "defaultValue": 10,
            "lowerBound": 1,
            "upperBound": 100,
        },
        {
            "variableName": "tLimit",
            "label": "Time limit",
            "type": "float",
            "defaultValue": 20.0,
            "unit": "s",
            "lowerBound": 0.0,
            "upperBound": 1000.0,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
