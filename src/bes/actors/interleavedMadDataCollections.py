from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging
from bes.workflow_lib import edna_mxv1


def run(
    beamline,
    workflowParameters,
    raw_directory,
    process_directory,
    prefix,
    expTypePrefix,
    run_number,
    osc_range,
    noDegreesPerWedge,
    numberOfImages,
    motorPositions,
    collectExposureTime,
    transmission,
    energy1,
    energy2,
    energy3,
    energy4,
    workflow_type,
    workflow_index,
    sample_node_id=1,
    isInverseBeam=False,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        osc_range = float(osc_range)
        numberOfImages = int(numberOfImages)
        noDegreesPerWedge = float(noDegreesPerWedge)
        energy1 = float(energy1)
        energy2 = float(energy2)
        energy3 = float(energy3)
        energy4 = float(energy4)
        workflow_index = int(workflow_index)
        collectExposureTime = float(collectExposureTime)
        transmission = float(transmission)
        run_number = int(run_number)
        sample_node_id = int(sample_node_id)

        resolution = collect.getResolution(beamline, token=token)
        motorPositions = common.jsonLoads(motorPositions)

        numberOfWedges = int(numberOfImages * osc_range / noDegreesPerWedge)
        logger.info("Number of interleaved wedges: %d" % numberOfWedges)

        numberOfImagesPerWedge = int(noDegreesPerWedge / osc_range)
        numberOfExtraImages = numberOfImages - numberOfWedges * numberOfImagesPerWedge
        if numberOfExtraImages > numberOfImagesPerWedge / 2.0:
            logger.info(
                "One extra wedge with %d images will be added" % numberOfExtraImages
            )
        else:
            logger.info(
                "Remaining %d images will be included in last wedge"
                % numberOfExtraImages
            )

        energy1 = round(energy1, 3)
        wavelength1 = 12.398 / energy1
        resolution1 = round(resolution, 3)
        detectorDistance1 = collect.resolutionToDetectorDistance(
            beamline, resolution1, wavelength1
        )

        energy2 = round(energy2, 3)
        wavelength2 = 12.398 / energy2
        resolution2 = round(
            collect.detectorDistanceToResolution(
                beamline, detectorDistance1, wavelength2
            ),
            3,
        )

        energy3 = round(energy3, 3)
        if energy3 < 1:
            energy3 = None
        else:
            wavelength3 = 12.398 / energy3
            resolution3 = round(
                collect.detectorDistanceToResolution(
                    beamline, detectorDistance1, wavelength3
                ),
                3,
            )

        energy4 = round(energy4, 3)
        if energy4 < 1:
            energy4 = None
        else:
            wavelength4 = 12.398 / energy4
            resolution4 = round(
                collect.detectorDistanceToResolution(
                    beamline, detectorDistance1, wavelength4
                ),
                3,
            )

        transmission = round(transmission, 2)

        data_collection_no_images = int(noDegreesPerWedge / osc_range)
        dataCollectionGroupNodeId = None

        expTypePrefix = ""

        for wedgeNumber in range(1, numberOfWedges + 1):
            logger.info("Setting up data collection for peak, wedge %d" % wedgeNumber)
            firstImageNo = 1 + (wedgeNumber - 1) * data_collection_no_images
            data_collection_start_phi = round(
                motorPositions["phi"] + (wedgeNumber - 1) * noDegreesPerWedge, 3
            )
            mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
                collectExposureTime,
                transmission,
                data_collection_no_images,
                osc_range,
                data_collection_start_phi,
            )

            dictId = collect.loadQueue(
                beamline,
                raw_directory,
                process_directory,
                "%s-e1" % prefix,
                expTypePrefix,
                run_number,
                resolution1,
                None,
                mxv1StrategyResult,
                motorPositions,
                workflow_type,
                workflow_index,
                dataCollectionGroupNodeId,
                sample_node_id,
                energy=energy1,
                enabled=False,
                firstImageNo=firstImageNo,
                token=token,
            )

            if dataCollectionGroupNodeId is None:
                dataCollectionGroupNodeId = dictId["group_node_id"]

            if isInverseBeam:

                mxv1StrategyResultInverseBeam = edna_mxv1.create_n_images_data_collect(
                    collectExposureTime,
                    transmission,
                    data_collection_no_images,
                    osc_range,
                    data_collection_start_phi + 180.0,
                )

                dictId = collect.loadQueue(
                    beamline,
                    raw_directory,
                    process_directory,
                    "%s-e1" % prefix,
                    expTypePrefix,
                    run_number + 1,
                    resolution1,
                    None,
                    mxv1StrategyResultInverseBeam,
                    motorPositions,
                    workflow_type,
                    workflow_index,
                    dataCollectionGroupNodeId,
                    sample_node_id,
                    energy=energy1,
                    enabled=False,
                    firstImageNo=firstImageNo,
                    token=token,
                )

            dictId = collect.loadQueue(
                beamline,
                raw_directory,
                process_directory,
                "%s-e2" % prefix,
                expTypePrefix,
                run_number,
                resolution2,
                None,
                mxv1StrategyResult,
                motorPositions,
                workflow_type,
                workflow_index,
                dataCollectionGroupNodeId,
                sample_node_id,
                energy=energy2,
                enabled=False,
                firstImageNo=firstImageNo,
                token=token,
            )

            if isInverseBeam:

                dictId = collect.loadQueue(
                    beamline,
                    raw_directory,
                    process_directory,
                    "%s-e2" % prefix,
                    expTypePrefix,
                    run_number + 1,
                    resolution2,
                    None,
                    mxv1StrategyResultInverseBeam,
                    motorPositions,
                    workflow_type,
                    workflow_index,
                    dataCollectionGroupNodeId,
                    sample_node_id,
                    energy=energy2,
                    enabled=False,
                    firstImageNo=firstImageNo,
                    token=token,
                )

            if energy3 > 1:

                dictId = collect.loadQueue(
                    beamline,
                    raw_directory,
                    process_directory,
                    "%s-e3" % prefix,
                    expTypePrefix,
                    run_number,
                    resolution3,
                    None,
                    mxv1StrategyResult,
                    motorPositions,
                    workflow_type,
                    workflow_index,
                    dataCollectionGroupNodeId,
                    sample_node_id,
                    energy=energy3,
                    enabled=False,
                    firstImageNo=firstImageNo,
                    token=token,
                )

                if isInverseBeam:

                    dictId = collect.loadQueue(
                        beamline,
                        raw_directory,
                        process_directory,
                        "%s-e3" % prefix,
                        expTypePrefix,
                        run_number + 1,
                        resolution3,
                        None,
                        mxv1StrategyResultInverseBeam,
                        motorPositions,
                        workflow_type,
                        workflow_index,
                        dataCollectionGroupNodeId,
                        sample_node_id,
                        energy=energy3,
                        enabled=False,
                        firstImageNo=firstImageNo,
                        token=token,
                    )

            if energy4 > 1:

                dictId = collect.loadQueue(
                    beamline,
                    raw_directory,
                    process_directory,
                    "%s-e4" % prefix,
                    expTypePrefix,
                    run_number,
                    resolution4,
                    None,
                    mxv1StrategyResult,
                    motorPositions,
                    workflow_type,
                    workflow_index,
                    dataCollectionGroupNodeId,
                    sample_node_id,
                    energy=energy4,
                    enabled=False,
                    firstImageNo=firstImageNo,
                    token=token,
                )

                if isInverseBeam:

                    dictId = collect.loadQueue(
                        beamline,
                        raw_directory,
                        process_directory,
                        "%s-e4" % prefix,
                        expTypePrefix,
                        run_number + 1,
                        resolution4,
                        None,
                        mxv1StrategyResultInverseBeam,
                        motorPositions,
                        workflow_type,
                        workflow_index,
                        dataCollectionGroupNodeId,
                        sample_node_id,
                        energy=energy4,
                        enabled=False,
                        firstImageNo=firstImageNo,
                        token=token,
                    )
    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
