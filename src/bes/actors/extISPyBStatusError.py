import os

from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(workflowParameters, token=None, **_):

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(None, workflowParameters, token=token)

    stackTraceText = None
    if "stackTraceFile" in workflowParameters:
        if os.path.exists(workflowParameters["stackTraceFile"]):
            stackTraceText = open(workflowParameters["stackTraceFile"]).read()
            stackTraceText = common.interpretStacktrace(stackTraceText)
        logger.error("Exception caught during execution of workflow:")
        logger.error(stackTraceText)
    else:
        logger.error("Error during execution of workflow")

    return {"extISPyBStatus": "ERROR"}
