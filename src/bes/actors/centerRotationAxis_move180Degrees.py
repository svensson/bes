import time

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


EPSILON = 5.0


def run(
    beamline,
    workflowParameters,
    directory,
    run_number,
    newMotorPositions,
    savedMotorPositions,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    try:
        phiy_scan1 = None
        phiz_scan1 = None

        # Move the sample back to the start position
        phiy = savedMotorPositions["phiy"]
        phiz = savedMotorPositions["phiz"]
        collect.moveMotors(beamline, directory, {"phiy": phiy}, token=token)
        collect.moveMotors(beamline, directory, {"phiz": phiz}, token=token)
        time.sleep(1)

        # Save phiy or phiz position
        newMotorPositions = common.jsonLoads(newMotorPositions)
        if beamline in ["id23eh2", "id30a2"]:
            phiy_scan1 = newMotorPositions["phiy"]
        else:
            phiz_scan1 = newMotorPositions["phiz"]

        run_number = int(run_number)
        savedMotorPositions = common.jsonLoads(savedMotorPositions)

        currentPhi = savedMotorPositions["phi"]
        newPhi = currentPhi + 180
        logger.info(
            "Rotating sample omega from %.3f to %.3f degrees" % (currentPhi, newPhi)
        )
        savedMotorPositions["phi"] = newPhi

        # Move the sample back to the start position and phi + 180 degrees
        collect.moveMotors(beamline, directory, {"phi": newPhi}, token=token)
        time.sleep(1)

        motorPositions = collect.readMotorPositions(beamline, token=token)
        # phi = motorPositions["phi"]
        #
        # logger.info("Omega position after move: phi=%.3f" % phi)
        # if not beamline == "simulator":
        #     deltaPhi = abs(phi - newPhi)
        #     while deltaPhi > (360.0 - EPSILON):
        #         deltaPhi -= 360.0
        #     logger.debug("deltaPhi=%.3f" % deltaPhi)
        #     if deltaPhi > EPSILON:
        #         raise Exception("Sample didn't move to new omega position!")
        #     logger.info("Sample moved to the new omega position")
        # else:
        #     logger.info("Simulator: sample omega not moved to the new position")

        # run_number += 1
        # logger.info("Increasing run number to: %d" % run_number)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "motorPositions": motorPositions,
        "run_number": run_number,
        "phiy_scan1": phiy_scan1,
        "phiz_scan1": phiz_scan1,
    }
