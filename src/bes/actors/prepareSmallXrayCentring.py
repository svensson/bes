#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "03/05/2021"

from bes.workflow_lib import grid
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters=None,
    resolution=None,
    token=None,
    beamSizeAtSampleX=0.050,
    beamSizeAtSampleY=0.050,
    do1DXrayCentring=False,
    isHorizontalRotationAxis=True,
    do2MeshScans=False,
    lineScanStep=0,
    lineScanLength=0.250,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    if beamline == "id30a2":
        if do1DXrayCentring:
            grid_info = {
                "dx_mm": 0.25,
                "dy_mm": 0.25,
                "steps_x": 1,
                "steps_y": 10,
                "x1": 0.0,
                "y1": -0.125,
            }
        else:
            grid_info = {
                "dx_mm": 0.25,
                "dy_mm": 0.25,
                "steps_x": 4,
                "steps_y": 4,
                "x1": -0.125,
                "y1": -0.125,
            }
    else:
        grid_info = grid.getSmallXrayCentringGrid(
            beamSizeAtSampleX,
            beamSizeAtSampleY,
            do1D=do1DXrayCentring,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
            lineScanStep=lineScanStep,
            lineScanLength=lineScanLength,
        )

    if do1DXrayCentring:
        expTypePrefix = "line-"
    else:
        expTypePrefix = "mesh-"

    motorPositions = collect.readMotorPositions(beamline, token=token)

    return {
        "grid_info": grid_info,
        "shape": None,
        "expTypePrefix": expTypePrefix,
        "shortLineScan": True,
        "motorPositions": motorPositions,
    }
