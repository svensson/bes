from bes.workflow_lib import common
from bes.workflow_lib import aperture_utils


def run(beamline, directory, **kwargs):

    grid_info = common.checkInputVariable(kwargs, "grid_info", None)
    if grid_info is None:
        if beamline in ["id23eh1", "id29"]:
            grid_info = {
                "x1": 0.20,
                "y1": -0.10,
                "dx_mm": 0.80,
                "dy_mm": 0.20,
                "steps_x": 40,
                "steps_y": 8,
                "angle": 0.0,
            }
        if beamline in ["id30a1"]:
            grid_info = {
                "x1": -0.40,
                "y1": -0.10,
                "dx_mm": 0.80,
                "dy_mm": 0.20,
                "steps_x": 30,
                "steps_y": 8,
                "angle": 0.0,
            }
        elif beamline in ["id23eh2", "simulator"]:
            grid_info = {
                "x1": -0.1,
                "y1": -0.04,
                "dx_mm": 0.30,
                "dy_mm": 0.080,
                "steps_x": 50,
                "steps_y": 8,
                "angle": 0.0,
            }

    workflowParameters = {}
    workflowParameters["title"] = "MXPressA: X-centre, eEDNA + dc on %s" % beamline
    workflowParameters["type"] = "MXPressA"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "grid_info": grid_info,
        "shape": None,
        "automatic_mode": True,
        "targetApertureName": aperture_utils.aperture_size_to_name(beamline, 50),
    }
