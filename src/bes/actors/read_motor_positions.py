from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    transmission=None,
    resolution=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        if resolution is None:
            resolution = collect.getResolution(beamline, token=token)

        if transmission is None:
            transmission = collect.getTransmission(beamline, token=token)
        else:
            transmission = float(transmission)

        motorPositions = collect.readMotorPositions(beamline, token=token)
        for motorName in motorPositions:
            if motorPositions[motorName] is not None:
                try:
                    motorPositions[motorName] = float(motorPositions[motorName])
                except Exception:
                    motorPositions[motorName] = motorPositions[motorName]

        energy = collect.getEnergy(beamline, token=token)
        if energy is None:
            wavelength = None
        else:
            wavelength = 12.398 / energy
            # Round transmission to four decimals
            wavelength = round(wavelength, 4)

        beamParameters = {}
        beamParameters["energy"] = energy
        beamParameters["wavelength"] = wavelength
        beamParameters["transmission"] = transmission
        beamParameters["resolution"] = resolution

        logger.debug("Beam parameters: %r" % beamParameters)
        logger.debug("Motor positions: %r" % motorPositions)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "beamParameters": beamParameters,
        "motorPositions": motorPositions,
        "wavelength": wavelength,
    }
