from bes.workflow_lib import collect
from bes.workflow_lib import dehydration
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    targetRH,
    steppingSign,
    step,
    doDataCollection,
    run_number,
    delay,
    phi_start,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    targetRH = float(targetRH)
    step = float(step)
    steppingSign = int(steppingSign)
    run_number = int(run_number)
    delay = float(delay)
    phi_start = float(phi_start)

    targetRH = targetRH + steppingSign * step

    dehydration.set_hydration_level(beamline, targetRH, delay)
    currentRH = dehydration.read_hydration_level(beamline, targetRH)

    # if doDataCollection:
    #     run_number += 1
    doRefDataCollectionReview = False

    logger.info("Setting data collection start angle to %.3f degrees" % phi_start)
    collect.moveMotors(
        beamline, directory=directory, dictPosition={"phi": phi_start}, token=token
    )

    return {
        "run_number": run_number,
        "targetRH": targetRH,
        "currentRH": currentRH,
        "doRefDataCollectionReview": doRefDataCollectionReview,
        "phi": phi_start,
    }
