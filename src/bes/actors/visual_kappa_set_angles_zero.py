import time

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, directory, token=None, **_):

    try:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        logger.info("Setting kappa angles to zero.")
        collect.moveMotors(
            beamline, directory, {"kappa": 0.0, "kappa_phi": 0.0}, token=token
        )

        time.sleep(1)

        dict_diffractometer_positions = collect.readMotorPositions(
            beamline, token=token
        )

        logger.debug("Motor positions: %r" % dict_diffractometer_positions)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return dict_diffractometer_positions
