from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes import config
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    motorPositions,
    flux,
    mxv1ResultCharacterisationFile,
    highResolution=None,
    lowResolution=None,
    flux100=None,
    mxpressoNoImages=900,
    mxpressoOscRange=0.2,
    anomalousData=False,
    axisRange=None,
    firstImagePath=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        workflowParameters = common.jsonLoads(workflowParameters)
        mxpressoNoImages = int(mxpressoNoImages)
        mxpressoOscRange = float(mxpressoOscRange)

        mxv1ResultCharacterisation = path.readXSDataFile(mxv1ResultCharacterisationFile)
        (resolution, phi) = edna_mxv1.getResolutionAndPhistart(
            beamline, mxv1ResultCharacterisation
        )
        # Check resolution (DPC-62)
        resolution = collect.limitResolutiom(
            resolution=resolution,
            lowResolution=lowResolution,
            highResolution=highResolution,
        )

        flux = float(flux)
        if flux100 is not None:
            flux = float(flux100)

        collectExposureTime = config.get_value(
            beamline, "Beam", "defaultCollectExposureTime"
        )
        if flux < 1.7e12:
            transmission = 100
        else:
            transmission = 100 * 1.7e12 / flux
        logger.info("Transmission set to %.1f %%" % transmission)

        if axisRange is not None:
            osc_range = mxpressoOscRange
            totalOscRange = axisRange
            number_of_images = int(totalOscRange / osc_range)
        else:
            if not anomalousData:
                number_of_images = mxpressoNoImages
            else:
                # Check if total oscillation range for 2 * number of images is greater than 360:
                totalOscRange = mxpressoNoImages * mxpressoOscRange * 2
                if totalOscRange > 360.0:
                    number_of_images = mxpressoNoImages
                else:
                    number_of_images = mxpressoNoImages * 2

            osc_range = mxpressoOscRange
            totalOscRange = number_of_images * mxpressoOscRange

        mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
            collectExposureTime, transmission, number_of_images, osc_range, phi
        )
        mxv1StrategyResultFile = path.createNewXSDataFile(
            workflowParameters["workingDir"],
            mxv1StrategyResult,
            "dataCollect180degrees",
        )

        if firstImagePath is not None and not workflowParameters["type"].startswith(
            "MXPressO"
        ):
            ispyb.updateDataCollectionGroupComment(
                beamline,
                firstImagePath,
                "{0} degree data collection with resolution {1:.2f} A from characterisation.".format(
                    totalOscRange, resolution
                ),
            )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "expTypePrefix": "",
        "do_data_collect": True,
        "automatic_mode": True,
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
        "resolution": resolution,
    }
