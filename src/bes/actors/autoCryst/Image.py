"""
Created on 10-Dec-2018
Author: S. Basu
"""

import logging
import numpy as np
import os

logger = logging.getLogger("autoCryst")


class CBFreader(object):
    def __init__(self, filename):
        """

        :rtype: type(headers) -> dict
        :rtype: type(data) -> numpy array 2D
        """
        if not os.path.exists(filename):
            err = "File does not exist %s." % filename
            logger.info("IOError:{}".format(err))
            return
        self.cbf_file = filename
        self.headers = {}
        self.data = np.empty([])
        return

    def read_cbfheaders(self):
        self.headers["filename"] = self.cbf_file
        for record in open(self.cbf_file):
            if "_array_data.data" in record:
                break
            if "Pixel_size" in record:
                self.headers["pixel_size"] = float(record.split()[2])
            if "Detector_distance" in record:
                self.headers["detector_distance"] = float(record.split()[2])
            if "Wavelength" in record:
                self.headers["photon_energy"] = 12398 / float(record.split()[2])
            if "Beam_xy" in record:
                beam = map(
                    float,
                    record.replace("(", "")
                    .replace(")", "")
                    .replace(",", "")
                    .split()[2:4],
                )  # type: list
                self.headers["beam_center_x"] = beam[0]
                self.headers["beam_center_y"] = beam[1]
            if "Detector:" in record:
                self.headers["detector_name"] = record.replace(",", "").split()[2:4]
            else:
                pass
        return

    def read_cbfdata(self):
        try:
            import pycbf

            handler = pycbf.cbf_handle_struct()
            handler.read_file(self.cbf_file, pycbf.MSG_DIGEST)
            # handler.rewind_datablock()
            handler.select_datablock(0)
            handler.select_category(0)
            handler.select_column(1)
            handler.select_row(0)

            img_as_string = handler.get_integerarray_as_string()
            self.data = np.fromstring(
                img_as_string, np.int32
            )  # look for image.py in dials/util
            dimension = (
                handler.get_integerarrayparameters_wdims()[9],
                handler.get_integerarrayparameters_wdims()[10],
            )
            self.data = self.data.reshape(dimension)

        except ImportError:
            pycbf = "pycbf module not in path"
            logger.info("ImportError:{}".format(pycbf))
        return

    @staticmethod
    def write_cbf():
        try:
            import pycbf  # noqa F401
        except ImportError as err:
            logger.info("ImportError:{}".format(err))
        "Method to write a cbf image file"
        return


class EigerReader(object):
    def __init__(self, filename):
        """

        :rtype: type(headers) -> dict
        :rtype: type(data) -> numpy array 2D
        """
        if not os.path.exists(filename):
            err = "File does not exist %s" % filename
            logger.info("IOError:{}".format(err))
            return

        self.eiger_file = filename
        self.headers = {}
        self.data = np.empty([])
        try:
            import h5py

            self.eiger_handle = h5py.File(self.eiger_file, "r")
        except (ImportError, NameError) as err:
            logger.info("ImportError:{}".format(err))
        return

    def read_h5headers(self):
        if "master" not in self.eiger_file:
            err = "%s is not a master file, no header info" % self.eiger_file
            logger.info("ValueError:{}".format(err))
            return
        else:
            self.headers["filename"] = self.eiger_file
            pix_size = self.eiger_handle["/entry/instrument/detector/x_pixel_size"]
            self.headers["pixel_size"] = np.array(pix_size)
            detector_name = self.eiger_handle["/entry/instrument/detector/description"]
            detector_name = np.array(detector_name).tolist()  # type: bytes
            self.headers["detector_name"] = detector_name.decode().split()[1:3]
            detector_distance = self.eiger_handle[
                "/entry/instrument/detector/detector_distance"
            ]
            self.headers["detector_distance"] = np.array(detector_distance)
            beamx = self.eiger_handle["/entry/instrument/detector/beam_center_x"]
            self.headers["beam_center_x"] = np.array(beamx)
            beamy = self.eiger_handle["/entry/instrument/detector/beam_center_y"]
            self.headers["beam_center_y"] = np.array(beamy)
            wave = self.eiger_handle["/entry/instrument/beam/incident_wavelength"]
            self.headers["photon_energy"] = 12398 / np.array(wave)
            return

    def read_h5data(self):
        self.data = self.eiger_handle["/data/data"]
        self.data = np.array(self.data)
        return

    def write_h5(self):
        """Method to write an Eiger h5 image"""
        return


class ImageHandler(object):
    def __init__(self, filename):

        self.imagefile = filename
        self.imobject = type("", (), {})

        if not os.path.exists(self.imagefile):
            err = "File does not exist: %s" % filename
            logger.info("ImportError:{}".format(err))
            return
        else:
            if ".cbf" in self.imagefile:
                self.imobject = CBFreader(self.imagefile)
                self.imobject.read_cbfheaders()
            elif "master" in self.imagefile:
                self.imobject = EigerReader(self.imagefile)
                self.imobject.read_h5headers()
            else:
                pass
        return

    def read_datablock(self):

        if isinstance(self.imobject, CBFreader):
            self.imobject.read_cbfdata()
        elif isinstance(self.imobject, EigerReader):
            self.imobject.read_h5data()
        else:
            pass
        return

    def check_icerings(self):
        """Method for ice rings analysis"""

        return

    def create_powdersum(self):
        """Method to write a powder pattern, useful for background checking"""

        return

    def create_maskfile(self):
        """Method for writing a mask file based on powder sum"""
        return

    def radial_avg(self):
        return


if __name__ == "__main__":
    c = CBFreader("../examples/mesh-insu_2_0_1143.cbf")
    c.read_cbfdata()
    c.read_cbfheaders()
