"""
Created on: 2019.02.04
Shibom Basu
"""

from __future__ import division, print_function
import os
import re


class GeomLoader(object):
    def __init__(self, geomfile):
        self._fname = geomfile
        if os.path.exists(self._fname):
            self.fobject = open(self._fname, "r")
        else:
            print("file does not exist")
        self.geom_as_dict = {}
        self.pobj = type("", (), {})()

        self.re_dict = dict(
            photon_energy=re.compile(
                r"photon_energy\s=\s(?P<photon_energy>.[1-9\.\+])\n"
            ),
            clen=re.compile(r"clen\s=\s(?P<clen>.[1-9\.\+\-])\n"),
            coffset=re.compile(r"coffset\s=\s(?P<coffset>[1-9\.\-\+])\n"),
            adu_per_eV=re.compile(r"adu_per_eV\s=\s(?P<adu_per_eV>[1-9\.\+])\n"),
        )
        return

    def _close(self):
        self.fobject.close()
        return

    def read_geomfile(self):
        _all = self.fobject.readlines()
        self.pobj = Panel("0")
        d = {}
        for lines in _all:
            if "/" in lines and "bad" not in lines:
                line = lines.split("/")

                rest = line[1:][0].strip("\n")

                if "min_fs" in rest:
                    tmp = rest.split()
                    self.pobj.minfs = round(float(tmp[2]))
                elif "max_fs" in rest:
                    tmp = rest.split()
                    self.pobj.maxfs = int(tmp[2])
                elif "min_ss" in rest:
                    tmp = rest.split()
                    self.pobj.minss = round(float(tmp[2]))
                elif "max_ss" in rest:
                    tmp = rest.split()
                    self.pobj.maxss = int(tmp[2])
                elif "corner_x" in rest:
                    tmp = rest.split()
                    self.pobj.cornerx = abs(round(float(tmp[2])))
                elif "corner_y" in rest:
                    tmp = rest.split()
                    self.pobj.cornery = abs(round(float(tmp[2])))
                else:
                    pass
            if "bad" in lines and "/" in lines:
                line = lines.split("/")
                d[line[0]] = []
        for lines in _all:
            if "bad" in lines and "/" in lines:
                line = lines.split("/")
                rest = line[1:][0].strip("\n")
                if any(t in rest for t in ["min_fs", "max_fs", "min_ss", "max_ss"]):
                    tmp = rest.split()
                    if line[0] in d.keys():
                        d[line[0]].append(int(tmp[2]))
                    else:
                        pass

        self.geom_as_dict = self.pobj.__dict__
        self.geom_as_dict["bad_region"] = d
        self._close()
        return


class Panel(object):
    def __init__(self, pname):
        self.panel = pname
        self.maxfs = 0
        self.minfs = 0
        self.maxss = 0
        self.minss = 0
        self.cornerx = 0
        self.cornery = 0
        self.fsv = "x"
        self.ssv = "y"
        return


def main():
    from Image import CBFreader
    import matplotlib.pyplot as plt

    cbf = CBFreader("../examples/mesh-insu_2_0_1143.cbf")
    cbf.read_cbfdata()
    gg = GeomLoader("../examples/pilatus2m.geom")
    gg.read_geomfile()
    for k, v in gg.geom_as_dict["bad_region"].items():
        cbf.data[v[0] : v[1], v[2] : v[3]] = -120000.0
    plt.imshow(cbf.data, interpolation="nearest")
    plt.show()


# print(gg.geom_as_dict)
main()
