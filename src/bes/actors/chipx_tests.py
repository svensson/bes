import os
import sys
import math

from chipx.image_tools import read_grayscale_image

# from chipx.image_tools import calculate_sharpness
from chipx.image_tools import get_capillary_vertical_position

from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


EPSILON = 0.1


def run(beamline, workflowParameters, directory, token=None, **_):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    message_text = ""

    motor_positions = collect.readMotorPositions(beamline, read_focus=True, token=token)
    focus_pos = motor_positions["focus"]
    logger.info(focus_pos)

    workflow_working_dir = workflowParameters["workingDir"]
    snapshot_index = 1
    snap_shot_path = os.path.join(
        workflow_working_dir, "snapshot_{0:02d}.png".format(snapshot_index)
    )
    collect.saveSnapshot(beamline, snap_shot_path, handle_light=False, token=token)
    logger.info(snap_shot_path)
    sys.path.insert(0, "/opt/pxsoft/bes/vgit/linux-x86_64/id30b/chipx")

    numpy_image = read_grayscale_image(snap_shot_path)
    y_size, x_size = numpy_image.shape
    vertical_pixel_pos = get_capillary_vertical_position(numpy_image)
    logger.info("Vertical position: {0}".format(vertical_pixel_pos))
    if vertical_pixel_pos is not None:
        delta_y = y_size / 2 - vertical_pixel_pos
        logger.info("Delta y (in pixels): {0}".format(delta_y))

        delta_y_mm = 1.9 * delta_y / 1000
        logger.info("Delta y (in mm): {0}".format(delta_y_mm))

        sampx = motor_positions["sampx"]
        sampy = motor_positions["sampy"]

        new_sampx = float(sampx - delta_y_mm / math.sqrt(2))
        new_sampy = float(sampy - delta_y_mm / math.sqrt(2))

        dict_position = {"sampx": new_sampx, "sampy": new_sampy}
        collect.moveMotors(beamline, directory, dict_position, token=token)

    # sharpness = calculate_sharpness(numpy_image)
    # logger.info("Sharpness: {0}".format(sharpness))
    #
    # for new_focus_pos in range(focus_pos - 0.1, focus_pos + 0.1, 0.01):

    # delta = 0.05
    # direction = 1
    # stop_focussing = False
    # found_peak = False
    # first_time_change_of_direction = True
    # while not stop_focussing:
    #     snapshot_index += 1
    #     new_focus_pos = focus_pos + delta * direction
    #     dict_position = {"focus": new_focus_pos}
    #     collect.moveMotors(beamline, directory, dict_position, move_focus=True, token=token)
    #     snap_shot_path = os.path.join(workflow_working_dir, "snapshot_{0:02d}.png".format(snapshot_index))
    #     collect.saveSnapshot(beamline, snap_shot_path, handle_light=False, token=token)
    #     numpy_image = read_grayscale_image(snap_shot_path)
    #     new_sharpness = calculate_sharpness(numpy_image)
    #     logger.info("Sharpness: {0}".format(new_sharpness))
    #     if new_sharpness > sharpness:
    #         if found_peak:
    #             delta = delta / 2
    #             logger.info("New delta: {0}".format(delta))
    #             found_peak = False
    #         # else:
    #         #     logger.info("Continuing to search...")
    #     else:
    #         direction = -direction
    #         logger.info("New direction: {0}".format(direction))
    #         if not first_time_change_of_direction:
    #             first_time_change_of_direction = False
    #             found_peak = True
    #     sharpness = new_sharpness
    #     focus_pos = new_focus_pos
    #     if snapshot_index >= 8:
    #         stop_focussing = True
    #

    # logger.debug("Checking move of phi motor")
    #
    # dict_positions = collect.readMotorPositions(beamline, token=token)
    # origPhi = dict_positions["phi"]
    # logger.info("phi motor (omega axis) original position: %.3f" % origPhi)
    #
    # newPhi = origPhi + 1.0
    # # On ID29 microdiff gives always values between 0 and 359.999
    # # if beamline in ["id23eh2", "id29", "id30a3", "id30b"] and newPhi > 360.0:
    # #     newPhi -= 360.0
    # logger.info("Moving phi motor (omega axis) new position: %.3f" % newPhi)
    # collect.moveMotors(beamline, directory, {"phi": newPhi}, token=token)
    # time.sleep(1)
    #
    # dict_positions = collect.readMotorPositions(beamline, token=token)
    # currentPhi = dict_positions["phi"]
    # logger.info("phi motor (omega axis) current position: %.3f" % currentPhi)
    #
    # logger.info(
    #     "Moving phi motor (omega axis) moved back to original position: %.3f"
    #     % origPhi
    # )
    # collect.moveMotors(beamline, directory, {"phi": origPhi}, token=token)
    # time.sleep(1)
    #
    # dict_positions = collect.readMotorPositions(beamline, token)
    # phi = dict_positions["phi"]
    #
    # if (abs(currentPhi - newPhi) % 360) > EPSILON:
    #     flagPhiMoved = False
    #     message_text = "Phi motor didn't move to requested position"
    #     logger.error(message_text)
    # else:
    #     flagPhiMoved = True

    return {
        # "phi": phi,
        "flagPhiMoved": True,
        "message_text": message_text,
    }
