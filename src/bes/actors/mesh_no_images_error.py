from bes.workflow_lib import ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    message_text=None,
    firstImagePath=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    if message_text is None:
        message_text = "No processing results (no images acquired?)"
    logger.error(message_text)
    if firstImagePath is not None:
        ispyb.updateDataCollectionGroupComment(beamline, firstImagePath, message_text)

    return {
        "message_text": message_text,
    }
