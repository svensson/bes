"""
Created on Feb 17, 2015

@author: svensson
"""

import time
import os
import sys
import traceback
import imageio
import numpy
import shutil

from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1


def run(directory, token=None, **_):

    try:
        errorMessage = ""
        backgroundImageCreated = False

        beamline = edna_mxv1.extractBeamlineFromDirectory(directory)
        if beamline != "id30a1":
            raise BaseException("This workflow can only run on ID30a1!")

        dateString = time.strftime("%Y%m%d-%H%M%S", time.localtime(time.time()))
        snapShotFilePath = "/data/id30a1/inhouse/opid30a1/snapshots/background/snapshot_background_{0}.png".format(
            dateString
        )
        #         snapShotFilePath = "/tmp/test1.png"
        collect.saveSnapshot("id30a1", snapShotFilePath, token=token)
        im = imageio.v3.imread(snapShotFilePath)
        numpyFileName1 = "/data/id30a1/inhouse/opid30a1/snapshots/background/snapshot_background_{0}.npy".format(
            dateString
        )
        numpyFileName2 = (
            "/data/id30a1/inhouse/opid30a1/snapshots/background/snapshot_background.npy"
        )
        numpy.save(numpyFileName1, im)
        shutil.copyfile(numpyFileName1, numpyFileName2)
        time.sleep(1)
        backgroundImageCreated = os.path.exists(numpyFileName2)

    except Exception:

        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        errorMessage = "{0} {1}".format(exc_type, exc_value)
        listTrace = traceback.extract_tb(exc_traceback)
        for listLine in listTrace:
            errorMessage += '  File "%s", line %d, in %s%s' % (
                listLine[0],
                listLine[1],
                listLine[2],
                os.linesep,
            )

    return {
        "errorMessage": errorMessage,
        "backgroundImageCreated": backgroundImageCreated,
    }
