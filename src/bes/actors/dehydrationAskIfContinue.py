from bes.workflow_lib import dialog
from bes import config


def run(
    beamline,
    workflowParameters,
    directory,
    continue_dehydration,
    doDataCollection,
    automatic_mode,
    collection_software=None,
    transmission=None,
    token=None,
    snapShots=None,
    characterisationExposureTime=None,
    no_reference_images=None,
    angle_between_reference_images=None,
    **_,
):

    listDialog = [
        {
            "variableName": "continue_dehydration",
            "label": "Continue dehydration?",
            "type": "boolean",
            "value": continue_dehydration,
        },
        {
            "variableName": "doDataCollection",
            "label": "Do data collection?",
            "type": "boolean",
            "value": doDataCollection,
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": characterisationExposureTime,
            "unit": "s",
            "lowerBound": config.get_value(
                beamline, "Beam", "minCharacterisationExposureTime"
            ),
            "upperBound": 10000.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "value": transmission,
            "unit": "s",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "no_reference_images",
            "label": "No reference images",
            "type": "int",
            "value": no_reference_images,
            "lowerBound": 1,
            "upperBound": 4,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "float",
            "value": angle_between_reference_images,
            "unit": "degrees",
            "lowerBound": 1.0,
            "upperBound": 180.0,
        },
        {
            "variableName": "snapShots",
            "label": "Snap shots",
            "type": "boolean",
            "value": snapShots,
        },
        {
            "variableName": "automatic_mode",
            "label": "Automatic mode",
            "type": "boolean",
            "value": automatic_mode,
        },
    ]

    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
