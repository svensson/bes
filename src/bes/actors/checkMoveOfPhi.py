import time

from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

EPSILON = 0.1


def run(beamline, workflowParameters, directory, token=None, **_):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    message_text = ""

    logger.debug("Checking move of phi motor")

    dict_positions = collect.readMotorPositions(beamline, token=token)
    origPhi = dict_positions["phi"]
    logger.info("phi motor (omega axis) original position: %.3f" % origPhi)

    newPhi = origPhi + 1.0
    # On ID29 microdiff gives always values between 0 and 359.999
    # if beamline in ["id23eh2", "id29", "id30a3", "id30b"] and newPhi > 360.0:
    #     newPhi -= 360.0
    logger.info("Moving phi motor (omega axis) new position: %.3f" % newPhi)
    collect.moveMotors(beamline, directory, {"phi": newPhi}, token=token)
    time.sleep(1)

    dict_positions = collect.readMotorPositions(beamline, token=token)
    currentPhi = dict_positions["phi"]
    logger.info("phi motor (omega axis) current position: %.3f" % currentPhi)

    logger.info(
        "Moving phi motor (omega axis) moved back to original position: %.3f" % origPhi
    )
    collect.moveMotors(beamline, directory, {"phi": origPhi}, token=token)
    time.sleep(1)

    dict_positions = collect.readMotorPositions(beamline, token=token)
    phi = dict_positions["phi"]

    if (abs(currentPhi - newPhi) % 360) > EPSILON:
        flagPhiMoved = False
        message_text = "Phi motor didn't move to requested position"
        logger.error(message_text)
    else:
        flagPhiMoved = True

    return {
        "phi": phi,
        "flagPhiMoved": flagPhiMoved,
        "message_text": message_text,
    }
