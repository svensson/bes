import os

from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    prefix,
    workflowParameters,
    directory=None,
    collection_software=None,
    token=None,
    **_,
):

    dictValues = {}
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    listDialog = [
        {
            "variableName": "no_reference_images",
            "label": "Number of reference images",
            "type": "int",
            "value": 3,
            "unit": "",
            "lowerBound": 1,
            "upperBound": 4,
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": 0.1,
            "unit": "s",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "prefix",
            "label": "Prefix",
            "type": "text",
            "defaultValue": prefix,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "combo",
            "defaultValue": "90",
            "textChoices": ["30", "45", "60", "90"],
        },
        {
            "variableName": "snapShots",
            "label": "Snap shots",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "data_collection_no_images",
            "label": "No images",
            "type": "int",
            "value": 1,
            "lowerBound": 1,
            "upperBound": 10000.0,
            "useDefaults": False,
        },
        {
            "variableName": "gridSnapShot",
            "label": "Take single snapshot",
            "type": "boolean",
            "value": False,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=False,
        collection_software=collection_software,
        file_prefix="trouble_shooting_dialog",
    )

    if dictValues["gridSnapShot"]:
        snapShotFilePath = os.path.join(
            directory.replace("RAW_DATA", "PROCESSED_DATA"), "snapshot.jpg"
        )
        collect.saveSnapshot(beamline, snapShotFilePath, token=token)

    # Testdialog

    message = "A test message.\n"
    message += "Several lines.\n"
    message += "Another line."
    message += "\n\n\n\n"
    message += "Another line."
    message += "\n\n\n"
    message += "Another line."
    dialog.displayMessage(
        beamline,
        "Test of workflow dialog",
        "info",
        message,
        token=token,
        automaticMode=False,
        collection_software=collection_software,
    )

    return dictValues
