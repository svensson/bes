"""
Created on Mar 3, 2015

@author: svensson
"""

import os

# from bes.workflow_lib import ispyb

from EDFactoryPluginStatic import EDFactoryPluginStatic

EDFactoryPluginStatic.loadModule("XSDataMXCuBEv1_4")
# from XSDataMXCuBEv1_4 import XSDataInputMXCuBE


def run(inputFile, workingDirectory, beamline=None, proposal=None, logFile=None, **_):

    if beamline is None:
        directories = workingDirectory.split(os.path.sep)
        try:
            if directories[2] == "visitor":
                beamline = directories[4]
                proposal = directories[3]
            else:
                beamline = directories[2]
                proposal = directories[4]
        except Exception:
            beamline = "unknown"
            proposal = "unknown"

    dataCollectionSuccess = True
    # xsDataInputMXCuBE = XSDataInputMXCuBE.parseFile(inputFile)
    # dataCollectionId = xsDataInputMXCuBE.dataCollectionId.value
    # dataCollection = ispyb.findDataCollection(beamline, dataCollectionId)
    # if dataCollection is None:
    #     raise RuntimeError(
    #         "No data collection found in ISPyB with dataCollectionId = {0}".format(
    #             dataCollectionId
    #         )
    #     )
    # if dataCollection.runStatus == "Data collection successful":
    #     dataCollectionSuccess = True

    return {
        "beamline": beamline,
        "proposal": proposal,
        "dataCollectionSuccess": dataCollectionSuccess,
        "workflowLogFile": logFile,
        # "runStatus": dataCollection.runStatus,
    }
