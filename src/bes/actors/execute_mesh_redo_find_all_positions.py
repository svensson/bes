import os
import json
import pprint

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    meshPositionFile,
    run_number,
    expTypePrefix,
    workflow_working_dir,
    prefix,
    suffix,
    workflow_id,
    grid_info,
    workflow_title,
    workflow_type,
    firstImagePath,
    data_threshold,
    data_collection_max_positions,
    data_collection_max_positions_orig,
    timeToReachHendersonLimit,
    data_collection_no_images,
    radius,
    list_node_id=None,
    diffractionSignalDetection="DozorM2 (macro-molecules crystal detection)",
    pyarch_html_dir=None,
    resultMeshPath=None,
    token=None,
    dozormListPositions=[],
    isHorizontalRotationAxis=True,
    dozormCrystalMapPath=None,
    dozormImageNumberMapPath=None,
    **_,
):

    try:
        meshResultsJsonPath = None
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        workflowParameters = common.jsonLoads(workflowParameters)
        workflowId = workflowParameters["ispyb_id"]
        meshBestPlotPath = None
        meshBestPositions = None

        run_number = int(run_number)
        workflow_id = int(workflow_id)
        data_threshold = float(data_threshold)
        data_collection_max_positions = int(data_collection_max_positions_orig)
        timeToReachHendersonLimit = float(timeToReachHendersonLimit)
        data_collection_no_images = int(data_collection_no_images)

        meshPositions = path.parseJsonFile(meshPositionFile)

        radius = float(radius)

        if diffractionSignalDetection.startswith("DozorM2"):
            all_positions = grid.find_all_position_dozorm(
                listPositions=dozormListPositions,
                grid_info=grid_info,
                meshPositions=meshPositions,
                isHorizontalRotationAxis=isHorizontalRotationAxis,
                workflowWorkingDir=workflow_working_dir,
            )
        else:
            all_positions = grid.find_all_positions(
                grid_info,
                meshPositions,
                maxNoPositions=int(data_collection_max_positions),
                data_threshold=data_threshold,
                upper_threshold=0.90,
                radius=radius,
                diffractionSignalDetection=diffractionSignalDetection,
            )
        logger.debug("all positions: %s" % pprint.pformat(all_positions))
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            all_positions,
            directory,
            workflow_working_dir,
            data_threshold,
            diffractionSignalDetection=diffractionSignalDetection,
            dozormListPositions=dozormListPositions,
            dozormCrystalMapPath=dozormCrystalMapPath,
            dozormImageNumberMapPath=dozormImageNumberMapPath,
        )
        dict_html_pyarch = {}
        if list_node_id is not None:
            list_node_id = common.jsonLoads(list_node_id)
            html_page_path = os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
            if os.path.exists(html_page_path):
                collect.displayNewHtmlPage(
                    beamline,
                    html_page_path,
                    "Grid results",
                    list_node_id,
                    strPageNumber=run_number,
                    token=token,
                )
                if pyarch_html_dir is not None:
                    if expTypePrefix == "mesh-":
                        directory_name = "Mesh"
                    elif expTypePrefix == "line-":
                        directory_name = "Line"
                    else:
                        directory_name = "Html"
                    dict_html_pyarch = path.copyHtmlDirectoryToPyarch(
                        pyarch_html_dir, dict_html, directory_name
                    )
                    workflowStepType = directory_name
                    status = "Success"
                    imageResultFilePath = os.path.join(
                        dict_html_pyarch["html_directory_path"],
                        dict_html_pyarch["plot_file_name"],
                    )
                    htmlResultFilePath = os.path.join(
                        dict_html_pyarch["html_directory_path"],
                        dict_html_pyarch["index_file_name"],
                    )
                    resultFilePath = os.path.join(
                        dict_html_pyarch["html_directory_path"], "result.json"
                    )
                    open(resultFilePath, "w").write(
                        json.dumps(dict_html["workflowStep"])
                    )
                    ispyb.storeWorkflowStep(
                        beamline,
                        workflowId,
                        workflowStepType,
                        status,
                        imageResultFilePath=imageResultFilePath,
                        htmlResultFilePath=htmlResultFilePath,
                        resultFilePath=resultFilePath,
                    )

        data_collection_max_positions = len(all_positions)
        if data_collection_max_positions > data_collection_max_positions_orig:
            data_collection_max_positions = data_collection_max_positions_orig

        data_collection_exposure_time = (
            timeToReachHendersonLimit / data_collection_no_images
        )
        allPositionFile = path.createNewJsonFile(
            workflow_working_dir, all_positions, "all_positions_"
        )

        returnDict = {
            "allPositionFile": allPositionFile,
            "data_collection_exposure_time": data_collection_exposure_time,
            "meshBestPlotPath": meshBestPlotPath,
            "meshBestPositions": meshBestPositions,
            "meshResultsJsonPath": meshResultsJsonPath,
        }

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    # Dataset outputs:
    return returnDict
