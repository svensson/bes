from bes.workflow_lib import path
from bes import config


def run(beamline, directory, **_):

    parameters = path.getDefaultParameters(directory)
    data_collection_exposure_time = parameters.get(
        "data_collection_exposure_time", None
    )
    data_collection_osc_range = parameters.get("data_collection_osc_range", 0.0)
    data_collection_no_images = parameters.get("data_collection_no_images", 100)

    if data_collection_exposure_time is None:
        data_collection_exposure_time = config.get_value(
            beamline, "Beam", "defaultCollectExposureTime"
        )

    return {
        "workflow_title": "Mesh and burn on %s" % beamline,
        "workflow_type": "MeshScan",
        "data_collection_exposure_time": data_collection_exposure_time,
        "data_collection_osc_range": data_collection_osc_range,
        "data_collection_no_images": data_collection_no_images,
        "data_collection_max_positions": 10000,
        "plateMode": True,
    }
