"""
Initialisation of a workflow
"""

import json
import pprint

from bes.workflow_lib import cryoem
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    processDirectory,
    currentGridSquare,
    gridSquaresJsonFile,
    REFID=None,
    REQUESTID=None,
    **_,
):

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(None, workflowParameters)
    logger.info("*********** cryoemProcessGridSquare.py **********")

    dictGridSquares = json.loads(open(gridSquaresJsonFile).read())

    dictGridSquare = dictGridSquares[currentGridSquare]
    listMovieCreatedSorted, currentMovie = cryoem.updateDictGridSquareForNewMovies(
        dictGridSquare
    )

    logger.info(
        "Sorted listMovieCreated {0}".format(pprint.pformat(listMovieCreatedSorted))
    )

    doProcessMovie = True
    firstMoviesCreated = []
    if currentMovie is None:
        if len(listMovieCreatedSorted) > 0:
            # Take the oldest one
            firstMoviesCreated = listMovieCreatedSorted[:3]
        else:
            logger.info("No more movies!")
            doProcessMovie = False

    open(gridSquaresJsonFile, "w").write(json.dumps(dictGridSquares, indent=4))

    return {
        "doProcessMovie": doProcessMovie,
        "currentGridSquare": currentGridSquare,
        "currentMovie": currentMovie,
        "gridSquaresJsonFile": gridSquaresJsonFile,
        "firstMoviesCreated": firstMoviesCreated,
        # "noMoviesCreated": noMoviesCreated
    }
