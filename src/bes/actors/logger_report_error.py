#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "14/02/2021"

import os


def run(
    workflowParameters=None,
    message_text=None,
    beamline="simulator",
    WorkflowException=None,
    token=None,
    **_,
):

    stackTraceText = None
    errorMessage = None
    try:
        from bes.workflow_lib import common
        from bes.workflow_lib import workflow_logging

        if WorkflowException is not None:
            errorMessage = WorkflowException["errorMessage"]
            message_text = errorMessage
            stackTraceText = ""
            for line in WorkflowException["traceBack"]:
                stackTraceText += line + "\n"
        elif "stackTraceFile" in workflowParameters:
            if os.path.exists(workflowParameters["stackTraceFile"]):
                stackTraceText = open(workflowParameters["stackTraceFile"]).read()
                stackTraceText = common.interpretStacktrace(stackTraceText)
        if message_text is None and stackTraceText is None:
            message_text = "No error message."
        elif message_text is None and stackTraceText is not None:
            message_text = stackTraceText
        elif message_text is not None and stackTraceText is not None:
            message_text = message_text + os.linesep + os.linesep + stackTraceText
        if workflowParameters is not None:
            logger = workflow_logging.getLogger(
                beamline, workflowParameters, token=token
            )
            logger.error("Exception caught during execution of workflow:")
            logger.error(message_text)
    except Exception:
        from bes.workflow_lib import common

        common.logStackTrace(workflowParameters)

    return {
        "errorMessage": errorMessage,
        "message_text": message_text,
        "done_error_report": True,
    }
