"""
Initialisation of a workflow
"""

from bes.workflow_lib import cryoem
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    movieFileName,
    proteinAcronym,
    sampleAcronym,
    doseInitial,
    dosePerFrame,
    cwd,
    timeout_sec=600,
    REFID=None,
    REQUESTID=None,
    **_,
):
    _ = workflow_logging.getLogger()

    stdout, stderr = cryoem.processMovie(
        beamline,
        directory,
        movieFileName,
        proteinAcronym,
        sampleAcronym,
        doseInitial,
        dosePerFrame,
        cwd,
        timeout_sec=600,
    )
    print(stdout)
    print(stderr)

    return {}
