import math
import time

from bes.workflow_lib import common
from bes.workflow_lib import dehydration
from bes.workflow_lib import workflow_logging

PRECISION = 0.1


def run(
    beamline,
    workflowParameters,
    startRH,
    endRH,
    delay,
    motorPositions,
    bestPositionFound=True,
    pyarch_html_dir=None,
    token=None,
    **_,
):
    targetRH = None
    currentRH = None
    steppingSign = 1

    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    motorPositions = common.jsonLoads(motorPositions)
    phi = motorPositions["phi"]

    if not bestPositionFound:

        continueDehydration = False

    else:

        # First loop
        targetRH = startRH

        # Read current RH value
        currentRH = dehydration.read_hydration_level(beamline, targetRH)

        # Set initial RH value
        dehydration.set_hydration_level(beamline, targetRH, delay)

        # Determine stepping sign
        if endRH < currentRH:
            steppingSign = -1

        if math.fabs(currentRH - endRH) > PRECISION:
            continueDehydration = True
        else:
            continueDehydration = False

    return {
        "targetRH": targetRH,
        "currentRH": currentRH,
        "steppingSign": steppingSign,
        "startTime": time.time(),
        "expTypePrefix": "hc1-",
        "dehydrationResults": "[]",
        "continueDehydration": continueDehydration,
        "doRefDataCollectionReview": False,
        "phi_start": phi,
    }
