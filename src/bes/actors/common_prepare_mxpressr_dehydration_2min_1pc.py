from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(beamline, directory, prefix, token=None, **kwargs):

    logger = workflow_logging.getLogger(beamline, token=token)

    if beamline == "id30a2":
        grid_info = {
            "x1": -0.15,
            "y1": -0.15,
            "dx_mm": 0.30,
            "dy_mm": 0.30,
            "steps_x": 5,
            "steps_y": 5,
            "angle": 0.0,
        }
    else:
        grid_info = {
            "x1": -0.10,
            "y1": -0.10,
            "dx_mm": 0.20,
            "dy_mm": 0.20,
            "steps_x": 10,
            "steps_y": 10,
            "angle": 0.0,
        }

    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressR_dehydration_2min_1pc: X-centre + dehydration on %s" % beamline
    )
    workflowParameters["type"] = "MXPressR_dehydration"

    try:
        prefix = collect.getCurrentCdCrystalId(beamline, token=token)
        logger.warning("Prefix set to: {0}".format(prefix))
    except Exception:
        logger.error("Cannot read crystal id! Prefix set to: {0}".format(prefix))

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "grid_info": grid_info,
        "shape": None,
        "automatic_mode": True,
        "doAutoMesh": False,
        "prefix": prefix,
        "anomalousData": False,
        "doCharacterisation": False,
        "checkForFlux": False,
        "do_data_collect": False,
        "startRH": 91.0,
        "endRH": 65.0,
        "step": 1.0,
        "delay": 60.0,
        "transmission": 20.0,
        "no_reference_images": 2,
        "angle_between_reference_images": 90.0,
        "snapShots": True,
        "continue_dehydration": True,
        "doDataCollection": True,
        "targetApertureName": aperture_utils.aperture_size_to_name(beamline, 30),
    }
