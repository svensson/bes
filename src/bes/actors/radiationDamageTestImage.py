import os
import json
import pprint

from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import radiation_damage
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging

from edna2.tasks.ControlDozor import ControlDozor


def run(**inData):
    dozor_result = None
    flux_result = None
    exp_type_prefix = "test-"
    beamline = inData["beamline"]
    workflowParameters = inData["workflowParameters"]
    directory = inData["directory"]
    process_directory = inData["process_directory"]
    token = inData["token"]
    resolution = inData["resolution"]
    prefix = inData["prefix"]
    suffix = inData["suffix"]
    run_number = inData["run_number"]
    transmission = inData["transmission"]
    motor_positions = collect.readMotorPositions(beamline=beamline, token=token)
    sampleInfo = inData["sampleInfo"]
    sample_node_id = sampleInfo["nodeId"]
    group_node_id = sampleInfo["groupNodeId"]
    wavelength = inData["wavelength"]
    sensitivity = inData.get("sensitivity", 1)

    logger = workflow_logging.getLogger(
        beamline=beamline, workflowParameters=workflowParameters, token=token
    )

    logger.info("Taking one test image")
    test_image_exposure_time = inData["test_image_exposure_time"]
    test_image_osc_range = inData["test_image_osc_range"]
    strategy = edna_mxv1.create_n_images_data_collect(
        exposureTime=test_image_exposure_time,
        transmission=transmission,
        number_of_images=1,
        osc_range=test_image_osc_range,
        phi=motor_positions["phi"],
    )
    logger.info("Loading the queue...")
    collect.loadQueue(
        beamline=beamline,
        directory=directory,
        process_directory=process_directory,
        prefix=prefix,
        expTypePrefix=exp_type_prefix,
        run_number=run_number,
        resolution=resolution,
        mxv1StrategyResult=strategy,
        motorPositions=motor_positions,
        workflow_type=workflowParameters["type"],
        workflow_index=workflowParameters["index"],
        group_node_id=group_node_id,
        sample_node_id=sample_node_id,
        dictCellSpaceGroup=None,
        energy=None,
        enabled=True,
        snapShots=False,
        firstImageNo=1,
        inverseBeam=False,
        doProcess=False,
        highResolution=None,
        lowResolution=None,
        token=token,
    )
    test_image_file_name = "{0}{1}_{2}_{3:04d}.{4}".format(
        exp_type_prefix, prefix, run_number, 1, suffix
    )
    test_image_path = os.path.join(directory, test_image_file_name)
    workflow_working_dir = workflowParameters["workingDir"]
    in_data_image_control_dozor = {
        "image": [test_image_path],
        "workingDirectory": workflow_working_dir,
    }
    logger.info("Running control dozor")
    control_dozor = ControlDozor(
        inData=in_data_image_control_dozor,
        workingDirectorySuffix="{0}{1}_{2}".format(exp_type_prefix, prefix, run_number),
    )
    control_dozor.execute()
    dozor_result = control_dozor.outData
    logger.info("")
    logger.info("Dozor results from test image:")
    logger.info("")
    logger.info(json.dumps(dozor_result, indent=4))
    visible_resolution = dozor_result["imageQualityIndicators"][0][
        "dozorVisibleResolution"
    ]
    flux_result = edna_ispyb.getFluxBeamsizeFromImage(
        test_image_path,
        workflow_working_dir,
    )
    logger.info("")
    logger.info("Flux from test image:")
    logger.info("")
    logger.info(json.dumps(flux_result, indent=4))
    aperture_in_um = collect.getApertureSize(beamline, token)
    if aperture_in_um is None:
        raise RuntimeError("Cannot get the current aperture size")
    logger.info("Current aperture size: %d um", aperture_in_um)
    aperture_in_mm = aperture_in_um / 1000.0
    flux = flux_result["flux"]
    dose_rate = radiation_damage.run_flux2dose(flux, wavelength, aperture_in_mm)
    logger.info("Dose rate: {0} [MGy/s]".format(dose_rate))
    rd_plan = radiation_damage.run_rdfit_plan(
        dose_rate * 1e6, visible_resolution, sensitivity
    )
    logger.info("RDFIT data collection plan: {0}".format(pprint.pformat(rd_plan)))
    data_collection_iterations = rd_plan["wedges"]
    data_collection_no_images = rd_plan["images"]
    data_collection_exposure_time = rd_plan["exposure"]
    resolution = rd_plan["resolution"]
    dose_per_wedge = rd_plan["dose_per_wedge"]

    return {
        "dozor_result": dozor_result,
        "flux": flux_result,
        "dose_rate": dose_rate,
        "data_collection_exposure_time": data_collection_exposure_time,
        "data_collection_iterations": data_collection_iterations,
        "data_collection_no_images": data_collection_no_images,
        "resolution": resolution,
        "dose_per_wedge": dose_per_wedge,
    }
