from bes.workflow_lib import collect
from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    collection_software,
    dozormTargetApertureSize=None,
    doCharacterisation=False,
    doChangeAperture=True,
    automatic_mode=False,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    if dozormTargetApertureSize is not None and beamline not in ["id23eh2", "id30a3"]:
        apertureSize = collect.getApertureSize(beamline, token=token)
        if apertureSize is None:
            raise RuntimeError("Cannot get the current aperture size")
        logger.info("Current aperture size: %d um", apertureSize)
        logger.info("DozorM suggested aperture: %d um", dozormTargetApertureSize)
        if apertureSize != dozormTargetApertureSize:
            if not doCharacterisation:
                listDialog = [
                    {
                        "variableName": "changeAperture",
                        "label": "DozorM suggests that aperture should be changed to {0} um for optimal data collection. Change aperture?".format(
                            dozormTargetApertureSize
                        ),
                        "type": "combo",
                        "defaultValue": "Yes",
                        "textChoices": ["Yes", "No"],
                    }
                ]
                dictValues = dialog.openDialog(
                    beamline,
                    listDialog,
                    token=token,
                    collection_software=collection_software,
                    automaticMode=automatic_mode,
                )
            elif doChangeAperture:
                dictValues = {"changeAperture": "Yes"}
            else:
                dictValues = {"changeAperture": "No"}

            if dictValues["changeAperture"] == "Yes":
                _ = collect.setApertureSize(
                    beamline, dozormTargetApertureSize, token=token
                )

    return {}
