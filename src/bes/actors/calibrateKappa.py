#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "27/05/2021"

import os
import time

from bes.workflow_lib import grid
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def centrePin(
    beamline,
    workflowParameters,
    directory,
    snapshotDir,
    background_image,
    pixelPerMm,
    tungstenPixels,
    convergeDistance,
    token,
    doCircleFit=False,
):
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    time.sleep(1)
    snapShotAngles = [0, 90, 180, 270]
    collect.takeSnapshots(
        beamline=beamline,
        snapShotDir=snapshotDir,
        background_image=background_image,
        snapShotAngles=snapShotAngles,
        token=token,
    )
    deltaX, deltaY, deltaZ = grid.centrePin(
        snapshotDir,
        autoMeshWorkingDir=snapshotDir,
        loopWidth=tungstenPixels,
        doCircleFit=doCircleFit,
        debug=False,
    )
    logger.info(
        "DeltaX = {0:.02f}, DeltaY = {1:.02f}, DeltaZ = {2:.02f} (pixels)".format(
            deltaX, deltaY, deltaZ
        )
    )
    logger.info(
        "DeltaX = {0:.04f}, DeltaY = {1:.04f}, DeltaZ = {2:.04f} (mm)".format(
            deltaX / pixelPerMm, deltaY / pixelPerMm, deltaZ / pixelPerMm
        )
    )
    motorPositions = collect.readMotorPositions(beamline, token=token)
    sampxOrig = motorPositions["sampx"]
    sampyOrig = motorPositions["sampy"]
    phiyOrig = motorPositions["phiy"]
    logger.info(
        "Old sampx: {0:.04f}, sampy: {1:.04f}, phiy: {2:.04f}".format(
            sampxOrig, sampyOrig, phiyOrig
        )
    )
    sampx = sampxOrig + deltaX / pixelPerMm
    sampy = sampyOrig + deltaY / pixelPerMm
    phiy = phiyOrig + deltaZ / pixelPerMm
    logger.info(
        "New sampx: {0:.04f}, sampy: {1:.04f}, phiy: {2:.04f}".format(
            sampx, sampy, phiy
        )
    )
    newPositions = {"sampx": float(sampx), "sampy": float(sampy), "phiy": float(phiy)}
    collect.moveMotors(beamline, directory, newPositions, token=token)
    if (
        abs(sampx - sampxOrig) < convergeDistance
        and abs(sampy - sampyOrig) < convergeDistance
    ):
        returnValue = True
        logger.info("Centre pin converged!")
    else:
        returnValue = False
        logger.info("Centre pin didn't converge.")
    return returnValue


def run(
    beamline,
    workflowParameters,
    directory,
    isHorizontalRotationAxis=True,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    motorPositions = collect.readMotorPositions(beamline, token=token)

    workingDir = workflowParameters["workingDir"]
    logger.debug("Snapshot directory: {0}".format(workingDir))
    resultsFile = os.path.join(workingDir, "results.txt")
    logger.info("Results file: {0}".format(resultsFile))

    zoomLevels = config.get_value(beamline, "CalibrateKappa", "zoomLevels")
    kappaAngles = config.get_value(beamline, "CalibrateKappa", "kappaAngles")
    kappaPhiAngles = config.get_value(beamline, "CalibrateKappa", "kappaPhiAngles")
    convergeDistance = config.get_value(beamline, "CalibrateKappa", "convergeDistance")

    dictBackgroundImage = {}
    results = None

    phiyOrig = None
    phizOrig = None
    # Move sample relative 1.5 mm for background snapshot
    if isHorizontalRotationAxis:
        deltaPhiy = config.get_value(beamline, "Goniometer", "deltaPhiy")
        signPhiy = config.get_value(beamline, "Goniometer", "signPhiy")
        phiyOrig = motorPositions["phiy"]
        phiyNew = phiyOrig + deltaPhiy * signPhiy
        collect.moveMotors(beamline, directory, {"phiy": phiyNew}, token=token)
    else:
        deltaPhiz = config.get_value(beamline, "Goniometer", "deltaPhiy")
        signPhiz = config.get_value(beamline, "Goniometer", "signPhiy")
        phizOrig = motorPositions["phiz"]
        phizNew = phizOrig + deltaPhiz * signPhiz
        collect.moveMotors(beamline, directory, {"phiz": phizNew}, token=token)

    # Take background images
    for zoomLevel in zoomLevels:
        logger.info("Zoom level: {0}".format(zoomLevel))
        collect.setZoomLevel(beamline, level=zoomLevel, token=token)
        background_image = os.path.join(
            workingDir, "background_zoom_{0}.png".format(zoomLevel)
        )
        dictBackgroundImage[zoomLevel] = background_image
        time.sleep(1)
        collect.saveSnapshot(beamline, background_image, token=token)
        while not os.path.exists(background_image):
            logger.info("Waiting for background image %s" % background_image)
            fd = os.open(workingDir, os.O_DIRECTORY)
            _ = os.fstat(fd)
            os.close(fd)
            time.sleep(1)

    # Move sample back
    if isHorizontalRotationAxis:
        collect.moveMotors(beamline, directory, {"phiy": phiyOrig}, token=token)
    else:
        collect.moveMotors(beamline, directory, {"phiz": phizOrig}, token=token)

    # Loop over all kappa values, starting with zoom level 1
    for kappaAngle in kappaAngles:
        logger.info("*" * 80)
        logger.info("Moving kappa to: {0:.04f}".format(kappaAngle))
        for kappaPhiAngle in kappaPhiAngles:
            index = 1
            logger.info("=" * 20)
            logger.info("Moving kappa_phi to: {0:.04f}".format(kappaPhiAngle))
            newMotorPositions = {"kappa": kappaAngle, "kappa_phi": kappaPhiAngle}
            collect.moveMotors(
                beamline,
                directory=directory,
                dictPosition=newMotorPositions,
                token=token,
            )
            # Loop over all zoom levels
            converged = False
            for zoomLevel in zoomLevels:
                logger.info("Zoom level: {0}".format(zoomLevel))
                collect.setZoomLevel(beamline, level=zoomLevel, token=token)
                pixelPerMm = config.get_value(
                    beamline, "SnapShotZoomMmPerPixel", str(zoomLevel)
                )
                logger.info("Pixels per mm: {0:.0f}".format(pixelPerMm))
                tungstenPixels = 0.020 * pixelPerMm
                logger.info(
                    "Number of pixels for tungsten pin: {0:.01f}".format(tungstenPixels)
                )
                if zoomLevel == zoomLevels[-1]:
                    doCircleFit = True
                else:
                    doCircleFit = False
                background_image = dictBackgroundImage[zoomLevel]
                kappaPhiSnapshotDir = os.path.join(
                    workingDir,
                    "kappa_{0}_kappaPhi_{1}_zoom_{2}_{3}".format(
                        kappaAngle, kappaPhiAngle, zoomLevel, index
                    ),
                )
                if not os.path.exists(kappaPhiSnapshotDir):
                    os.makedirs(kappaPhiSnapshotDir, mode=0o775)
                converged = centrePin(
                    beamline,
                    workflowParameters,
                    directory,
                    kappaPhiSnapshotDir,
                    background_image,
                    pixelPerMm,
                    tungstenPixels,
                    convergeDistance,
                    token,
                    doCircleFit,
                )
            # We are now at the highest zoom level, redo centre pin
            # till convergence if not converged
            zoomLevel = collect.getZoomLevel(beamline, token)
            background_image = dictBackgroundImage[zoomLevel]
            while not converged:
                index += 1
                kappaPhiSnapshotDir = os.path.join(
                    workingDir,
                    "kappa_{0}_kappaPhi_{1}_zoom_{2}_{3}".format(
                        kappaAngle, kappaPhiAngle, zoomLevel, index
                    ),
                )
                if not os.path.exists(kappaPhiSnapshotDir):
                    os.makedirs(kappaPhiSnapshotDir, mode=0o775)
                converged = centrePin(
                    beamline,
                    workflowParameters,
                    directory,
                    kappaPhiSnapshotDir,
                    background_image,
                    pixelPerMm,
                    tungstenPixels,
                    convergeDistance,
                    token,
                    doCircleFit,
                )
                if not converged and index > 2:
                    logger.info(
                        "Not converged after {0} iterations, stopping.".format(index)
                    )
                    converged = True
            motorPositions = collect.readMotorPositions(beamline, token=token)
            motorPositions["phi"] = 0.0
            if os.path.exists(resultsFile):
                with open(resultsFile) as fd:
                    results = fd.read()
            else:
                results = ""
            results += "{phi:10.4f}{kappa:10.4f}{kappa_phi:10.4f}{sampx:10.4f}{sampy:10.4f}{phiy:10.4f}  1.0\n".format(
                **motorPositions
            )
            with open(resultsFile, "w") as fd:
                fd.write(results)
    return {"results": results}
