"""
Initialisation of a workflow
"""

import os

from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(beamline, directory, REFID=None, REQUESTID=None, **_):

    logger = None

    workflowTitle = "CryoEM Process Grid"
    workflowType = "CryoEMProcessGrid"

    workflowParameters = {
        "title": workflowTitle,
        "type": workflowType,
    }

    # Create the working directory with respect to the image directory
    workflowWorkingDir = path.createWorkflowWorkingDirectory(directory)

    workflowParameters["workingDir"] = workflowWorkingDir

    # Create stack trace file
    workflowStackTraceFile = path.createStackTraceFile(workflowWorkingDir)
    workflowParameters["stackTraceFile"] = workflowStackTraceFile

    workflowLogFile = path.createWorkflowLogFilePath(workflowWorkingDir)
    workflowDebugLogFile = path.createWorkflowDebugLogFilePath(workflowWorkingDir)

    workflowParameters["logFile"] = workflowLogFile
    workflowParameters["debugLogFile"] = workflowDebugLogFile
    workflowParameters["pyarchLogFile"] = None

    # Then set up log file
    logger = workflow_logging.getLogger(
        beamline=None, workflowParameters=workflowParameters
    )
    logger.info("*********** cryoemInitGridProcess.py **********")

    # processDirectory
    processDirectory = directory.replace("RAW_DATA", "PROCESSED_DATA")

    returnDict = {
        "workflow_working_dir": workflowWorkingDir,
        "workflowLogFile": workflowLogFile,
        "workflowDebugLogFile": workflowDebugLogFile,
        "directory": directory,
        "processDirectory": processDirectory,
        "workflowParameters": workflowParameters,
    }

    # Try to create pyarchdir
    workflowPyarchLogFile = None
    pyarch_html_dir = None
    tmpPyarch = path.createPyarchFilePath(workflowWorkingDir)
    if tmpPyarch is not None:
        pyarchDir = tmpPyarch
        workflowPyarchLogFile = os.path.join(
            pyarchDir, os.path.basename(workflowLogFile)
        )
        pyarch_html_dir = os.path.join(pyarchDir, "html")
        workflowParameters["pyarchLogFile"] = workflowPyarchLogFile
        logger = workflow_logging.getLogger(None, workflowParameters)
        logger.debug("pyarchDir: %s" % pyarchDir)
        logger.debug("workflowPyarchLogFile: %s" % workflowPyarchLogFile)
        logger.debug("pyarch_html_dir: %s" % pyarch_html_dir)
        returnDict["pyarchDir"] = pyarchDir
        returnDict["workflowParameters"] = workflowParameters
        returnDict["pyarch_html_dir"] = pyarch_html_dir
        returnDict["workflowPyarchLogFile"] = workflowPyarchLogFile
    workflowParameters["workflowPyarchLogFile"] = workflowPyarchLogFile
    workflowParameters["pyarch_html_dir"] = pyarch_html_dir
    workflowParameters["workflowWorkingDir"] = workflowWorkingDir
    workflowParameters["workflowLogFile"] = workflowLogFile

    # Initialise ispyb
    workflow_id = edna_ispyb.storeOrUpdateWorkflow(
        beamline,
        workflowTitle=workflowParameters["title"],
        workflowType=workflowParameters["type"],
        workflowPyarchLogFile=workflowParameters["workflowPyarchLogFile"],
        workflowLogFile=workflowParameters["workflowLogFile"],
        pyarchHtmlDir=workflowParameters["pyarch_html_dir"],
        workflowWorkingDir=workflowParameters["workflowWorkingDir"],
    )
    workflowParameters["ispyb_id"] = workflow_id
    returnDict["workflow_id"] = workflow_id
    returnDict["workflowParameters"] = workflowParameters

    # Initialise log file
    logger.info("Starting new workflow '%s'" % workflowTitle)
    if REQUESTID is not None:
        logger.info(
            "Beamline Expert System REFID=%r, REQUESTID=%r" % (REFID, REQUESTID)
        )
    logger.info("Image directory: %s" % directory)
    logger.debug("Workflow type: %s" % workflowType)
    logger.debug("Workflow working directory: %r" % workflowWorkingDir)
    logger.debug("ISPyB workflow id: %r" % workflow_id)

    common.send_email(
        beamline,
        [
            "svensson@esrf.fr",
        ],
        "Started",
        directory,
        workflowTitle,
        workflowType,
        workflowWorkingDir,
    )

    return returnDict
