#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "03/05/2021"

from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, directory, workflowParameters=None, token=None, **_):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    newKappaAngle = 0.0
    kappa_settings_id = 1

    newMotorPositionKappa = {"kappa": newKappaAngle}

    collect.moveMotors(beamline, directory, newMotorPositionKappa, token=token)

    logger.info("Kappa angle set to {0}".format(newKappaAngle))

    motorPositions = collect.readMotorPositions(beamline, token=token)

    return {
        "motorPositions": motorPositions,
        "kappa_settings_id": kappa_settings_id,
        "doOpenKappa": False,
    }
