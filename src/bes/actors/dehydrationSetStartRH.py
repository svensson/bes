from bes.workflow_lib import dehydration
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, startRH=None, delay=None, token=None, **_):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    if startRH is not None:

        logger.info("Setting humidity to start level: {0} %".format(startRH))
        startRH = float(startRH)
        delay = float(delay)

        # Read current RH value
        currentRH = dehydration.read_hydration_level(beamline, startRH)

        # Set initial RH value
        dehydration.set_hydration_level(beamline, startRH, delay)

    else:
        logger.error("Cannot set humidity to start level")

    return {
        "currentRH": currentRH,
    }
