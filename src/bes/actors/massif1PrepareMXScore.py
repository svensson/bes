def run(**_):

    return {
        "expTypePrefix": "ref-",
        "doRefDataCollectionReview": False,
        "automatic_mode": True,
        "no_reference_images": 4,
        "anomalousData": False,
    }
