import json

from bes.workflow_lib import is4a
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging

from edna2.tasks.ISPyBTasks import RetrieveAttachmentFiles
from edna2.tasks.Is4aTasks import FindPipelineForMerge
from edna2.tasks.Is4aTasks import MergeUtls


def run(
    jobInput, is4aId, is4aStatus, is4aUrl, workflowParameters, proposal, token, **_
):

    jobInput = common.jsonLoads(jobInput)
    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(None, workflowParameters)

    logger.info("is4aId: '{0}'".format(is4aId))
    logger.info("is4aUrl: '{0}'".format(is4aUrl))
    logger.info("input: '{0}'".format(json.dumps(jobInput, indent=4)))

    if is4aStatus == "PENDING":

        listDataCollectionIds = list(map(int, jobInput["datacollectionids"].split(",")))

        inDataFindHkl = {
            "token": token,
            "proposal": proposal,
            "dataCollectionId": listDataCollectionIds,
        }

        logger.info(json.dumps(inDataFindHkl, indent=4))

        findPipelineForMerge = FindPipelineForMerge(inData=inDataFindHkl)
        findPipelineForMerge.execute()
        outDataFindPipeline = findPipelineForMerge.outData

        logger.info(json.dumps(outDataFindPipeline, indent=4))

        if "error" not in outDataFindPipeline:
            is4a.postOnHold(is4aId, is4aUrl, outDataFindPipeline)

    elif is4aStatus == "RESUME":

        if "pipeline" in jobInput:
            listAttachments = []
            pipeline = jobInput["pipeline"]
            listAttachments = jobInput[pipeline]
            inDataRetriveAttachment = {
                "token": token,
                "proposal": proposal,
                "attachment": listAttachments,
            }
            retrieveAttachmentFiles = RetrieveAttachmentFiles(
                inData=inDataRetriveAttachment
            )
            retrieveAttachmentFiles.execute()
            if retrieveAttachmentFiles.isSuccess():
                outData = retrieveAttachmentFiles.outData
                listFilePath = outData["filePath"]
                inData = {"listHklLp": []}
                for filePath in listFilePath:
                    inData["listHklLp"].append({"hkl": filePath})
                mergeUtls = MergeUtls(inData=inData)
                mergeUtls.execute()
                is4a.setStatus(is4aId, is4aUrl, "FINISHED")
            else:
                is4a.setStatus(is4aId, is4aUrl, "ERROR")

    is4aStatus = is4a.getStatus(is4aId, is4aUrl)
    logger.info("Status : {0}".format(is4aStatus))
    # is4a.setStatus(is4aId, is4aUrl, is4aStatus)
    # logger.info("Status from is4a : {0}".format(is4a.getStatus(is4aId, is4aUrl)))

    dictOutputFile = {}

    return {"dictOutputFile": dictOutputFile, "is4aStatus": is4aStatus}
