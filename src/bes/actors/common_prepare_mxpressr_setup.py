from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import aperture_utils


def run(directory, **kwargs):

    workflowParameters = {}
    workflowParameters["title"] = "MXPressR_setup"
    workflowParameters["type"] = "MXPressR_setup"

    beamline = edna_mxv1.extractBeamlineFromDirectory(directory)
    parameters = path.getDefaultParameters(directory)
    aimed_resolution = parameters.get("aimedResolution", 2.0)
    number_of_positions = parameters.get("numberOfPositions", 1)
    preferred_aperture = parameters.get(
        "preferredApertureName", aperture_utils.aperture_size_to_name(beamline, 30)
    )
    aimed_multiplicity = parameters.get("aimedMultiplicity", 2.5)
    aimed_completeness = parameters.get("aimedCompleteness", 100)
    grid_transmission = parameters.get("gridTransmission", 50)
    grid_size = parameters.get("gridSize", 300)
    grid_steps = parameters.get("gridSteps", 10)
    diffraction_signal_detection = parameters.get(
        "diffractionSignalDetection", "DozorM2 (macro-molecules crystal detection)"
    )
    forced_space_group = parameters.get("forcedSpaceGroup", None)

    return {
        "beamline": beamline,
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "automatic_mode": True,
        "doAutoMesh": False,
        "anomalousData": False,
        "doCharacterisation": False,
        "checkForFlux": False,
        "collectTransmission": 10,
        "collectExposureTime": 0.02,
        "gridTransmission": grid_transmission,
        "preferredApertureName": preferred_aperture,
        "aimedResolution": aimed_resolution,
        "aimedMultiplicity": aimed_multiplicity,
        "aimedCompleteness": aimed_completeness,
        "numberOfPositions": number_of_positions,
        "gridSize": grid_size,
        "gridSteps": grid_steps,
        "diffractionSignalDetection": diffraction_signal_detection,
        "forcedSpaceGroup": forced_space_group,
    }
