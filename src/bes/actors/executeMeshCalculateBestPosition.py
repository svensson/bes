#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF"
__updated__ = "2020-12-17"

import os
import sys
import pprint
import traceback

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes import config
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna2_tasks
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    meshPositionFile,
    run_number,
    workflow_working_dir,
    grid_info,
    firstImagePath,
    workflowParameters,
    weakDiffractionWarning=False,
    meshUpperThreshold=None,
    diffractionSignalDetection="DozorM2 (macro-molecules crystal detection)",
    numberOfPositions=1,
    moreSamples=False,
    remainingPositions=None,
    dozorThreshold=None,
    token=None,
    dozorAllFile=None,
    meshZigZag=None,
    dozormTargetApertureSize=None,
    multiPositionIndex=None,
    isHorizontalRotationAxis=True,
    reject_level=None,
    position_id=None,
    simulated_grid_positions=False,
    doTwoMeshes=False,
    **_,
):

    meshResultsJsonPath = None
    collectionComment = None
    dozormCrystalMapPath = None
    dozormColourMapPath = None
    dozormImageNumberMapPath = None
    dozorm2ExecutionError = False

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    if reject_level is None:
        reject_level = config.get_value(
            beamline, "Mesh", "rejectLevel", default_value=40
        )
    loop_thickness = config.get_value(
        beamline, "Mesh", "loopThickness", default_value=500
    )
    correct_dozorm2 = config.get_value(
        beamline,
        "Mesh",
        "correct_dozorm2",
        default_value=False,
    )

    logger.info("Calculating best position")
    logger.debug(f"executeMeshCalculateBestPosition correct_dozorm2: {correct_dozorm2}")
    workflowParameters = common.jsonLoads(workflowParameters)
    workingDirectory = workflowParameters["workingDir"]
    run_number = int(run_number)
    numberOfPositions = int(numberOfPositions)
    remainingPositions = common.jsonLoads(remainingPositions)
    grid_info = common.jsonLoads(grid_info)

    if position_id is None:
        position_id = 1

    meshPositions = path.parseJsonFile(meshPositionFile)
    dozormListPositions = None
    if dozorAllFile is None:
        listDozorAllFile = []
    else:
        listDozorAllFile = [dozorAllFile]

    if grid_info["steps_x"] != 1 and grid_info["steps_y"] != 1:
        if diffractionSignalDetection.startswith("DozorM2"):
            try:
                listApertureSizes = config.get_value(beamline, "Beam", "apertureSizes")
                nominalBeamSizeX = config.get_value(beamline, "Beam", "nominalSizeX")
                nominalBeamSizeY = config.get_value(beamline, "Beam", "nominalSizeY")
                logger.info("Running DozorM2...")
                outDataDozorM2 = edna2_tasks.runDozorM2(
                    beamline=beamline,
                    firstImagePath=firstImagePath,
                    grid_info=grid_info,
                    meshZigZag=meshZigZag,
                    listDozorAllFile=listDozorAllFile,
                    listApertureSizes=listApertureSizes,
                    nominalBeamSizeX=nominalBeamSizeX,
                    nominalBeamSizeY=nominalBeamSizeY,
                    workingDirectory=workingDirectory,
                    isHorizontalRotationAxis=isHorizontalRotationAxis,
                    reject_level=reject_level,
                    loop_thickness=loop_thickness,
                )
                # TODO: if outDataDozorM2 is an empty dict the processing failed
                logger.info("DozorM2 finished")
                dozormListPositions = outDataDozorM2["dictCoord"]["scan1"]
                arrayScore = outDataDozorM2["score"]
                meshPositions = edna2_tasks.updateDozorMMeshPositions(
                    arrayScore=arrayScore, meshPositions=meshPositions
                )
                prefix = path.getPrefix(firstImagePath)
                meshPositionFile = path.createNewJsonFile(
                    workingDirectory,
                    meshPositions,
                    "meshPositionsDozorM_{0}".format(prefix),
                )
                dozormCrystalMapPath = outDataDozorM2["crystalMapPath"]
                dozormColourMapPath = outDataDozorM2["colourMapPath"]
                dozormImageNumberMapPath = outDataDozorM2["imageNumberMapPath"]
            except Exception:
                dozorm2ExecutionError = True
                logger.error("DozorM2 error - no best position found")
                # logger.error("Falling back to 'Dozor' diffraction signal detection.")
                diffractionSignalDetection = "Dozor"
                (exc_type, exc_value, exc_traceback) = sys.exc_info()
                listTrace = traceback.extract_tb(exc_traceback)
                logger.debug(exc_type)
                logger.debug(exc_value)
                logger.debug("Traceback (most recent call last): %s" % os.linesep)
                for listLine in listTrace:
                    errorLine = '  File "%s", line %d, in %s%s' % (
                        listLine[0],
                        listLine[1],
                        listLine[2],
                        os.linesep,
                    )
                    logger.debug(errorLine)

    if meshUpperThreshold is not None:
        meshUpperThreshold = float(meshUpperThreshold)

    best_position = None
    if (
        numberOfPositions > 1
        and grid_info["steps_x"] != 1
        and grid_info["steps_y"] != 1
        and not dozorm2ExecutionError
    ):
        if diffractionSignalDetection.startswith("DozorM2"):
            logger.debug(
                f"Before find_all_position_dozorm correct_dozorm2: {correct_dozorm2}"
            )
            all_positions = grid.find_all_position_dozorm(
                listPositions=dozormListPositions,
                grid_info=grid_info,
                meshPositions=meshPositions,
                maxNoPositions=numberOfPositions,
                isHorizontalRotationAxis=isHorizontalRotationAxis,
                workflowWorkingDir=workflow_working_dir,
                correct_dozorm2=correct_dozorm2,
            )
        else:
            all_positions = grid.find_all_positions(
                grid_info,
                meshPositions,
                maxNoPositions=numberOfPositions,
                upper_threshold=meshUpperThreshold,
                diffractionSignalDetection=diffractionSignalDetection,
                radius=2,
            )
        if len(all_positions) > 0 and (remainingPositions is None or doTwoMeshes):
            best_position = all_positions[0]
            if len(all_positions) > 1:
                moreSamples = True
                remainingPositions = all_positions[1:]
                multiPositionIndex = 1
                if dozorThreshold is not None:
                    # Delete positions with dozor-score less that dozorThreshold in fraction
                    dozorScoreMax = None
                    if "dozor_score" in remainingPositions[0]:
                        dozorScoreMax = remainingPositions[0]["dozor_score"]
                    elif "dozor_score" in remainingPositions[0]["nearest_image"]:
                        dozorScoreMax = remainingPositions[0]["nearest_image"][
                            "dozor_score"
                        ]
                    else:
                        raise BaseException("Cannot find dozor score for position!!!")
                    dozorScoreThreshold = dozorScoreMax * dozorThreshold
                    remainingPositions = grid.deletePositions(
                        remainingPositions, dozorScoreThreshold
                    )
                    noPositions = len(remainingPositions)
                    if noPositions == 0:
                        moreSamples = False
                        collectionComment = "Only one position detected."
                    else:
                        collectionComment = "{0} positions detected.".format(
                            noPositions + 1
                        )
    elif (
        diffractionSignalDetection.startswith("DozorM2")
        and grid_info["steps_x"] != 1
        and grid_info["steps_y"] != 1
        and not dozorm2ExecutionError
    ):
        best_position = grid.find_best_position_dozorm(
            listPositions=dozormListPositions,
            grid_info=grid_info,
            meshPositions=meshPositions,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
            workflowWorkingDir=workflow_working_dir,
            correct_dozorm2=correct_dozorm2,
        )
    else:
        best_position = grid.find_best_position(
            grid_info=grid_info,
            meshPositions=meshPositions,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
            upper_threshold=meshUpperThreshold,
            diffractionSignalDetection=diffractionSignalDetection,
        )

    if beamline == "id30a3":
        firstImagePath = firstImagePath.replace(".cbf", ".h5")
    dictDataCollection = ispyb.findDataCollectionFromFileLocationAndFileName(
        beamline, firstImagePath
    )
    if dictDataCollection is None:
        raise RuntimeError(
            "Cannot find data collection for {0} in ISPyB - the data collection most likely failed.".format(
                firstImagePath
            )
        )
    try:
        beamSizeAtSampleX = dictDataCollection["beamSizeAtSampleX"]
        beamSizeAtSampleY = dictDataCollection["beamSizeAtSampleY"]
    except AttributeError:
        logger.warning("Couldn't retrieve the beam size from ISPyB!")
        apertureSize = collect.getApertureSize(beamline, token)
        beamSizeAtSampleX = apertureSize / 1000.0
        beamSizeAtSampleY = apertureSize / 1000.0
        logger.info(
            f"Beamsize from aperture: {beamSizeAtSampleX}, {beamSizeAtSampleY} mm"
        )
    if (
        beamSizeAtSampleX is None
        or beamSizeAtSampleX < 0
        or beamSizeAtSampleY is None
        or beamSizeAtSampleY < 0
    ):
        logger.warning(
            "Beam size X returned from ISPyB is {0}".format(beamSizeAtSampleX)
        )
        logger.warning(
            "Beam size Y returned from ISPyB is {0}".format(beamSizeAtSampleY)
        )
        if beamline in ["id23eh2"]:
            apertureSize = 5
            logger.warning(
                "Aperture size hard-wired to {0} um for {1}".format(
                    apertureSize, beamline
                )
            )
        else:
            apertureSize = collect.getApertureSize(beamline, token=token)
            if apertureSize is None:
                apertureSize = 10
                logger.warning(
                    "Aperture size hard-wired to %d um for %s", apertureSize, beamline
                )
        beamSizeAtSampleX = apertureSize / 1000.0
        beamSizeAtSampleY = apertureSize / 1000.0
        logger.warning(
            "Setting the beam size X and Y to the aperture size {0} um".format(
                apertureSize
            )
        )
        logger.warning("Beam size X: {0} mm".format(beamSizeAtSampleX))
        logger.warning("Beam size Y: {0} mm".format(beamSizeAtSampleY))

    if best_position is not None and "dozormTargetApertureSize" in best_position:
        if best_position["dozormTargetApertureSize"] is not None:
            dozormTargetApertureSize = best_position["dozormTargetApertureSize"]

    if simulated_grid_positions:
        if grid_info["steps_x"] > 1:
            indexY = (grid_info["steps_x"] - 1) / 2
        else:
            indexY = 0
        if grid_info["steps_y"] > 1:
            indexZ = (grid_info["steps_y"] - 1) / 2
        else:
            indexZ = 0
        for position in meshPositions:
            if position["indexY"] == int(indexY) and position["indexZ"] == int(indexZ):
                simulated_position = dict(position)
                break
        best_position = {
            "sampx": simulated_position["sampx"],
            "sampy": simulated_position["sampy"],
            "phiz": simulated_position["phiz"],
            "phiy": simulated_position["phiy"],
            "omega": simulated_position["omega"],
            "kappa": 0,
            "phi": 0,
            "focus": -0.0393,
            "indexY": indexY,
            "indexZ": indexZ,
            "nearest_image": {
                "indexY": int(indexY),
                "indexZ": int(indexZ),
                "focus": -0.0393,
                "omega": 60.141,
                "kappa": 0,
                "phi": 0,
                "sampx": simulated_position["sampx"],
                "sampy": simulated_position["sampy"],
                "phiz": simulated_position["phiz"],
                "phiy": simulated_position["phiy"],
                "index": 148,
                "imageName": simulated_position["imageName"],
                "dozor_score": 251.152,
                "angle": 60.08005000000001,
                "number": 149,
                "image": simulated_position["image"],
                "dozorScore": 251.152,
                "dozorSpotScore": 256.44,
                "dozorSpotsNumOf": 424,
                "dozorSpotsIntAver": 477,
                "dozorSpotsResolution": 2.1,
                "dozorVisibleResolution": 1.92,
                "dozorSpotListShape": [424, 5],
                "peak_size_x": 0.1869677419354839,
                "peak_size_y": 0.02457142857142857,
                "crystal_size_x": 0.2700645161290323,
                "crystal_size_y": 0.04914285714285714,
            },
            "max_peak": 300.338,
            "no_pixels_cross_x": 9,
            "no_pixels_cross_y": 1,
            "peak_size_x": 0.1869677419354839,
            "peak_size_y": 0.02457142857142857,
            "crystal_size_x": 0.2700645161290323,
            "crystal_size_y": 0.04914285714285714,
        }
        if numberOfPositions == 1 and (
            grid_info["steps_x"] == 1 or grid_info["steps_y"] == 1
        ):
            logger.warning("Using fake best position!")
        elif (
            numberOfPositions > 1
            and grid_info["steps_x"] != 1
            and grid_info["steps_y"] != 1
        ):
            logger.warning(f"Using {numberOfPositions} fake positions!")
            remainingPositions = []
            for i in range(numberOfPositions - 1):
                next_best_position = dict(best_position)
                next_best_position["indexY"] += 1
                next_best_position["indexZ"] += 1
                remainingPositions.append(next_best_position)

    logger.info("Data processing finished.")

    perform_extra_1D_scan = False
    if beamline in ["id30a1"]:
        if (
            isHorizontalRotationAxis
            and best_position is not None
            and grid_info["steps_x"] > 1
        ):
            if "no_pixels_cross_y" in best_position:
                if best_position["no_pixels_cross_y"] == 1:
                    logger.info(
                        "Only one pixel used for vertical beam position determination - extra vertical scan might be needed"
                    )
                    perform_extra_1D_scan = True
        elif (
            (not isHorizontalRotationAxis)
            and best_position is not None
            and grid_info["steps_y"] > 1
        ):
            if "no_pixels_cross_x" in best_position:
                if best_position["no_pixels_cross_x"] == 1:
                    logger.info(
                        "Only one pixel used for horizontal beam position determination - extra horizontal scan might be needed"
                    )
                    perform_extra_1D_scan = True
    elif beamline in ["id23eh2"]:
        perform_extra_1D_scan = False

    if best_position is not None:
        bestPositionFound = True
    else:
        bestPositionFound = False

    # Dataset outputs:
    dict_output = {
        "bestPosition": best_position,
        "bestPositionFound": bestPositionFound,
        "remainingPositions": remainingPositions,
        "moreSamples": moreSamples,
        "beamSizeAtSampleX": beamSizeAtSampleX,
        "beamSizeAtSampleY": beamSizeAtSampleY,
        "perform_extra_1D_scan": perform_extra_1D_scan,
        "meshResultsJsonPath": meshResultsJsonPath,
        "dozormListPositions": dozormListPositions,
        "meshPositionFile": meshPositionFile,
        "dozormCrystalMapPath": dozormCrystalMapPath,
        "dozormColourMapPath": dozormColourMapPath,
        "dozormImageNumberMapPath": dozormImageNumberMapPath,
        "dozormTargetApertureSize": dozormTargetApertureSize,
        "multiPositionIndex": multiPositionIndex,
        "dozorm2ExecutionError": dozorm2ExecutionError,
        "position_id": position_id,
    }

    logger.debug(
        "executeMeshCalculateBestPosition.py: dict_output: {0}".format(
            pprint.pformat(dict_output)
        )
    )

    if best_position is not None:
        if grid_info["steps_x"] == 1:
            # Vertical scan
            if "crystal_size_y" in best_position:
                dict_output["crystalSizeZ"] = best_position["crystal_size_y"]
            if "peak_size_y" in best_position:
                dict_output["peakSizeZ"] = best_position["peak_size_y"]
            if "dozorVisibleResolution" in best_position:
                dozorVisibleResolution = best_position["dozorVisibleResolution"]
                dict_output["dozorVisibleResolutionVertical"] = dozorVisibleResolution
            elif "dozorVisibleResolution" in best_position["nearest_image"]:
                dozorVisibleResolution = best_position["nearest_image"][
                    "dozorVisibleResolution"
                ]
                dict_output["dozorVisibleResolutionVertical"] = dozorVisibleResolution
        elif grid_info["steps_y"] == 1:
            # Horizontal scan
            if "crystal_size_x" in best_position:
                dict_output["crystalSizeZ"] = best_position["crystal_size_x"]
            if "peak_size_x" in best_position:
                dict_output["peakSizeZ"] = best_position["peak_size_x"]
            if "dozorVisibleResolution" in best_position:
                dozorVisibleResolution = best_position["dozorVisibleResolution"]
                dict_output["dozorVisibleResolutionVertical"] = dozorVisibleResolution
            elif "dozorVisibleResolution" in best_position["nearest_image"]:
                dozorVisibleResolution = best_position["nearest_image"][
                    "dozorVisibleResolution"
                ]
                dict_output["dozorVisibleResolutionVertical"] = dozorVisibleResolution
        else:
            if "crystal_size_x" in best_position and "crystal_size_y" in best_position:
                dict_output["crystalSizeX"] = best_position["crystal_size_x"]
                dict_output["crystalSizeY"] = best_position["crystal_size_y"]
            if "peak_size_x" in best_position and "peak_size_y" in best_position:
                dict_output["peakSizeX"] = best_position["peak_size_x"]
                dict_output["peakSizeY"] = best_position["peak_size_y"]
            if "dozorVisibleResolution" in best_position:
                dozorVisibleResolution = best_position["dozorVisibleResolution"]
                dict_output["dozorVisibleResolutionHorizontal"] = dozorVisibleResolution
            elif "dozorVisibleResolution" in best_position["nearest_image"]:
                dozorVisibleResolution = best_position["nearest_image"][
                    "dozorVisibleResolution"
                ]
                dict_output["dozorVisibleResolutionHorizontal"] = dozorVisibleResolution

        if diffractionSignalDetection.startswith("Dozor"):
            # Check if weak signal
            dozorScore = None

            if "dozor_score" in best_position:
                dozorScore = best_position["dozor_score"]
            elif "dozor_score" in best_position["nearest_image"]:
                dozorScore = best_position["nearest_image"]["dozor_score"]

            if dozorScore is not None and dozorScore < 1.0:
                message_text = "Very weak diffraction."
                logger.warning(message_text)
                ispyb.updateDataCollectionComment(
                    beamline, firstImagePath, message_text
                )
                if not weakDiffractionWarning:
                    ispyb.updateDataCollectionGroupComment(
                        beamline, firstImagePath, message_text
                    )
                    dict_output["weakDiffractionWarning"] = True

    logger.debug(
        "executeMeshCalculateBestPosition.py: dict_output: {0}".format(
            pprint.pformat(dict_output)
        )
    )

    if (
        collectionComment is not None
        and collectionComment != ""
        and firstImagePath is not None
    ):
        ispyb.updateDataCollectionComment(beamline, firstImagePath, collectionComment)
        ispyb.updateDataCollectionGroupComment(
            beamline, firstImagePath, collectionComment
        )

    return dict_output
