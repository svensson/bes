def run(beamline, **_):
    return {
        "workflow_title": "Kappa goniostat reorientation on %s" % beamline,
        "workflow_type": "KappaReorientation",
        "doRefDataCollectionReview": False,
        "createNewDataCollectionGroup": True,
    }
