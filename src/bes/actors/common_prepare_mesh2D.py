def run(beamline, expTypePrefix="mesh-", **_):
    return {
        "workflow_title": "Mesh scan on %s" % beamline,
        "workflow_type": "MeshScan",
        "expTypePrefix": expTypePrefix,
        "plateMode": True,
    }
