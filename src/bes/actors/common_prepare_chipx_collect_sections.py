def run(beamline, **_):
    return {
        "workflow_title": "ChipX collect sections on %s" % beamline,
        "workflow_type": "ChipX_collect_sections",
    }
