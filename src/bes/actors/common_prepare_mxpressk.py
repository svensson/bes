from bes.workflow_lib import common


def run(beamline, expTypePrefix="mesh-", **kwargs):

    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressK: X-centre, eEDNA, dc and if low symmetry dc with open kappa on %s"
        % beamline
    )
    workflowParameters["type"] = "MXPressA"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "automatic_mode": True,
        "openKappaForLowSymmetry": False,
    }
