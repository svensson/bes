#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "01/03/2021"

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    mxv1StrategyResultFile,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    flux,
    beamSizeX,
    beamSizeY,
    workflow_type,
    workflow_working_dir,
    list_node_id,
    workflowParameters,
    sampleInfo=None,
    transmission=None,
    resolution=None,
    omegaMin=None,
    apertureSize=None,
    minExposureTime=None,
    pyarch_html_dir=None,
    strategyOption="-low never",
    anomalousData=None,
    crystalSizeX=None,
    crystalSizeY=None,
    crystalSizeZ=None,
    aimedCompleteness=None,
    aimedMultiplicity=None,
    sampleSusceptibility=None,
    forcedSpaceGroup=None,
    characterisationComment=None,
    doseLimit=None,
    rFriedel=None,
    complexity="min",
    aimedIOverSigmaAtHighestResolution=1.0,
    token=None,
    **_,
):

    try:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        workflowParameters = common.jsonLoads(workflowParameters)
        sampleInfo = common.jsonLoads(sampleInfo)
        aimedIOverSigmaAtHighestResolution = float(aimedIOverSigmaAtHighestResolution)

        # Enable logging in mxCuBE
        # os.environ["mxCuBE_XMLRPC_log"] = "Enabled"

        run_number = int(run_number)
        flux = float(flux)
        beamSizeX = float(beamSizeX)
        beamSizeY = float(beamSizeY)
        if transmission is not None:
            transmission = float(transmission)
        if resolution is not None:
            resolution = float(resolution)

        characterisationInputFile = mxv1StrategyResultFile
        mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)

        listImages = edna_mxv1.getListImagePath(
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            bFirstImageOnly=False,
        )

        message_text = ""

        firstImagePath = edna_mxv1.getListImagePath(
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            bFirstImageOnly=True,
        )[0]

        if apertureSize is not None:
            apertureSize = float(apertureSize)

        if omegaMin is not None:
            omegaMin = float(omegaMin)

        (forcedSpaceGroup, newCharacterisationComment) = symmetry.checkForcedSpaceGroup(
            forcedSpaceGroup, sampleInfo, directory
        )

        if (
            characterisationComment is None
            and firstImagePath is not None
            and forcedSpaceGroup is not None
        ):
            characterisationComment = newCharacterisationComment
            if characterisationComment == "":
                characterisationComment = (
                    "Characterisation: Space group {0} forced.".format(forcedSpaceGroup)
                )
            else:
                characterisationComment += (
                    " Characterisation: Space group {0} forced.".format(
                        forcedSpaceGroup
                    )
                )
            ispyb.updateDataCollectionComment(
                beamline, firstImagePath, characterisationComment
            )
            ispyb.updateDataCollectionGroupComment(
                beamline, firstImagePath, characterisationComment
            )

        if minExposureTime is None:
            minExposureTime = config.get_value(
                beamline, "Beam", "minCharacterisationExposureTime"
            )

        detectorDistanceMin = config.get_value(beamline, "Detector", "distanceMin")
        detectorDistanceMax = config.get_value(beamline, "Detector", "distanceMax")
        minTransmission = config.get_value(beamline, "Beam", "minTransmission")

        xsDataInterfaceResult, ednaLogPath = edna_mxv1.mxv1ControlInterface(
            beamline,
            listImages,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            flux,
            beamSizeX,
            beamSizeY,
            strategyOption,
            minExposureTime,
            workflow_working_dir,
            transmission=transmission,
            crystalSizeX=crystalSizeX,
            crystalSizeY=crystalSizeY,
            crystalSizeZ=crystalSizeZ,
            bLogExecutiveSummary=False,
            anomalousData=anomalousData,
            aimedCompleteness=aimedCompleteness,
            aimedMultiplicity=aimedMultiplicity,
            sampleSusceptibility=sampleSusceptibility,
            detectorDistanceMin=detectorDistanceMin,
            detectorDistanceMax=detectorDistanceMax,
            minTransmission=minTransmission,
            forcedSpaceGroup=forcedSpaceGroup,
            omegaMin=omegaMin,
            apertureSize=apertureSize,
            complexity=complexity,
            rFriedel=rFriedel,
            doseLimit=doseLimit,
            edPluginControlInterfaceName="EDPluginControlInterfacev1_3",
            aimedIOverSigmaAtHighestResolution=aimedIOverSigmaAtHighestResolution,
        )

        mxv1StrategyResult = ""
        mxv1ResultCharacterisation = ""
        characterisation_success = False
        xsDataResultCharacterisation = xsDataInterfaceResult.resultCharacterisation
        if xsDataResultCharacterisation is None:
            message_text = "No results obtained from EDNA characterisation!"
            logger.error(message_text)
        else:
            mxv1ResultCharacterisation = xsDataResultCharacterisation.marshal()
            strHtmlPagePath, strJsonPath = edna_mxv1.createSimpleHtmlPage(
                beamline,
                mxv1ResultCharacterisation,
                workflow_working_dir,
                pyarch_html_dir,
                resolution=resolution,
            )
            if strHtmlPagePath is not None:
                if list_node_id is not None:
                    list_node_id = common.jsonLoads(list_node_id)
                    collect.displayNewHtmlPage(
                        beamline,
                        strHtmlPagePath,
                        "Enhanced characterisation",
                        list_node_id,
                        token=token,
                    )
            if xsDataResultCharacterisation.strategyResult is None:
                message_text = (
                    "No strategy results obtained from EDNA characterisation!"
                )
                logger.error(message_text)
            else:
                if xsDataResultCharacterisation.strategyResult.collectionPlan != []:
                    characterisation_success = True
                mxv1StrategyResult = (
                    xsDataResultCharacterisation.strategyResult.marshal()
                )

        mxv1StrategyResultFile = path.createNewXSDataFile(
            workflowParameters["workingDir"],
            mxv1StrategyResult,
            filePrefix="mxv1StrategyResult",
        )
        mxv1ResultCharacterisationFile = path.createNewXSDataFile(
            workflowParameters["workingDir"],
            mxv1ResultCharacterisation,
            filePrefix="mxv1ResultCharacterisation",
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "characterisation_success": characterisation_success,
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
        "mxv1ResultCharacterisationFile": mxv1ResultCharacterisationFile,
        "message_text": message_text,
        "characterisationComment": characterisationComment,
        "characterisationInputFile": characterisationInputFile,
    }
