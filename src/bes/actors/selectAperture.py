"""
Created on Oct 3, 2016

@author: svensson
"""

from bes import config
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    doCharacterisation=False,
    characterisationExposureTime=None,
    firstImagePath=None,
    targetApertureName=None,
    peakSizeX=None,
    peakSizeY=None,
    peakSizeZ=None,
    ispybApertureComment=None,
    ispybExposureTimeComment=None,
    token=None,
    doFbest=False,
    dozormTargetApertureSize=None,
    multiPositionIndex=None,
    adaptiveAperture=True,
    **_,
):

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    newApertureSize = None
    apertureComment = None
    newExposureTimeComment = None

    if adaptiveAperture and beamline in ["id23eh1", "id30a1", "id30a2", "id30b"]:

        # Change it only of target aperture not set
        if targetApertureName is None:

            # If we have dozorm results
            if dozormTargetApertureSize is not None:
                newApertureSize = dozormTargetApertureSize

            # Otherwise use peak size
            elif (
                peakSizeX is not None
                and peakSizeY is not None
                and peakSizeZ is not None
            ):
                peakSizeX = float(peakSizeX)
                peakSizeY = float(peakSizeY)
                peakSizeZ = float(peakSizeZ)

                aperture_sizes = config.get_value(beamline, "Beam", "apertureSizes")
                maxAperture = max(aperture_sizes)

                if maxAperture == 100 and peakSizeX > 0.065 and peakSizeY > 0.065:
                    newApertureSize = 100
                elif peakSizeX > maxAperture and peakSizeY > maxAperture:
                    newApertureSize = maxAperture
                else:
                    preferredBeamDiameter = min([peakSizeX, peakSizeY]) * 1000.0
                    newApertureSize = aperture_utils.closest_aperture_size(
                        beamline, preferredBeamDiameter
                    )

            # Set ISPyB data collection comment
            if multiPositionIndex is None:
                apertureComment = "Dynamic aperture set to "
            else:
                apertureComment = "Dynamic aperture for position {0} set to {1}".format(
                    multiPositionIndex, newApertureSize
                )
        else:
            # Use the target aperture
            newApertureSize = aperture_utils.aperture_name_to_size(
                beamline, targetApertureName
            )
            apertureComment = "Aperture set to "

        if newApertureSize is not None:

            if not collect.setApertureSize(beamline, newApertureSize, token=token):
                pass
                # apertureComment = "Warning! Cannot change the aperture to .".format(newApertureSize)
            newApertureSize = collect.getApertureSize(beamline, token=token)
            apertureComment = apertureComment + "{0} um".format(newApertureSize)
            logger.info(apertureComment)
            if apertureComment != ispybApertureComment and firstImagePath is not None:
                ispyb.updateDataCollectionGroupComment(
                    beamline, firstImagePath, apertureComment
                )

            dictApertureExpTime = config.get_value(
                beamline=beamline,
                section_name="Beam",
                key_name="dictApertureExpTime",
                default_value=None,
            )
            if dictApertureExpTime is not None:
                if newApertureSize in dictApertureExpTime and not doFbest:
                    characterisationExposureTime = dictApertureExpTime[
                        str(newApertureSize)
                    ]
                    if doCharacterisation:
                        newExposureTimeComment = (
                            "New characterisation exposure time {0} s.".format(
                                characterisationExposureTime
                            )
                        )
                        logger.info(newExposureTimeComment)
                    if (
                        ispybExposureTimeComment != newExposureTimeComment
                        and firstImagePath is not None
                    ):
                        ispyb.updateDataCollectionGroupComment(
                            beamline, firstImagePath, newExposureTimeComment
                        )

    return {
        "newApertureSize": newApertureSize,
        "characterisationExposureTime": characterisationExposureTime,
        "ispybApertureComment": apertureComment,
        "ispybExposureTimeComment": newExposureTimeComment,
    }
