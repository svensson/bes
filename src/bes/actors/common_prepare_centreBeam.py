def run(beamline=None, **_):

    return {
        "workflow_title": "Centre beam on %s" % beamline,
        "workflow_type": "Undefined",
    }
