import time

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

EPSILON = 0.01


def run(beamline, directory, newMotorPositions, workflowParameters, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        newMotorPositions = common.jsonLoads(newMotorPositions)

        positionString = "Moving to new sample positions: "
        for motorName in newMotorPositions:
            if not newMotorPositions[motorName] is None:
                positionString += " %s=%.3f" % (motorName, newMotorPositions[motorName])
        logger.info(positionString)

        collect.moveMotors(beamline, directory, newMotorPositions, token=token)

        time.sleep(1)

        motorPositions = collect.readMotorPositions(beamline, token=token)
        #
        positionString = "Sample position after move:"
        for motorName in newMotorPositions:
            if not newMotorPositions[motorName] is None:
                positionString += " %s=%.3f" % (motorName, motorPositions[motorName])
        logger.info(positionString)
        if not beamline == "simulator":
            for motorName in newMotorPositions:
                if not newMotorPositions[motorName] is None:
                    if (
                        abs(newMotorPositions[motorName] - motorPositions[motorName])
                        > EPSILON
                    ):
                        raise Exception(
                            "Sample didn't move to new position! Motor: %s" % motorName
                        )
            logger.debug("Sample moved to the new position.")
        else:
            logger.info("Simulator: sample not moved to the new position")

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {"motorPositions": motorPositions}
