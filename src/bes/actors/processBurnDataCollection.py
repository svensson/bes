from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    mxv1ResultCharacterisationFile,
    workflow_working_dir,
    token=None,
    **_,
):

    try:
        returnDict = {}
        _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        run_number = int(run_number)

        mxv1ResultCharacterisation = path.readXSDataFile(mxv1ResultCharacterisationFile)

        dictResultRdfit = edna_mxv1.processBurnStrategy(
            beamline,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            mxv1ResultCharacterisation,
            workflow_working_dir,
        )

        returnDict = {
            "result_rdfit_beta": dictResultRdfit["result_rdfit_beta"],
            "result_rdfit_gamma": dictResultRdfit["result_rdfit_gamma"],
            "result_rdfit_dose_half_th": dictResultRdfit["result_rdfit_dose_half_th"],
            "result_rdfit_dose_half": dictResultRdfit["result_rdfit_dose_half"],
            "result_rdfit_relative_radiation_sensitivity": dictResultRdfit[
                "result_rdfit_relative_radiation_sensitivity"
            ],
            "result_rdfit_htmlPage": dictResultRdfit["result_rdfit_htmlPage"],
            "result_rdfit_jsonPath": dictResultRdfit["result_rdfit_jsonPath"],
            "result_rdfit_scaleIntensityPlot": dictResultRdfit[
                "result_rdfit_scaleIntensityPlot"
            ],
        }

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return returnDict
