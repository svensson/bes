"""
Created on Mar 3, 2015

@author: svensson
"""

import sys
import time
import traceback
from xmlrpc.client import ServerProxy


def run(callBackHost=None, callBackPort=None, sleep=None, **_):

    if callBackHost is None:
        raise RuntimeError("No callBackHost given as argument!")

    if callBackPort is None:
        raise RuntimeError("No callBackPort given as argument!")

    if sleep:
        time.sleep(sleep)

    noTries = 0

    # Try 10 times to do callback
    succes = False
    exc_info = None
    while noTries <= 10 and not succes:
        noTries += 1
        try:
            with ServerProxy(f"http://{callBackHost}:{callBackPort}") as proxy:
                proxy.callBack()
            succes = True
        except Exception:
            exc_info = sys.exc_info()
            time.sleep(0.1)

    if succes:
        testStatus = "ok"
    else:
        if exc_info:
            traceback.print_exception(*exc_info)
        testStatus = "failed"
    return {"testStatus": testStatus, "noTries": noTries}
