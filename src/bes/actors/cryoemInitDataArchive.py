"""
Initialisation of a workflow
"""

import os
import json

from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(beamline, directory, REFID=None, REQUESTID=None, **_):

    logger = None

    workflowTitle = "CryoEM Data Archive"
    workflowType = "CryoEMDataArchive"

    workflowParameters = {
        "title": workflowTitle,
        "type": workflowType,
    }

    # Create the working directory with respect to the image directory
    workflowWorkingDir = path.createWorkflowWorkingDirectory(directory)

    workflowParameters["workingDir"] = workflowWorkingDir

    # Create stack trace file
    workflowStackTraceFile = path.createStackTraceFile(workflowWorkingDir)
    workflowParameters["stackTraceFile"] = workflowStackTraceFile

    workflowLogFile = path.createWorkflowLogFilePath(workflowWorkingDir)
    workflowDebugLogFile = path.createWorkflowDebugLogFilePath(workflowWorkingDir)

    workflowParameters["logFile"] = workflowLogFile
    workflowParameters["debugLogFile"] = workflowDebugLogFile
    workflowParameters["pyarchLogFile"] = None

    # Then set up log file
    logger = workflow_logging.getLogger(
        beamline=None, workflowParameters=workflowParameters
    )
    logger.info("*********** cryoemInitDataArchive **********")

    # processDirectory
    processDirectory = directory.replace("RAW_DATA", "PROCESSED_DATA")

    returnDict = {
        "workflow_working_dir": workflowWorkingDir,
        "workflowLogFile": workflowLogFile,
        "workflowDebugLogFile": workflowDebugLogFile,
        "directory": directory,
        "processDirectory": processDirectory,
        "workflowParameters": workflowParameters,
    }

    # Try to create pyarchdir
    workflowPyarchLogFile = None
    pyarch_html_dir = None
    tmpPyarch = path.createPyarchFilePath(workflowWorkingDir)
    if tmpPyarch is not None:
        pyarchDir = tmpPyarch
        workflowPyarchLogFile = os.path.join(
            pyarchDir, os.path.basename(workflowLogFile)
        )
        pyarch_html_dir = os.path.join(pyarchDir, "html")
        workflowParameters["pyarchLogFile"] = workflowPyarchLogFile
        logger = workflow_logging.getLogger(None, workflowParameters)
        logger.debug("pyarchDir: %s" % pyarchDir)
        logger.debug("workflowPyarchLogFile: %s" % workflowPyarchLogFile)
        logger.debug("pyarch_html_dir: %s" % pyarch_html_dir)
        returnDict["pyarchDir"] = pyarchDir
        returnDict["workflowParameters"] = workflowParameters
        returnDict["pyarch_html_dir"] = pyarch_html_dir
        returnDict["workflowPyarchLogFile"] = workflowPyarchLogFile
    workflowParameters["workflowPyarchLogFile"] = workflowPyarchLogFile
    workflowParameters["pyarch_html_dir"] = pyarch_html_dir
    workflowParameters["workflowWorkingDir"] = workflowWorkingDir
    workflowParameters["workflowLogFile"] = workflowLogFile

    # Initialise ispyb
    workflow_id = edna_ispyb.storeOrUpdateWorkflow(
        beamline,
        workflowTitle=workflowParameters["title"],
        workflowType=workflowParameters["type"],
        workflowPyarchLogFile=workflowParameters["workflowPyarchLogFile"],
        workflowLogFile=workflowParameters["workflowLogFile"],
        pyarchHtmlDir=workflowParameters["pyarch_html_dir"],
        workflowWorkingDir=workflowParameters["workflowWorkingDir"],
    )
    workflowParameters["ispyb_id"] = workflow_id
    returnDict["workflow_id"] = workflow_id
    returnDict["workflowParameters"] = workflowParameters

    # Initialise log file
    logger.info("Starting new workflow '%s'" % workflowTitle)
    if REQUESTID is not None:
        logger.info(
            "Beamline Expert System REFID=%r, REQUESTID=%r" % (REFID, REQUESTID)
        )
    logger.info("Image directory: %s" % directory)
    logger.debug("Workflow type: %s" % workflowType)
    logger.debug("Workflow working directory: %r" % workflowWorkingDir)
    logger.debug("ISPyB workflow id: %r" % workflow_id)

    common.send_email(
        beamline,
        [
            "svensson@esrf.fr",
        ],
        "Started",
        directory,
        workflowTitle,
        workflowType,
        workflowWorkingDir,
    )

    # For debugging:
    processDirectory = "/data/visitor/mx415/cm01/20171123/PROCESSED_DATA"
    returnDict["processDirectory"] = processDirectory

    dictPath = path.getDictPath(directory)
    if "beamline" in dictPath:
        beamline = dictPath["beamline"]
    else:
        beamline = "unknown-beamline"
    if "proposal" in dictPath:
        proposal = dictPath["proposal"]
    else:
        proposal = "unknown-proposal"
    gridSquaresJsonFile = os.path.join(
        processDirectory, "gridSquareArchive_{0}_{1}.json".format(beamline, proposal)
    )
    if os.path.exists(gridSquaresJsonFile):
        logger.info(
            "Using existing grid square json file: {0}".format(gridSquaresJsonFile)
        )
    else:
        logger.info(
            "Creating new grid square json file: {0}".format(gridSquaresJsonFile)
        )
        dictGridSquares = {}
        open(gridSquaresJsonFile, "w").write(json.dumps(dictGridSquares, indent=4))
    returnDict["gridSquaresJsonFile"] = gridSquaresJsonFile

    # Debugging
    returnDict["maxNoGridSquares"] = 1
    returnDict["maxNoMoviesToArchive"] = 2
    returnDict["noGridSquaresArchived"] = 0
    returnDict["noMoviesArchived"] = 0

    return returnDict
