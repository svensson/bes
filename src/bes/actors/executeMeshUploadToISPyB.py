#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

import os
import json

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging
from bes.workflow_lib import MeshBest_v9_4 as MeshBest


def run(
    beamline,
    directory,
    meshPositionFile,
    run_number,
    expTypePrefix,
    workflow_working_dir,
    prefix,
    suffix,
    workflow_id,
    grid_info,
    workflow_title,
    workflow_type,
    firstImagePath,
    bestPosition,
    workflowParameters,
    list_node_id=None,
    pyarch_html_dir=None,
    snapShotFilePath=None,
    titleMeshHtmlPage="Results",
    meshHtmlDirectoryName="",
    diffractionSignalDetection="DozorM2 (macro-molecules crystal detection)",
    remainingPositions=None,
    resultMeshPath=None,
    workflowStepType=None,
    meshBestPlotPath=None,
    meshBestPositions=None,
    token=None,
    **_,
):

    try:

        workflowParameters = common.jsonLoads(workflowParameters)
        meshBestPositions = common.jsonLoads(meshBestPositions)
        workflowId = workflowParameters["ispyb_id"]
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        bestPosition = common.jsonLoads(bestPosition)
        meshPositions = path.parseJsonFile(meshPositionFile)
        grid_info = common.jsonLoads(grid_info)
        remainingPositions = common.jsonLoads(remainingPositions)

        #        for position in meshPositions:
        #            strPos = "x=%2d y=%2d " % (position["indexY"] + 1, position["indexZ"] + 1)
        #            strPos += "%s " % position["imageName"]
        #            strPos += "sampx=%.3f " % position["sampx"]
        #            strPos += "sampy=%.3f " % position["sampy"]
        #            if "phiy" in position:
        #                strPos += "phiy=%.3f " % position["phiy"]
        #            if "phiz" in position:
        #                strPos += "phiz=%.3f " % position["phiz"]
        #            if "totalIntegratedSignal" in position:
        #                strPos += "totIntSign=%.3g " % position["totalIntegratedSignal"]
        #            if "dozor_score" in position:
        #                strPos += "dozor_score=%.3f " % position["dozor_score"]
        #            if "spaceGroup" in position:
        #                strPos += "spaceGroup=%s " % position["spaceGroup"]
        #            logger.info(strPos)
        # logger.debug(json.dumps(meshPositions, indent=4))

        #
        isHorizontalRotationAxis = True
        #     best_position = grid.find_best_position(grid_info, meshPositions,
        #                                             isHorizontalRotationAxis=isHorizontalRotationAxis)
        #     dictBeamSize = edna_ispyb.getMeshBeamsize(beamline, directory, firstImagePath, workflow_working_dir)
        #     beamSizeAtSampleX = dictBeamSize["beamSizeAtSampleX"]
        #     beamSizeAtSampleY = dictBeamSize["beamSizeAtSampleY"]

        plotPath = None
        proposalName, proposalNumber = path.getProposalNameNumber(directory)
        proposal = proposalName + str(proposalNumber)
        # Run mesh clustering only if opid231@id23eh1 or mx415@id30a2
        if meshBestPlotPath is not None:
            plotPath = meshBestPlotPath
        elif (
            beamline in ["id23eh1", "id30a2"]
            and proposal in ["opid231", "mx415"]
            and resultMeshPath is not None
            and grid_info["steps_x"] != 1
        ):
            try:
                dozor2XrayCenteringDir = os.path.join(workflow_working_dir, "MeshBest")
                os.mkdir(dozor2XrayCenteringDir, 0o755)
                os.chdir(dozor2XrayCenteringDir)
                logger.info("Execution of MeshBest started.")
                logger.debug("resultMeshPath: {0}".format(resultMeshPath))
                plotFileName = "MeshBest.png"
                plotPath = os.path.join(dozor2XrayCenteringDir, plotFileName)
                MeshBest.main(resultMeshPath, plotPath)
                logger.info("MeshBest executed: {0}.".format(plotPath))
            except BaseException as e:
                logger.info("Execution of MeshBest failed.")
                raise
                logger.info(e)

        listPositions = [bestPosition]
        if remainingPositions is not None and grid_info["steps_x"] != 1:
            listPositions += remainingPositions
        dict_html = grid.grid_create_result_html_page(
            beamline,
            grid_info,
            meshPositions,
            listPositions,
            directory,
            workflow_working_dir,
            isHorizontalRotationAxis=isHorizontalRotationAxis,
            title=titleMeshHtmlPage,
            diffractionSignalDetection=diffractionSignalDetection,
            meshBestPlotPath=meshBestPlotPath,
            meshBestPositions=meshBestPositions,
        )
        dict_html_pyarch = {}
        if list_node_id is not None:
            list_node_id = common.jsonLoads(list_node_id)
            html_page_path = os.path.join(
                dict_html["html_directory_path"], dict_html["index_file_name"]
            )
            if os.path.exists(html_page_path):
                collect.displayNewHtmlPage(
                    beamline,
                    html_page_path,
                    "Grid results",
                    list_node_id,
                    strPageNumber=run_number,
                    token=token,
                )
                if pyarch_html_dir is not None:
                    #                    if meshHtmlDirectoryName == "":
                    if expTypePrefix == "mesh-":
                        meshHtmlDirectoryName = "Mesh"
                    elif expTypePrefix == "line-":
                        meshHtmlDirectoryName = "Line"
                    else:
                        meshHtmlDirectoryName = "Html"
                    dict_html_pyarch = path.copyHtmlDirectoryToPyarch(
                        pyarch_html_dir, dict_html, meshHtmlDirectoryName
                    )
                    if workflowStepType is None:
                        workflowStepType = meshHtmlDirectoryName
                    status = "Success"
                    imageResultFilePath = os.path.join(
                        dict_html_pyarch["html_directory_path"],
                        dict_html_pyarch["plot_file_name"],
                    )
                    htmlResultFilePath = os.path.join(
                        dict_html_pyarch["html_directory_path"],
                        dict_html_pyarch["index_file_name"],
                    )
                    resultFilePath = os.path.join(
                        dict_html_pyarch["html_directory_path"], "result.json"
                    )
                    open(resultFilePath, "w").write(
                        json.dumps(dict_html["workflowStep"])
                    )
                    ispyb.storeWorkflowStep(
                        beamline,
                        workflowId,
                        workflowStepType,
                        status,
                        imageResultFilePath=imageResultFilePath,
                        htmlResultFilePath=htmlResultFilePath,
                        resultFilePath=resultFilePath,
                    )

        dict_statistics = grid.mesh_scan_statistics(
            meshPositions, workingDir=workflow_working_dir
        )
        if expTypePrefix == "mesh-":
            emailList = config.get_value(
                beamline, "Email", "meshStatistics", default_value=[]
            )
            common.send_email(
                beamline,
                emailList,
                "Mesh statistics",
                directory,
                workflow_title,
                workflow_type,
                workflow_working_dir,
                str_message=dict_statistics["message"],
            )
        logger.debug("Uploading data collection parameters to ISPyB...")
        iDataCollectionGroupId = edna_ispyb.updateDataCollectionGroupWorkflowId(
            beamline,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            firstImagePath,
            workflow_id,
            workflow_working_dir,
        )
        edna_ispyb.groupMeshDataCollections(
            beamline,
            directory,
            meshPositions,
            workflow_working_dir,
            iDataCollectionGroupId,
        )

        logger.debug("ISPyB uploading finished.")

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {"dict_html_pyarch": dict_html_pyarch, "Done": True}
