#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging
from bes.workflow_lib import grid
from bes.workflow_lib import path


def run(
    beamline,
    workflowParameters,
    directory,
    meshPositionFile,
    workflow_id,
    grid_info,
    workflow_working_dir,
    workflow_title,
    workflow_type,
    bestPosition,
    allPositionFile,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    firstImagePath,
    dict_html_pyarch,
    pyarch_html_dir=None,
    list_node_id=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        bestPosition = common.jsonLoads(bestPosition)
        _ = path.parseJsonFile(allPositionFile)
        meshPositions = path.parseJsonFile(meshPositionFile)
        grid_info = common.jsonLoads(grid_info)
        dict_html_pyarch = common.jsonLoads(dict_html_pyarch)
        logger.debug("Uploading data collection parameters to ISPyB...")
        dict_statistics = grid.mesh_scan_statistics(
            meshPositions, workingDir=workflow_working_dir
        )
        emailList = config.get_value(
            beamline, "Email", "meshStatistics", default_value=[]
        )
        common.send_email(
            beamline,
            emailList,
            "Mesh statistics",
            directory,
            workflow_title,
            workflow_type,
            workflow_working_dir,
            str_message=dict_statistics["message"],
        )

        iDataCollectionGroupId = edna_ispyb.updateDataCollectionGroupWorkflowId(
            beamline,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            firstImagePath,
            workflow_id,
            workflow_working_dir,
        )
        edna_ispyb.groupMeshDataCollections(
            beamline,
            directory,
            meshPositions,
            workflow_working_dir,
            iDataCollectionGroupId,
        )

        logger.debug("ISPyB uploading finished.")

        # BES-251 : Remove call to store_positions
        # ispyb_positions = grid.find_ispyb_positions(meshPositions)
        # list_image_creation = edna_ispyb.store_positions(beamline, directory, ispyb_positions, workflow_working_dir)
        # best_image_id = grid.find_best_image_id(meshPositions, list_image_creation, bestPosition)
        # logger.debug("Starting creation of image thumbnails...")
        # edna_mxv1.create_thumbnails_for_pyarch(list_image_creation, workflow_working_dir)
        # logger.debug("Image thumbnails created...")

    except Exception:
        common.logStackTrace(workflowParameters)
        raise
    return {}
