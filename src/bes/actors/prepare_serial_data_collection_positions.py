import os

from bes.workflow_lib import grid
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    process_directory,
    workflow_type,
    osc_range,
    phi,
    kappa,
    kappa_phi,
    sampx,
    sampy,
    phix,
    phiy,
    phiz,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    gridExposureTime,
    transmission,
    grid_info=None,
    meshZigZag=False,
    token=None,
    shape=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        if beamline != "simulator":
            grid_info = collect.getGridInfo(beamline, token=token, shape=shape)

        message_text = ""
        meshPositions = []
        meshQueueList = []
        newExposureTimeMessage = ""
        hasNewExposureTime = False
        firstImagePath = None

        if grid_info == {}:
            message_text = "No grid obtained from mxCuBE! Please draw a grid and restart the workflow."
        elif grid_info["steps_x"] == 0:
            message_text = "Horizontal grid dimension is zero! Please redraw a valid grid and restart the workflow."
            grid_info = {}
        elif grid_info["steps_y"] == 0:
            message_text = "Vertical grid dimension is zero! Please redraw a valid grid and restart the workflow."
            grid_info = {}
        else:
            if workflow_type == "LineScan":
                # Make grid 1D:
                grid_info["steps_y"] = 1
                logger.info("1D mesh requested, forcing grid ny to 1.")

            # Mesh positions:
            meshPositions = grid.determineMeshPositions(
                beamline,
                phi,
                kappa,
                kappa_phi,
                sampx,
                sampy,
                phix,
                phiy,
                phiz,
                run_number,
                expTypePrefix,
                prefix,
                suffix,
                grid_info,
                meshZigZag,
                multi_wedge=True,
            )

            firstImagePath = None
            for position in meshPositions:
                if position["index"] == 0:
                    firstImagePath = os.path.join(directory, position["imageName"])
                    logger.info("First image path %s" % firstImagePath)
                    break

            message_text = ""
            newExposureTimeMessage = ""
            hasNewExposureTime = False

            phi = phi - osc_range / 2.0
            logger.info("Starting phi for helical scan: %.1f" % phi)

            meshQueueList = collect.createSerialDataCollectionQueueList(
                beamline,
                directory,
                process_directory,
                run_number,
                expTypePrefix,
                prefix,
                meshPositions,
                phi,
                gridExposureTime,
                osc_range,
                transmission,
                grid_info,
                meshZigZag,
            )
            if beamline != "bm14":
                dictNewParameters = collect.checkMotorSpeed(
                    beamline, meshQueueList, gridExposureTime, transmission
                )
                if dictNewParameters is not None:
                    hasNewExposureTime = False
                    gridExposureTime = dictNewParameters["gridExposureTime"]
                    transmission = dictNewParameters["transmission"]
                    meshQueueList = collect.createSerialDataCollectionQueueList(
                        beamline,
                        directory,
                        process_directory,
                        run_number,
                        expTypePrefix,
                        prefix,
                        meshPositions,
                        phi,
                        gridExposureTime,
                        osc_range,
                        transmission,
                        grid_info,
                        meshZigZag,
                    )
                    newExposureTimeMessage = (
                        "In order to perform the scan with optimal motor speed\n"
                    )
                    newExposureTimeMessage += (
                        "the exposure time has been changed to %.3f s\n"
                        % gridExposureTime
                    )
                    newExposureTimeMessage += (
                        "and the transmission changed accordingly to %.1f %%.\n"
                        % transmission
                    )
                    #                 newExposureTimeMessage += "You can change these parameters but be aware that decreasing\n"
                    #                 newExposureTimeMessage += "the exposure time might lead to inaccuracy of position determination."
                    logger.info(newExposureTimeMessage)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "directory": directory,
        "grid_info": grid_info,
        "shape": None,
        "meshPositions": meshPositions,
        "meshQueueList": meshQueueList,
        "hasNewExposureTime": hasNewExposureTime,
        "newExposureTimeMessage": newExposureTimeMessage,
        "message_text": message_text,
        "firstImagePath": firstImagePath,
    }
