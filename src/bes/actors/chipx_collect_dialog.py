from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    collection_software,
    workflowParameters,
    transmission=100,
    no_lines=7,
    exposureTime=0.1,
    oscillationWidth=0.1,
    resolution=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(
        beamline, workflowParameters=workflowParameters, token=token
    )

    if resolution is None:
        resolution = collect.getResolution(beamline, token)

    aperture_names = collect.getApertureNames(beamline, token)

    listDialog = [
        {
            "variableName": "aperture",
            "label": "Aperture",
            "type": "combo",
            "value": aperture_names[0],
            "textChoices": aperture_names,
        },
        {
            "variableName": "exposureTime",
            "label": "Exposure Time",
            "type": "float",
            "value": exposureTime,
        },
        {
            "variableName": "oscillationWidth",
            "label": "Oscillation width per image",
            "type": "float",
            "value": oscillationWidth,
        },
        {
            "variableName": "no_lines",
            "label": "Number of lines",
            "type": "int",
            "value": no_lines,
        },
        {
            "variableName": "resolution",
            "label": "Resolution (A)",
            "type": "float",
            "value": resolution,
        },
        {
            "variableName": "transmission",
            "label": "Transmission (%)",
            "type": "float",
            "value": transmission,
        },
    ]

    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=False,
        collection_software=collection_software,
    )

    return dictValues
