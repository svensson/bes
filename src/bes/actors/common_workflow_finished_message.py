from bes.workflow_lib import workflow_logging


def run(message_text="message_text", **_):
    logger = workflow_logging.getLogger()

    if message_text == "":
        logger.error("Workflow finished with errors")
    else:
        logger.error("Workflow finished with error message: %s" % message_text)

    return {}
