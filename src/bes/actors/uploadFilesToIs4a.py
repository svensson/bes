from bes.workflow_lib import is4a
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(workflowParameters, is4aId, is4aUrl, jobInput, is4aStatus, dictOutputFile, **_):

    dictOutputFile = common.jsonLoads(dictOutputFile)
    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger()

    # Add report
    dictJobInfo = {
        "is4aId": is4aId,
        "is4aUrl": is4aUrl,
        "is4aInput": jobInput,
        "is4aStatus": is4aStatus,
        "type": workflowParameters["type"],
    }

    dictOutputFile["report"] = is4a.createResultJson(dictJobInfo)

    # Upload results
    is4a.uploadResults(is4aUrl, is4aId, dictOutputFile)

    logger.info("Results uploaded to is4a server")

    return {"dictOutputFile": dictOutputFile}
