from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    osc_range,
    characterisationExposureTime,
    transmission,
    automatic_mode=False,
    resolution=None,
    collection_software=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    characterisationExposureTime = float(characterisationExposureTime)
    osc_range = float(osc_range)
    transmission = float(transmission)

    if resolution is None:
        resolution = collect.getResolution(beamline, token=token)
    else:
        resolution = float(resolution)

    listSpaceGroups = symmetry.getListOfChiralSpaceGroups()
    listSpaceGroups.insert(0, "None")

    listDialog = [
        {
            "variableName": "forcedSpaceGroup",
            "label": "Forced space group",
            "type": "combo",
            "defaultValue": "None",
            "textChoices": listSpaceGroups,
        },
        {
            "variableName": "complexity",
            "label": "Best strategy complexity",
            "type": "combo",
            "defaultValue": "none",
            "textChoices": ["none", "min", "full"],
        },
        {
            "variableName": "no_reference_images",
            "label": "Number of reference images",
            "type": "int",
            "defaultValue": 2,
            "unit": "",
            "lowerBound": 0.0,
            "upperBound": 4.0,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "combo",
            "defaultValue": "90",
            "textChoices": ["30", "45", "60", "90"],
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": characterisationExposureTime,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "osc_range",
            "label": "Total oscillation range",
            "type": "float",
            "value": osc_range,
            "unit": "%",
            "lowerBound": 0.1,
            "upperBound": 10.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "value": transmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "resolution",
            "label": "Resolution",
            "type": "float",
            "defaultValue": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 7.0,
        },
        {
            "variableName": "apertureSize",
            "label": "Aperture size",
            "type": "float",
            "defaultValue": 0.1,
            "unit": "mm",
            "lowerBound": 0.01,
            "upperBound": 7.0,
        },
        {
            "variableName": "beamShift",
            "label": "Beam shift",
            "type": "float",
            "defaultValue": 0.0,
            "unit": "mm",
            "lowerBound": 0.0,
            "upperBound": 7.0,
        },
        {
            "variableName": "omegaMin",
            "label": "Omega for min crystal size",
            "type": "float",
            "defaultValue": 0.0,
            "unit": "mm",
            "lowerBound": 0.0,
            "upperBound": 360.0,
        },
        {
            "variableName": "crystalSizeMaxVertical",
            "label": "Max vertical crystal size",
            "type": "float",
            "defaultValue": 0.05,
            "unit": "mm",
            "lowerBound": 0.0,
            "upperBound": 7.0,
        },
        {
            "variableName": "crystalSizeMinVertical",
            "label": "Min vertical crystal size",
            "type": "float",
            "defaultValue": 0.05,
            "unit": "mm",
            "lowerBound": 0.0,
            "upperBound": 7.0,
        },
        {
            "variableName": "crystalSizeHorizontal",
            "label": "Horizontal crystal size",
            "type": "float",
            "defaultValue": 0.05,
            "unit": "mm",
            "lowerBound": 0.0,
            "upperBound": 7.0,
        },
        {
            "variableName": "doseLimit",
            "label": "Dose limit",
            "type": "float",
            "defaultValue": 500000.0,
            "unit": "Gy",
            "lowerBound": 0.0,
            "upperBound": 1.0e12,
        },
        {
            "variableName": "rFriedel",
            "label": "R Friedel",
            "type": "float",
            "defaultValue": 0.1,
            "lowerBound": 0.0,
            "upperBound": 1.0e3,
        },
        {
            "variableName": "noDCPositions",
            "label": "Number of data collection positions",
            "type": "int",
            "defaultValue": 4,
            "lowerBound": 1,
            "upperBound": 100,
        },
        {
            "variableName": "deltaKappa",
            "label": "Delta kappa movement",
            "type": "float",
            "defaultValue": 10.0,
            "lowerBound": 0.0,
            "upperBound": 120.0,
        },
        {
            "variableName": "deltaPhi",
            "label": "Delta phi movement",
            "type": "float",
            "defaultValue": 20.0,
            "lowerBound": 0.0,
            "upperBound": 360.0,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    if dictValues["noDCPositions"] == 1:
        dictValues["continueDataCollection"] = False
    else:
        dictValues["continueDataCollection"] = True

    return dictValues
