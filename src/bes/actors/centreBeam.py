from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, directory, **_):

    try:
        logger = workflow_logging.getLogger(beamline, workflowParameters)
        logger.warning("Starting beam centre - this operation takes about 40 s")
        collect.centreBeam(beamline)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
