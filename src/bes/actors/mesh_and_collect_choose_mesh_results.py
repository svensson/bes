import os
import time
from bes.workflow_lib import workflow_logging, dialog, common, collect, grid, path


def run(
    beamline,
    directory,
    workflowParameters,
    raw_directory=None,
    collection_software=None,
    token=None,
    **_,
):
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    workflowParameters = common.jsonLoads(workflowParameters)

    if raw_directory is None:
        raw_directory = directory
    listMeshResult = grid.findMeshResults(raw_directory)[::-1]

    if len(listMeshResult) == 0:
        raise RuntimeError(
            f"No mesh results found for sample {os.path.dirname(raw_directory)}!"
        )

    listDialog = [
        {
            "variableName": "meshPositionFile",
            "label": "Mesh position file",
            "type": "combo",
            "textChoices": listMeshResult,
            "value": listMeshResult[0],
        }
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        collection_software=collection_software,
    )

    meshResults = grid.readMeshResults(raw_directory, dictValues["meshPositionFile"])
    meshPositionFile = path.createNewJsonFile(
        workflowParameters["workingDir"],
        meshResults["meshPositions"],
        "meshPositionsAxisCentring",
    )
    firstImagePath = os.path.join(
        meshResults["directory"], meshResults["meshPositions"][0]["imageName"]
    )
    del meshResults["meshPositions"]
    dictValues.update(meshResults)
    dictValues["meshPositionFile"] = meshPositionFile
    dictValues["firstImagePath"] = firstImagePath
    logger.info(
        "Moving spindle axis to original mesh position: {0}".format(
            meshResults["meshPhiPosition"]
        )
    )
    collect.moveMotors(
        beamline, directory, {"phi": meshResults["meshPhiPosition"]}, token=token
    )
    time.sleep(1)
    dictValues["motorPositions"] = collect.readMotorPositions(beamline, token=token)

    return dictValues
