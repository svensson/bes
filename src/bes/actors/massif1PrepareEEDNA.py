from bes import config


def run(beamline, do_data_collect=True, **_):

    if do_data_collect:  # and beamline != "id30b":
        snapShots = False
    else:
        snapShots = True

    return {
        "expTypePrefix": "ref-",
        "doRefDataCollectionReview": False,
        "automatic_mode": True,
        "no_reference_images": 4,
        "snapShots": snapShots,
        "characterisationOscillationRange": config.get_value(
            beamline, "Goniometer", "defaultCharacterisationOscillationRange"
        ),
    }
