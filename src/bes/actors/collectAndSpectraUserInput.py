from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    motorPositions,
    collection_software=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    motorPositions = common.jsonLoads(motorPositions)
    data_collection_start_phi = motorPositions["phi"]
    spectrum_start_phi = data_collection_start_phi

    resolution = collect.getResolution(beamline, token=token)

    listDialog = [
        {
            "variableName": "noCycles",
            "label": "Number of cycles",
            "type": "int",
            "value": 10,
            "lowerBound": 1,
            "upperBound": 1000,
        },
        {
            "variableName": "osc_range",
            "label": "Oscillation range",
            "type": "float",
            "value": 0.1,
            "unit": "degree",
            "lowerBound": 0.05,
            "upperBound": 10.0,
        },
        {
            "variableName": "numberOfImages",
            "label": "Number of images",
            "type": "int",
            "value": 1800,
            "lowerBound": 1,
            "upperBound": 10000,
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": 0.05,
            "lowerBound": 0.004,
            "upperBound": 100.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "value": 100.0,
            "lowerBound": 0.01,
            "upperBound": 100.0,
        },
        {
            "variableName": "resolution",
            "label": "Resolution",
            "type": "float",
            "value": resolution,
            "lowerBound": 0.1,
            "upperBound": 100.0,
        },
        {
            "variableName": "data_collection_start_phi",
            "label": "Data collection start phi",
            "type": "float",
            "value": data_collection_start_phi,
            "lowerBound": -720,
            "upperBound": 720,
        },
        {
            "variableName": "spectrum_start_phi",
            "label": "Spectra acquisition start phi",
            "type": "float",
            "value": spectrum_start_phi,
            "lowerBound": -720,
            "upperBound": 720,
        },
        {
            "variableName": "spectralAcquisitionTime",
            "label": "Spectral acquisition time",
            "type": "float",
            "value": 100.0,
            "unit": "ms",
            "lowerBound": 1,
            "upperBound": 10000,
        },
        {
            "variableName": "spectralAverages",
            "label": "Spectral averages",
            "type": "int",
            "value": 1,
            "lowerBound": 1,
            "upperBound": 10000,
        },
    ]

    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        collection_software=collection_software,
    )

    return dictValues
