def run(beamline, **_):
    return {
        "workflow_title": "ChipX left position on %s" % beamline,
        "workflow_type": "ChipX_left_position",
    }
