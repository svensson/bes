import os

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    run_number,
    process_directory,
    prefix,
    suffix,
    gridOscillationRange,
    sampx2,
    sampy2,
    phiy2,
    phiz2,
    gridExposureTime,
    beamSizeAtSampleX=0.050,
    beamSizeAtSampleY=0.050,
    gridTransmission=None,
    token=None,
    **_,
):
    # prepareFirstVerticalXrayCentring gets the default beamSizeAtSampleY
    # Then executeMeshCalculateBestPosition sets it to 0.010 in the case we tested at ID30b
    # which leads to a scan that is too short.
    beamSizeAtSampleY = 0.050

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    run_number = run_number + 1

    # Save position in mxCuBE
    collect.saveCurrentPos(beamline, token=token)

    # Save current position
    motorPositions = collect.readMotorPositions(beamline, token=token)
    sampx1 = motorPositions["sampx"]
    sampy1 = motorPositions["sampy"]
    phiy1 = motorPositions["phiy"]
    phiz1 = motorPositions["phiz"]

    # Move to second position
    if beamline == "id23eh2":
        collect.moveMotors(
            beamline,
            directory,
            {"sampx": sampx2, "sampy": sampy2, "phiz": phiz2},
            token=token,
        )
    else:
        collect.moveMotors(
            beamline,
            directory,
            {"sampx": sampx2, "sampy": sampy2, "phiy": phiy2},
            token=token,
        )

    motorPositions = collect.readMotorPositions(beamline, token=token)

    # 1D vertical scan
    logger.info("Preparing vertical line scan")

    if beamline == "id23eh2":
        beamSizeAtSampleX = 0.010
        noSteps = 60
        deltaX = beamSizeAtSampleX * noSteps / 4.0
        grid_info = {
            "x1": -deltaX / 2.0,
            "y1": 0.0,
            "dx_mm": deltaX,
            "dy_mm": 0.1,
            "steps_x": noSteps,
            "steps_y": 1,
            "angle": 0.0,
        }
    else:
        if beamline == "id30a2":
            noSteps = 40
            deltaY = beamSizeAtSampleY * noSteps / 4.0
        elif beamline == "id30a3":
            beamSizeAtSampleY = 0.015
            noSteps = 60
            deltaY = beamSizeAtSampleY * noSteps / 4.0
        else:
            noSteps = 60
            deltaY = beamSizeAtSampleY * noSteps / 4.0

        grid_info = {
            "x1": 0.0,
            "y1": -deltaY / 2.0,
            "dx_mm": 0.1,
            "dy_mm": deltaY,
            "steps_x": 1,
            "steps_y": noSteps,
            "angle": 0.0,
        }

    expTypePrefix = "line-"

    if gridTransmission is None:
        transmission = config.get_value(beamline, "Beam", "defaultGridTransmission")
    else:
        transmission = float(gridTransmission)

    meshPositions = grid.determineMeshPositions(
        beamline,
        motorPositions,
        gridOscillationRange,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
    )

    firstImagePath = None
    for position in meshPositions:
        if position["index"] == 0:
            firstImagePath = os.path.join(directory, position["imageName"])
            logger.info("First image path %s" % firstImagePath)
            break

    meshQueueList = collect.createHelicalQueueList(
        beamline,
        directory,
        process_directory,
        run_number,
        expTypePrefix,
        prefix,
        meshPositions,
        motorPositions,
        gridExposureTime,
        gridOscillationRange,
        transmission,
        grid_info,
    )

    if beamline != "bm14":
        dictNewParameters = collect.checkMotorSpeed(
            beamline, meshQueueList, gridExposureTime, transmission
        )
        if dictNewParameters is not None:
            gridExposureTime = dictNewParameters["gridExposureTime"]
            transmission = dictNewParameters["transmission"]
            meshQueueList = collect.createHelicalQueueList(
                beamline,
                directory,
                process_directory,
                run_number,
                expTypePrefix,
                prefix,
                meshPositions,
                motorPositions,
                gridExposureTime,
                gridOscillationRange,
                transmission,
                grid_info,
            )
            newExposureTimeMessage = (
                "In order to perform the scan with optimal motor speed\n"
            )
            newExposureTimeMessage += (
                "the exposure time has been changed to %.3f s\n" % gridExposureTime
            )
            newExposureTimeMessage += (
                "and the transmission changed accordingly to %.1f %%.\n" % transmission
            )
            logger.info(newExposureTimeMessage)

    meshPositionFile = path.createNewJsonFile(
        workflowParameters["workingDir"], meshPositions, "meshPositions90degrees"
    )
    meshQueueListFile = path.createNewJsonFile(
        workflowParameters["workingDir"], meshQueueList, "meshQueueList90degrees"
    )

    return {
        "grid_info": grid_info,
        "shape": None,
        "meshPositionFile": meshPositionFile,
        "expTypePrefix": expTypePrefix,
        "meshQueueListFile": meshQueueListFile,
        "firstImagePath": firstImagePath,
        "sampx1": sampx1,
        "sampy1": sampy1,
        "phiy1": phiy1,
        "phiz1": phiz1,
        "directory": directory,
        "run_number": run_number,
    }
