from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(beamline, directory, prefix, token=None, **kwargs):

    logger = workflow_logging.getLogger(beamline, token=token)

    workflow_parameters = {}
    workflow_parameters["title"] = (
        "MXPressR_180: no auto-mesh, X-centre, fixed dc on %s" % beamline
    )
    workflow_parameters["type"] = "MXPressR_180"

    try:
        prefix = collect.getCurrentCdCrystalId(beamline, token=token)
        logger.warning("Prefix set to: {0}".format(prefix))
    except Exception:
        logger.error("Cannot read crystal id! Prefix set to: {0}".format(prefix))

    parameters = path.getDefaultParameters(directory, file_prefix="mxpressr")
    target_flux = parameters.get("targetFlux", 5e10)
    aimed_resolution = parameters.get("aimedResolution", 2.0)
    number_of_positions = parameters.get("numberOfPositions", 1)
    preferred_aperture = parameters.get(
        "preferredApertureName", aperture_utils.aperture_size_to_name(beamline, 30)
    )
    mxpresso_osc_range = parameters.get("mxpressoOscRange", 0.2)
    mxpresso_no_images = parameters.get("mxpressoNoImages", 900)
    grid_transmission = parameters.get("gridTransmission", 50)
    grid_size = parameters.get("gridSize", 300) / 1000.0  # mm
    grid_steps = parameters.get("gridSteps", 10)
    diffraction_signal_detection = parameters.get(
        "diffractionSignalDetection", "DozorM2 (macro-molecules crystal detection)"
    )
    forced_space_group = parameters.get("forcedSpaceGroup", None)

    grid_info = {
        "x1": -grid_size / 2,
        "y1": -grid_size / 2,
        "dx_mm": grid_size,
        "dy_mm": grid_size,
        "steps_x": grid_steps,
        "steps_y": grid_steps,
        "angle": 0.0,
    }

    return {
        "workflow_title": workflow_parameters["title"],
        "workflow_type": workflow_parameters["type"],
        "workflowParameters": workflow_parameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "grid_info": grid_info,
        "automatic_mode": True,
        "doAutoMesh": False,
        "prefix": prefix,
        "anomalousData": False,
        "doCharacterisation": False,
        "checkForFlux": False,
        "collectTransmission": 10,
        "collectExposureTime": 0.02,
        "gridTransmission": grid_transmission,
        "preferredApertureName": preferred_aperture,
        "targetFlux": target_flux,
        "aimedResolution": aimed_resolution,
        "numberOfPositions": number_of_positions,
        "mxpressoOscRange": mxpresso_osc_range,
        "mxpressoNoImages": mxpresso_no_images,
        "diffractionSignalDetection": diffraction_signal_detection,
        "forcedSpaceGroup": forced_space_group,
    }
