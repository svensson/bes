import os

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import dehydration
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    dehydrationResults,
    list_node_id,
    mxv1StrategyResultFile,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    startTime,
    currentRH,
    targetRH,
    continue_dehydration,
    diffractionSignalDetection="Dozor (macro-molecules)",
    firstListDehydrationNodeId=None,
    transmission=None,
    firstDehydrationLoop=True,
    pyarch_html_dir=None,
    token=None,
    firstImagePath=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    run_number = int(run_number)
    startTime = float(startTime)
    currentRH = float(currentRH)
    targetRH = float(targetRH)

    if transmission is not None:
        transmission = float(transmission)

    workflowParameters = common.jsonLoads(workflowParameters)
    workflowId = workflowParameters["ispyb_id"]
    firstListDehydrationNodeId = common.jsonLoads(firstListDehydrationNodeId)

    workflow_working_dir = workflowParameters["workingDir"]

    list_node_id = common.jsonLoads(list_node_id)
    listDehydrationResults = common.jsonLoads(dehydrationResults)
    if firstDehydrationLoop:
        firstDehydrationLoop = False
        firstListDehydrationNodeId = list_node_id

    flux = None
    beamSizeX = None
    beamSizeY = None
    strategyOption = None
    minExposureTime = None

    mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)

    listImages = edna_mxv1.getListImagePath(
        mxv1StrategyResult,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        bFirstImageOnly=False,
    )

    if beamline == "id30a3":
        edna_mxv1.h5ToCbf(beamline, listImages, workflow_working_dir)
        suffix = "cbf"
        firstImagePath = listImages[0]

    xsDataInterfaceResult, ednaLogPath = edna_mxv1.mxv1ControlInterface(
        beamline,
        listImages,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        flux,
        beamSizeX,
        beamSizeY,
        strategyOption,
        minExposureTime,
        workflow_working_dir,
        bLogExecutiveSummary=False,
        transmission=transmission,
    )

    useDozor = "dozor" in diffractionSignalDetection.lower()

    if (
        xsDataInterfaceResult is not None
        and xsDataInterfaceResult.resultCharacterisation is not None
    ):
        xsDataResultCharacterisation = xsDataInterfaceResult.resultCharacterisation
        mxv1ResultCharacterisation = xsDataResultCharacterisation.marshal()
        if not os.path.exists(pyarch_html_dir):
            os.mkdir(pyarch_html_dir, mode=0o755)
        html_page_path, json_path = edna_mxv1.createSimpleHtmlPage(
            beamline,
            mxv1ResultCharacterisation,
            workflow_working_dir,
            pyarch_html_dir,
        )
        if html_page_path is not None:
            collect.displayNewHtmlPage(
                beamline, html_page_path, "Dehydration", list_node_id, token=token
            )
            workflowStepType = os.path.basename(os.path.dirname(html_page_path))
            workflowStepImagePyarchPath = None
            bestWilsonPath = os.path.join(os.path.dirname(html_page_path), "B.jpg")
            if (
                xsDataResultCharacterisation.strategyResult is not None
                and os.path.exists(bestWilsonPath)
            ):
                workflowStepImagePyarchPath = bestWilsonPath
            elif len(xsDataResultCharacterisation.thumbnailImage) > 0:
                workflowStepImage = xsDataResultCharacterisation.thumbnailImage[
                    0
                ].path.value
                if os.path.exists(workflowStepImage):
                    path.systemCopyFile(
                        workflowStepImage, os.path.dirname(html_page_path)
                    )
                    workflowStepImagePyarchPath = os.path.join(
                        os.path.dirname(html_page_path),
                        os.path.basename(workflowStepImage),
                    )
            status = "Success"
            ispyb.storeWorkflowStep(
                beamline,
                workflowParameters["ispyb_id"],
                workflowStepType,
                status,
                imageResultFilePath=workflowStepImagePyarchPath,
                htmlResultFilePath=html_page_path,
                resultFilePath=json_path,
            )
        if xsDataResultCharacterisation.strategyResult is not None:
            mxv1StrategyResult = xsDataResultCharacterisation.strategyResult.marshal()
        else:
            mxv1StrategyResult = ""
            averageDozorScore = None
        if useDozor:
            # Check dozor scores
            listDozorScore = []
            for (
                imageQualityIndicator
            ) in xsDataResultCharacterisation.imageQualityIndicators:
                if imageQualityIndicator.dozor_score is not None:
                    listDozorScore.append(imageQualityIndicator.dozor_score.value)

            if len(listDozorScore) > 0:
                # Calculate average dozor score
                averageDozorScore = sum(listDozorScore) / len(listDozorScore)
                logger.info(
                    "Average dozor score after characterisation: {0:.3f}".format(
                        averageDozorScore
                    )
                )
                if averageDozorScore < 0.1:
                    message_text = "The average Dozor score after "
                    message_text += " characterisation ({0:.3f})".format(
                        averageDozorScore
                    )
                    message_text += " is below the threshold of 0.1,"
                    message_text += " therefore stopping automatic dehydration."
                    logger.error(message_text)
                    continue_dehydration = False
    else:
        xsDataResultCharacterisation = None
        mxv1ResultCharacterisation = ""
        logger.info("Stopping automatic dehydration")
        continue_dehydration = False

    listDehydrationResults.append(
        dehydration.extract_results(
            beamline, startTime, currentRH, xsDataResultCharacterisation
        )
    )
    strDehydrationWebPage, firstPlotName = dehydration.makePlotsAndWebPage(
        beamline,
        directory,
        workflow_working_dir,
        targetRH,
        currentRH,
        listDehydrationResults,
    )
    collect.displayNewHtmlPage(
        beamline,
        strDehydrationWebPage,
        "Dehydration Results",
        firstListDehydrationNodeId,
        token=token,
    )
    if pyarch_html_dir is not None:
        dict_html = {}
        dict_html["html_directory_path"] = os.path.dirname(strDehydrationWebPage)
        dict_html["index_file_name"] = os.path.basename(strDehydrationWebPage)
        dict_html_pyarch = path.copyHtmlDirectoryToPyarch(
            pyarch_html_dir, dict_html, directory_name="Dehydration"
        )
        workflowStepType = os.path.basename(dict_html_pyarch["html_directory_path"])
        status = "Success"
        snapShotPath = os.path.join(
            dict_html_pyarch["html_directory_path"], firstPlotName
        )
        jsonPath = os.path.join(dict_html_pyarch["html_directory_path"], "report.json")
        ispyb.storeWorkflowStep(
            beamline,
            workflowId,
            workflowStepType,
            status,
            imageResultFilePath=snapShotPath,
            htmlResultFilePath=dict_html_pyarch["html_directory_path"],
            resultFilePath=jsonPath,
        )
    dehydrationResults = listDehydrationResults
    logger.debug("Dehydration results: %r" % dehydrationResults)

    return {
        # "mxv1StrategyResult": str(mxv1StrategyResult),
        # "mxv1ResultCharacterisation": str(mxv1ResultCharacterisation),
        "dehydrationResults": dehydrationResults,
        "firstDehydrationLoop": firstDehydrationLoop,
        "firstListDehydrationNodeId": firstListDehydrationNodeId,
        "firstImagePath": firstImagePath,
        "continue_dehydration": continue_dehydration,
    }
