def run(beamline, **_):
    return {
        "workflow_title": "ChipX collect on %s" % beamline,
        "workflow_type": "ChipX_collect",
    }
