from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    run_number,
    meshPositions,
    workflow_working_dir,
    grid_info,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, token=token)

    logger.warning(
        "Starting online data processing, this can take up till one minute..."
    )
    meshPositions = edna_mxv1.storeImageQualityIndicatorsFromMeshPositions(
        beamline, directory, meshPositions, workflow_working_dir
    )
    # Dataset outputs:
    return {
        "meshPositions": meshPositions,
    }
