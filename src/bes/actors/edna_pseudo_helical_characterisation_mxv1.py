#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "01/03/2021"

import os
import shutil

from bes.workflow_lib import path
from bes.workflow_lib import icat
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    proposal,
    mxv1CharacterisationStrategyResultFile,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    flux,
    beamSizeX,
    beamSizeY,
    workflow_type,
    workflow_working_dir,
    list_node_id,
    workflowParameters,
    bestPosition,
    characterisation_images,
    characterisation_raw_directory,
    sampleInfo=None,
    characterisationTransmission=None,
    resolution=None,
    pyarch_html_dir=None,
    strategyOption="-low never",
    anomalousData=None,
    crystalSizeX=None,
    crystalSizeY=None,
    crystalSizeZ=None,
    aimedCompleteness=None,
    aimedMultiplicity=None,
    sampleSusceptibility=None,
    minExposureTime=None,
    forcedSpaceGroup=None,
    minOscWidth=None,
    aimedIOverSigmaAtHighestResolution=1.0,
    do_data_collect=None,
    do_data_collect_orig=None,
    diffractionSignalDetection="Dozor (macro-molecules)",
    misalignedCrystalDetected=False,
    remainingPositions=[],
    token=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    simulated_characterisation_results=False,
    besParameters=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    remainingPositions = common.jsonLoads(remainingPositions)

    cellSpaceGroup = {}

    workflowParameters = common.jsonLoads(workflowParameters)
    sampleInfo = common.jsonLoads(sampleInfo)

    if besParameters:
        bes_request_id = besParameters["request_id"]
    else:
        bes_request_id = None

    useDozor = "dozor" in diffractionSignalDetection.lower()
    run_number = int(run_number)
    flux = float(flux)
    beamSizeX = float(beamSizeX)
    beamSizeY = float(beamSizeY)
    if characterisationTransmission is not None:
        characterisationTransmission = float(characterisationTransmission)
    if resolution is not None:
        resolution = float(resolution)
    aimedIOverSigmaAtHighestResolution = float(aimedIOverSigmaAtHighestResolution)
    if do_data_collect_orig is not None:
        # Reset do_data_collect
        do_data_collect = do_data_collect_orig

    mxv1StrategyResult = path.readXSDataFile(mxv1CharacterisationStrategyResultFile)

    message_text = ""
    characterisationComment = None
    expTypePrefix = "ref-"
    status = "Failure"

    firstImagePath = characterisation_images[0]

    (forcedSpaceGroup, newCharacterisationComment) = symmetry.checkForcedSpaceGroup(
        forcedSpaceGroup, sampleInfo, directory
    )

    if (
        characterisationComment is None
        and firstImagePath is not None
        and forcedSpaceGroup is not None
    ):
        characterisationComment = newCharacterisationComment
        if characterisationComment == "":
            characterisationComment = (
                "Characterisation: Space group {0} forced.".format(forcedSpaceGroup)
            )
        else:
            characterisationComment += (
                " Characterisation: Space group {0} forced.".format(forcedSpaceGroup)
            )

    if minExposureTime is None:
        minExposureTime = config.get_value(
            beamline, "Beam", "minCharacterisationExposureTime"
        )

    detectorDistanceMin = config.get_value(beamline, "Detector", "distanceMin")
    detectorDistanceMax = config.get_value(beamline, "Detector", "distanceMax")
    minTransmission = config.get_value(beamline, "Beam", "minTransmission")

    noPositions = len(remainingPositions)

    if noPositions == 0:

        characterisationComment = "Characterisation: Space group {0} forced.".format(
            forcedSpaceGroup
        )
        logger.error(characterisationComment)
        mxv1StrategyResult = ""
        mxv1ResultCharacterisation = ""

    else:

        if noPositions == 1:
            message_text = "Only one extra position, normal strategy calculation."
        else:
            message_text = (
                "{0} positions used for pseudo-helical data collection".format(
                    noPositions
                )
            )

        if characterisationComment is None or characterisationComment == "":
            characterisationComment = message_text
        else:
            characterisationComment += " " + message_text
        message_text = ""

        xsDataInterfaceResult, ednaLogPath = edna_mxv1.mxv1ControlInterface(
            beamline=beamline,
            listImages=characterisation_images,
            directory=directory,
            run_number=run_number,
            expTypePrefix=expTypePrefix,
            prefix=prefix,
            suffix=suffix,
            flux=flux,
            beamSizeX=beamSizeX,
            beamSizeY=beamSizeY,
            strategyOption=strategyOption,
            minExposureTime=minExposureTime,
            workflow_working_dir=workflow_working_dir,
            transmission=characterisationTransmission,
            crystalSizeX=crystalSizeX,
            crystalSizeY=crystalSizeY,
            crystalSizeZ=crystalSizeZ,
            bLogExecutiveSummary=False,
            anomalousData=anomalousData,
            aimedCompleteness=aimedCompleteness,
            aimedMultiplicity=aimedMultiplicity,
            sampleSusceptibility=sampleSusceptibility,
            detectorDistanceMin=detectorDistanceMin,
            detectorDistanceMax=detectorDistanceMax,
            minTransmission=minTransmission,
            forcedSpaceGroup=forcedSpaceGroup,
            aimedIOverSigmaAtHighestResolution=aimedIOverSigmaAtHighestResolution,
            minOscWidth=minOscWidth,
            numberOfPositions=noPositions,
        )
        mxv1StrategyResult = ""
        mxv1ResultCharacterisation = ""
        characterisation_success = False
        xsDataResultCharacterisation = xsDataInterfaceResult.resultCharacterisation

        characterisation_id += 1

        if xsDataResultCharacterisation is None:
            message_text = "No results obtained from EDNA characterisation!"
            logger.error(message_text)
        else:
            mxv1ResultCharacterisation = xsDataResultCharacterisation.marshal()
            strHtmlPagePath, strJsonPath = edna_mxv1.createSimpleHtmlPage(
                beamline,
                mxv1ResultCharacterisation,
                workflow_working_dir,
                pyarch_html_dir,
                resolution=resolution,
                ednaCharacterisationLogPath=ednaLogPath,
            )
            if strHtmlPagePath is not None:
                if list_node_id is not None:
                    list_node_id = common.jsonLoads(list_node_id)
                    # Copy html files to workflow working dir
                    newHtmlDir = os.path.join(
                        workflow_working_dir,
                        os.path.basename(os.path.dirname(strHtmlPagePath)),
                    )
                    shutil.copytree(os.path.dirname(strHtmlPagePath), newHtmlDir)
                    newHtmlPagePath = os.path.join(
                        newHtmlDir, os.path.basename(strHtmlPagePath)
                    )
                    collect.displayNewHtmlPage(
                        beamline,
                        newHtmlPagePath,
                        "Enhanced characterisation",
                        list_node_id,
                        token=token,
                    )

            averageDozorScore = None
            if useDozor:
                # Check dozor scores
                listDozorScore = []
                for (
                    imageQualityIndicator
                ) in xsDataResultCharacterisation.imageQualityIndicators:
                    if imageQualityIndicator.dozor_score is not None:
                        listDozorScore.append(imageQualityIndicator.dozor_score.value)

                if len(listDozorScore) > 0:
                    # Calculate average dozor score
                    averageDozorScore = sum(listDozorScore) / len(listDozorScore)
                    logger.info(
                        "Average dozor score after characterisation: {0:.3f}".format(
                            averageDozorScore
                        )
                    )
                    if (
                        averageDozorScore > 10
                        and (min(listDozorScore) / max(listDozorScore) < 0.1)
                        and not misalignedCrystalDetected
                    ):
                        # Check max / min dozor score
                        logger.warning("Ratio min / max dozor score less than 10 %!")
                        logger.warning(
                            "This means that probably the crystal is not well aligned in beam."
                        )
                        misalignedCrystalDetected = True
                        message_text = "Possible crystal misalignment detected."
                    else:
                        misalignedCrystalDetected = False
                    if message_text != "":
                        if (
                            characterisationComment is None
                            or characterisationComment == ""
                        ):
                            characterisationComment = message_text
                        else:
                            characterisationComment += " " + message_text
                else:
                    logger.warning("No dozor scores obtained from reference images!")

            if (
                xsDataResultCharacterisation.strategyResult is None
                or len(xsDataResultCharacterisation.strategyResult.collectionPlan) == 0
            ):
                status = "Failure"
                message_text = (
                    "No strategy results obtained from EDNA characterisation!"
                )
                logger.error(message_text)
                if averageDozorScore < 0.1:
                    if do_data_collect:
                        message_text = f"The average Dozor score {averageDozorScore} after characterisation is below the threshold of 0.1, therefore no data collection."
                        logger.error(message_text)
                        do_data_collect_orig = do_data_collect
                        do_data_collect = False
                        if (
                            characterisationComment is None
                            or characterisationComment == ""
                        ):
                            characterisationComment = message_text
                        else:
                            characterisationComment += " " + message_text
                    misalignedCrystalDetected = False
            else:
                status = "Success"
                characterisation_success = True
                mxv1StrategyResult = (
                    xsDataResultCharacterisation.strategyResult.marshal()
                )
                # Extract cell and space group
                indexingResult = xsDataResultCharacterisation.indexingResult
                selectedSolution = indexingResult.selectedSolution
                crystal = selectedSolution.crystal
                spaceGroup = crystal.spaceGroup
                cell = crystal.cell
                cellSpaceGroup["spaceGroup"] = spaceGroup.name.value
                cellSpaceGroup["cell_a"] = cell.length_a.value
                cellSpaceGroup["cell_b"] = cell.length_b.value
                cellSpaceGroup["cell_c"] = cell.length_c.value
                cellSpaceGroup["cell_alpha"] = cell.angle_alpha.value
                cellSpaceGroup["cell_beta"] = cell.angle_beta.value
                cellSpaceGroup["cell_gamma"] = cell.angle_gamma.value
                # Extract strategy
                indexPosition = 0
                for (
                    collectionPlan
                ) in xsDataResultCharacterisation.strategyResult.collectionPlan:
                    collectionStrategy = collectionPlan.collectionStrategy
                    for subWedge in collectionStrategy.subWedge:
                        experimentalCondition = subWedge.experimentalCondition
                        goniostat = experimentalCondition.goniostat
                        beam = experimentalCondition.beam
                        dictStrategy = {}
                        dictStrategy["rotationAxisStart"] = (
                            goniostat.rotationAxisStart.value - 1.0
                        )
                        dictStrategy["rotationAxisEnd"] = (
                            goniostat.rotationAxisEnd.value + 2.0
                        )
                        dictStrategy["oscillationWidth"] = (
                            goniostat.oscillationWidth.value
                        )
                        dictStrategy["exposureTime"] = beam.exposureTime.value
                        dictStrategy["transmission"] = beam.transmission.value
                        dictStrategy["resolution"] = (
                            collectionPlan.strategySummary.resolution.value
                        )
                        remainingPositions[indexPosition]["strategy"] = dictStrategy
                        indexPosition += 1

            workflowStepType = "Characterisation"
            workflowStepImagePyarchPath = None
            bestWilsonPath = os.path.join(os.path.dirname(strHtmlPagePath), "B.jpg")
            if (
                xsDataResultCharacterisation.strategyResult is not None
                and os.path.exists(bestWilsonPath)
            ):
                workflowStepImagePyarchPath = bestWilsonPath
            elif len(xsDataResultCharacterisation.thumbnailImage) > 1:
                workflowStepImage = xsDataResultCharacterisation.thumbnailImage[
                    0
                ].path.value
                if os.path.exists(workflowStepImage):
                    shutil.copy(workflowStepImage, os.path.dirname(strHtmlPagePath))
                    workflowStepImagePyarchPath = os.path.join(
                        os.path.dirname(strHtmlPagePath),
                        os.path.basename(workflowStepImage),
                    )
            ispyb.storeWorkflowStep(
                beamline,
                workflowParameters["ispyb_id"],
                workflowStepType,
                status,
                imageResultFilePath=workflowStepImagePyarchPath,
                htmlResultFilePath=strHtmlPagePath,
                resultFilePath=strJsonPath,
            )
            logger.info("Storing workflow step in ICAT")
            sample_name = icat.get_sample_name(directory)
            metadata_urls = config.get_value(
                beamline, "ICAT", "metadata_urls", default_value=[]
            )
            if len(metadata_urls) > 0:
                icat.storeWorkflowStep(
                    beamline=beamline,
                    proposal=proposal,
                    directory=characterisation_raw_directory,
                    workflowStepType=workflowStepType.lower(),
                    sample_name=sample_name,
                    workflow_name=workflowParameters["title"],
                    workflow_type=workflowParameters["type"],
                    bes_request_id=bes_request_id,
                    snap_shot_path=workflowStepImagePyarchPath,
                    json_path=strJsonPath,
                    icat_sub_dir=f"characterisation_id_{characterisation_id}",
                    characterisation_id=characterisation_id,
                    kappa_settings_id=kappa_settings_id,
                    position_id=position_id,
                )

        if status == "Failure":
            # Prepare collect without strategy
            if anomalousData:
                mxpressoNoImages = int(1800.0 / noPositions + 0.5)
            else:
                mxpressoNoImages = int(900.0 / noPositions + 0.5)
            rotationAxisStart = 0.0
            oscillationWidth = 0.2
            characterisationExposureTime = (
                config.get_value(
                    beamline, "Beam", "defaultCharacterisationExposureTime"
                )
                * noPositions
            )
            transmission = config.get_value(
                beamline, "Beam", "defaultCollectTransmission"
            )
            # One degree overlap at start and end
            deltaRotation = mxpressoNoImages * oscillationWidth + 2.0
            for position in remainingPositions:
                dictStrategy = {}
                dictStrategy["rotationAxisStart"] = rotationAxisStart
                dictStrategy["rotationAxisEnd"] = rotationAxisStart + deltaRotation
                dictStrategy["oscillationWidth"] = oscillationWidth
                dictStrategy["exposureTime"] = characterisationExposureTime
                dictStrategy["transmission"] = transmission
                dictStrategy["resolution"] = resolution
                position["strategy"] = dictStrategy
                rotationAxisStart += deltaRotation - 1.0
            message_text = "Characterisation failure, default {0} degree data collection with {1} s exposure time per position.".format(
                deltaRotation, characterisationExposureTime
            )
            if characterisationComment is None or characterisationComment == "":
                characterisationComment = message_text
            else:
                characterisationComment += " " + message_text

    if (
        characterisationComment is not None
        and characterisationComment != ""
        and firstImagePath is not None
    ):
        ispyb.updateDataCollectionComment(
            beamline, firstImagePath, characterisationComment
        )
        ispyb.updateDataCollectionGroupComment(
            beamline, firstImagePath, characterisationComment
        )

    mxv1StrategyResultFile = path.createNewXSDataFile(
        workflowParameters["workingDir"],
        mxv1StrategyResult,
        filePrefix="mxv1StrategyResult",
    )
    mxv1ResultCharacterisationFile = path.createNewXSDataFile(
        workflowParameters["workingDir"],
        mxv1ResultCharacterisation,
        filePrefix="mxv1ResultCharacterisation",
    )

    if not characterisation_success and simulated_characterisation_results:
        logger.warning("Using simulated characterisation results!")
        characterisation_success = True

    return {
        "characterisation_success": characterisation_success,
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
        "mxv1ResultCharacterisationFile": mxv1ResultCharacterisationFile,
        "message_text": message_text,
        "characterisationComment": characterisationComment,
        "cellSpaceGroup": cellSpaceGroup,
        "do_data_collect": do_data_collect,
        "do_data_collect_orig": do_data_collect_orig,
        "misalignedCrystalDetected": misalignedCrystalDetected,
        "doCharacterisation": False,
        "remainingPositions": remainingPositions,
        "characterisation_id": characterisation_id,
        "process_partial_data_sets": True,
        "list_partial_data_sets": None,
    }
