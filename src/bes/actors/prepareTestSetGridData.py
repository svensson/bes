from bes.workflow_lib import edna_mxv1


def run(directory=None, beamline=None, **_):

    if beamline is None:
        try:
            beamline = edna_mxv1.extractBeamlineFromDirectory(directory)
        except Exception:
            beamline = "unknown"

    return {
        "workflow_title": "Test set grid data on {0}".format(beamline),
        "workflow_type": "TestSetGridData",
        "beamline": beamline.lower(),
    }
