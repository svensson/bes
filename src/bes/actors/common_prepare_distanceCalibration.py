def run(beamline=None, **_):

    return {
        "workflow_title": "Distance calibration on %s" % beamline,
        "workflow_type": "Undefined",
    }
