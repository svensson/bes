from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    meshPositionFile,
    run_number,
    expTypePrefix,
    workflow_working_dir,
    prefix,
    suffix,
    workflow_id,
    firstImagePath,
    workflowParameters,
    token=None,
    **_,
):

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    meshPositions = path.parseJsonFile(meshPositionFile)

    logger.debug("Uploading data collection parameters to ISPyB...")
    iDataCollectionGroupId = edna_ispyb.updateDataCollectionGroupWorkflowId(
        beamline,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        firstImagePath,
        workflow_id,
        workflow_working_dir,
    )
    edna_ispyb.groupMeshDataCollections(
        beamline, directory, meshPositions, workflow_working_dir, iDataCollectionGroupId
    )
    logger.debug("ISPyB uploading finished.")

    return {"Done": True}
