def run(beamline, expTypePrefix="mesh-", **_):

    return {
        "workflow_title": "X-ray centering on %s" % beamline,
        "workflow_type": "XrayCentering",
        "expTypePrefix": expTypePrefix,
        "do2DMeshOnly": False,
        "automatic_mode": True,
    }
