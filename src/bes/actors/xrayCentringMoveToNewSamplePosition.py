import time
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

EPSILON = 0.01


def run(
    beamline,
    workflowParameters,
    directory,
    motorPositions,
    newMotorPositions,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        motorPositions = common.jsonLoads(motorPositions)
        newMotorPositions = common.jsonLoads(newMotorPositions)

        logger.debug("Moving to new sample position.")

        # if beamline == "id30a1":
        #     oldPhiz = motorPositions["phiz"]
        #     newPhiy = newMotorPositions["phiy"]
        #     newPhiz = newMotorPositions["phiz"]
        #     distancePhiz = newPhiz - oldPhiz
        #     logger.info("Vertical distance: %.3f" % distancePhiz)
        #     motorPositions["phiy"] = newPhiy
        #     collect.moveMotors(beamline, directory, motorPositions, token=token)
        #     collect.moveSampleOrthogonallyToRotationAxis(beamline, directory, distancePhiz, token=token)
        # else:
        collect.moveMotors(beamline, directory, newMotorPositions, token=token)

        time.sleep(1)

        motorPositions = collect.readMotorPositions(beamline, token=token)
    #
    #     position_string = "Sample position after move:"
    #     for motorName in dict_diffractometer_positions:
    #         position_string += " %s=%.3f" % (motorName, dict_diffractometer_positions[motorName])
    #     logger.debug(position_string)
    #     if beamline != "simulator":
    #         for motorName in newMotorPositions:
    #             if abs(newMotorPositions[motorName] - dict_diffractometer_positions[motorName]) > EPSILON:
    #                 raise Exception("Sample didn't move to new position!")
    #         logger.debug("Sample moved to the new position.")
    #     else:
    #         logger.info("Simulator: sample not moved to the new position")

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {"motorPositions": motorPositions}
