import os
import shutil

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import edna2_tasks
from bes.workflow_lib import radiation_damage
from bes.workflow_lib import workflow_logging
from bes.workflow_lib.report import WorkflowStepReport

# beamline,
# workflowParameters,
# directory,
# raw_directory,
# process_directory,
# prefix,
# run_number,
# motorPositions,
# workflow_type,
# workflow_index,
# data_collection_exposure_time,
# transmission,
# data_collection_no_images,
# data_collection_osc_range,
# sampleInfo = None,
# resolution = None,
# data_collection_iterations = 1,
# token = None,


def run(**inData):
    dataCollectionGroupId = None

    beamline = inData["beamline"]
    workflowParameters = inData["workflowParameters"]
    token = inData["token"]
    data_collection_exposure_time = inData["data_collection_exposure_time"]
    data_collection_no_images = inData["data_collection_no_images"]
    data_collection_osc_range = inData["data_collection_osc_range"]
    data_collection_iterations = inData["data_collection_iterations"]
    transmission = inData["transmission"]
    sampleInfo = inData["sampleInfo"]
    wavelength = inData["wavelength"]
    resolution = inData.get("resolution", None)
    motorPositions = inData["motorPositions"]
    directory = inData["directory"]
    process_directory = directory.replace("RAW_DATA", "PROCESSED_DATA")
    prefix = inData["prefix"]
    workflow_type = inData["workflow_type"]
    workflow_index = inData["workflow_index"]
    highResolution = inData.get("highResolution", None)
    lowResolution = inData.get("lowResolution", None)
    sample_node_id = inData.get("sample_node_id", None)
    workflowId = workflowParameters["ispyb_id"]
    dose_per_wedge = inData["dose_per_wedge"]
    working_directory = workflowParameters["workingDir"]

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    suffix = config.get_value(beamline, "Detector", "suffix")

    logger.info("Starting data collection")
    logger.info("Exposure time: %r" % data_collection_exposure_time)
    logger.info("No images: %r" % data_collection_no_images)
    logger.info("Oscillation range: %r" % data_collection_osc_range)
    logger.info("Transmission: %r" % transmission)

    sampleInfo = common.jsonLoads(sampleInfo)
    if sampleInfo is None:
        groupNodeId = None
        sampleNodeId = sample_node_id
    else:
        groupNodeId = sampleInfo["groupNodeId"]
        sampleNodeId = sampleInfo["nodeId"]

    if resolution is None:
        resolution = collect.getResolution(beamline, token=token)
    else:
        resolution = float(resolution)

    # Collect the data automatically
    enabled = True

    expTypePrefix = ""

    osc_range = float(data_collection_osc_range) / data_collection_no_images

    collectExposureTime = data_collection_exposure_time

    #     motorPositions = collect.readMotorPositions(beamline, token=token)
    data_collection_start_phi = motorPositions["phi"] - data_collection_osc_range / 2.0

    list_dozor_all = []
    list_xds_ascii_hkl = []
    list_edna2_task = []

    for iteration in range(data_collection_iterations):

        # Find unique run number for data collect
        data_collect_run_number = path.findUniqueRunNumber(
            directory, prefix, expTypePrefix, run_number=1
        )
        mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
            collectExposureTime,
            transmission,
            data_collection_no_images,
            osc_range,
            data_collection_start_phi,
        )
        dictId = collect.loadQueue(
            beamline=beamline,
            directory=directory,
            process_directory=process_directory,
            prefix=prefix,
            expTypePrefix=expTypePrefix,
            run_number=data_collect_run_number,
            resolution=resolution,
            mxv1StrategyResult=mxv1StrategyResult,
            motorPositions=motorPositions,
            workflow_type=workflow_type,
            workflow_index=workflow_index,
            group_node_id=groupNodeId,
            sample_node_id=sampleNodeId,
            dictCellSpaceGroup={},
            energy=None,
            enabled=enabled,
            snapShots=False,
            firstImageNo=1,
            inverseBeam=False,
            doProcess=False,
            highResolution=highResolution,
            lowResolution=lowResolution,
            token=token,
        )

        groupNodeId = dictId["group_node_id"]
        sampleInfo["groupNodeId"] = groupNodeId

        listImagePath = []
        for imageNo in range(1, data_collection_no_images + 1):
            imageFileName = "{0}_{1}_{2:04d}.{3}".format(
                prefix, data_collect_run_number, imageNo, suffix
            )
            imagePath = os.path.join(directory, imageFileName)
            listImagePath.append(imagePath)
        firstImagePath = listImagePath[0]
        logger.info(
            "Data collection started, {0} images, first image = {1}".format(
                data_collection_no_images, listImagePath
            )
        )
        dataCollectionId = findDataCollectionId(beamline, firstImagePath)

        if dataCollectionGroupId is None:
            dataCollectionGroupId = edna_ispyb.updateGroupWorkflowId(
                inData, listImagePath, logger
            )

        edna2_dozor_task = edna2_tasks.startRadiationDamageDozorProcessing(
            beamline=beamline,
            directory=directory,
            workflow_working_dir=workflowParameters["workingDir"],
            list_image_path=listImagePath,
            dataCollectionId=dataCollectionId,
            working_directory_suffix=data_collect_run_number,
        )
        list_edna2_task.append(edna2_dozor_task)

        edna2_xds_task = edna2_tasks.startXdsIndexAndIntegration(
            imagePath=listImagePath,
            workflow_working_dir=workflowParameters["workingDir"],
            working_directory_suffix=data_collect_run_number,
        )
        list_edna2_task.append(edna2_xds_task)

    for edna2_task in list_edna2_task:
        edna2_task.join()
        if edna2_task.outData is not None:
            out_data = edna2_task.outData
            if "dozorAllFile" in out_data:
                list_dozor_all.append(edna2_task.outData["dozorAllFile"])
            elif "xdsAsciiHkl" in out_data:
                list_xds_ascii_hkl.append(out_data["xdsAsciiHkl"])

    resultDozorRD = edna2_tasks.runDozorRD(
        wavelength=wavelength,
        exposureTime=data_collection_exposure_time,
        numberOfImages=data_collection_no_images,
        listDozorAll=list_dozor_all,
        workingDirectory=workflowParameters["workflowWorkingDir"],
    )

    results_rdfit_rd = radiation_damage.run_rdfit(
        dose_per_wedge, list_xds_ascii_hkl, working_directory
    )
    logger.info("Results from RDFIT RD:")
    logger.info("")
    for line in results_rdfit_rd:
        logger.info(line)
    logger.info("")

    if "logPath" in resultDozorRD:
        with open(resultDozorRD["logPath"]) as fd:
            logger.info("\n\n" + fd.read())

    # Create result pages for ISPyB / EXI
    resultsPath = os.path.join(workflowParameters["workingDir"], "results")
    os.makedirs(resultsPath, mode=0o755)
    workflowStepReport = WorkflowStepReport(
        "RadiationDamage", "Radiation Damage Results"
    )
    if "listPlotFile" in resultDozorRD:
        firstImagePath = resultDozorRD["listPlotFile"][0]
        shutil.copy(firstImagePath, resultsPath)
        for plotFile in resultDozorRD["listPlotFile"]:
            workflowStepReport.addImage(plotFile, os.path.basename(plotFile))
    pagePath = workflowStepReport.renderHtml(resultsPath)
    pathToJsonFile = workflowStepReport.renderJson(resultsPath)
    pyarchPath = path._getPyarchFilePath(resultsPath)
    shutil.copytree(resultsPath, pyarchPath)
    workflowStepType = "Radiation Damage"
    status = "Success"

    ispyb.storeWorkflowStep(
        beamline,
        workflowId,
        workflowStepType,
        status,
        imageResultFilePath=os.path.join(pyarchPath, os.path.basename(firstImagePath)),
        htmlResultFilePath=os.path.join(pyarchPath, os.path.basename(pagePath)),
        resultFilePath=os.path.join(pyarchPath, os.path.basename(pathToJsonFile)),
    )

    return {
        "listDozorAll": list_dozor_all,
        "listXdsAsciiHkl": list_xds_ascii_hkl,
        "resultDozorRD": resultDozorRD,
    }


def findDataCollectionId(beamline, firstImagePath):
    # Retrieve the data collection id for the first image
    dataCollectionId = None
    dataCollection = ispyb.findDataCollectionFromFileLocationAndFileName(
        beamline, firstImagePath
    )
    if dataCollection is not None:
        dataCollectionId = dataCollection.dataCollectionId
    return dataCollectionId
