"""
Workflow module for preparing mesh collection
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2015"
__updated__ = "2021-05-03"

from bes.workflow_lib import path
from bes.workflow_lib import grid
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    shape,
    collection_software,
    motorPositions,
    run_number,
    prefix,
    suffix,
    meshZigZag=True,
    meshHorizontalSeparation=None,
    meshVerticalSeparation=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    gridOscillationRange = 0.0
    expTypePrefix = "mesh-"
    multi_wedge = False
    workflow_working_dir = workflowParameters["workingDir"]

    grid_info = collect.getGridInfo(beamline, token=token, shape=shape)

    # BES-318 : prevent 1x1 grids
    if grid_info["steps_x"] == 1 and grid_info["steps_y"] == 1:
        raise RuntimeError("Grid dimensions must be bigger than 1x1!")

    # Introduce gaps if necessary
    if meshHorizontalSeparation > 0 or meshVerticalSeparation > 0:
        grid_info = grid.newGridInfoWithGaps(
            grid_info,
            meshHorizontalSeparation / 1000.0,
            meshVerticalSeparation / 1000.0,
        )
        logger.info("New grid info with gaps:")
        logger.info(
            "x1   : %6.3f mm, y1   : %6.3f mm" % (grid_info["x1"], grid_info["y1"])
        )
        logger.info(
            "dx   : %6.3f mm, dy   : %6.3f mm"
            % (grid_info["dx_mm"], grid_info["dy_mm"])
        )
        logger.info(
            "gapx : %6.3f mm, gapy : %6.3f mm"
            % (meshHorizontalSeparation / 1000.0, meshVerticalSeparation / 1000.0)
        )
        logger.info(
            "nx   : %6d,      ny : %6d" % (grid_info["steps_x"], grid_info["steps_y"])
        )

    if collection_software == "MXCuBE - 3.0":
        grid_info["x1"] = grid_info["x1"] + grid_info["beam_width"] / 2.0
        grid_info["y1"] = grid_info["y1"] + grid_info["beam_height"] / 2.0
        grid_info["dx_mm"] = grid_info["dx_mm"] - grid_info["beam_width"]
        grid_info["dy_mm"] = grid_info["dy_mm"] - grid_info["beam_height"]

    # Mesh positions:
    meshPositions = grid.determineMeshPositions(
        beamline,
        motorPositions,
        gridOscillationRange,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
        meshZigZag,
        multi_wedge=multi_wedge,
    )

    allPositionFile = path.createNewJsonFile(
        workflow_working_dir, meshPositions, "all_positions_"
    )

    return {"grid_info": grid_info, "shape": None, "allPositionFile": allPositionFile}
