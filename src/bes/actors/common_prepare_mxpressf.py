def run(beamline, expTypePrefix="mesh-", **_):

    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressF: X-centre + fbest + 180 degree dc on %s" % beamline
    )
    workflowParameters["type"] = "MXPressF"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": expTypePrefix,
        "resolution": 2.0,
        "meshZigZag": False,
        "do_data_collect": True,
        "anomalousData": False,
        "doCharacterisation": False,
        "doFbest": True,
        "mxpressoNoImages": 900,
        "mxpressoOscRange": 0.2,
        "automatic_mode": True,
    }
