from bes.workflow_lib import common
from bes.workflow_lib import aperture_utils


def run(beamline, **kwargs):

    workflowParameters = {}
    workflowParameters["title"] = "MXPressA: X-centre, eEDNA + dc on %s" % beamline
    workflowParameters["type"] = "MXPressA"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": common.checkInputVariable(kwargs, "expTypePrefix", "mesh-"),
        "resolution": 2.0,
        "meshZigZag": False,
        "automatic_mode": True,
        "targetApertureName": aperture_utils.aperture_size_to_name(beamline, 10),
    }
