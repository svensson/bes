def run(beamline, directory, grid_info=None, expTypePrefix="mesh-", **_):

    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressM: high-exposure large mesh scan on %s" % beamline
    )
    workflowParameters["type"] = "MXPressM"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": expTypePrefix,
        "resolution": 2.0,
        "meshZigZag": False,
        "do_data_collect": False,
        "anomalousData": False,
        "doCharacterisation": False,
        "findLargestMesh": True,
        "gridExposureTime": 0.25,
        "do2DMeshOnly": True,
        "automatic_mode": True,
    }
