def run(beamline, **_):

    return {
        "workflow_title": "Radiation damage on %s" % beamline,
        "workflow_type": "RadiationDamage",
        "doRefDataCollectionReview": False,
        "radiation_damage_beta": 1.0,
        "radiation_damage_gamma": 0.06,
        "snapShots": True,
        "data_collection_no_images": 100,
        "data_collection_exposure_time": 0.05,
        "data_collection_osc_range": 0.1,
        "transmission": 100,
        "data_collection_iterations": 5,
    }
