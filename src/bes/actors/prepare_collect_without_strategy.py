#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "01/02/2021"

from bes.workflow_lib import path
from bes import config
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    motorPositions,
    flux,
    resolution,
    highResolution,
    mxpressoNoImages=900,
    mxpressoOscRange=0.2,
    collectTransmission=None,
    collectExposureTime=None,
    axisRange=None,
    minOscWidth=None,
    anomalousData=False,
    checkForFlux=True,
    targetFlux=None,
    firstImagePath=None,
    sampleInfo={},
    dataCollectionPosition=None,
    token=None,
    doFbest=False,
    wavelength=None,
    doCharacterisation=None,
    crystalSizeX=None,
    crystalSizeY=None,
    crystalSizeZ=None,
    beamSizeAtSampleX=None,
    beamSizeAtSampleY=None,
    sampleSusceptibility=None,
    dozorVisibleResolutionVertical=None,
    dozorVisibleResolutionHorizontal=None,
    dozor2m_resolution=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    mxpressoNoImages = int(mxpressoNoImages)
    mxpressoOscRange = float(mxpressoOscRange)
    dataCollectionPosition = common.jsonLoads(dataCollectionPosition)
    wavelength = float(wavelength)
    if dataCollectionPosition is None:
        dataCollectionPosition = {}
    motorPositions = common.jsonLoads(motorPositions)
    sampleInfo = common.jsonLoads(sampleInfo)

    phi = motorPositions["phi"]
    if flux is not None:
        flux = float(flux)
    else:
        doFbest = False

    workflowParameters = common.jsonLoads(workflowParameters)
    comment = None

    if beamSizeAtSampleX is not None:
        beamSizeAtSampleX = int(beamSizeAtSampleX * 1000)  # In microns
    if beamSizeAtSampleY is not None:
        beamSizeAtSampleY = int(beamSizeAtSampleY * 1000)  # In microns

    if doFbest:
        workflow_working_dir = workflowParameters["workingDir"]
        if (
            crystalSizeX is not None
            and crystalSizeY is not None
            and crystalSizeZ is not None
        ):
            crystalSizeX = float(crystalSizeX)
            crystalSizeY = float(crystalSizeY)
            crystalSizeZ = float(crystalSizeZ)
            crystalSize = (
                max(crystalSizeX, crystalSizeY, crystalSizeZ) * 1000
            )  # In microns
        else:
            crystalSize = None
        minExposureTime = config.get_value(
            beamline, "Beam", "minCharacterisationExposureTime"
        )
        if (
            dozorVisibleResolutionVertical is not None
            and dozorVisibleResolutionHorizontal is not None
        ):
            minDozorVisibleResolution = min(
                dozorVisibleResolutionVertical, dozorVisibleResolutionHorizontal
            )
            logger.debug(
                "minDozorVisibleResolution : {0} A".format(minDozorVisibleResolution)
            )
            maxDetectorDistance = config.get_value(beamline, "Detector", "distanceMax")
            minDetectorResolution = collect.detectorDistanceToResolution(
                beamline, maxDetectorDistance, wavelength
            )
            # Round to one decimal down
            minDetectorResolution = int(minDetectorResolution * 10) / 10.0
            logger.debug("minDetectorResolution : {0} A".format(minDetectorResolution))
            min_no_strategy_resolution = config.get_value(
                beamline, "Detector", "minNoStrategyResulution", default_value=4.0
            )
            resolution = min(
                minDozorVisibleResolution,
                minDetectorResolution,
                min_no_strategy_resolution,
            )
            # Max resolution
            logger.debug("highResolution : {0} A".format(highResolution))
            resolution = max(resolution, highResolution)
        elif dozor2m_resolution is not None:
            logger.debug("highResolution : {0} A".format(highResolution))
            resolution = max(dozor2m_resolution, highResolution)

        logger.info("Fbest resolution : {0} A".format(resolution))

        if axisRange is None:
            if anomalousData:
                rotationRange = 360.0
            else:
                rotationRange = 180.0
        else:
            rotationRange = axisRange
        logger.info(f"Fbest rotation range : {rotationRange} degrees")

        if minOscWidth is None:
            rotationWidth = 0.2
        else:
            rotationWidth = minOscWidth
        logger.info(f"Fbest rotation width : {rotationWidth} degrees")

        nominalBeamSizeX = config.get_value(beamline, "Beam", "nominalSizeX")
        nominalBeamSizeY = config.get_value(beamline, "Beam", "nominalSizeY")

        dictResult = edna_mxv1.runFbest(
            flux,
            resolution,
            beamH=nominalBeamSizeX,
            beamV=nominalBeamSizeY,
            aperture=(beamSizeAtSampleX + beamSizeAtSampleY) / 2.0,
            wavelength=wavelength,
            minExposureTime=minExposureTime,
            rotationRange=rotationRange,
            rotationWidth=rotationWidth,
            crystalSize=crystalSize,
            sampleSusceptibility=sampleSusceptibility,
            workflow_working_dir=workflow_working_dir,
        )
        if dictResult["success"]:
            collectExposureTime = dictResult["exposureTimePerImage"]
            transmission = dictResult["transmission"]
            osc_range = dictResult["rotationWidth"]
            number_of_images = dictResult["numberOfImages"]
            comment = " Fbest data collection: {0:.1f} degrees, exposure time {1:.3f} s, resolution {3:.2f} A, transmission {2:.2f} %.".format(
                osc_range * number_of_images,
                collectExposureTime,
                transmission,
                resolution,
            )
        else:
            raise RuntimeError(f"FBest execution error: {dictResult['log']}")
    elif "strategy" in dataCollectionPosition:
        dictStrategy = dataCollectionPosition["strategy"]

        phi = dictStrategy["rotationAxisStart"]
        osc_range = dictStrategy["oscillationWidth"]
        number_of_images = int(
            (dictStrategy["rotationAxisEnd"] - dictStrategy["rotationAxisStart"])
            / osc_range
        )
        collectExposureTime = dictStrategy["exposureTime"]
        transmission = dictStrategy["transmission"]
        resolution = dictStrategy["resolution"]

    else:

        if collectExposureTime is None:
            collectExposureTime = config.get_value(
                beamline, "Beam", "defaultCollectExposureTime"
            )

        if collectTransmission is None:
            transmission = config.get_value(
                beamline, "Beam", "defaultCollectTransmission"
            )
        else:
            transmission = collectTransmission
        logger.debug("Default transmission: %.1f %%" % transmission)

        if axisRange is not None:
            osc_range = mxpressoOscRange
            totalOscRange = axisRange
            number_of_images = int(totalOscRange / osc_range)
        else:
            collect360 = False
            if anomalousData:
                collect360 = True
            elif (
                "forcedSpaceGroup" in sampleInfo
                and sampleInfo["forcedSpaceGroup"] == "P1"
            ):
                collect360 = True
            elif (
                "crystalSpaceGroup" in sampleInfo
                and sampleInfo["crystalSpaceGroup"] == "P1"
            ):
                collect360 = True

            if collect360:
                # Check if total oscillation range for 2 * number of images is greater than 360:
                totalOscRange = mxpressoNoImages * mxpressoOscRange * 2
                if totalOscRange > 360.0:
                    number_of_images = int(360.0 / mxpressoOscRange)
                else:
                    number_of_images = mxpressoNoImages * 2
            else:
                number_of_images = mxpressoNoImages

            osc_range = mxpressoOscRange
            totalOscRange = number_of_images * mxpressoOscRange

        if targetFlux is not None:
            apertureSize = collect.getApertureSize(beamline, token)
            if apertureSize is None:
                raise RuntimeError("Cannot get the current aperture size")
            logger.info("Current aperture size: %d um", apertureSize)
            fluxApertureSize = int(beamSizeAtSampleX)
            logger.info("Aperture used for measuring flux: %d um", fluxApertureSize)

            if apertureSize == fluxApertureSize:
                correctedFlux = flux
            else:
                apertureName = collect.getApertureName(beamline, token)
                correctionFactor = aperture_utils.aperture_name_to_correction_factor(
                    beamline, apertureName
                )
                logger.info(
                    "CorrectionFactor for aperture {0} : {1}".format(
                        apertureSize, correctionFactor
                    )
                )

                correctionFactorFlux = (
                    aperture_utils.aperture_size_to_correction_factor(
                        beamline, fluxApertureSize
                    )
                )
                logger.info(
                    "CorrectionFactor for aperture {0} : {1}".format(
                        fluxApertureSize, correctionFactorFlux
                    )
                )
                correctedFlux = flux / correctionFactorFlux * correctionFactor
                logger.info("Original flux: {0:.2e}".format(flux))
                logger.info("Corrected flux: {0:.2e}".format(correctedFlux))

            logger.info("Target flux: {0:.2e}".format(targetFlux))
            transmissionFactor = targetFlux / correctedFlux
            logger.info(
                "Transmission factor (targetFlux / correctedFlux): {0:.2f}".format(
                    transmissionFactor
                )
            )
            logger.info("Current transmission: {0:.0f}".format(transmission))
            logger.info("Current exposure time: {0:.4f} s".format(collectExposureTime))
            if transmissionFactor > 1:
                logger.info("New transmission: 100 %")
                transmission = 100
                collectExposureTime = collectExposureTime * transmissionFactor
                logger.info("New exposure time: {0} s".format(collectExposureTime))
            else:
                transmission = transmissionFactor * 100
                logger.info("New transmission: {0:.1f} %".format(transmission))
            ispyb.updateDataCollectionGroupComment(
                beamline,
                firstImagePath,
                "Flux maintained at {0:.2e}".format(targetFlux),
            )
        elif (
            checkForFlux and flux is not None and flux > 1.7e12 * 180.0 / totalOscRange
        ):
            transmission = transmission * 1.7e12 / flux * 180.0 / totalOscRange
        logger.info("Transmission set to %.1f %%" % transmission)

        if doCharacterisation:
            comment = "{0} degree data collection.".format(totalOscRange)

    mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
        collectExposureTime, transmission, number_of_images, osc_range, phi
    )
    mxv1StrategyResultFile = path.createNewXSDataFile(
        workflowParameters["workingDir"],
        mxv1StrategyResult,
        "dataCollectWithoutStrategy",
    )

    if (
        firstImagePath is not None
        and not workflowParameters["type"].startswith("MXPressO")
        and comment is not None
    ):
        ispyb.updateDataCollectionGroupComment(beamline, firstImagePath, comment)

    return {
        "expTypePrefix": "",
        "do_data_collect": True,
        "automatic_mode": True,
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
        "resolution": resolution,
    }
