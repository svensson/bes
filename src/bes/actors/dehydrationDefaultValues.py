from bes import config


def run(beamline, **_):

    return {
        "characterisationExposureTime": config.get_value(
            beamline, "Beam", "defaultCharacterisationExposureTime"
        ),
        "transmission": config.get_value(
            beamline, "Beam", "defaultDehydrationTransmission"
        ),
        "continue_dehydration": True,
    }
