import os
import subprocess

from bes.workflow_lib import common
from bes.workflow_lib import mxnice
from bes.workflow_lib import reprocess
from bes.workflow_lib import workflow_logging


def run(
    token,
    workflowParameters,
    dataCollectionId,
    start=None,
    end=None,
    cutoff="Automatic",
    **_,
):

    try:
        workflowParameters = common.jsonLoads(workflowParameters)
        logger = workflow_logging.getLogger(None, workflowParameters)
        fileList = []

        workingDir = workflowParameters["workingDir"]

        logFilePath = os.path.join(workingDir, "reprocess.log")

        reprocessScriptPath = reprocess.prepareReprocessScript(
            autoProcProgram="EDNA_proc",
            dataCollectionId=dataCollectionId,
            startImage=start,
            endImage=end,
            resolutionCutoff=cutoff,
            workingDir=workingDir,
            logFilePath=logFilePath,
        )

        listSorted = mxnice.checkLoadOnMxNICE()
        hostName = listSorted[0][0]
        command_line = "ssh -f {0} 'export AutoPROCWorkFlowUser=True; {1}'".format(
            hostName, reprocessScriptPath
        )
        logger.info("Starting script '{0}'".format(command_line))
        process = subprocess.Popen(
            command_line,
            shell=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            close_fds=True,
        )
        for line in iter(process.stdout.readline, ""):
            logger.info(line.replace("\n", ""))

        if os.path.exists(logFilePath):
            extISPyBStatus = "FINISHED"
            fileList.append(["Reprocess log", logFilePath])
        else:
            extISPyBStatus = "ERROR"

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {"fileList": fileList, "extISPyBStatus": extISPyBStatus}
