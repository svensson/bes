from bes.workflow_lib import path
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, motorPositions, flux, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        motorPositions = common.jsonLoads(motorPositions)
        phi = motorPositions["phi"]
        flux = float(flux)
        workflowParameters = common.jsonLoads(workflowParameters)

        collectExposureTime = config.get_value(
            beamline, "Beam", "defaultCollectExposureTime"
        )
        if flux < 1.7e12:
            transmission = 100
        else:
            transmission = 100 * 1.7e12 / flux
        logger.info("Transmission set to %.1f %%" % transmission)
        number_of_images = 900
        osc_range = 0.2

        mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
            collectExposureTime, transmission, number_of_images, osc_range, phi
        )
        mxv1StrategyResultFile = path.createNewXSDataFile(
            workflowParameters["workingDir"],
            mxv1StrategyResult,
            "dataCollect180degrees",
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "expTypePrefix": "",
        "do_data_collect": True,
        "automatic_mode": True,
        "mxv1StrategyResultFile": mxv1StrategyResultFile,
    }
