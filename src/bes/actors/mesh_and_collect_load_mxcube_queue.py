from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import autoprocessing
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    raw_directory,
    process_directory,
    prefix,
    expTypePrefix,
    run_number,
    allPositionFile,
    motorPositions,
    workflow_type,
    workflow_index,
    data_collection_exposure_time,
    transmission,
    data_collection_no_images,
    data_collection_osc_range,
    data_collection_max_positions,
    data_collection_start_phi=None,
    sampleInfo=None,
    resolution=None,
    inverseBeam=False,
    data_collection_iterations=1,
    grenades_fastproc=False,
    grenades_parallelproc=False,
    EDNA_proc=False,
    XIA2_DIALS=False,
    autoPROC=False,
    XDSAPP=False,
    token=None,
    adaptiveAperture=False,
    listSnapShotFileName=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    besParameters=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    workflow_index = int(workflow_index)
    data_collection_exposure_time = float(data_collection_exposure_time)
    transmission = float(transmission)
    data_collection_no_images = int(data_collection_no_images)
    data_collection_osc_range = float(data_collection_osc_range)
    data_collection_max_positions = int(data_collection_max_positions)
    data_collection_iterations = int(data_collection_iterations)
    dataCollectionId = None

    if besParameters:
        bes_request_id = besParameters["request_id"]
    else:
        bes_request_id = None

    suffix = config.get_value(beamline, "Detector", "suffix")

    allPositions = path.parseJsonFile(allPositionFile)
    if len(allPositions) > data_collection_max_positions:
        allPositions = allPositions[0:data_collection_max_positions]

    expTypePrefix = ""

    sampleInfo = common.jsonLoads(sampleInfo)
    if sampleInfo is None:
        groupNodeId = None
        sampleNodeId = -1
    else:
        groupNodeId = sampleInfo["groupNodeId"]
        sampleNodeId = sampleInfo["nodeId"]

    if len(allPositions) == 0:
        data_collection_iterations = 0
    else:
        logger.info("Starting data collection")
        logger.info("Exposure time: %r" % data_collection_exposure_time)
        logger.info("No images: %r" % data_collection_no_images)
        logger.info("Oscillation range: %r" % data_collection_osc_range)
        logger.info("Transmission: %r" % transmission)

        if resolution is None:
            resolution = collect.getResolution(beamline, token=token)
        else:
            resolution = float(resolution)

        # Collect the data automatically
        enabled = True

        osc_range = float(data_collection_osc_range) / data_collection_no_images

        collectExposureTime = data_collection_exposure_time

        #     motorPositions = collect.readMotorPositions(beamline, token=token)
        if data_collection_start_phi is None:
            data_collection_start_phi = (
                motorPositions["phi"] - data_collection_osc_range / 2.0
            )

        firstDataCollectionId = None

        # Do processing?
        doProcess = False
        firstImagePath = None
        listAutoProcessingDir = []

    for _ in range(data_collection_iterations):

        for position in allPositions:

            # Find unique run number for data collect
            directory, run_number = path.createUniqueICATDirectory(
                raw_directory=raw_directory,
                run_number=run_number,
                workflow_index=workflow_index,
            )

            # Set aperture if requested
            if adaptiveAperture and "beam_size" in position:
                # float to int
                newApertureSize = int(position["beam_size"])
                _ = collect.setApertureSize(beamline, newApertureSize, token=token)

            newMotorPositions = dict(motorPositions)
            newMotorPositions["sampx"] = position["sampx"]
            newMotorPositions["sampy"] = position["sampy"]
            newMotorPositions["phiy"] = position["phiy"]
            newMotorPositions["phiz"] = position["phiz"]

            mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
                collectExposureTime,
                transmission,
                data_collection_no_images,
                osc_range,
                data_collection_start_phi,
            )

            dictId = collect.loadQueue(
                beamline=beamline,
                directory=directory,
                process_directory=process_directory,
                prefix=prefix,
                expTypePrefix=expTypePrefix,
                run_number=run_number,
                resolution=resolution,
                mxv1StrategyResult=mxv1StrategyResult,
                motorPositions=newMotorPositions,
                workflow_type=workflow_type,
                workflow_index=workflow_index,
                group_node_id=groupNodeId,
                sample_node_id=sampleNodeId,
                snapShots=False,
                enabled=enabled,
                doProcess=doProcess,
                token=token,
                workflow_name=workflowParameters["title"],
                bes_request_id=bes_request_id,
                characterisation_id=characterisation_id,
                kappa_settings_id=kappa_settings_id,
                position_id=position_id,
            )

            firstImagePath = edna_mxv1.getListImagePath(
                mxv1StrategyResult,
                directory,
                run_number,
                expTypePrefix,
                prefix,
                suffix=suffix,
                bFirstImageOnly=True,
            )[0]
            logger.info(
                "Data collection started, {0} images, first image = {1}".format(
                    data_collection_no_images, firstImagePath
                )
            )

            try:
                # First get the data collection id
                dataCollection = ispyb.findDataCollectionFromFileLocationAndFileName(
                    beamline, firstImagePath
                )
                logger.debug(
                    "Auto processing: dataCollection = {0}".format(dataCollection)
                )
                if dataCollection is not None:
                    dataCollectionId = dataCollection.dataCollectionId
                    if firstDataCollectionId is None:
                        firstDataCollectionId = dataCollectionId
                        logger.debug(
                            "First data collection id = {0}".format(
                                firstDataCollectionId
                            )
                        )
                    autoProcessingDir = autoprocessing.startAutoProcessing(
                        beamline,
                        firstImagePath,
                        dataCollection,
                        grenades_fastproc=grenades_fastproc,
                        grenades_parallelproc=grenades_parallelproc,
                        EDNA_proc=EDNA_proc,
                        XIA2_DIALS=XIA2_DIALS,
                        autoPROC=autoPROC,
                        XDSAPP=XDSAPP,
                    )
                    listAutoProcessingDir.append(autoProcessingDir)

                if inverseBeam:
                    mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
                        collectExposureTime,
                        transmission,
                        data_collection_no_images,
                        osc_range,
                        data_collection_start_phi + 180.0,
                    )
                    # Find unique run number for data collect
                    directory, run_number = path.createUniqueICATDirectory(
                        raw_directory=raw_directory,
                        run_number=run_number,
                        workflow_index=workflow_index,
                    )
                    dictId = collect.loadQueue(
                        beamline=beamline,
                        directory=directory,
                        prefix=prefix,
                        expTypePrefix=expTypePrefix,
                        run_number=run_number,
                        resolution=resolution,
                        mxv1StrategyResult=mxv1StrategyResult,
                        motorPositions=newMotorPositions,
                        workflow_type=workflow_type,
                        workflow_index=workflow_index,
                        group_node_id=groupNodeId,
                        sample_node_id=sampleNodeId,
                        enabled=enabled,
                        token=token,
                        workflow_name=workflowParameters["title"],
                        bes_request_id=bes_request_id,
                        characterisation_id=characterisation_id,
                        kappa_settings_id=kappa_settings_id,
                        position_id=position_id,
                    )
                    firstImagePath = edna_mxv1.getListImagePath(
                        mxv1StrategyResult,
                        raw_directory,
                        run_number,
                        expTypePrefix,
                        prefix,
                        suffix=suffix,
                        bFirstImageOnly=True,
                    )[0]
                    logger.info(
                        "Inverse beam data collection started, {0} images, first image = {1}".format(
                            data_collection_no_images, firstImagePath
                        )
                    )
                    autoprocessing.startAutoProcessing(
                        beamline,
                        firstImagePath,
                        grenades_fastproc=grenades_fastproc,
                        grenades_parallelproc=grenades_parallelproc,
                        EDNA_proc=EDNA_proc,
                        XIA2_DIALS=XIA2_DIALS,
                        autoPROC=autoPROC,
                        XDSAPP=XDSAPP,
                    )

                groupNodeId = dictId["group_node_id"]
                sampleInfo["groupNodeId"] = groupNodeId
            except Exception:
                logger.warning("Data collection failed for %s!", firstImagePath)

    if len(allPositions) > 0:
        logger.info(
            "Optimal strategy has been calculated and been transferred to the mxCuBE queue."
        )
        if firstDataCollectionId is not None:
            autoprocessing.startCodgasMeshAndCollect(
                beamline, firstImagePath, firstDataCollectionId, listAutoProcessingDir
            )

    return {
        "expTypePrefix": expTypePrefix,
        "sampleInfo": sampleInfo,
        "dataCollectionId": dataCollectionId,
    }
