from bes.workflow_lib import workflow_logging


def run(beamline="simulator", message_text="Workflow aborted.", **_):
    logger = workflow_logging.getLogger()
    logger.error("Workflow aborted")
    return {"message_text": message_text}
