from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    collection_software,
    startRH,
    endRH,
    step,
    delay,
    aimedResolution,
    transmission,
    no_reference_images,
    angle_between_reference_images,
    snapShots,
    workflowParameters,
    token=None,
    **_,
):

    dictValues = {}
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    aperture_names = collect.getApertureNames(beamline, token)

    listDialog = [
        {
            "variableName": "aimedResolution",
            "label": "Aimed resolution",
            "type": "float",
            "value": aimedResolution,
            "unit": "photon/s",
        },
        {
            "variableName": "startRH",
            "label": "Start humidity level [%]",
            "type": "float",
            "value": startRH,
            "unit": "%",
        },
        {
            "variableName": "endRH",
            "label": "End humidity level [%]",
            "type": "float",
            "value": endRH,
            "unit": "%",
        },
        {
            "variableName": "step",
            "label": "Step in humidity level [%]",
            "type": "float",
            "value": step,
            "unit": "%",
        },
        {
            "variableName": "delay",
            "label": "Delay before changing humidity level [s]",
            "type": "float",
            "value": delay,
            "unit": "s",
        },
        {
            "variableName": "transmission",
            "label": "Transmission [%]",
            "type": "float",
            "value": transmission,
            "unit": "%",
        },
        {
            "variableName": "no_reference_images",
            "label": "Number of reference images",
            "type": "int",
            "value": no_reference_images,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "combo",
            "defaultValue": angle_between_reference_images,
            "textChoices": ["30", "45", "60", "90"],
        },
        {
            "variableName": "targetApertureName",
            "label": "Aperture",
            "type": "combo",
            "defaultValue": aperture_utils.aperture_size_to_name(beamline, 30),
            "textChoices": aperture_names,
        },
        {
            "variableName": "snapShots",
            "label": "Snap shots",
            "type": "boolean",
            "value": False,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=False,
        collection_software=collection_software,
    )

    return dictValues
