from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import symmetry
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    targetFlux,
    aimedResolution,
    preferredApertureName,
    workflowParameters,
    directory,
    numberOfPositions,
    mxpressoOscRange,
    mxpressoNoImages,
    gridTransmission,
    gridSize,
    gridSteps,
    diffractionSignalDetection,
    forcedSpaceGroup,
    collection_software=None,
    token=None,
    **_,
):
    _ = workflow_logging.getLogger(
        beamline, workflowParameters=workflowParameters, token=token
    )
    aperture_names = collect.getApertureNames(beamline, token)

    list_of_chiral_spacegroups = symmetry.getListOfChiralSpaceGroups()
    list_of_chiral_spacegroups.insert(0, "None")
    if forcedSpaceGroup is None:
        forced_space_group_value = "None"
    else:
        forced_space_group_value = forcedSpaceGroup

    listDialog = [
        {
            "variableName": "targetFlux",
            "label": "Target flux",
            "type": "float",
            "value": targetFlux,
            "unit": "photon/s",
        },
        {
            "variableName": "aimedResolution",
            "label": "Aimed resolution",
            "type": "float",
            "value": aimedResolution,
            "unit": "photon/s",
        },
        {
            "variableName": "preferredApertureName",
            "label": "Aperture",
            "type": "combo",
            "value": preferredApertureName,
            "textChoices": aperture_names,
        },
        {
            "variableName": "gridTransmission",
            "label": "Grid transmission",
            "type": "float",
            "value": gridTransmission,
        },
        {
            "variableName": "mxpressoOscRange",
            "label": "Oscillation range",
            "type": "float",
            "value": mxpressoOscRange,
        },
        {
            "variableName": "mxpressoNoImages",
            "label": "Number of images",
            "type": "int",
            "value": mxpressoNoImages,
        },
        {
            "variableName": "numberOfPositions",
            "label": "Number of positions",
            "type": "int",
            "value": numberOfPositions,
        },
        {
            "variableName": "gridSize",
            "label": "Grid size [microns]",
            "type": "float",
            "value": gridSize,
        },
        {
            "variableName": "gridSteps",
            "label": "Grid number of steps",
            "type": "int",
            "value": gridSteps,
        },
        {
            "variableName": "diffractionSignalDetection",
            "label": "Diffraction signal detection",
            "type": "combo",
            "textChoices": [
                "DozorM2 (macro-molecules crystal detection)",
                "Dozor (macro-molecules)",
                "Spotfinder (small molecules)",
            ],
            "value": diffractionSignalDetection,
        },
        {
            "variableName": "forcedSpaceGroup",
            "label": "Forced space group",
            "type": "combo",
            "textChoices": list_of_chiral_spacegroups,
            "value": forced_space_group_value,
        },
    ]

    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=False,
        collection_software=collection_software,
        file_prefix="mxpressr",
    )

    if dictValues["forcedSpaceGroup"] == "None":
        dictValues["forcedSpaceGroup"] = None

    return dictValues
