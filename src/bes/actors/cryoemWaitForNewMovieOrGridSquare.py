"""
Initialisation of a workflow
"""

import time
import json

from bes.workflow_lib import common
from bes.workflow_lib import cryoem
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    processDirectory,
    gridSquaresJsonFile,
    currentMovie=None,
    currentGridSquare=None,
    REFID=None,
    REQUESTID=None,
    **_,
):

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(None, workflowParameters)
    logger.info("*********** cryoemWaitForNewMovieOrGridSquare **********")

    dictGridSquares = json.loads(open(gridSquaresJsonFile).read())
    dictGridSquare = dictGridSquares[currentGridSquare]

    continueToWait = True
    timeOut = False
    startTime = time.time()
    while continueToWait:
        # Check for new movies
        listMovieCreatedSorted, currentMovie = cryoem.updateDictGridSquareForNewMovies(
            dictGridSquare
        )
        if len(listMovieCreatedSorted) > 0:
            currentMovie = listMovieCreatedSorted.pop()
            logger.info("New current movie: {0}".format(currentMovie))
            continueToWait = False
        else:
            # Check for new grid squares
            (
                listGridSquareCreatedSorted,
                currentGridSquare,
            ) = cryoem.updateDictForNewGridSquares(directory, dictGridSquares)
            if len(listGridSquareCreatedSorted) > 0:
                # Mark current grid square as finished
                dictGridSquare["status"] = "finished"
                dictGridSquare["endTime"] = time.ctime(time.time())
                ctimeCreated, currentGridSquare = listGridSquareCreatedSorted.pop()
                dictGridSquare = dictGridSquares[currentGridSquare]
                dictGridSquare["status"] = "started"
                dictGridSquare["startTime"] = time.ctime(time.time())
                logger.info("New current grid square: {0}".format(currentGridSquare))
                continueToWait = False
            else:
                logger.info("Waiting for movie / grid square, sleeping 30 s")
                time.sleep(30)
                timeElapsed = time.time() - startTime
                if timeElapsed > 120:
                    logger.info("Time-out after 120 s!")
                    continueToWait = False
                    timeOut = True

    open(gridSquaresJsonFile, "w").write(json.dumps(dictGridSquares, indent=4))

    return {
        "currentMovie": currentMovie,
        "currentGridSquare": currentGridSquare,
        "timeOut": timeOut,
    }
