from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    remainingPositions,
    workflowParameters,
    motorPositions,
    multiPositionIndex=None,
    token=None,
    position_id=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    if position_id is None:
        position_id = 1
    else:
        position_id += 1

    if multiPositionIndex is not None:
        multiPositionIndex += 1

        logger.info(
            "Moving to next best position, position number {0}".format(
                multiPositionIndex
            )
        )
    best_position = {}
    newMotorPositions = {}
    dozormTargetApertureSize = None
    dozor2m_resolution = None

    if len(remainingPositions) == 0:
        moreSamples = False
    else:
        best_position = remainingPositions[0]
        remainingPositions = remainingPositions[1:]
        if len(remainingPositions) > 0:
            moreSamples = True
        else:
            moreSamples = False

        # Resolution:
        if "dozor2m_resolution" in best_position:
            dozor2m_resolution = best_position["dozor2m_resolution"]

        newMotorPositions = {}

        # newMotorPositions["phi"] = round(best_position["omega"], 3)
        newMotorPositions["sampx"] = round(best_position["sampx"], 3)
        newMotorPositions["sampy"] = round(best_position["sampy"], 3)
        newMotorPositions["phiy"] = round(best_position["phiy"], 3)
        position_string = "Best position:"
        for motorName in newMotorPositions:
            position_string += " %s=%.3f" % (motorName, newMotorPositions[motorName])
        logger.info(position_string)

        # Move phi axis to new position
        collect.moveMotors(beamline, directory, newMotorPositions, token=token)

        # If id30a1, move sample vertically to negative phiz position
        if beamline == "id30a1":
            collect.moveSampleOrthogonallyToRotationAxis(
                beamline, directory, -motorPositions["phiz"], token=token
            )

        # Read new motor positions
        motorPositions = collect.readMotorPositions(beamline, token=token)

        # Dozorm target aperture
        dozormTargetApertureSize = best_position.get("beam_size", None)
        if dozormTargetApertureSize is not None:
            # Float to int
            dozormTargetApertureSize = int(dozormTargetApertureSize)

    return {
        "newMotorPositions": newMotorPositions,
        "bestPosition": best_position,
        "dataCollectionPosition": best_position,
        "remainingPositions": remainingPositions,
        "moreSamples": moreSamples,
        "titleMeshHtmlPage": "",
        "meshHtmlDirectoryName": "",
        "dozormTargetApertureSize": dozormTargetApertureSize,
        "multiPositionIndex": multiPositionIndex,
        "dozor2m_resolution": dozor2m_resolution,
        "motorPositions": motorPositions,
        "position_id": position_id,
    }
