from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, shape=None, token=None, **_):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    grid_info = collect.getGridInfo(beamline, shape=shape, token=token)
    motor_positions = eval(grid_info["motor_positions"])
    logger.debug("Motor positions: {0}".format(motor_positions))
    pos1 = motor_positions[0]
    sampx1 = pos1["sampx"]
    sampy1 = pos1["sampy"]
    phiy1 = pos1["phiy"]
    phiz1 = pos1["phiz"]

    pos2 = motor_positions[1]
    sampx2 = pos2["sampx"]
    sampy2 = pos2["sampy"]
    phiy2 = pos2["phiy"]
    phiz2 = pos2["phiz"]

    return {
        "sampx1": sampx1,
        "sampy1": sampy1,
        "phiy1": phiy1,
        "phiz1": phiz1,
        "sampx2": sampx2,
        "sampy2": sampy2,
        "phiy2": phiy2,
        "phiz2": phiz2,
    }
