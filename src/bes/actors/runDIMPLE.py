import os
import urllib

from bes.workflow_lib import common
from bes.workflow_lib import extISPyB
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(
    token,
    pdbFile,
    workflowParameters,
    mtzFile=None,
    proposal=None,
    autoprocintegrationURL=None,
    autoProcAttachmentURL=None,
    downloadAutoProcProgramAttachementURL=None,
    **_,
):
    try:

        dstPdbFileName = "pdbFile.pdb"
        dstMtzFileName = "mtz.mtz"

        workflowParameters = common.jsonLoads(workflowParameters)

        logger = workflow_logging.getLogger(None, workflowParameters)

        workingDir = workflowParameters["workingDir"]
        mtzFilePath = os.path.join(workingDir, dstMtzFileName)
        pdbFilePath = os.path.join(workingDir, dstPdbFileName)

        # Getting MTZ input
        if mtzFile is None:
            listAutoProcIntegration = extISPyB.getFromURL(autoprocintegrationURL)
            # For the moment only look for EDNA_proc noanom:
            autoProcProgramId = None
            autoProcProgramAttachmentId = None
            for autoProcIntegration in listAutoProcIntegration:
                if (
                    autoProcIntegration["v_datacollection_processingPrograms"]
                    == "EDNA_proc"
                    and not autoProcIntegration[
                        "v_datacollection_summary_phasing_anomalous"
                    ]
                ):
                    autoProcProgramId = autoProcIntegration[
                        "v_datacollection_summary_phasing_autoProcProgramId"
                    ]
            if autoProcProgramId is not None:
                logger.info("autoProcProgramId: {0}".format(autoProcProgramId))
                url2 = autoProcAttachmentURL.replace(
                    "%autoprocprogramIds%", str(autoProcProgramId)
                )
                listAttachment = extISPyB.getFromURL(url2)
                for attachment in listAttachment:
                    if attachment["fileName"].endswith("noanom_aimless.mtz"):
                        autoProcProgramAttachmentId = attachment[
                            "autoProcProgramAttachmentId"
                        ]
            if autoProcProgramAttachmentId is not None:
                logger.info(
                    "autoProcProgramAttachmentId: {0}".format(
                        autoProcProgramAttachmentId
                    )
                )
                mtzFile = downloadAutoProcProgramAttachementURL.replace(
                    "%autoProcAttachmentId%", str(autoProcProgramAttachmentId)
                )

        if mtzFile is None:
            logger.error("Couldn't locate MTZ file!")
            extISPyBStatus = "ERROR"
        else:
            logger.info("mtzFile: {0}".format(mtzFile))
            urllib.urlretrieve(mtzFile, mtzFilePath)

            # Getting PDB input
            urllib.urlretrieve(pdbFile, pdbFilePath)

            extISPyBStatus, fileList = edna_mxv1.runDimple(
                mtzFilePath, pdbFilePath, workflowParameters["workingDir"]
            )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {"fileList": fileList, "extISPyBStatus": extISPyBStatus}
