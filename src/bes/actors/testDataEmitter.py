"""
Created on May 7, 2015

@author: svensson
"""


def run(**_):

    return {
        "none": None,
        "booleanTrue": True,
        "booleanFalse": False,
        "integer1": 123,
        "float1": 1.23,
        "string1": "A string",
        "dictionary1": {"a": 1, "b": True, "c": 1},
    }
