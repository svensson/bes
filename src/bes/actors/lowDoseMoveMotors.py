import time

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

EPSILON = 0.01


def run(
    beamline,
    directory,
    workflowParameters,
    deltaKappa,
    deltaPhi,
    noDCPositions,
    token=None,
    **_,
):

    try:

        deltaKappa = float(deltaKappa)
        deltaPhi = float(deltaPhi)
        noDCPositions = int(noDCPositions)

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        motorPositions = collect.readMotorPositions(beamline, token=token)
        #

        newKappaPosition = motorPositions["kappa"] + deltaKappa
        newPhiPosition = motorPositions["kappa_phi"] + deltaPhi

        newMotorPositions = {"kappa": newKappaPosition, "kappa_phi": newPhiPosition}

        collect.moveMotors(beamline, directory, newMotorPositions, token=token)

        time.sleep(1)

        motorPositions = collect.readMotorPositions(beamline, token=token)

        position_string = "Sample position after move:"
        for motorName in newMotorPositions:
            position_string += " %s=%.3f" % (motorName, motorPositions[motorName])
        logger.info(position_string)
        if not beamline == "simulator":
            for motorName in newMotorPositions:
                if (
                    abs(newMotorPositions[motorName] - motorPositions[motorName])
                    > EPSILON
                ):
                    raise Exception(
                        "Sample didn't move to new position! Motor: %s" % motorName
                    )
            logger.debug("Sample moved to the new position.")
        else:
            logger.info("Simulator: sample not moved to the new position")

        noDCPositions -= 1
        if noDCPositions == 1:
            continueDataCollection = False
        else:
            continueDataCollection = True

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "motorPositions": motorPositions,
        "continueDataCollection": continueDataCollection,
        "noDCPositions": noDCPositions,
    }
