from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    workflow_index,
    workflow_type,
    grid_info,
    meshQueueList,
    phiz,
    directory,
    meshPositions,
    workflow_working_dir,
    group_node_id=None,
    sample_node_id=1,
    resolution=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        meshPositions = common.jsonLoads(meshPositions)

        dict_id = collect.executeQueueForMeshScan(
            beamline,
            workflow_index,
            workflow_type,
            meshQueueList,
            group_node_id,
            sample_node_id,
            resolution,
            phiz,
            token=token,
        )
        logger.warning("Data collection done.")
        group_node_id = dict_id["group_node_id"]
        list_node_id = dict_id["listNodeId"]

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "group_node_id": group_node_id,
        "list_node_id": list_node_id,
    }
