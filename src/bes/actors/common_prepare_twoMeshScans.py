def run(beamline, expTypePrefix="mesh-", **_):
    return {
        "workflow_title": "Two Mesh scans on %s" % beamline,
        "workflow_type": "MeshScan",
        "expTypePrefix": expTypePrefix,
        "plateMode": True,
        "do2MeshScans": True,
        "lineScanStep": 0,
        "lineScanLength": 0.3,
        "doLineScans": False,
        "doTwoMeshes": True,
    }
