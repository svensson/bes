import os
import glob
import time
import pprint

from edna2.utils import UtilsPath
from edna2.tasks.AutoProcessingWrappers import AutoPROCWrapper

from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging

EPSILON = 0.1


def run(
    beamline,
    proposal,
    workflowParameters,
    besParameters,
    prefix,
    suffix,
    raw_directory,
    workflow_type,
    sampleInfo,
    workflow_node_id,
    spot_xds_path,
    resolution,
    gphl_strategy_variant="quick",
    workflow_id=None,
    token=None,
    characterisation_id=None,
    kappa_settings_id=None,
    position_id=None,
    sample_name=None,
    group_by=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    # Extract besParameters
    bes_request_id = besParameters["request_id"]

    # Construct workflow_parameters
    workflow_parameters = {
        "workflow_name": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflow_uid": bes_request_id,
        "workflow_characterisation_id": characterisation_id,
        "workflow_kappa_settings_id": kappa_settings_id,
        "workflow_position_id": position_id,
        # "workflow_group_by": group_by,
    }
    if sample_name:
        workflow_parameters["sample_name"] = sample_name

    # Test of GPhL workflow
    sample_node_id = sampleInfo["nodeId"]
    workflow_index = workflowParameters["index"]
    logger.info("#" * 80)
    logger.info("#" * 80)
    logger.info("#" * 80)

    task_dict = {
        "wfpath": "Gphl",  # Mandatory – specifies  GΦL workflow
        "automation_mode": "MASSIF1",  # Optional,  defaults to None (manual)
        "strategy_name": "Native data collection",  # (str) Mandatory.
        # Options are: ‘Native data collection’, ‘Phasing (SAD)’
        # ‘Two-wavelength MAD’, ‘Three-wavelength MAD’, ‘Diffractometer calibration’
        # "point_group": "1",  # (str) Optional. Guides indexing solution. Overridden by space group
        # One of ‘1’, ‘2’, ‘3’, ‘4’, ‘6’, ‘222’, ‘32’. ‘312’, ‘422’, ‘622’, ‘23’, ‘432’
        # "crystal_system": "a",  # (str) Optional. Guides indexing solution. Overridden by space group
        # One of ‘a’, ‘m’, ‘o’, ‘t’, ‘h’, ‘c’
        # directory and file names. All optional, default to session default
        # See File Locations in manual
        "prefix": prefix,  # (str)
        "suffix": suffix,  # (str) Optional
        "subdir": raw_directory,  # (str) Optional
        # Workflow parameters
        "workflow_parameters": workflow_parameters,
        # Optional, overriding configured defaults:
        "decay_limit": 25,  # (%).
        # Minimum % intensity remaining at target resolution if using recommended dose budget
        # dose_budget = 2 * resolution**2 * log(100. / decay_limit) / relative_sensitivity
        "maximum_dose_budget": 20,  # (MGy). Maximum when calculating budget from resolution
        "characterisation_budget_fraction": 0.05,  # Dose to use for characterisation
        # Relevant for MASSIF1
        "characterisation_strategy": "Char_4_by_10",
        # Strategy name, matching strategylib.nml.
        "auto_acq_parameters": [
            {
                # (list of dict) automation parameters for acquisitions. Optional.
                # The standard workflows have two acquisitions, characterisation and main;
                # diffractometer calibration has only one, and future workflows may have more.
                # The dictionaries are applied to the acquisitions in order;
                # if there is only one dictionary in the list, it applies to all acquisitions.
                # The following appear in task dictionaries in interactive MXCuBE3, but are NOT used by the  GΦL workflow:
                # wfname, wftype, strategies, type
                # The first dictionary are the parameters needed for MASSIF1 characterisation
                # where characterisation and first XDS run are done BEFORE starting the workflow.
                # init_spot_dir can be set ONLY for characterisation
                # init_spot_dir, if set, turns off the workflow-driven characterisation
                # resolution, energies, and transmission are taken from the current values in this case
                # unless explicitly set
                # "resolution": 1.21,  # (Å) Optional. Defaults to current value, or to diffraction plan aimed_resolution
                "exposure_time": 0.01,  # (s) Optional, default configured. Mandatory if init_spot_dir is set
                "image_width": 0.1,  # (°) Optional, default configured. Mandatory if init_spot_dir is set
                "init_spot_dir": spot_xds_path,  # Directory containing SPOTS.XDS file from preliminary XDS calculation
                # acquisition parameters, for successive acquisition steps (characterisation or main).
                # This example lists all options
            },
            {
                "resolution": resolution,  # (Å) Optional. Defaults to current value, or to diffraction plan aimed_resolution
                # "energies": (12.842,),  # tuple(keV) List of energies  to use in experiment. two or three for MAD (only).
                # First wavelength will be used for calculating strategies, dose budget, etc.
                # Optional, except for MAD - will defaults to current beamline energy.
                "exposure_time": 0.02,  # (s) Optional, default configured. Mandatory if init_spot_dir si set
                "image_width": 0.1,  # (°) Optional, default configured. Mandatory if init_spot_dir is set
                "wedge_width": 15.0,  # (°)  Optional, default configured. Wedge  width (for interleaving)
                "snapshot_count": 2,  # Optional, default configured.
                # "dose_budget": 20.0,  # (MGy) NB Optional. Overrides transmission and recommended dose budget,
                # and resets transmission and maybe exposure time
                # "transmission": 80.0,  # (%) NB Optional  Defaults to current valuee if init_spot_dir is set
                # Otherwise is set from dose budget and resolution.
                # If set this value will reset the dose budget
                "strategy_options": {  # (dict) parameters strategy calculation (stratcal). See below
                    # strategy options. Control the stratcal strategy calculation program.
                    "variant": gphl_strategy_variant,  # Optional. Strategy variant, “full”, or “quick”.
                    # Default is “quick” for MAD, “full” for native or SAD
                    "maximum_chi": 44.0,  # (°) Optional. Maximum chi value allowed for strategies. Default 48
                    "angular_tolerance": 1.0,  # (°) Optional. Tolerance to distinguish strategy orientations
                },
            },
        ],
    }
    logger.debug("")
    logger.debug("Task dictionary sent to the GPhL workflow:")
    logger.debug("")
    logger.debug(pprint.pformat(task_dict))
    logger.debug("")
    _ = collect.runGphlWorkflow(
        beamline=beamline,
        task_dict=task_dict,
        workflow_type=workflow_type,
        workflow_index=workflow_index,
        workflow_id=workflow_id,
        group_node_id=int(workflow_node_id),
        sample_node_id=sample_node_id,
        message="GPhL workflow",
        token=token,
    )
    time.sleep(2)
    logger.info("#" * 80)
    logger.info("#" * 80)
    logger.info("#" * 80)
    processed_data_dir = UtilsPath.getProcessedDataPath(raw_directory, raise_error=True)
    # raw_directory/LYS-ispy-kappa2_8_001/main/G1B1
    list_partial_data_sets = sorted(glob.glob(f"{raw_directory}/*/main/G*B*"))
    list_runs = ""
    for data_set in list_partial_data_sets:
        data_set_name = os.path.basename(data_set)
        list_runs += "_" + data_set_name
    autoproc_processed_data_dir = (
        processed_data_dir.parent / f"autoPROC_merge{list_runs}"
    )
    index = 1
    while autoproc_processed_data_dir.exists():
        autoproc_processed_data_dir = (
            processed_data_dir.parent / f"autoPROC_merge{list_runs}_{index}"
        )
        index += 1
    working_dir = autoproc_processed_data_dir / "nobackup"
    working_dir.mkdir(mode=0o755, parents=True, exist_ok=False)
    logger.info(f"autoPROC merged processing data dir: {autoproc_processed_data_dir}")
    in_data = {
        "beamline": beamline,
        "proposal": proposal,
        "sample_name": sample_name,
        "raw_data": list_partial_data_sets,
        "icat_processed_data_dir": str(autoproc_processed_data_dir),
        "workingDirectory": str(working_dir),
    }
    # autoProcessingWrappers = AutoPROCWrapper(inData=in_data)
    # autoProcessingWrappers.execute()
    environment = {
        "EDNA2_SITE": os.environ["EDNA_SITE"],
        "ISPyB_user": os.environ["ISPyB_user"],
        "ISPyB_pass": os.environ["ISPyB_pass"],
    }
    AutoPROCWrapper.launch_on_slurm(
        working_dir=working_dir,
        in_data=in_data,
        partition="mx",
        environment=environment,
        list_modules=["AutoProcessingLaunchers", "autoPROC"],
    )
    logger.info("#" * 80)
    logger.info("#" * 80)
    logger.info("#" * 80)

    return {
        "autoproc_processed_data_dir": autoproc_processed_data_dir,
    }
