import os
import time
import subprocess

from bes.workflow_lib import is4a
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(is4aId, is4aUrl, workflowParameters, jobInput, **_):

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(None, workflowParameters)

    # jobs = is4a.getJobs(is4aUrl)
    logger.info("is4aId: '{0}'".format(is4aId))
    logger.info("is4aUrl: '{0}'".format(is4aUrl))
    # logger.info("jobs: '{0}'".format(pprint.pformat(jobs)))

    jobInput = common.jsonLoads(jobInput)
    pipelineName = jobInput["type"]
    dataCollectionId = jobInput["datacollectionid"]
    startImage = jobInput.get("Start", None)
    endImage = jobInput.get("End", None)
    spaceGroup = jobInput.get("Space Group", None)
    doAnom = jobInput.get("Anomalous", False)
    _ = jobInput.get("cutoff", "Automatic")
    dictOutputFile = {}

    workingDir = workflowParameters["workingDir"]

    launcher = None
    mode = "after"
    if pipelineName == "autoPROC":
        launcher = "autoPROCLauncher"
    elif pipelineName == "EDNA_proc":
        launcher = "EDNA_procLauncher"
    elif pipelineName == "XDSAPP":
        launcher = "XDSAPPLauncher"
    elif pipelineName in ["XIA2_DIALS", "XIA2_dials"]:
        launcher = "XIA2_DIALS_Launcher"
    elif pipelineName == "grenades_fastproc":
        launcher = "GrenadesFastProcLauncher"
    elif pipelineName == "grenades_parallelproc":
        launcher = "GrenadesParallelProcLauncher"

    args = "-workingDir {0}".format(workingDir)
    args += " -mode {0}".format(mode)
    args += " -datacollectionID {0}".format(dataCollectionId)
    #    args += " -residues 200 -anomalous False -cell 0,0,0,0,0,0 "
    args += " -reprocess True"
    if startImage is not None:
        args += " -start_image {0}".format(startImage)
    if endImage is not None:
        args += " -end_image {0}".format(endImage)
    if doAnom:
        args += " -do_anom true"
    else:
        args += " -do_anom false"
    if spaceGroup is not None:
        args += " -sg {0}".format(spaceGroup)

    if launcher is None:
        errorMessage = "ERROR: Unknown pipeline: {0}".format(pipelineName)
        logger.error(errorMessage)
        is4a.logMessage(is4aId, is4aUrl, errorMessage)
        is4aStatus = "ERROR"
    else:
        command_line = launcher + " " + args
        logger.info("Starting script '{0}'".format(command_line))
        process = subprocess.run(
            command_line.split(" "), stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        )
        listLines = process.stdout.decode("utf-8").split("\n")
        oarJobId = None
        scriptDir = None
        for line in listLines:
            logger.info(line)
            if "Script dir:" in line:
                scriptDir = line.split("Script dir:")[1].strip()
            if "Job id:" in line:
                oarJobId = int(line.split("Job id:")[1].strip())

        if oarJobId is None:
            is4aStatus = "ERROR"
            logger.info("Status : {0}".format(is4aStatus))
            is4a.setStatus(is4aId, is4aUrl, is4aStatus)
        else:
            is4aStatus = "SUBMITTED"
            logger.info("Status : {0}".format(is4aStatus))
            is4a.setStatus(is4aId, is4aUrl, is4aStatus)
            logger.info(
                "Status from is4a : {0}".format(is4a.getStatus(is4aId, is4aUrl))
            )
            startedFile = os.path.join(scriptDir, "STARTED")

            hasTimedOut = False
            timeout = 7200  # s
            startTime = time.time()
            while not os.path.exists(startedFile):
                time.sleep(1)
                fd = os.open(workingDir, os.O_DIRECTORY)
                _ = os.fstat(fd)
                os.close(fd)
                if time.time() - startTime > timeout:
                    is4aStatus = "TIMEOUT_SUBMISSION"
                    hasTimedOut = True

            if not hasTimedOut:
                is4aStatus = "RUNNING"
                logger.info("Status : {0}".format(is4aStatus))
                is4a.setStatus(is4aId, is4aUrl, is4aStatus)
                logger.info(
                    "Status from is4a : {0}".format(is4a.getStatus(is4aId, is4aUrl))
                )

                finishedFile = os.path.join(scriptDir, "FINISHED")

                timeout = 600  # s
                startTime = time.time()
                while not os.path.exists(finishedFile):
                    time.sleep(10)
                    fd = os.open(workingDir, os.O_DIRECTORY)
                    _ = os.fstat(fd)
                    os.close(fd)
                    if time.time() - startTime > timeout:
                        is4aStatus = "TIMEOUT_RUNNING"
                        hasTimedOut = True

            if not hasTimedOut:
                is4aStatus = "FINISHED"

        message = "Status : {0}".format(is4aStatus)
        logger.info(message)
        is4a.logMessage(is4aId, is4aUrl, message)
        is4a.setStatus(is4aId, is4aUrl, is4aStatus)
        logger.info("Status from is4a : {0}".format(is4a.getStatus(is4aId, is4aUrl)))

    return {"dictOutputFile": dictOutputFile, "is4aStatus": is4aStatus}
