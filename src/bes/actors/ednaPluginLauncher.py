"""
Created on Mar 3, 2015

@author: svensson
"""

import os
import sys
import time
import traceback

from EDVerbose import EDVerbose
from EDFactoryPluginStatic import EDFactoryPluginStatic


def run(
    inputFile, outputFile, workingDirectory, pluginName, debug=False, timeout=7200, **_
):

    try:
        errorMessage = ""
        ednaSuccess = False

        EDVerbose.screen("Executing EDNA plugin %s" % pluginName)
        EDVerbose.screen("EDNA_SITE %s" % os.environ["EDNA_SITE"])

        if debug:
            EDVerbose.setVerboseDebugOn()
            EDVerbose.screen("DEBUG log messages enabled")
        else:
            EDVerbose.setVerboseOn()

        timeString = time.strftime("%Y%m%d-%H%M%S", time.localtime(time.time()))
        baseName = "EDApplication_" + timeString
        baseDir = os.path.join(workingDirectory, baseName)
        if not os.path.exists(baseDir):
            os.makedirs(baseDir, 0o755)
        EDVerbose.screen("EDNA plugin working directory: %s" % baseDir)
        os.chdir(baseDir)

        ednaLogName = "%s.log" % baseName
        EDVerbose.setLogFileName(os.path.join(workingDirectory, ednaLogName))

        edPlugin = EDFactoryPluginStatic.loadPlugin(pluginName)
        edPlugin.setDataInput(open(inputFile).read())
        edPlugin.setBaseDirectory(baseDir)
        edPlugin.setBaseName(baseName)
        edPlugin.setTimeOut(timeout)
        EDVerbose.screen("Timeout set to %f s" % timeout)
        EDVerbose.screen("Start of execution of EDNA plugin %s" % pluginName)
        edPlugin.executeSynchronous()
        fileOutput = open(outputFile, "w")
        fileOutput.write(edPlugin.dataOutput.marshal())
        fileOutput.close()

        ednaSuccess = True

    except Exception:

        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        errorMessage = "{0} {1}".format(exc_type, exc_value)
        listTrace = traceback.extract_tb(exc_traceback)
        for listLine in listTrace:
            errorMessage += '  File "%s", line %d, in %s%s' % (
                listLine[0],
                listLine[1],
                listLine[2],
                os.linesep,
            )

    return {"errorMessage": errorMessage, "ednaSuccess": ednaSuccess}
