from bes import config
from bes.workflow_lib import dialog
from bes.workflow_lib import collect


def run(
    beamline,
    directory,
    data_collection_no_images,
    data_collection_exposure_time,
    data_collection_osc_range,
    transmission,
    data_collection_iterations,
    resolution=None,
    automatic_mode=False,
    collection_software=None,
    token=None,
    **_,
):

    if resolution is None:
        resolution = collect.getResolution(beamline, token)

    listDialog = [
        {
            "variableName": "test_image_exposure_time",
            "label": "Test image exposure time (s)",
            "type": "float",
            "defaultValue": data_collection_exposure_time,
            "lowerBound": config.get_value(beamline, "Beam", "minCollectExposureTime"),
            "upperBound": 100.0,
        },
        {
            "variableName": "test_image_osc_range",
            "label": "Test image oscillation range (degrees)",
            "type": "float",
            "defaultValue": data_collection_osc_range,
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "resolution",
            "label": "Resolution (A)",
            "type": "float",
            "value": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 10.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission (%)",
            "type": "float",
            "defaultValue": transmission,
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "sensitivity",
            "label": "RDFIT sensitivity",
            "type": "float",
            "defaultValue": 1,
            "lowerBound": 0,
            "upperBound": 100.0,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
