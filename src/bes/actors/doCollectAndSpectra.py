"""
Created on Jun 30, 2015

@author: svensson
"""

import os

from bes.workflow_lib import common
from bes.workflow_lib import spectra
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    noCycles,
    dataCollectionOscillationRange,
    numberOfImages,
    spectralAcquisitionTime,
    spectralAverages,
    collectExposureTime,
    transmission,
    data_collection_start_phi,
    spectrum_start_phi,
    motorPositions,
    directory,
    prefix,
    process_directory,
    resolution,
    sampleInfo,
    pyarch_html_dir=None,
    token=None,
    **_,
):

    try:
        figFileName = None
        spectraNumpyFile = None

        logger = workflow_logging.getLogger(beamline, token=token)

        noCycles = int(noCycles)
        dataCollectionOscillationRange = float(dataCollectionOscillationRange)
        numberOfImages = int(numberOfImages)
        spectralAcquisitionTime = float(spectralAcquisitionTime)
        spectralAverages = int(spectralAverages)
        workflowParameters = common.jsonLoads(workflowParameters)
        motorPositions = common.jsonLoads(motorPositions)
        collectExposureTime = float(collectExposureTime)
        transmission = float(transmission)
        sampleInfo = common.jsonLoads(sampleInfo)
        data_collection_start_phi = float(data_collection_start_phi)
        spectrum_start_phi = float(spectrum_start_phi)
        resolution = float(resolution)

        expTypePrefix = ""
        groupNodeId = None
        sampleNodeId = sampleInfo["nodeId"]
        listNodeId = []
        suffix = "cbf"
        host = "massif3backup.esrf.fr"
        port = 47351

        spectrum = collect.rpcGetSpectrum(host, port)
        logger.info(spectrum)

        allSpectra = None
        spectraNumpyFile = os.path.join(workflowParameters["workingDir"], "spectra.npy")
        htmlDir = os.path.join(workflowParameters["workingDir"], "html")
        os.makedirs(htmlDir, 0o755)
        figFileName = os.path.join(htmlDir, "spectraPlot.png")
        snapShots = True

        allSpectra = spectra.acquireSpectra(
            beamline,
            directory,
            prefix,
            host,
            port,
            allSpectra,
            figFileName,
            spectraNumpyFile,
            spectrum_start_phi,
            listNodeId,
            htmlDir,
            pyarch_html_dir,
            totalSpectra=noCycles,
        )

        for index in range(noCycles):

            mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
                collectExposureTime,
                transmission,
                numberOfImages,
                dataCollectionOscillationRange,
                data_collection_start_phi,
            )
            run_number = index + 1
            dictId = collect.loadQueue(
                beamline,
                directory,
                process_directory,
                prefix,
                expTypePrefix,
                run_number,
                resolution,
                None,
                mxv1StrategyResult,
                motorPositions,
                workflowParameters["type"],
                workflowParameters["index"],
                groupNodeId,
                sampleNodeId,
                enabled=True,
                snapShots=snapShots,
                token=token,
            )
            # Only do snapshots the first iteration
            snapShots = False
            groupNodeId = dictId["group_node_id"]
            listNodeId += dictId["listNodeId"]
            listImagePath = edna_mxv1.getListImagePath(
                mxv1StrategyResult, directory, run_number, expTypePrefix, prefix, suffix
            )
            iDataCollectionGroupId = edna_ispyb.updateDataCollectionGroupWorkflowId(
                beamline,
                directory,
                run_number,
                expTypePrefix,
                prefix,
                suffix,
                listImagePath[0],
                workflowParameters["ispyb_id"],
                workflowParameters["workingDir"],
            )
            edna_ispyb.groupMeshDataCollectionsFromQueueEntry(
                beamline,
                directory,
                listImagePath,
                workflowParameters["workingDir"],
                iDataCollectionGroupId,
            )

            allSpectra = spectra.acquireSpectra(
                beamline,
                directory,
                prefix,
                host,
                port,
                allSpectra,
                figFileName,
                spectraNumpyFile,
                spectrum_start_phi,
                listNodeId,
                htmlDir,
                pyarch_html_dir,
                totalSpectra=noCycles,
            )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "figFileName": figFileName,
        "spectraNumpyFile": spectraNumpyFile,
    }
