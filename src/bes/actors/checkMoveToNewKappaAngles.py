from bes.workflow_lib import workflow_logging, common


def run(
    beamline,
    workflowParameters,
    phi,
    kappa,
    kappa_phi,
    newPhi,
    newKappa,
    newKappa_phi,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    try:

        logger.info(
            "Kappa goiniostat after move: phi=%.3f kappa=%.3f kappa_phi=%.3f"
            % (phi, kappa, kappa_phi)
        )
        epsilon = 0.1
        if not beamline == "simulator":
            if (
                abs(kappa - newKappa) > epsilon
                or abs(kappa_phi - newKappa_phi) > epsilon
            ):
                raise Exception("Kappa goniostat didn't move to new angles!")
            logger.info("Kappa goniostat moved to new angles.")
        else:
            logger.info("Simulator: kappa goniostat didn't move to new angles")
    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
