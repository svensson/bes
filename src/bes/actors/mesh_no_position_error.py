import time

from bes.workflow_lib import ispyb
from bes.workflow_lib import dialog
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    firstImagePath,
    collection_software,
    token=None,
    automatic_mode=False,
    dozorm2ExecutionError=False,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    workflow_working_dir = workflowParameters["workingDir"]

    if dozorm2ExecutionError:
        message_text = "DozorM2 error - no best position found."
    else:
        message_text = "No diffraction."

    # Check that we have flux
    if firstImagePath is not None:
        dictResult = edna_ispyb.getFluxBeamsizeFromImage(
            firstImagePath,
            workflow_working_dir,
        )
        flux = dictResult["flux"]
        if flux is not None and flux < 1e9:
            message_text = message_text[
                :-1
            ] + " probably because very low flux: {0:0.1f} photons/s. ".format(flux)
            message_text += "Please check machine status and/or beamline."
            dialog.displayMessage(
                beamline,
                "No diffraction because of no flux",
                "error",
                message_text,
                token=token,
                automaticMode=automatic_mode,
                collection_software=collection_software,
            )

    logger.error(message_text)
    ispyb.updateDataCollectionComment(beamline, firstImagePath, message_text)
    ispyb.updateDataCollectionGroupComment(beamline, firstImagePath, message_text)
    time.sleep(1)

    return {
        "message_text": message_text,
    }
