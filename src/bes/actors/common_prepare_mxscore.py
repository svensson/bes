def run(beamline, directory, grid_info=None, expTypePrefix="mesh-", **_):

    if grid_info is None:
        if beamline in ["id23eh1", "id29"]:
            grid_info = {
                "x1": 0.20,
                "y1": -0.10,
                "dx_mm": 0.80,
                "dy_mm": 0.20,
                "steps_x": 40,
                "steps_y": 8,
                "angle": 0.0,
            }
        if beamline in ["id30a1"]:
            grid_info = {
                "x1": -0.40,
                "y1": -0.10,
                "dx_mm": 0.80,
                "dy_mm": 0.20,
                "steps_x": 30,
                "steps_y": 8,
                "angle": 0.0,
            }
        elif beamline in ["id23eh2", "simulator"]:
            grid_info = {
                "x1": -0.1,
                "y1": -0.04,
                "dx_mm": 0.30,
                "dy_mm": 0.080,
                "steps_x": 50,
                "steps_y": 8,
                "angle": 0.0,
            }

    workflowParameters = {}
    workflowParameters["title"] = "MXScore: X-centre + eEDNA on %s" % beamline
    workflowParameters["type"] = "MXScore"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": expTypePrefix,
        "resolution": 2.0,
        "meshZigZag": False,
        "grid_info": grid_info,
        "do_data_collect": False,
        "anomalousData": False,
        "doCharacterisation": True,
        "automatic_mode": True,
    }
