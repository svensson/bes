"""
Created on May 7, 2015

@author: svensson
"""

import numpy
import pickle
import ctypes

# from bes.workflow_lib import grid
# from bes.workflow_lib import workflow_logging


def run(**_):
    result = None
    f = open("/tmp/dictValues_0eQX_K.pickle")
    data = pickle.load(f)
    spots1 = data["spots1"]
    spots2 = data["spots2"]
    a = numpy.size(spots1) / 5
    b = numpy.size(spots2) / 5
    spots1 = spots1.astype(ctypes.c_float)
    spots2 = spots2.astype(ctypes.c_float)
    arg1 = spots1.ctypes.data
    arg2 = spots2.ctypes.data
    Clib = ctypes.cdll.LoadLibrary(
        "/data/id23eh1/inhouse/Igor/PROGRAM/libSpotSimilarityDistanceFunction.so"
    )
    Clib.SpotSimilarityDistanceFunction.restype = ctypes.c_char_p
    result = Clib.SpotSimilarityDistanceFunction(a, b, arg1, arg2)
    return {"result": result}


#    beamline = "simulator"
#    workflowParameters = {}
#    workflowParameters["debugLogFile"] = tempfile.mkstemp(suffix=".txt", prefix="BestMesh_", dir="/tmp")[1]
#    logger = workflow_logging.getLogger(beamline, workflowParameters)
#    beamline = "simulator"
#    resultMeshPath = "/data/visitor/mx1791/id23eh1/20161214/PROCESSED_DATA/AS074/AS074-AS8c1_4a/MeshResults/MeshResults_20161214-161400.json"
#    bestPosition = grid.find_best_position_mcv4(resultMeshPath)


#    return { "bestPosition":  bestPosition,
#             "debugLogFile": workflowParameters["debugLogFile"]}
