"""
Created on Mar 3, 2015

@author: svensson
"""

import os
import re
import subprocess

from bes.workflow_lib import mxnice
from bes.workflow_lib import workflow_logging


def run(
    beamline=None,
    dataCollectionId=None,
    ednaDpLaunchPath=None,
    launchPath=None,
    procType=None,
    numberOfImages=None,
    proposal=None,
    nodes=1,
    core=4,
    queue="mx",
    host=None,
    mem=None,
    time="2:00:00",
    **_,
):

    logger = workflow_logging.getLogger(beamline)
    logger.info("In submitSLURMJob.py")

    nodes = int(nodes)
    core = int(core)

    # Dynamic core allocation
    sinfo = subprocess.run(
        args=["sinfo", "-N", "-o", "%n %C", f"-p{queue}"],
        capture_output=True,
        text=True,
    )
    sinfo_output = sinfo.stdout.strip()
    sinfo_lines = sinfo_output.splitlines()
    minimum_number_cores = 12
    max_found_cores = minimum_number_cores
    for machine_line in sinfo_lines:
        groups = machine_line.split()
        if re.search("^mxhpc", groups[0]):
            for group in groups:
                if re.search(r"\/", group):  # Noqa W605
                    machine_stats = group.split("/")
                    idle = int(machine_stats[1])
                    logger.debug(f"Idle cpus: {idle}")
                    if idle > minimum_number_cores and idle > max_found_cores:
                        max_found_cores = idle
    logger.info(f"Max found cores: {max_found_cores}")
    core = int(max_found_cores / 2)
    logger.info(f"Using {core} cores for submission")
    # To be removed when all launchers use launchPath instead of ednaDpLaunchPath
    if launchPath is None:
        launchPath = ednaDpLaunchPath

    # Dynamic memory allocation
    max_memory = int(1000 * core)
    if mem is None or mem < max_memory:
        mem = max_memory
    logger.info(f"Using {mem} MBytes of memory")

    name = "{procType}, {proposal}, {beamline}, {numberOfImages}, id={dataCollectionId}".format(
        procType=procType,
        beamline=beamline,
        numberOfImages=numberOfImages,
        dataCollectionId=dataCollectionId,
        proposal=proposal,
    )
    commandLine = (
        f"export AutoPROCWorkFlowUser=True; export SLURM_NO_CORES={core}; {launchPath}"
    )
    logger.info("commandLine: {0}".format(commandLine))
    workingDirectory = os.path.dirname(launchPath)
    slurmScriptPath, slurmJobId, stdout, stderr = mxnice.submitJobToSLURM(
        commandLine,
        workingDirectory,
        queue=queue,
        mem=mem,
        host=host,
        nodes=nodes,
        core=core,
        time=time,
        name=name,
    )
    logger.info("slurmScriptPath: {0}".format(slurmScriptPath))
    logger.info("slurmJobId: {0}".format(slurmJobId))
    logger.info("stdout: {0}".format(stdout))
    logger.info("stderr: {0}".format(stderr))

    return {
        "slurmScriptPath": slurmScriptPath,
        "slurmJobId": slurmJobId,
        "commandLine": commandLine,
        "workingDirectory": workingDirectory,
        "stdout": stdout,
        "stderr": stderr,
    }
