#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline="simulator",
    directory="directory not yet defined",
    workflowParameters=None,
    message_text="No error message.",
    workflow_title="workflow_title not yet defined",
    workflow_type="workflow_type not yet defined",
    errorMessage=None,
    done_error_report=None,
    token=None,
    **_,
):

    if workflowParameters is None:
        logger = workflow_logging.getLogger(beamline, {}, token=token)
        workflow_title = "Unknown"
        workflow_working_dir = "Unknown"
        workflow_id = None
    else:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        workflow_title = common.checkInputVariable(
            workflowParameters, "title", workflow_title, b_warn_if_default=False
        )
        workflow_type = common.checkInputVariable(
            workflowParameters, "type", workflow_type, b_warn_if_default=False
        )
        workflow_working_dir = common.checkInputVariable(
            workflowParameters,
            "workingDir",
            "workflow_working_dir not yet defined",
            b_warn_if_default=False,
        )
        workflow_id = common.checkInputVariable(
            workflowParameters, "ispyb_id", -1, b_warn_if_default=False
        )

    logger.error("Workflow finished with status failure.")
    if workflow_id is not None:
        edna_ispyb.setWorkflowStatus(
            beamline, workflow_working_dir, workflow_id, "Failure"
        )

    emailList = config.get_value(beamline, "Email", "error", default_value=[])
    if errorMessage is None:
        errorMessage = "Failure"
    common.send_email(
        beamline,
        emailList,
        errorMessage,
        directory,
        workflow_title,
        workflow_type,
        workflow_working_dir,
        message_text,
    )

    return {"message_text": message_text}
