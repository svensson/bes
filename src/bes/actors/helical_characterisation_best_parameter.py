import math

from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    mxv1StrategyResultFile,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    workflow_working_dir,
    helicalDistance,
    dataCollectionType="Oscillation",
    susceptibility=None,
    token=None,
    **_,
):

    try:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        run_number = int(run_number)
        helicalDistance = float(helicalDistance)

        mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)
        dictResult = edna_ispyb.getFluxBeamsize(
            beamline,
            mxv1StrategyResult,
            directory,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            workflow_working_dir,
        )

        beamSizeX = dictResult["beamSizeAtSampleX"]
        beamSizeY = dictResult["beamSizeAtSampleY"]

        if beamSizeX is None or beamSizeX < 0 or beamSizeY is None or beamSizeY < 0:
            logger.warning("Beam size X returned from ISPyB is {0}".format(beamSizeX))
            logger.warning("Beam size Y returned from ISPyB is {0}".format(beamSizeY))

            apertureSize = collect.getApertureSize(beamline, token)
            if apertureSize is None:
                if beamline == "id23eh2":
                    apertureSize = 5
                else:
                    apertureSize = 10

            beamSizeX = apertureSize / 1000
            beamSizeY = apertureSize / 1000
            logger.warning(
                "Setting the beam size X and Y to the aperture size %d um", apertureSize
            )

        bestHelicalParameter = None
        noPositions = None

        if dataCollectionType == "Multi-position":
            noPositions = math.fabs(helicalDistance / beamSizeX) + 1
            logger.info("Best number of positions: %d" % noPositions)
            strategyOption = "-low never -Npos %d" % noPositions
        else:
            # Helical
            bestHelicalParameter = helicalDistance / beamSizeX
            logger.info("Best helical parameter: %.1f" % bestHelicalParameter)
            strategyOption = "-low never -helic %f" % bestHelicalParameter

        if susceptibility is not None:
            strategyOption += " -su {0}".format(round(float(susceptibility), 1))

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "beamSizeX": beamSizeX,
        "beamSizeY": beamSizeY,
        "strategyOption": strategyOption,
        "bestHelicalParameter": bestHelicalParameter,
        "noPositions": noPositions,
    }
