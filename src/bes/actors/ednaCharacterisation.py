"""
Created on Mar 3, 2015

@author: svensson
"""

import os
import sys
import time
import socket
import traceback

from bes.workflow_lib import ispyb

from EDVerbose import EDVerbose
from EDFactoryPluginStatic import EDFactoryPluginStatic

from XSDataCommon import XSDataString
from XSDataCommon import XSDataFile

EDFactoryPluginStatic.loadModule("XSDataMXCuBEv1_4")
from XSDataMXCuBEv1_4 import XSDataInputMXCuBE  # noqa E402


def run(
    inputFile,
    outputFile,
    workingDirectory,
    logFile=None,
    ednaStartScript=None,
    pluginName="EDPluginControlInterfaceToMXCuBEv1_4",
    timeout=600,
    **_,
):

    try:
        errorMessage = ""
        characterisationSuccess = False
        shortSummary = ""
        ednaLogPath = None

        directories = workingDirectory.split(os.path.sep)
        try:
            if directories[2] == "visitor":
                beamline = directories[4]
            else:
                beamline = directories[2]
        except Exception:
            beamline = "unknown"

        xsDataInputMXCuBE = XSDataInputMXCuBE.parseFile(inputFile)
        if xsDataInputMXCuBE.dataCollectionId is None:
            uniqueIdentifier = id(xsDataInputMXCuBE)
        else:
            dataCollectionId = xsDataInputMXCuBE.dataCollectionId.value
            uniqueIdentifier = dataCollectionId
            dataCollection = ispyb.findDataCollection(beamline, dataCollectionId)
            if dataCollection is None:
                raise RuntimeError(
                    "No data collection found in ISPyB with dataCollectionId = {0}".format(
                        dataCollectionId
                    )
                )
            # if dataCollection.runStatus != "Data collection successful":
            #     raise RuntimeError("Data collection failed!")

        xsDataInputMXCuBE.htmlDir = XSDataFile(
            XSDataString(
                os.path.join(workingDirectory, "EDNA_html_{0}".format(uniqueIdentifier))
            )
        )

        # Enable logging in mxCuBE
        # os.environ["mxCuBE_XMLRPC_log"] = "Enabled"

        EDVerbose.screen("Executing EDNA plugin %s" % pluginName)
        EDVerbose.screen("EDNA_SITE %s" % os.environ["EDNA_SITE"])

        EDVerbose.screen("Executing EDNA plugin %s" % pluginName)
        EDVerbose.screen("EDNA_SITE %s" % os.environ["EDNA_SITE"])

        if ednaStartScript is None:
            hostname = socket.gethostname()
            dateString = time.strftime("%Y%m%d", time.localtime(time.time()))
            timeString = time.strftime("%H%M%S", time.localtime(time.time()))
            strPluginBaseDir = os.path.join("/tmp_14_days", beamline, dateString)
            if not os.path.exists(strPluginBaseDir):
                os.makedirs(strPluginBaseDir, 0o755)

            baseName = "{0}_EDNAchar".format(timeString)
            baseDir = os.path.join(strPluginBaseDir, baseName)
            if not os.path.exists(baseDir):
                os.makedirs(baseDir, 0o755)
            EDVerbose.screen("EDNA plugin working directory: %s" % baseDir)

            linkName = "{hostname}_{uniqueIdentifier}".format(
                hostname=hostname, uniqueIdentifier=uniqueIdentifier
            )
            if not os.path.exists(os.path.join(workingDirectory, linkName)):
                os.symlink(baseDir, os.path.join(workingDirectory, linkName))

            ednaLogName = "EDNA_{0}.log".format(uniqueIdentifier)
            logFile = os.path.join(workingDirectory, ednaLogName)
            EDVerbose.setLogFileName(ednaLogPath)
            EDVerbose.setVerboseOn()

            edPlugin = EDFactoryPluginStatic.loadPlugin(pluginName)
            edPlugin.setDataInput(xsDataInputMXCuBE)
            edPlugin.setBaseDirectory(strPluginBaseDir)
            edPlugin.setBaseName(baseName)
            edPlugin.setTimeOut(timeout)

            EDVerbose.screen("Timeout set to %f s" % timeout)
            EDVerbose.screen("Start of execution of EDNA plugin %s" % pluginName)

            edPlugin.executeSynchronous()

            if edPlugin.dataOutput is not None:
                fileOutput = open(outputFile, "w")
                fileOutput.write(edPlugin.dataOutput.marshal())
                fileOutput.close()

                if edPlugin.dataOutput.characterisationResult is not None:
                    if (
                        edPlugin.dataOutput.characterisationResult.shortSummary
                        is not None
                    ):
                        shortSummary = (
                            edPlugin.dataOutput.characterisationResult.shortSummary.value
                        )

                if not edPlugin.isFailure():
                    characterisationSuccess = True

        else:
            os.chmod(ednaStartScript, 0o755)
            os.system(ednaStartScript)
            characterisationSuccess = True
    except Exception:

        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        errorMessage = "{0} {1}".format(exc_type, exc_value)
        listTrace = traceback.extract_tb(exc_traceback)
        for listLine in listTrace:
            errorMessage += '  File "%s", line %d, in %s%s' % (
                listLine[0],
                listLine[1],
                listLine[2],
                os.linesep,
            )

    return {
        "shortSummary": shortSummary,
        "errorMessage": errorMessage,
        "characterisationSuccess": characterisationSuccess,
        "workflowLogFile": logFile,
    }
