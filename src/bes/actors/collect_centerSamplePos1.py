from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, workflow_type, sampleInfo, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        sampleInfo = common.jsonLoads(sampleInfo)
        sample_node_id = sampleInfo["nodeId"]
        group_node_id = sampleInfo["groupNodeId"]

        workflowParameters = common.jsonLoads(workflowParameters)
        workflow_index = workflowParameters["index"]

        logger.info(
            "Please move the sample to the first position and make sure it is centred"
        )

        sampleInfo["groupNodeId"] = collect.addCentringToQueue(
            beamline,
            workflow_type,
            workflow_index,
            group_node_id,
            sample_node_id,
            message="Center first position",
            token=token,
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "sampleInfo": sampleInfo,
    }
