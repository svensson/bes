import math

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import meshbest
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    indexMesh2d,
    run_number,
    newPhi=None,
    noMesh2d=None,
    token=None,
    meshResultsJsonPath=None,
    meshResultsJsonPath1=None,
    **_,
):

    try:
        _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        # Here code should be inserted for calculating the two new angles

        angle1, angle2 = meshbest.methods.nextprojectionangle.determine(
            meshResultsJsonPath1, meshResultsJsonPath
        )

        angle1 = math.degrees(angle1)
        angle2 = math.degrees(angle2)

        motorPositions = collect.readMotorPositions(beamline, token=token)

        currentPhi = motorPositions["phi"]

        angleRelative1 = currentPhi + angle1
        angleRelative2 = currentPhi + angle2

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "currentPhi": currentPhi,
        "angle1": angle1,
        "angle2": angle2,
        "angleRelative1": angleRelative1,
        "angleRelative2": angleRelative2,
    }
