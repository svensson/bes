from bes.workflow_lib import common


def run(
    beamline,
    workflowParameters,
    directory,
    meshPositionFile,
    run_number,
    expTypePrefix,
    workflow_working_dir,
    prefix,
    suffix,
    workflow_id,
    grid_info,
    workflow_title,
    workflow_type,
    data_threshold,
    data_collection_max_positions,
    radius,
    list_node_id=None,
    firstImagePath=None,
    pyarch_html_dir=None,
    diffractionSignalDetection="Dozor (macro-molecules)",
    resultMeshPath=None,
    token=None,
    **_,
):

    try:

        # Crystfel code go here...:
        pass

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
