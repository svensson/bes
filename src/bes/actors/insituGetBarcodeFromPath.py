from bes.workflow_lib import crims
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(beamline, directory, workflowParameters, token=None, **_):

    try:
        url = "https://embl.fr/htxlab/index.php"

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        barcode = crims.getBarcodeFromPath(directory)
        logger.info("Plate bar code: {0}".format(barcode))

        xtalInfo = crims.getBarcodeXtalInfos(url, barcode)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "barcode": barcode,
        "xtalInfo": xtalInfo,
    }
