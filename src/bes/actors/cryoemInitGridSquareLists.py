"""
Initialisation of a workflow
"""

import json
import time

from bes.workflow_lib import cryoem
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    gridSquaresJsonFile,
    currentGridSquare=None,
    newGridSquares=[],
    REFID=None,
    REQUESTID=None,
    **_,
):

    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(None, workflowParameters)
    logger.info("*********** cryoemInitGridSquareLists **********")

    dictGridSquares = json.loads(open(gridSquaresJsonFile).read())
    listGridSquareCreatedSorted = cryoem.updateDictForNewGridSquares(
        directory, dictGridSquares
    )

    if len(listGridSquareCreatedSorted) > 0:
        # Take the oldest one
        ctimeCreated, currentGridSquare = listGridSquareCreatedSorted[0]
        dictGridSquare = dictGridSquares[currentGridSquare]
        dictGridSquare["status"] = "started"
        dictGridSquare["startTime"] = time.ctime(time.time())
        logger.info("New current grid square: {0}".format(currentGridSquare))
        doProcessGridSquare = True
    else:
        logger.info("No more grid squares!")
        doProcessGridSquare = False
        currentGridSquare = None

    open(gridSquaresJsonFile, "w").write(json.dumps(dictGridSquares, indent=4))

    return {
        "doProcessGridSquare": doProcessGridSquare,
        "currentGridSquare": currentGridSquare,
        "listGridSquareCreatedSorted": listGridSquareCreatedSorted,
    }
