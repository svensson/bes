#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "01/03/2021"

from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    osc_range,
    gridExposureTime,
    transmission,
    plateMode,
    gridSnapShots,
    collection_software=None,
    token=None,
    indexMesh2d=None,
    **_,
):
    _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    gridExposureTime = float(gridExposureTime)
    osc_range = float(osc_range)
    transmission = float(transmission)

    listDialog = []
    if beamline in ["id23eh2", "id30a1", "id29", "id30b"]:
        listDialog.append(
            {
                "variableName": "useFastMesh",
                "label": "Use fast mesh (beta)",
                "type": "combo",
                "textChoices": ["true", "false"],
                "value": "true",
            }
        )
    elif beamline in ["id30a3"]:
        listDialog.append(
            {
                "variableName": "useFastMesh",
                "label": "Use fast mesh (beta)",
                "type": "boolean",
                "value": True,
            }
        )
    listDialog += [
        {
            "variableName": "gridExposureTime",
            "label": "Grid exposure time",
            "type": "float",
            "defaultValue": gridExposureTime,
            "unit": "%",
            "lowerBound": config.get_value(beamline, "Beam", "minGridExposureTime"),
            "upperBound": 100.0,
        },
        {
            "variableName": "osc_range",
            "label": "Total oscillation range",
            "type": "float",
            "defaultValue": osc_range,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 1000.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission",
            "type": "float",
            "defaultValue": transmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        collection_software=collection_software,
    )
    if isinstance(dictValues["useFastMesh"], str):
        dictValues["useFastMesh"] = dictValues["useFastMesh"] == "true"

    # Prepare plot title
    dict_diffractometer_positions = collect.readMotorPositions(beamline, token=token)
    phi = dict_diffractometer_positions["phi"]
    dictValues["titleMeshHtmlPage"] = (
        "MeshBest 3D mesh no {0}: centre of sample at phi={1:.1f} deg".format(
            indexMesh2d, phi
        )
    )
    dictValues["meshHtmlDirectoryName"] = "MeshBest3D_{0}".format(indexMesh2d)

    return dictValues
