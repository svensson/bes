import os
import datetime
import tempfile

from bes.workflow_lib import is4a
from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(job, is4aUrl, **_):

    job = common.jsonLoads(job)
    is4aId = job["_id"]
    is4aStatus = job["status"]
    jobInput = job["input"]
    token = job["token"]
    workflowType = "IS4a_{0}".format(job["type"])
    proposal = job["username"]
    workflowParameters = {}
    workflowParameters["type"] = workflowType
    workflowParameters["version"] = "1.0"

    topTmpDir = "/tmp_14_days/is4a"
    if not os.path.exists(topTmpDir):
        os.makedirs(topTmpDir, 0o755)

    workflowWorkingDir = tempfile.mkdtemp(dir=topTmpDir)
    os.chmod(workflowWorkingDir, 0o755)

    workflowParameters["workingDir"] = workflowWorkingDir

    # Create stack trace file
    workflowStackTraceFile = path.createStackTraceFile(workflowWorkingDir)
    workflowParameters["stackTraceFile"] = workflowStackTraceFile

    workflowLogFile = path.createWorkflowLogFilePath(workflowWorkingDir)
    workflowDebugLogFile = path.createWorkflowDebugLogFilePath(workflowWorkingDir)

    workflowParameters["logFile"] = workflowLogFile
    workflowParameters["debugLogFile"] = workflowDebugLogFile

    beamline = None
    logger = workflow_logging.getLogger(beamline, workflowParameters)

    logger.info("Starting {0} workflow".format(workflowType))
    logger.info("Workflow working directory: {0}".format(workflowWorkingDir))

    logger.debug("URL Is4a: {0}".format(is4aUrl))

    #    is4a.setStarted(is4aId, is4aUrl)
    startDate = str(datetime.datetime.now())

    is4a.logMessage(
        is4aId, is4aUrl, "Workflow: starting new job of type {0}".format(job["type"])
    )

    return {
        "workflowParameters": workflowParameters,
        "startDate": startDate,
        "is4aId": is4aId,
        "is4aStatus": is4aStatus,
        "jobInput": jobInput,
        "proposal": proposal,
        "token": token,
    }
