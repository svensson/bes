from bes.workflow_lib import dialog
from bes.workflow_lib import workflow_logging


def run(
    beamline, workflowParameters, directory, collection_software=None, token=None, **_
):
    _ = workflow_logging.getLogger(beamline, workflowParameters)

    listDialog = [
        {
            "variableName": "redoCharacterisation",
            "label": "Redo characterisation (using already collected reference images)",
            "type": "boolean",
            "defaultValue": False,
        }
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        token=token,
        directory=directory,
        automaticMode=False,
        collection_software=collection_software,
    )

    return dictValues
