"""
Workflow module for preparing mesh collection
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2015"
__updated__ = "2021-05-03"

import os
import json

from bes.workflow_lib import path
from bes.workflow_lib import grid
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import dialog
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    raw_directory,
    process_directory,
    workflow_type,
    gridOscillationRange,
    motorPositions,
    beamParameters,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    gridExposureTime,
    transmission,
    useFastMesh=True,
    meshZigZag=False,
    multi_wedge=False,
    collection_software=None,
    token=None,
    shape=None,
    allowZeroDegreeMeshScan=False,
    workflow_index=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    logger.debug("In prepare_mesh_getGridPositions.py")
    logger.debug("allowZeroDegreeMeshScan = {0}".format(allowZeroDegreeMeshScan))
    snapShotFilePath = None

    noImages = None
    message_text = ""
    meshPositions = []
    meshQueueList = []
    newExposureTimeMessage = ""
    hasNewExposureTime = False
    firstImagePath = None
    force_not_shutterless = False

    run_number = int(run_number)
    gridOscillationRange = float(gridOscillationRange)
    gridExposureTime = float(gridExposureTime)
    transmission = float(transmission)
    workflowParameters = common.jsonLoads(workflowParameters)
    workingDir = workflowParameters["workingDir"]
    logger.info("Current motor positions: %r" % motorPositions)
    logger.info("Beam parameters: %r" % beamParameters)

    # Create unique directory for ICAT
    directory, run_number = path.createUniqueICATDirectory(
        raw_directory=raw_directory,
        run_number=run_number,
        expTypePrefix=expTypePrefix,
        workflow_index=workflow_index,
    )

    if beamline in ["id30a2", "simulator_mxcube"]:
        if prefix in ["insu_3_11", "insu_5_20"]:
            grid_info = json.loads(
                """{
    "refs": [],
    "cell_width": 10,
    "height": 122.0423412204234,
    "result": null,
    "y1": 0.42887479999999994,
    "steps_x": 49,
    "steps_y": 33,
    "pixels_per_mm": [
        625.3908692933084,
        622.66500622665
    ],
    "beam_width": 0.01,
    "dx_mm": 0.32,
    "id": "G2",
    "selected": true,
    "cell_count_fun": "top-down-zig-zag",
    "num_cols": 33,
    "label": "Grid",
    "width": 206.37898686679176,
    "state": "SAVED",
    "num_rows": 49,
    "cell_v_space": 0,
    "cell_height": 4,
    "dy_mm": 0.19199999999999998,
    "angle": 0,
    "x1": -0.04957919999999999,
    "hide_threshold": 5,
    "motor_positions": {
        "sampx": -0.48441658511586516,
        "sampy": -0.2222730572018905,
        "phi": 219.5179999999964,
        "kappa": null,
        "kappa_phi": null,
        "zoom": null,
        "focus": null,
        "phiz": -2.223125668750001,
        "phiy": -0.3320075
    },
    "beam_height": 0.004,
    "name": "Grid-2",
    "beam_pos": [
        696,
        523
    ],
    "screen_coord": [
        661.8666666666667,
        788.8
    ],
    "cell_h_space": 0,
    "t": "G"
}"""
            )
        else:
            grid_info = {
                "x1": -0.429,
                "dx_mm": 0.6,
                "dy_mm": 0.145,
                "y1": -0.112,
                "steps_y": 14,
                "steps_x": 24,
                "cell_width": 33,
                "cell_height": 36,
                "beam_width": 30,
                "beam_height": 30,
            }
    else:
        grid_info = collect.getGridInfo(beamline, token=token, shape=shape)

    # BES-318 : prevent 1x1 grids
    if grid_info["steps_x"] == 1 and grid_info["steps_y"] == 1:
        raise RuntimeError("Grid dimensions must be bigger than 1x1!")

    # Use the motor positions coming from the grid
    # if "motor_positions" in grid_info:
    #     motorPositions = grid_info["motor_positions"]
    #     logger.info("Using motor positions from grid: %r" % motorPositions)

    if collection_software == "MXCuBE - 3.0":
        grid_info["x1"] = grid_info["x1"] + grid_info["beam_width"] / 2.0
        grid_info["y1"] = grid_info["y1"] + grid_info["beam_height"] / 2.0
        grid_info["dx_mm"] = grid_info["dx_mm"] - grid_info["beam_width"]
        grid_info["dy_mm"] = grid_info["dy_mm"] - grid_info["beam_height"]

    # zigzag and multi_wedge
    if useFastMesh:
        meshZigZag = True
        multi_wedge = False

    # Check oscillation range
    if gridOscillationRange > 25:
        dialog.displayMessage(
            beamline,
            "Large oscillation range",
            "warning",
            "The oscillation range is very large: {0} degrees.\n".format(
                gridOscillationRange
            )
            + "A large oscillation range may result in poor accuracy for position determination.\n\n"
            + "If this was not intended please abort the workflow now (mxCuBE red stop button), re-define the oscillation range and restart the workflow.\n"
            + "If on the other hand this large oscillation range is intended click on the continue button.",
            automaticMode=False,
            collection_software=collection_software,
            token=token,
        )

    # Check if grid has gaps
    isHelicalDataCollection = True
    if "cell_h_space" in grid_info and "cell_v_space" in grid_info:
        gap_x = float(grid_info["cell_h_space"])
        gap_y = float(grid_info["cell_v_space"])
    else:
        gap_x, gap_y = grid.determineMeshGaps(grid_info)
    if beamline == "id23eh2" and gap_y is not None and gap_y > 0.001:
        dialog.displayMessage(
            beamline,
            "Non-shutterless data collection",
            "warning",
            "Positive vertical grid gap detected ({0} um), therefore helical shutterless scan disabled.\n\n".format(
                int(gap_y * 1000)
            )
            + "If this was not intended please abort the workflow now (mxCuBE red stop button), re-define the grid parameters and restart the workflow.",
            automaticMode=False,
            collection_software=collection_software,
            token=token,
        )
        isHelicalDataCollection = False
        force_not_shutterless = True
    elif gap_x is not None and gap_x > 0.001:
        dialog.displayMessage(
            beamline,
            "Non-shutterless data collection",
            "warning",
            "Positive horizontal grid gap detected ({0} um), therefore helical shutterless scan disabled.\n\n".format(
                int(gap_x * 1000)
            )
            + "If this was not intended please abort the workflow now (mxCuBE red stop button), re-define the grid parameters and restart the workflow.",
            automaticMode=False,
            collection_software=collection_software,
            token=token,
        )
        isHelicalDataCollection = False
        force_not_shutterless = True

    if grid_info == {}:
        message_text = (
            "No grid obtained from mxCuBE! Please draw a grid and restart the workflow."
        )
    elif grid_info["steps_x"] == 0:
        message_text = "Horizontal grid dimension is zero! Please redraw a valid grid and restart the workflow."
        grid_info = {}
    elif grid_info["steps_y"] == 0:
        message_text = "Vertical grid dimension is zero! Please redraw a valid grid and restart the workflow."
        grid_info = {}
    elif grid_info["steps_x"] < 0:
        message_text = "Horizontal grid dimension is negative! Please redraw a valid grid and restart the workflow."
        grid_info = {}
    elif grid_info["steps_y"] < 0:
        message_text = "Vertical grid dimension is negative! Please redraw a valid grid and restart the workflow."
        grid_info = {}
    else:
        noImages = grid_info["steps_x"] * grid_info["steps_y"]
        logger.debug("No images: {0}".format(noImages))
        if useFastMesh and noImages > 9999 and beamline not in ["id23eh2"]:
            message_text = "Fast mesh is incompatible with current grid size: {0} images! Please redraw a smaller grid with less than 10000 images or don't use fast mesh.".format(
                noImages
            )
            grid_info = {}
        else:
            if workflow_type == "LineScan":
                # Make grid 1D:
                grid_info["steps_y"] = 1
                logger.info("1D mesh requested, forcing grid ny to 1.")

            # Check minimum oscillation range
            if not allowZeroDegreeMeshScan and beamline != "id23eh2":
                if gridOscillationRange > 0:
                    dx_mm = grid_info["dx_mm"]
                    maxDistPerDegree = config.get_value(
                        beamline, "Goniometer", "maxDistPerDegree", default_value=None
                    )
                    if (
                        maxDistPerDegree is not None
                        and dx_mm / gridOscillationRange > maxDistPerDegree
                    ):
                        logger.warning(
                            "Oscillation range %.1f degrees too short for beamline %s!"
                            % (gridOscillationRange, beamline)
                        )
                        gridOscillationRange = dx_mm / maxDistPerDegree
                        logger.warning(
                            "New oscialltion range: %.1f degrees" % gridOscillationRange
                        )
                else:
                    logger.warning("*" * 40)
                    logger.warning(
                        "Zero degree mesh scan not allowed but grid oscillation range is zero!"
                    )
                    logger.warning("*" * 40)

            # Mesh positions:
            meshPositions = grid.determineMeshPositions(
                beamline,
                motorPositions,
                gridOscillationRange,
                run_number,
                expTypePrefix,
                prefix,
                suffix,
                grid_info,
                meshZigZag,
                multi_wedge=multi_wedge,
            )

            # Check if we should force not shutterless, i.e. if the grid has a positive overlap
            # TODO: waiting for relevant info from mxCuBE...

            firstImagePath = None
            for position in meshPositions:
                if position["index"] == 0:
                    firstImagePath = os.path.join(directory, position["imageName"])
                    logger.info("First image path %s" % firstImagePath)
                    break

            if not isHelicalDataCollection:
                meshQueueList = collect.createMeshQueueList(
                    beamline,
                    directory,
                    process_directory,
                    run_number,
                    expTypePrefix,
                    prefix,
                    meshPositions,
                    motorPositions,
                    gridExposureTime,
                    gridOscillationRange,
                    transmission,
                )
            elif multi_wedge:
                meshQueueList = collect.createSerialDataCollectionQueueList(
                    beamline,
                    directory,
                    process_directory,
                    run_number,
                    expTypePrefix,
                    prefix,
                    meshPositions,
                    motorPositions,
                    gridExposureTime,
                    gridOscillationRange,
                    transmission,
                    grid_info,
                    meshZigZag,
                )
            else:
                meshQueueList = collect.createHelicalQueueList(
                    beamline,
                    directory,
                    process_directory,
                    run_number,
                    expTypePrefix,
                    prefix,
                    meshPositions,
                    motorPositions,
                    gridExposureTime,
                    gridOscillationRange,
                    transmission,
                    grid_info,
                    meshZigZag,
                )
            if beamline != "bm14":
                dictNewParameters = collect.checkMotorSpeed(
                    beamline, meshQueueList, gridExposureTime, transmission
                )
                if dictNewParameters is not None:
                    hasNewExposureTime = False
                    gridExposureTime = dictNewParameters["gridExposureTime"]
                    transmission = dictNewParameters["transmission"]
                    if multi_wedge:
                        meshQueueList = collect.createSerialDataCollectionQueueList(
                            beamline,
                            directory,
                            process_directory,
                            run_number,
                            expTypePrefix,
                            prefix,
                            meshPositions,
                            motorPositions,
                            gridExposureTime,
                            gridOscillationRange,
                            transmission,
                            grid_info,
                            meshZigZag,
                        )
                    elif not isHelicalDataCollection:
                        meshQueueList = collect.createMeshQueueList(
                            beamline,
                            directory,
                            process_directory,
                            run_number,
                            expTypePrefix,
                            prefix,
                            meshPositions,
                            motorPositions,
                            gridExposureTime,
                            gridOscillationRange,
                            transmission,
                        )
                    else:
                        meshQueueList = collect.createHelicalQueueList(
                            beamline,
                            directory,
                            process_directory,
                            run_number,
                            expTypePrefix,
                            prefix,
                            meshPositions,
                            motorPositions,
                            gridExposureTime,
                            gridOscillationRange,
                            transmission,
                            grid_info,
                            meshZigZag,
                        )
                    newExposureTimeMessage = (
                        "In order to perform the scan with optimal motor speed\n"
                    )
                    newExposureTimeMessage += (
                        "the exposure time has been changed to %.3f s\n"
                        % gridExposureTime
                    )
                    newExposureTimeMessage += (
                        "and the transmission changed accordingly to %.1f %%.\n"
                        % transmission
                    )
                    #                 newExposureTimeMessage += "You can change these parameters but be aware that decreasing\n"
                    #                 newExposureTimeMessage += "the exposure time might lead to inaccuracy of position determination."
                    logger.info(newExposureTimeMessage)

    if grid_info == {}:
        gridInfoIsOk = False
        dialog.displayMessage(
            beamline,
            "Grid input",
            "warning",
            message_text,
            automaticMode=False,
            token=token,
            collection_software=collection_software,
        )
    else:
        gridInfoIsOk = True
        # Take a crystal snapshot before mesh
        snapShotFileName = "%s_%d_snapshot_before_mesh.png" % (prefix, run_number)
        snapShotDirectory = path._getPyarchFilePath(directory)
        snapShotFilePath = collect.takeOneCrystalSnapshot(
            beamline,
            directory,
            prefix,
            run_number,
            snapShotFileName,
            snapShotDirectory,
            token=token,
        )

    meshPositionFile = path.createNewJsonFile(
        workingDir, meshPositions, "meshPositionsBefore"
    )
    meshQueueListFile = path.createNewJsonFile(
        workingDir, meshQueueList, "meshQueueListFileBefore"
    )

    return {
        "directory": directory,
        "run_number": run_number,
        "grid_info": grid_info,
        "gridInfoIsOk": gridInfoIsOk,
        "meshPositionFile": meshPositionFile,
        "meshQueueListFile": meshQueueListFile,
        "hasNewExposureTime": hasNewExposureTime,
        "newExposureTimeMessage": newExposureTimeMessage,
        "message_text": message_text,
        "firstImagePath": firstImagePath,
        "force_not_shutterless": force_not_shutterless,
        "gridOscillationRange": gridOscillationRange,
        "snapShotFilePath": snapShotFilePath,
        "meshZigZag": meshZigZag,
        # "grid_info_mxcube": grid_info_mxcube,
        "gridExposureTime": gridExposureTime,
        "transmission": transmission,
    }
