import sys
import traceback

from bes.workflow_lib import edna_mxv1


def run(outputFile, indexingOutput, **_):

    ednaIndexingOutput = None
    errorMessage = None
    listTrace = None

    try:
        ednaIndexingOutput = edna_mxv1.createEdna2CharacterisationOutput(outputFile)
    except Exception:
        (exc_type, exc_value, exc_traceback) = sys.exc_info()
        errorMessage = "{0} {1}".format(exc_type, exc_value)
        listTrace = traceback.extract_tb(exc_traceback)

    return {
        "ednaIndexingOutput": ednaIndexingOutput,
        "edna2IndexingOutput": indexingOutput,
        "errorMessage": errorMessage,
        "listTrace": listTrace,
    }
