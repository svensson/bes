import os

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    raw_directory,
    process_directory,
    run_number,
    prefix,
    suffix,
    gridExposureTime,
    gridOscillationRange,
    lineScanLength,
    lineScanOverSampling,
    gridTransmission=None,
    token=None,
    **_,
):
    expTypePrefix = "line-"
    # Create unique directory for ICAT
    directory, run_number = path.createUniqueICATDirectory(
        raw_directory=raw_directory,
        run_number=run_number,
        workflow_index=None,
        expTypePrefix=expTypePrefix,
    )

    motorPositions = collect.readMotorPositions(beamline, token=token)
    savedMotorPositions = collect.readMotorPositions(beamline, token=token)
    workflowParameters = common.jsonLoads(workflowParameters)
    workingDir = workflowParameters["workingDir"]
    gridExposureTime = float(gridExposureTime)
    gridOscillationRange = float(gridOscillationRange)
    run_number = int(run_number) + 1

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    logger.info("Preparing vertical line scan for axis centring")

    apertureSize = collect.getApertureSize(beamline, token=token)
    if apertureSize is None:
        raise RuntimeError("Cannot get the current aperture size")
    logger.info("Current aperture size: %d um", apertureSize)
    beamSize = apertureSize / 1000.0
    logger.info("Beam size: %f mm", beamSize)
    noSteps = int(lineScanLength / beamSize * lineScanOverSampling)
    rotationAxis = config.get_value(beamline, "Goniometer", "rotationAxis")
    isHorizontalRotationAxis = rotationAxis == "horizontal"

    if isHorizontalRotationAxis:
        deltaY = lineScanLength
        grid_info = {
            "x1": 0.0,
            "y1": -deltaY / 2.0,
            "dx_mm": 0.1,
            "dy_mm": deltaY,
            "steps_x": 1,
            "steps_y": noSteps,
            "angle": 0.0,
        }
    else:
        deltaX = lineScanLength
        grid_info = {
            "x1": -deltaX / 2.0,
            "y1": 0.0,
            "dx_mm": deltaX,
            "dy_mm": 0.1,
            "steps_x": noSteps,
            "steps_y": 1,
            "angle": 0.0,
        }

    if gridTransmission is None:
        transmission = config.get_value(beamline, "Beam", "defaultGridTransmission")
    else:
        transmission = float(gridTransmission)

    # Mesh positions:
    meshPositions = grid.determineMeshPositions(
        beamline,
        motorPositions,
        gridOscillationRange,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
        isRotationAxisScan=True,
    )

    firstImagePath = None
    for position in meshPositions:
        if position["index"] == 0:
            firstImagePath = os.path.join(directory, position["imageName"])
            logger.info("First image path %s" % firstImagePath)
            break

    meshQueueList = collect.createHelicalQueueList(
        beamline,
        directory,
        process_directory,
        run_number,
        expTypePrefix,
        prefix,
        meshPositions,
        motorPositions,
        gridExposureTime,
        gridOscillationRange,
        transmission,
        grid_info,
    )

    dictNewParameters = collect.checkMotorSpeed(
        beamline, meshQueueList, gridExposureTime, transmission
    )
    if dictNewParameters is not None:
        gridExposureTime = dictNewParameters["gridExposureTime"]
        transmission = dictNewParameters["transmission"]
        meshQueueList = collect.createHelicalQueueList(
            beamline,
            directory,
            process_directory,
            run_number,
            expTypePrefix,
            prefix,
            meshPositions,
            motorPositions,
            gridExposureTime,
            gridOscillationRange,
            transmission,
            grid_info,
        )
        newExposureTimeMessage = (
            "In order to perform the scan with optimal motor speed\n"
        )
        newExposureTimeMessage += (
            "the exposure time has been changed to %.3f s\n" % gridExposureTime
        )
        newExposureTimeMessage += (
            "and the transmission changed accordingly to %.1f %%.\n" % transmission
        )
        logger.info(newExposureTimeMessage)

    titleMeshHtmlPage = (
        "Vertical line scan: Center of rotation axis at phi={0:.1f} deg".format(
            motorPositions["phi"]
        )
    )
    meshHtmlDirectoryName = "Rotation axis scan"

    meshPositionFile = path.createNewJsonFile(
        workingDir, meshPositions, "meshPositionsAxisCentring"
    )
    meshQueueListFile = path.createNewJsonFile(
        workingDir, meshQueueList, "meshQueueListFileAxisCentring"
    )

    return {
        "grid_info": grid_info,
        "directory": directory,
        "shape": None,
        "meshPositionFile": meshPositionFile,
        "expTypePrefix": expTypePrefix,
        "meshQueueListFile": meshQueueListFile,
        "savedMotorPositions": savedMotorPositions,
        "firstImagePath": firstImagePath,
        "meshUpperThreshold": 0.25,
        "titleMeshHtmlPage": titleMeshHtmlPage,
        "meshHtmlDirectoryName": meshHtmlDirectoryName,
    }
