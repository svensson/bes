from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, energy=None, token=None, **_):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        energy = collect.getEnergy(beamline, token=token)

        logger.info("Beamline {0} energy is at {1} keV".format(beamline, energy))

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "energy1": round(energy - 0.01, 3),
        "energy2": round(energy + 0.01, 3),
        "energy3": 0.0,
        "energy4": 0.0,
    }
