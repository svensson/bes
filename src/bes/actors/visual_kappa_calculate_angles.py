from bes.workflow_lib import workflow_logging, collect, kappa_reorientation, common


def run(
    beamline,
    workflowParameters,
    directory,
    phi,
    sampx1,
    sampy1,
    phiy1,
    sampx2,
    sampy2,
    phiy2,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        sampx1 = float(sampx1)
        sampy1 = float(sampy1)
        phiy1 = float(phiy1)
        sampx2 = float(sampx2)
        sampy2 = float(sampy2)
        phiy2 = float(phiy2)

        target_vector = [-(sampx2 - sampx1), sampy2 - sampy1, phiy2 - phiy1]

        new_sample_pos = {"sampx": "", "sampy": "", "phiy": ""}

        logger.debug("Vector to be aligned: %r" % target_vector)
        dictKappa = kappa_reorientation.alignVectorWithOmegaAxis(
            beamline, target_vector
        )

        if dictKappa["status"] != "ok":
            logger.error(dictKappa["message_text"])
        else:
            logger.info("Moving sample to the middle between the two input positions.")
            mid_sampx = (sampx1 + sampx2) / 2.0
            mid_sampy = (sampy1 + sampy2) / 2.0
            mid_phiy = (phiy1 + phiy2) / 2.0

            if beamline in ["id30b"]:
                new_sample_pos = kappa_reorientation.getNewSamplePosition(
                    beamline,
                    0.0,
                    0.0,
                    mid_sampx,
                    mid_sampy,
                    mid_phiy,
                    dictKappa["kappa"],
                    dictKappa["phi"],
                )
            else:
                new_sample_pos = {
                    "sampx": mid_sampx,
                    "sampy": mid_sampy,
                    "phiy": mid_phiy,
                }

            collect.moveMotors(beamline, directory, new_sample_pos, token=token)

            logger.info(
                "New sample position: sampx=%.3f mm, sampy=%.3f mm and phiy=%.3f mm"
                % (
                    new_sample_pos["sampx"],
                    new_sample_pos["sampy"],
                    new_sample_pos["phiy"],
                )
            )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "visualKappaStatus": dictKappa["status"] == "ok",
        "message_text": dictKappa["message_text"],
        "newPhi": phi,
        "newKappa": dictKappa["kappa"],
        "newKappa_phi": dictKappa["phi"],
        "newSampx": new_sample_pos["sampx"],
        "newSampy": new_sample_pos["sampy"],
        "newPhiy": new_sample_pos["phiy"],
    }
