#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "01/02/2021"

from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    raw_directory,
    workflowParameters,
    directory,
    process_directory,
    expTypePrefix,
    prefix,
    suffix,
    grid_info,
    gridOscillationRange,
    gridExposureTime,
    transmission,
    run_number,
    deltaOmega,
    motorInitialPositions,
    dozorAllFile=None,
    useFastMesh=True,
    meshZigZag=False,
    multi_wedge=False,
    token=None,
    workflow_index=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    workingDir = workflowParameters["workingDir"]

    # zigzag and multi_wedge
    if useFastMesh:
        meshZigZag = True
        multi_wedge = False

    run_number = int(run_number)
    # Remove phi from motorInitialPositions
    new_motor_positions = dict(motorInitialPositions)
    if "phi" in new_motor_positions:
        del new_motor_positions["phi"]
    collect.moveMotors(beamline, directory, new_motor_positions, token=token)
    motorPositions = collect.readMotorPositions(beamline, token=token)
    currentPhi = motorPositions["phi"]

    newPhi = currentPhi + deltaOmega
    logger.info(
        "Rotating sample omega from %.3f to %.3f degrees" % (currentPhi, newPhi)
    )

    collect.moveMotors(beamline, directory, {"phi": newPhi}, token=token)
    motorPositions = collect.readMotorPositions(beamline, token=token)

    run_number += 1

    # Create unique directory for ICAT
    directory, run_number = path.createUniqueICATDirectory(
        raw_directory=raw_directory,
        run_number=run_number,
        expTypePrefix=expTypePrefix,
        workflow_index=workflow_index,
    )

    meshPositions = grid.determineMeshPositions(
        beamline,
        motorPositions,
        gridOscillationRange,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
        meshZigZag,
        multi_wedge=multi_wedge,
    )

    meshQueueList = collect.createHelicalQueueList(
        beamline=beamline,
        directory=directory,
        process_directory=process_directory,
        run_number=run_number,
        expTypePrefix=expTypePrefix,
        prefix=prefix,
        meshPositions=meshPositions,
        motorPositions=motorPositions,
        exposureTime=gridExposureTime,
        osc_range=gridOscillationRange,
        transmission=transmission,
        grid_info=grid_info,
        meshZigZag=meshZigZag,
    )

    meshPositionFile = path.createNewJsonFile(
        workingDir, meshPositions, "meshPositions"
    )
    meshQueueListFile = path.createNewJsonFile(
        workingDir, meshQueueList, "meshQueueListFile"
    )

    return {
        "newPhi": newPhi,
        "run_number": run_number,
        "directory": directory,
        "meshPositionFile": meshPositionFile,
        "meshQueueListFile": meshQueueListFile,
        "dozorAllFile1": dozorAllFile,
        "phi1": currentPhi,
        "phi2": newPhi,
    }
