from bes.workflow_lib import path
from bes import config
from bes.workflow_lib import common
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    sampleInfo={},
    dataCollectionGroupComment="",
    numberOfPositions=1,
    do_data_collect=True,
    doCharacterisation=True,
    anomalousData=False,
    do2DMeshOnly=False,
    doOnlyEDNAResolution=False,
    diffractionSignalDetection="DozorM2 (macro-molecules crystal detection)",
    findLargestMesh=False,
    aimedCompleteness=None,
    aimedMultiplicity=None,
    aimedResolution=None,
    observedResolution=None,
    minOscWidth=None,
    sampleSusceptibility=None,
    moreSamples=False,
    targetApertureName=None,
    doAutoMesh=True,
    aimedIOverSigmaAtHighestResolution=1.0,
    doPseudoHelical=False,
    token=None,
    dozorThreshold=0.1,
    strategyOption="-low never",
    axisRange=None,
    doFbest=None,
    openKappaForLowSymmetry=True,
    runGphlWorkflow=False,
    doTwoMeshes=False,
    deltaOmega=90,
    synchronizeBestPosition=True,
    **_,
):
    sample_info = common.jsonLoads(sampleInfo)
    workflowParameters = common.jsonLoads(workflowParameters)
    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
    diffractionPlanComment = "Predefined parameters:"

    diffractionSignalDetection = config.get_value(
        beamline,
        "Mesh",
        "diffractionSignalDetection",
        default_value="Dozor (macro-molecules)",
    )

    if doTwoMeshes:
        doTwoMeshes = True
        deltaOmega = 90
        synchronizeBestPosition = False
    elif config.get_value(beamline, "Mesh", "twoMeshes", default_value=False):
        doTwoMeshes = True
        deltaOmega = 90
        synchronizeBestPosition = False

    if workflowParameters["type"] == "MXPressA":
        if (
            "experimentKind" not in sample_info
            or sample_info["experimentKind"] == "Default"
            or sample_info["experimentKind"] == "None"
        ):
            sample_info["experimentKind"] = "MXPressE"
        # 'Default','MXPressE','MXPressO','MXPressE_SAD','MXScore','MXPressM','MAD','SAD','Fixed','Ligand binding','Refinement','OSC','MAD - Inverse Beam','SAD - Inverse Beam
        if sample_info["experimentKind"] == "MXPressE_SAD":
            diffractionPlanComment += " experiment kind = {0},".format(
                sample_info["experimentKind"]
            )
            sample_info["experimentKind"] = "MXPressE"
            anomalousData = True
        elif sample_info["experimentKind"] == "MXPressO":
            doCharacterisation = False
        elif sample_info["experimentKind"] == "MXScore":
            do_data_collect = False
        elif sample_info["experimentKind"] == "MXPressM":
            do_data_collect = False
            doCharacterisation = False
            do2DMeshOnly = True
            findLargestMesh = True
        elif sample_info["experimentKind"] == "MXPressI":
            doOnlyEDNAResolution = True
            strategyOption = "-low never"
        elif sample_info["experimentKind"] == "MXPressP":
            doPseudoHelical = True
            anomalousData = False
            # diffractionSignalDetection = "Dozor (macro-molecules)"
            openKappaForLowSymmetry = False
        elif sample_info["experimentKind"] == "MXPressP_SAD":
            doPseudoHelical = True
            anomalousData = True
            # diffractionSignalDetection = "Dozor (macro-molecules)"
            openKappaForLowSymmetry = False
        elif sample_info["experimentKind"] == "MXPressL":
            doCharacterisation = False
        elif sample_info["experimentKind"] == "MXPressF":
            doCharacterisation = False
            doFbest = True
        elif sample_info["experimentKind"] == "MXPressK":
            openKappaForLowSymmetry = False

        # Update ispyb
        _ = edna_ispyb.storeOrUpdateWorkflow(
            beamline,
            workflowId=workflowParameters["ispyb_id"],
            workflowTitle=workflowParameters["title"],
            workflowType=sample_info["experimentKind"],
            workflowPyarchLogFile=workflowParameters["workflowPyarchLogFile"],
            workflowLogFile=workflowParameters["workflowLogFile"],
            pyarchHtmlDir=workflowParameters["pyarch_html_dir"],
            workflowWorkingDir=workflowParameters["workflowWorkingDir"],
        )

    # Check if default parameters set by "setup" workflow
    mxpress_parameters = path.getDefaultParameters(
        directory, file_prefix="mxpress_setup"
    )

    # Check if simulataed position and/or characterisation
    simulated_grid_positions = False
    if (
        "simulated_grid_positions" in mxpress_parameters
        and mxpress_parameters["simulated_grid_positions"]
    ):
        diffractionPlanComment += " WARNING! Simulated best position(s),"
        simulated_grid_positions = True
    simulated_characterisation_results = False
    if (
        "simulated_characterisation_results" in mxpress_parameters
        and mxpress_parameters["simulated_characterisation_results"]
    ):
        diffractionPlanComment += " WARNING! Simulated characterisation results,"
        simulated_characterisation_results = True

    # Check if multiple positions:
    provenance = None
    if (
        "numberOfPositions" in mxpress_parameters
        and mxpress_parameters["numberOfPositions"] != "Diffraction plan"
    ):
        numberOfPositions = int(mxpress_parameters["numberOfPositions"])
        provenance = "mxpress setup"
        logger.debug(f"mxpress setup number of positions {numberOfPositions}")
    elif "numberOfPositions" in sample_info:
        numberOfPositions = sample_info["numberOfPositions"]
        provenance = "ISPyB"
        logger.debug(f"ISPyB number of positions {numberOfPositions}")
    elif doPseudoHelical:
        # Default 5 positions
        numberOfPositions = 5
        dozorThreshold = 0.3
        provenance = "pseudo-helical"
    if numberOfPositions > 1:
        findLargestMesh = True
        diffractionPlanComment += f" required number of positions = {numberOfPositions}"
        if provenance is not None:
            diffractionPlanComment += f" ({provenance})"
        diffractionPlanComment += ","

    if "aimedMultiplicity" in sample_info:
        aimedMultiplicity = sample_info["aimedMultiplicity"]
    elif "requiredMultiplicity" in sample_info:
        aimedMultiplicity = sample_info["requiredMultiplicity"]

    if "axisRange" in sample_info:
        axisRange = sample_info["axisRange"]
    if axisRange is not None:
        diffractionPlanComment += " total rotation = {0:.1f} degrees,".format(
            float(axisRange)
        )
        if aimedMultiplicity is not None:
            dataCollectionGroupComment = (
                "Warning! Cannot set aimed multiplicity as total rotation is forced. "
                + dataCollectionGroupComment
            )
            aimedMultiplicity = None
    if aimedMultiplicity is not None:
        diffractionPlanComment += " aimed multiplicity = {0:.1f},".format(
            float(aimedMultiplicity)
        )

    if "aimedCompleteness" in sample_info:
        aimedCompleteness = sample_info["aimedCompleteness"]
    elif "requiredCompleteness" in sample_info:
        aimedCompleteness = sample_info["requiredCompleteness"]
    if aimedCompleteness is not None:
        diffractionPlanComment += " aimed completeness = {0:.1f} %,".format(
            100.0 * float(aimedCompleteness)
        )

    provenance = None
    if (
        "aimedResolution" in mxpress_parameters
        and mxpress_parameters["aimedResolution"] != "Diffraction plan"
    ):
        aimedResolution = float(mxpress_parameters["aimedResolution"])
        provenance = "mxpress setup"
    elif "aimedResolution" in sample_info:
        aimedResolution = sample_info["aimedResolution"]
        provenance = "ISPyB"
    if aimedResolution is not None:
        diffractionPlanComment += f" aimed resolution = {float(aimedResolution):.2f} A,"
        if provenance is not None:
            diffractionPlanComment += f" ({provenance})"
        diffractionPlanComment += ","

    if "observedResolution" in sample_info:
        observedResolution = sample_info["observedResolution"]
    if observedResolution is not None:
        diffractionPlanComment += " observed resolution = {0:.2f} A,".format(
            float(observedResolution)
        )

    if "radiationSensitivity" in sample_info:
        sampleSusceptibility = sample_info["radiationSensitivity"]
    if sampleSusceptibility is not None:
        diffractionPlanComment += " radiation sensitivity = {0:.2f},".format(
            float(sampleSusceptibility)
        )

    if "minOscWidth" in sample_info:
        minOscWidth = sample_info["minOscWidth"]
    if minOscWidth is not None and doCharacterisation:
        if float(minOscWidth) <= 1.0:
            diffractionPlanComment += " min osc width = {0:.2f} degrees,".format(
                float(minOscWidth)
            )
        else:
            logger.warning(
                "Ignoring min osc width: {0:.2f}.".format(float(minOscWidth))
            )
            minOscWidth = None

    provenance = None
    if (
        "targetApertureName" in mxpress_parameters
        and "Default" not in mxpress_parameters["targetApertureName"]
    ):
        targetApertureName = mxpress_parameters["targetApertureName"]
        provenance = "mxpress setup"
    elif "preferredBeamDiameter" in sample_info:
        preferredBeamDiameter = int(sample_info["preferredBeamDiameter"])
        logger.debug(f"sample_info preferredBeamDiameter: {preferredBeamDiameter} um")
        targetApertureSize = aperture_utils.closest_aperture_size(
            beamline, preferredBeamDiameter
        )
        targetApertureName = aperture_utils.aperture_size_to_name(
            beamline, targetApertureSize
        )
        logger.debug(f"targetApertureName: {targetApertureName}")
        if targetApertureSize != preferredBeamDiameter:
            diffractionPlanComment += f" Warning! Preferred beam diameter {preferredBeamDiameter} not obtainable."
            diffractionPlanComment += " Closest aperture chosen."
        provenance = "ISPyB"
    if targetApertureName is not None:
        diffractionPlanComment += f" target aperture = {targetApertureName}"
        if provenance is not None:
            diffractionPlanComment += f" ({provenance})"
        diffractionPlanComment += ","

    if (
        "diffractionSignalDetection" in mxpress_parameters
        and "Default" not in mxpress_parameters["diffractionSignalDetection"]
    ):
        diffractionSignalDetection = mxpress_parameters["diffractionSignalDetection"]
        diffractionPlanComment += f" diffraction signal detection = {diffractionSignalDetection} (mxpress setup),"

    if (
        "loopMinWidth" in mxpress_parameters
        and "Default" not in mxpress_parameters["loopMinWidth"]
    ):
        loopMinWidth = float(mxpress_parameters["loopMinWidth"])
        diffractionPlanComment += (
            f" auto-mesh loop min width = {loopMinWidth} (mxpress setup),"
        )
    else:
        loopMinWidth = float(config.get_value(beamline, "AutoMesh", "loopMinWidth"))

    if (
        "loopMaxWidth" in mxpress_parameters
        and "Default" not in mxpress_parameters["loopMaxWidth"]
    ):
        loopMaxWidth = float(mxpress_parameters["loopMaxWidth"])
        diffractionPlanComment += (
            f" auto-mesh loop max width = {loopMaxWidth} (mxpress setup),"
        )
    else:
        loopMaxWidth = float(config.get_value(beamline, "AutoMesh", "loopMaxWidth"))

    if not doAutoMesh:
        diffractionPlanComment += " no auto mesh,"

    if diffractionPlanComment.endswith(","):
        if dataCollectionGroupComment == "":
            dataCollectionGroupComment = diffractionPlanComment[0:-1] + ";"
        else:
            dataCollectionGroupComment = (
                diffractionPlanComment[0:-1] + "; " + dataCollectionGroupComment
            )

    logger.debug(f"diffractionPlanComment: {diffractionPlanComment}")
    # Fake values for testing of GPhL workflows
    # aimedResolution = 1.363
    # observedResolution = 1.363

    # numberOfPositions = 2

    return {
        "do_data_collect": do_data_collect,
        "anomalousData": anomalousData,
        "doCharacterisation": doCharacterisation,
        "do2DMeshOnly": do2DMeshOnly,
        "dataCollectionGroupComment": dataCollectionGroupComment,
        "doOnlyEDNAResolution": doOnlyEDNAResolution,
        "aimedCompleteness": aimedCompleteness,
        "aimedMultiplicity": aimedMultiplicity,
        "aimedResolution": aimedResolution,
        "observedResolution": observedResolution,
        "minOscWidth": minOscWidth,
        "sampleSusceptibility": sampleSusceptibility,
        "findLargestMesh": findLargestMesh,
        "numberOfPositions": numberOfPositions,
        "moreSamples": moreSamples,
        "targetApertureName": targetApertureName,
        "doAutoMesh": doAutoMesh,
        "doPseudoHelical": doPseudoHelical,
        "dozorThreshold": dozorThreshold,
        "strategyOption": strategyOption,
        "axisRange": axisRange,
        "doFbest": doFbest,
        "diffractionSignalDetection": diffractionSignalDetection,
        "openKappaForLowSymmetry": openKappaForLowSymmetry,
        "runGphlWorkflow": runGphlWorkflow,
        "doTwoMeshes": doTwoMeshes,
        "deltaOmega": deltaOmega,
        "synchronizeBestPosition": synchronizeBestPosition,
        "loopMinWidth": loopMinWidth,
        "loopMaxWidth": loopMaxWidth,
        "simulated_grid_positions": simulated_grid_positions,
        "simulated_characterisation_results": simulated_characterisation_results,
    }
