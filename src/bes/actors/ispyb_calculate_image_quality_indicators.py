from bes.workflow_lib import edna_mxv1


def run(
    beamline,
    mxv1StrategyResult,
    directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    workflow_working_dir,
    **_,
):

    edna_mxv1.storeImageQualityIndicators(
        beamline,
        mxv1StrategyResult,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        workflow_working_dir,
    )
    return {}
