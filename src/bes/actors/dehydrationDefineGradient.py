from bes.workflow_lib import dialog
from bes import config


def run(
    beamline,
    workflowParameters,
    directory,
    characterisationExposureTime,
    characterisationTransmission,
    automaticMode=False,
    token=None,
    collection_software=None,
    **_,
):

    characterisationExposureTime = float(characterisationExposureTime)
    characterisationTransmission = float(characterisationTransmission)

    listDialog = [
        {
            "variableName": "startRH",
            "label": "Relative Humidity Start Level",
            "type": "float",
            "defaultValue": 99.0,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "endRH",
            "label": "Relative Humidity End Level",
            "type": "float",
            "defaultValue": 50.0,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "step",
            "label": "Step in humidity level",
            "type": "float",
            "defaultValue": 1.0,
            "unit": "%",
            "lowerBound": 0.1,
            "upperBound": 50.0,
        },
        {
            "variableName": "delay",
            "label": "Delay after changing humidity level",
            "type": "float",
            "defaultValue": 60,
            "unit": "s",
            "lowerBound": 1.0,
            "upperBound": 10000.0,
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": characterisationExposureTime,
            "unit": "s",
            "lowerBound": config.get_value(
                beamline, "Beam", "minCharacterisationExposureTime"
            ),
            "upperBound": 10000.0,
        },
        {
            "variableName": "characterisationTransmission",
            "label": "Characterisation transmission",
            "type": "float",
            "value": characterisationTransmission,
            "unit": "s",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "doDataCollection",
            "label": "Do data collection?",
            "type": "boolean",
            "value": True,
        },
        {
            "variableName": "automatic_mode",
            "label": "Automatic mode",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "no_reference_images",
            "label": "No reference images",
            "type": "int",
            "value": 1,
            "lowerBound": 1,
            "upperBound": 4,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "float",
            "value": 90.0,
            "unit": "degrees",
            "lowerBound": 1.0,
            "upperBound": 180.0,
        },
        {
            "variableName": "snapShots",
            "label": "Snap shots",
            "type": "boolean",
            "value": False,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        automaticMode=automaticMode,
        token=token,
        collection_software=collection_software,
    )

    return dictValues
