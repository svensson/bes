"""
Initialisation of a workflow
"""

import os
import json
import time

from bes.workflow_lib import cryoem
from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    workflowParameters,
    archiveGridSquare,
    processDirectory,
    gridSquaresJsonFile,
    noGridSquaresArchived=1,
    maxNoMoviesToArchive=1,
    noMoviesArchived=0,
    REFID=None,
    REQUESTID=None,
    **_,
):

    workflowParameters = common.jsonLoads(workflowParameters)

    logger = workflow_logging.getLogger(None, workflowParameters)
    logger.info("*********** cryoemArchiveGridSquare **********")
    if archiveGridSquare is not None:
        dictGridSquares = json.loads(open(gridSquaresJsonFile).read())
        dictGridSquareToBeArchived = dictGridSquares[archiveGridSquare]
        dictMovies = dictGridSquareToBeArchived["movies"]

        if len(dictMovies) > 0:
            listFilesToArchive = []
            moviesAlreadyArchived = 0
            for movie in dictMovies:
                if dictMovies[movie]["status"] != "archived":
                    if noMoviesArchived < maxNoMoviesToArchive:
                        logger.info("Archiving movie {0}".format(movie))
                        moviePath = dictMovies[movie]["path"]
                        listFilesToArchive.append(moviePath)
                        listFilesToArchive += cryoem.findFilesToArchive(moviePath)
                        noMoviesArchived += 1
                        dictMovies[movie]["status"] = "archived"
                    else:
                        logger.info("Not archiving movie {0}".format(movie))
                else:
                    logger.info("Movie {0} already archived".format(movie))
                    moviesAlreadyArchived += 1

            proposal = "id310005"
            sample = "sample1"
            dataSetName = "{0}_{1}".format(archiveGridSquare, round(time.time()))
            cryoem.archiveDataFiles(
                listFilesToArchive, directory, proposal, sample, dataSetName
            )

            open(
                os.path.join(processDirectory, "{0}.json".format(archiveGridSquare)),
                "w",
            ).write(json.dumps(listFilesToArchive, indent=4))

            if len(dictMovies) == (moviesAlreadyArchived + noMoviesArchived):
                noGridSquaresArchived += 1
                dictGridSquareToBeArchived["status"] = "archived"
        else:
            dictGridSquareToBeArchived["status"] = "empty"

        open(gridSquaresJsonFile, "w").write(json.dumps(dictGridSquares, indent=4))

    return {
        "noGridSquaresArchived": noGridSquaresArchived,
        "noMoviesArchived": noMoviesArchived,
    }
