import time

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


EPSILON = 2.5


def run(
    beamline,
    workflowParameters,
    directory,
    run_number,
    currentPhi,
    angleRelative1,
    token=None,
    **_,
):

    try:
        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        run_number = int(run_number)

        newPhi = currentPhi + angleRelative1
        logger.info(
            "Rotating sample omega from %.3f to %.3f degrees" % (currentPhi, newPhi)
        )

        collect.moveMotors(beamline, directory, {"phi": newPhi}, token=token)
        time.sleep(1)

        dict_diffractometer_positions = collect.readMotorPositions(
            beamline, token=token
        )
        phi = dict_diffractometer_positions["phi"]

        logger.info("Omega position after move: phi=%.3f" % phi)
        if not beamline == "simulator":
            deltaPhi = abs(phi - newPhi)
            while deltaPhi > (360.0 - EPSILON):
                deltaPhi -= 360.0
            logger.debug("deltaPhi=%.3f" % deltaPhi)
            if deltaPhi > EPSILON:
                raise Exception("Sample didn't move to new omega position!")
            logger.info("Sample moved to the new omega position")
        else:
            logger.info("Simulator: sample omega not moved to the new position")

        # run_number += 1
        indexMesh2d = 3

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "newPhi": newPhi,
        "run_number": run_number,
        "motorPositions": dict_diffractometer_positions,
        "indexMesh2d": indexMesh2d,
    }
