import os
import shutil
import pprint

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import edna_ispyb
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    raw_directory,
    process_directory,
    prefix,
    suffix,
    expTypePrefix,
    run_number,
    motorPositions,
    noCycles,
    osc_range,
    noImagesPerCycle,
    tLimit,
    transmission,
    sampleInfo,
    resolution=None,
    token=None,
    **_,
):

    try:

        workflowParameters = common.jsonLoads(workflowParameters)
        pyarchHtmlDir = workflowParameters["pyarch_html_dir"]
        workflowWorkingDir = workflowParameters["workflowWorkingDir"]
        workflowId = workflowParameters["ispyb_id"]
        logger = workflow_logging.getLogger(beamline, workflowParameters)
        motorPositions = common.jsonLoads(motorPositions)
        run_number = int(run_number)
        noCycles = int(noCycles)
        osc_range = float(osc_range)
        noImagesPerCycle = int(noImagesPerCycle)
        tLimit = float(tLimit)
        transmission = float(transmission)

        collectExposureTime = tLimit / (noImagesPerCycle * noCycles)

        logger.info("Starting data collection")
        logger.info("No cycles: %r" % noCycles)
        logger.info("Exposure time: %r s" % collectExposureTime)
        logger.info("No images per cycle: %r" % noImagesPerCycle)
        logger.info("Oscillation range: %r degrees" % osc_range)
        logger.info("Transmission: %r %%" % transmission)

        sampleInfo = common.jsonLoads(sampleInfo)
        if sampleInfo is None:
            groupNodeId = None
            sampleNodeId = -1
        else:
            groupNodeId = sampleInfo["groupNodeId"]
            sampleNodeId = sampleInfo["nodeId"]

        if resolution is None:
            resolution = collect.getResolution(beamline, token=token)
        else:
            resolution = float(resolution)

        # Collect the data automatically
        enabled = True

        expTypePrefix = ""

        #     motorPositions = collect.readMotorPositions(beamline, token=token)
        data_collection_start_phi = motorPositions["phi"]

        listImages = []
        iDataCollectionGroupId = None
        listNodeId = []

        # Find unique run number for data collect
        data_collect_run_number = path.findUniqueRunNumber(
            raw_directory, prefix, expTypePrefix, run_number
        )

        for iteration in range(noCycles):

            firstImageNo = iteration * noImagesPerCycle + 1

            mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
                collectExposureTime,
                transmission,
                noImagesPerCycle,
                osc_range,
                data_collection_start_phi,
            )

            dictId = collect.loadQueue(
                beamline,
                directory,
                process_directory,
                prefix,
                expTypePrefix,
                data_collect_run_number,
                resolution,
                None,
                mxv1StrategyResult,
                motorPositions,
                workflowParameters["type"],
                workflowParameters["index"],
                groupNodeId,
                sampleNodeId,
                firstImageNo=firstImageNo,
                enabled=enabled,
                doProcess=False,
                token=token,
            )

            groupNodeId = dictId["group_node_id"]
            logger.debug("groupNodeId: {0}".format(groupNodeId))
            sampleInfo["groupNodeId"] = groupNodeId
            listNodeId += dictId["listNodeId"]

            if iDataCollectionGroupId is None:
                listImagePath = edna_mxv1.getListImagePath(
                    mxv1StrategyResult,
                    directory,
                    run_number,
                    expTypePrefix,
                    prefix,
                    suffix,
                )
                logger.debug("listImagePath: {0}".format(pprint.pformat(listImagePath)))
                iDataCollectionGroupId = edna_ispyb.updateDataCollectionGroupWorkflowId(
                    beamline,
                    directory,
                    run_number,
                    expTypePrefix,
                    prefix,
                    suffix,
                    listImagePath[0],
                    workflowParameters["ispyb_id"],
                    workflowParameters["workflowWorkingDir"],
                )
                logger.debug(
                    "iDataCollectionGroupId: {0}".format(iDataCollectionGroupId)
                )

            # Add images to listImages
            for index in range(noImagesPerCycle):
                listImages.append(
                    os.path.join(
                        directory,
                        "{0}_{1}_{2:04d}.cbf".format(
                            prefix, data_collect_run_number, index + firstImageNo
                        ),
                    )
                )

        # Run processing
        logger.info("Now processing data...")
        logger.info(listImages)
        halfDoseTime, xsDataResult = edna_mxv1.runDozorForHalfDoseTime(
            beamline, listImages, noImagesPerCycle, workflowParameters["workingDir"]
        )

        halfDoseTime = round(halfDoseTime, 1)
        logger.info("Half-dose time: {0}".format(halfDoseTime))

        # Create result html page
        snapShotPath, htmlFilePath, jsonFilePath = edna_mxv1.createRdExpressWebpage(
            xsDataResult, halfDoseTime, pyarchHtmlDir
        )

        newHtmlDir = os.path.join(
            workflowWorkingDir, os.path.basename(os.path.dirname(htmlFilePath))
        )
        shutil.copytree(os.path.dirname(htmlFilePath), newHtmlDir)
        newHtmlPagePath = os.path.join(newHtmlDir, os.path.basename(htmlFilePath))

        # Upload web page to mxCuBE
        collect.displayNewHtmlPage(
            beamline, newHtmlPagePath, "RDExpress", listNodeId, token=token
        )

        workflowStepType = "RDExpress"
        status = "Success"
        ispyb.storeWorkflowStep(
            beamline,
            workflowId,
            workflowStepType,
            status,
            imageResultFilePath=snapShotPath,
            htmlResultFilePath=htmlFilePath,
            resultFilePath=jsonFilePath,
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {"expTypePrefix": expTypePrefix, "sampleInfo": sampleInfo}
