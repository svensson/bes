"""
Created on Jun 9, 2016

@author: svensson
"""

import os
import time
import pprint
import tempfile

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import workflow_logging


def run(beamline, dataCollectionId, doWaitForImages=True, **_):

    logger = workflow_logging.getLogger(beamline)
    logger.info("In waitForFirstAndLastImage.py")
    logger.info("Beamline: {0}".format(beamline))
    firstAndLastImagesOk = False
    firstImageTimeOut = False
    lastImageTimeOut = False

    dateString = time.strftime("%Y%m%d", time.localtime(time.time()))
    timeString = time.strftime("%H%M%S", time.localtime(time.time()))
    pluginBaseDir = os.path.join("/tmp", beamline, dateString)
    if not os.path.exists(pluginBaseDir):
        try:
            os.makedirs(pluginBaseDir, 0o755)
        except Exception:
            pass

    workflowWorkingDir = tempfile.mkdtemp(
        prefix="{0}_waitForFirstAndLastImage_".format(timeString), dir=pluginBaseDir
    )
    os.chmod(workflowWorkingDir, 0o755)

    logger.info(
        "Searching for data collection id {0} in ISPyB".format(dataCollectionId)
    )
    ispybDataCollection = ispyb.findDataCollection(beamline, dataCollectionId)
    if ispybDataCollection is None:
        raise RuntimeError(
            "No data collection found in ISPyB with dataCollectionId = {0}".format(
                dataCollectionId
            )
        )
    directory = ispybDataCollection.imageDirectory
    fileTemplate = ispybDataCollection.fileTemplate
    imageNoStart = ispybDataCollection.startImageNumber
    imageNoEnd = imageNoStart + ispybDataCollection.numberOfImages - 1
    overlap = ispybDataCollection.overlap
    logger.info("Directory:    {0}".format(directory))
    logger.info("fileTemplate: {0}".format(fileTemplate))
    logger.info("imageNoStart: {0}".format(imageNoStart))
    logger.info("imageNoEnd:   {0}".format(imageNoEnd))
    if fileTemplate.endswith(".h5"):
        pathToStartImage = os.path.join(
            directory, path.eigerTemplateToImage(fileTemplate, imageNoStart, overlap)
        )
        pathToEndImage = os.path.join(
            directory, path.eigerTemplateToImage(fileTemplate, imageNoEnd, overlap)
        )
    else:
        pathToStartImage = os.path.join(
            directory, ispybDataCollection.fileTemplate % imageNoStart
        )
        pathToEndImage = os.path.join(
            directory, ispybDataCollection.fileTemplate % imageNoEnd
        )

    if beamline in ["id30a1", "id30a2"]:
        minFileSize = 2.0e6
    elif beamline in ["id30a3", "id23eh1", "id23eh2", "id30b"]:
        minFileSize = 1.0e4
    else:
        minFileSize = 6.0e6

    if doWaitForImages:
        logger.info("Waiting for first image {0}".format(pathToStartImage))
        firstImageTimeOut = edna_mxv1.waitForFile(
            beamline,
            pathToStartImage,
            workflowWorkingDir,
            fileSize=minFileSize,
            timeOut=900,
        )

        if not firstImageTimeOut:
            if pathToStartImage == pathToEndImage:
                lastImageTimeOut = False
                firstAndLastImagesOk = True
            else:
                lastImageTimeOut = edna_mxv1.waitForFile(
                    beamline,
                    pathToEndImage,
                    workflowWorkingDir,
                    fileSize=minFileSize,
                    timeOut=900,
                )
                logger.info("Waiting for end image {0}".format(pathToEndImage))
                if not lastImageTimeOut:
                    firstAndLastImagesOk = True
                    logger.info("Both first and end images found.")
    else:
        firstAndLastImagesOk = True
        logger.info("Not waiting for images.")

    return {
        "pathToStartImage": pathToStartImage,
        "pathToEndImage": pathToEndImage,
        "firstImageTimeOut": firstImageTimeOut,
        "lastImageTimeOut": lastImageTimeOut,
        "firstAndLastImagesOk": firstAndLastImagesOk,
        "minFileSize": minFileSize,
    }


if __name__ == "__main__":
    workflowParameters = {}
    beamline = "id30a2"
    #    dataCollectionId = 1774250
    #    workflow_working_dir = tempfile.mkdtemp(prefix="test_waitForFirstAndLastImage_")
    #    dictResult = run(beamline, workflowParameters, dataCollectionId, workflow_working_dir)
    #    pprint.pprint(dictResult)
    dataCollectionId = 1766980
    #    dataCollectionId = 1961981
    dictResult = run(beamline, dataCollectionId)
    pprint.pprint(dictResult)
