from bes.workflow_lib import path
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    beamParameters,
    run_number,
    mxv1StrategyResultFile,
    workflowParameters,
    characterisationTransmission,
    token=None,
    **_,
):

    try:
        _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        run_number = int(run_number)

        mxv1StrategyResult = path.readXSDataFile(mxv1StrategyResultFile)
        dictDataCollectionInfo = collect.getDataCollectionInfo(
            beamline,
            directory,
            characterisationTransmission,
            run_number,
            mxv1StrategyResult,
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return dictDataCollectionInfo
