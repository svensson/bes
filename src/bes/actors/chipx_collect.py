import os
import math
import json
import pprint

from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    prefix,
    run_number,
    exposureTime,
    oscillationWidth,
    workflowParameters,
    transmission,
    resolution,
    raw_directory,
    sample_node_id,
    no_lines=7,
    token=None,
    **_,
):
    logger = workflow_logging.getLogger(
        beamline, workflowParameters=workflowParameters, token=token
    )
    motorPositions = collect.readMotorPositions(beamline, token=token)
    logger.debug(pprint.pformat(motorPositions))
    raw_process_directory = raw_directory.replace("RAW_DATA", "PROCESSED_DATA")
    left_file_path = os.path.join(raw_process_directory, "left_pos.json")
    with open(left_file_path) as fd:
        left_pos = json.loads(fd.read())
    right_file_path = os.path.join(raw_process_directory, "right_pos.json")
    with open(right_file_path) as fd:
        right_pos = json.loads(fd.read())
    logger.debug(pprint.pformat(left_pos))
    logger.debug(pprint.pformat(right_pos))
    list_queue_entries = []
    direction = 1
    first_image = 1
    for index_line in range(no_lines):
        delta_h = (index_line - int(no_lines / 2)) * 0.025
        if direction > 0:
            phiy_s = left_pos["phiy"]
            phiz_s = left_pos["phiz"]
            phiy_e = right_pos["phiy"]
            phiz_e = right_pos["phiz"]
            delta_sampx_s = -delta_h * math.sin(math.radians(left_pos["phi"]))
            delta_sampy_s = delta_h * math.cos(math.radians(left_pos["phi"]))
            delta_sampx_e = -delta_h * math.sin(math.radians(right_pos["phi"]))
            delta_sampy_e = delta_h * math.cos(math.radians(right_pos["phi"]))
            sampx_s = left_pos["sampx"] + delta_sampx_s
            sampy_s = left_pos["sampy"] + delta_sampy_s
            sampx_e = right_pos["sampx"] + delta_sampx_e
            sampy_e = right_pos["sampy"] + delta_sampy_e
            rotation_axis_start = round(left_pos["phi"], 2)
        else:
            phiy_s = right_pos["phiy"]
            phiz_s = right_pos["phiz"]
            phiy_e = left_pos["phiy"]
            phiz_e = left_pos["phiz"]
            delta_sampx_s = -delta_h * math.sin(math.radians(right_pos["phi"]))
            delta_sampy_s = delta_h * math.cos(math.radians(right_pos["phi"]))
            delta_sampx_e = -delta_h * math.sin(math.radians(left_pos["phi"]))
            delta_sampy_e = delta_h * math.cos(math.radians(left_pos["phi"]))
            sampx_s = right_pos["sampx"] + delta_sampx_s
            sampy_s = right_pos["sampy"] + delta_sampy_s
            sampx_e = left_pos["sampx"] + delta_sampx_e
            sampy_e = left_pos["sampy"] + delta_sampy_e
            rotation_axis_start = round(right_pos["phi"], 2)
        motorPositions4dscan = {
            "phiy_s": phiy_s,
            "phiz_s": phiz_s,
            "sampx_s": sampx_s,
            "sampy_s": sampy_s,
            "phiy_e": phiy_e,
            "phiz_e": phiz_e,
            "sampx_e": sampx_e,
            "sampy_e": sampy_e,
        }
        # logger.debug(pprint.pformat(motorPositions4dscan)
        fileData = {
            "directory": raw_directory,
            "expTypePrefix": "",
            "firstImage": first_image,
            "prefix": prefix,
            "process_directory": raw_process_directory,
            "runNumber": run_number,
        }
        # logger.debug(pprint.pformat(fileData)
        noImages = int((right_pos["phi"] - left_pos["phi"]) / oscillationWidth + 0.5)
        subWedgeData = {
            "doProcessing": True,
            "exposureTime": exposureTime,
            "noImages": noImages,
            "oscillationWidth": oscillationWidth * direction,
            "rotationAxisStart": rotation_axis_start,
            "transmission": transmission,
        }
        # logger.debug(pprint.pformat(subWedgeData)
        queue_entry = {
            "fileData": fileData,
            "motorPositions4dscan": motorPositions4dscan,
            "subWedgeData": subWedgeData,
        }
        list_queue_entries.append(queue_entry)
        first_image += noImages
        direction = direction * -1
    logger.debug(pprint.pformat(list_queue_entries))
    motorPositions = collect.readMotorPositions(beamline, token=token)
    force_not_shutterless = False
    sample_node_id = 1
    _ = collect.executeQueueForMeshScan(
        beamline=beamline,
        workflow_index=workflowParameters["index"],
        workflow_type=workflowParameters["type"],
        meshQueueList=list_queue_entries,
        group_node_id=None,
        sample_node_id=sample_node_id,
        resolution=resolution,
        motorPositions=motorPositions,
        force_not_shutterless=force_not_shutterless,
        process_data=True,
        token=token,
    )
    return {}
