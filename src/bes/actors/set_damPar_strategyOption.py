def run(beamline, radiation_damage_beta, radiation_damage_gamma, **_):

    radiation_damage_beta = float(radiation_damage_beta)
    radiation_damage_gamma = float(radiation_damage_gamma)

    return {
        "minExposureTime": 0.050,
        "strategyOption": "-DamPar -beta %g -gama %g"
        % (radiation_damage_beta / 1e6, radiation_damage_gamma / 1e6),
    }
