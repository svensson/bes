def run(beamline, expTypePrefix="mesh-", **_):

    workflowParameters = {}
    workflowParameters["title"] = (
        "MXPressI: X-centre, EDNA resolution + 180 degree dc on %s" % beamline
    )
    workflowParameters["type"] = "MXPressI"

    return {
        "workflow_title": workflowParameters["title"],
        "workflow_type": workflowParameters["type"],
        "workflowParameters": workflowParameters,
        "expTypePrefix": expTypePrefix,
        "resolution": 2.0,
        "meshZigZag": False,
        "do_data_collect": True,
        "anomalousData": False,
        "doCharacterisation": True,
        "doOnlyEDNAResolution": True,
        "automatic_mode": True,
        "strategyOption": "-low never",
    }
