def run(run_number, **_):

    expTypePrefix = "ref-kappa-"
    run_number = int(run_number) + 1
    doRefDataCollectionReview = False

    return {
        "expTypePrefix": expTypePrefix,
        "run_number": run_number,
        "doRefDataCollectionReview": doRefDataCollectionReview,
    }
