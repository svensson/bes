#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "03/05/2021"

from bes.workflow_lib import ispyb
from bes import config
from bes.workflow_lib import symmetry
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    directory,
    firstImagePath=None,
    cellSpaceGroup=None,
    forcedSpaceGroup=None,
    workflowParameters=None,
    resolution=None,
    token=None,
    sampleInfo=None,
    openKappaForLowSymmetry=False,
    process_partial_data_sets=False,
    # last_data_collection_directory=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    doOpenKappa = False
    (forcedSpaceGroup, newCharacterisationComment) = symmetry.checkForcedSpaceGroup(
        forcedSpaceGroup, sampleInfo, directory
    )

    if openKappaForLowSymmetry and resolution is not None and resolution <= 4.0:

        newKappaAngle = config.get_value(
            beamline, "Goniometer", "lowSymmetryKappaAngle", default_value=None
        )
        if newKappaAngle is not None:

            if forcedSpaceGroup is not None and forcedSpaceGroup == "P1":
                message = "Resolution {0:.2f} A better than 2.0 A and forced space group P1 - new data collection with kappa open".format(
                    resolution
                )
                logger.info(message)
                if firstImagePath is not None:
                    ispyb.updateDataCollectionGroupComment(
                        beamline, firstImagePath, message
                    )
                doOpenKappa = True

            elif (
                cellSpaceGroup is not None
                and "spaceGroup" in cellSpaceGroup
                and cellSpaceGroup["spaceGroup"] == "P1"
            ):
                message = "Resolution {0:.2f} A better than 2.0 A and characterisation space group P1 - new data collection with kappa open".format(
                    resolution
                )
                logger.info(message)
                if firstImagePath is not None:
                    ispyb.updateDataCollectionGroupComment(
                        beamline, firstImagePath, message
                    )
                doOpenKappa = True

    if doOpenKappa:
        process_partial_data_sets = True

    return {
        "doOpenKappa": doOpenKappa,
        "process_partial_data_sets": process_partial_data_sets,
        # "list_partial_data_sets": list_partial_data_sets
    }
