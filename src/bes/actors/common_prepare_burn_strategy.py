def run(beamline, **_):

    return {
        "workflow_title": "Burn strategy on %s" % beamline,
        "workflow_type": "BurnStrategy",
        "doRefDataCollectionReview": False,
        "radiation_damage_beta": 1.0,
        "radiation_damage_gamma": 0.06,
        "snapShots": True,
    }
