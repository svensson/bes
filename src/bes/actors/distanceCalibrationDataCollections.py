from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import edna_ispyb


def run(
    beamline,
    workflowParameters,
    raw_directory,
    process_directory,
    dataCollectionOscillationRange,
    prefix,
    expTypePrefix,
    run_number,
    suffix,
    csvOfResolutions,
    collectExposureTime,
    transmission,
    motorPositions,
    workflow_type,
    workflow_index,
    workflow_working_dir,
    sample_node_id=1,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        motorPositions = common.jsonLoads(motorPositions)
        dataCollectionOscillationRange = float(dataCollectionOscillationRange)
        collectExposureTime = float(collectExposureTime)
        transmission = float(transmission)
        data_collection_start_phi = motorPositions["phi"]
        run_number = int(run_number)
        sample_node_id = int(sample_node_id)

        dataCollectionGroupNodeId = None
        expTypePrefix = ""

        # Split the csvOfResolutions into a list of resolutions
        listResolutions = map(float, csvOfResolutions.split(","))
        listResolutionPrefix = []

        for resolution in listResolutions:
            logger.info(
                "Setting up data collection for resolution {0} A".format(resolution)
            )
            mxv1StrategyResult = edna_mxv1.create_n_images_data_collect(
                collectExposureTime,
                transmission,
                1,
                dataCollectionOscillationRange,
                data_collection_start_phi,
            )

            resolutionPrefix = "r{0}_{1}".format(resolution, prefix)
            listResolutionPrefix.append(resolutionPrefix)

            dictId = collect.loadQueue(
                beamline,
                raw_directory,
                process_directory,
                resolutionPrefix,
                expTypePrefix,
                run_number,
                resolution,
                None,
                mxv1StrategyResult,
                motorPositions,
                workflow_type,
                workflow_index,
                dataCollectionGroupNodeId,
                sample_node_id,
                enabled=True,
                token=token,
            )

            if dataCollectionGroupNodeId is None:
                dataCollectionGroupNodeId = dictId["group_node_id"]

            listImages = edna_mxv1.getListImagePath(
                mxv1StrategyResult,
                raw_directory,
                run_number,
                expTypePrefix,
                resolutionPrefix,
                suffix,
                bFirstImageOnly=True,
            )

            firstImagePath = listImages[0]

            dictBeamSize = edna_ispyb.getMeshBeamsize(
                beamline, raw_directory, firstImagePath, workflow_working_dir
            )

            logger.info("Distance: {0}".format(dictBeamSize["detectorDistance"]))

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
