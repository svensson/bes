import os
import tempfile

from bes.workflow_lib import common
from bes.workflow_lib import workflow_logging


def run(beamline, workflowParameters, token=None, **_):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    try:

        # Testing the distl server
        testImagePath = (
            "/opt/pxsoft/EDNA/vGIT/edna/tests/data/images/ref-testscale_1_001.img"
        )
        serverHost = "mxhpc2-1705"
        serverPort = 8125

        fd, tempLogFile = tempfile.mkstemp(prefix="DistlServerTest_", suffix=".txt")
        os.close(fd)

        commandLine = (
            "unset PYTHONPATH; /opt/pxsoft/bin/distl.thin_client %s %s %d > %s 2>&1"
            % (testImagePath, serverHost, serverPort, tempLogFile)
        )
        logger.debug("Executing command line '%s'" % commandLine)
        os.system(commandLine)

        f = open(tempLogFile)
        logText = f.read()
        f.close()

        os.remove(tempLogFile)

        if "Total integrated signal" in logText:
            flagDistlServerRunning = True
            message_text = ""
            logger.info("Phenix/Labelit spot finder server is running")
        else:
            flagDistlServerRunning = False
            message_text = "Phenix/Labelit spot finder server not is running!"
            logger.error(message_text)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "flagDistlServerRunning": flagDistlServerRunning,
        "message_text": message_text,
    }
