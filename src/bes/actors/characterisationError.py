"""
Created on Mar 3, 2015

@author: svensson
"""

import os

from EDFactoryPluginStatic import EDFactoryPluginStatic

EDFactoryPluginStatic.loadModule("XSDataMXCuBEv1_4")
from XSDataMXCuBEv1_4 import XSDataResultMXCuBE  # noqa E402


def run(outputFile, logFile=None, **_):

    if not os.path.exists(outputFile):
        XSDataOutputMXCuBE = XSDataResultMXCuBE()
        with open(outputFile, "w") as fd:
            fd.write(XSDataOutputMXCuBE.marshal())

    return {}
