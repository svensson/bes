from bes.workflow_lib import workflow_logging, common

EPSILON = 0.01


def run(
    beamline,
    workflowParameters,
    sampx,
    sampy,
    phiy,
    newSampx,
    newSampy,
    newPhiy,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    try:

        logger.info(
            "Sample position after move: sampx=%.3f sampy=%.3f phiy=%.3f"
            % (sampx, sampy, phiy)
        )
        if not beamline == "simulator":
            if (
                abs(sampx - newSampx) > EPSILON
                or abs(sampy - newSampy) > EPSILON
                or abs(phiy - newPhiy) > EPSILON
            ):
                raise Exception("Sample didn't move to best position!")
            logger.info("Sample moved to the best position!")
        else:
            logger.info("Simulator: sample not moved to the best position")

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {}
