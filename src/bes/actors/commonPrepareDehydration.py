def run(beamline, **_):

    return {
        "workflow_title": "Dehydration on %s" % beamline,
        "workflow_type": "Dehydration",
    }
