def run(beamline, expTypePrefix="mesh-", **_):
    return {
        "workflow_title": "Ice rings mesh scan on %s" % beamline,
        "workflow_type": "MeshScan",
        "expTypePrefix": expTypePrefix,
        "plateMode": True,
        "doDistlSignalStrength": True,
    }
