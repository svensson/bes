from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    raw_directory,
    process_directory,
    run_number,
    expTypePrefix,
    prefix,
    suffix,
    motorPositions,
    gridExposureTime,
    gridOscillationRange,
    transmission,
    grid_info,
    beamSizeAtSampleX=0.1,
    beamSizeAtSampleY=0.1,
    gridTransmission=None,
    workflow_index=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        logger.info("Preparing vertical line scan")

        logger.info("Motor positions: %s" % motorPositions)

        motorPositions = common.jsonLoads(motorPositions)
        gridExposureTime = float(gridExposureTime)
        gridOscillationRange = float(gridOscillationRange)
        run_number = int(run_number)

        if beamline == "simulator":
            noSteps = 40
            deltaY = beamSizeAtSampleY * noSteps / 6.0
        if beamline == "id23eh2":
            noSteps = 20
            deltaY = beamSizeAtSampleY * noSteps / 6.0
        else:
            noSteps = 10
            deltaY = beamSizeAtSampleY * noSteps / 4.0

        grid_info = {
            "x1": 0.0,
            "y1": -deltaY / 2.0,
            "dx_mm": 0.1,
            "dy_mm": deltaY,
            "steps_x": 1,
            "steps_y": noSteps,
            "angle": 0.0,
        }

        expTypePrefix = "line-"
        directory, run_number = path.createUniqueICATDirectory(
            raw_directory=raw_directory,
            run_number=run_number,
            workflow_index=workflow_index,
            expTypePrefix=expTypePrefix,
        )

        if gridTransmission is None:
            transmission = config.get_value(beamline, "Beam", "defaultGridTransmission")
        else:
            transmission = float(gridTransmission)

        meshPositions = grid.determineMeshPositions(
            beamline,
            motorPositions,
            gridOscillationRange,
            run_number,
            expTypePrefix,
            prefix,
            suffix,
            grid_info,
        )

        meshQueueList = collect.createHelicalQueueList(
            beamline,
            directory,
            process_directory,
            run_number,
            expTypePrefix,
            prefix,
            meshPositions,
            motorPositions,
            gridExposureTime,
            gridOscillationRange,
            transmission,
            grid_info,
        )

        dictNewParameters = collect.checkMotorSpeed(
            beamline, meshQueueList, gridExposureTime, transmission
        )
        if dictNewParameters is not None:
            gridExposureTime = dictNewParameters["gridExposureTime"]
            transmission = dictNewParameters["transmission"]
            meshQueueList = collect.createHelicalQueueList(
                beamline,
                directory,
                process_directory,
                run_number,
                expTypePrefix,
                prefix,
                meshPositions,
                motorPositions,
                gridExposureTime,
                gridOscillationRange,
                transmission,
                grid_info,
            )
            newExposureTimeMessage = (
                "In order to perform the scan with optimal motor speed\n"
            )
            newExposureTimeMessage += (
                "the exposure time has been changed to %.3f s\n" % gridExposureTime
            )
            newExposureTimeMessage += (
                "and the transmission changed accordingly to %.1f %%.\n" % transmission
            )
            logger.info(newExposureTimeMessage)

        meshPositionFile = path.createNewJsonFile(
            workflowParameters["workingDir"], meshPositions, "meshPositions"
        )
        meshQueueListFile = path.createNewJsonFile(
            workflowParameters["workingDir"], meshQueueList, "meshQueueList"
        )

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "grid_info": grid_info,
        "shape": None,
        "meshPositionFile": meshPositionFile,
        "expTypePrefix": expTypePrefix,
        "meshQueueListFile": meshQueueListFile,
        "directory": directory,
        "run_number": run_number,
    }
