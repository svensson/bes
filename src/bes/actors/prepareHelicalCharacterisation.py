from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(beamline, directory, workflowParameters, token=None, **_):

    try:
        _ = workflow_logging.getLogger(beamline, workflowParameters, token=token)

        workflowParameters = common.jsonLoads(workflowParameters)

        # Save position in mxCuBE
        collect.saveCurrentPos(beamline, token=token)

        # Save current position
        motorPositions = collect.readMotorPositions(beamline, token=token)
        sampx2 = motorPositions["sampx"]
        sampy2 = motorPositions["sampy"]
        phiy2 = motorPositions["phiy"]
        phiz2 = motorPositions["phiz"]

        expTypePrefix = "ref-"

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return {
        "sampx2": sampx2,
        "sampy2": sampy2,
        "phiy2": phiy2,
        "phiz2": phiz2,
        "expTypePrefix": expTypePrefix,
    }
