"""
Init module for EXT ISPyB
"""

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2016"
__updated__ = "2016-07-21"

import os
import datetime
import tempfile

from bes.workflow_lib import path
from bes.workflow_lib import extISPyB
from bes.workflow_lib import workflow_logging


def run(
    projectId, runId, extispyb, token, workflowType="Unknown", version="Unknown", **_
):
    workflowParameters = {}

    workflowParameters["type"] = workflowType
    workflowParameters["version"] = version

    topTmpDir = "/tmp_14_days/reprocess"
    if not os.path.exists(topTmpDir):
        os.makedirs(topTmpDir, 0o755)

    workflowWorkingDir = tempfile.mkdtemp(dir=topTmpDir)
    os.chmod(workflowWorkingDir, 0o755)

    workflowParameters["workingDir"] = workflowWorkingDir

    # Create stack trace file
    workflowStackTraceFile = path.createStackTraceFile(workflowWorkingDir)
    workflowParameters["stackTraceFile"] = workflowStackTraceFile

    workflowLogFile = path.createWorkflowLogFilePath(workflowWorkingDir)
    workflowDebugLogFile = path.createWorkflowDebugLogFilePath(workflowWorkingDir)

    workflowParameters["logFile"] = workflowLogFile
    workflowParameters["debugLogFile"] = workflowDebugLogFile

    beamline = None
    logger = workflow_logging.getLogger(beamline, workflowParameters)

    logger.info("Starting {0} data processing workflow".format(workflowType))
    logger.info("Workflow working directory: {0}".format(workflowWorkingDir))

    logger.debug("URL ExtISPyB: {0}".format(extispyb))

    extISPyB.setStarted(projectId, extispyb, runId, token)
    startDate = str(datetime.datetime.now())

    return {"workflowParameters": workflowParameters, "startDate": startDate}
