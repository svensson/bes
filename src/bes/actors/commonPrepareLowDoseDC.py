def run(beamline, **_):

    return {
        "workflow_title": "Test Low Dose DC on %s" % beamline,
        "workflow_type": "LowDoseDC",
        "doRefDataCollectionReview": False,
        "no_reference_images": 2,
        "continueDataCollection": True,
        "do_data_collect": True,
        "anomalousData": True,
    }
