from bes.workflow_lib import collect
from bes.workflow_lib import dialog


def run(
    beamline,
    workflowParameters,
    directory,
    characterisationOscillationRange,
    characterisationExposureTime,
    characterisationTransmission,
    automatic_mode=False,
    resolution=None,
    collection_software=None,
    token=None,
    **_,
):

    characterisationExposureTime = float(characterisationExposureTime)
    characterisationOscillationRange = float(characterisationOscillationRange)
    characterisationTransmission = float(characterisationTransmission)

    if resolution is None:
        resolution = collect.getResolution(beamline, token=token)
    else:
        resolution = float(resolution)

    listDialog = [
        {
            "variableName": "strategyType",
            "label": "Type of strategy",
            "type": "combo",
            "defaultValue": "OPTIMAL",
            "textChoices": ["OPTIMAL", "FAST", "FULL"],
        },
        {
            "variableName": "no_reference_images",
            "label": "Number of reference images",
            "type": "int",
            "defaultValue": 2,
            "unit": "",
            "lowerBound": 0.0,
            "upperBound": 4.0,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "combo",
            "defaultValue": "90",
            "textChoices": ["30", "45", "60", "90"],
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": characterisationExposureTime,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "characterisationOscillationRange",
            "label": "Characterisation oscillation range",
            "type": "float",
            "value": characterisationOscillationRange,
            "unit": "%",
            "lowerBound": 0.1,
            "upperBound": 10.0,
        },
        {
            "variableName": "characterisationTransmission",
            "label": "Transmission for characterisation reference images",
            "type": "float",
            "value": characterisationTransmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "resolution",
            "label": "Resolution",
            "type": "float",
            "defaultValue": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 7.0,
        },
        {
            "variableName": "do_data_collect",
            "label": "Do data collection?",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "snapShots",
            "label": "Snap shots",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "fineSlicedReferenceImages",
            "label": "Fine-sliced reference images",
            "type": "boolean",
            "value": False,
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
