def run(beamline, **_):

    return {
        "workflow_title": "Helical characterisation on %s" % beamline,
        "workflow_type": "HelicalCharacterisation",
        "doRefDataCollectionReview": False,
        "createNewDataCollectionGroup": True,
        "doVerticalXrayCentring": False,
        "do2DMeshOnly": False,
        "perform_extra_1D_scan": False,
        "centerRotationAxis": False,
    }
