#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__authors__ = ["O. Svensson"]
__license__ = "MIT"
__date__ = "26/01/2021"

import os
import time
import tempfile

from pyicat_plus.client.main import IcatClient

from bes.workflow_lib import icat
from bes.workflow_lib import grid
from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import common
from bes import config
from bes.workflow_lib import collect
from bes.workflow_lib import auto_mesh
from bes.workflow_lib import aperture_utils
from bes.workflow_lib import workflow_logging


def run(  # noqa C901, R0913
    beamline,
    icat_beamline,
    workflowParameters,
    raw_directory,
    run_number,
    proposal=None,
    isHorizontalRotationAxis=False,
    pyarch_html_dir=None,
    dataCollectionGroupComment="",
    numberOfPositions=1,
    targetApertureName=None,
    token=None,
    doTwoMeshes=False,
    workflow_index=None,
    loopMinWidth=None,
    loopMaxWidth=None,
    besParameters=None,
    **_,
):
    newPhi = None
    motorPositions = {}
    snapshotDir = None
    PIXELS_PER_MM = None
    autoMeshWorkingDir = None
    workflowId = None
    isVerticalAxis = not isHorizontalRotationAxis
    if besParameters:
        bes_request_id = besParameters["request_id"]
    else:
        bes_request_id = None

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    try:
        workflowParameters = common.jsonLoads(workflowParameters)
        workflowId = workflowParameters["ispyb_id"]
        workflowWorkingDir = workflowParameters["workingDir"]
        autoMeshWorkingDir = tempfile.mkdtemp(
            prefix="autoMesh_", dir=workflowWorkingDir
        )
        os.chmod(autoMeshWorkingDir, 0o755)
        numberOfPositions = int(numberOfPositions)

        motorPositions = collect.readMotorPositions(beamline, token=token)

        # Find unique run number for data collect
        directory, run_number = path.createUniqueICATDirectory(
            raw_directory=raw_directory,
            run_number=run_number,
            workflow_index=workflow_index,
            expTypePrefix="automesh",
        )

        if dataCollectionGroupComment != "":
            dataCollectionGroupComment += " "
        gridComment = None

        PIXELS_PER_MM = config.get_value(beamline, "SnapShotZoomMmPerPixel", "2")
        if isHorizontalRotationAxis:
            deltaPhiy = config.get_value(beamline, "AutoMesh", "deltaPhiy")
            signPhiy = config.get_value(beamline, "AutoMesh", "signPhiy")
        else:
            deltaPhiz = config.get_value(beamline, "AutoMesh", "deltaPhiz")
            signPhiz = config.get_value(beamline, "AutoMesh", "signPhiz")
        snapShotAngles = config.get_value(beamline, "AutoMesh", "snapShotAngles")
        zoomLevel = config.get_value(beamline, "AutoMesh", "zoomLevel")
        if loopMinWidth is None:
            loopMinWidth = float(config.get_value(beamline, "AutoMesh", "loopMinWidth"))
        if loopMaxWidth is None:
            loopMaxWidth = float(config.get_value(beamline, "AutoMesh", "loopMaxWidth"))
        frontLightLevel = config.get_value(
            beamline, "AutoMesh", "frontLightLevel", default_value=None
        )
        backLightLevel = config.get_value(
            beamline, "AutoMesh", "backLightLevel", default_value=None
        )
        fixedTargetApertureSize = config.get_value(
            beamline, "AutoMesh", "fixedTargetApertureSize"
        )
        findLargestMesh = config.get_value(beamline, "AutoMesh", "findLargestMesh")
        snapshot_image_suffix = config.get_value(
            beamline, "AutoMesh", "snapshotImageSuffix"
        )
        xOffsetLeft = config.get_value(beamline, "AutoMesh", "xOffsetLeft")
        xOffsetRight = config.get_value(beamline, "AutoMesh", "xOffsetRight")
        overSamplingX = config.get_value(beamline, "AutoMesh", "overSamplingX")
        overSamplingY = config.get_value(beamline, "AutoMesh", "overSamplingY")

        if targetApertureName is None:
            targetApertureName = aperture_utils.aperture_size_to_name(
                beamline, fixedTargetApertureSize
            )

        logger.info("Loop min width: {0:.2f} mm".format(loopMinWidth))
        logger.info("Loop max width: {0:.2f} mm".format(loopMaxWidth))

        snapshotDir = directory
        logger.debug("Snapshot directory: {0}".format(snapshotDir))

        collect.setZoomLevel(beamline, zoomLevel, token=token)
        if frontLightLevel:
            collect.setFrontLightLevel(beamline, frontLightLevel, token=token)
        if backLightLevel:
            collect.setBackLightLevel(beamline, backLightLevel, token=token)

        if beamline in ["id23eh1", "id30a1", "id30a3", "id30b"]:
            _ = collect.setApertureName(beamline, targetApertureName, token=token)
        apertureSize = collect.getApertureSize(beamline, token=token)
        if apertureSize is None:
            raise RuntimeError("Cannot get the current aperture size")
        logger.info("Current aperture size: %d um", apertureSize)
        beamSize = apertureSize / 1000.0
        logger.info("Beam size: %f mm", beamSize)
        loopMinWidthPixels = loopMinWidth * PIXELS_PER_MM
        loopMaxWidthPixels = loopMaxWidth * PIXELS_PER_MM
        logger.info("Loop min width: {0:.2f} pixels".format(loopMinWidthPixels))
        logger.info("Loop max width: {0:.2f} pixels".format(loopMaxWidthPixels))

        # Take snapshots
        snapshotDelay = config.get_value(beamline, "AutoMesh", "snapshotDelay")
        if snapshotDelay is None:
            snapshotDelay = 0.1
        backgroundDir = directory
        background_image = os.path.join(
            backgroundDir,
            "snapshot_background_{0}.{1}".format(
                str(zoomLevel).replace(" ", "_"), snapshot_image_suffix
            ),
        )
        if beamline == "id30a1":
            collect.take_twelve_snapshots(beamline, snapshotDir, token=token)
            # Create animated gif
            try:
                meshSnapShotPath = os.path.join(snapshotDir, "snapshot_animated.gif")
                import glob
                import subprocess

                listSnapshot = glob.glob(
                    os.path.join(
                        snapshotDir, "snapshot_???.{0}".format(snapshot_image_suffix)
                    )
                )
                listSnapshot.sort()
                listCommandLine = "convert -delay 25 -loop 0 -scale 150x150".split(" ")
                for snapShotFile in listSnapshot:
                    listCommandLine.append(snapShotFile)
                listCommandLine.append(meshSnapShotPath)
                logger.debug(listCommandLine)
                subprocess.call(listCommandLine)
            except Exception as e:
                logger.warning("Error when creating animated gif: {0}".format(e))
        else:
            if not os.path.exists(backgroundDir):
                os.makedirs(backgroundDir, 0o755)
            if beamline in ["id30a1"] and os.path.exists(background_image):
                os.remove(background_image)
            # Move sample relative 1.5 mm for background snapshot
            if isVerticalAxis:
                phizOrig = motorPositions["phiz"]
                phizNew = phizOrig + deltaPhiz * signPhiz
                collect.moveMotors(beamline, directory, {"phiz": phizNew}, token=token)
                time.sleep(snapshotDelay)
                collect.saveSnapshot(beamline, background_image, token=token)
                collect.moveMotors(beamline, directory, {"phiz": phizOrig}, token=token)
            else:
                phiyOrig = motorPositions["phiy"]
                phiyNew = phiyOrig + deltaPhiy * signPhiy
                collect.moveMotors(beamline, directory, {"phiy": phiyNew}, token=token)
                time.sleep(snapshotDelay)
                collect.saveSnapshot(beamline, background_image, token=token)
                collect.moveMotors(beamline, directory, {"phiy": phiyOrig}, token=token)
            index = 0
            while not os.path.exists(background_image):
                logger.info("Waiting for background image %s" % background_image)
                os.system("ls {0}> /dev/null".format(os.path.dirname(background_image)))
                time.sleep(1)
                index += 1
                if index > 10:
                    raise FileNotFoundError(
                        "Cannot find background image {0}".format(background_image)
                    )

            time.sleep(snapshotDelay)
            collect.takeSnapshots(
                beamline, snapshotDir, background_image, snapShotAngles, token=token
            )

        metadata_urls = config.get_value(
            beamline, "ICAT", "metadata_urls", default_value=[]
        )
        sample_name = icat.get_sample_name(directory)
        if proposal is not None and len(metadata_urls) > 0:
            client = IcatClient(metadata_urls=metadata_urls)
            dataset_name = os.path.basename(directory)
            data = {
                "scanType": "automesh",
                "Sample_name": sample_name,
                "Workflow_name": workflowParameters["title"],
                "Workflow_type": workflowParameters["type"],
                "Workflow_id": bes_request_id,
            }
            logger.info("Storing automesh in ICAT")
            logger.debug(f"beamline: {icat_beamline}")
            logger.debug(f"proposal: {proposal}")
            logger.debug(f"dataset_name: {dataset_name}")
            logger.debug(f"directory: {str(directory)}")
            logger.debug(f"data: {data}")
            client.store_dataset(
                beamline=icat_beamline,
                proposal=proposal,
                dataset=dataset_name,
                path=str(directory),
                metadata=data,
            )

        (
            snapShotPath,
            htmlResultFilePath,
            jsonFilePath,
        ) = auto_mesh.createSnapshotHTMLPage(
            snapshotDir,
            snapShotAngles=snapShotAngles,
            pyarchHtmlDir=pyarch_html_dir,
            suffix=snapshot_image_suffix,
        )
        workflowStepType = "Snapshots"
        status = "Success"
        ispyb.storeWorkflowStep(
            beamline,
            workflowId,
            workflowStepType,
            status,
            imageResultFilePath=snapShotPath,
            htmlResultFilePath=htmlResultFilePath,
            resultFilePath=jsonFilePath,
        )

        (
            newPhi,
            x1Pixels,
            y1Pixels,
            dxPixels,
            dyPixels,
            deltaPhizPixels,
            stdPhizPixels,
            imagePath,
            areTheSameImage,
        ) = grid.autoMesh(
            snapshotDir=snapshotDir,
            workflowWorkingDir=workflowWorkingDir,
            autoMeshWorkingDir=autoMeshWorkingDir,
            snapShotAngles=snapShotAngles,
            debug=False,
            loopMaxWidth=loopMaxWidthPixels,
            loopMinWidth=loopMinWidthPixels,
            findLargestMesh=findLargestMesh,
            isVerticalAxis=isVerticalAxis,
            suffix=snapshot_image_suffix,
        )

        if areTheSameImage:
            count = path.checkSameSnapShotImageCountFlag(
                workflowWorkingDir, timeOut=300
            )
            if count == 1:
                # The first time the same snapshot images are detected a file stamp
                # count is created in PROCESSED_DATA. The workflow then ends with an error
                # and the next sample is loaded.
                errorMessage = "Same snapshot image detected! Workflow interrupted."
                logger.error(errorMessage)
                raise RuntimeError(errorMessage)
            elif count == 2:
                # The next time the same snapshot images are detected and the file stamp
                # count is one and the time of creation of the time stamp is less than
                # 5 min., the file stamp count is incremented to two, otherwise the time
                # stamp is reset to one count. The workflow then ends with an error and
                # the next sample is loaded.
                errorMessage = "Second time in a row the same snapshot"
                errorMessage += " image detected! Workflow interrupted."
                logger.error(errorMessage)
                raise RuntimeError(errorMessage)
            else:
                # The next time the same snapshot images are detected and the file stamp
                # count is two and the time of creation of the time stamp is less than 10 min.
                # the workflow sleeps forever and a message is sent every hour. Otherwise
                # the time stamp is reset to one count
                doContinue = True
                while doContinue:
                    if besParameters is not None:
                        statusBES = common.getBESWorkflowStatus(besParameters)
                        if statusBES != "STARTED":
                            doContinue = False
                    if doContinue:
                        errorMessage = "Same snapshot image detected for three"
                        errorMessage += " samples in a row! Workflow halted."
                        logger.error(errorMessage)
                        emailList = config.get_value(
                            beamline, "Email", "sameSnapshotImage", default_value=[]
                        )
                        common.send_email(
                            beamline,
                            emailList,
                            errorMessage,
                            directory,
                            workflowParameters["title"],
                            workflowParameters["type"],
                            workflowWorkingDir,
                        )
                        time.sleep(3600)

        if deltaPhizPixels is not None:
            deltaPhiz = deltaPhizPixels / PIXELS_PER_MM
            # For the moment ignore deltaPhizPixels
            logger.debug("Ignoring delta phiz (mm): %.4f" % deltaPhiz)
        deltaPhiz = 0.0
        deltaPhizPixels = 0.0
        #        stdPhiz = stdPhizPixels / PIXELS_PER_MM
        #        logger.info("Std phiz (mm): %.4f" % stdPhiz)
        #
        # Move phi 45 degrees if two meshes
        if doTwoMeshes:
            # Find the angle which is the closest
            minAngleDistance = None
            for angle in snapShotAngles:
                angleDistance = abs(newPhi - angle + 45)
                if minAngleDistance is None or angleDistance < minAngleDistance:
                    minAngleDistance = angleDistance
                    twoMeshNewPhi = angle
                angleDistance = abs(newPhi - angle - 45)
                if angleDistance < minAngleDistance:
                    minAngleDistance = angleDistance
                    twoMeshNewPhi = angle
            newPhi = twoMeshNewPhi
            logger.info(
                f"New phi for two meshes: {newPhi}, distance from autoMesh phi: {minAngleDistance}"
            )

        collect.moveMotors(beamline, directory, {"phi": newPhi}, token=token)
        motorPositions = collect.readMotorPositions(beamline, token=token)
        logger.info(
            "Grid coordinates in pixels: x1=%.1f y1=%.1f dx=%.1f dy=%.1f"
            % (x1Pixels, y1Pixels, dxPixels, dyPixels)
        )
        if isVerticalAxis:
            x1 = x1Pixels / PIXELS_PER_MM
            y1 = -(y1Pixels + dyPixels + deltaPhizPixels) / PIXELS_PER_MM - xOffsetLeft
            dx_mm = dxPixels / PIXELS_PER_MM
            dy_mm = dyPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight
            steps_x = int(dxPixels / PIXELS_PER_MM / beamSize * overSamplingX)
            steps_y = int(
                (dyPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight)
                / beamSize  # noqa W503
                * overSamplingY  # noqa W503
            )
        else:
            x1 = x1Pixels / PIXELS_PER_MM - xOffsetLeft
            y1 = -(y1Pixels + dyPixels + deltaPhizPixels) / PIXELS_PER_MM
            dx_mm = dxPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight
            dy_mm = dyPixels / PIXELS_PER_MM
            steps_x = int(
                (dxPixels / PIXELS_PER_MM + xOffsetLeft + xOffsetRight)
                / beamSize  # noqa W503
                * overSamplingX  # noqa W503
            )
            steps_y = int(dyPixels / PIXELS_PER_MM / beamSize * overSamplingY)
        # Check that we have at least three lines:
        if isVerticalAxis and steps_x < 3:
            steps_x = 3
            x1 -= dxPixels / PIXELS_PER_MM / 4.0
            dx_mm *= 1.5
        elif steps_y < 3:
            steps_y = 3
            y1 -= dyPixels / PIXELS_PER_MM / 4.0
            dy_mm *= 1.5

        #        # Check max vertical grid size
        #        if steps_y > maxNoVerticalLines:
        #            newDy_mm = (dy_mm / steps_y) * maxNoVerticalLines
        #            newY1 = y1 + newDy_mm / 2.0
        #            dy_mm = newDy_mm
        #            y1 = newY1
        #            steps_y = maxNoVerticalLines

        grid_info = {
            "x1": round(float(x1), 4),
            "y1": round(float(y1), 4),
            "dx_mm": round(float(dx_mm), 4),
            "dy_mm": round(float(dy_mm), 4),
            "steps_x": int(steps_x),
            "steps_y": int(steps_y),
        }
        logger.info("Auto grid_info: %r" % grid_info)
        # newPhiz = motorPositions["phiz"] - deltaPhiz
        # logger.info("New phiz position: %.3f" % newPhiz)

        # Check grid_info
        logger.info("Grid info: %r" % grid_info)
        if beamline not in ["id30a2", "simulator_mxcube", "simulator"]:
            if isVerticalAxis and (
                grid_info is None
                or grid_info["x1"] > 0.5  # noqa W503
                or grid_info["dx_mm"] > 1.5  # noqa W503
                or grid_info["dy_mm"] > 1.5  # noqa W503
                or grid_info["steps_x"] > 200  # noqa W503
            ):
                # Something is wrong! Use "standard" mesh
                grid_info = {
                    "x1": -0.05,
                    "y1": -0.20,
                    "dx_mm": 0.10,
                    "dy_mm": 0.40,
                    "steps_x": 20,
                    "steps_y": 100,
                    "angle": 0.0,
                }
                gridComment = "Standard mesh used : 800 x 200 um."
                logger.warning("Fixed grid_info: %r" % grid_info)
                # if os.path.exists(background_image):
                #     os.remove(background_image)
            elif not isVerticalAxis and (
                grid_info is None
                or grid_info["x1"] > 0.5  # noqa W503
                or grid_info["dx_mm"] > 1.5  # noqa W503
                or grid_info["dy_mm"] > 1.5  # noqa W503
                or grid_info["steps_y"] > 200  # noqa W503
            ):
                # Something is wrong! Use "standard" mesh
                grid_info = {
                    "x1": -0.40,
                    "y1": -0.10,
                    "dx_mm": 0.80,
                    "dy_mm": 0.20,
                    "steps_x": 30,
                    "steps_y": 8,
                    "angle": 0.0,
                }
                gridComment = "Standard mesh used : 800 x 200 um."
                logger.warning("Fixed grid_info: %r" % grid_info)
                # if os.path.exists(background_image):
                #     os.remove(background_image)
            else:
                logger.info("Automesh grid info: %r" % grid_info)
        else:
            logger.info("Using default simulation grid.")
            grid_info = {
                "x1": -0.42857142857142855,
                "dx_mm": 0.60000000000000009,
                "dy_mm": 0.14523809523809525,
                "y1": -0.11190476190476191,
                "steps_y": 14,
                "steps_x": 24,
            }
            gridComment = "Simulation mesh used."
        motorPositions = collect.readMotorPositions(beamline, token=token)

    except RuntimeError:
        common.logStackTrace(workflowParameters)
        raise
    except Exception:
        common.logStackTrace(workflowParameters)
        if beamline not in ["id30a2", "simulator_mxcube", "simulator"]:
            logger.info("Using default grid.")
            if isVerticalAxis:
                grid_info = {
                    "x1": -0.05,
                    "y1": -0.20,
                    "dx_mm": 0.10,
                    "dy_mm": 0.40,
                    "steps_x": 20,
                    "steps_y": 100,
                    "angle": 0.0,
                }
            else:
                grid_info = {
                    "x1": -0.40,
                    "y1": -0.10,
                    "dx_mm": 0.80,
                    "dy_mm": 0.20,
                    "steps_x": 30,
                    "steps_y": 8,
                    "angle": 0.0,
                }
            gridComment = "Standard mesh used : 800 x 200 um."
        else:
            logger.info("Using default simulation grid.")
            grid_info = {
                "x1": -0.42857142857142855,
                "dx_mm": 0.60000000000000009,
                "dy_mm": 0.14523809523809525,
                "y1": -0.11190476190476191,
                "steps_y": 14,
                "steps_x": 24,
            }
            gridComment = "Simulation mesh used."

    try:
        snapshot_image_suffix = config.get_value(
            beamline, "AutoMesh", "snapshotImageSuffix"
        )
        if newPhi is None:
            newPhi = 0
        imagePath = os.path.join(
            str(snapshotDir),
            "snapshot_{0:03d}.{1}".format(newPhi, snapshot_image_suffix),
        )
        if not os.path.exists(imagePath):
            collect.saveSnapshot(beamline, imagePath, token=token)

        (
            meshSnapShotPath,
            htmlResultFilePath,
            jsonFilePath,
        ) = auto_mesh.createAutomeshHTMLPage(
            beamline,
            newPhi,
            grid_info,
            PIXELS_PER_MM,
            snapshotDir,
            autoMeshWorkingDir,
            pyarchHtmlDir=pyarch_html_dir,
            suffix=snapshot_image_suffix,
        )
        workflowStepType = "Automesh"
        status = "Success"
        ispyb.storeWorkflowStep(
            beamline,
            workflowId,
            workflowStepType,
            status,
            imageResultFilePath=meshSnapShotPath,
            htmlResultFilePath=htmlResultFilePath,
            resultFilePath=jsonFilePath,
            comments=gridComment,
        )
        metadata_urls = config.get_value(
            beamline, "ICAT", "metadata_urls", default_value=[]
        )
        if len(metadata_urls) > 0:
            logger.info("Storing workflow step in ICAT")
            icat.storeWorkflowStep(
                beamline,
                proposal,
                directory,
                workflowStepType.lower(),
                sample_name,
                workflowParameters["title"],
                workflowParameters["type"],
                bes_request_id,
                meshSnapShotPath,
                jsonFilePath,
            )

    except Exception as e:
        logger.warning("Error when trying to create auto mesh html page: {0}".format(e))
        common.logStackTrace(workflowParameters)

    if gridComment is not None:
        dataCollectionGroupComment += gridComment

    # Set front light to 80 %
    collect.setFrontLightLevel(beamline, 80.0, token=token)

    # Make sure we have "cell_height" and "cell_width" in grid_info
    if "cell_height" not in grid_info:
        grid_info["cell_height"] = int(grid_info["dx_mm"] / grid_info["steps_x"] * 1000)

    if "cell_width" not in grid_info:
        grid_info["cell_width"] = int(grid_info["dy_mm"] / grid_info["steps_y"] * 1000)

    return {
        "grid_info": grid_info,
        "shape": None,
        "motorPositions": motorPositions,
        "dataCollectionGroupComment": dataCollectionGroupComment,
        "autoMeshPhiPosition": newPhi,
    }


# if beamline == "id23eh1":
#     PIXELS_PER_MM = 490.0
#     zoomLevel = 2
#     beamSize = 0.020  # mm
#     overSamplingX = 1.0
#     overSamplingY = 1.0
#     xOffsetLeft = 0.05
#     xOffsetRight = 0.05
#     loopMaxWidth = 0.8  # mm
#     signPhiy = -1
#     findLargestMesh = True
# elif beamline == "id23eh2":
#     PIXELS_PER_MM = 1235.0
#     zoomLevel = 2
#     beamSize = 0.005  # mm
#     overSamplingX = 1.0
#     overSamplingY = 1.0
#     xOffsetLeft = 0.005
#     xOffsetRight = 0.005
#     loopMinWidth = 0.15  # mm
#     loopMaxWidth = 0.35  # mm
#     signPhiz = -1
#     backLightLevel = 6.0
#     findLargestMesh = True
# elif beamline == "id30a1":
#     zoomLevel = 2
#     beamSize = 0.030  # mm
#     fixedTargetApertureSize = 30
#     overSamplingX = 1.5
#     overSamplingY = 1.5
#     xOffsetLeft = 0.05
#     xOffsetRight = 0.05
#     if loopMinWidth is None:
#         loopMinWidth = 0.15  # mm
#     if loopMaxWidth is None:
#         loopMaxWidth = 0.3  # mm
#     signPhiy = -1
#     deltaPhiy = 1.15
#     backLightLevel = 20.0
#     findLargestMesh = True
#     snapshot_image_suffix = "jpg"
# elif beamline == "id30a3":
#     PIXELS_PER_MM = 625.0
#     zoomLevel = 2
#     beamSize = 0.010  # mm
#     fixedTargetApertureSize = 10
#     overSamplingX = 1.25
#     overSamplingY = 1.25
#     xOffsetLeft = 0.05
#     xOffsetRight = 0.05
#     backLightLevel = 20.0
#     loopMaxWidth = 0.35  # mm
#     signPhiy = -1
#     findLargestMesh = True
# elif beamline == "id29":
#     PIXELS_PER_MM = 641.0
#     zoomLevel = "Zoom 2"
#     beamSize = 0.030  # mm
#     overSamplingX = 1.5
#     overSamplingY = 1.5
#     xOffsetLeft = 0.05
#     xOffsetRight = 0.05
#     loopMaxWidth = 1.0  # mm
#     deltaPhiy = 1.2
#     signPhiy = -1
# elif beamline == "id30b":
#     PIXELS_PER_MM = 608.0
#     zoomLevel = 2
#     fixedTargetApertureSize = 20
#     beamSize = 0.030
#     overSamplingX = 1.5
#     overSamplingY = 1.5
#     xOffsetLeft = 0.05
#     xOffsetRight = 0.05
#     loopMaxWidth = 0.35  # mm
#     deltaPhiy = 1.1
#     signPhiy = -1
#     frontLightLevel = 0.0
#     backLightLevel = 20.0
#     findLargestMesh = True
# else:
#     PIXELS_PER_MM = 300.0
#     zoomLevel = 2
#     beamSize = 0.025  # mm
#     overSamplingX = 1.5
#     overSamplingY = 1.2
#     xOffsetLeft = 0.2
#     xOffsetRight = 0.1
#     loopMaxWidth = 1.0  # mm
#
