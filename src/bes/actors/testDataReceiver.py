"""
Created on May 7, 2015

@author: svensson
"""

import json


def run(none, booleanTrue, booleanFalse, integer1, float1, string1, dictionary1, **_):

    resultString = "none: type {0} value {1}\n".format(type(none), none)
    resultString += "booleanTrue: type {0} value {1}\n".format(
        type(booleanTrue), booleanTrue
    )
    resultString += "booleanFalse: type {0} value {1}\n".format(
        type(booleanFalse), booleanFalse
    )
    resultString += "integer1: type {0} value {1}\n".format(type(integer1), integer1)
    resultString += "float1: type {0} value {1}\n".format(type(float1), float1)
    resultString += "string1: type {0} value {1}\n".format(type(string1), string1)
    resultString += "dictionary1: type {0} value {1}\n".format(
        type(dictionary1), dictionary1
    )

    none = None
    booleanTrue = True
    booleanFalse = False
    integer1 = int(integer1)
    float1 = float(float1)
    dictionary1 = json.loads(dictionary1)

    resultStringReference = "none: type {0} value {1}\n".format(type(none), none)
    resultStringReference += "booleanTrue: type {0} value {1}\n".format(
        type(booleanTrue), booleanTrue
    )
    resultStringReference += "booleanFalse: type {0} value {1}\n".format(
        type(booleanFalse), booleanFalse
    )
    resultStringReference += "integer1: type {0} value {1}\n".format(
        type(integer1), integer1
    )
    resultStringReference += "float1: type {0} value {1}\n".format(type(float1), float1)
    resultStringReference += "string1: type {0} value {1}\n".format(
        type(string1), string1
    )
    resultStringReference += "dictionary1: type {0} value {1}\n".format(
        type(dictionary1), dictionary1
    )

    return {
        "resultString": resultString,
        "resultStringReference": resultStringReference,
    }
