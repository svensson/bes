#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__contact__ = "svensson@esrf.eu"
__copyright__ = "ESRF, 2015"
__updated__ = "2015-11-26"

import os

from bes.workflow_lib import path
from bes.workflow_lib import grid
from bes import config
from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    raw_directory,
    process_directory,
    motorPositions,
    run_number,
    expTypePrefix,
    gridOscillationRange,
    transmission,
    highResolution,
    prefix,
    suffix,
    workflow_working_dir,
    workflowParameters,
    gridExposureTime,
    sampleInfo=None,
    meshZigZag=False,
    grid_info=None,
    gridTransmission=None,
    allowZeroDegreeMeshScan=False,
    aimedResolution=None,
    observedResolution=None,
    workflow_index=None,
    token=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    # Create unique directory for ICAT
    directory, run_number = path.createUniqueICATDirectory(
        raw_directory=raw_directory,
        run_number=run_number,
        workflow_index=workflow_index,
        expTypePrefix=expTypePrefix,
    )

    logger.debug("Creating auto grid")

    motorPositions = common.jsonLoads(motorPositions)

    run_number = int(run_number)
    gridOscillationRange = float(gridOscillationRange)
    transmission = float(transmission)
    gridExposureTime = float(gridExposureTime)
    # grid_info_mxcube = None

    meshZigZag = True
    useFastMesh = True

    if gridTransmission is None:
        transmission = config.get_value(beamline, "Beam", "defaultGridTransmission")
    else:
        transmission = float(gridTransmission)

    # Check minimum scillation range
    # BES-309
    if allowZeroDegreeMeshScan:
        gridOscillationRange = 0.0
    elif beamline != "id23eh2":
        dx_mm = grid_info["dx_mm"]
        maxDistPerDegree = config.get_value(
            beamline, "Goniometer", "maxDistPerDegree", default_value=None
        )
        if (
            maxDistPerDegree is not None
            and dx_mm / gridOscillationRange > maxDistPerDegree
        ):
            logger.warning(
                "Oscillation range %.1f degrees too short for beamline %s!"
                % (gridOscillationRange, beamline)
            )
            gridOscillationRange = dx_mm / maxDistPerDegree
            logger.warning("New oscialltion range: %.1f degrees" % gridOscillationRange)

    # Mesh positions:
    meshPositions = grid.determineMeshPositions(
        beamline,
        motorPositions,
        gridOscillationRange,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
        meshZigZag,
    )

    firstImagePath = None
    for position in meshPositions:
        if position["index"] == 0:
            firstImagePath = os.path.join(directory, position["imageName"])
            logger.info("First image path %s" % firstImagePath)
            break

    meshQueueList = collect.createHelicalQueueList(
        beamline,
        directory,
        process_directory,
        run_number,
        expTypePrefix,
        prefix,
        meshPositions,
        motorPositions,
        gridExposureTime,
        gridOscillationRange,
        transmission,
        grid_info,
        meshZigZag,
    )

    if beamline != "bm14":
        dictNewParameters = collect.checkMotorSpeed(
            beamline, meshQueueList, gridExposureTime, transmission
        )
        if dictNewParameters is not None:
            gridExposureTime = dictNewParameters["gridExposureTime"]
            transmission = dictNewParameters["transmission"]
            meshQueueList = collect.createHelicalQueueList(
                beamline,
                directory,
                process_directory,
                run_number,
                expTypePrefix,
                prefix,
                meshPositions,
                motorPositions,
                gridExposureTime,
                gridOscillationRange,
                transmission,
                grid_info,
                meshZigZag,
            )
            newExposureTimeMessage = (
                "In order to perform the scan with optimal motor speed\n"
            )
            newExposureTimeMessage += (
                "the exposure time has been changed to %.3f s\n" % gridExposureTime
            )
            newExposureTimeMessage += (
                "and the transmission changed accordingly to %.1f %%.\n" % transmission
            )
            logger.info(newExposureTimeMessage)

    # Title of HTML page
    titleMeshHtmlPage = "Results of mesh scan"
    meshHtmlDirectoryName = "Mesh scan"

    # Save mesh positions to disk
    meshPositionFile = path.createNewJsonFile(
        workflow_working_dir, meshPositions, "meshPositions2D"
    )
    meshQueueListFile = path.createNewJsonFile(
        workflow_working_dir, meshQueueList, "meshQueueList"
    )
    return_dict = {
        "grid_info": grid_info,
        "shape": None,
        "meshPositionFile": meshPositionFile,
        "meshQueueListFile": meshQueueListFile,
        "firstImagePath": firstImagePath,
        "gridOscillationRange": gridOscillationRange,
        "titleMeshHtmlPage": titleMeshHtmlPage,
        "meshHtmlDirectoryName": meshHtmlDirectoryName,
        "meshZigZag": meshZigZag,
        "useFastMesh": useFastMesh,
        # "grid_info_mxcube": grid_info_mxcube,
        "directory": directory,
        "run_number": run_number,
    }

    # Try to obtain sample info
    if sampleInfo is not None:
        resolution = None
        if aimedResolution is not None:
            resolution = aimedResolution  # sampleInfo["aimedResolution"]
        elif observedResolution is not None:
            resolution = observedResolution
        if resolution is not None:
            if resolution < highResolution:
                logger.warning(
                    "Observed resolution = %.2f A from ISPyB too high for %s!"
                    % (resolution, beamline)
                )
                resolution = highResolution
                logger.warning("Resolution set to %.2f A" % resolution)
            elif resolution > 5.0:
                logger.warning(
                    "Observed resolution = %.2f A from ISPyB too low for %s!"
                    % (resolution, beamline)
                )
                resolution = 5.0
                logger.warning("Resolution set to %.2f A" % resolution)
            else:
                logger.info(
                    "Using observed resolution = %.2f A from ISPyB" % resolution
                )
            return_dict["resolution"] = resolution

    # Take a crystal snapshot before mesh
    snapShotFileName = "%s_%d_snapshot_before_mesh.png" % (prefix, run_number)
    snapShotDirectory = path._getPyarchFilePath(directory)
    snapShotFilePath = collect.takeOneCrystalSnapshot(
        beamline,
        directory,
        prefix,
        run_number,
        snapShotFileName,
        snapShotDirectory,
        token=token,
    )
    return_dict["snapShotFilePath"] = snapShotFilePath
    return_dict["gridExposureTime"] = gridExposureTime
    return_dict["transmission"] = transmission

    return return_dict
