from bes.workflow_lib import dialog
from bes.workflow_lib import collect


def run(
    beamline,
    directory,
    characterisationOscillationRange,
    characterisationExposureTime,
    transmission,
    characterisationTransmission,
    automatic_mode=False,
    resolution=None,
    collection_software=None,
    token=None,
    **_,
):

    characterisationExposureTime = float(characterisationExposureTime)
    characterisationOscillationRange = float(characterisationOscillationRange)
    transmission = float(transmission)
    characterisationTransmission = float(characterisationTransmission)

    if resolution is None:
        resolution = collect.getResolution(beamline, token=token)
    else:
        resolution = float(resolution)

    listDialog = [
        {
            "variableName": "no_reference_images",
            "label": "Number of reference images",
            "type": "int",
            "defaultValue": 2,
            "unit": "",
            "lowerBound": 0.0,
            "upperBound": 4.0,
        },
        {
            "variableName": "angle_between_reference_images",
            "label": "Angle between reference images",
            "type": "combo",
            "defaultValue": "90",
            "textChoices": ["30", "45", "60", "90"],
        },
        {
            "variableName": "characterisationExposureTime",
            "label": "Characterisation exposure time",
            "type": "float",
            "value": characterisationExposureTime,
            "unit": "%",
            "lowerBound": 0.0,
            "upperBound": 100.0,
        },
        {
            "variableName": "characterisationOscillationRange",
            "label": "Characterisation oscillation range",
            "type": "float",
            "value": characterisationOscillationRange,
            "unit": "%",
            "lowerBound": 0.1,
            "upperBound": 10.0,
        },
        {
            "variableName": "susceptibility",
            "label": "Susceptibility, default 1, - increase for radiation-sensitive crystals",
            "type": "float",
            "value": 1.0,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 10.0,
        },
        {
            "variableName": "transmission",
            "label": "Transmission for line scans",
            "type": "float",
            "value": transmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "characterisationTransmission",
            "label": "Transmission for characterisation reference images",
            "type": "float",
            "value": characterisationTransmission,
            "unit": "%",
            "lowerBound": 0,
            "upperBound": 100.0,
        },
        {
            "variableName": "resolution",
            "label": "Resolution",
            "type": "float",
            "defaultValue": resolution,
            "unit": "A",
            "lowerBound": 0.5,
            "upperBound": 7.0,
        },
        {
            "variableName": "do_data_collect",
            "label": "Do data collection?",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "doVerticalXrayCentring",
            "label": "Do X-ray refinement of start and end positions?",
            "type": "boolean",
            "value": False,
        },
        {
            "variableName": "diffractionSignalDetection",
            "label": "Diffraction signal detection (if X-ray refinement of positions)",
            "type": "combo",
            "textChoices": [
                "DozorM2 (macro-molecules crystal detection)",
                "Dozor (macro-molecules)",
                "Spotfinder (small molecules)",
            ],
            "value": "DozorM2 (macro-molecules crystal detection)",
        },
    ]
    dictValues = dialog.openDialog(
        beamline,
        listDialog,
        directory=directory,
        token=token,
        automaticMode=automatic_mode,
        collection_software=collection_software,
    )

    return dictValues
