import os
import pprint

from bes.workflow_lib import path
from bes.workflow_lib import ispyb
from bes.workflow_lib import collect


def run(
    beamline,
    directory,
    run_number,
    prefix,
    expTypePrefix="",
    dataCollectionId=None,
    token=None,
    **_,
):

    listAnglePath = []
    listSnapShotPath = []

    pyarchDir = path.createPyarchFilePath(directory)
    snapShotFileName = "{0}{1}_{2}.snapshot.png".format(
        expTypePrefix, prefix, run_number
    )
    strPathToSnapshot = os.path.join(pyarchDir, snapShotFileName)
    collect.saveSnapshot(beamline, strPathToSnapshot, token=token)
    listSnapShotPath.append(strPathToSnapshot)
    if dataCollectionId is not None:
        ispyb.updateDataCollectionSnapShots(
            beamline, int(dataCollectionId), listSnapShotPath
        )

    return {
        "pyarchDir": pyarchDir,
        "listAnglePath": listAnglePath,
        "listSnapShotFileName": listSnapShotPath,
    }


if __name__ == "__main__":
    workflowParameters = {}
    beamline = "id30a2"
    directory = (
        "/data/visitor/mx415/id30a2/20170209/RAW_DATA/t1/EnhancedCharacterisation_01"
    )
    run_number = 1
    prefix = "test"
    expTypePrefix = "ref-"
    dataCollectionId = "1776454"
    dictResult = run(
        beamline,
        workflowParameters,
        directory,
        run_number,
        prefix,
        expTypePrefix=expTypePrefix,
        dataCollectionId=dataCollectionId,
    )
    pprint.pprint(dictResult)
