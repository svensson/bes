from bes.workflow_lib import edna_mxv1


def run(directory=None, beamline=None, **_):

    return_dict = {}

    if beamline is None:
        try:
            beamline = edna_mxv1.extractBeamlineFromDirectory(directory)
        except Exception:
            beamline = "unknown"

    return_dict["workflow_title"] = "Test Gphl workflow on {0}".format(beamline)
    return_dict["workflow_type"] = "TestGphlWorkflow"
    return_dict["beamline"] = beamline.lower()

    return return_dict
