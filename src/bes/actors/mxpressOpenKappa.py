#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__author__ = "Olof Svensson"
__license__ = "MIT"
__copyright__ = "ESRF"
__updated__ = "03/05/2021"

from bes.workflow_lib import grid
from bes.workflow_lib import collect
from bes import config
from bes.workflow_lib import workflow_logging
from bes.workflow_lib import kappa_reorientation


def run(
    beamline,
    directory,
    workflowParameters=None,
    token=None,
    kappa_settings_id=None,
    newApertureSize=None,
    **_,
):

    logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)

    newKappaAngle = config.get_value(beamline, "Goniometer", "lowSymmetryKappaAngle")

    motorPositions = collect.readMotorPositions(beamline, token=token)
    kappa_settings_id += 1
    sampx = motorPositions["sampx"]
    sampy = motorPositions["sampy"]
    phiy = motorPositions["phiy"]
    kappa = motorPositions["kappa"]
    kappaPhi = motorPositions["kappa_phi"]
    newMotorPositions = kappa_reorientation.getNewSamplePosition(
        beamline, kappa, kappaPhi, sampx, sampy, phiy, newKappaAngle, kappaPhi
    )
    newMotorPositions["kappa"] = newKappaAngle

    collect.moveMotors(beamline, directory, newMotorPositions, token=token)

    logger.info("Kappa angle set to {0}".format(newKappaAngle))

    if newApertureSize is None:
        newApertureSize = collect.getBeamSize(beamline, token)

    logger.info("Current aperture: {0} um".format(newApertureSize))

    smallGridApertureSize = config.get_value(
        beamline, "AutoMesh", "fixedTargetApertureSize"
    )
    if newApertureSize != smallGridApertureSize and beamline in [
        "id23eh1",
        "id30a1",
        "id30a3",
        "id30b",
    ]:
        logger.info(
            "Aperture for small grid/line scan: {0} um".format(smallGridApertureSize)
        )
        if not collect.setApertureSize(beamline, smallGridApertureSize, token=token):
            smallGridApertureSize = None
    else:
        smallGridApertureSize = None

    if smallGridApertureSize is None:
        grid_info = grid.getSmallXrayCentringGrid(
            newApertureSize / 1000.0, newApertureSize / 1000.0
        )
    else:
        grid_info = grid.getSmallXrayCentringGrid(
            smallGridApertureSize / 1000.0, smallGridApertureSize / 1000.0
        )

    motorPositions = collect.readMotorPositions(beamline, token=token)

    return {
        "grid_info": grid_info,
        "shape": None,
        "expTypePrefix": "mesh-",
        "shortLineScan": True,
        "motorPositions": motorPositions,
        "kappa_settings_id": kappa_settings_id,
        "newApertureSize": newApertureSize,
        "smallGridApertureSize": smallGridApertureSize,
    }
