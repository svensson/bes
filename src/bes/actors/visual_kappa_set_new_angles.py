import time

from bes.workflow_lib import common
from bes.workflow_lib import collect
from bes.workflow_lib import workflow_logging


def run(
    beamline,
    workflowParameters,
    directory,
    newKappa,
    newKappa_phi,
    newPhi,
    newSampx=None,
    newSampy=None,
    newPhiy=None,
    token=None,
    **_,
):

    try:

        logger = workflow_logging.getLogger(beamline, workflowParameters, token=token)
        newKappa = float(newKappa)
        newKappa_phi = float(newKappa_phi)
        newPhi = float(newPhi)

        if beamline in ["id29"]:
            logger.info("Setting new kappa angles.")
            collect.moveMotors(beamline, directory, {"kappa": newKappa}, token=token)
            collect.moveMotors(
                beamline, directory, {"kappa_phi": newKappa_phi}, token=token
            )
            collect.moveMotors(beamline, directory, {"phi": newPhi}, token=token)
        else:
            logger.info("Setting new kappa angles and sample position.")
            newSampx = float(newSampx)
            newSampy = float(newSampy)
            newPhi = float(newPhiy)
            collect.moveMotors(
                beamline,
                directory,
                {
                    "kappa": newKappa,
                    "kappa_phi": newKappa_phi,
                    "phi": newPhi,
                    "sampx": newSampx,
                    "sampy": newSampy,
                    "phiy": newPhiy,
                },
                token=token,
            )
        time.sleep(1)

        dict_diffractometer_positions = collect.readMotorPositions(
            beamline, token=token
        )

        logger.debug("Motor positions: %r" % dict_diffractometer_positions)

    except Exception:
        common.logStackTrace(workflowParameters)
        raise

    return dict_diffractometer_positions
