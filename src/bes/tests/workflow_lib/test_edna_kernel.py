import os

from XSDataCommon import XSDataString

from bes.workflow_lib import edna_kernel


def test_executeEdnaPlugin(workflow_working_dir, edna_id30a2):
    xsDataString = XSDataString("test")
    xsDataResult, ednaLogPath = edna_kernel.executeEdnaPlugin(
        "EDPluginTestPluginFactory", xsDataString, str(workflow_working_dir)
    )
    assert os.path.exists(ednaLogPath)
    assert xsDataResult.value == "test", "Not correct result"
