import os
import json
from glob import glob

import fabio
import pytest

from bes.workflow_lib import grid
from bes.workflow_lib import edna_mxv1
from bes.workflow_lib import mini_cbf_utils

from .xsdata import load_xsdata


@pytest.mark.skipif(
    not os.path.exists("/data/id14eh4/inhouse/opid144/20130220/RAW_DATA/WF/burn"),
    reason="Directory not available",
)
def test_storeImageQualityIndicators(workflow_working_dir):
    edna_mxv1.storeImageQualityIndicators(
        beamline="simulator",
        mxv1StrategyResult=load_xsdata("burn_strategy"),
        directory="/data/id14eh4/inhouse/opid144/20130220/RAW_DATA/WF/burn",
        run_number=6,
        expTypePrefix="ref-",
        prefix="ref-p1burnw1",
        suffix="img",
        workflow_working_dir=workflow_working_dir,
    )


@pytest.mark.skipif(
    not os.path.exists("/data/id23eh1/inhouse/opid231/20130417/RAW_DATA/kappa/test2"),
    reason="Directory not available",
)
def test_processBurnStrategy(workflow_working_dir, edna_id23eh1):
    edna_mxv1.processBurnStrategy(
        beamline="simulator",
        directory="/data/id23eh1/inhouse/opid231/20130417/RAW_DATA/kappa/test2",
        run_number=2,
        expTypePrefix="burn-",
        prefix="test2",
        suffix="cbf",
        mxv1ResultCharacterisation=load_xsdata("mxv1ResultCharacterisation"),
        workflow_working_dir=workflow_working_dir,
    )


@pytest.mark.skipif(
    not os.path.exists(
        "/sware/exp/pxsoft/bes/vgit/linux-x86_64/id30b/edna2/testdata/images"
    ),
    reason="Directory not available",
)
def test_mxv1ControlInterface(workflow_working_dir, edna_id30b):
    mxv1StrategyResult = edna_mxv1.create_ref_data_collect(
        exposureTime=0.01,
        transmission=100,
        osc_range=0.1,
        no_reference_images=1,
        angle_between_reference_images=90,
        phi=0.0,
    )
    run_number = "1"
    expTypePrefix = ""
    prefix = "ab_test-eiger"
    directory = "/sware/exp/pxsoft/bes/vgit/linux-x86_64/id30b/edna2/testdata/images"
    suffix = "h5"
    beamline = "id30b"
    flux = 1e12
    beamSizeX = 0.1
    beamSizeY = 0.1
    strategyOption = None
    minExposureTime = None
    crystalSizeX = 0.5
    crystalSizeY = 0.3
    crystalSizeZ = 0.15
    anomalousData = True

    # Generate image paths
    listImages = edna_mxv1.getListImagePath(
        mxv1StrategyResult,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        bFirstImageOnly=False,
    )
    edna_mxv1.h5ToCbf(beamline, listImages, str(workflow_working_dir))
    suffix = "cbf"
    listImages = [imagePath.replace(".h5", ".cbf") for imagePath in listImages]

    xs_data_result, edna_log_path = edna_mxv1.mxv1ControlInterface(
        beamline,
        listImages,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        flux,
        beamSizeX,
        beamSizeY,
        strategyOption,
        minExposureTime,
        str(workflow_working_dir),
        crystalSizeX=crystalSizeX,
        crystalSizeY=crystalSizeY,
        crystalSizeZ=crystalSizeZ,
        anomalousData=anomalousData,
        bLogExecutiveSummary=True,
    )

    assert os.path.exists(edna_log_path)
    with open(edna_log_path) as fd:
        print(fd.read())


@pytest.mark.skipif(
    not os.path.exists("/data/id14eh4/inhouse/opid144/20130403/RAW_DATA/Andrew/test"),
    reason="Directory not available",
)
def test_mxv2ControlInterface(workflow_working_dir):
    beamline = "simulator"
    mxv1StrategyResult = edna_mxv1.create_ref_data_collect(
        exposureTime=0.01,
        transmission=100,
        osc_range=0.1,
        no_reference_images=1,
        angle_between_reference_images=90,
        phi=0.0,
    )
    run_number = 2
    expTypePrefix = "ref-"
    prefix = "TestX1w1"
    directory = "/data/id14eh4/inhouse/opid144/20130403/RAW_DATA/Andrew/test"
    suffix = "img"
    flux = 1e12
    beamSizeX = 0.1
    beamSizeY = 0.1
    kappaStrategyOption = "CELL"
    phi = 0.0
    omega = 0.0
    kappa = 0.0

    xs_data_result, edna_log_path = edna_mxv1.mxv2ControlInterface(
        beamline,
        mxv1StrategyResult,
        directory,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        omega,
        kappa,
        phi,
        flux,
        beamSizeX,
        beamSizeY,
        kappaStrategyOption,
        str(workflow_working_dir),
    )

    assert os.path.exists(edna_log_path)
    with open(edna_log_path) as fd:
        print(fd.read())


@pytest.mark.skip("TODO")
def test_createSimpleHtmlPage(workflow_working_dir):
    beamline = "simulator"
    mxv1ResultCharacterisation = (
        "<characterisation_data_here>"  # Mock or use actual test data
    )
    pyarch_html_dir = None

    # Call createSimpleHtmlPage and check the result
    strPath, strJsonPath = edna_mxv1.createSimpleHtmlPage(
        beamline, mxv1ResultCharacterisation, str(workflow_working_dir), pyarch_html_dir
    )

    # Verify that the generated path exists
    assert os.path.exists(strPath), f"Expected file at {strPath} was not found."


@pytest.mark.skip("TODO")
def test_createSimpleHtmlPagev2_0(workflow_working_dir):
    beamline = "simulator"
    mxv2ResultCharacterisation = (
        "<characterisation_data_here>"  # Mock or use actual test data
    )
    pyarch_html_dir = None

    # Call createSimpleHtmlPagev2_0 and check the result
    strPath, strJsonPath = edna_mxv1.createSimpleHtmlPagev2_0(
        beamline, mxv2ResultCharacterisation, str(workflow_working_dir), pyarch_html_dir
    )

    # Verify that the generated path exists
    assert os.path.exists(strPath), f"Expected file at {strPath} was not found."


@pytest.mark.skipif(
    not os.path.exists("/data/scisoft/pxsoft/data/FastMesh1D/RAW_DATA"),
    reason="Directory not available",
)
def test_storeImageQualityIndicatorsFromMeshPositions_2D(workflow_working_dir):
    beamline = "simulator"
    directory = "/data/scisoft/pxsoft/data/FastMesh1D/RAW_DATA"
    run_number = 2
    expTypePrefix = "4dscan-"
    prefix = "testw1"
    suffix = "cbf"
    osc_range = 0.1
    motor_positions = {
        "phi": 0.0,
        "kappa": 0.0,
        "kappa_phi": 0.0,
        "sampx": 0.0,
        "sampy": 0.0,
        "phix": 0.0,
        "phiy": 0.0,
        "phiz": 0.0,
        "focus": 0.0,
        "chi": 0.0,
    }

    grid_info = {
        "x1": 0.0,
        "y1": 0.0,
        "dx_mm": 1.0,
        "dy_mm": 1.0,
        "steps_x": 100,
        "steps_y": 1,
        "angle": 0.0,
    }

    mesh_positions = grid.determineMeshPositions(
        beamline,
        motor_positions,
        osc_range,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
    )

    edPlugin = edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsStart(
        beamline, directory, mesh_positions, str(workflow_working_dir), grid_info
    )
    xs_data_result, inputDozor = (
        edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsSynchronize(
            edPlugin, mesh_positions
        )
    )


@pytest.mark.skipif(
    not os.path.exists(
        "/data/scisoft/pxsoft/data/Mesh/opid231/20130507/RAW_DATA/mesh003"
    ),
    reason="Directory not available",
)
def test_storeImageQualityIndicatorsFromMeshPositions_1D(workflow_working_dir):
    pytest.skip("TODO")

    beamline = "simulator"
    directory = "/data/scisoft/pxsoft/data/Mesh/opid231/20130507/RAW_DATA/mesh003"
    expTypePrefix = "mesh-"
    prefix = "test"
    suffix = "cbf"
    run_number = 2

    motor_positions = {
        "phi": 0.0,
        "kappa": 0.0,
        "kappa_phi": 0.0,
        "sampx": 0.0,
        "sampy": 0.0,
        "phix": 0.0,
        "phiy": 0.0,
        "phiz": 0.0,
        "focus": 0.0,
        "chi": 0.0,
    }

    osc_range = 0.1
    grid_info = {
        "angle": 0.0,
        "dx_mm": 0.100,
        "dy_mm": 0.110,
        "steps_x": 11,
        "steps_y": 1,
        "x1": -0.1036,
        "y1": -0.0621,
    }

    mesh_positions = grid.determineMeshPositions(
        beamline,
        motor_positions,
        osc_range,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
    )

    edPlugin = edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsStart(
        beamline, directory, mesh_positions, str(workflow_working_dir), grid_info
    )
    xs_data_result, inputDozor = (
        edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsSynchronize(
            edPlugin, mesh_positions
        )
    )


@pytest.mark.skipif(
    not os.path.exists(
        "/data/scisoft/pxsoft/data/Mesh/opid231/20130507/RAW_DATA/mesh001"
    ),
    reason="Directory not available",
)
def test_storeImageQualityIndicatorsFromMeshPositions(workflow_working_dir):

    beamline = "simulator"
    directory = "/data/scisoft/pxsoft/data/Mesh/opid231/20130507/RAW_DATA/mesh001"
    expTypePrefix = "mesh-"
    prefix = "mesh"
    suffix = "cbf"
    run_number = 1

    motor_positions = {
        "phi": 0.0,
        "kappa": 0.0,
        "kappa_phi": 0.0,
        "sampx": 0.0,
        "sampy": 0.0,
        "phix": 0.0,
        "phiy": 0.0,
        "phiz": 0.0,
        "focus": 0.0,
        "chi": 0.0,
    }

    osc_range = 0.1
    grid_info = json.loads(
        """{
            "angle": 0.0,
            "dx_mm": 0.100,
            "dy_mm": 0.110,
            "steps_x": 11,
            "steps_y": 11,
            "x1": -0.1036,
            "y1": -0.0621
        }"""
    )

    mesh_positions = grid.determineMeshPositions(
        beamline,
        motor_positions,
        osc_range,
        run_number,
        expTypePrefix,
        prefix,
        suffix,
        grid_info,
    )

    edPlugin = edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsStart(
        beamline, directory, mesh_positions, str(workflow_working_dir), grid_info
    )
    _ = edna_mxv1.storeImageQualityIndicatorsFromMeshPositionsSynchronize(
        edPlugin, mesh_positions
    )


@pytest.mark.skipif(
    not os.path.exists(
        "/data/visitor/mx2503/id23eh1/20230412/RAW_DATA/Sample-2-2-02/ref-test_LC_1_0001.h5"
    ),
    reason="File not available",
)
def test_create_thumbnails_for_pyarch(workflow_working_dir):
    list_image = [
        "/data/visitor/mx2503/id23eh1/20230412/RAW_DATA/Sample-2-2-02/ref-test_LC_1_0001.h5"
    ]

    edna_mxv1.create_thumbnails_for_pyarch(list_image, str(workflow_working_dir))


@pytest.mark.skipif(
    not os.path.exists("/data/id23eh2/inhouse/opid232/20130516/RAW_DATA/delete_me"),
    reason="Directory not available",
)
def test_create_thumbnails_for_pyarch_asynchronous():
    beamline = "simulator"
    list_image_creation = [
        {
            "isCreated": True,
            "fileLocation": "/data/id23eh2/inhouse/opid232/20130516/RAW_DATA/delete_me",
            "imageId": 43645776,
            "fileName": "delete_me_1_0010.mccd",
        }
    ]

    edna_mxv1.create_thumbnails_for_pyarch_asynchronous(beamline, list_image_creation)


@pytest.mark.skipif(
    not os.path.exists("/data/scisoft/pxsoft/data/DOZOR/id23eh1/20160229"),
    reason="Directory not available",
)
def test_runDozorForHalfDoseTime(workflow_working_dir):
    beamline = "simulator"
    dataDirectory = "/data/scisoft/pxsoft/data/DOZOR/id23eh1/20160229"
    prefix = "x"
    runNumber = 1
    wedgeNumber = 10
    noCycles = 10
    listImages = []

    for index in range(wedgeNumber * noCycles):
        imagePath = os.path.join(
            dataDirectory,
            "{0}_{1}_{2:04d}.cbf".format(prefix, runNumber, index + 1),
        )
        listImages.append(imagePath)

    halfDoseTime, xsDataResult = edna_mxv1.runDozorForHalfDoseTime(
        beamline, listImages, wedgeNumber, str(workflow_working_dir)
    )


@pytest.mark.skipif(
    not os.path.exists("/data/id30a3/inhouse/opid30a3/20160301/RAW_DATA"),
    reason="Directory not available",
)
def test_h5ToCbf(workflow_working_dir):
    beamline = "simulator"
    dir = "/data/id30a3/inhouse/opid30a3/20160301/RAW_DATA"
    dir = os.path.join(dir, "test1/EnhancedCharacterisation_03")

    imagePath1 = os.path.join(dir, "ref-x_1_0001.h5")
    imagePath2 = os.path.join(dir, "ref-x_1_0002.h5")

    if not os.path.exists(imagePath1) or not os.path.exists(imagePath2):
        pytest.skip("H5 image files are missing, skipping test.")

    listImages = [imagePath1, imagePath2]

    output_dir = workflow_working_dir / ".." / "results"
    edna_mxv1.h5ToCbf(beamline, listImages, workflow_working_dir, str(output_dir))

    for image in listImages:
        output_image = image.replace(".h5", ".cbf")
        assert os.path.exists(
            output_image
        ), f"Converted file {output_image} was not created."


@pytest.mark.skipif(
    not os.path.exists(
        "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA/id30a1/20140901/RAW_DATA"
    ),
    reason="Directory not available",
)
def test_waitForFile(workflow_working_dir):
    beamline = "simulator"

    dir = "/data/scisoft/pxsoft/data/WORKFLOW_TEST_DATA"
    dir = os.path.join(dir, "id30a1/20140901/RAW_DATA")

    imagePath1 = os.path.join(dir, "line-phiz_centred_6_0001.cbf")
    imagePath2 = "/path/that/does/not/exist"

    assert not edna_mxv1.waitForFile(beamline, imagePath1, str(workflow_working_dir))

    assert edna_mxv1.waitForFile(
        beamline, imagePath2, str(workflow_working_dir), timeOut=5
    )


@pytest.mark.skipif(
    not os.path.exists("/data/scisoft/pxsoft/data/DOZOR/id23eh1/20160229"),
    reason="Directory not available",
)
def test_runFbest(workflow_working_dir):
    flux = 1e12
    resolution = 2.0

    _ = edna_mxv1.runFbest(
        flux=flux,
        resolution=resolution,
        workflow_working_dir=str(workflow_working_dir),
    )


@pytest.mark.skipif(
    not os.path.exists(
        "/data/id30a1/inhouse/opid30a1/20220408/RAW_DATA/Sample-8-1-01/MXPressA_01"
    ),
    reason="Directory not available",
)
def test_getListFineSlicedReferenceImagePath(workflow_working_dir):
    phi = 123.0
    osc_range = 0.1
    nsum = 10

    strategy_result = edna_mxv1.create_n_images_data_collect(
        exposureTime=1.0,
        transmission=100,
        number_of_images=nsum,
        osc_range=osc_range,
        phi=phi,
    )
    directory = (
        "/data/id30a1/inhouse/opid30a1/20220408/RAW_DATA/Sample-8-1-01/MXPressA_01"
    )
    run_number = 1
    exp_type_prefix = "ref-fineslice-"
    prefix = "opid30a1"
    suffix = "cbf"

    workflow_working_dir.mkdir()
    edna_mxv1.getListFineSlicedReferenceImagePath(
        beamline="id30a1",
        mxv1StrategyResult=strategy_result,
        characterisationOscillationRange=osc_range * nsum,
        directory=directory,
        run_number=run_number,
        expTypePrefix=exp_type_prefix,
        prefix=prefix,
        suffix=suffix,
        dest_dir=str(workflow_working_dir),
    )

    files = sorted(glob(os.path.join(directory, "ref-fineslice-opid30a1_1_*.cbf")))

    for i in range(4):
        expected = sum(
            fabio.open(filename).data[13, 14]
            for filename in files[i * nsum : (i + 1) * nsum]
        )

        filename = workflow_working_dir / f"ref-opid30a1_1_{i+1:04d}.cbf"
        assert os.path.exists(filename)

        with fabio.open(filename) as fh:
            actual = fh.data[13, 14]

            assert actual == expected
            assert fh.data.min() == -2

            mini_cbf_header = fh.header["_array_data.header_contents"]
            header = mini_cbf_utils.deserialize_mini_cbf_header(
                mini_cbf_header, as_dict=True
            )

            Start_angle = (phi + i * 90, "deg.")
            Angle_increment = (osc_range * nsum, "deg.")
            assert header["Start_angle"] == Start_angle
            assert header["Angle_increment"] == Angle_increment
