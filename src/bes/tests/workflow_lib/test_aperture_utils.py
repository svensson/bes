import pytest

from bes.workflow_lib import aperture_utils


def test_aperture_name_size_mapping():
    beamline = "id23eh2"
    names = "4x4 um", "8x4 um", "25x4 um", "20.5x5 um", "45x5 um", "50x5 um"
    sizes = 4, 4, 4, 5, 5, 5

    for name, size in zip(names, sizes):
        assert aperture_utils.aperture_name_to_size(beamline, name) == size

    assert aperture_utils.aperture_size_to_name(beamline, 4) == "25x4 um"
    assert aperture_utils.aperture_size_to_name(beamline, 5) == "50x5 um"

    with pytest.raises(ValueError):
        _ = aperture_utils.aperture_name_to_size(beamline, "wrongvalue")

    with pytest.raises(ValueError):
        _ = aperture_utils.aperture_size_to_name(beamline, 999)

    with pytest.raises(TypeError):
        _ = aperture_utils.aperture_name_to_size(beamline, 999)

    with pytest.raises(TypeError):
        _ = aperture_utils.aperture_size_to_name(beamline, "wrongtype")


def test_aperture_factor_mapping():
    beamline = "id30a1"
    sizes = 10, 20, 30, 50, 100
    names = "A10", "A20", "A30", "A50", "A100"
    factors = 0.03, 0.11, 0.18, 0.39, 0.8, 1

    for name, size, factor in zip(names, sizes, factors):
        assert (
            aperture_utils.aperture_size_to_correction_factor(beamline, size) == factor
        )
        assert (
            aperture_utils.aperture_name_to_correction_factor(beamline, name) == factor
        )

    with pytest.raises(ValueError):
        _ = aperture_utils.aperture_name_to_correction_factor(beamline, "wrongvalue")

    with pytest.raises(TypeError):
        _ = aperture_utils.aperture_name_to_correction_factor(beamline, 999)

    with pytest.raises(ValueError):
        _ = aperture_utils.aperture_size_to_correction_factor(beamline, 999)

    with pytest.raises(TypeError):
        _ = aperture_utils.aperture_size_to_correction_factor(beamline, "wrongtype")


def test_aperture_closest_size():
    beamline = "id30a1"
    # sizes = 10, 20, 30, 50, 100

    assert aperture_utils.closest_aperture_size(beamline, 5) == 10
    assert aperture_utils.closest_aperture_size(beamline, 25) == 20
    assert aperture_utils.closest_aperture_size(beamline, 26) == 30
    assert aperture_utils.closest_aperture_size(beamline, 50) == 50
    assert aperture_utils.closest_aperture_size(beamline, 200) == 100
