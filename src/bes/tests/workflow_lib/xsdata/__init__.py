import sys

if sys.version_info < (3, 9):
    import importlib_resources  # for Python < 3.9
else:
    import importlib.resources as importlib_resources  # for Python >= 3.9


def load_xsdata(basename: str) -> str:
    with importlib_resources.open_text(__name__, f"{basename}.xml") as file:
        return file.read()
