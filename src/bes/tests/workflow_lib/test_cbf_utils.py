import pathlib
from typing import Tuple, List

import numpy
import pytest
from fabio import cbfimage

from bes.workflow_lib import cbf_utils
from bes.workflow_lib import mini_cbf_utils


@pytest.mark.parametrize("preserve_dtype", [True, False])
def test_sum_images(tmp_path: pathlib.Path, preserve_dtype):
    sub_wedge_no = 2
    nimages = 10
    angle_increment = 0.1

    filenames, sumdata = _generate_cbf_files(
        tmp_path, sub_wedge_no, nimages, angle_increment, preserve_dtype
    )

    rotation_axis_start = 0.0
    characterisationOscillationRange = nimages * angle_increment

    angle_start = rotation_axis_start + 90 * sub_wedge_no
    angle_increment = characterisationOscillationRange

    sumcbf = cbf_utils.sum_cbf_images(
        filenames, angle_start, angle_increment, preserve_dtype=preserve_dtype
    )
    numpy.testing.assert_array_equal(sumdata, sumcbf.data)


def test_mini_cbf_header_parsing():
    start_angle = 3
    mini_cbf_header = _DECTRIS_ID30A1_HEADER["_array_data.header_contents"]
    mini_cbf_header += f"{start_angle:.4f} deg."

    parsed_header = mini_cbf_utils.deserialize_mini_cbf_header(mini_cbf_header)

    header_dict = mini_cbf_utils.deserialize_mini_cbf_header(
        mini_cbf_header, as_dict=True
    )
    expected = {
        "Alpha": (0.0, "deg."),
        "Angle_increment": (0.1, "deg."),
        "Beam_xy": ((1198.07, 1305.2), "pixels"),
        "Chi": (0.0, "deg."),
        "Count_cutoff": (1048500, None),
        "Detector": ("PILATUS3 6M, S/N 60-0128, ESRF ID30", None),
        "Detector_2theta": (0.0, "deg."),
        "Detector_Voffset": (0.0, "m"),
        "Detector_distance": (0.388637, "m"),
        "Energy_range": ((0, 0), "eV"),
        "Excluded_pixels": ("badpix_mask.tif", None),
        "Exposure_period": (0.01595, "s"),
        "Exposure_time": (0.015, "s"),
        "Flat_field": (None, None),
        "Flux": (683049870000.0, None),
        "Kappa": (0.0, "deg."),
        "N_excluded_pixels:": (321, None),
        "N_oscillations": (40, None),
        "Oscillation_axis": ("omega", None),
        "Phi": (0.0, "deg."),
        "Pixel_size": ((0.000172, 0.000172), "m"),
        "Polarization": (0.99, None),
        "Silicon sensor, thickness": (0.001, "m"),
        "Start_angle": (3.0, "deg."),
        "Tau": (0, "s"),
        "Threshold_setting": (6421, "eV"),
        "Transmission": (100.0, None),
        "Trim_directory": (None, None),
        "Wavelength": (0.965459, "A"),
        "file_comments": ("", None),
    }
    assert header_dict == expected

    reconstructed_mini_cbf_header = mini_cbf_utils.serialize_mini_cbf_header(
        parsed_header
    )
    expected = [
        "# Detector: PILATUS3 6M, S/N 60-0128, ESRF ID30",
        "# 2024/Nov/07 10:17:09",
        "",
        "# Silicon sensor, thickness: 0.001 m",
        "",
        "# Pixel_size: (0.000172, 0.000172) m",
        "# file_comments",
        "# N_oscillations: 40",
        "# Oscillation_axis: omega",
        "# Chi: 0.0 deg.",
        "# Phi: 0.0 deg.",
        "# Kappa: 0.0 deg.",
        "# Alpha: 0.0 deg.",
        "# Polarization: 0.99",
        "# Detector_2theta: 0.0 deg.",
        "# Angle_increment: 0.1 deg.",
        "# Transmission: 100.0",
        "# Flux: 683049870000.0",
        "# Beam_xy: (1198.07, 1305.2) pixels",
        "# Detector_Voffset: 0.0 m",
        "# Energy_range: (0, 0) eV",
        "# Detector_distance: 0.388637 m",
        "# Wavelength: 0.965459 A",
        "# Trim_directory: (nil)",
        "# Flat_field: (nil)",
        "# Excluded_pixels: badpix_mask.tif",
        "# N_excluded_pixels: 321",
        "# Threshold_setting: 6421 eV",
        "# Count_cutoff: 1048500",
        "# Tau: 0 s",
        "# Exposure_period: 0.01595 s",
        "# Exposure_time: 0.015 s",
        "# Start_angle: 3.0 deg.",
    ]
    reconstructed = reconstructed_mini_cbf_header.replace("\r\n", "\n").splitlines()
    assert reconstructed == expected


def _generate_cbf_files(
    tmp_path: pathlib.Path,
    sub_wedge_no: int,
    nimages: int,
    angle_increment: float,
    preserve_dtype: bool,
) -> Tuple[List[str], numpy.ndarray]:
    start_image_index = (sub_wedge_no + 1) * nimages
    start_angle = angle_increment * start_image_index

    image_indices = list(range(start_image_index, start_image_index + nimages))
    start_angles = numpy.arange(
        start_angle, start_angle + nimages * angle_increment, angle_increment
    )

    basename_template = "ref-fineslice-Tk-CD042366_C07-3_1_4_{:04d}.cbf"
    filenames = list()

    dtype = numpy.int32
    max_value = 2147483647  # maximum signed-int32 value

    for image_index, start_angle in zip(image_indices, start_angles):
        header = _DECTRIS_ID30A1_HEADER.copy()
        header["_array_data.header_contents"] += f"{start_angle:.4f} deg."

        file_index = image_index + 1
        filename = str(tmp_path / basename_template.format(file_index))
        filenames.append(filename)

        data = numpy.ones((10, 20), dtype=dtype)
        data[0, 0] = 0
        data[0, 1] = -1
        data[0, 2] = -2
        data[0, 3] = max_value
        img = cbfimage.CbfImage(data=data, header=header)
        img.write(filename)

    if not preserve_dtype:
        dtype = numpy.int64

    sumdata = numpy.ones((10, 20), dtype=dtype) * nimages
    sumdata[0, 0] = 0
    sumdata[0, 1] = -1
    sumdata[0, 2] = -2
    if preserve_dtype:
        sumdata[0, 3] = max_value
    else:
        sumdata[0, 3] = max_value * nimages

    return filenames, sumdata


_DECTRIS_ID30A1_HEADER = {
    "_Misc.acq_autoexpo_mode": "OFF",
    "_Misc.acq_expo_time": "0.015",
    "_Misc.acq_latency_time": "0",
    "_Misc.acq_mode": "Single",
    "_Misc.acq_nb_frames": "40",
    "_Misc.acq_trigger_mode": "ExtGate",
    "_Misc.image_bin": "<1x1>",
    "_Misc.image_flip": "<flip x : False,flip y : False>",
    "_Misc.image_roi": "<0,0>-<2463x2527>",
    "_Misc.image_rotation": "Rotation_0",
    "_array_data.header_contents": (
        "# Detector: PILATUS3 6M, S/N 60-0128, ESRF ID30\r\n"
        "# 2024/Nov/07 10:17:09\r\n"
        "\r\n"
        "# Silicon sensor, thickness 0.001000 m\r\n"
        "\r\n"
        "# Pixel_size 172e-6 m x 172e-6 m\r\n"
        "# file_comments \r\n"
        "# N_oscillations 40\r\n"
        "# Oscillation_axis omega\r\n"
        "# Chi 0.0000 deg.\r\n"
        "# Phi 0.0000 deg.\r\n"
        "# Kappa 0.0000 deg.\r\n"
        "# Alpha 0.0000 deg.\r\n"
        "# Polarization 0.99\r\n"
        "# Detector_2theta 0.0000 deg.\r\n"
        "# Angle_increment 0.1000 deg.\r\n"
        "# Transmission 100.0\r\n"
        "# Flux 683049870000.0\r\n"
        "# Beam_xy (1198.07, 1305.20) pixels\r\n"
        "# Detector_Voffset 0.0000 m\r\n"
        "# Energy_range (0, 0) eV\r\n"
        "# Detector_distance 0.388637 m\r\n"
        "# Wavelength 0.965459 A\r\n"
        "# Trim_directory: (nil)\r\n"
        "# Flat_field: (nil)\r\n"
        "# Excluded_pixels:  badpix_mask.tif\r\n"
        "# N_excluded_pixels: = 321\r\n"
        "# Threshold_setting 6421 eV\r\n"
        "# Count_cutoff 1048500\r\n"
        "# Tau = 0 s\r\n"
        "# Exposure_period 0.015950 s\r\n"
        "# Exposure_time 0.015000 s\r\n"
        "# Start_angle "
    ),
}
