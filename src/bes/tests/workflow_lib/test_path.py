import os
import pytest
from bes.workflow_lib import path as workflow_lib_path

try:
    from XSDataCommon import XSDataString
except ImportError:
    XSDataString = None


def test_json_file(tmp_path):
    data = {"a": 1}
    filename = workflow_lib_path.createNewJsonFile(str(tmp_path), data)
    basename = os.path.basename(filename)
    assert (tmp_path / basename).is_file()
    assert basename.startswith("data_")
    assert basename.endswith(".json")

    data2 = workflow_lib_path.parseJsonFile(filename)
    assert data == data2


def test_xsdata_file(tmp_path):
    data = "xsdata_content"
    filename = workflow_lib_path.createNewXSDataFile(str(tmp_path), data)
    basename = os.path.basename(filename)
    assert (tmp_path / basename).is_file()
    assert basename.startswith("xsData_")
    assert basename.endswith(".xml")

    data2 = workflow_lib_path.readXSDataFile(filename)
    assert data == data2


@pytest.mark.skipif(XSDataString is None, reason="required edna-mx to be installed")
def test_xsdataobj_file(tmp_path):
    data = "xsdata_value"
    filename = workflow_lib_path.createNewXSDataFile(str(tmp_path), XSDataString(data))
    basename = os.path.basename(filename)
    assert (tmp_path / basename).is_file()
    assert basename.startswith("xsData_")
    assert basename.endswith(".xml")

    xml_repr = workflow_lib_path.readXSDataFile(filename)
    data2 = XSDataString.parseString(xml_repr).getValue()
    assert data == data2


def test_stack_file(tmp_path):
    filename = workflow_lib_path.createStackTraceFile(str(tmp_path))
    basename = os.path.basename(filename)
    assert (tmp_path / os.path.basename(filename)).is_file()
    assert basename.startswith("stackTrace_")
