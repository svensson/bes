import sys
import json
import logging
from pathlib import Path
from typing import Generator
from contextlib import contextmanager

from edna2.tasks.AbstractTask import AbstractTask

from bes.bin import run_ppf_json


def test_edna2_logs(tmp_path: Path, capfd, bes_parameters):
    filename = create_workflow(tmp_path)
    log_file_path = tmp_path / (filename.stem + ".log")

    with reset_logging_handlers():
        inData = {"workingDirectory": str(tmp_path), "besParameters": bes_parameters}
        result = run_ppf_json.execute_workflow(
            str(filename),
            inData,
            dict(),
            log_directory=tmp_path,
            log_level=logging.DEBUG,
        )
        assert result["_ppfdict"]["isFailure"]

    expected = {
        "THIS IS AN ERROR LOG",
        "THIS IS A WARNING LOG",
        "THIS IS AN INFO LOG",
        "THIS IS A DEBUG LOG",
        "THIS IS A PRINT OUT",
        "THIS IS A PRINT ERR",
        "THIS IS AN EXCEPTION",
    }

    # Check stdout/stderr logs
    captured = capfd.readouterr()
    output = f"{captured.out}\n{captured.err}"
    for msg in expected:
        assert msg in output, msg

    # Check file logs
    with open(log_file_path, "r") as fh:
        output = {s.rstrip() for s in fh.readlines()}
    for msg in expected:
        if "PRINT" in msg:
            assert all(msg not in line for line in output), msg
        else:
            assert any(msg in line for line in output), msg


class Edna2Printer(AbstractTask):
    """EDNA2 task that logs/prints in several ways"""

    def run(self, inData):
        logging.error("THIS IS AN ERROR LOG")
        logging.warning("THIS IS A WARNING LOG")
        logging.info("THIS IS AN INFO LOG")
        logging.debug("THIS IS A DEBUG LOG")
        print("THIS IS A PRINT OUT")
        print("THIS IS A PRINT ERR", file=sys.stderr)
        raise RuntimeError("THIS IS AN EXCEPTION")


@contextmanager
def reset_logging_handlers() -> Generator[Path, None, None]:
    level = logging.root.level
    handlers = logging.root.handlers
    logging.root.handlers = []
    try:
        yield
    finally:
        logging.root.setLevel(level)
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
            handler.close()
        logging.root.handlers = handlers


def create_workflow(tmp_path: Path) -> Path:
    workflow = {
        "graph": {"id": "test_graph"},
        "nodes": [
            {
                "id": "1",
                "task_identifier": __name__ + ".run",
                "task_type": "ppfmethod",
            }
        ],
    }

    filename = tmp_path / "test_graph.json"
    with open(filename, "w") as f:
        json.dump(workflow, f, indent=2)

    return filename


def run(**inData):
    """Ewoks task type 'ppfmethod'"""
    diffractionThumbnail = Edna2Printer(inData=inData)
    diffractionThumbnail.execute()
    return {"isFailure": diffractionThumbnail.isFailure()}
