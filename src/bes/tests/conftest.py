import os
import socket
import threading
import contextlib
from contextlib import contextmanager

import pytest
from bson.objectid import ObjectId
from xmlrpc.client import ServerProxy

from .utils import xmlrpc_server


def find_free_port():
    with contextlib.closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(("", 0))
        return s.getsockname()[1]


@pytest.fixture()
def bes_parameters() -> dict:
    return {"request_id": str(ObjectId())}


@pytest.fixture()
def xmlrpc(bes_parameters):
    host = "127.0.0.1"
    port = find_free_port()

    start_event = threading.Event()
    server_thread = threading.Thread(
        target=xmlrpc_server.start_server,
        args=(host, port),
        kwargs={"start_event": start_event},
    )
    server_thread.start()
    start_event.wait()

    inData = {
        "callBackHost": host,
        "callBackPort": port,
        "besParameters": bes_parameters,
    }
    with ServerProxy(f"http://{host}:{port}") as proxy:
        yield inData, proxy

        proxy.shutdown()
        server_thread.join(timeout=10)
        assert not server_thread.is_alive(), "XML-RPC server did not shut down"


@contextmanager
def edna_site(beamline: str):
    site = f"ESRF_{beamline}".upper()
    os.environ["EDNA_SITE"] = site
    os.environ["EDNA2_SITE"] = site
    try:
        yield
    finally:
        del os.environ["EDNA_SITE"]
        del os.environ["EDNA2_SITE"]


@pytest.fixture
def edna_id30a1():
    with edna_site("id30a1"):
        yield


@pytest.fixture
def edna_id30a2():
    with edna_site("id30a2"):
        yield


@pytest.fixture
def edna_id30a3():
    with edna_site("id30a3"):
        yield


@pytest.fixture
def edna_id30b():
    with edna_site("id30b"):
        yield


@pytest.fixture
def edna_id23eh1():
    with edna_site("id23eh1"):
        yield


@pytest.fixture
def edna_id23eh2():
    with edna_site("id23eh2"):
        yield


@pytest.fixture
def edna_bm07():
    with edna_site("bm07"):
        yield


@pytest.fixture
def workflow_working_dir(tmp_path):
    return tmp_path / "Workflow"
