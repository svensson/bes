from bes.bin import run_ppf_json
from ewokscore.ppftasks import PPF_DICT_ARGUMENT


def test_xmlrpc(xmlrpc):
    _, proxy = xmlrpc
    assert proxy.callBack() == 1
    assert proxy.callBackCount() == 1


def test_run_ppf_json(xmlrpc):
    inData, proxy = xmlrpc
    assert proxy.callBackCount() == 0
    result = run_ppf_json.execute_workflow("BesTestExecution", inData, dict())
    assert proxy.callBackCount() == 1
    result = result[PPF_DICT_ARGUMENT]
    assert result["testStatus"] == "ok"
