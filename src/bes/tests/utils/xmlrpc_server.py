import os
import sys
import time
import tempfile
import threading
import argparse
import logging
from uuid import uuid4
from typing import Optional
from xmlrpc.server import SimpleXMLRPCServer


_logger = logging.getLogger(__name__)


class PayloadXMLRPCServer(SimpleXMLRPCServer):
    def __init__(
        self,
        host: str,
        port: int,
        output_directory: Optional[str] = None,
        **_,
    ):
        super().__init__((host, port), **_)
        self.output_filenames = []
        self.output_directory = output_directory
        self.payloads = 0

    def callBack(self, payload="undefined"):
        _logger.debug("PayloadXMLRPCServer.callBack: %s", payload)
        self.payloads += 1
        if isinstance(payload, dict) and "besParameters" in payload:
            bes_request_id = payload["besParameters"]["request_id"]
        else:
            bes_request_id = str(uuid4())
        if self.output_directory:
            output_filename = os.path.join(
                self.output_directory, f"{bes_request_id}.dat"
            )
            with open(output_filename, "w") as f:
                f.write(payload)
            self.output_filenames.append(output_filename)
        return self.payloads

    def callBackCount(self):
        _logger.debug("PayloadXMLRPCServer.callBackCount")
        return self.payloads

    def resetCallBack(self):
        _logger.debug("PayloadXMLRPCServer.resetCallBack")
        for output_filename in self.output_filenames:
            os.unlink(output_filename)
        self.output_filenames.clear()
        self.payloads = 0
        return 0

    def shutdown(self):
        threading.Thread(target=self._shutdown).start()
        return "Server is shutting down..."

    def _shutdown(self):
        self.server_close()
        time.sleep(0.5)
        super().shutdown()


def start_server(
    host: str,
    port: int,
    output_directory: Optional[str] = None,
    start_event: Optional[threading.Event] = None,
) -> None:
    server = PayloadXMLRPCServer(host, port, output_directory, allow_none=True)
    server.register_function(server.callBack)
    server.register_function(server.callBackCount)
    server.register_function(server.resetCallBack)
    server.register_function(server.shutdown)
    _logger.info(f"XML-RPC Server running at http://{host}:{port}")
    if start_event:
        start_event.set()
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        _logger.info("Shutting down XML-RPC server...")
    finally:
        server.server_close()


def main_cli(argv=None) -> int:
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(
        "xmlrpc_server",
        description="XML RPC server that saves results in files for each request ID",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("--host", default="127.0.0.1", type=str, help="XML_RPC host")
    parser.add_argument("--port", default=8080, type=int, help="XML_RPC port")
    parser.add_argument(
        "--output-directory",
        default=tempfile.gettempdir(),
        type=str,
        help="Workflow input parameters",
    )
    parser.add_argument(
        "--log-level",
        default="DEBUG",
        type=str.upper,
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        help="Set the logging level",
    )
    args = parser.parse_args(args=argv[1:])
    logging.basicConfig(level=getattr(logging, args.log_level))

    start_server(args.host, args.port, output_directory=args.output_directory)

    return 0


if __name__ == "__main__":
    sys.exit(main_cli())
