from bes import config


def test_scalars():
    beamline = "id30a2"
    section_name = "Test"

    value = config.get_value(beamline, section_name, "float")
    assert isinstance(value, float)

    value = config.get_value(beamline, section_name, "int")
    assert isinstance(value, int)

    value = config.get_value(beamline, section_name, "booleanTrue")
    assert value is True

    value = config.get_value(beamline, section_name, "empty")
    assert value == ""


def test_lists():
    beamline = "id30a2"
    section_name = "Test"

    value = config.get_value(beamline, section_name, "error")
    assert isinstance(value, str)

    value = config.get_value(beamline, section_name, "success")
    assert isinstance(value, list)
    assert all(isinstance(value, str) for value in value)

    value = config.get_value(beamline, section_name, "absent", default_value=[])
    assert value == []

    value = config.get_value(beamline, section_name, "floatlist")
    assert isinstance(value, list)
    assert all(isinstance(value, float) for value in value)

    value = config.get_value(beamline, section_name, "intlist")
    assert isinstance(value, list)
    assert all(isinstance(value, int) for value in value)

    value = config.get_value(beamline, section_name, "intlist2")
    assert isinstance(value, list)
    assert all(isinstance(value, int) for value in value)


def test_default():
    beamline = "id30a2"

    value = config.get_value(beamline, "NoSection", "noKey", default_value="testValue")
    assert value == "testValue"

    value = config.get_value(beamline, "Mesh", "noKey", default_value="testValue")
    assert value == "testValue"


def test_dict():
    beamline = "id30a2"

    dict_aperture = config.get_value(beamline, "Beam", "dictApertureExpTime")
    assert dict_aperture["10"] == 1.8
    assert dict_aperture["100"] == 0.075
