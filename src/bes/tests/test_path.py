import os
import stat
import time
import shutil
import tempfile

from bes.workflow_lib import path


def test_createWorkflowWorkingDirectoryName():

    # Test creation of workflow working directory
    directory = "/data/id23eh1/inhouse/opid231/20130328/RAW_DATA/WF/burn"
    strWorkingDir = path._createWorkflowWorkingDirectoryName(directory)
    assert strWorkingDir.startswith(
        "/data/id23eh1/inhouse/opid231/20130328/PROCESSED_DATA/WF/burn/Workflow_"
    )


def test_createWorkflowWorkingDirectory(tmpdir):
    raw_dir = os.path.join(tmpdir, "RAW_DATA")
    working_dir = path.createWorkflowWorkingDirectory(raw_dir)
    assert os.access(working_dir, os.R_OK or os.W_OK or os.X_OK)
    assert working_dir.startswith(
        os.path.join(raw_dir.replace("RAW_DATA", "PROCESSED_DATA"), "Workflow_")
    )
    # Then test path to workflow_working_dir which does not exist
    workflow_working_dir = "/path/to/directory/which/does/not/exist"
    working_dir = path.createWorkflowWorkingDirectory(workflow_working_dir)
    assert os.access(working_dir, os.R_OK or os.W_OK or os.X_OK)
    shutil.rmtree(working_dir)

    # Then test path to workflow_working_dir which exists but is not writeable
    workflow_working_dir = "/"
    working_dir = path.createWorkflowWorkingDirectory(workflow_working_dir)
    assert os.access(working_dir, os.R_OK or os.W_OK or os.X_OK)
    shutil.rmtree(working_dir)


def test_createStackTraceFile(tmpdir):
    stack_trace_file = path.createStackTraceFile(tmpdir)
    assert stack_trace_file is not None
    assert os.path.exists(stack_trace_file)
    fileStat = os.stat(stack_trace_file)
    assert bool(fileStat.st_mode & stat.S_IRGRP)


def test_createWorkflowLogFilePath(tmpdir):
    workflow_log_file = path.createWorkflowLogFilePath(tmpdir)
    assert workflow_log_file is not None


def test_getDictPath():
    # /data/visitor
    assert path.getDictPath("") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data/visitor") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data/visitor/mx415") == {
        "beamline": None,
        "proposal": "mx415",
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data/visitor/mx415/id30a1") == {
        "beamline": "id30a1",
        "proposal": "mx415",
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data/visitor/mx415/id30a1/20180213") == {
        "beamline": "id30a1",
        "proposal": "mx415",
        "date": "20180213",
        "rest": None,
    }
    assert path.getDictPath("/data/visitor/mx415/id30a1/20180213/RAW_DATA") == {
        "beamline": "id30a1",
        "proposal": "mx415",
        "date": "20180213",
        "rest": "RAW_DATA",
    }
    assert path.getDictPath("/data/visitor/mx415/id30a1/20180213/RAW_DATA/1/2/3") == {
        "beamline": "id30a1",
        "proposal": "mx415",
        "date": "20180213",
        "rest": "RAW_DATA/1/2/3",
    }
    # /mntdirect/_data_visitor/
    assert path.getDictPath("") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect/_data_visitor") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect/_data_visitor/mx415") == {
        "beamline": None,
        "proposal": "mx415",
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect/_data_visitor/mx415/id30a1") == {
        "beamline": "id30a1",
        "proposal": "mx415",
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect/_data_visitor/mx415/id30a1/20180213") == {
        "beamline": "id30a1",
        "proposal": "mx415",
        "date": "20180213",
        "rest": None,
    }
    assert path.getDictPath(
        "/mntdirect/_data_visitor/mx415/id30a1/20180213/RAW_DATA"
    ) == {
        "beamline": "id30a1",
        "proposal": "mx415",
        "date": "20180213",
        "rest": "RAW_DATA",
    }
    assert path.getDictPath(
        "/mntdirect/_data_visitor/mx415/id30a1/20180213/RAW_DATA/1/2/3"
    ) == {
        "beamline": "id30a1",
        "proposal": "mx415",
        "date": "20180213",
        "rest": "RAW_DATA/1/2/3",
    }
    # /data/id30a1/inhouse
    assert path.getDictPath("") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data/id30a1") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data/id30a1/inhouse/opid30a1") == {
        "beamline": "id30a1",
        "proposal": "opid30a1",
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/data/id30a1/inhouse/opid30a1/20180213") == {
        "beamline": "id30a1",
        "proposal": "opid30a1",
        "date": "20180213",
        "rest": None,
    }
    assert path.getDictPath("/data/id30a1/inhouse/opid30a1/20180213/RAW_DATA") == {
        "beamline": "id30a1",
        "proposal": "opid30a1",
        "date": "20180213",
        "rest": "RAW_DATA",
    }
    assert path.getDictPath(
        "/data/id30a1/inhouse/opid30a1/20180213/RAW_DATA/1/2/3"
    ) == {
        "beamline": "id30a1",
        "proposal": "opid30a1",
        "date": "20180213",
        "rest": "RAW_DATA/1/2/3",
    }
    # /mntdirect/_data_id30a1_inhouse/opid30a1/20180212/RAW_DATA
    assert path.getDictPath("") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect/_data_id30a1_inhouse") == {
        "beamline": None,
        "proposal": None,
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect/_data_id30a1_inhouse/opid30a1") == {
        "beamline": "id30a1",
        "proposal": "opid30a1",
        "date": None,
        "rest": None,
    }
    assert path.getDictPath("/mntdirect/_data_id30a1_inhouse/opid30a1/20180213") == {
        "beamline": "id30a1",
        "proposal": "opid30a1",
        "date": "20180213",
        "rest": None,
    }
    assert path.getDictPath(
        "/mntdirect/_data_id30a1_inhouse/opid30a1/20180213/RAW_DATA"
    ) == {
        "beamline": "id30a1",
        "proposal": "opid30a1",
        "date": "20180213",
        "rest": "RAW_DATA",
    }
    assert path.getDictPath(
        "/mntdirect/_data_id30a1_inhouse/opid30a1/20180213/RAW_DATA/1/2/3"
    ) == {
        "beamline": "id30a1",
        "proposal": "opid30a1",
        "date": "20180213",
        "rest": "RAW_DATA/1/2/3",
    }


def test_createPyarchFilePath():
    assert path._getPyarchFilePath("/") is None
    assert path._getPyarchFilePath("/data") is None
    assert path._getPyarchFilePath("/data/visitor") is None
    assert path._getPyarchFilePath("/data/visitor/mx415/id30a1") is None
    assert (
        path._getPyarchFilePath("/data/visitor/mx415/id30a1/20100212")
        == "/data/pyarch/2010/id30a1/mx415/20100212"
    )
    assert (
        path._getPyarchFilePath("/data/visitor/mx415/id30a1/20100212/1")
        == "/data/pyarch/2010/id30a1/mx415/20100212/1"
    )
    assert (
        path._getPyarchFilePath("/data/visitor/mx415/id30a1/20100212/1/2")
        == "/data/pyarch/2010/id30a1/mx415/20100212/1/2"
    )
    # Test with inhouse account...
    assert path._getPyarchFilePath("/") is None
    assert path._getPyarchFilePath("/data") is None
    assert path._getPyarchFilePath("/data/id23eh2") is None
    assert path._getPyarchFilePath("/data/id23eh2/inhouse") is None
    assert path._getPyarchFilePath("/data/id23eh2/inhouse/opid232") is None
    assert (
        path._getPyarchFilePath("/data/id23eh2/inhouse/opid232/20100525")
        == "/data/pyarch/2010/id23eh2/opid232/20100525"
    )
    assert (
        path._getPyarchFilePath("/data/id23eh2/inhouse/opid232/20100525/1")
        == "/data/pyarch/2010/id23eh2/opid232/20100525/1"
    )
    assert (
        path._getPyarchFilePath("/data/id23eh2/inhouse/opid232/20100525/1/2")
        == "/data/pyarch/2010/id23eh2/opid232/20100525/1/2"
    )


def test_addUniqueSubDirToPath(tmpdir):
    workflow_type = "LineScan"
    directory, index = path.addUniqueSubDirToPath(tmpdir, workflow_type)
    assert workflow_type + "_01", os.path.basename(directory)
    assert index == 1
    #        os.makedirs(directory)
    directory2, index = path.addUniqueSubDirToPath(tmpdir, workflow_type)
    assert workflow_type + "_02", os.path.basename(directory2)
    assert index == 2


def test_addUniqueSubDirPrefixToPath(tmpdir):
    workflow_type = "LineScan"
    directory, index = path.addUniqueSubDirPrefixToPath(tmpdir, workflow_type)
    assert os.path.basename(directory) == "01_" + workflow_type
    assert index == 1
    directory2, index2 = path.addUniqueSubDirPrefixToPath(tmpdir, workflow_type)
    assert os.path.basename(directory2) == "02_" + workflow_type
    assert index2 == 2


def test_copyHtmlDirectoryToPyarch(tmpdir):
    # Create dummy directory with files
    tmpdir = tempfile.mkdtemp(prefix="TestPathHtml_")
    dummy_html = os.path.join(tmpdir, "index.html")
    f = open(dummy_html, "w")
    f.write("Dummy test data...")
    f.close()
    f = open(os.path.join(tmpdir, "file1.txt"), "w")
    f.write("Dummy test data...")
    f.close()
    f = open(os.path.join(tmpdir, "grid.png"), "w")
    f.write("Dummy test data...")
    f.close()
    pyarch_html_dir = tempfile.mkdtemp(prefix="TestPathHtmlPyarch_")
    dict_html = {
        "html_directory_path": tmpdir,
        "index_file_name": "index.html",
        "plot_file_name": "grid.png",
    }
    pyarch_dict_html = path.copyHtmlDirectoryToPyarch(pyarch_html_dir, dict_html)
    assert os.path.exists(
        os.path.join(
            pyarch_dict_html["html_directory_path"],
            pyarch_dict_html["index_file_name"],
        )
    )
    assert os.path.exists(
        os.path.join(
            pyarch_dict_html["html_directory_path"],
            pyarch_dict_html["plot_file_name"],
        )
    )
    assert os.path.exists(os.path.join(pyarch_html_dir, "html_01", "file1.txt"))
    # Test what happens if the same directory is copied again...
    pyarch_dict_html = path.copyHtmlDirectoryToPyarch(pyarch_html_dir, dict_html)
    shutil.rmtree(pyarch_html_dir)


def test_extractBeamlineFromDirectory():
    # Test inhouse directory
    directory = "/data/id23eh1/inhouse/opid231/20130521/RAW_DATA/Mesh2D_05"
    assert path.extractBeamlineFromDirectory(directory) == "id23eh1"
    # Test visitor directory
    directory = "/data/visitor/mx1469/id14eh4/20130428/PROCESSED_DATA"
    assert path.extractBeamlineFromDirectory(directory) == "id14eh4"
    # Test bm14 inhouse directory
    directory = "/data/bm14/inhouse/opd14/20131031/PROCESSED_DATA"
    assert path.extractBeamlineFromDirectory(directory) == "bm14"
    # Test bm14 vistor directory
    directory = "/data/visitor/bm141084/bm14/20130925/PROCESSED_DATA"
    assert path.extractBeamlineFromDirectory(directory) == "bm14"


def test_findUniqueRunNumber(tmpdir):
    run_number = path.findUniqueRunNumber(tmpdir, "mx415")
    assert run_number == 1
    # Create a file
    f = open(os.path.join(tmpdir, "mx415_1_0001.cbf"), "w")
    f.write("Hello!")
    f.close()
    run_number = path.findUniqueRunNumber(tmpdir, "mx415")
    assert run_number == 2
    # Create another file
    f = open(os.path.join(tmpdir, "ref-mx415_3_0001.cbf"), "w")
    f.write("Hello!")
    f.close()
    run_number = path.findUniqueRunNumber(tmpdir, "mx415", "ref-", 3)
    assert run_number == 4


def test_findSampleDirectory():
    directory = "/data/visitor/mx1582/id30a1/20141029/RAW_DATA/R341/R341-AAA129Apos02/MXPressE_01"
    sample_directory = path.findProcessedSampleDirectory(directory)
    assert (
        sample_directory == "/data/visitor/mx1582/id30a1/20141029/PROCESSED_DATA/R341"
    )
    temp_directory = "/tmp/Workflow_20141031-092751arFaRP/Workflow_20141031-092753"
    temp_sample_directory = path.findProcessedSampleDirectory(temp_directory)
    assert temp_directory == temp_sample_directory


def test_createNewJsonFile(tmpdir):
    data = {"a": "hello", "b": 1, "c": 1.0, "d": False}
    assert os.path.exists(path.createNewJsonFile(tmpdir, data))


def test_parseJsonFile():
    test_module_path = os.path.abspath(__file__)
    test_data_directory = os.path.join(os.path.dirname(test_module_path), "data")
    jsonFile = os.path.join(test_data_directory, "meshResults.json")
    data = path.parseJsonFile(jsonFile)
    assert type(data) is list


def test_createNewXSDataFile(tmpdir):
    from XSDataCommon import XSDataString

    xs_data_string = XSDataString("Hello World!")
    xml = xs_data_string.marshal()
    assert os.path.exists(path.createNewXSDataFile(tmpdir, xml))
    assert os.path.exists(path.createNewXSDataFile(tmpdir, xs_data_string))


def test_readXSDataFile():
    from XSDataCommon import XSDataString

    xs_data_string = XSDataString("Hello World!")
    testModulePath = os.path.abspath(__file__)
    testDataDirectory = os.path.join(os.path.dirname(testModulePath), "data")
    xml_file = os.path.join(testDataDirectory, "xsDataString.xml")
    xml_string = path.readXSDataFile(xml_file)
    assert xs_data_string.marshal() == xml_string


def test_getImagePathFromQueueEntry():
    queueEntry = {
        "fileData": {
            "expTypePrefix": "line-",
            "runNumber": 2,
            "prefix": "hSRP-xt9_D6_16dec201",
            "directory": "/data/visitor/mx1581/id30a1/20150122/RAW_DATA/hSRP/hSRP-xt9_D6_16dec2014/MXPressE_01",
            "firstImage": 1,
            "process_directory": "/data/visitor/mx1581/id30a1/20150122/PROCESSED_DATA/hSRP/hSRP-xt9_D6_16dec2014/MXPressE_01",
        },
        "subWedgeData": {
            "exposureTime": 0.1,
            "transmission": 100.0,
            "rotationAxisStart": 270.1204379562044,
            "noImages": 40,
            "doProcessing": False,
            "oscillationWidth": 0.025,
        },
        "motorPositions4dscan": {
            "sampx_s": -1.0049046321525885,
            "phiy_s": -0.163,
            "phiz_s": -0.199,
            "sampy_s": 0.5809264305177112,
            "phiy_e": -0.163,
            "sampx_e": -1.0049046321525885,
            "sampy_e": 0.5809264305177112,
            "phiz_e": 0.201,
        },
    }
    suffix = "cbf"
    firstImagePath = path.getImagePathFromQueueEntry(queueEntry, suffix)
    assert (
        firstImagePath
        == "/data/visitor/mx1581/id30a1/20150122/RAW_DATA/hSRP/hSRP-xt9_D6_16dec2014/MXPressE_01/line-hSRP-xt9_D6_16dec201_2_0001.cbf"
    )


def test_getQueueEntryImagePathList():
    queueEntry = {
        "fileData": {
            "expTypePrefix": "line-",
            "runNumber": 2,
            "prefix": "hSRP-xt9_D6_16dec201",
            "directory": "/data/visitor/mx1581/id30a1/20150122/RAW_DATA/hSRP/hSRP-xt9_D6_16dec2014/MXPressE_01",
            "firstImage": 1,
            "process_directory": "/data/visitor/mx1581/id30a1/20150122/PROCESSED_DATA/hSRP/hSRP-xt9_D6_16dec2014/MXPressE_01",
        },
        "subWedgeData": {
            "exposureTime": 0.1,
            "transmission": 100.0,
            "rotationAxisStart": 270.1204379562044,
            "noImages": 40,
            "doProcessing": False,
            "oscillationWidth": 0.025,
        },
        "motorPositions4dscan": {
            "sampx_s": -1.0049046321525885,
            "phiy_s": -0.163,
            "phiz_s": -0.199,
            "sampy_s": 0.5809264305177112,
            "phiy_e": -0.163,
            "sampx_e": -1.0049046321525885,
            "sampy_e": 0.5809264305177112,
            "phiz_e": 0.201,
        },
    }
    suffix = "cbf"
    list_image_path = path.getQueueEntryImagePathList(queueEntry, suffix)
    assert len(list_image_path) == 40


def test_getProposalNameNumber():
    pname, pno = path.getProposalNameNumber(
        "/data/visitor/mx1581/id30a1/20150209/RAW_DATA/pol3/pol3-S04/MXPressE_01"
    )
    assert pname == "mx"
    assert pno == "1581"
    pname, pno = path.getProposalNameNumber(
        "/data/id29/inhouse/opid291/20150209/RAW_DATA/ESRF/opid291/manually-mounted"
    )
    assert pname == "opid"
    assert pno == "291"
    pname, pno = path.getProposalNameNumber(
        "/data/id30a1/inhouse/opid30a1/20151027/PROCESSED_DATA/MXPressE_01"
    )
    assert pname == "opid"
    assert pno == "30a1"
    pname, pno = path.getProposalNameNumber(
        "/data/id30b/inhouse/mxihr2/20170301/RAW_DATA/nicolas/dna/anneal1"
    )
    assert pname == "mxihr"
    assert pno == "2"
    pname, pno = path.getProposalNameNumber(
        "/data/visitor/ihmx557/id30a1/20250220/RAW_DATA/Sample-8-3-01"
    )
    assert pname == "ihmx"
    assert pno == "557"
    # Test with non-existing proposal starting with e.g. "ih"
    pname, pno = path.getProposalNameNumber(
        "/data/visitor/ihqq557/id30a1/20250220/RAW_DATA/Sample-8-3-01"
    )
    assert pname is None
    assert pno is None


def test_getImageNumber():
    assert path.getImageNumber("ref-testscale_1_001.img") == 1
    assert path.getImageNumber("ref-testscale_1_1234.h5") == 1234
    assert path.getImageNumber("ref-testscale_1_12345.h5") == 12345
    assert (
        path.getImageNumber(
            "/data/id30a3/inhouse/opid30a3/20160204/RAW_DATA/meshtest/XrayCentering_01/mesh-meshtest_1_0001.cbf"
        )
        == 1
    )


def test_getPrefix():
    assert path.getPrefix("ref-testscale_1_001.img") == "ref-testscale_1"
    assert (
        path.getPrefix(
            "/data/id30a3/inhouse/opid30a3/20160204/RAW_DATA/meshtest/XrayCentering_01/mesh-meshtest_1_0001.cbf"
        )
        == "mesh-meshtest_1"
    )


def test_getH5FilePath1():
    file_path1 = "/data/id30a3/inhouse/opid30a3/20160204/RAW_DATA/meshtest/XrayCentering_01/mesh-meshtest_1_0001.cbf"
    h5_master_file_path1, h5_data_file_path1, h5_file_number = path.getH5FilePath(
        file_path1, 9
    )
    h5_master_file_path1_reference = "/data/id30a3/inhouse/opid30a3/20160204/RAW_DATA/meshtest/XrayCentering_01/mesh-meshtest_1_1_master.h5"
    h5_data_file_path1_reference = "/data/id30a3/inhouse/opid30a3/20160204/RAW_DATA/meshtest/XrayCentering_01/mesh-meshtest_1_1_data_000001.h5"
    assert h5_master_file_path1 == h5_master_file_path1_reference
    assert h5_data_file_path1 == h5_data_file_path1_reference
    assert h5_file_number == 1


def test_getH5FilePath2():
    # fast mesh
    file_path2 = "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/mesh2/MeshScan_02/mesh-opid30a3_2_0021.cbf"
    h5_master_file_path2, h5_data_file_path2, h5_file_number2 = path.getH5FilePath(
        file_path2, batchSize=20, isFastMesh=True
    )
    h5_master_file_path2_reference = "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/mesh2/MeshScan_02/mesh-opid30a3_2_1_master.h5"
    assert h5_master_file_path2 == h5_master_file_path2_reference
    h5_data_file_path2_reference = "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/mesh2/MeshScan_02/mesh-opid30a3_2_1_data_000001.h5"
    assert h5_data_file_path2 == h5_data_file_path2_reference


def test_getH5FilePath3():
    # fast mesh 2
    file_path3 = "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/mesh2/MeshScan_02/mesh-opid30a3_2_0321.cbf"
    h5_master_file_path3, h5_data_file_path3, h5_file_number3 = path.getH5FilePath(
        file_path3, batchSize=20, isFastMesh=True
    )
    h5_master_file_path3Reference = "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/mesh2/MeshScan_02/mesh-opid30a3_2_1_master.h5"
    assert h5_master_file_path3 == h5_master_file_path3Reference, "master path2"
    h5_data_file_path3Reference = "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/mesh2/MeshScan_02/mesh-opid30a3_2_1_data_000004.h5"
    assert h5_data_file_path3 == h5_data_file_path3Reference, "data path2"


def test_getH5FilePath4():
    file_path4 = (
        "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/ref-UPF2-UPF2__4_1_0002.h5"
    )
    h5_master_file_path4, h5_data_file_path4, h5_file_number4 = path.getH5FilePath(
        file_path4, isFastMesh=False
    )
    assert (
        h5_master_file_path4
        == "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/ref-UPF2-UPF2__4_1_2_master.h5"
    )
    assert (
        h5_data_file_path4
        == "/data/id30a3/inhouse/opid30a3/20171017/RAW_DATA/ref-UPF2-UPF2__4_1_2_data_000001.h5"
    )
    assert h5_file_number4 == 2


def test_getH5FilePath5():
    file_path5 = "/data/id23eh1/inhouse/opid231/20210315/RAW_DATA/tryps/tryps-x/ref-x-tryps_1_0002.h5"
    h5_master_file_path5, h5_data_file_path5, h5_file_number5 = path.getH5FilePath(
        file_path5, isFastMesh=False
    )
    assert (
        h5_master_file_path5
        == "/data/id23eh1/inhouse/opid231/20210315/RAW_DATA/tryps/tryps-x/ref-x-tryps_1_2_master.h5"
    )
    assert (
        h5_data_file_path5
        == "/data/id23eh1/inhouse/opid231/20210315/RAW_DATA/tryps/tryps-x/ref-x-tryps_1_2_data_000001.h5"
    )
    assert h5_file_number5 == 2


def test_getH5FilePath6():
    file_path6 = "/data/id23eh1/inhouse/opid231/20210315/RAW_DATA/tryps/tryps-x/ref-x-tryps_1_12345.h5"
    h5_master_file_path6, h5_data_file_path6, h5_file_number6 = path.getH5FilePath(
        file_path6, isFastMesh=True
    )
    assert (
        h5_master_file_path6
        == "/data/id23eh1/inhouse/opid231/20210315/RAW_DATA/tryps/tryps-x/ref-x-tryps_1_1_master.h5"
    )
    assert (
        h5_data_file_path6
        == "/data/id23eh1/inhouse/opid231/20210315/RAW_DATA/tryps/tryps-x/ref-x-tryps_1_1_data_000124.h5"
    )
    assert h5_file_number6 == 1


def test_getH5FilePath7():
    file_path7 = "/data/id30b/inhouse/opid30b/20220915/RAW_DATA/Sample-8:1:16/MXPressE_01/mesh-opid30b_0_000300.h5"
    h5_master_file_path7, h5_data_file_path7, h5_file_number7 = path.getH5FilePath(
        file_path7, isFastMesh=True
    )
    assert (
        h5_master_file_path7
        == "/data/id30b/inhouse/opid30b/20220915/RAW_DATA/Sample-8:1:16/MXPressE_01/mesh-opid30b_0_1_master.h5"
    )
    assert (
        h5_data_file_path7
        == "/data/id30b/inhouse/opid30b/20220915/RAW_DATA/Sample-8:1:16/MXPressE_01/mesh-opid30b_0_1_data_000003.h5"
    )
    assert h5_file_number7 == 1


def test_getListH5FilePath():
    directory = "/data/id30a3/inhouse/opid30a3/20180328/RAW_DATA/XrayCentering_01"
    list_file_path = []
    for index in range(1, 301):
        file_path = os.path.join(directory, "mesh-opid30a3_1_{0:04d}.h5".format(index))
        list_file_path.append(file_path)
    list_h5_file_path = path.getListH5FilePath(list_file_path, isFastMesh=True)
    ref_list_h5_file_path = [
        "/data/id30a3/inhouse/opid30a3/20180328/RAW_DATA/XrayCentering_01/mesh-opid30a3_1_1_data_000001.h5",
        "/data/id30a3/inhouse/opid30a3/20180328/RAW_DATA/XrayCentering_01/mesh-opid30a3_1_1_data_000002.h5",
        "/data/id30a3/inhouse/opid30a3/20180328/RAW_DATA/XrayCentering_01/mesh-opid30a3_1_1_data_000003.h5",
        "/data/id30a3/inhouse/opid30a3/20180328/RAW_DATA/XrayCentering_01/mesh-opid30a3_1_1_master.h5",
    ]
    assert ref_list_h5_file_path == list_h5_file_path


def test_eigerTemplateToImage():
    # Characterisation
    file_template = "ref-TcUGPD-TcUGP_UG_02_1_%04d.h5"
    image_number = 1
    overlap = -89.0
    eigerTemplate = path.eigerTemplateToImage(file_template, image_number, overlap)
    assert eigerTemplate == "ref-TcUGPD-TcUGP_UG_02_1_1_data_000001.h5"
    image_number = 4
    eigerTemplate = path.eigerTemplateToImage(file_template, image_number, overlap)
    assert eigerTemplate == "ref-TcUGPD-TcUGP_UG_02_1_4_data_000001.h5"
    # Normal data collection
    file_template = "164A-6_w1_1_%04d.h5"
    image_number = 1
    overlap = 0.0
    eigerTemplate = path.eigerTemplateToImage(file_template, image_number, overlap)
    assert eigerTemplate == "164A-6_w1_1_1_data_000001.h5"
    image_number = 3600
    eigerTemplate = path.eigerTemplateToImage(file_template, image_number, overlap)
    assert eigerTemplate == "164A-6_w1_1_1_data_000036.h5"


def test_checkNoImageFlag(tmpdir):
    # Create dummy directory structure
    workflow_working_dir = os.path.join(
        tmpdir, "PROCESSED_DATA", "Acronym", "Sample", "Bla", "bla"
    )
    os.makedirs(workflow_working_dir)
    # Create time stamp
    assert not path.checkNoImageFlag(workflow_working_dir)
    time.sleep(1)
    # Check second time
    assert path.checkNoImageFlag(workflow_working_dir)
    # Check with shorter timeout
    assert not path.checkNoImageFlag(workflow_working_dir, timeOut=1)


def test_checkSameSnapShotImageFlag(tmpdir):
    # Create dummy directory structure
    workflow_working_dir = os.path.join(
        tmpdir, "PROCESSED_DATA", "Acronym", "Sample", "Bla", "bla"
    )
    os.makedirs(workflow_working_dir)
    # Create time stamp
    assert not path.checkSameSnapShotImageFlag(workflow_working_dir)
    time.sleep(1)
    # Check second time
    assert path.checkSameSnapShotImageFlag(workflow_working_dir)
    # Check with shorter timeout
    assert not path.checkSameSnapShotImageFlag(workflow_working_dir, timeOut=1)


def test_checkSameSnapShotImageCountFlag(tmpdir):
    # Create dummy directory structure
    workflow_working_dir = os.path.join(
        tmpdir, "PROCESSED_DATA", "Acronym", "Sample", "Bla", "bla"
    )
    os.makedirs(workflow_working_dir)
    # Create time stamp
    count = path.checkSameSnapShotImageCountFlag(workflow_working_dir)
    assert count == 1
    time.sleep(1)
    # Increase the time stamp
    count = path.checkSameSnapShotImageCountFlag(workflow_working_dir)
    assert count == 2
    time.sleep(1)
    # Increase the time stamp
    count = path.checkSameSnapShotImageCountFlag(workflow_working_dir)
    assert count == 3
    # Test time stamp with timeout
    time.sleep(2)
    count = path.checkSameSnapShotImageCountFlag(workflow_working_dir, timeOut=1)
    assert count == 1
    # Increase the time stamp
    time.sleep(1)
    count = path.checkSameSnapShotImageCountFlag(workflow_working_dir)
    assert count == 2


def test_systemCopyFile(tmpdir):
    test_text = "Test text"
    file1 = tempfile.NamedTemporaryFile(dir=tmpdir, prefix="file1_")
    file_name1 = file1.name
    file1.close()
    with open(file_name1, "w") as f1:
        f1.write(test_text)
    file2 = tempfile.NamedTemporaryFile(dir=tmpdir, prefix="file2_")
    file2.close()
    file_name2 = file2.name
    path.systemCopyFile(file_name1, file_name2)
    with open(file_name2) as f:
        file2_content = f.read()
    assert test_text == file2_content


def test_systemRmTree_without_errors(tmpdir):
    test_text = "Test text"
    dir1 = tempfile.mkdtemp(dir=tmpdir, prefix="dir1_")
    test_file_name = "test_file.txt"
    test_file_path1 = os.path.join(dir1, test_file_name)
    with open(test_file_path1, "w") as f1:
        f1.write(test_text)
    path.systemRmTree(dir1, ignore_errors=False)


def test_systemRmTree_with_errors(tmpdir):
    # try with ignore_errors=False
    dir1 = "/a/path/which/does/not/exists"
    try:
        path.systemRmTree(dir1, ignore_errors=False)
        exception_caught = False
    except Exception:
        exception_caught = True
    assert exception_caught
    # try with ignore_errors=True
    try:
        path.systemRmTree(dir1, ignore_errors=True)
        exception_caught = False
    except Exception:
        exception_caught = True
    assert not exception_caught


def test_systemCopyTree(tmpdir):
    test_text = "Test text"
    dir1 = tempfile.mkdtemp(dir=tmpdir, prefix="dir1_")
    test_file_name = "test_file.txt"
    test_file_path1 = os.path.join(dir1, test_file_name)
    with open(test_file_path1, "w") as f1:
        f1.write(test_text)
    dir2 = tempfile.mkdtemp(dir=tmpdir, prefix="dir2_")
    path.systemCopyTree(dir1, dir2, dirs_exists_ok=True)
    test_file_path2 = os.path.join(dir2, test_file_name)
    with open(test_file_path2) as f:
        file2_content = f.read()
    assert test_text == file2_content


def test_getSampleNameFromPath():
    path1 = "/data/visitor/fx29/id30a1/20230928/RAW_DATA/XEKK1/XEKK1-ucb26-27/run_01_MXPressA/run_01_02_mesh"
    sample_name = path.getSampleNameFromPath(path1)
    assert sample_name == "XEKK1-ucb26-27"
    path2 = "/data/visitor/fx29/id30a1/20230928/RAW_DATA/XEKK1/XEKK1-ucb26-27/run_01_MXPressA"
    sample_name2 = path.getSampleNameFromPath(path2)
    assert sample_name2 == "XEKK1-ucb26-27"
