import logging

import flask
import flask_cors

from .routes import main
from .job_manager import JobManager


def create_app() -> flask.Flask:
    app = flask.Flask(__name__)
    flask_cors.CORS(app, support_credentials=True)
    app.config.from_object("bes.app.config.Config")
    app.register_blueprint(main)
    app.job_manager = JobManager(app.logger)

    #
    # werkzeug WSGI
    # =============
    #
    #  Loggers
    #
    #    * `bes.app`: flask application logger (default: `NOTSET`, propagating)
    #    * `werkzeug`: REST call INFO logs (default: `INFO`, propagating)
    #
    #  CLI arguments (flask --app bes.bin.ppf_server run ...)
    #
    #    * `--debug`: sets the level of the flask application logger to `DEBUG`
    #
    # gunicorn WSGI
    # =============
    #
    #  Loggers
    #
    #    * `bes.app`: flask application logger (default: `NOTSET`, propagating)
    #    * `gunicorn.access`: REST call DEBUG logs (default: `INFO`, not propagating)
    #    * `gunicorn.error`: ??? (default level: `INFO`, not propagating, stderr handler)
    #
    #  CLI arguments (gunicorn bes.bin.ppf_server:app ...)
    #
    #    * `--log-level DEBUG`: sets the level of `gunicorn.error` to `DEBUG`
    #

    gunicorn_errors = logging.getLogger("gunicorn.error")
    if gunicorn_errors.level:
        gunicorn_access = logging.getLogger("gunicorn.access")
        gunicorn_access.setLevel(gunicorn_errors.level)
        app.logger.setLevel(gunicorn_errors.level)
        app.logger.handlers = gunicorn_errors.handlers
        app.logger.propagate = False

    return app
