import atexit
import threading
import subprocess


class SubprocessManager:
    """Start sub-processes and get their state in the background to avoid zombie processes."""

    def __init__(
        self, logger, clean_period=10, exit_timeout=5, terminate_on_exit=True
    ) -> None:
        self._subprocess_list = []
        self._subprocess_lock = threading.Lock()
        self._logger = logger
        self._shutdown_event = threading.Event()
        self._cleanup_thread = None
        self._clean_period = clean_period
        self._exit_timeout = exit_timeout
        self._terminate_on_exit = terminate_on_exit

    def start_subprocess(self, cmd, env) -> subprocess.Popen:
        if self._shutdown_event.is_set():
            raise RuntimeError(f"{self} was stopped")
        with self._subprocess_lock:
            # stdout/stderr produces too much logs which makes the subprocess
            # hangs when the parent process is not continuously reading the pipe.
            proc = subprocess.Popen(cmd, env=env, stdout=None, stderr=None)
            self._logger.debug("Command (pid=%s):\n %s", proc.pid, " ".join(cmd))
            self._subprocess_list.append(proc)
            return proc

    def start(self) -> None:
        if self._cleanup_thread is not None:
            return
        self._cleanup_thread = threading.Thread(
            target=self._cleanup_subprocesses, daemon=True
        )
        self._cleanup_thread.start()
        atexit.register(self.stop)

    def _cleanup_subprocesses(self) -> None:
        self._logger.debug("Sub-processes cleanup task started.")
        try:
            while not self._shutdown_event.wait(self._clean_period):
                ncleaned = 0
                with self._subprocess_lock:
                    for proc in list(self._subprocess_list):
                        retcode = proc.poll()
                        if retcode is not None:
                            self._subprocess_list.remove(proc)
                            self._log_proc_exit(proc)
                            ncleaned += 1
        finally:
            self._logger.debug("Sub-processes cleanup task exits.")

    def stop(self) -> bool:
        self._shutdown_event.set()
        self._cleanup_thread.join(self._exit_timeout)

        if self._terminate_on_exit:
            with self._subprocess_lock:
                remaining_procs = list(self._subprocess_list)
                self._subprocess_list.clear()

            for proc in remaining_procs:
                self._logger.warning(f"Terminate process {proc.pid}.")
                proc.terminate()

            for proc in remaining_procs:
                try:
                    self._log_proc_exit(proc, timeout=self._exit_timeout)
                except subprocess.TimeoutExpired:
                    self._logger.warning(
                        f"Process {proc.pid} did not terminate in time and will be forcefully killed."
                    )
                    proc.kill()
                    self._log_proc_exit(proc, timeout=self._exit_timeout)

        return self._cleanup_thread.is_alive()

    def _log_proc_exit(self, proc, timeout=None) -> None:
        returncode = proc.wait(timeout=timeout)
        if returncode:
            self._logger.warning(
                f"Process {proc.pid} failed with return code {returncode}"
            )
        else:
            self._logger.debug(f"Process {proc.pid} finished successfully")
