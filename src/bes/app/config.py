import os
import logging

_EDNA_SITE = os.environ.get("EDNA_SITE", None)
if not _EDNA_SITE:
    logging.debug('WARNING: EDNA_SITE is not set, setting it to "ESRF".')
    _EDNA_SITE = "ESRF"

if _EDNA_SITE != "ESRF":
    if "ISPyB_user" not in os.environ:
        raise RuntimeError(f'ISPyB_user is not set for EDNA_SITE="{_EDNA_SITE}"!')
    if "ISPyB_pass" not in os.environ:
        raise RuntimeError(f'ISPyB_pass is not set for EDNA_SITE="{_EDNA_SITE}"!')

_EDNA2_SITE = os.environ.get("EDNA2_SITE", _EDNA_SITE)

_INITIATOR = os.environ.get("PYPUSHFLOW_INITIATOR", "unknown")


class Config:
    EDNA_SITE = _EDNA_SITE  # For example: ESRF_id30a2
    EDNA2_SITE = _EDNA2_SITE  # For example: ESRF_id30a2
    INITIATOR = _INITIATOR  # For example: id30a2
    BESDB_HOST = os.environ.get("BESDB_HOST", "127.0.0.1")
    BESDB_PORT = int(os.environ.get("BESDB_PORT", 27017))
