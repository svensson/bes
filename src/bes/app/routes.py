import os
import time
import pprint
import socket
import signal
import functools
import datetime

import flask
import pymongo
import pymongo.errors
import flask_cors
from bson import json_util
from bson.objectid import ObjectId
from dateutil.parser import parse as parse_date

from bes.workflow_lib import mxnice


main = flask.Blueprint("main", __name__)


def _handle_mongo_timeout(method):
    @functools.wraps(method)
    def wrapper(*args, **kw):
        try:
            return method(*args, **kw)
        except pymongo.errors.ServerSelectionTimeoutError:
            return "BESDB cannot be reached", 500

    return wrapper


@main.route("/bes/requests", methods=["POST"])
@flask_cors.cross_origin(supports_credentials=True)
@_handle_mongo_timeout
def requests_query():
    if not flask.request.json:
        flask.abort(400)
    dictRequest = dict(flask.request.json)
    flask.current_app.logger.debug(dictRequest)
    dictResults = _search_bes(**dictRequest)
    flask.current_app.logger.debug(len(dictResults))
    return str(json_util.dumps(dictResults))


@main.route("/bes/readfile", methods=["POST"])
def readfile():
    fileContent = None
    dictRequest = dict(flask.request.json)
    filePath = dictRequest.get("filePath")
    if filePath:
        if os.path.exists(filePath):
            with open(filePath) as f:
                fileContent = f.read()
        # Limit the number of lines to 10000
        if len(fileContent.split("\n")) > 10000:
            fileContent = "\n".join(fileContent.split("\n")[0:10000])
            fileContent += "...\n"
            fileContent += "Remainder of file truncated\n"
            fileContent += "See following file for full log:\n"
            fileContent += filePath + "\n"
    return fileContent


@main.route("/RUN/<string:workflowName>", methods=["POST"])
def do_run(workflowName, requestDict=None):
    flask.current_app.logger.debug("*" * 80)
    flask.current_app.logger.debug(workflowName)
    if requestDict:
        inData = requestDict
    elif flask.request.json:
        inData = flask.request.json
    else:
        flask.abort(400)

    bes_request_id = str(ObjectId())
    _, bes_port = flask.request.host.split(":")
    inData["besParameters"] = {
        "host": socket.gethostname(),
        "port": int(bes_port),
        "request_id": bes_request_id,
    }
    flask.current_app.logger.debug("inData = %s", inData)

    db_options = _db_options(bes_request_id)
    flask.current_app.logger.debug("db_options = %s", db_options)

    env = _workflow_environment()
    flask.current_app.logger.debug("env = %s", env)
    env = {**os.environ, **env}

    log_subdir = flask.current_app.config["INITIATOR"]
    data_directory = f"/var/log/bes/{log_subdir}"

    run_status = flask.current_app.job_manager.submit(
        bes_request_id, workflowName, inData, db_options, env, data_directory
    )
    flask.current_app.logger.debug("*" * 80)

    flask.current_app.logger.debug(
        "Workflow %s started, request ID = %s", workflowName, bes_request_id
    )
    flask.current_app.logger.debug(pprint.pformat(run_status))
    return bes_request_id


def _db_options(bes_request_id: str) -> dict:
    current_host = socket.gethostname()
    current_port = flask.request.host.split(":")[1]
    besdb_url = _besdb_url()
    initiator = flask.current_app.config["INITIATOR"]

    return {
        "db_type": "besdb",
        "url": besdb_url,
        "host": current_host,
        "port": current_port,
        "request_id": bes_request_id,
        "initiator": initiator,
    }


def _workflow_environment() -> dict:
    return {
        "EDNA_SITE": flask.current_app.config["EDNA_SITE"],
        "EDNA2_SITE": flask.current_app.config["EDNA2_SITE"],
    }


# /BES/bridge/rest/processes/TroubleShooting/RUN?sessionId=70195
@main.route("/BES/bridge/rest/processes/<string:workflowName>/RUN", methods=["POST"])
def do_legacy_run(workflowName):
    requestDict = dict(flask.request.args)
    bes_request_id = do_run(workflowName, requestDict=requestDict)
    return bes_request_id


@main.route("/ABORT/<string:bes_request_id>")
@_handle_mongo_timeout
def do_abort(bes_request_id):
    flask.current_app.logger.debug("Aborting request id %s", bes_request_id)

    # Abort when still running, get status otherwise
    run_status = flask.current_app.job_manager.get(bes_request_id)
    if run_status:
        proc_status = _get_workflow_status_from_handle(run_status)
        if proc_status == "started":
            run_status["proc"].send_signal(signal.SIGKILL)
            status = "aborted"
            run_status["status"] = status
        else:
            status = proc_status
    else:
        status = "not exists"
    flask.current_app.logger.debug(f"{bes_request_id=}: {status}")

    # Update database when aborted
    if status == "aborted":
        collection = _besdb_collection()
        objectId = ObjectId(bes_request_id)
        workflowInfo = collection.find_one({"_id": objectId})
        if not workflowInfo:
            flask.current_app.logger.debug("No database entry found")
        elif workflowInfo.get("status") == status:
            flask.current_app.logger.debug(
                "Database entry status already set to aborted"
            )
        else:
            workflowInfo["status"] = status
            workflowInfo["stopTime"] = datetime.datetime.now()
            collection.update_one(
                {"_id": objectId}, {"$set": workflowInfo}, upsert=False
            )
            flask.current_app.logger.debug("Database entry set to aborted")

    return status


@main.route("/BES/bridge/rest/processes/<string:bes_request_id>/STOP", methods=["POST"])
def do_legacy_abort(bes_request_id):
    return do_abort(bes_request_id)


@main.route("/STATUS/<string:bes_request_id>")
def get_status(bes_request_id):
    collection = _besdb_collection()
    return _get_workflow_status(bes_request_id, collection)


def _get_workflow_status(bes_request_id, collection):
    """Get the workflow status as reported in the BESDB or get it from the process handle."""
    return_value, return_code = _get_workflow_status_from_db(bes_request_id, collection)
    if return_code == 200:
        return return_value.upper(), return_code

    flask.current_app.logger.debug("Request id %s not in the database", bes_request_id)

    run_status = flask.current_app.job_manager.get(bes_request_id)
    if run_status:
        return_value = _get_workflow_status_from_handle(run_status)
        return_code = 200
    else:
        flask.current_app.logger.debug("Request id %s does not exist", bes_request_id)
        return_value = "not exists"
        return_code = 404
    return return_value.upper(), return_code


@_handle_mongo_timeout
def _get_workflow_status_from_db(bes_request_id, collection):
    """Get the workflow status as reported in the BESDB."""
    workflowInfo = collection.find_one({"Request ID": bes_request_id})
    if workflowInfo is None:
        return "not exists", 404
    return workflowInfo["status"], 200


def _get_workflow_status_from_handle(run_status) -> str:
    """Get the workflow status from the process handle."""
    return_code = run_status["proc"].poll()
    # Must be a valid BESDB workflow status
    if return_code is None:
        status = "started"
    elif return_code:
        status = "error"
    else:
        status = "finished"
    run_status["status"] = status
    return status


@main.route("/BES/bridge/rest/processes/<string:bes_request_id>/STATUS")
def get_legacy_status(bes_request_id):
    return get_status(bes_request_id)


@main.route("/SLURM/jobs/pending")
@main.route("/SLURM/jobs/pending/<string:partition>")
def are_jobs_pending(partition=None):
    jobsPending = mxnice.areJobsPending(partitionName=partition)
    return {"jobsPending": jobsPending}


@main.route("/")
def index():
    page = "<!DOCTYPE html>\n"
    page += "<html>\n"
    page += "<head></head>\n"
    page += "<body>\n"
    page += "<h1>BES server running on " + socket.gethostname() + "</h1>\n"
    page += "<br>\n"
    if len(flask.current_app.job_manager) == 0:
        page += "No running tasks<br>\n"
    else:
        page += "Running tasks:<br>\n"
        page += '<table border="1" cellpadding="5" cellspacing="0">\n'
        page += "<tr>\n"
        page += "<th>Request Id</th>\n"
        page += "<th>Start Time</th>\n"
        page += "<th>Status</th>\n"
        page += "</tr>\n"
        collection = _besdb_collection()
        for bes_request_id in flask.current_app.job_manager:
            status, _ = _get_workflow_status(bes_request_id, collection)
            startTime = flask.current_app.job_manager[bes_request_id]["startTime"]
            strDateTime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(startTime))
            page += "<tr>\n"
            page += "<th>{0}</th>\n".format(bes_request_id)
            page += "<th>{0}</th>\n".format(strDateTime)
            page += "<th>{0}</th>\n".format(status)
            page += "</tr>\n"
        page += "</table>\n"
    page += "</body>\n"
    return page


def _besdb_url():
    besdb_host = flask.current_app.config["BESDB_HOST"]
    besdb_port = flask.current_app.config["BESDB_PORT"]
    return f"mongodb://bes:bes@{besdb_host}:{besdb_port}/bes"


def _besdb_collection():
    return pymongo.MongoClient(_besdb_url(), serverSelectionTimeoutMS=3000).bes.bes


def _search_bes(
    listName=[],
    listExcludeName=[],
    id=None,
    initiator=None,
    startDateTime=None,
    stopDateTime=None,
    status=None,
):
    collection = _besdb_collection()
    listWorkflows = []
    listQuery = []
    # Name
    for name in listName:
        if name != "":
            nameQuery = {"name": {"$regex": name, "$options": "i"}}
            listQuery.append(nameQuery)
    # Excluded name
    for excludeName in listExcludeName:
        if excludeName != "":
            nameQuery = {
                "name": {"$regex": "^((?!{0}).)*$".format(excludeName), "$options": "i"}
            }
            listQuery.append(nameQuery)
    # Id
    if id is not None:
        idQuery = {"_id": ObjectId(id)}
        listQuery.append(idQuery)
    # Initiator
    if initiator is not None:
        initiatorQuery = {"initiator": initiator}
        listQuery.append(initiatorQuery)
    # Start date/time
    if startDateTime is not None:
        d1 = parse_date(startDateTime)
        startDateQuery = {"startTime": {"$gt": d1}}
        listQuery.append(startDateQuery)
    # Stop date/time
    if stopDateTime is not None and stopDateTime != "":
        d2 = parse_date(stopDateTime)
        stopDateQuery = {"stopTime": {"$lt": d2}}
        listQuery.append(stopDateQuery)
    # Status
    if status is not None:
        statusQuery = {"status": {"$regex": status, "$options": "i"}}
        listQuery.append(statusQuery)
    if len(listQuery) == 0:
        # Search for everything
        # Projection - remove actors
        for workflow in collection.find({}, {"actors": 0}):
            listWorkflows.append(workflow)
    else:
        if len(listQuery) == 1:
            query = listQuery[0]
        else:
            query = {"$and": listQuery}
        if id is None:
            for workflow in collection.find(query, {"actors": 0}):
                listWorkflows.append(workflow)
        else:
            for workflow in collection.find(query):
                listWorkflows.append(workflow)

    return listWorkflows
