import json
import time
from collections import abc
from typing import Iterator

from .subprocess_manager import SubprocessManager


class JobManager(abc.Mapping):
    """Submit jobs in sub-processes."""

    def __init__(self, logger, **kw) -> None:
        self._subprocess_manager = SubprocessManager(logger, **kw)
        self._subprocess_manager.start()
        self._run_status = dict()
        self._logger = logger

    def __getitem__(self, bes_request_id: str) -> dict:
        return self._run_status.__getitem__(bes_request_id)

    def __iter__(self) -> Iterator[str]:
        return self._run_status.__iter__()

    def __len__(self) -> int:
        return self._run_status.__len__()

    def submit(
        self,
        bes_request_id: str,
        workflowName: str,
        inData: dict,
        db_options: dict,
        env: dict,
        data_directory: str,
        time_profiling: bool = False,
    ) -> dict:
        cmd = [
            "bes-run-workflow",
            "--inData",
            json.dumps(inData),
            "--db-options",
            json.dumps(db_options),
            workflowName,
            "--log-level",
            "WARNING",
            "--ppf-log-level",
            "WARNING",
            "--ewoks-log-level",
            "INFO",
            "--data-directory",
            data_directory,
        ]
        if time_profiling:
            cmd.append("--time-profile")

        proc = self._subprocess_manager.start_subprocess(cmd, env)

        run_status = {
            "workflowName": workflowName,
            "status": "RUNNING",
            "startTime": time.time(),
            "proc": proc,
        }

        self._run_status[bes_request_id] = run_status
        return run_status
